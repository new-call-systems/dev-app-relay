# -*- mode: ruby -*-
# vi: set ft=ruby :

module VagrantPlugins
  module GuestLinux
    class Plugin < Vagrant.plugin("2")
      guest_capability("linux", "change_host_name") { Cap::ChangeHostName }
      guest_capability("linux", "configure_networks") { Cap::ConfigureNetworks }
    end
  end
end

Vagrant.configure("2") do |config|

  #
  # Box
  #
  config.vm.box = "ailispaw/barge"
  config.vm.box_check_update = false

  #
  # Network
  #
  config.vm.network "private_network", ip: "192.168.33.55"
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 8000, host: 8000
  config.vm.network "forwarded_port", guest: 19000, host: 19000
  config.vm.network "forwarded_port", guest: 19001, host: 19001
  config.vm.network "forwarded_port", guest: 19002, host: 19002

  #
  # Synced Folder
  #
  config.vm.synced_folder ".", "/vagrant"

  #
  # Plugin Disabled
  #
  config.vbguest.auto_update = false            if Vagrant.has_plugin?("vagrant-vbguest")
  config.hostsupdater.remove_on_suspend = false if Vagrant.has_plugin?('vagrant-hostsupdater')

  #
  # VirtualBox Setting
  #
  config.vm.provider :virtualbox do |vb|
    vb.name = "cocoen"
    vb.customize [
      "modifyvm", :id,
      "--cpus", 2,
      "--cpuexecutioncap", "80",
      "--memory", "4096",
    ]
  end

  #
  # Provision
  #
  config.vm.provision "shell", inline: <<-SHELL
    set -ex

    # time
    cd /etc; unlink localtime; ln -s ../usr/share/zoneinfo/Etc/GMT-9 localtime

    # docker
    sudo /etc/init.d/docker restart latest

    # docker-compose
    wget -nv -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m`
    chmod +x docker-compose-`uname -s`-`uname -m`
    sudo mv docker-compose-`uname -s`-`uname -m` /opt/bin/docker-compose
    sudo chown root:root /opt/bin/docker-compose

    # docker build
    cd /vagrant && docker-compose build --no-cache;

    # Vagrant
    echo "cd /vagrant" >> /home/bargee/.bashrc
  SHELL

  #
  # App start
  #
  if ARGV[0] == "up" || ARGV[0] == "reload" then
    config.vm.provision "shell", run: "always", inline: <<-SHELL
      sudo pkg install make
      cd /vagrant && make up
    SHELL
  end

end
