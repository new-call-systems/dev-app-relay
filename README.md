# dev-app-relay #

### 概要 ###
* 当レポジトリはcoco-enの「中継サーバ」「業者アプリ」「入居者アプリ」を同時に起動することができる
* ローカルアプリからローカルの中継サーバに対してリクエストを投げること可能としている

### 環境 ###
* VirtualBox 6.1.6
* Vagrant 2.2.7

### 環境構築手順 ###
1. VirtualBoxとVagrantをインストール　※必ずしも環境で記述してあるバージョンである必要はない
2. ソースをクローン
3. 以下のパスの直下に「sessions」「cache」という空のフォルダーを作成<br>`\dev-app-relay\relay-server\storage\framework`
4. envファイルにローカルIPを記載 <br>4-1. ipconfigでローカルIP確認<br>4-2.dev-app-relay直下のenvファイルにローカルipを記載<br>4-3.resident-gui直下のenvファイルにローカルipを記載
#### /dev-app-relay/.env ####
```
COMPOSE_PROJECT_NAME=cocoen
DOCUMENTROOT=/opt/cocoen

#
# ビルド時にExpoのアカウントが必要
# https://expo.io/signup
#
EXPO_CLI_USER=smillione
EXPO_CLI_PASSWORD=1234qwer

#
# ExpoエミュレータをLAN内で配信するたLocalPCのIPを指定する
#
REACT_NATIVE_PACKAGER_HOSTNAME=192.168.92.15 #here

#
# 開発時に任意の中継サーバを使いたいときに指定する
#
REACT_NATIVE_RELAY_SERVER=http://192.168.92.17:8000  #here 
#REACT_NATIVE_RELAY_SERVER=http://ec2-54-249-211-57.ap-northeast-1.compute.amazonaws.com
```


#### /dev-app-relay/resident-gui/.env ####
```
REACT_APP_ENDPOINT_RELAY_SERVER="http://192.168.92.17:8000" #here
REACT_APP_BASIC_KEY=""
REACT_APP_BASE_URL=""
```


5. vagrantfileがある直下でvagrant upを実行
```
$ vagrant up
```

5. 仮想サーバにssh接続
```
$ vagrant ssh
```

6. 仮想サーバの中継サーバのキャッシュをクリア
```
#仮想サーバー内
$ make cahce.clear
```

7.boot関数のID生成箇所をコメントアウト
#### deve-app-relay/relay-server/app/Models/ManagementCompany.php #####
```php
   public static function boot()
    {
        parent::boot();
        self::creating(function(ManagementCompany $mc){
            $client = new Client();
//            $mc->management_company_id = 'MC_'.$client->generateId(12, Client::MODE_DYNAMIC);
        });
    }
```
####  dev-app-relay/relay-server/app/Models/Vendor.php ####
```php
    public static function boot()
    {
        parent::boot();
        self::creating(function(Vendor $vendor){
            $client = new Client();
//            $vendor->vendor_id = 'VN_'.$client->generateId(12, Client::MODE_DYNAMIC);
        });
    }
```

8.マイグレーションを実行
```
#仮想サーバー内
$ make db.reset
```
9.　7で外したコメントアウトをもとに戻す

10.生成されたDBの「management_company_api_access_keys」を参照しaccess_key_idとsecret_keyをコロンで区切ってenvファイルに記載<br>以下の場合

![img.png](img.png)

```
REACT_APP_ENDPOINT_RELAY_SERVER="http://192.168.92.17:8000"
REACT_APP_BASIC_KEY="U3CEM97YAWVZ8XS09OFK:c80hoqmc0o6qnc2hq3ytvduce3jrz64gbqv4shqs"
REACT_APP_BASE_URL=""
```


### 接続情報　###
vagrant upを行うと以下のappが利用可能になり、ローカル入居者アプリから中継サーバーのAPIを叩きログインやDB登録等ができる  
業者APPについてはexpoで実機で確認する必要があるため別途記載する


#### 入居者アプリ ####
[http://192.168.33.55:3000/](http://192.168.33.55:3000/)
- 入居者コード:30000001
- 物件コード:1001
- 部屋コード:2001

#### システムオーナーWEB ####
[http://192.168.33.55:8000/so/login](http://192.168.33.55:8000/so/login)
- メールアドレス:so@test.com
- パスワード:password

#### 管理者WEB ####
[http://192.168.33.55:8000/mc/login](http://192.168.33.55:8000/mc/login)

- メールアドレス:mc_manager@test.com
- パスワード:password

#### 作業員管理WEB ####
[http://192.168.33.55:8000/vn/login](http://192.168.33.55:8000/vn/login)
- メールアドレス:vendor_managemer@test.com
- パスワード:password



### Expoによる業者app構築手順 ###
1.vagrant upで開発環境を立ち上げ、vagrant sshでマシン内に入る

2.監視できるファイル数が上限を確認する
```shell
$ cat /proc/sys/fs/inotify/max_user_watches
8192
```

3.デフォルトの8192ならinotifyの上限値を永続的に増加させるコマンドを実行
```shell
$ echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
$ sudo sysctl -p
```
１行目でfs.inotify.max_user_watches=524288をteeコマンドに渡してsysctl.confに書き込んでいます。そして２行目ではロードしています。  
これをしないとdockerでExpoを立ち上げる際にエラーが発生する

4.dockerが立ち上がっているか確認
```shell
$ docker-compose ps
```
以下のようにステートがすべて**UP**だったらOK
![img_1.png](img_1.png)

これ以降は一応構築できたがエラーが発生しているので後日記載
