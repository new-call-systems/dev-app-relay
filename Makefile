NAME=cocoen
DOCKER=docker
COMPOSE=docker-compose

LARAVEL_HOME=./app

PHP_CMD=$(COMPOSE) exec php /bin/ash -c
MYSQL_CMD=$(COMPOSE) exec mysql /bin/ash -c
REACT_CMD=$(COMPOSE) exec react /bin/bash -c
EXPO_CMD=$(COMPOSE) exec expo /bin/bash -c

DOCKER_VOLUME_PATH_PHP=/mnt/data/var/lib/docker/volumes/$(NAME)_composer/_data
DOCKER_VOLUME_PATH_MYSQL=/mnt/data/var/lib/docker/volumes/$(NAME)_mydata/_data
DOCKER_VOLUME_PATH_REACT=/mnt/data/var/lib/docker/volumes/$(NAME)_node_modules_react/_data
DOCKER_VOLUME_PATH_EXPO=/mnt/data/var/lib/docker/volumes/$(NAME)_node_modules_expo/_data


#=====================================================
# Docker
#=====================================================
up:
	$(COMPOSE) up -d

down:
	$(COMPOSE) down

restart:
	make down
	make up

ps:
	$(COMPOSE) ps

log:
	$(COMPOSE) logs -f


#=====================================================
# PHP
#=====================================================
ssh.php:
	$(COMPOSE) exec php /bin/ash

build.php:
	$(COMPOSE) build php

rebuild.php:
	make down
	-sudo rm -rf $(DOCKER_VOLUME_PATH_PHP)/*;
	$(COMPOSE) build --no-cache php

composer.install:
	$(PHP_CMD) 'composer install'

composer.update:
	$(PHP_CMD) 'composer update'

cache.clear:
	rm -rf $(LARAVEL_HOME)/bootstrap/cache/*
	rm -rf $(LARAVEL_HOME)/storage/framework/cache/data/*
	rm -rf $(LARAVEL_HOME)/storage/framework/sessions/*
	rm -rf $(LARAVEL_HOME)/storage/framework/views/*
	rm -rf $(LARAVEL_HOME)/storage/logs/*
	$(PHP_CMD) 'php artisan cache:clear'; \
	$(PHP_CMD) 'php artisan config:clear'; \
	$(PHP_CMD) 'php artisan route:clear'; \
	$(PHP_CMD) 'php artisan view:clear'; \
	$(PHP_CMD) 'php artisan clear-compiled' \
	$(PHP_CMD) 'composer dump-autoload';
#	$(PHP_CMD) 'composer dump-autoload --optimize';
#	$(PHP_CMD) 'php artisan route:cache';
#	$(PHP_CMD) 'php artisan config:cache';
#	$(PHP_CMD) 'php artisan config:cache --env=development';

db.reset:
	$(PHP_CMD) 'php artisan migrate:fresh --seed'

db.migrate:
	$(PHP_CMD) 'php artisan migrate'

db.seed:
	$(PHP_CMD) 'php artisan db:seed'


#=====================================================
# React
#=====================================================
ssh.react:
	$(COMPOSE) exec react /bin/bash

rebuild.react:
	make down
	-sudo rm -rf $(DOCKER_VOLUME_PATH_REACT)/*;
	$(COMPOSE) build --no-cache react


#=====================================================
# React-Native
#=====================================================
ssh.expo:
	$(COMPOSE) exec expo /bin/bash

rebuild.expo:
	make down
	-sudo rm -rf $(DOCKER_VOLUME_PATH_EXPO)/*;
	$(COMPOSE) build --no-cache expo


#=====================================================
# MySQL
#=====================================================
ssh.mysql:
	$(COMPOSE) exec mysql /bin/bash

build.mysql:
	$(COMPOSE) build mysql

rebuild.mysql:
	-sudo rm -rf $(DOCKER_VOLUME_PATH_MYSQL)/*;
	$(COMPOSE) build --no-cache mysql


#=====================================================
# Development
#=====================================================
lint:
	$(EXPO_CMD) 'yarn run lint:fix';

touch:
	find ./expo/src -type f \( -name "*.ts" -o -name "*.tsx" \) | xargs touch

