function dispLoading(){
    if ($('#loading').length == 0) {
        $(':button').prop('disabled', true);
        $('body').append('<div id="loading"><div class="loadingMsg"><div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div></div></div>');
    }
}

function removeLoading(){
    $("#loading").remove();
    $(':button').prop('disabled', false);
}

function buttonDisabled() {
    $('button').prop('disabled', true);
}

