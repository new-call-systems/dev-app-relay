<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->name('api.')->group(function() {

    //************************************
    // 業者向けネイティブアプリ
    //************************************
    Route::prefix('vn')->group(function () {
        Route::prefix('{version}')->group(function () {
            //認証なし
            Route::prefix('auth')->group(function () {
                Route::post('login', 'VendorMemberController@login');
                Route::post('token', 'VendorMemberController@accessTokenReissue');
            });

            //認証あり
            Route::middleware('auth:guard_vendor_member')->group(function () {
                //問合せ系
                Route::prefix('request')->group(function () {
                    Route::get('', 'VendorMemberController@getRequestList');
                    Route::get('{request_id}', 'VendorMemberController@getRequestInfo');
                    Route::put('{request_id}', 'VendorMemberController@changeRequestStatus');
                    Route::put('{request_id}/bid', 'VendorMemberController@bidRequest');
                    Route::post('{request_id}/cancel', 'VendorMemberController@requestCancel');
                    Route::get('{request_id}/handlers', 'VendorMemberController@getRequestHandlers');
                    Route::put('{request_id}/handlers/{request_handle_id}', 'VendorMemberController@updateRequestHandler');
                    Route::post('{request_id}/reports', 'VendorMemberController@createReport');
                    Route::get('{request_id}/reports/{report_id}', 'VendorMemberController@getReport');
                    Route::put('{request_id}/reports/{report_id}', 'VendorMemberController@updateReport');
                    Route::post('{request_id}/reports/{report_id}/draft', 'VendorMemberController@changeReportStatus');
                    Route::get('{request_id}/reports/{report_id}/config', 'VendorMemberController@getAwsConfig');
                });
                //完了報告書
                Route::get('reports', 'VendorMemberController@getReportList');
                //その他
                Route::get('timeSection', 'CommonController@getTimeSections');
                Route::put('vendorMember/{user_id}/{vendor_id}', 'VendorMemberController@updateVendorMember');
                Route::get('badge', 'VendorMemberController@getBadgeInfo');
                Route::post('vendorMember/{user_id}/{vendor_id}/deviceId', 'VendorMemberController@updateFirebaseDeviceId');
            });
        });
    });


    //************************************
    // 入居者向けネイティブアプリ
    //************************************
    Route::prefix('rd')->group(function () {
        Route::prefix('{version}')->group(function () {
            //認証なし
            Route::prefix('auth')->group(function () {
                Route::post('login', 'ResidentController@getOtp');
                Route::get('login', 'ResidentController@login');
                Route::post('localLogin', 'ResidentController@localLogin');
                Route::post('token', 'ResidentController@accessTokenReissue');
                Route::get('liff/{line_client_id}', 'LineController@getLiffId');
                Route::post('line-login', 'LineController@login');
            });

            //認証あり
            Route::middleware('auth:guard_resident')->group(function () {
                //問合せ系
                Route::prefix('request')->group(function () {
                    Route::get('', 'ResidentController@getRequestList');
                    Route::post('', 'ResidentController@createRequest');
                    Route::get('{request_id}', 'ResidentController@getRequestInfo');
                    Route::post('{request_id}/order', 'ResidentController@requestOrder');
                    Route::post('{request_id}/cancel', 'ResidentController@requestCancel');
                    Route::post('{request_id}/solved', 'ResidentController@requestSolved');
                    Route::post('{request_id}/feedback', 'ResidentController@createFeedback');
                    Route::get('{request_id}/preSignedUrl', 'ResidentController@getS3PreSignedURL');
                    Route::post('{request_id}/approve', 'ResidentController@requestBidApprove');
                });
                //LINE系
                Route::prefix('line')->group(function () {
                    Route::get('auth', 'LineController@getUrlLineAccountLinkage');
                    Route::delete('auth', 'LineController@cancelingLineAccountLinkage');
                    Route::get('verify', 'LineController@lineAccountLinkageAuthentication');
                });
                //その他
                Route::get('managementCompany', 'ResidentController@getManagementCompanyInfo');
                Route::get('timeSection', 'CommonController@getTimeSections');
                Route::get('badge', 'ResidentController@getBadgeInfo');
            });
        });
    });

    //************************************
    // 管理会社向けAPI
    //************************************
    Route::prefix('mc')->group(function () {
        Route::prefix('{version}')->group(function () {
            //認証あり
            Route::middleware('auth:guard_management_company')->group(function () {
                Route::post('account/tempRegistration', 'ManagementCompanyController@residentTempRegistration');
            });
        });
    });

    //************************************
    // 基幹システムCSV import or export
    //************************************
    //import
    Route::post('import/facilities' ,'CoreDataImportController@importFacilities');
    Route::post('import/parking' ,'CoreDataImportController@importParking');
    Route::post('import/properties' ,'CoreDataImportController@importProperties');
    Route::post('import/proprietary_facilities' ,'CoreDataImportController@importProprietaryFacilities');
    Route::post('import/residents' ,'CoreDataImportController@importResidents');
    Route::post('import/rooms' ,'CoreDataImportController@importRooms');
    Route::post('import/shared_facilities' ,'CoreDataImportController@importSharedFacilities');
    Route::post('import/vendors' ,'CoreDataImportController@importVendors');


    //************************************
    // その他
    //************************************
    //ヘルスチェック
    Route::get('ping', function () {
        return response()->json([], 200);
    });

});
