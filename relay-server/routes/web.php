<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::domain('localhost')->group(function () {
     Route::view('/', 'index');
     Route::view('/mock', 'mock.index');
// });

//*************************************************************************************************************************
// システムオーナー向け
//*************************************************************************************************************************
Route::namespace('So')->name('so.')->prefix('so')->group(function () {

    //ログイン認証関連
    Auth::routes([
        'register' => false,
        'verify'   => false
    ]);

    //ログイン認証後
    Route::middleware(['auth:guard_system_owner', 'permission:system_owner_permission','check.session.timeout'])->group(function () {
        //============
        // 画面
        //============
        //ダッシュボード画面
        Route::get('/', 'DashboardController@index')->name('dashboard');
        //業者一覧画面
        Route::get('/vendor', 'VendorController@index')->name('vendor.index');
        //管理会社一覧画面
        Route::get('/managementCompany', 'ManagementCompanyController@index')->name('managementCompany.index');

        //============
        // その他
        //============
        //業者登録処理
        Route::post('/vendor', 'VendorController@store')->name('vendor.store');
        //業者更新処理
        Route::put('/vendor/{vendorId}', 'VendorController@update')->name('vendor.update');
        //管理会社登録処理
        Route::post('/managementCompany', 'ManagementCompanyController@store')->name('managementCompany.store');
        //管理会社更新処理
        Route::put('/managementCompany/{managementCompanyId}', 'ManagementCompanyController@update')->name('managementCompany.update');
    });
});


//*************************************************************************************************************************
// 業者向け
//*************************************************************************************************************************
Route::namespace('Vn')->name('vn.')->prefix('vn')->group(function () {

    //ログイン認証関連
    Auth::routes([
        'register' => false,
        'reset'    => false,
        'verify'   => false
    ]);

    //ログイン認証後
    Route::middleware(['auth:guard_vendor_manager', 'permission:vendor_manager_permission','check.session.timeout'])->group(function () {
        //============
        // 画面
        //============
        //ダッシュボード画面
        Route::get('/', 'DashboardController@index')->name('dashboard');
        //作業員管理画面
        Route::get('/vendorMember', 'VendorMemberController@index')->name('vendorMember.index');
        //問合せ入札履歴画面
        Route::get('/bidHistory', 'RequestController@getBidHistory')->name('bidHistory');
        //入札可能問合せ一覧画面
        Route::get('/bidPossible', 'RequestController@getBidPossible')->name('bidPossible');
        //完了報告一覧画面
        Route::get('/report', 'ReportController@getReportList')->name('reportList');
        //完了報告書詳細画面
        Route::get('/report/{report_id}', 'ReportController@getReportDetail')->name('reportDetail');
        //完了報告書編集画面
        Route::get('/report/{report_id}/edit', 'ReportController@getReportEdit')->name('reportEdit');
        //完了報告書プレビュー画面
        Route::post('/report/{report_id}/preview', 'ReportController@getReportPreview')->name('reportPreview');
        //マイページ
        Route::get('/mypage', 'MyPageController@index')->name('mypage');

        //============
        // その他
        //============
        //作業員登録処理
        Route::post('/vendorMember', 'VendorMemberController@store')->name('vendorMember.store');
        //作業員更新処理
        Route::put('/vendorMember/{user_id}', 'VendorMemberController@update')->name('vendorMember.update');
        //完了報告書更新処理
        Route::put('/report/{report_id}', 'ReportController@updateReport')->name('updateReport');
        //完了報告書提出処理
        Route::post('/report/{report_id}', 'ReportController@submitReport')->name('submitReport');
        //完了報告書PDFダウンロード処理
        Route::post('/report/{report_id}/pdf', 'ReportController@downloadReportPdf')->name('downloadReportPdf');
        //マイページ・更新処理
        Route::post('/mypage/update', 'MyPageController@update')->name('mypage.update');
        //問合せ担当者変更処理
        Route::post('/request/{request_id}/changeRequestHandler', 'RequestController@changeRequestHandler')->name('changeRequestHandler');
    });
});


//*************************************************************************************************************************
// 管理会社向け
//*************************************************************************************************************************
Route::namespace('Mc')->name('mc.')->prefix('mc')->group(function () {

    //ログイン認証関連
    Auth::routes([
        'register' => false,
        'reset'    => false,
        'verify'   => false
    ]);

    //ログイン認証後
    Route::middleware(['auth:guard_management_company_member', 'permission:management_company_member_permission','check.session.timeout'])->group(function () {
        //============
        // 画面
        //============
        //ダッシュボード画面
        Route::get('/', 'DashboardController@index')->name('dashboard');
        //問合せ一覧画面
        Route::get('/request', 'RequestController@index')->name('request');
        //問合せ一覧画面(ID指定)
        Route::get('/request/{request_id}/once', 'RequestController@index')->name('request.once');
        //ユーザー管理画面
        Route::get('/managementCompanyMember', 'ManagementCompanyMemberController@index')->name('managementCompanyMember');
        //完了報告一覧画面
        Route::get('/report', 'ReportController@getReportList')->name('reportList');
        //完了報告一覧画面
        Route::get('/report/{report_id}/once', 'ReportController@getReportList')->name('reportList.once');
        //完了報告書詳細画面
        Route::get('/report/{report_id}', 'ReportController@getReportDetail')->name('reportDetail');
        //入居者一覧画面
        Route::get('/resident', 'ResidentController@getResidentList')->name('residentList');

        //============
        // その他
        //============
        //AccessKeyファイルダウンロード処理
        Route::post('/getAccessKey', 'AccessKeyController@getAccessKey')->name('getAccessKey');
        //ユーザー登録処理
        Route::post('/managementCompanyMember', 'ManagementCompanyMemberController@store')->name('managementCompanyMember.store');
        //ユーザー更新処理
        Route::put('/managementCompanyMember/{user_id}', 'ManagementCompanyMemberController@update')->name('managementCompanyMember.update');
        //問合せ履歴 エクスポート
        Route::post('/request/export', 'RequestController@exportRequestCsv')->name('request.export');
        //完了報告書 承認処理
        Route::post('/report/{report_id}/approve', 'ReportController@approveReport')->name('approveReport');
        //完了報告書 差し戻し処理
        Route::post('/report/{report_id}/decline', 'ReportController@declineReport')->name('declineReport');
        //完了報告書PDFダウンロード処理
        Route::post('/report/{report_id}/pdf', 'ReportController@downloadReportPdf')->name('downloadReportPdf');
        //完了報告書ZIPファイルダウンロード処理
        Route::post('/report/{report_id}/zip', 'ReportController@downloadReportZip')->name('downloadReportZip');
    });
});


// パスワードリセット関連
Route::prefix('password_reset')->name('password_reset.')->group(function () {
    Route::prefix('email')->name('email.')->group(function () {
        // パスワードリセットメール送信フォームページ
        Route::get('/', 'Api\ForgotPasswordController@emailFormResetPassword')->name('form');
        // メール送信処理
        Route::post('/', 'Api\ForgotPasswordController@sendEmailResetPassword')->name('send');
        // メール送信完了ページ
        Route::get('/send_complete', 'Api\ForgotPasswordController@sendComplete')->name('send_complete');
    });
    // パスワード再設定ページ
    Route::get('/edit', 'Api\ForgotPasswordController@edit')->name('edit');
    // パスワード更新処理
    Route::post('/update', 'Api\ForgotPasswordController@update')->name('update');
    // パスワード更新終了ページ
    Route::get('/edited', 'Api\ForgotPasswordController@edited')->name('edited');
});


//*************************************************************************************************************************
// その他
//*************************************************************************************************************************
Route::name('other.')->group(function () {
    //============
    // 画面
    //============
    //本登録画面
    Route::get('/account/register', 'Mc\AccountController@index')->name('account');
    //本登録処理
    Route::post('/account/register', 'Mc\AccountController@register')->name('account.register');
});

