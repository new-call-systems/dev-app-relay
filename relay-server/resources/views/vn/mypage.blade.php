@extends('layouts.vn.app')

@section('content')
<ul class="nav nav-tabs pl-4"></ul>
<div class="container-fluid px-5 mt-n3">
    <form method="POST" action="{{ route('vn.mypage.update') }}" onsubmit="buttonDisabled();">
        @csrf
        <div class="row justify-content-center mt-4">
            <div class="col-md-12 mt-3">
                <label for="work_area" style="color: #464f69;">対応エリア</label>
                <div class="col-md-6 mypage-work-items">
                    @php
                        $work_areas = ['朝倉郡筑前町','朝倉郡東峰村','朝倉市','飯塚市','糸島市','うきは市','大川市','大野城市','大牟田市','小郡市','遠賀郡芦屋町','遠賀郡岡垣町','遠賀郡遠賀町','遠賀郡水巻町','春日市','糟屋郡宇美町','糟屋郡粕屋町','糟屋郡篠栗町','糟屋郡志免町','糟屋郡新宮町','糟屋郡須惠町','糟屋郡久山町','嘉穂郡桂川町','嘉麻市','北九州市小倉北区','北九州市小倉南区','北九州市戸畑区','北九州市門司区','北九州市八幡西区','北九州市八幡東区','北九州市若松区','鞍手郡鞍手町','鞍手郡小竹町','久留米市','古賀市','田川郡赤村','田川郡糸田町','田川郡大任町','田川郡川崎町','田川郡香春町','田川郡添田町','田川郡福智町','田川市','太宰府市','筑後市','筑紫野市','築上郡上毛町','築上郡築上町','築上郡吉富町','那珂川市','中間市','直方市','福岡市早良区','福岡市城南区','福岡市中央区','福岡市西区','福岡市博多区','福岡市東区','福岡市南区','福津市','豊前市','三井郡大刀洗町','三潴郡大木町','京都郡苅田町','京都郡みやこ町','みやま市','宮若市','宗像市','柳川市','八女郡広川町','八女市','行橋市',];
                        $areas = array();
                        if (old('work_areas')) {
                            $areas = old('work_areas');
                        } else {
                            $areas = $vendor->work_areas;
                        }
                    @endphp
                    @foreach ($work_areas as $idx => $area)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="work_area_{{$idx}}" name="work_areas[]" value="{{$area}}" @if(in_array($area, $areas, true)) checked @endif>
                            <label class="form-check-label" for="work_area_{{$idx}}">{{$area}}</label>
                        </div>
                    @endforeach
                    @error('work_areas')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <label for="work_time_from" style="color: #464f69;">対応可能曜日</label>
                <div class="col-md-6 mypage-work-items">
                    @php
                        $work_weeks = ['日', '月', '火', '水', '木', '金', '土'];
                        $weeks = array();
                        if (old('work_weeks')) {
                            $weeks = old('work_weeks');
                        } else {
                            $weeks = $vendor->work_weeks;
                        }
                    @endphp
                    @foreach ($work_weeks as $idx => $week)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="work_week_{{$idx}}" name="work_weeks[]" value="{{$week}}" @if(in_array($week, $weeks, true)) checked @endif>
                            <label class="form-check-label" for="work_week_{{$idx}}">{{$week}}</label>
                        </div>
                    @endforeach
                    @error('work_weeks')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <label for="work_time_from" style="color: #464f69;">対応可能時間</label>
                <div class="col-md-6 pl-0">
                    <input type="time" class="form-control d-inline-block @error('work_time_from') is-invalid @enderror" name="work_time_from" id="work_time_from" style="width:150px;" value="{{old('work_time_from', substr($vendor->work_time_from, 0, 5))}}">
                    <span class="d-inline-block">～</span>
                    <input type="time" class="form-control d-inline-block @error('work_time_to') is-invalid @enderror" name="work_time_to" id="work_time_to" style="width:150px;" value="{{old('work_time_to', substr($vendor->work_time_to, 0, 5))}}">
                    @error('work_time_from')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    @error('work_time_to')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <label style="color: #464f69;">対応可能項目</label>
                <div class="col-md-6 mypage-work-items">
                    @php
                        $work_items = ['エアコン','水廻り','ガス廻り','各種設備','スイッチ／コンセント','感知器（熱／煙）','クロス','床','ガラス','サッシ','畳／襖','網戸','建具','換気扇','照明','鍵','物干','解体','大工／造作','木工','軽鉄／金物','電気','通信','左官','給排水','空調','美装','インターホン','自動扉（オートロック含む）','機械警備','郵便受','宅配ボックス','掲示板','エレベーター','立体駐車場','駐輪場','テレビアンテナ','インターネット','電話','監視カメラ','水廻り（貯水槽／直結増圧ポンプ）','ガス廻り（都市ガス／ＬＰガス）','電気（低圧／高圧）','消防設備（消火器／防火扉／火災受信機／スプリンクラー／誘導灯）','植栽','照明（共用灯／非常灯）','巡回清掃（日常／定期（機械洗浄含む）／特別（高圧洗浄含む）・・ゴミ庫／廃棄問題含む）','雑排水管','汚水管','雨樋（雨水管含む）','防水','塗装','タイル','サイン','外部足場','外壁診断',];
                        $items = array();
                        if (old('work_items')) {
                            $items = old('work_items');
                        } else {
                            $items = $vendor->work_items;
                        }
                    @endphp
                    @foreach ($work_items as $idx => $item)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="work_item_{{$idx}}" name="work_items[]" value="{{$item}}" @if(in_array($item, $items, true)) checked @endif>
                            <label class="form-check-label" for="work_item_{{$idx}}">{{$item}}</label>
                        </div>
                    @endforeach
                    @error('work_items')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <button type="submit" class="btn btn-search" style="width:110px;">保存</button>
            </div>
        </div>
    </form>
</div>

<script>
</script>

@endsection