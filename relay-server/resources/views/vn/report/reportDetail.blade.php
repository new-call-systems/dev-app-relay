<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>reportDetail</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">

    <style type="text/css" media="print">
        body {
            -webkit-print-color-adjust: exact;
        }

        .print-display-none {
            display: none;
        }

        .reportDetailDescription {
            height: 100%;
        }
    </style>
</head>
@if (!$report)
<body>参照できない完了報告書です。</body>
@elseif (session('is_submit'))
<script>
$(function() {
    window.open("{{ route('vn.reportList') }}?s=submitted&section={{config('const.Reports.DispSection.SUBMITTED')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@elseif (session('is_update'))
<script>
$(function() {
    window.open("{{ route('vn.reportList') }}?s=updated&section={{config('const.Reports.DispSection.NOT_SUBMITTED')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@elseif (session('is_update_decline'))
<script>
$(function() {
    window.open("{{ route('vn.reportList') }}?s=updated&section={{config('const.Reports.DispSection.REMAND')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@else
<body class="pt-5" style="background-color:#fff;">
    <!-- フラッシュメッセージ -->
    @if ($errors->any())
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="alert alert-danger col-md-12">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
    <div class="container-fluid mt-n3 px-0">
        <div class="reportDetailTitle">
            @if (isset($preview))
                プレビュー
            @else
                完了報告書(@if ($report->report_status === config('const.Reports.Status.DRAFT'))未提出@elseif($report->report_status === config('const.Reports.Status.DECLINED'))差戻し@else提出済み@endif)
            @endif
        </div>
        <div class="row justify-content-center mt-3 px-5">
            <div class="col-md-6">
                <span class="reportDetailDateLabel">受付日</span>
                <span class="reportDetailDate">{{ Cmn::dateFormat($report->reception_date) }}</span>
                <span class="reportDetailActionSection">@if ($report->action_section === '1') 一次対応 @else 二次対応 @endif</span>
                @if ($report->action_status === config('const.Reports.ActionStatus.COMPLETED'))
                            <span class="reportDetailActionStatusComplete">対応完了</span>
                        @else
                            <span class="reportDetailActionStatusIncomplete">未完了引継ぎ</span>
                        @endif
            </div>
            <div class="col-md-6 text-right">
                <a class="print-display-none" href="javascript:pdfForm.submit()"><img src="{{ asset('img/btn-pdfdl.svg') }}"></a>
                <a class="print-display-none" href="javascript:void(0)" onclick="print();return false;"><img class="ml-2" src="{{ asset('img/btn-print.svg') }}"></a>
                <form method="POST" name="pdfForm" action="{{ route('vn.downloadReportPdf', ['report_id' => $report->request_report_id]) }}" target="_blank">
                    @csrf
                    <input type="hidden" name="action_date" value="{{$report->action_date}}">
                    <input type="hidden" name="starting_time" value="{{$report->starting_time}}">
                    <input type="hidden" name="ending_time" value="{{$report->ending_time}}">
                    <input type="hidden" name="construction_amount" value="{{$report->construction_amount}}">
                    <input type="hidden" name="description" value="{{$report->description}}">
                    <input type="hidden" name="action_section" value="{{$report->action_section}}">
                    <input type="hidden" name="action_status" value="{{$report->action_status}}">
                </form>
            </div>
        </div>
        <div class="row justify-content-center mt-2 px-5">
            <div class="col-md-12 reportDetailManagementCompanyName">{{ $report->management_company_name }}</div>
            <div class="col-md-12 reportDetailPropertyName">{{ $report->property_name }}</div>
            <div class="col-md-12 reportDetailRoomNo">{{ $report->room_no }}号室</div>
        </div>
        <div class="row justify-content-center mt-2 px-5">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-man.svg') }}">お客様名</div><div class="col-md-6 reportDetailInfoValue">{{ $report->resident_name }} 様</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-date.svg') }}">工事日</div><div class="col-md-6 reportDetailInfoValue">{{ Cmn::dateFormat($report->action_date) }}</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-time.svg') }}">対応時間</div><div class="col-md-6 reportDetailInfoValue">{{ substr($report->starting_time, 0, 5) }}-{{ substr($report->ending_time, 0, 5) }}</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-bill.svg') }}">工事金額</div><div class="col-md-6 reportDetailInfoValue">{{ number_format($report->construction_amount) }}円</div>
                    @if ($report->image_paths)
                        <div class="col-md-12 mt-3 reportDetailImg">
                            <a href="#lightbox" data-toggle="modal">
                                <img style="max-height:225px;max-width:100%;" src="{{ $report->image_paths[0] }}">
                            </a>
                        </div>
                    @else
                        <div class="col-md-12 mt-3 reportDetailNoImg">
                            <img style="height:100%;" src="{{ asset('img/no-image-l.svg') }}">
                        </div>
                    @endif
                    <div class="col-md-12 reportDetailDeclare mt-3">上記の内容で作業を実施しました</div>
                    <div class="col-md-12 reportDetailUserName">{{ $report->user_name }}</div>
                </div>
            </div>
            <div class="col-md-8 reportDetailDescription">
                @if ($report->comment)
                    <div class="alert alert-danger print-display-none" role="alert" style="font-size: 15px;line-height: initial;">[差戻し理由]<br>{!! nl2br($report->comment) !!}</div>
                @endif
                {!! nl2br($report->description) !!}
            </div>
        </div>
        @if (isset($preview))
            <form id="form" method="POST" action="{{ route('vn.updateReport', ['report_id' => $report->request_report_id]) }}" onsubmit="buttonDisabled();" style="width:100%;" class="modal-footer mt-5 print-display-none">
                @csrf
                @method("PUT")
                <input type="hidden" name="action_date" value="{{$report->action_date}}">
                <input type="hidden" name="starting_time" value="{{substr($report->starting_time, 0, 5)}}">
                <input type="hidden" name="ending_time" value="{{substr($report->ending_time, 0, 5)}}">
                <input type="hidden" name="construction_amount" value="{{$report->construction_amount}}">
                <input type="hidden" name="description" value="{{$report->description}}">
                <input type="hidden" name="action_section" value="{{$report->action_section}}">
                <input type="hidden" name="action_status" value="{{$report->action_status}}">
                @if (isset($delete_images))
                    <input type="hidden" name="delete_images" value="{{$delete_images}}">
                @endif
                @if (isset($select_images))
                    @foreach ($select_images as $val)
                        <input type="hidden" name="select_images[]" value="{{$val}}">
                    @endforeach
                @endif
                <button type="button" id="btn-prev" class="btn btn-slate mr-auto">戻る</button>
                <button type="button" id="btn-save" class="btn btn-slate">保存する</button>
                <button type="button" class="btn btn-green" data-toggle="modal" data-target="#submit-modal">提出する</button>
            </form>
        @else
            <div class="modal-footer customModalFooter print-display-none">
                <button type="button" class="btn btn-slate mr-auto" onclick="window.parent.closeModal();">閉じる</button>
                @if ($report->report_status === config('const.Reports.Status.DRAFT') || $report->report_status === config('const.Reports.Status.DECLINED'))
                    <a href="{{ route('vn.reportEdit', ['report_id' => $report->request_report_id]) }}" onclick="buttonDisabled();"><button type="button" class="btn btn-slate">編集する</button></a>
                    <button type="button" class="btn btn-green" data-toggle="modal" data-target="#submit-modal">提出する</button>
                @endif
            </div>
        @endif
    </div>

<!-- 提出Modal -->
<div class="modal fade" id="submit-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title submitModalTitle w-100 text-center">報告書提出</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body submitModalBody">登録内容を保存して、報告書を提出しますか？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-slate px-3 mr-3" data-dismiss="modal">キャンセル</button>
                <form method="POST" action="{{ route('vn.updateReport', ['report_id' => $report->request_report_id]) }}" onsubmit="buttonDisabled();">
                    @csrf
                    @method("PUT")
                    <input type="hidden" name="submit" value="true">
                    <input type="hidden" name="action_date" value="{{$report->action_date}}">
                    <input type="hidden" name="starting_time" value="{{substr($report->starting_time, 0, 5)}}">
                    <input type="hidden" name="ending_time" value="{{substr($report->ending_time, 0, 5)}}">
                    <input type="hidden" name="construction_amount" value="{{$report->construction_amount}}">
                    <input type="hidden" name="description" value="{{$report->description}}">
                    <input type="hidden" name="action_section" value="{{$report->action_section}}">
                    <input type="hidden" name="action_status" value="{{$report->action_status}}">
                    @if (isset($delete_images))
                        <input type="hidden" name="delete_images" value="{{$delete_images}}">
                    @endif
                    @if (isset($select_images))
                        @foreach ($select_images as $val)
                            <input type="hidden" name="select_images[]" value="{{$val}}">
                        @endforeach
                    @endif
                    <button type="submit" class="btn btn-green px-4">提出する</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- lightbox -->
<div class="modal fade and carousel slide" id="lightbox">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width:1150px;height:70%;background-color:transparent;">
        <div class="modal-content mx-auto" style="border:0;width:100%;height:683px;background-color:#000;">
            <div class="modal-header" style="border:transparent;">
                <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span id="carouselControls-image-number">{{'1/'.count($report->image_paths)}}</span>）</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff;">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <div id="carouselControls" class="carousel slide h-100" data-ride="carousel" data-interval="0" total-count="{{count($report->image_paths)}}">
                    <div class="carousel-inner m-auto h-100" style="width:60%;">
                        @foreach ($report->image_paths as $idx => $img)
                            <div class="carousel-item h-100 text-center @if ($idx === 0) active @endif">
                                <img class="carousel-img" src="{{ $img }}">
                            </div>
                        @endforeach
                    </div>
                    @if (count($report->image_paths) > 1)
                        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        </a>
                        <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
@if (isset($preview))
    var backHidden = '<input type="hidden" name="back" value="true">';
    $('#btn-save').on('click', function () {
        $('#form').submit();
    });
    $('#btn-prev').on('click', function () {
        $('#form').append(backHidden);
        $('#form').submit();
    });
@endif
$(document).ready(function () {
    $(document).on('slid.bs.carousel', function(e) {
        var nextPage = e.to + 1;
        var totalCnt = $(e.target).attr('total-count');
        var targetId = $(e.target).attr('id') + '-image-number';
        $('#' + targetId).html(nextPage + '/' + totalCnt);
    });
    $(document).on('show.bs.modal', function(e) {
        window.parent.dispOverlay();
    });
    $(document).on('hide.bs.modal', function(e) {
        window.parent.removeOverlay();
    });
});
function closeModal() {
    $('.modal').each(function (i, obj) {
        $(obj).modal('hide');
    });
}
</script>
</body>
@endif
</html>