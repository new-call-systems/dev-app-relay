<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>reportEdit</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">
</style>
</head>
@if (!$report)
<body>編集できない完了報告書です。</body>
@elseif (session('is_update'))
<script>
$(function() {
    window.open("{{ route('vn.reportList') }}?s=updated&section={{config('const.Reports.DispSection.NOT_SUBMITTED')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@else
<body class="pt-5" style="background-color:#fff;">
    <div class="container-fluid mt-n3 px-0">
        <form id="form" method="POST" action="{{ route('vn.reportEdit', ['report_id' => $report->request_report_id]) }}" onsubmit="buttonDisabled();" enctype="multipart/form-data">
            @csrf
            <div class="reportDetailTitle">完了報告書編集</div>
            <div class="row justify-content-center mt-3 px-5">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="reportEditLabel" for="reception_date">受付日</label>
                        <input type="date" class="form-control @error('reception_date') is-invalid @enderror" id="reception_date" name="reception_date" value="{{ old('reception_date', $report->reception_date) }}" disabled>
                        @error('reception_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="management_company_name">管理会社名</label>
                        <input type="text" class="form-control @error('management_company_name') is-invalid @enderror" id="management_company_name" name="management_company_name" value="{{ old('management_company_name', $report->management_company_name) }}" disabled>
                        @error('management_company_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="property_name">物件名</label>
                        <input type="text" class="form-control @error('property_name') is-invalid @enderror" id="property_name" name="property_name" value="{{ old('property_name', $report->property_name) }}" disabled>
                        @error('property_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="room_no">部屋番号</label>
                        <input type="text" class="form-control @error('room_no') is-invalid @enderror" id="room_no" name="room_no" value="{{ old('room_no', $report->room_no) }}" disabled>
                        @error('room_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="resident_name">お客様名</label>
                        <input type="text" class="form-control @error('resident_name') is-invalid @enderror" id="resident_name" name="resident_name" value="{{ old('resident_name', $report->resident_name) }}" disabled>
                        @error('resident_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel reportEditRequired" for="action_date">工事日</label>
                        <input type="date" class="form-control @error('action_date') is-invalid @enderror" id="action_date" name="action_date" value="{{ old('action_date', $report->action_date) }}">
                        @error('action_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="starting_time">対応時間</label>
                        <div class="form-inline">
                            <input type="time" class="form-control @error('starting_time') is-invalid @enderror mr-3" id="starting_time" name="starting_time" value="{{ old('starting_time', substr($report->starting_time, 0, 5)) }}" style="width:175px;">
                            -
                            <input type="time" class="form-control @error('ending_time') is-invalid @enderror ml-3" id="ending_time" name="ending_time" value="{{ old('ending_time', substr($report->ending_time, 0, 5)) }}" style="width:175px;">
                        </div>
                        @error('starting_time')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @error('ending_time')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="construction_amount">工事金額</label>
                        <div class="amount-form">
                            <input type="text" class="form-control @error('construction_amount') is-invalid @enderror" id="construction_amount" name="construction_amount" value="{{ old('construction_amount', $report->construction_amount) }}">
                        </div>
                        @error('construction_amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="reportEditLabel reportEditRequired" for="reception_date">作業内容</label>
                        <textarea style="height:116px;resize:none;" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="作業した内容を記載してください。">{{ old('description', $report->description) }}</textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="user_name">作業担当者名</label>
                        <input type="text" class="form-control @error('user_name') is-invalid @enderror" id="user_name" name="user_name" value="{{ old('user_name', $report->user_name) }}" disabled>
                        @error('user_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="action_section">対応区分</label>
                        <div class="form-group" style="margin-top:6px;margin-bottom:5px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @error('action_section') is-invalid @enderror" type="radio" value="1" name="action_section" id="action_section_1" @if (old('action_section', $report->action_section) === '1') checked @endif>
                                <label class="form-check-label" for="action_section_1">一次対応</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @error('action_section') is-invalid @enderror" type="radio" value="2" name="action_section" id="action_section_2" @if (old('action_section', $report->action_section) === '2') checked @endif>
                                <label class="form-check-label" for="action_section_2">二次対応</label>
                            </div>
                        </div>
                        @error('action_section')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="action_status">ステータス</label>
                        <div class="form-group" style="margin-top:6px;margin-bottom:5px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @error('action_status') is-invalid @enderror" type="radio" value="{{config('const.Reports.ActionStatus.INCOMPLETE')}}" name="action_status" id="action_status_1" @if (old('action_status', $report->action_status) === config('const.Reports.ActionStatus.INCOMPLETE')) checked @endif>
                                <label class="form-check-label" for="action_status_1">未完了引継ぎ</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @error('action_status') is-invalid @enderror" type="radio" value="{{config('const.Reports.ActionStatus.COMPLETED')}}" name="action_status" id="action_status_2" @if (old('action_status', $report->action_status) === config('const.Reports.ActionStatus.COMPLETED')) checked @endif>
                                <label class="form-check-label" for="action_status_2">工事完了</label>
                            </div>
                        </div>
                        @error('action_status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="reportEditLabel" for="">現場画像</label>
                        <div class="reportEditImageDiv">
                            <input type="hidden" id="delete_images" name="delete_images" value="{{ old('delete_images', $delete_images) }}">
                            <input type="file" id="photoImage" name="photoImage" style="display:none;" accept="image/*" multiple>
                            <label for="photoImage" style="cursor:pointer;" class="mr-3"><img src="{{ asset('img/btn-pic-l.svg') }}"></label>
                            @foreach ($report->image_paths as $img)
                                <div class="reportEditSelectImage s3" fileName="{{Cmn::basename($img)}}" style="background-image:url({{ $img }});">
                                    <img onclick="fileDelete(this);" class="btn-delete" src="{{ asset('img/btn-photo-delete.svg') }}">
                                </div>
                            @endforeach
                            @foreach ($select_images as $img)
                                <div class="reportEditSelectImage" style="background-image:url({{ $img }});">
                                    <input type="hidden" name="select_images[]" value="{{ $img }}">
                                    <img onclick="fileDelete(this);" class="btn-delete" src="{{ asset('img/btn-photo-delete.svg') }}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer customModalFooter">
                <button type="button" class="btn btn-slate mr-auto" onclick="window.parent.closeModal();">閉じる</button>
                <button type="button" class="btn btn-slate" id="btn-save">保存する</button>
                <button type="button" class="btn btn-green" id="btn-preview">プレビュー</button>
            </div>
        </form>
    </div>

<script>
var putform = '@method("PUT")';
var updateAction = "{{ route('vn.updateReport', ['report_id' => $report->request_report_id]) }}";
var previewAction = "{{ route('vn.reportPreview', ['report_id' => $report->request_report_id]) }}";
$('#btn-save').on('click', function () {
    if (!checkFileSize()) {
        return;
    }
    $('#form').attr('action', updateAction);
    $('#form').append(putform);
    $('#form').submit();
});
$('#btn-preview').on('click', function () {
    if (!checkFileSize()) {
        return;
    }
    $('#form').attr('action', previewAction);
    // $('#form').attr('method', 'GET');
    $('#form').submit();
});
$('#photoImage').on('change', function (e) {
    try {
        var existNotImageFile = false;
        $(e.target.files).each(function (idx, obj) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var hidden = '<input type="hidden" name="select_images[]" value="' + e.target.result + '">';
                var imgTag = '<div class="reportEditSelectImage" style="background-image:url(' + e.target.result + ');"><img class="btn-delete" onclick="fileDelete(this);" src="{{ asset('img/btn-photo-delete.svg') }}">' + hidden + '</div>';
                $(".reportEditImageDiv").append(imgTag);
            }
            if (isCorrectExtension(obj.name)) {
                reader.readAsDataURL(obj);
            } else {
                existNotImageFile = true;
            }
        })
        if (existNotImageFile) {
            alert('画像ファイル以外は選択できません。');
        }

    } finally {
        this.value = '';
    }
});
function fileDelete(obj) {
    var parent = $(obj).parent();
    if ($(parent).hasClass('s3')) {
        var val = $('#delete_images').val();
        if (val == '') {
            val = $(parent).attr('fileName');
        } else {
            val = val + ',' + $(parent).attr('fileName');
        }
        $('#delete_images').val(val);
    }
    $(parent).remove();
}
function isCorrectExtension(val) {
    var format = new RegExp('([^\s]+(\\.(jpg|jpeg|png|gif|bmp))$)', 'i');
    return format.test(val);
}
function checkFileSize() {
    var totalFileSize = 0;
    var selectImages = $('[name="select_images[]"]');
    $(selectImages).each(function(index, element) {
        file = base64ToFile($(element).val());
        totalFileSize += file.size;
    });
    if (totalFileSize >= ({{ $postMaxSize }} * 0.75)) {
        alert('一度にアップロードできるファイルサイズを超えています。\nファイルを削除してやり直してください。');
        return false;
    }
    return true;
}
function base64ToFile(data) {
  try{
    let separetedDate = data.split(',');
    let mimeTypeData = separetedDate[0].match(/:(.*?);/);
    let mimeType = Array.isArray(mimeTypeData) ? mimeTypeData[0] : '';
    let decodedData = atob(separetedDate[1]);
    let dataLength = decodedData.length;
    let arrayBuffer = new ArrayBuffer(dataLength);
    let u8arr = new Uint8Array(arrayBuffer);
    for( let i = 0; i < dataLength; i +=1){
      u8arr[i] = decodedData.charCodeAt(i);
    }
    return new Blob([u8arr] , {type:mimeType});

  }catch (errors){
    console.log(errors);
    return new Blob([])
  }
}
</script>
</body>
@endif
</html>