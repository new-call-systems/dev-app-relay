@extends('layouts.vn.app')

@section('content')
@if(session('s'))
<div class="container-fluid" style="margin: 20px 0 -80px 0;padding: 0 59px;">
    <div class="row justify-content-center">
        <div class="alert alert-success col-md-12">{{session('s')}}</div>
    </div>
</div>
@endif
<ul class="nav nav-tabs pl-4" style="height:50px;"></ul>
<div class="container-fluid px-5 mt-n3">
    <form method="GET" action="{{ route('vn.bidHistory') }}" onsubmit="buttonDisabled();">
        <div class="row justify-content-center mt-4" id="searchConditions">
            <div class="col-md-4"><h6 class="searchCondLabel">問合せ日</h6></div>
            <div class="col-md-4"><h6 class="searchCondLabel">キーワード</h6></div>
            <div class="col-md-4"></div>
            <div class="col-md-2"><input type="date" name="date_from" class="form-control" value="{{ $conditions['date_from'] }}"></div>
            <div class="col-md-2"><input type="date" name="date_to" class="form-control" value="{{ $conditions['date_to'] }}"></div>
            <div class="col-md-3 search-form"><input type="text" name="keyword" class="form-control" value="{{ $conditions['keyword'] }}"></div>
            <div class="col-md-5">
                <button type="button" class="btn btn-clear mr-3">検索条件のクリア</button>
                <button type="submit" class="btn btn-search" style="width:160px;">検索</button>
            </div>
        </div>
    </form>
    <div class="row justify-content-center mt-4">
        <table class="table request-list">
            <tr>
                <th style="width:13%;">問合せ日時</th>
                <th style="width:12%;">対応状況</th>
                <th style="width:12%;">希望日時</th>
                <th style="width:20%;">建物名</th>
                <th style="width:8%;">部屋番号</th>
                <th style="width:11%;">入居者名</th>
                <th style="width:24%;">問合せ内容</th>
            </tr>
            @if ($requests->count() === 0)
                <tr><td colspan="7" class="text-center" style="cursor:default;font-size:18px;">検索結果がありませんでした</td></tr>
            @endif
            @foreach ($requests as $idx => $req)
                <tr data-toggle="modal" data-target="#modal-{{ $idx }}">
                    <td title="{{ Cmn::dateFormat($req->created_at) }}">{{ Cmn::dateFormat($req->created_at) }}</td>
                    <td title="{{ $req->status }}">{{ $req->status }}</td>
                    <td title="{{ Cmn::dateFormat($req->preferable_time) }}">{{ Cmn::dateFormat($req->preferable_time) }}</td>
                    <td title="{{ $req->property_name }}">{{ $req->property_name }}</td>
                    <td title="{{ $req->room_no }}">{{ $req->room_no }}</td>
                    <td title="{{ $req->resident_name }}">{{ $req->resident_name }}</td>
                    <td title="{{ $req->context }}">{{ $req->context }}</td>
                </tr>
            @endforeach
        </table>
    </div>

    @foreach ($requests as $idx => $req)
        <div class="modal fade" id="modal-{{ $idx }}" tabindex="-1" >
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="container">
                        <div class="justify-content-center">
                            <div class="col-md-12 mt-5 mb-5 text-center">
                                <h5 class="modal-title">問合せ 入札履歴詳細</h5>
                            </div>
                            <table class="table request-info-table border-bottom">
                                <tr>
                                    <th style="width:13%;">問合せ日時</th>
                                    <td style="width:37%;">{{ Cmn::dateFormat($req->created_at) }}</td>
                                    <td style="width:50%;" rowspan="8">
                                        <span class="image-movie-title">現場画像・映像</span>
                                        <div class="@if(count($req->image_paths) > 1) request-info-img-multiple @else request-info-img @endif">
                                            <div class="position-relative d-inline-block">
                                                @if ($req->image_paths)
                                                    <a href="#lightbox-{{ $idx }}" data-toggle="modal">
                                                        @if ($req->image_paths[0]->type === 'video')
                                                            <video style="max-height:340px;max-width:100%;display:block;" src="{{ $req->image_paths[0]->url }}"></video>
                                                        @else
                                                            <img style="max-height:340px;max-width:100%;" src="{{ $req->image_paths[0]->url }}">
                                                        @endif
                                                    </a>
                                                @else
                                                    <img style="height: 340px;" src="{{ asset('img/no-image-l.svg') }}">
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="align-middle" rowspan="2">入居者情報</th><td>{{ $req->resident_name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->resident_tel }}</td>
                                </tr>
                                <tr>
                                    <th class="align-middle" rowspan="3">物件情報</th><td>{{ $req->property_name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->property_address }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->room_no }}</td>
                                </tr>
                                <tr>
                                    <th>対応状況</th><td>{{ $req->status }}</td>
                                </tr>
                                <tr>
                                    <th>問合せ内容</th><td>{!! nl2br($req->context) !!}</td>
                                </tr>
                                <tr>
                                    <th>希望日時</th>
                                    <td colspan="2">
                                        @if ($req->emergency_flg)
                                            緊急
                                        @else
                                            <table class="table request-info-sub-table border-bottom">
                                                <tr>
                                                    <th>希望順序</th>
                                                    <th>日付</th>
                                                    <th>時間区分</th>
                                                    <th>業者選択</th>
                                                </tr>
                                                @foreach ($req->preferable_times as $i => $rpt)
                                                    <tr>
                                                        <td>第{{ $i + 1 }}希望</td>
                                                        <td>{{ Cmn::dateFormat($rpt->date) }}</td>
                                                        <td>{{ $rpt->label }}</td>
                                                        <td>
                                                            @if ($rpt->selected)
                                                                ○
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        作業者情報
                                        <button data-toggle="modal" data-target="#request-handler-modal-{{ $idx }}" class="btn btn-search mt-2">作業者変更</button>
                                    </th>
                                    <td colspan="2">
                                        @isset($req->vendor_members)
                                            <table class="table request-info-sub-table border-bottom">
                                                <tr>
                                                    <th>作業者名</th>
                                                    <th>作業者連絡先</th>
                                                </tr>
                                                @foreach ($req->vendor_members as $vm)
                                                    @if ($vm->admin_flg)
                                                        <tr>
                                                            <td>{{ $vm->last_name.$vm->first_name }}</td>
                                                            <td>{{ $vm->emergency_tel }}</td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        @else
                                            ----
                                        @endisset
                                    </td>
                                </tr>
                            </table>
                            <button type="button" class="btn btn-slate mb-4" data-dismiss="modal">閉じる</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- lightbox -->
        <div class="modal fade and carousel slide" id="lightbox-{{ $idx }}">
            <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width:1150px;height:70%;background-color:transparent;">
                <div class="modal-content mx-auto" style="border:0;width:100%;height:683px;background-color:#000;">
                    <div class="modal-header" style="border:transparent;">
                        <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span id="carouselControls-{{ $idx }}-image-number">{{'1/'.count($req->image_paths)}}</span>）</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:#fff;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                        <div id="carouselControls-{{ $idx }}" class="carousel slide h-100" data-ride="carousel" data-interval="0" total-count="{{count($req->image_paths)}}">
                            <div class="carousel-inner m-auto h-100" style="width:60%;">
                                @foreach ($req->image_paths as $i => $img)
                                    <div class="carousel-item h-100 text-center @if ($i === 0) active @endif">
                                        @if ($img->type === 'video')
                                            <video class="carousel-img" src="{{ $img->url }}" controls></video>
                                        @else
                                            <img class="carousel-img" src="{{ $img->url }}">
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            @if (count($req->image_paths) > 1)
                                <a class="carousel-control-prev" href="#carouselControls-{{ $idx }}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControls-{{ $idx }}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- request handler select modal -->
        <div class="modal fade" id="request-handler-modal-{{ $idx }}" tabindex="-1" >
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="container px-0">
                        <form method="POST" action="{{ route('vn.changeRequestHandler', ['request_id' => $req->request_id]) }}" onsubmit="buttonDisabled();">
                            @csrf
                            <div class="justify-content-center">
                                <div class="col-md-12 mt-5 mb-5 text-center">
                                    <h5 class="modal-title">作業担当者変更</h5>
                                </div>
                                <div class="row mx-3" style="font-size:20px;max-height:400px;overflow-y:auto;">
                                    @foreach ($req->vendor_members as $vmIdx => $vm)
                                        <div class="col-md-4">
                                            <input id="vendor-member-{{$idx}}-{{$vmIdx}}" name="vendor_members[]" type="checkbox" value="{{$vm->user_id}}" @if ($vm->admin_flg) checked @endif />
                                            <label for="vendor-member-{{$idx}}-{{$vmIdx}}">
                                                {{ $vm->last_name.$vm->first_name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer mt-4">
                                    <button type="button" class="btn btn-slate mr-auto" data-dismiss="modal">閉じる</button>
                                    <button type="submit" class="btn btn-green" id="btn-save">保存する</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row justify-content-center">
        {{ $requests->appends($conditions)->links() }}
    </div>
</div>

<script>
$('.btn-clear').on('click', function () {
    $('#searchConditions').find('input').val('');
    $('#searchConditions').find('option').attr("selected", false);
    $('#searchConditions').find('select').val('');
});
$('[id^=lightbox-]').on('hidden.bs.modal', function () {
    $(this).find('video').each(function(i, obj) {
        $(obj)[0].pause();
    })
    $('body').addClass('modal-open');
});
$('[id^=request-handler-modal-]').on('hidden.bs.modal', function () {
    $('body').addClass('modal-open');
});
$('[class^=carousel-control-]').on('click', function () {
    $(this).parent().find('video').each(function(i, obj) {
        $(obj)[0].pause();
    })
});
$(document).ready(function () {
    $(document).on('show.bs.modal', '.modal', e => {
        const $currentModal = $(e.currentTarget);
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $currentModal.css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop')
            .not('.modal-stack')
            .css('z-index', zIndex - 1)
            .addClass('modal-stack');
        }, 0);
    });
    $(document).on('slid.bs.carousel', function(e) {
        var nextPage = e.to + 1;
        var totalCnt = $(e.target).attr('total-count');
        var targetId = $(e.target).attr('id') + '-image-number';
        $('#' + targetId).html(nextPage + '/' + totalCnt);
    });
});
</script>

@endsection
