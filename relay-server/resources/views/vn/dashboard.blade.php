@extends('layouts.vn.app')

@section('content')
<div class="container-fluid mt-5">
    <div class="row justify-content-center">
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>管理会社別対応件数</h6>
                <canvas id="chart-by-mc"></canvas>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>時間帯別問合せ数</h6>
                <canvas id="chart-by-time-zone"></canvas>
            </div>
        </div>
    </div>
</div>
<script>
var timeLabels = new Array();
for (i = 0; i < 24; i++) {
    timeLabels.push(i + '時');
}


var cbmc = document.getElementById("chart-by-mc").getContext('2d');
var chartByMc = new Chart(cbmc, {
    type: 'bar',
    data: {
        labels: [
            @foreach ($chartByMc as $cbmc)
                '{{ $cbmc->name }}',
            @endforeach
        ],
        datasets: [{
            label: '対応数',
            data: [
                @foreach ($chartByMc as $cbmc)
                    {{ $cbmc->cnt.',' }}
                @endforeach
            ],
            backgroundColor: 'rgba(101, 137, 229, 0.5)',
            borderColor: 'rgba(101, 137, 229, 1)',
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        // when the floored value is the same as the value we have a whole number
                        if (Math.floor(label) === label) {
                            return label;
                        }
                    },
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
            }]
        }
    }
});

var cbtz = document.getElementById("chart-by-time-zone").getContext('2d');
var chartByTimeZone = new Chart(cbtz, {
    type: 'bar',
    data: {
        labels: timeLabels,
        datasets: [{
            label: '問合せ数',
            data: [
                @foreach ($chartByTimeZone as $val)
                    {{ $val.',' }}
                @endforeach
            ],
            backgroundColor: 'rgba(101, 137, 229, 0.5)',
            borderColor: 'rgba(101, 137, 229, 1)',
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        // when the floored value is the same as the value we have a whole number
                        if (Math.floor(label) === label) {
                            return label;
                        }
                    },
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
            }]
        }
    }
});
</script>
@endsection
