@extends('layouts.mc.app')

@section('content')
<ul class="nav nav-tabs pl-4" style="height:50px;"></ul>
<div class="container-fluid px-5 mt-n3">
    <form method="GET" action="{{ route('mc.request') }}" onsubmit="buttonDisabled();">
        <div class="row justify-content-center mt-4" id="searchConditions">
            <div class="col-md-12"><h6 class="searchCondLabel">対応状況</h6></div>
            <div class="col-md-11">
                @php
                    $request_statuses = [
                        'open' => '業者入札待ち' ,
                        'bid' => '業者入札済み' ,
                        'scheduling' => '業者対応予定' ,
                        'in_progress' => '業者対応中' ,
                        'completed' => '業者対応完了' ,
                        'waiting_for_cancel' => '入居者キャンセル待ち' ,
                        'cancelled' => 'キャンセル' ,
                        'transferred' => '電話番号案内済み' ,
                        'auto_completed' => '自動回答済み' ,
                    ];
                @endphp
                <div class="row search-condition-checkbox-items">
                    @foreach ($request_statuses as $key => $val)
                        <div class="col-md-2">
                            {{ Form::checkbox('request_statuses[]', $key, in_array($key, $conditions['request_statuses'], true), ['id' => 'request-status-'.$key]) }}
                            <label for="{{ 'request-status-'.$key }}">{{ $val }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4"><h6 class="searchCondLabel">問合せ日</h6></div>
            <div class="col-md-4"><h6 class="searchCondLabel">問合せ種類</h6></div>
            <div class="col-md-4"></div>
            <div class="col-md-2"><input type="date" name="date_from" class="form-control" value="{{ $conditions['date_from'] }}"></div>
            <div class="col-md-2"><input type="date" name="date_to" class="form-control" value="{{ $conditions['date_to'] }}"></div>
            <div class="col-md-3">
                @php
                    $request_types = [
                        '' => '' ,
                        'in_house' => '管理会社対応' ,
                        'vendor' => '業者対応' ,
                        'auto_response' => 'AI自動回答' ,
                    ];
                @endphp
                {{ Form::select('request_type', $request_types, $conditions['request_type'], ['class' => 'form-control']) }}
            </div>
            <div class="col-md-5">
                <button type="button" class="btn btn-clear mr-3">検索条件のクリア</button>
                <button type="submit" class="btn btn-search" style="width:160px;">検索</button>
            </div>

        </div>
    </form>
    <div class="row justify-content-center mt-4">
        <table class="table request-list">
            <tr>
                <th style="width:13%;">問合せ日時</th>
                <th style="width:12%;">対応状況</th>
                <th style="width:12%;">問合せ種類</th>
                <th style="width:20%;">建物名</th>
                <th style="width:8%;">部屋番号</th>
                <th style="width:11%;">入居者名</th>
                <th style="width:24%;">問合せ内容</th>
            </tr>
            @if ($requests->count() === 0)
                <tr><td colspan="7" class="text-center" style="cursor:default;font-size:18px;">検索結果がありませんでした</td></tr>
            @endif
            @foreach ($requests as $idx => $req)
                <tr data-toggle="modal" data-target="#modal-{{ $idx }}">
                    <td title="{{ Cmn::dateFormat($req->created_at) }}">{{ Cmn::dateFormat($req->created_at) }}</td>
                    <td title="{{ $req->status }}">{{ $req->status }}</td>
                    <td title="{{ $req->request_type }}">{{ $req->request_type }}</td>
                    <td title="{{ $req->property_name }}">{{ $req->property_name }}</td>
                    <td title="{{ $req->room_no }}">{{ $req->room_no }}</td>
                    <td title="{{ $req->resident_name }}">{{ $req->resident_name }}</td>
                    <td title="{{ $req->context }}">{{ $req->context }}</td>
                </tr>
            @endforeach
        </table>
    </div>

    @foreach ($requests as $idx => $req)
        <div class="modal fade" id="modal-{{ $idx }}" tabindex="-1" >
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="container">
                        <div class="justify-content-center">
                            <div class="col-md-12 mt-5 mb-5 text-center">
                                <h5 class="modal-title" title="{{ $req->request_id }}">問合せ詳細</h5>
                            </div>
                            <table class="table request-info-table border-bottom">
                                <tr>
                                    <th style="width:13%;">問合せ日時</th>
                                    <td style="width:37%;">{{ Cmn::dateFormat($req->created_at) }}</td>
                                    <td style="width:50%;" rowspan="8">
                                        <span class="image-movie-title">現場画像・映像</span>
                                        <div class="@if(count($req->image_paths) > 1) request-info-img-multiple @else request-info-img @endif">
                                            <div class="position-relative d-inline-block">
                                                @if ($req->image_paths)
                                                    <a href="#lightbox-{{ $idx }}" data-toggle="modal">
                                                        @if ($req->image_paths[0]->type === 'video')
                                                            <video style="max-height:340px;max-width:100%;display:block;" src="{{ $req->image_paths[0]->url }}"></video>
                                                        @else
                                                            <img style="max-height:340px;max-width:100%;" src="{{ $req->image_paths[0]->url }}">
                                                        @endif
                                                    </a>
                                                @else
                                                    <img style="height: 340px;" src="{{ asset('img/no-image-l.svg') }}">
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="align-middle" rowspan="2">入居者情報</th><td>{{ $req->resident_name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->resident_tel }}</td>
                                </tr>
                                <tr>
                                    <th class="align-middle" rowspan="3">物件情報</th><td>{{ $req->property_name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->property_address }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $req->room_no }}</td>
                                </tr>
                                <tr>
                                    <th>対応状況</th><td>{{ $req->status }}</td>
                                </tr>
                                <tr>
                                    <th>問合せ内容</th><td>{!! nl2br($req->context) !!}</td>
                                </tr>
                                <tr>
                                    <th>希望日時</th>
                                    <td colspan="2">
                                        @if ($req->emergency_flg)
                                            緊急
                                        @else
                                            <table class="table request-info-sub-table border-bottom">
                                                <tr>
                                                    <th>希望順序</th>
                                                    <th>日付</th>
                                                    <th>時間区分</th>
                                                    <th>業者選択</th>
                                                </tr>
                                                @foreach ($req->preferable_times as $i => $rpt)
                                                    <tr>
                                                        <td>第{{ $i + 1 }}希望</td>
                                                        <td>{{ Cmn::dateFormat($rpt->date) }}</td>
                                                        <td>{{ $rpt->label }}</td>
                                                        <td>
                                                            @if ($rpt->selected)
                                                                ○
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>作業者情報</th>
                                    <td colspan="2">
                                        @isset($req->handle_users)
                                            <table class="table request-info-sub-table border-bottom">
                                                <tr>
                                                    <th>作業者名</th>
                                                    <th>作業者連絡先</th>
                                                </tr>
                                                @foreach ($req->handle_users as $rhu)
                                                    <tr>
                                                        <td>{{ $rhu->vm_last_name.$rhu->vm_first_name }}</td>
                                                        <td>{{ $rhu->vm_emergency_tel }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @else
                                            ----
                                        @endisset
                                    </td>
                                </tr>
                            </table>
                            <button type="button" class="btn btn-slate mb-4" data-dismiss="modal">閉じる</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- lightbox -->
        <div class="modal fade and carousel slide" id="lightbox-{{ $idx }}">
            <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width:1150px;height:70%;background-color:transparent;">
                <div class="modal-content mx-auto" style="border:0;width:100%;height:683px;background-color:#000;">
                    <div class="modal-header" style="border:transparent;">
                        <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span id="carouselControls-{{ $idx }}-image-number">{{'1/'.count($req->image_paths)}}</span>）</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:#fff;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                        <div id="carouselControls-{{ $idx }}" class="carousel slide h-100" data-ride="carousel" data-interval="0" total-count="{{count($req->image_paths)}}">
                            <div class="carousel-inner m-auto h-100" style="width:60%;">
                                @foreach ($req->image_paths as $i => $img)
                                    <div class="carousel-item h-100 text-center @if ($i === 0) active @endif">
                                        @if ($img->type === 'video')
                                            <video class="carousel-img" src="{{ $img->url }}" controls></video>
                                        @else
                                            <img class="carousel-img" src="{{ $img->url }}">
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            @if (count($req->image_paths) > 1)
                                <a class="carousel-control-prev" href="#carouselControls-{{ $idx }}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControls-{{ $idx }}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row justify-content-center">
        {{ $requests->appends($conditions)->links() }}
    </div>


    <form method="POST" action="{{ route('mc.request.export') }}">
        @csrf
        <input type="hidden" name="requests" value="{{ json_encode($requests) }}">
        <div class="row justify-content-end">
            <button  class="btn btn-search" style="width:160px;">エクスポート</button>
        </div>
    </form>
</div>

<script>
$('.btn-clear').on('click', function () {
    $('#searchConditions').find('input').val('');
    $('#searchConditions').find('option').attr("selected", false);
    $('#searchConditions').find('select').val('');
});
$('[id^=lightbox-]').on('hidden.bs.modal', function () {
    $(this).find('video').each(function(i, obj) {
        $(obj)[0].pause();
    })
    $('body').addClass('modal-open');
});
$('[class^=carousel-control-]').on('click', function () {
    $(this).parent().find('video').each(function(i, obj) {
        $(obj)[0].pause();
    })
});
$(document).ready(function () {
    $(document).on('show.bs.modal', '.modal', e => {
        const $currentModal = $(e.currentTarget);
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $currentModal.css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop')
            .not('.modal-stack')
            .css('z-index', zIndex - 1)
            .addClass('modal-stack');
        }, 0);
    });
    $(document).on('slid.bs.carousel', function(e) {
        var nextPage = e.to + 1;
        var totalCnt = $(e.target).attr('total-count');
        var targetId = $(e.target).attr('id') + '-image-number';
        $('#' + targetId).html(nextPage + '/' + totalCnt);
    });
});

@if ($request_id && $requests->count() !== 0)
    $('.request-list').find('tr').click();
@endif

</script>

@endsection
