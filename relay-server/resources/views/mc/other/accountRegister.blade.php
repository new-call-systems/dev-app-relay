<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>Coco-en アカウント本登録</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">
</style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                @if (isset($s))
                    <div class="row justify-content-center">
                        <div class="col-md-12 pl-0 mt-5 mr-4 mb-3 text-center">
                            <h3 class="system-title">Coco-en 会員登録が完了しました</h3>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 py-5 px-3 text-center" style="background-color:#fff;">
                            アカウントの登録が完了いたしました。<br><br>
                            {{$email}}宛にお送りしました登録内容で、<br>
                            スマホアプリ『Coco-en』よりサービスにログインいただけます。
                        </div>
                    </div>
                @else
                    <div class="row justify-content-center">
                        <div class="col-md-12 pl-0 mt-5 mr-4 mb-3 text-center">
                            <h3 class="system-title">Coco-en 会員登録 本登録</h3>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-5 py-5 px-5" style="background-color:#fff;">
                            <form method="POST" action="{{ route('other.account.register') }}" onsubmit="buttonDisabled();">
                                @csrf
                                <input type="hidden" name="code" value="{{$code}}">

                                <div class="form-group row">
                                    <label for="email" class="col-md-12 col-form-label">メールアドレス</label>
                                    <div class="col-md-12">
                                        <input id="email" type="text" class="form-control" name="email" value="{{ $email }}" disabled>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-12 col-form-label">パスワード</label>
                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autofocus>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password_confirmation" class="col-md-12 col-form-label">パスワード(確認用)</label>
                                    <div class="col-md-12">
                                        <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required>
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mt-4 mb-0">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-search px-4">登録する</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</body>
</html>