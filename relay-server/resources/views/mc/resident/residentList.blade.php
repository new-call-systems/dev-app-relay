@extends('layouts.mc.app')

@section('content')
<ul class="nav nav-tabs pl-4" style="height:50px;"></ul>
<div class="container-fluid px-5 mt-n3">
    <form method="GET" action="{{ route('mc.residentList') }}" onsubmit="buttonDisabled();">
        <div class="row justify-content-center mt-4" id="searchConditions">
            <div class="col-md-4"><h6 class="searchCondLabel">物件名</h6></div>
            <div class="col-md-4"><h6 class="searchCondLabel">キーワード</h6></div>
            <div class="col-md-4"></div>
            <div class="col-md-4 search-form"><input type="text" name="property_name" class="form-control" value="{{ $conditions['property_name'] }}"></div>
            <div class="col-md-4 search-form"><input type="text" name="keyword" class="form-control" value="{{ $conditions['keyword'] }}"></div>
            <div class="col-md-4">
                <button type="button" class="btn btn-clear mr-3">検索条件のクリア</button>
                <button type="submit" class="btn btn-search" style="width:160px;">検索</button>
            </div>
        </div>
    </form>
    <div class="row justify-content-center mt-4">
        <table class="table resident-list">
            <tr>
                <th style="width:30%;">物件名</th>
                <th style="width:21%;">物件住所</th>
                <th style="width:10%;">部屋番号</th>
                <th style="width:13%;">入居者名</th>
                <th style="width:13%;">入居者連絡先</th>
                <th style="width:13%;">メールアドレス</th>
            </tr>
            @if ($residents->count() === 0)
                <tr><td colspan="6" class="text-center" style="cursor:default;font-size:18px;">検索結果がありませんでした</td></tr>
            @endif
            @foreach ($residents as $idx => $r)
                <tr>
                    <td title="{{ $r->property_name }}">{{ $r->property_name }}</td>
                    <td title="{{ $r->property_address }}">{{ $r->property_address }}</td>
                    <td title="{{ $r->room_no }}">{{ $r->room_no }}</td>
                    <td title="{{ $r->resident_name }}">{{ $r->resident_name }}</td>
                    <td title="{{ $r->resident_tel }}">{{ $r->resident_tel }}</td>
                    <td title="{{ $r->resident_email }}">{{ $r->resident_email }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="row justify-content-center">
        {{ $residents->appends(request()->query())->links() }}
    </div>
</div>

<script>
$('.btn-clear').on('click', function () {
    $('#searchConditions').find('input').val('');
    $('#searchConditions').find('option').attr("selected", false);
    $('#searchConditions').find('select').val('');
});
</script>
@endsection
