@extends('layouts.mc.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pl-0 mt-5 mr-4 mb-3 text-center">
                <h3 class="system-title">管理ログイン</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 py-5 px-5" style="background-color:#fff;">
                <form method="POST" action="{{ route('mc.login') }}" onsubmit="buttonDisabled();">
                    @csrf

                    <div class="form-group row">
                        <label for="code" class="col-md-12 col-form-label">メールアドレス</label>

                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-12 col-form-label">パスワード</label>

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mt-4 mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-search px-4">
                                ログイン
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12 mt-4 text-center">
                <a class="nav-link" href="{{ route('password_reset.email.form')  }}">
                    パスワードを忘れた方
                </a>
            </div>
        </div>
    </div>
@endsection
