@extends('layouts.mc.app')

@section('content')
<div class="container-fluid mt-5">
    @if ($errors->any())
        <div class="row justify-content-center">
            <div class="col-md-8 alert alert-danger" id="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>過去1ヶ月の時間帯別問合せ数</h6>
                <canvas id="chart-by-time-zone"></canvas>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>過去1ヶ月の曜日別問合せ数</h6>
                <canvas id="chart-by-week"></canvas>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>業者別 対応中件数</h6>
                <canvas id="chart-by-vendor"></canvas>
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="col-md-12 dashboard-content">
                <h6>問合せ統計</h6>
                <table class="table dashboard-request-list">
                    <tr><th>未入札</th><td>{{ number_format($count->not_bid_count) }} 件</td></tr>
                    <tr><th>対応中</th><td>{{ number_format($count->in_progress_count) }} 件</td></tr>
                    <tr><th>対応済</th><td>{{ number_format($count->completed_count) }} 件</td></tr>
                    <tr><th>12時間以上入札のない問合せ</th><td>{{ number_format($past12HourCount) }} 件</td></tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="col-md-12 dashboard-content">
                <h6>AccessKey情報</h6>
                <div class="col-md-12 text-center">
                    <form method="POST" action="{{ route('mc.getAccessKey') }}">
                        @csrf
                        <button type="submit" class="btn btn-search px-4">AccessKey情報ダウンロード</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var timeLabels = new Array();
for (i = 0; i < 24; i++) {
    timeLabels.push(i + '時');
}
var cbtz = document.getElementById("chart-by-time-zone").getContext('2d');
var chartByTimeZone = new Chart(cbtz, {
    type: 'bar',
    data: {
        labels: timeLabels,
        datasets: [{
            label: '問合せ数',
            data: [
                @foreach ($chartByTimeZone as $val)
                    {{ $val.',' }}
                @endforeach
            ],
            backgroundColor: 'rgba(101, 137, 229, 0.5)',
            borderColor: 'rgba(101, 137, 229, 1)',
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        // when the floored value is the same as the value we have a whole number
                        if (Math.floor(label) === label) {
                            return label;
                        }
                    },
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
            }]
        }
    }
});

var cbw = document.getElementById("chart-by-week").getContext('2d');
var chartByTimeZone = new Chart(cbw, {
    type: 'bar',
    data: {
        labels: ['日','月','火','水','木','金','土'],
        datasets: [{
            label: '問合せ数',
            data: [
                @foreach ($chartByWeek as $val)
                    {{ $val.',' }}
                @endforeach
            ],
            backgroundColor: 'rgba(101, 137, 229, 0.5)',
            borderColor: 'rgba(101, 137, 229, 1)',
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        // when the floored value is the same as the value we have a whole number
                        if (Math.floor(label) === label) {
                            return label;
                        }
                    },
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
            }]
        }
    }
});

var cbv = document.getElementById("chart-by-vendor").getContext('2d');
var chartByVendor = new Chart(cbv, {
    type: 'bar',
    data: {
        labels: [
            @foreach ($chartByVendor as $cbv)
                '{{ $cbv->name }}',
            @endforeach
        ],
        datasets: [{
            label: '対応中件数',
            data: [
                @foreach ($chartByVendor as $cbv)
                    {{ $cbv->cnt.',' }}
                @endforeach
            ],
            backgroundColor: 'rgba(101, 137, 229, 0.5)',
            borderColor: 'rgba(101, 137, 229, 1)',
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                        // when the floored value is the same as the value we have a whole number
                        if (Math.floor(label) === label) {
                            return label;
                        }
                    },
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
            }]
        }
    }
});
</script>
@endsection
