@extends('layouts.mc.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4 mb-4">
            <div id="cmn-list-store" class="cmn-list cmn-list-add text-center align-middle">
                ユーザー登録
            </div>
        </div>
        @foreach ($managementCompanyMembers as $mcm)
            <div class="col-md-4  mb-4">
                <div id="cmn-list-{{$mcm->user_id}}" class="cmn-list">
                    <div class="cmn-list-created-at mb-2">登録日&nbsp;{{ Cmn::dateFormat($mcm->created_at) }}</div>
                    <h6 class="cmn-list-title">{{ $mcm->last_name }}&nbsp{{ $mcm->first_name }}</h6>
                    <div class="cmn-list-other-info">部署：{{ $mcm->department }}</div>
                    <div class="cmn-list-other-info">メールアドレス：{{ $mcm->email }}</div>
                    <div class="cmn-list-other-info">電話番号：{{ $mcm->emergency_tel }}</div>
                    <input type="hidden" name="user_id" value="{{$mcm->user_id}}">
                    <input type="hidden" name="last_name" value="{{$mcm->last_name}}">
                    <input type="hidden" name="first_name" value="{{$mcm->first_name}}">
                    <input type="hidden" name="email" value="{{$mcm->email}}">
                    <input type="hidden" name="emergency_tel" value="{{$mcm->emergency_tel}}">
                    <input type="hidden" name="representative_flg" value="{{$mcm->representative_flg}}">
                    <input type="hidden" name="department" value="{{$mcm->department}}">
                    <input type="hidden" name="manager_flg" value="{{$mcm->manager_flg}}">
                    <input type="hidden" name="updated_at" value="{{$mcm->updated_at}}">
                </div>
            </div>
        @endforeach
        @if ((count($managementCompanyMembers) + 1) % 3 !== 0)
            @for ($i = 0; $i < 3 - (count($managementCompanyMembers) + 1) % 3; $i++)
            <div class="col-md-4  mb-4"><div class="cmn-list-none"></div></div>
            @endfor
        @endif
    </div>
    <div class="row justify-content-center">
        {{ $managementCompanyMembers->links() }}
    </div>
</div>

<!-- modal -->
<form method="POST" id="form" action="{{ route('mc.managementCompanyMember.store') }}" onsubmit="buttonDisabled();">
    @csrf
    <input type="hidden" name="user_id" value="{{ old('user_id') }}" >
    <input type="hidden" name="updated_at" value="{{ old('updated_at') }}">
    <div class="modal fade" id="modal" tabindex="-1" >
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header custom-modal-header">
                    <h5 class="modal-title w-100 text-center">ユーザー登録</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container">
                    <div class="justify-content-center">
                        <div class="row">
                            <div class="col-md-6 mb-1">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="姓" required>
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="名" required>
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="emergency_tel" type="tel" class="form-control @error('emergency_tel') is-invalid @enderror" name="emergency_tel" value="{{ old('emergency_tel') }}" placeholder="緊急連絡先（ハイフン有、半角数字）" required>
                                @error('emergency_tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="department" type="tel" class="form-control @error('department') is-invalid @enderror" name="department" value="{{ old('department') }}" placeholder="部署名">
                                @error('department')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1 mt-2">
                                <div class="form-check">
                                    <label class="form-check-label" for="manager_flg">
                                        <input class="form-check-input" type="checkbox" id="manager_flg" name="manager_flg" value="1" @if(old('manager_flg')=='1') checked @endif>
                                        全アクセス権限
                                    </label>
                                </div>
                                @error('manager_flg')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 text-center mt-5 mb-5">
                            <button type="submit" class="btn btn-search px-5" id="submit-button">
                                登録
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var putform = '@method("PUT")';
    var storeUrl = '{{ route("mc.managementCompanyMember.store") }}';
    var updateUrl = '{{ route("mc.managementCompanyMember.update", "USER_ID") }}';
    var modalObj = $('#modal');
    var formObj = $('#form');
    $('.cmn-list').on('click', function () {
        var userId = $(this).attr('id').replace('cmn-list-', '');
        $(formObj).find('[name=user_id]').val(userId);
        $('#alert').remove();
        $('[role=alert]').remove();
        $(formObj).find('input').removeClass('is-invalid');
        if (userId === 'store') {
            $(modalObj).find('input').val('');
            $(modalObj).find('input[type=checkbox]').prop("checked",false);
            atStore();
        } else {
            atUpdate(userId);
            $(formObj).find('[name=last_name]').val($(this).find('[name=last_name]').val());
            $(formObj).find('[name=first_name]').val($(this).find('[name=first_name]').val());
            $(formObj).find('[name=email]').val($(this).find('[name=email]').val());
            $(formObj).find('[name=emergency_tel]').val($(this).find('[name=emergency_tel]').val());
            $(formObj).find('[name=department]').val($(this).find('[name=department]').val());
            $(formObj).find('[name=updated_at]').val($(this).find('[name=updated_at]').val());
            if ($(this).find('[name=manager_flg]').val() == '1') {
                $(formObj).find('[name=manager_flg]').prop("checked",true);
            } else {
                $(formObj).find('[name=manager_flg]').prop("checked",false);
            }
        }
        $(modalObj).modal('show');
    });
    function atStore() {
        $('.modal-title').text('ユーザー登録');
        $('#submit-button').text('登録');
        $(formObj).attr('action', storeUrl);
        $(formObj).find('[name=_method]').remove();
        $(formObj).find('[name=email]').prop('disabled', false);
    }
    function atUpdate(userId) {
        $('.modal-title').text('ユーザー更新');
        $('#submit-button').text('更新');
        $(formObj).attr('action', updateUrl.replace('USER_ID', userId));
        $(formObj).find('[name=_method]').remove();
        $(formObj).append(putform);
        $(formObj).find('[name=email]').prop('disabled', true);
    }
    @if(count($errors) > 0)
        window.onload = function () {
            userId = $('#form').find('[name=user_id]').val();
            if (userId === 'store') {
                atStore();
            } else {
                var email = $('#cmn-list-' + userId).find('[name=email]').val();
                $('#form').find('[name=email]').val(email);
                atUpdate(userId);
            }
            $('#modal').removeClass('fade');
            $('#modal').modal();
            $('#modal').addClass('fade');
        };
    @endif
</script>
@endsection
