@extends('layouts.mc.app')

@section('content')
@if ($s === 'approve')
<div class="container-fluid" style="margin: 10px 0 -55px 0;padding: 0 59px;">
    <div class="row justify-content-center">
        <div class="alert alert-success col-md-12">完了報告書を承認しました。</div>
    </div>
</div>
@elseif ($s === 'declined')
<div class="container-fluid" style="margin: 10px 0 -55px 0;padding: 0 59px;">
    <div class="row justify-content-center">
        <div class="alert alert-success col-md-12">完了報告書を差し戻しました。</div>
    </div>
</div>
@endif
<ul class="nav nav-tabs pl-4">
  <li class="nav-item">
    <a class="nav-link @if ($section === config('const.Reports.DispSection.SUBMITTED')) active @endif" href="{{ route('mc.reportList', ['section' => config('const.Reports.DispSection.SUBMITTED')]) }}">受信未処理({{$submitted_cnt}})</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if ($section === config('const.Reports.DispSection.COMPLETED')) active @endif" href="{{ route('mc.reportList', ['section' => config('const.Reports.DispSection.COMPLETED')]) }}">承認済み</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if ($section === config('const.Reports.DispSection.REMAND')) active @endif" href="{{ route('mc.reportList', ['section' => config('const.Reports.DispSection.REMAND')]) }}">差し戻し({{$declined_cnt}})</a>
  </li>
</ul>
<div class="container-fluid px-5 mt-n3">
    <form method="GET" action="{{ route('mc.reportList') }}" onsubmit="buttonDisabled();">
        <input type="hidden" name="section" value="{{$section}}">
        <div class="row justify-content-center mt-4" id="searchConditions">
            <div class="col-md-4"><h6 class="searchCondLabel">物件名</h6></div>
            <div class="col-md-4"><h6 class="searchCondLabel">ステータス</h6></div>
            <div class="col-md-3"><h6 class="searchCondLabel">キーワード</h6></div>
            <div class="col-md-1"></div>
            <div class="col-md-4 search-form"><input type="text" name="property_name" class="form-control" value="{{ $conditions['property_name'] }}"></div>
            <div class="col-md-4">
                <select class="form-control" name="action_status">
                    <option value=""></option>
                    <option value="completed" @if($conditions['action_status'] === config('const.Reports.ActionStatus.COMPLETED')) selected @endif>対応完了</option>
                    <option value="incomplete" @if($conditions['action_status'] === config('const.Reports.ActionStatus.INCOMPLETE')) selected @endif>未完了引継ぎ</option>
                </select>
            </div>
            <div class="col-md-3 search-form"><input type="text" name="keyword" class="form-control" value="{{ $conditions['keyword'] }}"></div>
            <div class="col-md-1"></div>
            <div class="col-md-4"><h6 class="searchCondLabel">受付日</h6></div>
            <div class="col-md-4"><h6 class="searchCondLabel">工事日</h6></div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md"><input type="date" name="reception_date_from" class="form-control" value="{{ $conditions['reception_date_from'] }}"></div>
                    <div class="col-md-1 text-center font-size-22 font-weight-bold px-0">～</div>
                    <div class="col-md"><input type="date" name="reception_date_to" class="form-control" value="{{ $conditions['reception_date_to'] }}"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md"><input type="date" name="construction_date_from" class="form-control" value="{{ $conditions['construction_date_from'] }}"></div>
                    <div class="col-md-1 text-center font-size-22 font-weight-bold px-0">～</div>
                    <div class="col-md"><input type="date" name="construction_date_to" class="form-control" value="{{ $conditions['construction_date_to'] }}"></div>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-clear mr-3">検索条件のクリア</button>
                <button type="submit" class="btn btn-search" style="width:160px;">検索</button>
            </div>

        </div>
    </form>
    <div class="row justify-content-center mt-4">
        <table class="table report-list">
            <tr>
                <th style="width:5%;"></th>
                <th style="width:15%;">物件名</th>
                <th style="width:8%;">部屋番号</th>
                <th style="width:8%;">お客様名</th>
                <th style="width:9%;">@sortablelink('reception_date', '受付日')</th>
                <th style="width:10%;">ステータス</th>
                <th style="width:8%;">対応区分</th>
                <th style="width:9%;">@sortablelink('action_date', '工事日')</th>
                <th style="width:8%;">@sortablelink('construction_amount', '工事金額')</th>
                <th style="width:11%;">業者名</th>
                <th style="width:9%;">工事担当者名</th>
            </tr>
            @if ($reports->count() === 0)
                <tr><td colspan="11" class="text-center" style="cursor:default;font-size:18px;">検索結果がありませんでした</td></tr>
            @endif
            @foreach ($reports as $idx => $r)
                <tr data-href="{{url('/').route('mc.reportDetail', ['report_id' => $r->request_report_id], false)}}" data-toggle="modal" data-target="#modal">
                    <td class="text-center">
                        @if ($r->image_path)
                            <img style="max-height:29px;max-width:38px;" src="{{ $r->image_path }}" title="{{ $r->request_report_id }}">
                        @else
                            <img style="max-height:29px;max-width:38px;" src="{{ asset('img/thmb-photo.svg') }}" title="{{ $r->request_report_id }}">
                        @endif
                    </td>
                    <td title="{{ $r->property_name }}">{{ $r->property_name }}</td>
                    <td title="{{ $r->room_no }}">{{ $r->room_no }}</td>
                    <td title="{{ $r->resident_name }}">{{ $r->resident_name }}</td>
                    <td title="{{ Cmn::dateFormat($r->reception_date) }}">{{ Cmn::dateFormat($r->reception_date) }}</td>
                    <td>
                        @if ($r->action_status === config('const.Reports.ActionStatus.COMPLETED'))
                            <span class="reportListActionStatusComplete">対応完了</span>
                        @else
                            <span class="reportListActionStatusIncomplete">未完了引継ぎ</span>
                        @endif
                    </td>
                    <td>
                        <span class="reportListActionSection">
                            @if ($r->action_section === '1')
                                一次対応
                            @else
                                二次対応
                            @endif
                        </span>
                    </td>
                    <td title="{{ Cmn::dateFormat($r->action_date) }}">{{ Cmn::dateFormat($r->action_date) }}</td>
                    <td class="text-right" title="{{ number_format($r->construction_amount) }}">{{ number_format($r->construction_amount) }}</td>
                    <td title="{{ $r->vendor_name }}">{{ $r->vendor_name }}</td>
                    <td title="{{ $r->user_name }}">{{ $r->user_name }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="row justify-content-center">

        {{ $reports->appends(request()->query())->links() }}
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-fluid" role="document">
        <div class="modal-content p-0">
            <div id="modal-loading" style="width:100%;height:880px;">Loading...</div>
            <iframe id="iframe" src="" frameborder="0" scrolling="no" style="width:100%;height:880px;z-index:2000;"></iframe>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.btn-clear').on('click', function () {
        $('#searchConditions').find('input').val('');
        $('#searchConditions').find('option').attr("selected", false);
        $('#searchConditions').find('select').val('');
    });

    $("#modal").on("show.bs.modal", function(e) {
        $('#modal-loading').show();
        $('#iframe').hide();
        var link = $(e.relatedTarget);
        $('#iframe').attr('src', link.attr("data-href"));
    });

    $('#iframe').on('load', function() {
        $('#modal-loading').hide();
        $('#iframe').show();
    });

    function closeModal() {
        $('#modal').modal('hide');
    }

    function dispOverlay() {
        var overlay = '<div class="modal fade show" id="child-modal" tabindex="-1" aria-modal="true" style="display: block; padding-left: 17px;" onclick="childModalClose();"></div>';
        $('.modal-content').append(overlay);
    }
    function removeOverlay() {
        $('#child-modal').remove();
    }
    function childModalClose() {
        document.getElementById('iframe').contentWindow.closeModal();
    }
    @if ($report_id && $reports->count() !== 0)
        $('.report-list').find('tr').click();
    @endif
</script>
@endsection
