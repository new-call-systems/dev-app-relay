<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>reportDetail</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">

    <style type="text/css" media="print">
        body {
            -webkit-print-color-adjust: exact;
        }

        .print-display-none {
            display: none;
        }

        .reportDetailDescription {
            height: 100%;
        }
    </style>
</head>
@if (!$report)
<body>参照できない完了報告書です。</body>
@elseif (session('is_approve'))
<script>
$(function() {
    window.open("{{ route('mc.reportList') }}?s=approve&section={{config('const.Reports.DispSection.COMPLETED')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@elseif (session('is_declined'))
<script>
$(function() {
    window.open("{{ route('mc.reportList') }}?s=declined&section={{config('const.Reports.DispSection.REMAND')}}", "_parent");
});
</script>
<body style="background-color:#fff;"></body>
@else
<body class="pt-5" style="background-color:#fff;">
    <!-- フラッシュメッセージ -->
    @if ($errors->any())
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="alert alert-danger col-md-12">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
    <div class="container-fluid mt-n3 px-0">
        <div class="reportDetailTitle">
            完了報告書(@if ($report->report_status === config('const.Reports.Status.SUBMITTED'))未承認@elseif($report->report_status === config('const.Reports.Status.DECLINED'))差戻し@else承認済み@endif)
        </div>
        <div class="row justify-content-center mt-3 px-5">
            <div class="col-md-6">
                <span class="reportDetailDateLabel">受付日</span>
                <span class="reportDetailDate">{{ Cmn::dateFormat($report->reception_date) }}</span>
                <span class="reportDetailActionSection">@if ($report->action_section === '1') 一次対応 @else 二次対応 @endif</span>
                @if ($report->action_status === config('const.Reports.ActionStatus.COMPLETED'))
                            <span class="reportDetailActionStatusComplete">対応完了</span>
                        @else
                            <span class="reportDetailActionStatusIncomplete">未完了引継ぎ</span>
                        @endif
            </div>
            <div class="col-md-6 text-right">
                @if ($report->image_paths)
                    <a class="print-display-none" href="javascript:zipForm.submit()"><img src="{{ asset('img/btn-imgdl.svg') }}"></a>
                @endif
                <a class="print-display-none ml-2" href="javascript:pdfForm.submit()"><img src="{{ asset('img/btn-pdfdl.svg') }}"></a>
                <a class="print-display-none" href="javascript:void(0)" onclick="print();return false;"><img class="ml-2" src="{{ asset('img/btn-print.svg') }}"></a>
                <form method="POST" name="pdfForm" action="{{ route('mc.downloadReportPdf', ['report_id' => $report->request_report_id]) }}" target="_blank">
                    @csrf
                </form>
                <form method="POST" name="zipForm" action="{{ route('mc.downloadReportZip', ['report_id' => $report->request_report_id]) }}" target="_blank">
                    @csrf
                </form>
            </div>
        </div>
        <div class="row justify-content-center mt-2 px-5">
            <div class="col-md-12 reportDetailManagementCompanyName">{{ $report->management_company_name }}</div>
            <div class="col-md-12 reportDetailPropertyName">{{ $report->property_name }}</div>
            <div class="col-md-12 reportDetailRoomNo">{{ $report->room_no }}号室</div>
        </div>
        <div class="row justify-content-center mt-2 px-5">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-man.svg') }}">お客様名</div><div class="col-md-6 reportDetailInfoValue">{{ $report->resident_name }} 様</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-date.svg') }}">工事日</div><div class="col-md-6 reportDetailInfoValue">{{ Cmn::dateFormat($report->action_date) }}</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-time.svg') }}">対応時間</div><div class="col-md-6 reportDetailInfoValue">{{ substr($report->starting_time, 0, 5) }}-{{ substr($report->ending_time, 0, 5) }}</div>
                    <div class="col-md-6 reportDetailInfoHeader"><img class="mr-2" src="{{ asset('img/icon-bill.svg') }}">工事金額</div><div class="col-md-6 reportDetailInfoValue">{{ number_format($report->construction_amount) }}円</div>
                    @if ($report->image_paths)
                        <div class="col-md-12 mt-3 reportDetailImg">
                            <a href="#lightbox" data-toggle="modal">
                                <img style="max-height:225px;max-width:100%;" src="{{ $report->image_paths[0] }}">
                            </a>
                        </div>
                    @else
                        <div class="col-md-12 mt-3 reportDetailNoImg">
                            <img style="height:100%;" src="{{ asset('img/no-image-l.svg') }}">
                        </div>
                    @endif
                    <div class="col-md-12 reportDetailDeclare mt-3">上記の内容で作業を実施しました</div>
                    <div class="col-md-12 reportDetailUserName">{{ $report->user_name }}</div>
                </div>
            </div>
            <div class="col-md-8 reportDetailDescription">
                @if ($report->comment)
                    <div class="alert alert-danger print-display-none" role="alert" style="font-size: 15px;line-height: initial;">[差戻し理由]<br>{!! nl2br($report->comment) !!}</div>
                @endif
                {!! nl2br($report->description) !!}
            </div>
        </div>
        <div class="modal-footer customModalFooter print-display-none">
            @if ($report->report_status === config('const.Reports.Status.SUBMITTED') || $report->report_status === config('const.Reports.Status.DECLINE'))
                <button type="button" class="btn btn-slate" onclick="window.parent.closeModal();">閉じる</button>
                <button type="button" class="btn btn-orange mr-auto" data-toggle="modal" data-target="#decline-modal">差し戻し</button>
                <button type="button" class="btn btn-green" data-toggle="modal" data-target="#approve-modal">承認する</button>
            @else
                <button type="button" class="btn btn-slate mr-auto" onclick="window.parent.closeModal();">閉じる</button>
            @endif
        </div>
    </div>

<!-- 承認Modal -->
<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title approveModalTitle w-100">承認</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body approveModalBody">記載の内容で完了報告書を承認します。</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-slate px-3 mr-3" data-dismiss="modal">キャンセル</button>
                <form method="POST" action="{{ route('mc.approveReport', ['report_id' => $report->request_report_id]) }}" onsubmit="buttonDisabled();">
                    @csrf
                    <button type="submit" class="btn btn-green px-4">承認する</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 差し戻しModal -->
<div class="modal fade" id="decline-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div id="decline-modal-form">
                <div class="modal-header" style="border:transparent;">
                    <div class="modal-title approveModalTitle w-100">差し戻し</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" placeholder="差し戻し理由を入力してください。" style="height:146px;resize:none;" id="decline-text"></textarea>
                    <span class="invalid-feedback" role="alert" id="decline-text-error" style="display:none;">
                        <strong>差し戻し理由は必ず指定してください。</strong>
                    </span>
                </div>
                <div class="modal-footer" style="border:transparent;">
                    <button type="button" class="btn btn-slate px-3 mr-3" data-dismiss="modal">キャンセル</button>
                    <button type="submit" class="btn btn-green px-4" id="btn-decline-preview">プレビュー</button>
                </div>
            </div>

            <div id="decline-modal-preview" style="display:none;">
                <div class="modal-header">
                    <div class="modal-title approveModalTitle w-100">差し戻し</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="decline-modal-preview-body" style="height:176px;overflow-y:scroll;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-slate px-3 mr-3" id="btn-decline-edit">編集する</button>
                    <form method="POST" action="{{ route('mc.declineReport', ['report_id' => $report->request_report_id]) }}" onsubmit="buttonDisabled();">
                        @csrf
                        <input type="hidden" name="comment" id="comment" value="">
                        <button type="submit" class="btn btn-orange px-4">差し戻し</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- lightbox -->
<div class="modal fade and carousel slide" id="lightbox">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width:1150px;height:70%;background-color:transparent;">
        <div class="modal-content mx-auto" style="border:0;width:100%;height:683px;background-color:#000;">
            <div class="modal-header" style="border:transparent;">
                <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span id="carouselControls-image-number">{{'1/'.count($report->image_paths)}}</span>）</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff;">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <div id="carouselControls" class="carousel slide h-100" data-ride="carousel" data-interval="0" total-count="{{count($report->image_paths)}}">
                    <div class="carousel-inner m-auto h-100" style="width:60%;">
                        @foreach ($report->image_paths as $idx => $img)
                            <div class="carousel-item h-100 text-center @if ($idx === 0) active @endif">
                                <img class="carousel-img" src="{{ $img }}">
                            </div>
                        @endforeach
                    </div>
                    @if (count($report->image_paths) > 1)
                        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        </a>
                        <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
$('#btn-decline-preview').on('click', function() {
    if ($('#decline-text').val() == '') {
        $('#decline-text').addClass('is-invalid')
        $('#decline-text-error').show()
        $('#decline-text').focus();
        return false;
    }
    var text = $('#decline-text').val().replace( /\r?\n/g, '<br>' );;
    $('#comment').val($('#decline-text').val());
    $('#decline-modal-preview-body').html(text);
    $('#decline-modal-form').hide();
    $('#decline-modal-preview').show();
});
$('#btn-decline-edit').on('click', function () {
    $('#decline-text').removeClass('is-invalid')
    $('#decline-text-error').hide()
    $('#decline-modal-preview').hide();
    $('#decline-modal-form').show();
    $('#decline-text').focus();
});
$('#decline-modal').on('show.bs.modal', function () {
    $('#decline-text').removeClass('is-invalid')
    $('#decline-text-error').hide()
    $('#decline-text').val('');
    $('#comment').val('');
    $('#decline-modal-preview').hide();
    $('#decline-modal-form').show();
});
$('#decline-modal').on('shown.bs.modal', function () {
    $('#decline-text').focus();
});
$(document).ready(function () {
    $(document).on('slid.bs.carousel', function(e) {
        var nextPage = e.to + 1;
        var totalCnt = $(e.target).attr('total-count');
        var targetId = $(e.target).attr('id') + '-image-number';
        $('#' + targetId).html(nextPage + '/' + totalCnt);
    });
    $(document).on('show.bs.modal', function(e) {
        window.parent.dispOverlay();
    });
    $(document).on('hide.bs.modal', function(e) {
        window.parent.removeOverlay();
    });
});
function closeModal() {
    $('.modal').each(function (i, obj) {
        $(obj).modal('hide');
    });
}
</script>
@endif
</html>