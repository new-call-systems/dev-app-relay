<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>完了報告書</title>

    <style type="text/css">
        @font-face {
            font-family: ipag;
            font-style: normal;
            font-weight: normal;
            src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
        }
        @font-face {
            font-family: ipag;
            font-style: bold;
            font-weight: bold;
            src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
        }
        body {
            font-family: ipag !important;
            font-size: 38px;
            border: 5px solid #000;
            padding: 0;
        }
        .title {
            text-align: center;
            font-size: 50px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .table {
            border-top: 1px #000 dotted;
            border-bottom: 1px #000 dotted;
            padding: 10px;
            width: 100%;
            margin-bottom: 10px;
            table-layout: fixed;
        }
        .td-title {
            width: 20%;
        }
        .description-table {
            border-bottom: 1px #000 dotted;
            padding: 0 10px 10px 10px;
            width: 100%;
            margin-bottom: 10px;
            table-layout: fixed;
            height: 540px;
        }
        .description-td {
            font-size: 22px;
            width: 100%;
            word-wrap: break-word;
            padding-right: 10px;
            vertical-align: top;
        }
        .other-table {
            padding:10px 0;
        }
    </style>
</style>
</head>

<body>
    <div class="title">
        完了報告書(@if ($report->report_status === config('const.Reports.Status.SUBMITTED'))未承認@elseif($report->report_status === config('const.Reports.Status.DECLINED'))差戻し@else承認済み@endif)
    </div>
    <table class="table">
        <td class="td-title">受付日</td>
        <td>{{ Cmn::dateFormat($report->reception_date) }}</td>
    </table>
    <table class="table">
        <td class="td-title">物件名</td>
        <td>{{ $report->property_name }}</td>
        </table>
    <table class="table">
        <td class="td-title">部屋番号</td>
        <td>{{ $report->room_no }}号室</td>
    </table>
    <table class="table">
        <tr><td class="td-title">作業内容</td></tr>
    </table>
    <table class="description-table">
        <tr><td class="description-td">{!! nl2br($report->description) !!}</td></tr>
    </table>
    <table class="table">
        <td class="td-title">工事日</td>
        <td>{{ Cmn::dateFormat($report->action_date) }}</td>
    </table>
    <table class="table">
        <td class="td-title">対応時間</td>
        <td>{{ substr($report->starting_time, 0, 5) }} ～ {{ substr($report->ending_time, 0, 5) }}</td>
    </table>
    <table class="table">
        <td class="td-title">工事金額</td>
        <td>{{ number_format($report->construction_amount) }}円</td>
    </table>
    <table class="other-table" style="border-top:1px solid #000;width:100%;margin:0;">
        <tr>
            <td style="width:30%;vertical-align:middle;">
                @if($report->action_section === '1')
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox.png') }}">一次対応
                @else
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox-none.png') }}">一次対応
                @endif
            </td>
            <td rowspan="2">上記の内容で作業を実施しました。</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;">
                @if($report->action_section === '2')
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox.png') }}">二次対応
                @else
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox-none.png') }}">二次対応
                @endif
            </td>
        </tr>
    </table>
    <table class="other-table" style="border-top:1px solid #000;border-bottom:1px solid #000;width:100%;margin:0;">
        <tr>
            <td style="width:30%;">
                @if($report->action_status === config('const.Reports.ActionStatus.INCOMPLETE'))
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox.png') }}">未完了引継ぎ
                @else
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox-none.png') }}">未完了引継ぎ
                @endif
            </td>
            <td>お客様名</td>
            <td>{{ $report->resident_name }}</td>
        </tr>
        <tr>
            <td>
                @if($report->action_status === config('const.Reports.ActionStatus.INCOMPLETE'))
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox-none.png') }}">工事完了
                @else
                    <img style="margin: 0 5px;" src="{{ asset('img/icon-checkbox.png') }}">工事完了
                @endif
            </td>
            <td>作業担当者名</td>
            <td>{{ $report->user_name }}</td>
        </tr>
    </table>
</body>
</html>