@extends('layouts.so.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4 mb-4">
            <div id="cmn-list-store" class="cmn-list cmn-list-add text-center align-middle">
                管理会社登録
            </div>
        </div>
        @for ($i = 0; $i < count($managementCompanies); $i++)
            <div class="col-md-4  mb-4">
                <div id="cmn-list-{{$managementCompanies[$i]->management_company_id}}" class="cmn-list">
                    <div class="cmn-list-created-at mb-2">登録日&nbsp;{{ Cmn::dateFormat($managementCompanies[$i]->created_at) }}</div>
                    <h6 class="cmn-list-title">{{ $managementCompanies[$i]->name }}</h6>
                    <div class="cmn-list-other-info">代表者：{{ $managementCompanies[$i]->last_name }}&nbsp{{ $managementCompanies[$i]->first_name }}</div>
                    <div class="cmn-list-other-info">メールアドレス：{{ $managementCompanies[$i]->email }}</div>
                    <div class="cmn-list-other-info">電話番号：{{ $managementCompanies[$i]->tel }}</div>
                    <input type="hidden" name="user_id" value="{{$managementCompanies[$i]->user_id}}">
                    <input type="hidden" name="name" value="{{$managementCompanies[$i]->name}}">
                    <input type="hidden" name="last_name" value="{{$managementCompanies[$i]->last_name}}">
                    <input type="hidden" name="first_name" value="{{$managementCompanies[$i]->first_name}}">
                    <input type="hidden" name="department" value="{{$managementCompanies[$i]->department}}">
                    <input type="hidden" name="email" value="{{$managementCompanies[$i]->email}}">
                    <input type="hidden" name="emergency_tel" value="{{$managementCompanies[$i]->emergency_tel}}">
                    <input type="hidden" name="address" value="{{$managementCompanies[$i]->address}}">
                    <input type="hidden" name="tel" value="{{$managementCompanies[$i]->tel}}">
                    <input type="hidden" name="fax" value="{{$managementCompanies[$i]->fax}}">
                    <input type="hidden" name="support_tel" value="{{$managementCompanies[$i]->support_tel}}">
                    <input type="hidden" name="mc_updated_at" value="{{$managementCompanies[$i]->mc_updated_at}}">
                    <input type="hidden" name="mcm_updated_at" value="{{$managementCompanies[$i]->mcm_updated_at}}">
                    <input type="hidden" name="channel_id" value="{{$managementCompanies[$i]->channel_id}}">
                    <input type="hidden" name="channel_secret" value="{{$managementCompanies[$i]->channel_secret}}">
                    <input type="hidden" name="channel_access_token" value="{{$managementCompanies[$i]->channel_access_token}}">
                    <input type="hidden" name="liff_id" value="{{$managementCompanies[$i]->liff_id}}">
                </div>
            </div>
        @endfor
        @if ((count($managementCompanies) + 1) % 3 !== 0)
            @for ($i = 0; $i < 3 - (count($managementCompanies) + 1) % 3; $i++)
            <div class="col-md-4  mb-4"><div class="cmn-list-none"></div></div>
            @endfor
        @endif
    </div>
    <div class="row justify-content-center">
        {{ $managementCompanies->links() }}
    </div>
</div>

<!-- modal -->
<form method="POST" id="form" action="{{ route('so.managementCompany.store') }}" onsubmit="buttonDisabled();">
    @csrf
    <input type="hidden" name="user_id" value="{{ old('user_id') }}" >
    <input type="hidden" name="management_company_id" value="{{ old('management_company_id') }}" >
    <input type="hidden" name="mc_updated_at" value="{{ old('mc_updated_at') }}">
    <input type="hidden" name="mcm_updated_at" value="{{ old('mcm_updated_at') }}">
    <div class="modal fade" id="modal" tabindex="-1" >
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header custom-modal-header">
                    <h5 class="modal-title w-100 text-center">管理会社登録</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="管理会社名" required autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <label for="last_name" class="col-md-12 col-form-label">代表者</label>
                        <div class="col-md-6 mb-1">
                            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="姓" required>
                            @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="名" required>
                            @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="department" type="text" class="form-control @error('department') is-invalid @enderror" name="department" value="{{ old('department') }}" placeholder="部署名">
                            @error('department')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1"></div>
                        <label for="email" class="col-md-12 col-form-label">連絡先</label>
                        <div class="col-md-6 mb-1">
                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="所在地" required>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="tel" type="tel" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" placeholder="電話番号（ハイフン有、半角数字）" required>
                            @error('tel')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="fax" type="tel" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ old('fax') }}" placeholder="FAX（ハイフン有、半角数字）">
                            @error('fax')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="emergency_tel" type="tel" class="form-control @error('emergency_tel') is-invalid @enderror" name="emergency_tel" value="{{ old('emergency_tel') }}" placeholder="緊急連絡先（ハイフン有、半角数字）" required>
                            @error('emergency_tel')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="support_tel" type="tel" class="form-control @error('support_tel') is-invalid @enderror" name="support_tel" value="{{ old('support_tel') }}" placeholder="入居者問合せ先（ハイフン有、半角数字）">
                            @error('support_tel')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <label for="email" class="col-md-12 col-form-label">LINE情報</label>
                        <div class="col-md-6 mb-1">
                            <input id="channel_id" type="text" class="form-control @error('channel_id') is-invalid @enderror" name="channel_id" value="{{ old('channel_id') }}" placeholder="チャンネルID">
                            @error('channel_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="channel_secret" type="text" class="form-control @error('channel_secret') is-invalid @enderror" name="channel_secret" value="{{ old('channel_secret') }}" placeholder="シークレット">
                            @error('channel_secret')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="channel_access_token" type="text" class="form-control @error('channel_access_token') is-invalid @enderror" name="channel_access_token" value="{{ old('channel_access_token') }}" placeholder="アクセストークン">
                            @error('channel_access_token')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-1">
                            <input id="liff_id" type="text" class="form-control @error('liff_id') is-invalid @enderror" name="liff_id" value="{{ old('liff_id') }}" placeholder="LIFF ID">
                            @error('liff_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-12 text-center mt-5 mb-5">
                            <button type="submit" class="btn btn-search px-5" id="submit-button">
                                登録
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var putform = '@method("PUT")';
    var storeUrl = '{{ route("so.managementCompany.store") }}';
    var updateUrl = '{{ route("so.managementCompany.update", "MANAGEMENT_COMPANY_ID") }}';
    var modalObj = $('#modal');
    var formObj = $('#form');
    $('.cmn-list').on('click', function () {
        var companyId = $(this).attr('id').replace('cmn-list-', '');
        $(formObj).find('[name=management_company_id]').val(companyId);
        $('#alert').remove();
        $('[role=alert]').remove();
        $(formObj).find('input').removeClass('is-invalid');
        if (companyId === 'store') {
            $(modalObj).find('input').val('');
            atStore();
        } else {
            atUpdate(companyId);
            $(formObj).find('[name=user_id]').val($(this).find('[name=user_id]').val());
            $(formObj).find('[name=name]').val($(this).find('[name=name]').val());
            $(formObj).find('[name=last_name]').val($(this).find('[name=last_name]').val());
            $(formObj).find('[name=first_name]').val($(this).find('[name=first_name]').val());
            $(formObj).find('[name=department]').val($(this).find('[name=department]').val());
            $(formObj).find('[name=email]').val($(this).find('[name=email]').val());
            $(formObj).find('[name=emergency_tel]').val($(this).find('[name=emergency_tel]').val());
            $(formObj).find('[name=address]').val($(this).find('[name=address]').val());
            $(formObj).find('[name=tel]').val($(this).find('[name=tel]').val());
            $(formObj).find('[name=fax]').val($(this).find('[name=fax]').val());
            $(formObj).find('[name=support_tel]').val($(this).find('[name=support_tel]').val());
            $(formObj).find('[name=mc_updated_at]').val($(this).find('[name=mc_updated_at]').val());
            $(formObj).find('[name=mcm_updated_at]').val($(this).find('[name=mcm_updated_at]').val());
            $(formObj).find('[name=channel_id]').val($(this).find('[name=channel_id]').val());
            $(formObj).find('[name=channel_secret]').val($(this).find('[name=channel_secret]').val());
            $(formObj).find('[name=channel_access_token]').val($(this).find('[name=channel_access_token]').val());
            $(formObj).find('[name=liff_id]').val($(this).find('[name=liff_id]').val());
        }
        $(modalObj).modal('show');
    });
    function atStore() {
        $('.modal-title').text('管理会社登録');
        $('#submit-button').text('登録');
        $(formObj).attr('action', storeUrl);
        $(formObj).find('[name=_method]').remove();
        $(formObj).find('[name=email]').prop('disabled', false);
    }
    function atUpdate(companyId) {
        $('.modal-title').text('管理会社更新');
        $('#submit-button').text('更新');
        $(formObj).attr('action', updateUrl.replace('MANAGEMENT_COMPANY_ID', companyId));
        $(formObj).find('[name=_method]').remove();
        $(formObj).append(putform);
        $(formObj).find('[name=email]').prop('disabled', true);
    }
    @if(count($errors) > 0)
        window.onload = function () {
            companyId = $('#form').find('[name=management_company_id]').val();
            if (companyId === 'store') {
                atStore();
            } else {
                var email = $('#cmn-list-' + companyId).find('[name=email]').val();
                $('#form').find('[name=email]').val(email);
                atUpdate(companyId);
            }
            $('#modal').removeClass('fade');
            $('#modal').modal();
            $('#modal').addClass('fade');
        };
    @endif
</script>
@endsection
