@extends('layouts.so.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4 mb-4">
            <div id="cmn-list-store" class="cmn-list cmn-list-add text-center align-middle">
                業者登録
            </div>
        </div>
        @for ($i = 0; $i < count($vendors); $i++)
            <div class="col-md-4  mb-4">
                <div id="cmn-list-{{$vendors[$i]->vendor_id}}" class="cmn-list">
                    <div class="cmn-list-created-at mb-2">登録日&nbsp;{{ Cmn::dateFormat($vendors[$i]->created_at) }}</div>
                    <h6 class="cmn-list-title">{{ $vendors[$i]->name }}</h6>
                    <div class="cmn-list-other-info">代表者：{{ $vendors[$i]->last_name }}&nbsp{{ $vendors[$i]->first_name }}</div>
                    <div class="cmn-list-other-info">メールアドレス：{{ $vendors[$i]->email }}</div>
                    <div class="cmn-list-other-info">電話番号：{{ $vendors[$i]->tel }}</div>
                    <input type="hidden" name="user_id" value="{{$vendors[$i]->user_id}}">
                    <input type="hidden" name="name" value="{{$vendors[$i]->name}}">
                    <input type="hidden" name="last_name" value="{{$vendors[$i]->last_name}}">
                    <input type="hidden" name="first_name" value="{{$vendors[$i]->first_name}}">
                    <input type="hidden" name="email" value="{{$vendors[$i]->email}}">
                    <input type="hidden" name="emergency_tel_1" value="{{$vendors[$i]->emergency_tel_1}}">
                    <input type="hidden" name="address" value="{{$vendors[$i]->address}}">
                    <input type="hidden" name="tel" value="{{$vendors[$i]->tel}}">
                    <input type="hidden" name="fax" value="{{$vendors[$i]->fax}}">
                    <input type="hidden" name="emergency_tel_2" value="{{$vendors[$i]->emergency_tel_2}}">
                    <input type="hidden" name="branch_name" value="{{$vendors[$i]->branch_name}}">
                    <input type="hidden" name="parent_address" value="{{$vendors[$i]->parent_address}}">
                    <input type="hidden" name="parent_tel" value="{{$vendors[$i]->parent_tel}}">
                    <input type="hidden" name="parent_fax" value="{{$vendors[$i]->parent_fax}}">
                    <input type="hidden" name="vn_updated_at" value="{{$vendors[$i]->vn_updated_at}}">
                    <input type="hidden" name="vnm_updated_at" value="{{$vendors[$i]->vnm_updated_at}}">
                </div>
            </div>
        @endfor
        @if ((count($vendors) + 1) % 3 !== 0)
            @for ($i = 0; $i < 3 - (count($vendors) + 1) % 3; $i++)
            <div class="col-md-4  mb-4"><div class="cmn-list-none"></div></div>
            @endfor
        @endif
    </div>
    <div class="row justify-content-center">
        {{ $vendors->links() }}
    </div>
</div>

<!-- modal -->
<form method="POST" id="form" action="{{ route('so.vendor.store') }}" onsubmit="buttonDisabled();">
    @csrf
    <input type="hidden" name="user_id" value="{{ old('user_id') }}" >
    <input type="hidden" name="vendor_id" value="{{ old('vendor_id') }}" >
    <input type="hidden" name="vn_updated_at" value="{{ old('vn_updated_at') }}">
    <input type="hidden" name="vnm_updated_at" value="{{ old('vnm_updated_at') }}">
    <div class="modal fade" id="modal" tabindex="-1" >
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header custom-modal-header">
                    <h5 class="modal-title w-100 text-center">業者登録</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container">
                    <div class="justify-content-center">
                        <div class="row" >
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="業者社名" required autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-3">
                                <input id="branch_name" type="text" class="form-control @error('branch_name') is-invalid @enderror" name="branch_name" value="{{ old('branch_name') }}" placeholder="支店・営業所名">
                                @error('branch_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <label for="email" class="col-form-label">業者情報</label>
                                <span class="text-muted ml-5 col-form-label form-control-sm" >※支店登録の場合は、支店情報を入力してください。</span>
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" placeholder="所在地" required>
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="tel" type="tel" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" placeholder="連絡先（ハイフン有、半角数字）" required>
                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="fax" type="tel" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ old('fax') }}" placeholder="FAX（ハイフン有、半角数字）">
                                @error('fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="emergency_tel_2" type="tel" class="form-control @error('emergency_tel_2') is-invalid @enderror" name="emergency_tel_2" value="{{ old('emergency_tel_2') }}" placeholder="緊急連絡先（ハイフン有、半角数字）" required>
                                @error('emergency_tel_2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <label for="last_name" class="col-form-label">担当窓口</label>
                                <span class="text-muted ml-5 col-form-label form-control-sm" >※緊急連絡先は携帯電話の番号を入力してください。</span>
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="姓" required>
                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="名" required>
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="emergency_tel_1" type="tel" class="form-control @error('emergency_tel_1') is-invalid @enderror" name="emergency_tel_1" value="{{ old('emergency_tel_1') }}" placeholder="緊急連絡先（ハイフン有、半角数字）" required>
                                @error('emergency_tel_1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <label for="email" class="col-form-label">本社情報</label>
                                <span class="text-muted ml-5 col-form-label form-control-sm" >※支店登録の場合のみ入力してください。</span>
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="parent_address" type="text" class="form-control @error('parent_address') is-invalid @enderror" name="parent_address" value="{{ old('parent_address') }}" placeholder="所在地">
                                @error('parent_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="parent_tel" type="tel" class="form-control @error('parent_tel') is-invalid @enderror" name="parent_tel" value="{{ old('parent_tel') }}" placeholder="連絡先（ハイフン有、半角数字）">
                                @error('parent_tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-1">
                                <input id="parent_fax" type="tel" class="form-control @error('parent_fax') is-invalid @enderror" name="parent_fax" value="{{ old('parent_fax') }}" placeholder="FAX（ハイフン有、半角数字）">
                                @error('parent_fax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 text-center mt-5 mb-5">
                            <button type="submit" class="btn btn-search px-5" id="submit-button">
                                登録
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var putform = '@method("PUT")';
    var storeUrl = '{{ route("so.vendor.store") }}';
    var updateUrl = '{{ route("so.vendor.update", "VENDOR_ID") }}';
    var modalObj = $('#modal');
    var formObj = $('#form');
    $('.cmn-list').on('click', function () {
        var vendorId = $(this).attr('id').replace('cmn-list-', '');
        $(formObj).find('[name=vendor_id]').val(vendorId);
        $('#alert').remove();
        $('[role=alert]').remove();
        $(formObj).find('input').removeClass('is-invalid');
        if (vendorId === 'store') {
            $(modalObj).find('input').val('');
            atStore();
        } else {
            atUpdate(vendorId);
            $(formObj).find('[name=user_id]').val($(this).find('[name=user_id]').val());
            $(formObj).find('[name=name]').val($(this).find('[name=name]').val());
            $(formObj).find('[name=last_name]').val($(this).find('[name=last_name]').val());
            $(formObj).find('[name=first_name]').val($(this).find('[name=first_name]').val());
            $(formObj).find('[name=email]').val($(this).find('[name=email]').val());
            $(formObj).find('[name=emergency_tel_1]').val($(this).find('[name=emergency_tel_1]').val());
            $(formObj).find('[name=address]').val($(this).find('[name=address]').val());
            $(formObj).find('[name=tel]').val($(this).find('[name=tel]').val());
            $(formObj).find('[name=fax]').val($(this).find('[name=fax]').val());
            $(formObj).find('[name=emergency_tel_2]').val($(this).find('[name=emergency_tel_2]').val());
            $(formObj).find('[name=branch_name]').val($(this).find('[name=branch_name]').val());
            $(formObj).find('[name=parent_address]').val($(this).find('[name=parent_address]').val());
            $(formObj).find('[name=parent_tel]').val($(this).find('[name=parent_tel]').val());
            $(formObj).find('[name=parent_fax]').val($(this).find('[name=parent_fax]').val());

            $(formObj).find('[name=vn_updated_at]').val($(this).find('[name=vn_updated_at]').val());
            $(formObj).find('[name=vnm_updated_at]').val($(this).find('[name=vnm_updated_at]').val());
        }
        $(modalObj).modal('show');
    });
    function atStore() {
        $('.modal-title').text('業者登録');
        $('#submit-button').text('登録');
        $(formObj).attr('action', storeUrl);
        $(formObj).find('[name=_method]').remove();
        $(formObj).find('[name=email]').prop('disabled', false);
    }
    function atUpdate(vendorId) {
        $('.modal-title').text('業者更新');
        $('#submit-button').text('更新');
        $(formObj).attr('action', updateUrl.replace('VENDOR_ID', vendorId));
        $(formObj).find('[name=_method]').remove();
        $(formObj).append(putform);
        $(formObj).find('[name=email]').prop('disabled', true);
    }
    @if(count($errors) > 0)
        window.onload = function () {
            vendorId = $('#form').find('[name=vendor_id]').val();
            if (vendorId === 'store') {
                atStore();
            } else {
                var email = $('#cmn-list-' + vendorId).find('[name=email]').val();
                $('#form').find('[name=email]').val(email);
                atUpdate(vendorId);
            }
            $('#modal').removeClass('fade');
            $('#modal').modal();
            $('#modal').addClass('fade');
        };
    @endif
</script>
@endsection
