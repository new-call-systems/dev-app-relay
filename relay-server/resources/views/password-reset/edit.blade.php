@extends('layouts.mc.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pl-0 mt-5 mb-3 text-center">
                <h3 class="system-title">パスワード再設定</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 py-5 px-5" style="background-color:#fff;">
                <form method="POST"  action="{{ route('password_reset.update') }}" onsubmit="buttonDisabled();">
                    @csrf
                    <input type="hidden" id="reset_token" name="reset_token" value="{{ $userToken->token }}">
                    <div class="form-group row">
                        <label for="password" class="col-md-12 col-form-label">新パスワード</label>

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  required autofocus>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password_confirmation" class="col-md-12 col-form-label">新パスワード再確認</label>

                        <div class="col-md-12">
                            <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="current-password">

                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mt-4 mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-search px-4">
                                再設定
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
