@extends('layouts.mc.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pl-0 mt-5  mb-3 text-center">
                <h3 class="system-title">パスワードリセットメール送信完了</h3>
            </div>
        </div>
        <div class="col-md-12 pb-2 text-center">
            <label for="code" class="col-md-12 col-form-label">パスワードリセットに必要なメールを送信しました。</label>
            <label for="code" class="col-md-12 col-form-label">メール添付のURLにアクセスし、パスワードの再設定を行ってください。</label>
            <label for="code" class="col-md-12 col-form-label">有効期限はメールを受信してから1時間となります。</label>
        </div>
    </div>
@endsection
