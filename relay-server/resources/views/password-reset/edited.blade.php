@extends('layouts.mc.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pl-0 mt-5  mb-3 text-center">
                <h3 class="system-title">パスワード再設定完了</h3>
            </div>
        </div>
        <div class="col-md-12 pb-2 text-center">
            <label for="code" class="col-md-12 col-form-label">パスワードの再設定が完了しました</label>
        </div>
        <div class="col-md-12 mt-4 text-center">
        @if($roleName === 'management_company_manager')
            <a class="nav-link" href="/mc/login">ログイン画面へ</a>
        @elseif($roleName === 'management_company_member')
            <a class="nav-link" href="/mc/login">ログイン画面へ</a>
        @elseif($roleName === 'system_owner')
            <a class="nav-link" href="/so/login">ログイン画面へ</a>
        @endif
        </div>
    </div>
@endsection
