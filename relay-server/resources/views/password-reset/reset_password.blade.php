@extends('layouts.mc.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pl-0 mt-5  mb-3 text-center">
                <h3 class="system-title">パスワードリセット</h3>
            </div>
        </div>
        <div class="col-md-12 pb-2 text-center">
            <label for="code" class="col-md-12 col-form-label">ご登録されているメールアドレスを入力してください</label>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 py-5 px-5" style="background-color:#fff;">
                <form method="POST" action="{{ route('password_reset.email.send') }}" onsubmit="buttonDisabled();">
                    @csrf

                    <div class="form-group row">
                        <label for="code" class="col-md-12 col-form-label">メールアドレス</label>

                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mt-4 mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-search px-4">
                                メール送信
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
