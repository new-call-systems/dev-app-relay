<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>Coco-en for Management Company</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">
</style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      @unless (Auth::guard('guard_management_company_member')->check())
        <main role="main" class="col-md-12 ml-sm-auto">
          @if (session('info'))
          <div class="container-fluid mt-5">
            <div class="row justify-content-center">
              <div class="alert alert-warning col-md-12">
                {{ session('info') }}
              </div>
            </div>
          </div>
          @endif
          @yield('content')
        </main>
      @else
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div id="sidebarLogo">
              <img src="{{ asset('img/logo-coco-en.svg') }}">
              <div id="sidebarTitle">管理会社</div>
            </div>
            <ul class="nav flex-column">
              <li class="nav-item @if('/'.request()->path() === route('mc.dashboard', [], false)) nav-item-active @endif">
                <a class="nav-link" href="{{ route('mc.dashboard') }}">
                  ダッシュボード
                </a>
              </li>
              <li class="nav-item @if('/'.request()->path() === route('mc.managementCompanyMember', [], false)) nav-item-active @endif">
                <a class="nav-link" href="{{ route('mc.managementCompanyMember') }}">
                  ユーザー管理
                </a>
              </li>
              <li class="nav-item @if('/'.request()->path() === route('mc.request', [], false)) nav-item-active @endif">
                <a class="nav-link" href="{{ route('mc.request') }}">
                  問合せ履歴
                </a>
              </li>
              <li class="nav-item @if('/'.request()->path() === route('mc.reportList', [], false)) nav-item-active @endif">
                <a class="nav-link" href="{{ route('mc.reportList') }}">
                  完了報告一覧
                </a>
              </li>
              <li class="nav-item @if('/'.request()->path() === route('mc.residentList', [], false)) nav-item-active @endif">
                <a class="nav-link" href="{{ route('mc.residentList') }}">
                  入居者一覧
                </a>
              </li>
            </ul>
            <ul class="nav flex-column logout pb-4">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('mc.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                      ログアウト
                  </a>
                  <form id="logout-form" action="{{ route('mc.logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4"  style="padding:0 !important;">
          <!-- フラッシュメッセージ -->
          @if (session('my_status'))
          <div class="container-fluid mt-5">
            <div class="row justify-content-center">
              <div class="alert alert-success col-md-12">
                {{ session('my_status') }}
              </div>
            </div>
          </div>
          @endif
          @if (session('info'))
          <div class="container-fluid mt-5">
            <div class="row justify-content-center">
              <div class="alert alert-warning col-md-12">
                {{ session('info') }}
              </div>
            </div>
          </div>
          @endif
          @yield('content')
        </main>
      @endunless
    </div>
  </div>
</body>
</html>