<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>Coco-en CSP mock</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.5.1.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js').'?'.date('YmdHis') }}" ></script>
    <script src="{{ asset('js/common.js').'?'.date('YmdHis') }}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css').'?'.date('YmdHis') }}" rel="stylesheet">
    <style type="text/css">
        .residentInfo {
            color: #8692b5;
            font-size: 30px;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">

            <main role="main" style="padding:0 !important;">

                <ul class="nav nav-tabs py-4" style="margin-top:15px !important;">
                    <div class="container-fluid px-5 mt-n3">
                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                <h6 class="searchCondLabel">建物名</h6>
                            </div>
                            <div class="col-md-2">
                                <h6 class="searchCondLabel">部屋番号</h6>
                            </div>
                            <div class="col-md-3">
                                <h6 class="searchCondLabel">入居者名</h6>
                            </div>
                            <div class="col-md"></div>

                            <div class="col-md-4 residentInfo">〇〇〇〇マンションA</div>
                            <div class="col-md-2 residentInfo">101</div>
                            <div class="col-md-3 residentInfo">入居者A</div>
                            <div class="col-md"></div>
                        </div>
                    </div>
                </ul>
                <div class="container-fluid px-5 mt-n3">
                    <div class="row justify-content-center mt-4" id="searchConditions">
                        <div class="col-md-2">
                            <h6 class="searchCondLabel">問合せ日<span style="color:red;font-weight: bold;margin-left: 3px;">*</span></h6>
                        </div>
                        <div class="col-md-4">
                            <h6 class="searchCondLabel">問合せ時刻</h6>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-2"><input type="date" name="date" class="form-control" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"></div>
                        <div class="col-md-2"><input type="time" name="time_from" class="form-control" value=""></div>
                        <div class="col-md-2"><input type="time" name="time_to" class="form-control" value=""></div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-clear mr-3">検索条件のクリア</button>
                            <button type="button" class="btn btn-search" style="width:160px;">検索</button>
                        </div>

                    </div>
                    <div class="row justify-content-center mt-4">
                        <table class="table request-list">
                            <tr>
                                <th style="width:15%;">問合せ日時</th>
                                <th style="width:15%;">対応状況</th>
                                <th style="width:70%;">問合せ内容</th>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-0">
                                <td title="2021/02/25 18:12:37">2021/02/25 18:12:37</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="トイレのタンクからの水漏れ">トイレのタンクからの水漏れ</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-1">
                                <td title="2021/02/18 15:17:04">2021/02/18 15:17:04</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンが壊れた">エアコンが壊れた</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-2">
                                <td title="2021/02/12 15:50:44">2021/02/12 15:50:44</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンのルーバーが動かない">エアコンのルーバーが動かない</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-3">
                                <td title="2021/02/12 14:02:29">2021/02/12 14:02:29</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンから風が出ない">エアコンから風が出ない</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-4">
                                <td title="2021/02/12 12:23:53">2021/02/12 12:23:53</td>
                                <td title="業者入札済み">業者入札済み</td>
                                <td title="エアコン本体から異音がする">エアコン本体から異音がする</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-5">
                                <td title="2021/02/12 12:22:53">2021/02/12 12:22:53</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="玄関扉のみ解錠依頼">玄関扉のみ解錠依頼</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-6">
                                <td title="2021/02/12 11:09:01">2021/02/12 11:09:01</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンの室外機から水が漏れている">エアコンの室外機から水が漏れている</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-7">
                                <td title="2021/02/12 10:58:23">2021/02/12 10:58:23</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンのルーバーが動かない">エアコンのルーバーが動かない</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-8">
                                <td title="2021/02/12 10:58:16">2021/02/12 10:58:16</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="玄関の鍵が開かない">玄関の鍵が開かない</td>
                            </tr>
                            <tr data-toggle="modal" data-target="#modal-9">
                                <td title="2021/02/12 10:57:43">2021/02/12 10:57:43</td>
                                <td title="業者対応予定">業者対応予定</td>
                                <td title="エアコンのルーバーが動かない">エアコンのルーバーが動かない</td>
                            </tr>
                        </table>
                    </div>

                    <div class="modal fade" id="modal-0" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/25 18:12:37</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img ">
                                                        <div class="position-relative d-inline-block">
                                                            <img style="height: 340px;"
                                                                src="{{ asset('img/no-image-l.svg') }}">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>トイレのタンクからの水漏れ</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-0">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-0-image-number">1/0</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-0" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="0">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-1" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/18 15:17:04</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img ">
                                                        <div class="position-relative d-inline-block">
                                                            <img style="height: 340px;"
                                                                src="{{ asset('img/no-image-l.svg') }}">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンが壊れた</td>
                                            </tr>
                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-1-image-number">1/0</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-1" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="0">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-2" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 15:50:44</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img-multiple ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-2" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンのルーバーが動かない</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-2">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-2-image-number">1/5</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-2" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="5">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo2.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo3.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo4.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo5.jpg') }}">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls-2" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls-2" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-3" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 14:02:29</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img ">
                                                        <div class="position-relative d-inline-block">
                                                            <img style="height: 340px;"
                                                                src="{{ asset('img/no-image-l.svg') }}">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンから風が出ない</td>
                                            </tr>
                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-3">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-3-image-number">1/0</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-3" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="0">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-4" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 12:23:53</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img ">
                                                        <div class="position-relative d-inline-block">
                                                            <img style="height: 340px;"
                                                                src="{{ asset('img/no-image-l.svg') }}">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者入札済み</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコン本体から異音がする</td>
                                            </tr>
                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-4">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-4-image-number">1/0</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-4" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="0">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-5" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 12:22:53</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-5" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>玄関扉のみ解錠依頼</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-5">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-5-image-number">1/1</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-5" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="1">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-6" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 11:09:01</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img-multiple ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-6" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンの室外機から水が漏れている</td>
                                            </tr>
                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-6">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-6-image-number">1/2</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-6" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="2">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo2.jpg') }}">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls-6" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls-6" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-7" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 10:58:23</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img-multiple ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-7" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンのルーバーが動かない</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-7">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-7-image-number">1/3</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-7" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="3">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo2.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo3.jpg') }}">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls-7" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls-7" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-8" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 10:58:16</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img-multiple ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-8" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>玄関の鍵が開かない</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-8">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-8-image-number">1/4</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-8" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="4">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo2.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo3.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo4.jpg') }}">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls-8" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls-8" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-9" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content">
                                <div class="container">
                                    <div class="justify-content-center">
                                        <div class="col-md-12 mt-5 mb-5 text-center">
                                            <h5 class="modal-title">問合せ詳細</h5>
                                        </div>
                                        <table class="table request-info-table border-bottom">
                                            <tr>
                                                <th style="width:13%;">問合せ日時</th>
                                                <td style="width:37%;">2021/02/12 10:57:43</td>
                                                <td style="width:50%;" rowspan="8">
                                                    <span class="image-movie-title">現場画像・映像</span>
                                                    <div class=" request-info-img-multiple ">
                                                        <div class="position-relative d-inline-block">
                                                            <a href="#lightbox-9" data-toggle="modal">
                                                                <img style="max-height:340px;max-width:100%;"
                                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="2">入居者情報</th>
                                                <td>入居者A</td>
                                            </tr>
                                            <tr>
                                                <td>090-1111-0001</td>
                                            </tr>
                                            <tr>
                                                <th class="align-middle" rowspan="3">物件情報</th>
                                                <td>〇〇〇〇マンションA</td>
                                            </tr>
                                            <tr>
                                                <td>福岡県福岡市〇〇〇〇1-1</td>
                                            </tr>
                                            <tr>
                                                <td>101</td>
                                            </tr>
                                            <tr>
                                                <th>対応状況</th>
                                                <td>業者対応予定</td>
                                            </tr>
                                            <tr>
                                                <th>問合せ内容</th>
                                                <td>エアコンのルーバーが動かない</td>
                                            </tr>

                                        </table>
                                        <button type="button" class="btn btn-slate mb-4"
                                            data-dismiss="modal">閉じる</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- lightbox -->
                    <div class="modal fade and carousel slide" id="lightbox-9">
                        <div class="modal-dialog modal-lg modal-dialog-centered"
                            style="max-width:1150px;height:70%;background-color:transparent;">
                            <div class="modal-content mx-auto"
                                style="border:0;width:100%;height:683px;background-color:#000;">
                                <div class="modal-header" style="border:transparent;">
                                    <div class="modal-title text-center w-100 photo-modal-title">現場画像（<span
                                            id="carouselControls-9-image-number">1/3</span>）</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body p-0">
                                    <div id="carouselControls-9" class="carousel slide h-100" data-ride="carousel"
                                        data-interval="0" total-count="3">
                                        <div class="carousel-inner m-auto h-100" style="width:60%;">
                                            <div class="carousel-item h-100 text-center  active ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo1.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo2.jpg') }}">
                                            </div>
                                            <div class="carousel-item h-100 text-center ">
                                                <img class="carousel-img"
                                                    src="{{ asset('img/mock/photo3.jpg') }}">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls-9" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls-9" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <nav>
                            <ul class="pagination">

                                <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; 戻る">
                                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                                </li>





                                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">2</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">3</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">4</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">5</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">6</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">7</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">8</a></li>

                                <li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span>
                                </li>





                                <li class="page-item"><a class="page-link"
                                        href="#">12</a></li>
                                <li class="page-item"><a class="page-link"
                                        href="#">13</a></li>


                                <li class="page-item">
                                    <a class="page-link" href="#" rel="next"
                                        aria-label="次へ &raquo;">&rsaquo;</a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </div>

                <script>
                    $('.btn-clear').on('click', function () {
                        $('#searchConditions').find('input').val('');
                        $('#searchConditions').find('option').attr("selected", false);
                        $('#searchConditions').find('select').val('');
                    });
                    $('[id^=lightbox-]').on('hidden.bs.modal', function () {
                        $(this).find('video').each(function (i, obj) {
                            $(obj)[0].pause();
                        })
                        $('body').addClass('modal-open');
                    });
                    $('[class^=carousel-control-]').on('click', function () {
                        $(this).parent().find('video').each(function (i, obj) {
                            $(obj)[0].pause();
                        })
                    });
                    $(document).ready(function () {
                        $(document).on('show.bs.modal', '.modal', e => {
                            const $currentModal = $(e.currentTarget);
                            var zIndex = 1040 + (10 * $('.modal:visible').length);
                            $currentModal.css('z-index', zIndex);
                            setTimeout(function () {
                                $('.modal-backdrop')
                                    .not('.modal-stack')
                                    .css('z-index', zIndex - 1)
                                    .addClass('modal-stack');
                            }, 0);
                        });
                        $(document).on('slid.bs.carousel', function (e) {
                            var nextPage = e.to + 1;
                            var totalCnt = $(e.target).attr('total-count');
                            var targetId = $(e.target).attr('id') + '-image-number';
                            $('#' + targetId).html(nextPage + '/' + totalCnt);
                        });
                    });
                </script>

            </main>
        </div>
    </div>
</body>

</html>