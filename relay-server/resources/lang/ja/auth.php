<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '入力されたメールアドレスかパスワードが間違っています。',
    'throttle' => 'ログイン試行が多すぎます。:seconds秒後に再度試してください。',

];
