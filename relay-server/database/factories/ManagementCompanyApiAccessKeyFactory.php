<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ManagementCompanyApiAccessKey;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ManagementCompanyApiAccessKey::class, function (Faker $faker) {
    return [
        'management_company_id' => '1',
        'access_key_id' => 'N9DHO6EYL53S6YZK6HWZ',
        'secret_key' => 'q3bm8qbjme3pwfhrihb6zvv5xiqkjiicv5hgvehg',
    ];
});
