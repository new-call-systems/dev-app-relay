<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestPreferableTime;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequestPreferableTime::class, function (Faker $faker) {
    return [
        'request_id' => '12345678901',
        'priority' => '1',
        'date' => Carbon::now()->format('Y/m/d'),
        'time_section_id' => '1',
    ];
});
