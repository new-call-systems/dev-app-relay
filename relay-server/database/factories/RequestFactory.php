<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Request;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Request::class, function (Faker $faker) {
    return [
        'resident_id' => 'RD_6_9999999999',
        'management_company_id' => 'MC_fjw89f3jekhf',
        'visibility' => 'public',
        'status' => 'open',
        'request_type' => 'vendor',
        'ai_category' => '1',
        'ai_sub_category' => '1-1',
        'emergency_flg' => 0,
        'summary' => null,
        'rating' => null,
    ];
});
