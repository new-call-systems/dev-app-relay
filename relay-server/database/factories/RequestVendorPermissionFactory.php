<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestVendorPermission;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequestVendorPermission::class, function (Faker $faker) {
    return [
        'request_id' => '12345678901',
        'vendor_id' => '1',
    ];
});
