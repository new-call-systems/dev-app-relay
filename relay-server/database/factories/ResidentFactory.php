<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Resident;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Resident::class, function (Faker $faker) {
    return [
        'management_company_id' => '1',
        'room_id' => '1',
        'resident_code' => $faker->regexify('[0-9]{10}'),
        'name' => $faker->name,
        'tel' => '012-345-6789',
        'email' => $faker->unique()->safeEmail,
        'line_user_id' => $faker->regexify('[A-Za-z0-9]{20}'),
    ];
});
