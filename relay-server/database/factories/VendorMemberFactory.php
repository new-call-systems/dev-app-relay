<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\VendorMember;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(VendorMember::class, function (Faker $faker) {
    return [
        'user_id' => '1',
        'vendor_id' => 'VN_fjw89f3jekhf',
        'last_name' => 'factory姓',
        'first_name' => 'factory名',
        'emergency_tel' => '080-1234-5678',
        'retire_flg' => null,
        'representative_flg' => null,
    ];
});
