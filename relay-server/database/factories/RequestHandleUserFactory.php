<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestHandleUser;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequestHandleUser::class, function (Faker $faker) {
    return [
        'request_id' => '12345678901',
        'user_id' => '1',
        'group_id' => 'VN_123456789012',
    ];
});
