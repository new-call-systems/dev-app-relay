<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vendor;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => 'factory業者名',
        'address' => 'factory住所',
        'tel' => '012-345-6789',
        'fax' => '012-345-6780',
        'emergency_tel' => '012-345-6789',
        'branch_name' => 'factory支店',
    ];
});
