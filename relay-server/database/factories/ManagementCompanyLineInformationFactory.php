<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ManagementCompanyLineInformation;
use Faker\Generator as Faker;

$factory->define(ManagementCompanyLineInformation::class, function (Faker $faker) {
    return [
        'management_company_id' => '',
        'channel_id' => $faker->regexify('[A-Za-z0-9]{20}'),
        'channel_secret' => $faker->regexify('[A-Za-z0-9]{20}'),
        'channel_access_token' => $faker->regexify('[A-Za-z0-9]{20}'),
        'liff_id' => $faker->regexify('[A-Za-z0-9]{20}'),
    ];
});
