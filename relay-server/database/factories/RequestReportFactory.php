<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestReport;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequestReport::class, function (Faker $faker) {
    $dt = Carbon::now();
    return [
        'request_id' => '1',
        'author_id' => '1',
        'vendor_id' => '1',
        'management_company_id' => '1',
        'resident_id' => '1',
        'reception_date' => $dt->subMonth()->format('Y-m-d'),
        'report_status' => config('const.Reports.Status.WRITING'),
        'action_status' => config('const.Reports.ActionStatus.COMPLETED'),
        'description' => '工事内容',
        'action_date' => $dt->format('Y-m-d'),
        'starting_time' => '10:00',
        'ending_time' => '10:30',
        'comment' => null,
        'action_section' => '1',
        'construction_amount' => null,
    ];
});
