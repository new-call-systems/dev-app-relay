<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestHandle;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequestHandle::class, function (Faker $faker) {
    return [
        'request_id' => '12345678901',
        'vendor_id' => 'VN_123456789012',
        'scheduled_time' => '2020/12/31 09:10:00',
        'worked_minute' => '40',
        'description' => '説明',
    ];
});
