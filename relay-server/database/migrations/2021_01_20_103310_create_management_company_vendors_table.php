<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementCompanyVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_company_vendors', function (Blueprint $table) {
            $table->string('management_company_id', 15);
            $table->string('vendor_id', 15);
            $table->string('core_system_vendor_code', 255);
            $table->timestamps();

            $table->primary(['management_company_id', 'core_system_vendor_code'], 'mc_vendors_mcid_core_system_vendor_code_primary');
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_company_vendors');
    }
}
