<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnVendorsTable extends Migration
{
    /**
     * Run the migrations.
     * Out_業者.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_vendors', function (Blueprint $table) {
            $table->id();
            $table->string('vendor_name',40)->nullable()->comment("名称");
            $table->string('tel_number', 20)->nullable()->comment("電話");
            $table->string('post_number', 8)->nullable()->comment("郵便番号");
            $table->string('address', 100)->nullable()->comment("住所");
            $table->string('business_hours', 255)->nullable()->comment("営業時間");
            $table->string('emergency_tel_number1', 20)->nullable()->comment("緊急連絡先1");
            $table->string('emergency_tel_number2', 20)->nullable()->comment("緊急連絡先2");
            $table->string('vendor_code', 20)->nullable()->comment("業者コード");
            $table->string('emergency_tel_number3', 20)->nullable()->comment("緊急連絡先3");
            $table->string('vendor_kbn', 20)->nullable()->comment("業者区分");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_vendors');
    }
}
