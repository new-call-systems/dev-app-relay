<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->string('resident_id', 255);
            $table->unsignedBigInteger('management_company_id');
            $table->string('room_id', 255);
            $table->string('resident_code');
            $table->string('name');
            $table->string('tel');
            $table->string('access_token')->nullable();
            $table->timestamp('token_expire', 0)->nullable();
            $table->string('refresh_token')->nullable();
            $table->timestamps();

            $table->primary('resident_id');

            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
            $table->foreign('room_id')->references('room_id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
