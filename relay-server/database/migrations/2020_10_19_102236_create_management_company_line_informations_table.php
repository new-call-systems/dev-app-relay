<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementCompanyLineInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_company_line_informations', function (Blueprint $table) {
            $table->string('management_company_id', 15);
            $table->string('channel_id', 255);
            $table->string('channel_secret', 255);
            $table->string('channel_access_token', 255);
            $table->string('liff_id', 255);
            $table->timestamps();

            // $table->foreign('management_company_id')->references('management_company_id')->on('management_companies')->name('management_company_line_informations_mcid_foreign');
            $table->foreign('management_company_id', 'management_company_line_informations_mcid_foreign')->references('management_company_id')->on('management_companies');

            // $table->primary('management_company_id')->name('management_company_line_informations_mcid_primary');
            $table->primary('management_company_id', 'management_company_line_informations_mcid_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_company_line_informations');
    }
}
