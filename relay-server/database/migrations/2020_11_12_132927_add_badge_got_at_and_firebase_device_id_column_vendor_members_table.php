<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBadgeGotAtAndFirebaseDeviceIdColumnVendorMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->timestamp('badge_got_at', 0)->default(\DB::raw('CURRENT_TIMESTAMP'))->after('representative_flg');
            $table->string('firebase_device_id', 255)->nullable()->after('badge_got_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->dropColumn('badge_got_at');
            $table->dropColumn('firebase_device_id');
        });
    }
}
