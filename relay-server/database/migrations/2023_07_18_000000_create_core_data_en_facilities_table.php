<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     * Out_設備マスタ.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_facilities', function (Blueprint $table) {
            $table->id();
            $table->string('housing_cd',20)->nullable()->comment("住設CD");
            $table->string('pamphlet_name', 255)->nullable()->comment("パンフ名称");
            $table->string('housing_name', 255)->nullable()->comment("住設名称");
            $table->string('housing_name1', 255)->nullable()->comment("住設名称1");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_facilities');
    }
}
