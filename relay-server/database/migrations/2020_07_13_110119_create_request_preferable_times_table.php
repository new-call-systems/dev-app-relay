<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestPreferableTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_preferable_times', function (Blueprint $table) {
            $table->bigIncrements('request_preferable_time_id');
            $table->unsignedBigInteger('request_id');
            $table->integer('priority');
            $table->date('date');
            $table->unsignedBigInteger('time_section_id');
            $table->boolean('selected')->nullable();
            $table->timestamps();

            $table->foreign('request_id')->references('request_id')->on('requests');
            $table->foreign('time_section_id')->references('time_section_id')->on('time_sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_preferable_times');
    }
}
