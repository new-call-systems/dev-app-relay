<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->string('property_id', 255);
            $table->unsignedBigInteger('management_company_id');
            $table->string('property_code');
            $table->string('name');
            $table->string('address');
            $table->timestamps();

            $table->primary('property_id');

            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
