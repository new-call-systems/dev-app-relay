<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentInfosToVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            Schema::table('vendors', function (Blueprint $table) {
                $table->string('parent_address')->nullable()->after('branch_name');
                $table->string('parent_tel', 13)->nullable()->after('parent_address');
                $table->string('parent_fax', 13)->nullable()->after('parent_tel');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->dropColumn('parent_address');
            $table->dropColumn('parent_tel');
            $table->dropColumn('parent_fax');
        });
    }
}
