<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTokenInfoToVendorMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->string('access_token')->nullable()->after('representative_flg');
            $table->timestamp('token_expire', 0)->nullable()->after('access_token');
            $table->string('refresh_token')->nullable()->after('token_expire');
            $table->timestamp('license_expire', 0)->nullable()->after('refresh_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->dropColumn('access_token');
            $table->dropColumn('token_expire');
            $table->dropColumn('refresh_token');
            $table->dropColumn('license_expire');
        });
    }
}
