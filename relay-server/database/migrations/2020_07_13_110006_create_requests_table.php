<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->unsignedBigInteger('request_id');
            $table->string('resident_id', 255);
            $table->string('management_company_id', 15);
            $table->string('visibility', 255);
            $table->string('status', 255);
            $table->string('request_type', 255);
            $table->string('ai_category', 255);
            $table->string('ai_sub_category', 255);
            $table->boolean('emergency_flg');
            $table->text('summary')->nullable();
            $table->integer('rating')->nullable();
            $table->timestamps();

            $table->primary('request_id');

            $table->foreign('resident_id')->references('resident_id')->on('residents');
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
