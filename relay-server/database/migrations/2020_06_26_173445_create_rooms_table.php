<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->string('room_id', 255);
            $table->unsignedBigInteger('management_company_id');
            $table->string('property_id', 255);
            $table->string('room_code');
            $table->string('room_no');
            $table->timestamps();

            $table->primary('room_id');

            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
            $table->foreign('property_id')->references('property_id')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
