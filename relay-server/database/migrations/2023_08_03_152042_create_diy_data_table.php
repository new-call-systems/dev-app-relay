<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiyDataTable extends Migration
{
    public function up()
    {
        Schema::create('diy_data', function (Blueprint $table) {
            $table->string('S_CAT')->nullable();
            $table->string('TYPE')->nullable();
            $table->string('KEYWORD')->nullable();
            $table->text('STATE')->nullable();
            $table->string('TARGET')->nullable();
            $table->string('DIY_ID')->nullable();
            $table->string('AID')->unique();
            $table->string('SPECIAL')->nullable();
            $table->text('SENTENCE')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('diy_data');
    }
}
