<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_feedbacks', function (Blueprint $table) {
            $table->bigIncrements('request_feedback_id');
            $table->unsignedBigInteger('request_id');
            $table->string('resident_id', 255);
            $table->text('feedback_content');
            $table->timestamps();

            $table->foreign('request_id')->references('request_id')->on('requests');
            $table->foreign('resident_id')->references('resident_id')->on('residents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_feedbacks');
    }
}
