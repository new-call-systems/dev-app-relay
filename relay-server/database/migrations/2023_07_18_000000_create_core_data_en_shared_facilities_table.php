<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnSharedFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     * Out_共用設備.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_shared_facilities', function (Blueprint $table) {
            $table->id();
            $table->integer('property_code')->nullable()->comment("物件コード");
            $table->string('cleaning_code', 20)->nullable()->comment("清掃.会社コード");
            $table->string('guard_code', 20)->nullable()->comment("警備.会社コード");
            $table->string('fire_check_code', 20)->nullable()->comment("消防設備点検.会社コード");
            $table->string('parking_code', 20)->nullable()->comment("立体駐車場.会社コード");
            $table->string('delivery_box_code', 20)->nullable()->comment("宅配BOX.会社コード");
            $table->string('auto_lock_code', 20)->nullable()->comment("自動ドア保守.会社コード");
            $table->string('pump_code', 20)->nullable()->comment("ポンプ保守会社.会社コード");
            $table->string('elevator_code', 20)->nullable()->comment("エレベーター.会社コード");
            $table->string('security_camera_watching_code', 20)->comment("監視カメラ遠隔監視.会社コード");
            $table->string('construction_code', 20)->nullable()->comment("建設会社.会社コード");
            $table->string('construction_field_code', 20)->nullable()->comment("建築会社（現場担当）.会社コード");
            $table->string('plumbing_system_code', 20)->nullable()->comment("給排水設備.会社コード");
            $table->string('electrical_facilities_code', 20)->comment("電気設備.会社コード");
            $table->string('electric_code', 20)->nullable()->comment("電力.会社コード");
            $table->string('water_line_code', 20)->nullable()->comment("'水道.会社コード");
            $table->string('wrecker_code', 20)->nullable()->comment("レッカー移動.会社コード");
            $table->string('cleaning_emergency_name', 255)->nullable()->comment("清掃.緊急担当");
            $table->string('guard_emergency_name', 255)->nullable()->comment("警備.緊急担当");
            $table->string('fire_check_emergency_name', 255)->nullable()->comment("消防設備点検.緊急担当");
            $table->string('parking_emergency_name', 255)->nullable()->comment("立体駐車場.緊急担当");
            $table->string('delivery_box_emergency_name', 255)->nullable()->comment("宅配BOX.緊急担当");
            $table->string('auto_lock_emergency_name', 255)->nullable()->comment("自動ドア保守.緊急担当");
            $table->string('pump_emergency_name', 255)->nullable()->comment("ポンプ保守会社.緊急担当");
            $table->string('elevator_emergency_name', 255)->nullable()->comment("エレベーター.緊急担当");
            $table->string('security_camera_emergency_name', 255)->nullable()->comment("監視カメラ遠隔監視.緊急担当");
            $table->string('construction_emergency_name', 255)->nullable()->comment("建築会社.緊急担当");
            $table->string('construction_field_emergency_name', 255)->nullable()->comment("建築会社（現場担当）.緊急担当");
            $table->string('plumbing_system_emergency_name', 255)->nullable()->comment("給排水設備.緊急担当");
            $table->string('electrical_facilities_emergency_name', 255)->nullable()->comment("電気設備.緊急担当");
            $table->string('electric_emergency_name', 255)->nullable()->comment("電力.緊急担当");
            $table->string('water_line_emergency_name', 255)->nullable()->comment("水道.緊急担当");
            $table->string('wrecker_emergency_name', 255)->nullable()->comment("レッカー移動.緊急担当");
            $table->string('cleaning_name', 255)->nullable()->comment("清掃.担当者");
            $table->string('guard_name', 255)->nullable()->comment("警備.担当者");
            $table->string('fire_check_name', 255)->nullable()->comment("消防設備点検.担当者");
            $table->string('parking_name', 255)->nullable()->comment("立体駐車場.担当者");
            $table->string('delivery_box_name', 255)->nullable()->comment("宅配BOX.緊急担当");
            $table->string('auto_lock_name', 255)->nullable()->comment("自動ドア保守.緊急担当");
            $table->string('pump_name', 255)->nullable()->comment("ポンプ保守会社.緊急担当");
            $table->string('elevator_name', 255)->nullable()->comment("エレベーター.担当者");
            $table->string('security_camera_name', 255)->nullable()->comment("監視カメラ遠隔監視.担当者");
            $table->string('construction_name', 255)->nullable()->comment("建築会社.担当者");
            $table->string('construction_field_name', 255)->nullable()->comment("建築会社（現場担当）.担当者");
            $table->string('plumbing_system_name', 255)->nullable()->comment("給排水設備.担当者");
            $table->string('electrical_facilities_name', 255)->nullable()->comment("電気設備.担当者");
            $table->string('electric_name', 255)->nullable()->comment("電力.担当者");
            $table->string('water_line_name', 255)->nullable()->comment("水道.担当者");
            $table->string('wrecker_name', 255)->nullable()->comment("レッカー移動.担当者");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *->nullable()
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_shared_facilities');
    }
}
