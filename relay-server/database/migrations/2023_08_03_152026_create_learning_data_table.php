<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearningDataTable extends Migration
{
    public function up()
    {
        Schema::create('learning_data', function (Blueprint $table) {
            $table->string('csp_code')->nullable();
            $table->string('sentence')->unique();
            $table->text('text')->nullable();
            $table->string('s_cat')->nullable();
            $table->string('type')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('learning_data');
    }
}
