<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorWorkSupportCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_work_support_categories', function (Blueprint $table) {
            $table->string('vendor_id', 15);
            $table->unsignedBigInteger('work_support_category_id');
            $table->timestamps();

            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
            $table->foreign('work_support_category_id')->references('work_support_category_id')->on('work_support_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_work_support_categories');
    }
}
