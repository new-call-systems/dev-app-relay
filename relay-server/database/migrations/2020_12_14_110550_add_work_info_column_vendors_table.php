<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWorkInfoColumnVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('work_area', 255)->nullable()->after('parent_fax');
            $table->time('work_time_from', 0)->nullable()->after('work_area');
            $table->time('work_time_to', 0)->nullable()->after('work_time_from');
            $table->string('work_items', 1024)->nullable()->after('work_time_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->dropColumn('work_area');
            $table->dropColumn('work_time_from');
            $table->dropColumn('work_time_to');
            $table->dropColumn('work_items');
        });
    }
}
