<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeManagementCompaniesTableColumnId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('management_company_members', function (Blueprint $table) {
            $table->dropForeign('management_company_members_management_company_id_foreign');
        });
        Schema::table('management_company_api_access_keys', function (Blueprint $table) {
            $table->dropForeign('management_company_api_access_keys_management_company_id_foreign');
        });
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_management_company_id_foreign');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign('rooms_management_company_id_foreign');
        });
        Schema::table('residents', function (Blueprint $table) {
            $table->dropForeign('residents_management_company_id_foreign');
        });
        Schema::table('management_companies', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->string('support_tel', 13)->nullable()->after('fax');
        });
        Schema::table('management_company_members', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
        Schema::table('management_company_api_access_keys', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
        Schema::table('properties', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
        Schema::table('residents', function (Blueprint $table) {
            $table->string('management_company_id', 15)->change();
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        // Schema::table('management_company_members', function (Blueprint $table) {
        //     $table->dropForeign('management_company_members_management_company_id_foreign');
        // });
        // Schema::table('management_company_api_access_keys', function (Blueprint $table) {
        //     $table->dropForeign('management_company_api_access_keys_management_company_id_foreign');
        // });
        // Schema::table('properties', function (Blueprint $table) {
        //     $table->dropForeign('properties_management_company_id_foreign');
        // });
        // Schema::table('rooms', function (Blueprint $table) {
        //     $table->dropForeign('rooms_management_company_id_foreign');
        // });
        // Schema::table('residents', function (Blueprint $table) {
        //     $table->dropForeign('residents_management_company_id_foreign');
        // });
        // Schema::table('management_companies', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->dropColumn('support_tel');
        // });
        // Schema::table('management_company_members', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        // });
        // Schema::table('management_company_api_access_keys', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        // });
        // Schema::table('properties', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        // });
        // Schema::table('rooms', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        // });
        // Schema::table('residents', function (Blueprint $table) {
        //     $table->unsignedBigInteger('management_company_id')->change();
        //     $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
        // });

    }
}
