<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTokenInfoFromVendorMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->dropColumn('api_token');
            $table->dropColumn('refresh_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->string('api_token')->nullable()->after('emergency_tel');
            $table->string('refresh_token')->nullable()->after('api_token');
        });
    }
}
