<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestVendorPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_vendor_permissions', function (Blueprint $table) {
            $table->unsignedBigInteger('request_id');
            $table->string('vendor_id', 15);
            $table->timestamps();

            $table->primary(['request_id', 'vendor_id']);

            $table->foreign('request_id')->references('request_id')->on('requests');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_vendor_permissions');
    }
}
