<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQaDataTable extends Migration
{
    public function up()
    {
        Schema::create('qa_data', function (Blueprint $table) {
            $table->string('S_CAT')->nullable();
            $table->string('TYPE')->nullable();
            $table->text('SENTENCE')->nullable();
            $table->string('AID')->unique();
            $table->text('ANSWER_TEXT')->nullable();
            $table->json('ACTION')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('qa_data');
    }
}
