<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRequestReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_reports', function (Blueprint $table) {
            $table->string('action_status', 255)->after('status');
            $table->string('action_section', 1)->after('comment');
            $table->string('construction_amount', 255)->nullable()->after('action_section');
            $table->renameColumn('status', 'report_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_reports', function (Blueprint $table) {
            $table->dropColumn('action_status');
            $table->dropColumn('action_section');
            $table->dropColumn('construction_amount');
            $table->renameColumn('report_status', 'status');
        });
    }
}
