<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_reports', function (Blueprint $table) {
            $table->bigIncrements('request_report_id');
            $table->unsignedBigInteger('request_id')->nullable();
            $table->unsignedBigInteger('author_id');
            $table->string('vendor_id', 15)->nullable();
            $table->string('management_company_id', 15);
            $table->string('resident_id', 255);
            $table->string('status', 255);
            $table->text('description');
            $table->date('action_date');
            $table->time('starting_time', 0)->nullable();
            $table->time('ending_time', 0)->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();

            $table->foreign('request_id')->references('request_id')->on('requests');
            $table->foreign('author_id')->references('user_id')->on('users');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
            $table->foreign('resident_id')->references('resident_id')->on('residents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_reports');
    }
}
