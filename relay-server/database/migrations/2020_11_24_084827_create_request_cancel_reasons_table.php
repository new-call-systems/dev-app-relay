<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestCancelReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_cancel_reasons', function (Blueprint $table) {
            $table->unsignedBigInteger('request_id');
            $table->text('content');
            $table->timestamps();

            $table->primary('request_id');
            $table->foreign('request_id')->references('request_id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_cancel_reasons');
    }
}
