<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqDiyDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_diy_datas', function (Blueprint $table) {
            $table->string('faq_diy_id', 20);
            $table->string('management_company_id', 15);
            $table->integer('resident_type');
            $table->text('text');
            $table->string('extra_table', 255)->nullable();
            $table->string('extra_column', 255)->nullable();
            $table->text('template')->nullable();
            $table->string('alternate_id', 20)->nullable();
            $table->timestamps();

            $table->primary(['faq_diy_id', 'management_company_id', 'resident_type'], 'faq_diy_datas_fdid_mcid_rdtype_primary');
            $table->foreign('management_company_id', 'faq_diy_datas_mcid_foreign')->references('management_company_id')->on('management_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_diy_datas');
    }
}
