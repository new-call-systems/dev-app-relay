<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestHandlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_handles', function (Blueprint $table) {
            $table->bigIncrements('request_handle_id');
            $table->unsignedBigInteger('request_id');
            $table->string('vendor_id', 15);
            $table->timestamp('scheduled_time', 0)->nullable();
            $table->integer('worked_minute')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('request_id')->references('request_id')->on('requests');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_handles');
    }
}
