<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnResidentsTable extends Migration
{
    /**
     * Run the migrations.
     * Out_入居者.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_residents', function (Blueprint $table) {
            $table->id();
            $table->integer('property_code')->nullable()->comment("物件コード");
            $table->integer('room_code')->nullable()->comment("部屋コード");
            $table->string('key_unlock_service', 20)->nullable()->comment("鍵解除サービス");
            $table->dateTime('contract_date')->nullable()->comment("契約日付");
            $table->dateTime('move_in_date')->nullable()->comment("入居日付");
            $table->dateTime('move_out_date')->nullable()->comment("退去日付");
            $table->string('vendor_code', 20)->nullable()->comment("業者コード");
            $table->dateTime('move_out_declaration_date')->nullable()->comment("退去申出日");
            $table->string('contractor_code', 255)->nullable()->comment("契約者コード");
            $table->string('resident_name', 50)->nullable()->comment("入居者名");
            $table->string('cohabitant', 255)->nullable()->comment("同居人");
            $table->string('guarantor', 255)->nullable()->comment("保証人");
            $table->string('contractor', 50)->nullable()->comment("契約者");
            $table->string('guarantor_tel', 20)->nullable()->comment("保証人TEL");
            $table->string('guarantor_address', 100)->nullable()->comment("保証人住所");
            $table->string('contractor_tel', 20)->nullable()->comment("契約者TEL");
            $table->string('contractor_address', 100)->nullable()->comment("契約者住所");
            $table->string('en_club_id', 10)->nullable()->comment("えんくらぶID");
            $table->string('resident_tel', 20)->nullable()->comment("入居者TEL");
            $table->string('resident_mobile', 20)->nullable()->comment("入居者携帯番号");
            $table->dateTime('resident_birthday')->nullable()->comment("入居者生年月日");
            $table->integer('resident_code')->nullable()->comment("入居者コード");
            $table->string('resident_gender', 10)->nullable()->comment("入居者性別名");
            $table->string('vehicle_info', 255)->nullable()->comment("車両情報");
            $table->string('parking_code', 255)->nullable()->comment("駐車場コード");
            $table->dateTime('parking_contract_date')->nullable()->comment("駐車場契約日");
            $table->dateTime('parking_cancellation_date')->nullable()->comment("駐車場解約日");
            $table->dateTime('move_out_appointment_date')->nullable()->comment("退居立会予定日時");
            $table->string('parking_contractor_name', 255)->nullable()->comment("駐車場契約者名");
            $table->string('parking_user_name', 255)->nullable()->comment("駐車場使用者名");
            $table->string('parking_contractor_phone', 20)->nullable()->comment("駐車場契約者電話番号");
            $table->string('parking_contractor_mobile', 20)->nullable()->comment("駐車場契約者携帯番号");
            $table->string('parking_user_phone', 20)->nullable()->comment("駐車場使用者電話番号");
            $table->string('parking_user_mobile', 20)->nullable()->comment("駐車場使用者携帯電話");
            $table->string('resident_name_kana', 50)->nullable()->comment("入居者名カナ");
            $table->string('contractor_name_kana', 50)->nullable()->comment("契約者名カナ");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_residents');
    }
}
