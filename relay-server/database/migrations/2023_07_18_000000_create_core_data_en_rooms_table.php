<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnRoomsTable extends Migration
{
    /**
     * Run the migrations.
     * Out_部屋.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_rooms', function (Blueprint $table) {
            $table->id();
            $table->integer('room_code')->nullable()->comment("部屋コード");
            $table->integer('property_code')->nullable()->comment("物件コード");
            $table->integer('support_code')->nullable()->comment("えんサポートコード");
            $table->text('special_note')->nullable()->comment("備考特記");
            $table->string('parcel_locker_password', 30)->nullable()->comment("宅配ボックス暗証番号");
            $table->string('layout', 255)->nullable()->comment("間取");
            $table->integer('owner_code')->nullable()->comment("家主コード");
            $table->string('management_type', 10)->nullable()->comment("管理区分");
            $table->string('room_management_agency_code', 20)->nullable()->comment("部屋管理業者コード");
            $table->string('ssid', 50)->nullable()->comment("SSID");
            $table->string('ssid2', 50)->nullable()->comment("SSID2");
            $table->string('encryption_key', 64)->nullable()->comment("暗号化キー");
            $table->string('electricity_company_code', 20)->nullable()->comment("電力会社コード");
            $table->string('water_company_code', 20)->nullable()->comment("水道会社コード");
            $table->string('electricity_company_name', 100)->nullable()->comment("電力会社名");
            $table->string('water_company_name', 100)->nullable()->comment("水道会社名");
            $table->string('gas_company_name', 100)->nullable()->comment("ガス会社名");
            $table->string('gas_company_contact', 40)->nullable()->comment("ガス会社連絡先");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_rooms');
    }
}
