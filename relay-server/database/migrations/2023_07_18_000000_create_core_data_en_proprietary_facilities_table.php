<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnProprietaryFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     * Out_専有設備.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_proprietary_facilities', function (Blueprint $table) {
            $table->id();
            $table->string('key_code')->nullable()->comment('鍵コード');
            $table->string('key_type')->nullable()->comment('鍵_種類（略名）');
            $table->string('warm_water_toilet_model')->nullable()->comment('温水洗浄便座 品番');
            $table->string('ac_model')->nullable()->comment('エアコン 品番');
            $table->string('gas_water_heater_model')->nullable()->comment('ガス給湯器 品番');
            $table->string('room_code')->nullable()->comment('部屋コード');
            $table->string('property_code')->nullable()->comment('物件コード');
            $table->string('warm_water_toilet_model_1')->nullable()->comment('温水洗浄便座 品番_1');
            $table->string('ac_model_2')->nullable()->comment('エアコン 品番_2');
            $table->string('gas_water_heater_model_3')->nullable()->comment('ガス給湯器 品番_3');
            $table->string('cooking_equipment_model')->nullable()->comment('調理器具 品番');
            $table->string('intercom_entrance_set_model')->nullable()->comment('インターホン玄関子機 品番');
            $table->string('bath_drying_unit')->nullable()->comment('バス乾燥機ﾕﾆｯﾄ');
            $table->string('bath_faucet_model')->nullable()->comment('浴室水栓 品番');
            $table->string('vanity_model')->nullable()->comment('洗面化粧台 品番');
            $table->string('toilet_tank_model')->nullable()->comment('トイレ 便器 タンク 品番');
            $table->string('tv_reception_equipment')->nullable()->comment('TV受信設備');
            $table->string('satellite_broadcast')->nullable()->comment('衛星放送（BS/CS）');
            $table->string('free_internet')->nullable()->comment('インターネット無料');
            $table->string('kitchen_faucet_model')->nullable()->comment('キッチン水栓 品番');
            $table->string('home_delivery_manual')->nullable()->comment('宅配 取扱説明書');
            $table->string('pet_breeding')->nullable()->comment('ペット飼育【ホ】');
            $table->string('cooking_equipment_model_4')->nullable()->comment('調理器具 品番_4');
            $table->string('intercom_entrance_set_model_5')->nullable()->comment('インターホン玄関子機 品番_5');
            $table->string('bath_drying_unit_6')->nullable()->comment('バス乾燥機ﾕﾆｯﾄ_6');
            $table->string('bath_faucet_model_7')->nullable()->comment('浴室水栓 品番_7');
            $table->string('vanity_model_8')->nullable()->comment('洗面化粧台 品番_8');
            $table->string('toilet_tank_model_9')->nullable()->comment('トイレ 便器 タンク 品番_9');
            $table->string('tv_reception_equipment_10')->nullable()->comment('TV受信設備_10');
            $table->string('satellite_broadcast_11')->nullable()->comment('衛星放送（BS/CS）_11');
            $table->string('free_internet_12')->nullable()->comment('インターネット無料_12');
            $table->string('kitchen_faucet_model_13')->nullable()->comment('キッチン水栓 品番_13');
            $table->string('home_delivery_manual_14')->nullable()->comment('宅配 取扱説明書_14');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_proprietary_facilities');
    }
}
