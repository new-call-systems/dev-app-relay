<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_members', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->string('vendor_id', 15);
            $table->string('last_name');
            $table->string('first_name');
            $table->string('emergency_tel', 13);
            $table->string('refresh_token')->nullable();
            $table->integer('retire_flg')->nullable();
            $table->integer('representative_flg')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_members');
    }
}
