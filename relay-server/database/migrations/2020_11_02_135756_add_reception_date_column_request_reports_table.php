<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceptionDateColumnRequestReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_reports', function (Blueprint $table) {
            $table->date('reception_date')->default('2020-01-01')->after('resident_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_reports', function (Blueprint $table) {
            $table->dropColumn('reception_date');
        });
    }
}
