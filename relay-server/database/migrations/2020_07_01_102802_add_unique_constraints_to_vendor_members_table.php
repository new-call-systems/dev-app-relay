<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueConstraintsToVendorMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->unique('access_token');
            $table->unique('refresh_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_members', function (Blueprint $table) {
            $table->dropUnique('vendor_members_access_token_unique');
            $table->dropUnique('vendor_members_refresh_token_unique');
        });
    }
}
