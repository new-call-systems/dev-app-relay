<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementCompanyApiAccessKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_company_api_access_keys', function (Blueprint $table) {
            $table->unsignedBigInteger('management_company_id');
            $table->string('access_key_id');
            $table->string('secret_key');
            $table->timestamps();

            $table->foreign('management_company_id')->references('management_company_id')->on('management_companies');
            $table->primary('management_company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_company_api_access_keys');
    }
}
