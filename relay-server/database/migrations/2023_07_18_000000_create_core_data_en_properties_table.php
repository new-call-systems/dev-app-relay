<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     * Out_物件.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_properties', function (Blueprint $table) {
            $table->id();
            $table->string('property_name', 40)->nullable()->comment("物件名");
            $table->integer('property_code')->nullable()->comment("物件コード");
            $table->dateTime('completion_date')->nullable()->comment("竣工日");
            $table->string('management_company_code', 20)->nullable()->comment("管理会社コード");
            $table->string('company_name', 40)->nullable()->comment("会社名");
            $table->string('sticker_management_company_code', 20)->nullable()->comment("ステッカー管理会社コード");
            $table->string('sticker_management_company_tel', 20)->nullable()->comment("ステッカー管理会社TEL");
            $table->string('parking_management_company_code', 20)->nullable()->comment("駐車場管理会社コード");
            $table->string('parking_management_company_tel', 20)->nullable()->comment("駐車場管理会社TEL");
            $table->string('landmark', 255)->nullable()->comment("周辺目印");
            $table->string('management_room_tel', 20)->nullable()->comment("管理室TEL");
            $table->string('work_day', 50)->nullable()->comment("出勤日");
            $table->string('work_time', 50)->nullable()->comment("出勤時間");
            $table->string('manager_name', 50)->nullable()->comment("管理人名");
            $table->string('manager_tel', 20)->nullable()->comment("管理員TEL");
            $table->text('note')->nullable()->comment("備考");
            $table->string('address', 200)->nullable()->comment("所在地_住所１");
            $table->string('large_bike_contract_management_company_code', 20)->nullable()->comment("大型バイク契約管理会社コード");
            $table->string('large_bike_contract_company_tel', 20)->nullable()->comment("大型バイク契約会社TEL");
            $table->string('company_code', 10)->nullable()->comment("管理会社コード");
            $table->string('property_kana', 50)->nullable()->comment("物件カナ");
            $table->string('internet_general_guidance_code', 20)->nullable()->comment("インターネット総合案内コード");
            $table->string('construction_company_code', 20)->nullable()->comment("建築会社コード");
            $table->string('electric_equipment_code', 20)->nullable()->comment("電気設備コード");
            $table->string('water_supply_and_drainage_facility_code', 20)->nullable()->comment("給排水設備コード");
            $table->string('internet_general_guidance', 100)->nullable()->comment("インターネット総合案内");
            $table->string('construction_company', 100)->nullable()->comment("建築会社");
            $table->string('electric_equipment', 100)->nullable()->comment("電気設備");
            $table->string('water_supply_and_drainage_facility', 100)->nullable()->comment("給排水設備");
            $table->string('nearest_police_management_company_code', 20)->nullable()->comment("最寄警察管理会社コード");
            $table->string('nearest_police_management_contact', 20)->nullable()->comment("最寄り警察管理連絡先");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_properties');
    }
}
