<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_contents', function (Blueprint $table) {
            $table->bigIncrements('request_content_id');
            $table->unsignedBigInteger('request_id');
            $table->text('context');
            $table->text('response');
            $table->timestamps();

            $table->foreign('request_id')->references('request_id')->on('requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_contents');
    }
}
