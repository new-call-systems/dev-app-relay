<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProcessIdColumnFaqDiyDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faq_diy_datas', function (Blueprint $table) {
            $table->string('process_id', 255)->nullable()->after('alternate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faq_diy_datas', function (Blueprint $table) {
            $table->dropColumn('process_id');
        });
    }
}
