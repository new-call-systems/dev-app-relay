<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoginInfoColumnResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('residents', function (Blueprint $table) {
            $table->string('email', 255)->after('tel');
            $table->unsignedBigInteger('user_id')->nullable()->after('email');
            $table->boolean('system_unavailable_flg')->nullable()->after('refresh_token');

            $table->foreign('user_id')->references('user_id')->on('users');
            $table->unique('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('residents', function (Blueprint $table) {
            $table->dropForeign('residents_user_id_foreign');
            $table->dropUnique('residents_user_id_unique');
            $table->dropColumn('email');
            $table->dropColumn('user_id');
            $table->dropColumn('system_unavailable_flg');
        });
    }
}
