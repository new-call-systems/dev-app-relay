<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDataEnParkingsTable extends Migration
{
    /**
     * Run the migrations.
     * Out_駐車場マスタ.xlsxを元に作成
     * @return void
     */
    public function up()
    {
        Schema::create('core_data_en_parkings', function (Blueprint $table) {
            $table->id();
            $table->string('parking_code', 255)->nullable()->comment('駐車場CD');
            $table->integer('property_code')->nullable()->comment('物件CD');
            $table->string('key_number', 255)->nullable()->comment('鍵番号');
            $table->string('roof', 10)->nullable()->comment('屋根');
            $table->string('floor', 255)->nullable()->comment('階');
            $table->string('parking_remark', 255)->nullable()->comment('駐車場備考');
            $table->integer('total_length')->nullable()->comment('全長');
            $table->integer('total_width')->nullable()->comment('全幅');
            $table->integer('total_height')->nullable()->comment('全高');
            $table->integer('max_weight')->nullable()->comment('最大重量');
            $table->string('shape', 20)->nullable()->comment('形状');
            $table->string('parking_cd', 255)->nullable()->comment('駐車場CD');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_data_en_parkings');
    }
}
