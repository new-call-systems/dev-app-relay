<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Hidehalo\Nanoid\Client;

class McAndResidentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            //管理会社マスタ登録
            $mc = new \App\Models\ManagementCompany;
            $mc->management_company_id = 'MC_cocoen';
            $mc->name = 'テスト管理会社';
            $mc->address = '福岡市中央区1-1-1';
            $mc->tel = '092-999-9999';
            $mc->fax = '092-111-1111';
            $mc->save();

            //ユーザー登録
            $user = new \App\Models\User;
            $user->email = 'mc_seeder@test.com';
            $user->password = Hash::make('password');
            $user->save();
            //ユーザーロール登録
            $user->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));

            //管理会社担当者登録
            $mcm = new \App\Models\ManagementCompanyMember;
            $mcm->user_id = $user->user_id;
            $mcm->management_company_id = $mc->management_company_id;
            $mcm->last_name = '山田';
            $mcm->first_name = '太郎';
            $mcm->emergency_tel = '090-1234-5678';
            $mcm->representative_flg = 1;
            $mcm->save();

            //管理会社APIアクセスキーテーブル登録
            $client = new Client();
            $accessKeyId = $client->formattedId('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', 20);
            $secretKey = $client->formattedId('abcdefghijklmnopqrstuvwxyz1234567890', 40);
            $mcaak = new \App\Models\ManagementCompanyApiAccessKey;
            $mcaak->management_company_id = $mc->management_company_id;
            $mcaak->access_key_id = $accessKeyId;
            $mcaak->secret_key = $secretKey;
            $mcaak->save();

            //物件登録
            $property = new \App\Models\Property;
            $property->management_company_id = $mc->management_company_id;
            $property->property_code = '1001';
            $property->name = 'テスト物件1';
            $property->address = '福岡県福岡市テスト物件1-1-1';
            $property->save();


            // $mc = \App\Models\ManagementCompany::find(6);
            // $property = \App\Models\Property::find('PR_6_1234567890');

            //部屋登録
            $room = new \App\Models\Room;
            $room->management_company_id = $mc->management_company_id;
            $room->property_id = $property->property_id;
            $room->room_code = '2001';
            $room->room_no = '101';
            $room->save();

            //入居者登録
            $resident = new \App\Models\Resident;
            $resident->management_company_id = $mc->management_company_id;
            $resident->room_id = $room->room_id;
            $resident->resident_code = '30000001';
            $resident->name = '入居者 太郎';
            $resident->tel = '090-1111-9999';
            $resident->email = 'test@test';
            $resident->save();
        });
    }
}
