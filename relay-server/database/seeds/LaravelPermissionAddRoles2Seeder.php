<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class LaravelPermissionAddRoles2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //*************************
        //permission
        //*************************
        $permissions = [
            'management_company_member_permission',
        ];
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission
            ]);
        }

        //*************************
        //role
        //*************************
        $roles = [
            'management_company_member',
        ];
        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }

        //*************************
        //rolehaspermission
        //*************************
        //management_company_memberロールのパーミッション設定
        $permissions = [
            'management_company_member_permission',
        ];
        $role = Role::findByName('management_company_member');
        $role->givePermissionTo($permissions);

        //management_company_managerロールのパーミッション設定
        $role = Role::findByName('management_company_manager');
        $role->givePermissionTo($permissions);
    }
}
