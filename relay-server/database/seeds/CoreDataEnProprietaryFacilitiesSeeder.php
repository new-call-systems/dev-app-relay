<?php

use Illuminate\Database\Seeder;

class CoreDataEnProprietaryFacilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key_code' => 2001500003,
                'key_type' => '美和U9シリンダー',
                'warm_water_toilet_model' => null,
                'ac_model' => '三菱 MSZ-KS20F-W',
                'gas_water_heater_model' => 'TOTO RGH16CF5-S',
                'room_code' => 999,
                'property_code' => 9999,
                'warm_water_toilet_model_1' => null,
                'ac_model_2' => '2007400030',
                'gas_water_heater_model_3' => '2007500020',
                'cooking_equipment_model' => '2007600002',
                'intercom_entrance_set_model' => '2007700002',
                'bath_drying_unit' => '2007900011',
                'bath_faucet_model' => null,
                'vanity_model' => '1002000003',
                'toilet_tank_model' => '1002600003',
                'tv_reception_equipment' => '2007800027',
                'satellite_broadcast' => null,
                'free_internet' => '2003200002',
                'kitchen_faucet_model' => 'ﾊｰﾏﾝ ｶﾞｽｺﾝﾛ DC1001',
                'home_delivery_manual' => 'ｱｲﾎﾝ VG-2A',
                'pet_breeding' => null,
                'cooking_equipment_model_4' => 'KVK KF30N2 2ﾊﾝﾄﾞﾙ水栓',
                'intercom_entrance_set_model_5' => 'BS',
                'bath_drying_unit_6' => 'MYM 2ﾊﾝﾄﾞﾙ水栓',
                'bath_faucet_model_7' => null,
                'vanity_model_8' => null,
                'toilet_tank_model_9' => null,
                'tv_reception_equipment_10' => '2007800027',
                'satellite_broadcast_11' => null,
                'free_internet_12' => '2003200002',
                'kitchen_faucet_model_13' => 'ﾊｰﾏﾝ ｶﾞｽｺﾝﾛ DC1001',
                'home_delivery_manual_14' => 'ｱｲﾎﾝ VG-2A',
            ],
        ];

        DB::table('core_data_en_proprietary_facilities')->insert($data);
    }
}
