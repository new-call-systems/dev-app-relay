<?php

use Illuminate\Database\Seeder;

class CoreDataEnRoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'room_code' => 999,
                'property_code' => 999,
                'support_code' => 1,
                'special_note' => '左へ2回1 右へ1回4',
                'parcel_locker_password' => '',
                'layout' => 'B3',
                'owner_code' => 512,
                'management_type' => '自社管理',
                'room_management_agency_code' => 2000000000,
                'ssid' => '',
                'ssid2' => '',
                'encryption_key' => '',
                'electricity_company_code' => 1000000136,
                'water_company_code' => 1000000001,
                'electricity_company_name' => '九州電力株式会社',
                'water_company_name' => '株式会社えん賃貸管理',
                'gas_company_name' => '西部ガスお客様サービスセンター',
                'gas_company_contact' => '0570-000-312',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 2,
                'room_code' => 999,
                'property_code' => 999,
                'support_code' => 0,
                'special_note' => '左へ2回8 右へ1回2',
                'parcel_locker_password' => '',
                'layout' => 'B2',
                'owner_code' => 102986,
                'management_type' => '他社管理',
                'room_management_agency_code' => 2000000084,
                'ssid' => '',
                'ssid2' => '',
                'encryption_key' => '',
                'electricity_company_code' => 1000000136,
                'water_company_code' => 1000000001,
                'electricity_company_name' => '九州電力株式会社',
                'water_company_name' => '株式会社えん賃貸管理',
                'gas_company_name' => '西部ガスお客様サービスセンター',
                'gas_company_contact' => '0570-000-312',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 3,
                'room_code' => 999,
                'property_code' => 999,
                'support_code' => 0,
                'special_note' => '左へ2回2 右へ1回5',
                'parcel_locker_password' => '',
                'layout' => 'A',
                'owner_code' => 103402,
                'management_type' => '他社管理',
                'room_management_agency_code' => 2000000121,
                'ssid' => '',
                'ssid2' => '',
                'encryption_key' => '',
                'electricity_company_code' => 1000000136,
                'water_company_code' => 1000000001,
                'electricity_company_name' => '九州電力株式会社',
                'water_company_name' => '株式会社えん賃貸管理',
                'gas_company_name' => '西部ガスお客様サービスセンター',
                'gas_company_contact' => '0570-000-312',
                'created_at' => null,
                'updated_at' => null,
            ],
        ];

        DB::table('core_data_en_rooms')->insert($data);
    }
}
