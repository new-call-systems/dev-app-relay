<?php

use Illuminate\Database\Seeder;

class CoreDataEnVendorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'vendor_name' => '株式会社〇〇',
                'tel_number' => '999-999-9999',
                'post_number' => '999-9999',
                'address' => '福岡市博多区住吉3丁目',
                'business_hours' => '',
                'emergency_tel_number1' => '999-999-9999',
                'emergency_tel_number2' => '',
                'vendor_code' => 1000000001,
                'emergency_tel_number3' => '',
                'vendor_kbn' => '緊急連絡先',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 2,
                'vendor_name' => '株式会社△△',
                'tel_number' => '999-999-9999',
                'post_number' => '999-9999',
                'address' => '福岡市博多区住吉3丁目',
                'business_hours' => '',
                'emergency_tel_number1' => '999-999-9999',
                'emergency_tel_number2' => '',
                'vendor_code' => 1000000002,
                'emergency_tel_number3' => '',
                'vendor_kbn' => '賃貸管理',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 3,
                'vendor_name' => '株式会社××',
                'tel_number' => '999-999-9999',
                'post_number' => '999-9999',
                'address' => '福岡市博多区住吉3丁目',
                'business_hours' => '',
                'emergency_tel_number1' => '',
                'emergency_tel_number2' => '',
                'vendor_code' => 1000000003,
                'emergency_tel_number3' => '',
                'vendor_kbn' => 'リフォーム',
                'created_at' => null,
                'updated_at' => null,
            ],
        ];

        DB::table('core_data_en_vendors')->insert($data);
    }
}
