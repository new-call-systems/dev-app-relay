<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            //vendor_managerユーザーを作成する
            $user = \App\Models\User::create([
                'email' => 'vendor_managemer@test.com',
                'password' => Hash::make('password'),
            ]);
            $user->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);

            //業者マスタ
            $vn = \App\Models\Vendor::create([
                'vendor_id' => 'VN_GjzLh1FaiFnq',
                'name' => 'テスト業者',
                'address' => 'テスト業者所在地',
                'tel' => '092-111-1111',
                'emergency_tel' => '090-1111-1111',
            ]);

            //業者作業者
            $vnm = \App\Models\VendorMember::create([
                'user_id' => $user->user_id,
                'vendor_id' => $vn->vendor_id,
                'last_name' => '山田',
                'first_name' => '太郎',
                'emergency_tel' => '090-1111-1111',
                'representative_flg' => '1',
            ]);
        });
    }
}
