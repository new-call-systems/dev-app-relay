<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class SystemOwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ログイン情報
        $email = 'so1@test.com';
        $password = 'password';

        //system_ownerユーザーを作成する
        $user = User::create([
            'email' => $email,
            'password' => Hash::make($password),
        ]);
        $user->assignRole(config('const.Roles.SYSTEM_OWNER'));
    }
}
