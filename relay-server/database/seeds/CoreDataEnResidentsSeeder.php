<?php

use Illuminate\Database\Seeder;

class CoreDataEnResidentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'property_code' => 9999,
                'room_code' => 999,
                'key_unlock_service' => null,
                'contract_date' => '2001/03/01 0:00:00',
                'move_in_date' => '2001/03/01 0:00:00',
                'move_out_date' => null,
                'vendor_code' => null,
                'move_out_declaration_date' => null,
                'contractor_code' => null,
                'resident_name' => '山田太郎',
                'cohabitant' => null,
                'guarantor' => '山田花子',
                'contractor' => '山田次郎',
                'guarantor_tel' => '999-999-9999',
                'guarantor_address' => '博多区住吉3',
                'contractor_tel' => '999-999-9999',
                'contractor_address' => '博多区住吉3',
                'en_club_id' => null,
                'resident_tel' => '999-999-9999',
                'resident_mobile' => null,
                'resident_birthday' => null,
                'resident_code' => null,
                'resident_gender' => null,
                'vehicle_info' => null,
                'parking_code' => null,
                'parking_contract_date' => null,
                'parking_cancellation_date' => null,
                'move_out_appointment_date' => null,
                'parking_contractor_name' => null,
                'parking_user_name' => null,
                'parking_contractor_phone' => null,
                'parking_contractor_mobile' => null,
                'parking_user_phone' => null,
                'parking_user_mobile' => null,
                'resident_name_kana' => null,
                'contractor_name_kana' => null,
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 2,
                'property_code' => 9999,
                'room_code' => 999,
                'key_unlock_service' => '有償',
                'contract_date' => '2001/07/07 0:00:00',
                'move_in_date' => '2001/07/07 0:00:00',
                'move_out_date' => null,
                'vendor_code' => null,
                'move_out_declaration_date' => null,
                'contractor_code' => null,
                'resident_name' => '鈴木一郎',
                'cohabitant' => null,
                'guarantor' => '鈴木次郎',
                'contractor' => '鈴木三郎',
                'guarantor_tel' => '999-999-9999',
                'guarantor_address' => '博多区住吉3',
                'contractor_tel' => '999-999-9999',
                'contractor_address' => '博多区住吉3',
                'en_club_id' => null,
                'resident_tel' => '999-999-9999',
                'resident_mobile' => '999-999-9999',
                'resident_birthday' => '2023/07/25 0:00:00',
                'resident_code' => '193',
                'resident_gender' => '男性',
                'vehicle_info' => null,
                'parking_code' => null,
                'parking_contract_date' => '2023/07/25 0:00:00',
                'parking_cancellation_date' => null,
                'move_out_appointment_date' => null,
                'parking_contractor_name' => null,
                'parking_user_name' => null,
                'parking_contractor_phone' => null,
                'parking_contractor_mobile' => null,
                'parking_user_phone' => null,
                'parking_user_mobile' => null,
                'resident_name_kana' => 'ｽｽﾞｷｲﾁﾛｳ',
                'contractor_name_kana' => 'ｽｽﾞｷｻﾌﾞﾛｳ',
                'created_at' => null,
                'updated_at' => null,
            ],
            // 追加のデータがあればここに記述
        ];

        DB::table('core_data_en_residents')->insert($data);
    }
}
