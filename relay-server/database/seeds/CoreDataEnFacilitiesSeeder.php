<?php

use Illuminate\Database\Seeder;

class CoreDataEnFacilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'housing_cd' => '1000100001',
                'pamphlet_name' => '調査中',
                'housing_name' => '',
                'housing_name1' => '0',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'housing_cd' => '1000100003',
                'pamphlet_name' => '光ファイバ・インターネット接続サービス各',
                'housing_name' => '有',
                'housing_name1' => '1',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'housing_cd' => '1000100004',
                'pamphlet_name' => '光ファイバ・インターネット接続サービス「',
                'housing_name' => '有 UCAM',
                'housing_name1' => '1',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'housing_cd' => '1000100005',
                'pamphlet_name' => '光ファイバ・インターネット接続サービス「',
                'housing_name' => '有 J:COM',
                'housing_name1' => '1',
                'created_at' => null,
                'updated_at' => null,
            ],
        ];

        DB::table('core_data_en_facilities')->insert($data);
    }
}
