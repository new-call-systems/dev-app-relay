<?php

use App\Library\Cmn;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class FaqDiyDatasForEnSeeder extends Seeder
{
	public function run()
	{
        //設定値
        $table = 'faq_diy_datas';
        $now  = Carbon::now();

        //管理会社ID
        $managementCompanyIds = ['MC_cocoen'];
        //$managementCompanyIds = ['1', 'MC_wjG-TOOoyx3q'];

        //CSV読込
        //*************************************
        //* CSVについて
        //* 1.ヘッダ行有
        //* 2.区切り文字はカンマ(,)
        //* 3.データはダブルクォート(")で囲む
        //*************************************
        setlocale(LC_ALL,'ja_JP.UTF-8');
        $file = new SplFileObject('database/seeds/csvs/faq_diy_datas_for_en1.csv');
        $file->setFlags(
            \SplFileObject::READ_CSV |
            \SplFileObject::READ_AHEAD |
            //\SplFileObject::DROP_NEW_LINE |
            \SplFileObject::SKIP_EMPTY
        );

        $list = [];
        foreach($file as $rowNum => $line) {
            if ($rowNum === 0) {
                continue;
            }
            foreach ($managementCompanyIds as $mcId) {
                $list[] = [
                    'faq_diy_id' => $line[3],
                    'management_company_id' => $mcId,
                    'resident_type' => $line[9],
                    'text' => $line[4],
                    'extra_table' => Cmn::empty2Null($line[6]),
                    'extra_column' => Cmn::empty2Null($line[7]),
                    'template' => Cmn::empty2Null($line[5]),
                    'process_id' => Cmn::empty2Null($line[8]),
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
        }

        DB::transaction(function () use ($table, $list, $managementCompanyIds) {
            DB::table($table)->whereIn('management_company_id', $managementCompanyIds)->delete();
            DB::table($table)->insert($list);
        });
	}
}
