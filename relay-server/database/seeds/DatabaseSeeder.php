<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaravelPermissionAddRolesSeeder::class);
        $this->call(LaravelPermissionAddRoles2Seeder::class);
        $this->call(LaravelPermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(McAndResidentSeeder::class);
        $this->call(TimeSectionSeeder::class);
        $this->call(VendorSeeder::class);
        $this->call(RequestSeeder::class);
        $this->call(SystemOwnerSeeder::class);
        $this->call(TimeSectionSeeder::class);
        $this->call(CoreDataEnFacilitiesSeeder::class);
        $this->call(CoreDataEnParkingSeeder::class);
        $this->call(CoreDataEnPropertiesSeeder::class);
        $this->call(CoreDataEnProprietaryFacilitiesSeeder::class);
        $this->call(CoreDataEnResidentsSeeder::class);
        $this->call(CoreDataEnRoomsSeeder::class);
        $this->call(CoreDataEnSharedFacilitiesSeeder::class);
        $this->call(CoreDataEnVendorsSeeder::class);
//        $this->call(FaqDiyDatasForEnSeeder::class);
    }
}
