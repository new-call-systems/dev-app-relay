<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class LaravelPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //*************************
        //permission
        //*************************
        $permissions = [
            'system_owner_permission',
            'vendor_manager_permission',
            'vendor_member_permission',
        ];
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission
            ]);
        }

        //*************************
        //role
        //*************************
        $roles = [
            'system_owner',
            'vendor_manager',
            'vendor_member',
        ];
        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }

        //*************************
        //rolehaspermission
        //*************************
        //system_ownerロールのパーミッション設定
        $permissions = [
            'system_owner_permission',
        ];
        $role = Role::findByName('system_owner');
        $role->givePermissionTo($permissions);

        //vendor_managerロールのパーミッション設定
        $permissions = [
            'vendor_manager_permission',
            'vendor_member_permission',
        ];
        $role = Role::findByName('vendor_manager');
        $role->givePermissionTo($permissions);

        //vendor_memberロールのパーミッション設定
        $permissions = [
            'vendor_member_permission',
        ];
        $role = Role::findByName('vendor_member');
        $role->givePermissionTo($permissions);
    }
}
