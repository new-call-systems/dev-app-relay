<?php

use Illuminate\Database\Seeder;

class CoreDataEnPropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'property_name' => 'エンクレスト舞鶴',
                'property_code' => 16,
                'completion_date' => '2003/02/24 0:00:00',
                'management_company_code' => 1000000002,
                'company_name' => '株式会社えん建物管理',
                'sticker_management_company_code' => 1000000001,
                'sticker_management_company_tel' => null,
                'parking_management_company_code' => 1000001213,
                'parking_management_company_tel' => null,
                'landmark' => '舞鶴小学校',
                'management_room_tel' => '999-999-9999',
                'work_day' => '週2回 月 木',
                'work_time' => '巡回管理',
                'manager_name' => '山田太郎（巡回）',
                'manager_tel' => '999-999-9999',
                'note' => null,
                'address' => '福岡県福岡市中央区舞鶴1-5-10',
                'large_bike_contract_management_company_code' => '自社管理',
                'large_bike_contract_company_tel' => 'ｴﾝｸﾚｽﾄﾏｲﾂﾞﾙ',
                'company_code' => 1000001047,
                'property_kana' => 1000000040,
                'internet_general_guidance_code' => 1000000098,
                'construction_company_code' => 1000000062,
                'electric_equipment_code' => 'インターネット総合受付センター',
                'water_supply_and_drainage_facility_code' => 1000000243,
                'internet_general_guidance' => null,
                'construction_company' => null,
                'electric_equipment' => '« NULL »',
                'water_supply_and_drainage_facility' => '« NULL »',
                'nearest_police_management_company_code' => 1000000098,
                'nearest_police_management_contact' => null,
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'property_name' => 'エンクレスト博多駅前�U',
                'property_code' => 17,
                'completion_date' => '2003/02/21 0:00:00',
                'management_company_code' => 1000000002,
                'company_name' => '株式会社えん建物管理',
                'sticker_management_company_code' => 1000000001,
                'sticker_management_company_tel' => null,
                'parking_management_company_code' => 1000001213,
                'parking_management_company_tel' => null,
                'landmark' => '八百治博多ホテル ホテルサンライン福岡',
                'management_room_tel' => '999-999-9999',
                'work_day' => '週5回 月〜金',
                'work_time' => '13:00〜15:00',
                'manager_name' => '鈴木一郎(女性)',
                'manager_tel' => '999-999-9999',
                'note' => null,
                'address' => '福岡県福岡市博多区博多駅前4-9-36',
                'large_bike_contract_management_company_code' => '自社管理',
                'large_bike_contract_company_tel' => 'ｴﾝｸﾚｽﾄﾊｶﾀｴｷﾏｴﾂｰ',
                'company_code' => 1000001047,
                'property_kana' => 1000000036,
                'internet_general_guidance_code' => 1000000092,
                'construction_company_code' => 1000000062,
                'electric_equipment_code' => 'インターネット総合受付センター',
                'water_supply_and_drainage_facility_code' => 1000001188,
                'internet_general_guidance' => null,
                'construction_company' => null,
                'electric_equipment' => '« NULL »',
                'water_supply_and_drainage_facility' => '« NULL »',
                'nearest_police_management_company_code' => 1000000243,
                'nearest_police_management_contact' => null,
                'created_at' => null,
                'updated_at' => null,
            ],

        ];

        DB::table('core_data_en_properties')->insert($data);
    }
}
