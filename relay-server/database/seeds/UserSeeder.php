<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //system_ownerユーザーを作成する
        $user = User::create([
            'email' => 'so@test.com',
            'password' => Hash::make('password'),
        ]);
        $user->assignRole(config('const.Roles.SYSTEM_OWNER'));

        //vendor_managerユーザーを作成する
        $user = User::create([
            'email' => 'v_manager@test.com',
            'password' => Hash::make('password'),
        ]);
        $user->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);

        //management_company_managerユーザーを作成する
        $user = User::create([
            'email' => 'mc_manager@test.com',
            'password' => Hash::make('password'),
        ]);
        $user->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));

    }
}
