<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class RequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            $vendor_id = 'VN_GjzLh1FaiFnq';


            $resident = \App\Models\Resident::first();
            $vendor = \App\Models\Vendor::first();
            $vendorMember = \App\Models\VendorMember::where(['vendor_id' => $vendor->vendor_id])->first();

            $request = \App\Models\Request::create([
                'resident_id' => $resident->resident_id,
                'management_company_id' => $resident->management_company_id,
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'status' => config('const.Request.Status.OPEN'),
                'request_type' => config('const.Request.RequestType.VENDOR'),
                'ai_category' => '1',
                'ai_sub_category' => '1-1',
                'emergency_flg' => '0',
            ]);

            \App\Models\RequestContent::create([
                'request_id' => $request->request_id,
                'context' => 'エアコンから水がポタポタ落ちてくるのですが？',
                'response' => '業者を派遣します。',
            ]);

            for ($i = 0; $i < 3; $i++) {
                \App\Models\RequestPreferableTime::create([
                    'request_id' => $request->request_id,
                    'priority' => $i + 1,
                    'date' => Carbon::now()->format('Y/m/d'),
                    'time_section_id' => $i + 1,
                ]);
            }

            $request2 = \App\Models\Request::create([
                'resident_id' => $resident->resident_id,
                'management_company_id' => $resident->management_company_id,
                'visibility' => config('const.Request.Visibility.PROTECTED'),
                'status' => config('const.Request.Status.OPEN'),
                'request_type' => config('const.Request.RequestType.VENDOR'),
                'ai_category' => '2',
                'ai_sub_category' => '2-1',
                'emergency_flg' => '1',
            ]);

            \App\Models\RequestContent::create([
                'request_id' => $request2->request_id,
                'context' => 'エレベーターの中に閉じ込められた。助けてくれ。',
                'response' => '業者を派遣します。',
            ]);

            for ($i = 0; $i < 3; $i++) {
                \App\Models\RequestPreferableTime::create([
                    'request_id' => $request2->request_id,
                    'priority' => $i + 1,
                    'date' => Carbon::now()->format('Y/m/d'),
                    'time_section_id' => $i + 1,
                ]);
            }

            \App\Models\RequestVendorPermission::create([
                'request_id' => $request2->request_id,
                'vendor_id' =>$vendor->vendor_id,
            ]);

        });
    }
}
