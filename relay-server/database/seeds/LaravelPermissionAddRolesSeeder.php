<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class LaravelPermissionAddRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //*************************
        //permission
        //*************************
        $permissions = [
            'management_company_manager_permission',
            'resident_permission',
        ];
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission
            ]);
        }

        //*************************
        //role
        //*************************
        $roles = [
            'management_company_manager',
            'resident',
        ];
        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }

        //*************************
        //rolehaspermission
        //*************************
        //management_company_managerロールのパーミッション設定
        $permissions = [
            'management_company_manager_permission',
        ];
        $role = Role::findByName('management_company_manager');
        $role->givePermissionTo($permissions);

        //residentロールのパーミッション設定
        $permissions = [
            'resident_permission',
        ];
        $role = Role::findByName('resident');
        $role->givePermissionTo($permissions);
    }
}
