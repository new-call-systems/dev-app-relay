<?php

use Illuminate\Database\Seeder;

class TimeSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = [
            '午前中',
            '午後0時～3時',
            '午後3時～6時',
            '午後6時以降',
        ];
        foreach ($sections as $section) {
            \App\Models\TimeSection::create([
                'label' => $section
            ]);
        }
    }
}
