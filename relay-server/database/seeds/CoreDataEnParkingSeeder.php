<?php

use Illuminate\Database\Seeder;

class CoreDataEnParkingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'parking_code' => '00999P001',
                'property_code' => 999,
                'key_number' => 'TAKIGEN 1873',
                'roof' => '有',
                'floor' => '1階',
                'parking_remark' => '',
                'total_length' => 4800,
                'total_width' => 1800,
                'total_height' => 1550,
                'max_weight' => 1700,
                'shape' => '機械式',
                'parking_cd' => '00999P001',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 2,
                'parking_code' => '00999P002',
                'property_code' => 999,
                'key_number' => 'TAKIGEN 1873',
                'roof' => '有',
                'floor' => '1階',
                'parking_remark' => '',
                'total_length' => 4800,
                'total_width' => 1800,
                'total_height' => 1550,
                'max_weight' => 1700,
                'shape' => '機械式',
                'parking_cd' => '00999P002',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 3,
                'parking_code' => '00999P003',
                'property_code' => 999,
                'key_number' => 'TAKIGEN 1873',
                'roof' => '有',
                'floor' => 'B1階',
                'parking_remark' => '',
                'total_length' => 4800,
                'total_width' => 1800,
                'total_height' => 1550,
                'max_weight' => 1700,
                'shape' => '機械式',
                'parking_cd' => '00999P003',
                'created_at' => null,
                'updated_at' => null,
            ],
            [
                'id' => 4,
                'parking_code' => '00999P004',
                'property_code' => 999,
                'key_number' => 'TAKIGEN 1873',
                'roof' => '有',
                'floor' => 'B1階',
                'parking_remark' => '',
                'total_length' => 4800,
                'total_width' => 1800,
                'total_height' => 1550,
                'max_weight' => 1700,
                'shape' => '機械式',
                'parking_cd' => '00999P004',
                'created_at' => null,
                'updated_at' => null,
            ],
        ];

        DB::table('core_data_en_parkings')->insert($data);
    }
}
