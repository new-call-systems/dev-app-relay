<?php

namespace Tests\Unit\so;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class VendorTest extends TestCase
{

    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 業者一覧画面が表示されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_view_vendor()
    {
        $this->get('/so/vendor')
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * 業者一覧画面が正常に表示されること
     * @test
     */
    public function authenticated_user_can_view_vendor()
    {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user, 'guard_system_owner')->get('/so/vendor');
        $response->assertStatus(200);
    }

    /**
     * ログインしていない場合、
     * 業者登録されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_store_vendor()
    {
        $data = [
            'name' => '業者名',
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'emergency_tel_1' => '090-1111-2222',
            'address' => '住所',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'emergency_tel_2' => '080-0000-9999',
            'branch_name' => '支店名',
        ];
        $this->post('/so/vendor', $data)
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で業者情報が登録されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_store_vendor()
    {
        $data = [
            'name' => 'テストテスト業者名テストテスト',
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test2@test.com',
            'emergency_tel_1' => '090-1111-2222',
            'address' => '住所',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'emergency_tel_2' => '080-0000-9999',
            'branch_name' => '支店名',
            'parent_address' => 'parent_address',
            'parent_tel' => '000-000-0000',
            'parent_fax' => '000-000-0000'
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $this->actingAs($user, 'guard_system_owner')->post('/so/vendor', $data)->assertRedirect(url()->previous());
        //検証
        $vendor = \App\Models\Vendor::where('name', $data['name'])->where('address', $data['address'])->first();
        $this->assertTrue($data['name'] === $vendor->name);
        $this->assertTrue($data['address'] === $vendor->address);
        $this->assertTrue($data['tel'] === $vendor->tel);
        $this->assertTrue($data['fax'] === $vendor->fax);
        $this->assertTrue($data['emergency_tel_2'] === $vendor->emergency_tel);
        $this->assertTrue($data['branch_name'] === $vendor->branch_name);
        $user = \App\Models\User::where('email', $data['email'])->first();
        $this->assertTrue($data['email'] === $user->email);
        $vnm = \App\Models\VendorMember::where('user_id', $user->user_id)->where('vendor_id', $vendor->vendor_id)->first();
        $this->assertTrue($user->user_id === $vnm->user_id);
        $this->assertTrue($vendor->vendor_id === $vnm->vendor_id);
        $this->assertTrue($data['last_name'] === $vnm->last_name);
        $this->assertTrue($data['first_name'] === $vnm->first_name);
        $this->assertTrue($data['emergency_tel_1'] === $vnm->emergency_tel);
        $this->assertTrue(null === $vnm->retire_flg);
        $this->assertTrue(1 === $vnm->representative_flg);
        $this->assertTrue(null === $vnm->access_token);
        $this->assertTrue(null === $vnm->token_expire);
        $this->assertTrue(null === $vnm->refresh_token);
        $this->assertTrue(null === $vnm->license_expire);
    }

    /**
     * 業者登録時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_store_vendor_required()
    {
        $data = [
            'name' => '',
            'last_name' => '',
            'first_name' => '',
            'email' => '',
            'emergency_tel_1' => '',
            'address' => '',
            'tel' => '',
            'fax' => '',
            'emergency_tel_2' => '',
            'branch_name' => '',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/vendor', $data);
        //検証
        $response->assertSessionHasErrors([
            'name' => '名称は必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'email' => 'メールアドレスは必ず指定してください。',
            'emergency_tel_1' => '緊急連絡先は必ず指定してください。',
            'address' => '住所は必ず指定してください。',
            'tel' => '電話番号は必ず指定してください。',
            'emergency_tel_2' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 業者登録時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_store_vendor_max_length()
    {
        $str256 = $this->faker->realText(256);
        $data = [
            'name' => $str256,
            'last_name' => $str256,
            'first_name' => $str256,
            'email' => $str256,
            'emergency_tel_1' => '12345678901234',
            'address' => $str256,
            'tel' => '12345678901234',
            'fax' => '12345678901234',
            'emergency_tel_2' => '12345678901234',
            'branch_name' => $str256,
            'parent_address' => $str256,
            'parent_tel' => '12345678901234',
            'parent_fax' => '12345678901234',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/vendor', $data);
        //検証

        $response->assertSessionHasErrors([
            'name' => '名称は、255文字以下で指定してください。',
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'email' => 'メールアドレスは、255文字以下で指定してください。',
            'emergency_tel_1' => '緊急連絡先は、13文字以下で指定してください。',
            'address' => '住所は、255文字以下で指定してください。',
            'tel' => '電話番号は、13文字以下で指定してください。',
            'fax' => 'FAXは、13文字以下で指定してください。',
            'emergency_tel_2' => '緊急連絡先は、13文字以下で指定してください。',
            'branch_name' => '支店・営業所名は、255文字以下で指定してください。',
            'parent_address' => '住所は、255文字以下で指定してください。',
            'parent_tel' => '電話番号は、13文字以下で指定してください。',
            'parent_fax' => 'FAXは、13文字以下で指定してください。',
        ]);
    }

    /**
     * 業者登録時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_store_vendor_type()
    {
        $data = [
            'name' => 'テスト業者',
            'last_name' => '姓',
            'first_name' => '名',
            'email' => '12345',
            'emergency_tel_1' => '0120-111-111',
            'address' => '住所',
            'tel' => '0-3333-3333',
            'fax' => '09000-111-111',
            'emergency_tel_2' => '123-456-7890',
            'branch_name' => '支店',
            'parent_tel' => '0-3333-3333',
            'parent_fax' => '09000-111-111',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/vendor', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスには、有効なメールアドレスを指定してください。',
            'emergency_tel_1' => '緊急連絡先は携帯電話番号を指定してください。',
            'tel' => '電話番号に正しい形式を指定してください。',
            'fax' => 'FAXに正しい形式を指定してください。',
            'emergency_tel_2' => '緊急連絡先に正しい形式を指定してください。',
            'parent_tel' => '電話番号に正しい形式を指定してください。',
            'parent_fax' => 'FAXに正しい形式を指定してください。',
        ]);
    }

    /**
     * 業者登録時バリデーション
     * （ユニークチェック）
     * @test
     */
    public function validator_check_for_store_vendor_unique()
    {
        $data = [
            'name' => 'テスト業者',
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'so@test.com',
            'emergency_tel_1' => '090-1111-2222',
            'address' => '住所',
            'tel' => '03-3333-3333',
            'fax' => '03-1111-1111',
            'emergency_tel_2' => '090-1456-7890',
            'branch_name' => '支店',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/vendor', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスの値は既に存在しています。',
        ]);
    }

    /**
     * ログインしていない場合、
     * 業者更新されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_vendor()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'name' => '更新業者名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel_1' => '090-1111-2222',
            'address' => '更新住所',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'emergency_tel_2' => '080-0000-9999',
            'branch_name' => '更新支店名',
        ];
        $this->put('/so/vendor/'.$createVendor->vendor_id, $data)
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で業者情報が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_vendor()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'name' => '更新業者名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel_1' => '090-1111-2222',
            'address' => '更新住所',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'emergency_tel_2' => '080-0000-9999',
            'branch_name' => '更新支店名',
            'parent_address' => '本社更新住所',
            'parent_tel' => '092-111-1111',
            'parent_fax' => '092-999-9999',
            'vn_updated_at' => $createVendor->updated_at,
            'vnm_updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/vendor/'.$createVendor->vendor_id, $data)->assertRedirect(url()->previous());
        //検証
        $vendor = \App\Models\Vendor::find($createVendor->vendor_id);
        $this->assertTrue($data['name'] === $vendor->name);
        $this->assertTrue($data['address'] === $vendor->address);
        $this->assertTrue($data['tel'] === $vendor->tel);
        $this->assertTrue($data['fax'] === $vendor->fax);
        $this->assertTrue($data['emergency_tel_2'] === $vendor->emergency_tel);
        $this->assertTrue($data['branch_name'] === $vendor->branch_name);
        $this->assertTrue($data['parent_address'] === $vendor->parent_address);
        $this->assertTrue($data['parent_tel'] === $vendor->parent_tel);
        $this->assertTrue($data['parent_fax'] === $vendor->parent_fax);

        $vnm = \App\Models\VendorMember::where('user_id', $createVnm->user_id)->where('vendor_id', $createVnm->vendor_id)->first();
        $this->assertTrue($createVnm->user_id === $vnm->user_id);
        $this->assertTrue($createVnm->vendor_id === $vnm->vendor_id);
        $this->assertTrue($data['last_name'] === $vnm->last_name);
        $this->assertTrue($data['first_name'] === $vnm->first_name);
        $this->assertTrue($data['emergency_tel_1'] === $vnm->emergency_tel);
        $this->assertTrue($createVnm->retire_flg === $vnm->retire_flg);
        $this->assertTrue($representativeFlg === $vnm->representative_flg);
        $this->assertTrue($createVnm->access_token === $vnm->access_token);
        $this->assertTrue($createVnm->token_expire === $vnm->token_expire);
        $this->assertTrue($createVnm->refresh_token === $vnm->refresh_token);
        $this->assertTrue($createVnm->license_expire === $vnm->license_expire);
    }

    /**
     * 業者更新時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_update_vendor_required()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => '',
            'vendor_id' => '',
            'name' => '',
            'last_name' => '',
            'first_name' => '',
            'emergency_tel_1' => '',
            'address' => '',
            'tel' => '',
            'fax' => '',
            'emergency_tel_2' => '',
            'branch_name' => '',
            'vn_updated_at' => '',
            'vnm_updated_at' => '',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/vendor/'.$createVendor->vendor_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'user_id' => 'user idは必ず指定してください。',
            'vendor_id' => 'vendor idは必ず指定してください。',
            'name' => '名称は必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'emergency_tel_1' => '緊急連絡先は必ず指定してください。',
            'address' => '住所は必ず指定してください。',
            'tel' => '電話番号は必ず指定してください。',
            'emergency_tel_2' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 業者更新時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_update_vendor_max_length()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $str256 = $this->faker->realText(256);
        $data = [
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'name' => $str256,
            'last_name' => $str256,
            'first_name' => $str256,
            'emergency_tel_1' => '12345678901234',
            'address' => $str256,
            'tel' => '12345678901234',
            'fax' => '12345678901234',
            'emergency_tel_2' => '12345678901234',
            'branch_name' => $str256,
            'vn_updated_at' => $createVendor->updated_at,
            'vnm_updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/vendor/'.$createVendor->vendor_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'name' => '名称は、255文字以下で指定してください。',
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'emergency_tel_1' => '緊急連絡先は、13文字以下で指定してください。',
            'address' => '住所は、255文字以下で指定してください。',
            'tel' => '電話番号は、13文字以下で指定してください。',
            'fax' => 'FAXは、13文字以下で指定してください。',
            'emergency_tel_2' => '緊急連絡先は、13文字以下で指定してください。',
            'branch_name' => '支店・営業所名は、255文字以下で指定してください。',
        ]);
    }

    /**
     * 業者更新時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_update_vendor_type()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'name' => '更新業者名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel_1' => '092-111-2222',
            'address' => '更新住所',
            'tel' => '0-1111-1111',
            'fax' => '01234-5-6789',
            'emergency_tel_2' => '123-456-7890',
            'branch_name' => '更新支店名',
            'vn_updated_at' => $createVendor->updated_at,
            'vnm_updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/vendor/'.$createVendor->vendor_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'emergency_tel_1' => '緊急連絡先は携帯電話番号を指定してください。',
            'tel' => '電話番号に正しい形式を指定してください。',
            'fax' => 'FAXに正しい形式を指定してください。',
            'emergency_tel_2' => '緊急連絡先に正しい形式を指定してください。',
        ]);
    }
}
