<?php

namespace Tests\Unit\so;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ユーザーがログインできること
     * @test
     */
    public function user_can_login()
    {
        $data = [
            'email' => 'so@test.com',
            'password' => 'password',
        ];
        $this->post('/so/login', $data)->assertRedirect('/so');
    }

    /**
     * ユーザーがログインできないこと
     * @test
     */
    public function user_cannot_login()
    {
        $data = [
            'email' => 'so@test.com',
            'password' => 'unauthenticated',
        ];
        $response = $this->post('/so/login', $data);
        $this->post('/so/login', $data)->assertRedirect(url()->previous());
    }

    /**
     * ログインしていない場合、
     * ログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_can_view_login()
    {
        $response = $this->get('/so/login');

        $response->assertStatus(200);
    }

    /**
     * 既にログインしている場合、
     * ログイン画面は表示されずにダッシュボード画面が表示されること
     * @test
     */
    public function authenticated_user_cannot_view_login()
    {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user, 'guard_system_owner')->get('/so/login')->assertRedirect('/so');;
    }

    /**
     * ログインしていない場合、
     * ダッシュボード画面が表示されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_view_home()
    {
        $this->get('/so/')
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * ダッシュボード画面が正常に表示されること
     * @test
     */
    public function authenticated_user_can_view_home()
    {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user, 'guard_system_owner')->get('/so');
        $response->assertStatus(200);
    }
}
