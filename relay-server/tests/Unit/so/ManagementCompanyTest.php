<?php

namespace Tests\Unit\so;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ManagementCompanyTest extends TestCase
{

    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 管理会社一覧画面が表示されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_view_management_company()
    {
        $this->get('/so/managementCompany')
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * 管理会社一覧画面が正常に表示されること
     * @test
     */
    public function authenticated_user_can_view_management_company()
    {
        $user = \App\Models\User::find(1);
        $response = $this->actingAs($user, 'guard_system_owner')->get('/so/managementCompany');
        $response->assertStatus(200);
    }

    /**
     * ログインしていない場合、
     * 管理会社登録されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_store_management_company()
    {
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId'
        ];
        $this->post('/so/managementCompany', $data)
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で管理会社情報が登録されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_store_management_company()
    {
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'unit_test@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'support_tel' => '098-111-9999',
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data)->assertRedirect(url()->previous());
        //検証
        $mc = \App\Models\ManagementCompany::where('name', $data['name'])->where('address', $data['address'])->first();
        $this->assertTrue($data['name'] === $mc->name);
        $this->assertTrue($data['address'] === $mc->address);
        $this->assertTrue($data['tel'] === $mc->tel);
        $this->assertTrue($data['fax'] === $mc->fax);
        $this->assertTrue($data['support_tel'] === $mc->support_tel);
        $user = \App\Models\User::where('email', $data['email'])->first();
        $this->assertTrue($data['email'] === $user->email);
        $mcm = \App\Models\ManagementCompanyMember::where('user_id', $user->user_id)->where('management_company_id', $mc->management_company_id)->first();
        $this->assertTrue($user->user_id === $mcm->user_id);
        $this->assertTrue($mc->management_company_id === $mcm->management_company_id);
        $this->assertTrue($data['last_name'] === $mcm->last_name);
        $this->assertTrue($data['first_name'] === $mcm->first_name);
        $this->assertTrue($data['emergency_tel'] === $mcm->emergency_tel);
        $this->assertTrue(1 === $mcm->representative_flg);
        $this->assertTrue($data['department'] === $mcm->department);
        $mcaak = \App\Models\ManagementCompanyApiAccessKey::find($mc->management_company_id);
        $this->assertTrue($mcaak !== null);
        $mcli = \App\Models\ManagementCompanyLineInformation::find($mc->management_company_id);
        $this->assertTrue($data['channel_id'] === $mcli->channel_id);
        $this->assertTrue($data['channel_secret'] === $mcli->channel_secret);
        $this->assertTrue($data['channel_access_token'] === $mcli->channel_access_token);
        $this->assertTrue($data['liff_id'] === $mcli->liff_id);
    }

    /**
     * 管理会社登録時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_store_management_company_required()
    {
        $data = [
            'name' => '',
            'last_name' => '',
            'first_name' => '',
            'department' => '',
            'email' => '',
            'tel' => '',
            'fax' => '',
            'address' => '',
            'emergency_tel' => '',
            'support_tel' => '',
            'channel_id' => '',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => '',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'name' => '名称は必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'email' => 'メールアドレスは必ず指定してください。',
            'tel' => '電話番号は必ず指定してください。',
            'address' => '住所は必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 管理会社登録時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_store_management_company_max_length()
    {
        $str256 = $this->faker->realText(256);
        $data = [
            'name' => $str256,
            'last_name' => $str256,
            'first_name' => $str256,
            'department' => $str256,
            'email' => $str256,
            'tel' => '12345678901234',
            'fax' => '12345678901234',
            'address' => $str256,
            'emergency_tel' => '12345678901234',
            'support_tel' => '12345678901234',
            'channel_id' => $str256,
            'channel_secret' => $str256,
            'channel_access_token' => $str256,
            'liff_id' => $str256,
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'name' => '名称は、255文字以下で指定してください。',
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'department' => '部署名は、255文字以下で指定してください。',
            'email' => 'メールアドレスは、255文字以下で指定してください。',
            'tel' => '電話番号は、13文字以下で指定してください。',
            'fax' => 'FAXは、13文字以下で指定してください。',
            'address' => '住所は、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
            'support_tel' => '入居者問合せ先は、13文字以下で指定してください。',
            'channel_id' => 'チャンネルIDは、255文字以下で指定してください。',
            'channel_secret' => 'シークレットは、255文字以下で指定してください。',
            'channel_access_token' => 'アクセストークンは、255文字以下で指定してください。',
            'liff_id' => 'LIFF IDは、255文字以下で指定してください。',
        ]);
    }

    /**
     * 管理会社登録時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_store_management_company_type()
    {
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'unit_testtest.com',
            'tel' => '123456789',
            'fax' => '191-111-9999',
            'address' => '住所',
            'emergency_tel' => '082-000-9999',
            'support_tel' => '999-1111-1111',
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスには、有効なメールアドレスを指定してください。',
            'tel' => '電話番号に正しい形式を指定してください。',
            'fax' => 'FAXに正しい形式を指定してください。',
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
            'support_tel' => '入居者問合せ先に正しい形式を指定してください。',
        ]);
    }

    /**
     * 管理会社登録時バリデーション
     * （ユニークチェック）
     * @test
     */
    public function validator_check_for_store_management_company_unique()
    {
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'so@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'support_tel' => '092-123-4567',
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスの値は既に存在しています。',
        ]);
    }

    /**
     * 管理会社登録時バリデーション
     * （相対必須）
     * @test
     */
    public function validator_check_for_store_management_company_with_required()
    {
        //**********************************
        // LIFF IDだけ入力パターン
        //**********************************
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'so@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'support_tel' => '092-123-4567',
            'channel_id' => '',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => 'liffId',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'channel_id' => 'シークレット / アクセストークン / LIFF IDを指定する場合は、チャンネルIDも指定してください。',
            'channel_secret' => 'チャンネルID / アクセストークン / LIFF IDを指定する場合は、シークレットも指定してください。',
            'channel_access_token' => 'チャンネルID / シークレット / LIFF IDを指定する場合は、アクセストークンも指定してください。',
        ]);

        //**********************************
        // チャンネルIDだけ入力パターン
        //**********************************
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'so@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'support_tel' => '092-123-4567',
            'channel_id' => '12345',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => '',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'channel_secret' => 'チャンネルID / アクセストークン / LIFF IDを指定する場合は、シークレットも指定してください。',
            'channel_access_token' => 'チャンネルID / シークレット / LIFF IDを指定する場合は、アクセストークンも指定してください。',
            'liff_id' => 'チャンネルID / シークレット / アクセストークンを指定する場合は、LIFF IDも指定してください。',
        ]);

        //**********************************
        // チャンネルIDだけ未入力パターン
        //**********************************
        $data = [
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'email' => 'so@test.com',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'support_tel' => '092-123-4567',
            'channel_id' => '',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $user = \App\Models\User::find(1);
        //登録処理
        $response = $this->actingAs($user, 'guard_system_owner')->post('/so/managementCompany', $data);
        //検証
        $response->assertSessionHasErrors([
            'channel_id' => 'シークレット / アクセストークン / LIFF IDを指定する場合は、チャンネルIDも指定してください。',
        ]);
    }

    /**
     * ログインしていない場合、
     * 管理会社更新されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_management_company()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '管理会社名',
            'last_name' => '姓',
            'first_name' => '名',
            'department' => '部署名',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'support_tel' => '092-123-4567',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $this->put('/so/managementCompany/'.$createMc->management_company_id, $data)
            ->assertRedirect('/so/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で管理会社情報が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_management_company()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '更新管理会社名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'department' => '更新部署名',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'support_tel' => '092-123-4567',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $mc = \App\Models\ManagementCompany::find($createMc->management_company_id);
        $this->assertTrue($data['name'] === $mc->name);
        $this->assertTrue($data['address'] === $mc->address);
        $this->assertTrue($data['tel'] === $mc->tel);
        $this->assertTrue($data['fax'] === $mc->fax);
        $this->assertTrue($data['support_tel'] === $mc->support_tel);
        $mcm = \App\Models\ManagementCompanyMember::where('user_id', $createUser->user_id)->where('management_company_id', $createMc->management_company_id)->first();
        $this->assertTrue($createMcm->user_id === $mcm->user_id);
        $this->assertTrue($createMcm->management_company_id === $mcm->management_company_id);
        $this->assertTrue($data['last_name'] === $mcm->last_name);
        $this->assertTrue($data['first_name'] === $mcm->first_name);
        $this->assertTrue($data['emergency_tel'] === $mcm->emergency_tel);
        $this->assertTrue($representativeFlg === $mcm->representative_flg);
        $this->assertTrue($data['department'] === $mcm->department);
        $mcli = \App\Models\ManagementCompanyLineInformation::find($createMc->management_company_id);
        $this->assertTrue($data['channel_id'] === $mcli->channel_id);
        $this->assertTrue($data['channel_secret'] === $mcli->channel_secret);
        $this->assertTrue($data['channel_access_token'] === $mcli->channel_access_token);
        $this->assertTrue($data['liff_id'] === $mcli->liff_id);
    }

    /**
     * 管理会社更新時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_update_management_company_required()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        $data = [
            'user_id' => '',
            'management_company_id' => '',
            'name' => '',
            'last_name' => '',
            'first_name' => '',
            'department' => '',
            'tel' => '',
            'fax' => '',
            'support_tel' => '',
            'address' => '',
            'emergency_tel' => '',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => '',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'user_id' => 'user idは必ず指定してください。',
            'management_company_id' => 'management company idは必ず指定してください。',
            'name' => '名称は必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'tel' => '電話番号は必ず指定してください。',
            'address' => '住所は必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 管理会社更新時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_update_management_company_max_length()
    {
        $str256 = $this->faker->realText(256);
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => $str256,
            'last_name' => $str256,
            'first_name' => $str256,
            'department' => $str256,
            'tel' => '12345678901234',
            'fax' => '12345678901234',
            'support_tel' => '12345678901234',
            'address' => $str256,
            'emergency_tel' => '12345678901234',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => $str256,
            'channel_secret' => $str256,
            'channel_access_token' => $str256,
            'liff_id' => $str256,
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'name' => '名称は、255文字以下で指定してください。',
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'department' => '部署名は、255文字以下で指定してください。',
            'tel' => '電話番号は、13文字以下で指定してください。',
            'fax' => 'FAXは、13文字以下で指定してください。',
            'support_tel' => '入居者問合せ先は、13文字以下で指定してください。',
            'address' => '住所は、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
            'channel_id' => 'チャンネルIDは、255文字以下で指定してください。',
            'channel_secret' => 'シークレットは、255文字以下で指定してください。',
            'channel_access_token' => 'アクセストークンは、255文字以下で指定してください。',
            'liff_id' => 'LIFF IDは、255文字以下で指定してください。',
        ]);
    }

    /**
     * 管理会社更新時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_update_management_company_type()
    {
        $str256 = $this->faker->realText(256);
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '更新管理会社名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'department' => '更新部署名',
            'tel' => '123-1111-1111',
            'fax' => '0-9999-9999',
            'support_tel' => '123-9999-9999',
            'address' => '住所',
            'emergency_tel' => '092-111-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '12345',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'tel' => '電話番号に正しい形式を指定してください。',
            'fax' => 'FAXに正しい形式を指定してください。',
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
            'support_tel' => '入居者問合せ先に正しい形式を指定してください。',
        ]);
    }

    /**
     * 管理会社更新時バリデーション
     * （相対必須）
     * @test
     */
    public function validator_check_for_update_management_company_with_required()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $representativeFlg = 1;
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        //**********************************
        // LIFF IDだけ入力パターン
        //**********************************
        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '更新管理会社名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'department' => '更新部署名',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'support_tel' => '092-123-4567',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => 'liffId',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'channel_id' => 'シークレット / アクセストークン / LIFF IDを指定する場合は、チャンネルIDも指定してください。',
            'channel_secret' => 'チャンネルID / アクセストークン / LIFF IDを指定する場合は、シークレットも指定してください。',
            'channel_access_token' => 'チャンネルID / シークレット / LIFF IDを指定する場合は、アクセストークンも指定してください。',
        ]);

        //**********************************
        // チャンネルIDだけ入力パターン
        //**********************************
        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '更新管理会社名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'department' => '更新部署名',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'support_tel' => '092-123-4567',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '12345',
            'channel_secret' => '',
            'channel_access_token' => '',
            'liff_id' => '',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'channel_secret' => 'チャンネルID / アクセストークン / LIFF IDを指定する場合は、シークレットも指定してください。',
            'channel_access_token' => 'チャンネルID / シークレット / LIFF IDを指定する場合は、アクセストークンも指定してください。',
            'liff_id' => 'チャンネルID / シークレット / アクセストークンを指定する場合は、LIFF IDも指定してください。',
        ]);

        //**********************************
        // チャンネルIDだけ未入力パターン
        //**********************************
        $data = [
            'user_id' => $createUser->user_id,
            'management_company_id' => $createMc->management_company_id,
            'name' => '更新管理会社名',
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'department' => '更新部署名',
            'tel' => '092-111-1111',
            'fax' => '092-999-9999',
            'support_tel' => '092-123-4567',
            'address' => '住所',
            'emergency_tel' => '080-0000-9999',
            'mc_updated_at' => $createMc->updated_at,
            'mcm_updated_at' => $createMcm->updated_at,
            'channel_id' => '',
            'channel_secret' => 'secret',
            'channel_access_token' => 'accessToken',
            'liff_id' => 'liffId',
        ];
        $loginUser = \App\Models\User::find(1);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_system_owner')->put('/so/managementCompany/'.$createMc->management_company_id, $data)->assertRedirect(url()->previous());
        //検証
        $response->assertSessionHasErrors([
            'channel_id' => 'シークレット / アクセストークン / LIFF IDを指定する場合は、チャンネルIDも指定してください。',
        ]);
    }

}
