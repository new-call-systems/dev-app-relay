<?php

namespace Tests\Unit\api;

use Tests\TestCase;
use App\Library\Cmn;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ResidentTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('TimeSectionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    private function login($data, $accessKeyId, $secretKey) {
        $token = '';
        $loginFlg = false;
        while (!$loginFlg) {
            $response = $this->withHeaders([
                'Authorization' => 'Basic '.base64_encode($accessKeyId.':'.$secretKey),
            ])
            ->post('/api/rd/v1/auth/login', $data);
            $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
            $responseParams = (Object) $response->decodeResponseJson();
            if (isset($responseParams->access_token)) {
                $loginFlg = true;
                $token = $responseParams->access_token;
            }
        }

        return $token;
    }

    /**
     * ユーザーが1段階目認証に成功すること
     * @test
     */
    public function user_can_auth_1()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(200);
        $this->assertTrue($response->content() !== null);
    }

    /**
     * ユーザーが1段階目認証に失敗すること
     * （リクエストヘッダ フォーマット違い）
     * @test
     */
    public function user_cannot_auth_1_wrong_header_format()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key).' test',
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーが1段階目認証に失敗すること
     * （リクエストヘッダ Basicでない）
     * @test
     */
    public function user_cannot_auth_1_wrong_header_Basic()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Test '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーが1段階目認証に失敗すること
     * （リクエストヘッダ credentials不備）
     * @test
     */
    public function user_cannot_auth_1_wrong_header_credentials()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーが1段階目認証に失敗すること
     * （AccessKeyId, SecretKeyから管理会社特定できない）
     * @test
     */
    public function user_cannot_auth_1_wrong_key()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':testSecretKey'),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーが1段階目認証に失敗すること
     * （入居者が特定できない）
     * @test
     */
    public function user_cannot_auth_1_cannot_identify_resident()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => 'ABC123',
            'room_code' => 'ABC123',
            'resident_code' => 'ABC123',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーが2段階目認証に成功すること
     * （1段階目から続けて）
     * @test
     */
    public function user_can_auth_2()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);


        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $response->assertStatus(200);

        $responseParams = (Object) $response->decodeResponseJson();
        $this->assertTrue($responseParams->access_token !== null);
        $this->assertTrue($responseParams->refresh_token !== null);
        $this->assertTrue($responseParams->resident != null);
    }

    /**
     * ユーザーが2段階目認証に失敗すること
     * （無効なOTP）
     * @test
     */
    public function user_cannot_auth_2_wrong_otp()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);

        $response = $this->get('/api/rd/v1/auth/login?code=12345678');
        $response->assertStatus(401);
    }

    /**
     * ユーザーが2段階目認証に失敗すること
     * （1段階目の後、20秒以上経ってから）
     * @test
     */
    public function user_cannot_auth_2_timeout()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);

        sleep(20);

        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できること
     * @test
     */
    public function user_can_reissue_token()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken,
                ])->post('/api/rd/v1/auth/token', $data);
        $response->assertStatus(200);

        $responseParams = (Object) $response->decodeResponseJson();
        $this->assertTrue($responseParams->access_token !== $accessToken);
        $this->assertTrue($responseParams->refresh_token !== $refreshToken);
    }

    /**
     * アクセストークンが再発行できないこと
     * Authorizationヘッダが不正
     * @test
     */
    public function user_cannot_reissue_token_wrong_authorization_header()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer Test '.$accessToken,
                ])->post('/api/rd/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * AuthorizationヘッダがBearerでない
     * @test
     */
    public function user_cannot_reissue_token_authorization_header_is_not_bearer()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Basic '.$accessToken,
                ])->post('/api/rd/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * access_tokenが異なる
     * @test
     */
    public function user_cannot_reissue_token_wrong_access_token()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken.'test',
                ])->post('/api/rd/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * refresh_tokenが異なる
     * @test
     */
    public function user_cannot_reissue_token_wrong_refresh_token()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken.'test',
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken,
                ])->post('/api/rd/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * 問合せの一覧が取得できること
     * @test
     */
    public function user_can_get_request_list()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $requestCnt = 3;
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        for ($i = 0; $i < $requestCnt; $i++) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $createResident->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'ai_category' => strval($i),
                'ai_sub_category' => $i.'-1',
            ]);
            array_push($createRequests, $createRequest);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
        }

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ一覧取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/request');

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/rd/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === $requestCnt);
        $this->assertTrue($requests->total === $requestCnt);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 問合せの一覧が取得できること
     * ( filter == 'future')
     * @test
     */
    public function user_can_get_request_list_filter_is_future()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $statuses = [
            config('const.Request.Status.WAITING_GRASPED'),
            config('const.Request.Status.WAITING_FOR_OPEN'),
            config('const.Request.Status.OPEN'),
            config('const.Request.Status.BID'),
            config('const.Request.Status.SCHEDULING'),
            config('const.Request.Status.IN_PROGRESS'),
            config('const.Request.Status.COMPLETED'),
            config('const.Request.Status.DELETED'),
            config('const.Request.Status.CANCELLED'),
            config('const.Request.Status.TRANSFERRED'),
            config('const.Request.Status.AUTO_COMPLETED'),
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($statuses as $i => $status) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $createResident->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'status' => $status,
                'ai_category' => strval($i),
                'ai_sub_category' => $i.'-1',
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
        }

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ一覧取得
        $filter = 'future';
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/request?filter='.$filter);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/rd/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 5);
        $this->assertTrue($requests->total === 5);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 問合せの一覧が取得できること
     * ( filter == 'past')
     * @test
     */
    public function user_can_get_request_list_filter_is_past()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $statuses = [
            config('const.Request.Status.WAITING_GRASPED'),
            config('const.Request.Status.WAITING_FOR_OPEN'),
            config('const.Request.Status.OPEN'),
            config('const.Request.Status.BID'),
            config('const.Request.Status.SCHEDULING'),
            config('const.Request.Status.IN_PROGRESS'),
            config('const.Request.Status.COMPLETED'),
            config('const.Request.Status.DELETED'),
            config('const.Request.Status.CANCELLED'),
            config('const.Request.Status.TRANSFERRED'),
            config('const.Request.Status.AUTO_COMPLETED'),
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($statuses as $i => $status) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $createResident->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'status' => $status,
                'ai_category' => strval($i),
                'ai_sub_category' => $i.'-1',
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
        }

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ一覧取得
        $filter = 'past';
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/request?filter='.$filter);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/rd/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/rd/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 6);
        $this->assertTrue($requests->total === 6);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 問合せの詳細が取得できること
     * @test
     */
    public function user_can_get_request_info()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ一覧取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/request/'.$createRequest->request_id);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($request->request_id == $createRequest->request_id);
        $this->assertTrue($request->resident_name === $createResident->name);
        $this->assertTrue($request->resident_tel === $createResident->tel);
        $this->assertTrue($request->property_name === $createProperty->name);
        $this->assertTrue($request->property_address === $createProperty->address);
        $this->assertTrue($request->room_no === $createRoom->room_no);
        $this->assertTrue($request->status === $createRequest->status);
        $this->assertTrue($request->emergency_flg === $createRequest->emergency_flg);
        $this->assertTrue($request->ai_category === $createRequest->ai_category);
        $this->assertTrue($request->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($request->context === $createContent->context);
        $this->assertTrue($request->response === $createContent->response);
        foreach ($request->preferable_info as $i => $preferableInfo) {
            $this->assertTrue($preferableInfo['request_preferable_time_id'] === $createPreerableTimes[$i]->request_preferable_time_id);
            $this->assertTrue(str_replace('-', '/', $preferableInfo['date']) === $createPreerableTimes[$i]->date);
            $this->assertTrue($preferableInfo['time_section_id'] === $createPreerableTimes[$i]->time_section_id);
        }
        $this->assertTrue($request->images === array());
        $this->assertTrue($request->cancel_reason === null);
    }

    /**
     * 他の入居者の問合せの詳細が取得できないこと
     * @test
     */
    public function user_cannot_get_request_info_of_other_resident()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createOtherRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
            'room_code' => '1111111111',
            'room_no' => '102',
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createOtherResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createOtherRoom->room_id,
            'resident_code' => '9999999999',
        ]);
        $createOtherRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createOtherResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createOtherContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createOtherRequest->request_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createOtherRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ一覧取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/request/'.$createOtherRequest->request_id);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 希望時間区分が取得できること
     * @test
     */
    public function user_can_get_time_sections()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //希望時間区分取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/timeSection');

        $request = $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue(count($request) !== 0);
    }

    /**
     * 問合せの登録ができること
     * (ai_category == communal_area)
     * @test
     */
    public function user_can_create_request_ai_category_communal_area()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.COMMUNAL_AREA'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //問合せ登録
        $context = '3階の廊下が汚れているので掃除して欲しい';
        $data = [
            'context' => $context,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$responseParams->access_token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->request_id !== '');
        $this->assertTrue($responseParams->request_type === config('const.Request.RequestType.IN_HOUSE'));
        $this->assertTrue($responseParams->response_text === '');
        $this->assertTrue($responseParams->may_unknown === false);
        $this->assertTrue($responseParams->can_order === false);
        $this->assertTrue($responseParams->support_center === false);

        $request = \App\Models\Request::find($responseParams->request_id);
        $this->assertTrue($request->request_id == $responseParams->request_id);
        $this->assertTrue($request->resident_id === $createResident->resident_id);
        $this->assertTrue($request->management_company_id === $createResident->management_company_id);
        $this->assertTrue($request->visibility === config('const.Request.Visibility.IN_HOUSE'));
        $this->assertTrue($request->status === config('const.Request.Status.TRANSFERRED'));
        $this->assertTrue($request->request_type === config('const.Request.RequestType.IN_HOUSE'));
        $this->assertTrue($request->ai_category === config('const.AiCategory.COMMUNAL_AREA'));
        $this->assertTrue($request->ai_sub_category === '');
        $this->assertTrue($request->emergency_flg == false);
        $this->assertTrue($request->summary === null);
        $this->assertTrue($request->rating === null);

        $requestContent = \App\Models\RequestContent::where('request_id', $responseParams->request_id)->first();
        $this->assertTrue($requestContent->request_id == $responseParams->request_id);
        $this->assertTrue($requestContent->context == $context);
        $this->assertTrue($requestContent->response == $responseParams->response_text);
    }

    /**
     * 問合せの登録ができること
     * (ai_category == management_company)
     * @test
     */
    public function user_can_create_request_ai_category_management_company()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.MANAGEMENT_COMPANY'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
                    ])
                    ->post('/api/rd/v1/auth/login', $data);
        $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
        $responseParams = (Object) $response->decodeResponseJson();

        //問合せ登録
        $context = 'wifiが繋がらなくなった';
        $data = [
            'context' => $context,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$responseParams->access_token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->request_id !== '');
        $this->assertTrue($responseParams->request_type === config('const.Request.RequestType.IN_HOUSE'));
        $this->assertTrue($responseParams->response_text === '');
        $this->assertTrue($responseParams->may_unknown === false);
        $this->assertTrue($responseParams->can_order === false);
        $this->assertTrue($responseParams->support_center === false);

        $request = \App\Models\Request::find($responseParams->request_id);
        $this->assertTrue($request->request_id == $responseParams->request_id);
        $this->assertTrue($request->resident_id === $createResident->resident_id);
        $this->assertTrue($request->management_company_id === $createResident->management_company_id);
        $this->assertTrue($request->visibility === config('const.Request.Visibility.IN_HOUSE'));
        $this->assertTrue($request->status === config('const.Request.Status.TRANSFERRED'));
        $this->assertTrue($request->request_type === config('const.Request.RequestType.IN_HOUSE'));
        $this->assertTrue($request->ai_category === config('const.AiCategory.MANAGEMENT_COMPANY'));
        $this->assertTrue($request->ai_sub_category === '');
        $this->assertTrue($request->emergency_flg == false);
        $this->assertTrue($request->summary === null);
        $this->assertTrue($request->rating === null);

        $requestContent = \App\Models\RequestContent::where('request_id', $responseParams->request_id)->first();
        $this->assertTrue($requestContent->request_id == $responseParams->request_id);
        $this->assertTrue($requestContent->context == $context);
        $this->assertTrue($requestContent->response == $responseParams->response_text);
    }

    /**
     * 問合せの登録ができること
     * (ai_category == support_center)
     * @test
     */
    public function user_can_create_request_ai_category_support_center()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.SUPPORT_CENTER'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $context = 'マンションの敷地内で落とし物を拾いました';
        $data = [
            'context' => $context,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->request_id !== '');
        $this->assertTrue($responseParams->request_type === config('const.Request.RequestType.AUTO_RESPONSE'));
        //$this->assertTrue($responseParams->response_text !== '');
        $this->assertTrue($responseParams->may_unknown === false);
        $this->assertTrue($responseParams->can_order === false);
        //$this->assertTrue($responseParams->support_center === false);

        $request = \App\Models\Request::find($responseParams->request_id);
        $this->assertTrue($request->request_id == $responseParams->request_id);
        $this->assertTrue($request->resident_id === $createResident->resident_id);
        $this->assertTrue($request->management_company_id === $createResident->management_company_id);
        $this->assertTrue($request->visibility === config('const.Request.Visibility.IN_HOUSE'));
        if ($responseParams->response_text === '') {
            $this->assertTrue($request->status === config('const.Request.Status.TRANSFERRED'));
        } else {
            $this->assertTrue($request->status === config('const.Request.Status.WAITING_FOR_OPEN'));
        }
        $this->assertTrue($request->request_type === config('const.Request.RequestType.AUTO_RESPONSE'));
        $this->assertTrue($request->ai_category === config('const.AiCategory.SUPPORT_CENTER'));
        $this->assertTrue($request->ai_sub_category === '');
        $this->assertTrue($request->emergency_flg == false);
        $this->assertTrue($request->summary === null);
        $this->assertTrue($request->rating === null);

        $requestContent = \App\Models\RequestContent::where('request_id', $responseParams->request_id)->first();
        $this->assertTrue($requestContent->request_id == $responseParams->request_id);
        $this->assertTrue($requestContent->context == $context);
        $this->assertTrue($requestContent->response == $responseParams->response_text);
    }

    // /**
    //  * 問合せの登録ができること
    //  * (ai_category == support_center)
    //  * response_textなしパターン
    //  * @test
    //  */
    // public function user_can_create_request_ai_category_support_center_and_response_nothing()
    // {
    //     $createMc = factory(\App\Models\ManagementCompany::class)->create();
    //     $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
    //         'management_company_id' => $createMc->management_company_id,
    //     ]);
    //     $createProperty = factory(\App\Models\Property::class)->create([
    //         'management_company_id' => $createMc->management_company_id,
    //     ]);
    //     $createRoom = factory(\App\Models\Room::class)->create([
    //         'management_company_id' => $createMc->management_company_id,
    //         'property_id' => $createProperty->property_id,
    //     ]);
    //     $createResident = factory(\App\Models\Resident::class)->create([
    //         'management_company_id' => $createMc->management_company_id,
    //         'room_id' => $createRoom->room_id,
    //     ]);

    //     //ログイン
    //     $data = [
    //         'property_code' => $createProperty->property_code,
    //         'room_code' => $createRoom->room_code,
    //         'resident_code' => $createResident->resident_code,
    //     ];
    //     $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

    //     //問合せ登録
    //     $context = 'あああ';
    //     $data = [
    //         'context' => $context,
    //     ];
    //     $response = $this->withHeaders([
    //                     'Authorization' => 'Bearer '.$token,
    //                 ])
    //                 ->post('/api/rd/v1/request', $data);

    //     $responseParams = (Object) $response->decodeResponseJson();
    //     $response->assertStatus(200);

    //     $this->assertTrue($responseParams->request_id !== '');
    //     $this->assertTrue($responseParams->request_type === config('const.Request.RequestType.AUTO_RESPONSE'));
    //     //$this->assertTrue($responseParams->response_text !== '');
    //     //$this->assertTrue($responseParams->may_unknown === false);
    //     $this->assertTrue($responseParams->can_order === false);
    //     //$this->assertTrue($responseParams->support_center === false);

    //     $request = \App\Models\Request::find($responseParams->request_id);
    //     $this->assertTrue($request->request_id == $responseParams->request_id);
    //     $this->assertTrue($request->resident_id === $createResident->resident_id);
    //     $this->assertTrue($request->management_company_id === $createResident->management_company_id);
    //     $this->assertTrue($request->visibility === config('const.Request.Visibility.IN_HOUSE'));
    //     if ($responseParams->response_text === '') {
    //         $this->assertTrue($request->status === config('const.Request.Status.TRANSFERRED'));
    //     } else {
    //         $this->assertTrue($request->status === config('const.Request.Status.WAITING_FOR_OPEN'));
    //     }
    //     $this->assertTrue($request->request_type === config('const.Request.RequestType.AUTO_RESPONSE'));
    //     $this->assertTrue($request->ai_category === config('const.AiCategory.SUPPORT_CENTER'));
    //     $this->assertTrue($request->ai_sub_category === '');
    //     $this->assertTrue($request->emergency_flg == false);
    //     $this->assertTrue($request->summary === null);
    //     $this->assertTrue($request->rating === null);

    //     $requestContent = \App\Models\RequestContent::where('request_id', $responseParams->request_id)->first();
    //     $this->assertTrue($requestContent->request_id == $responseParams->request_id);
    //     $this->assertTrue($requestContent->context == $context);
    //     $this->assertTrue($requestContent->response == $responseParams->response_text);
    // }

    /**
     * 問合せの登録ができること
     * (ai_category == vendor)
     * @test
     */
    public function user_can_create_request_ai_category_vendor()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $context = 'エアコンから水が漏れています';
        $data = [
            'context' => $context,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->request_id !== '');
        $this->assertTrue($responseParams->request_type === config('const.Request.RequestType.VENDOR'));
        //$this->assertTrue($responseParams->response_text !== '');
        $this->assertTrue($responseParams->may_unknown === false);
        $this->assertTrue($responseParams->can_order === true);
        $this->assertTrue($responseParams->support_center === false);

        $request = \App\Models\Request::find($responseParams->request_id);
        $this->assertTrue($request->request_id == $responseParams->request_id);
        $this->assertTrue($request->resident_id === $createResident->resident_id);
        $this->assertTrue($request->management_company_id === $createResident->management_company_id);
        $this->assertTrue($request->visibility === config('const.Request.Visibility.PUBLIC'));
        $this->assertTrue($request->status === config('const.Request.Status.WAITING_FOR_OPEN'));
        $this->assertTrue($request->request_type === config('const.Request.RequestType.VENDOR'));
        $this->assertTrue($request->ai_category === 'vendor');
        $this->assertTrue($request->ai_sub_category === '');
        $this->assertTrue($request->emergency_flg == false);
        $this->assertTrue($request->summary === null);
        $this->assertTrue($request->rating === null);

        $requestContent = \App\Models\RequestContent::where('request_id', $responseParams->request_id)->first();
        $this->assertTrue($requestContent->request_id == $responseParams->request_id);
        $this->assertTrue($requestContent->context == $context);
        $this->assertTrue($requestContent->response == $responseParams->response_text);
    }

    /**
     * 問合せの登録ができないこと
     * (問合せ空)
     * @test
     */
    public function user_cannot_create_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => '',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $response->assertStatus(400);
    }

    /**
     * 問合せ出動依頼ができること
     * （emergency_flg == false）
     * @test
     */
    public function user_can_order_request_not_emergency()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'assigned_vendors' => $createVn->vendor_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => 'エアコンから水が漏れています',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $requestId = $responseParams->request_id;

        //出動依頼
        $paramPreferableTimes = [
            ['date' => '2020/07/15', 'time_section_id' => '1', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '2', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '3', 'selected' => false],
        ];
        $emergencyFlg = 'false';
        $data = [
            'emergency_flg' => $emergencyFlg,
            'preferable_times' => $paramPreferableTimes
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        $response->assertStatus(201);
        $request = \App\Models\Request::find($requestId);
        $this->assertTrue($request->status === config('const.Request.Status.OPEN'));
        $this->assertTrue($request->emergency_flg == Cmn::str2bool($emergencyFlg));

        $preferableTimes = \App\Models\RequestPreferableTime::where('request_id', $requestId)->orderByRaw('priority ASC')->get();
        foreach ($preferableTimes as $idx => $preferableTime) {
            $this->assertTrue($preferableTime->priority === ($idx + 1));
            $this->assertTrue(str_replace('-', '/', $preferableTime->date) === $paramPreferableTimes[$idx]['date']);
            $this->assertTrue($preferableTime->time_section_id == $paramPreferableTimes[$idx]['time_section_id']);
            $this->assertTrue($preferableTime->selected == false);
        }

        $rvp = \App\Models\RequestVendorPermission::where('request_id', $requestId)->first();
        $this->assertTrue($rvp->vendor_id === $createVn->vendor_id);
    }

    /**
     * 問合せ出動依頼ができること
     * （emergency_flg == true）
     * @test
     */
    public function user_can_order_request_emergency()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'assigned_vendors' => $createVn->vendor_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => 'エアコンから水が漏れています',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $requestId = $responseParams->request_id;

        //出動依頼
        $paramPreferableTimes = [
            ['date' => '2020/07/15', 'time_section_id' => '1', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '2', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '3', 'selected' => false],
        ];
        $emergencyFlg = 'true';
        $data = [
            'emergency_flg' => $emergencyFlg,
            'preferable_times' => $paramPreferableTimes
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        $response->assertStatus(201);
        $request = \App\Models\Request::find($requestId);
        $this->assertTrue($request->status === config('const.Request.Status.OPEN'));
        $this->assertTrue($request->emergency_flg == Cmn::str2bool($emergencyFlg));

        $preferableTimes = \App\Models\RequestPreferableTime::where('request_id', $requestId)->orderByRaw('priority ASC')->get();
        $this->assertTrue(count($preferableTimes) === 0);

        $rvp = \App\Models\RequestVendorPermission::where('request_id', $requestId)->first();
        $this->assertTrue($rvp->vendor_id === $createVn->vendor_id);
    }

    /**
     * 問合せ出動依頼ができること
     * （emergency_flg == true & 指定業者なし）
     * @test
     */
    public function user_can_order_request_emergency_dont_have_assigned_vendor()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => 'エアコンから水が漏れています',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $requestId = $responseParams->request_id;

        //出動依頼
        $paramPreferableTimes = [
            ['date' => '2020/07/15', 'time_section_id' => '1', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '2', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '3', 'selected' => false],
        ];
        $emergencyFlg = 'true';
        $data = [
            'emergency_flg' => $emergencyFlg,
            'preferable_times' => $paramPreferableTimes
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        $response->assertStatus(201);
        $request = \App\Models\Request::find($requestId);
        $this->assertTrue($request->status === config('const.Request.Status.OPEN'));
        $this->assertTrue($request->emergency_flg == Cmn::str2bool($emergencyFlg));

        $preferableTimes = \App\Models\RequestPreferableTime::where('request_id', $requestId)->orderByRaw('priority ASC')->get();
        $this->assertTrue(count($preferableTimes) === 0);

        $rvp = \App\Models\RequestVendorPermission::where('request_id', $requestId)->first();
        $this->assertTrue($rvp === null);
    }

    /**
     * 問合せ出動依頼ができないこと
     * （status != waitng_for_open）
     * @test
     */
    public function user_cannot_order_request_status_is_not_waiting_for_open()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'assigned_vendors' => $createVn->vendor_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => 'エアコンから水が漏れています',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $requestId = $responseParams->request_id;

        //出動依頼
        $paramPreferableTimes = [
            ['date' => '2020/07/15', 'time_section_id' => '1', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '2', 'selected' => false],
            ['date' => '2020/07/15', 'time_section_id' => '3', 'selected' => false],
        ];
        $emergencyFlg = 'true';
        $data = [
            'emergency_flg' => $emergencyFlg,
            'preferable_times' => $paramPreferableTimes
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        //2回目（ここで失敗）
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        $response->assertStatus(400);
    }

    /**
     * 問合せ出動依頼ができないこと
     * （緊急じゃない且つ希望日なし）
     * @test
     */
    public function user_cannot_order_request_not_emergency_and_not_preferable_times()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'assigned_vendors' => $createVn->vendor_id,
        ]);

        //スタブ生成
        Http::fake([
            env('AI_SERVER_HOST').'/api/v1/answer*' =>
            Http::response([
                'ai_category' => config('const.AiCategory.VENDOR'),
                'confidence' => 0.9983190894126892,
                'may_unknown' => false,
                'specific' => null,
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ登録
        $data = [
            'context' => 'エアコンから水が漏れています',
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request', $data);

        $responseParams = (Object) $response->decodeResponseJson();
        $requestId = $responseParams->request_id;

        //出動依頼
        $paramPreferableTimes = [];
        $emergencyFlg = 'false';
        $data = [
            'emergency_flg' => $emergencyFlg,
            'preferable_times' => $paramPreferableTimes
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$requestId.'/order', $data);

        $response->assertStatus(400);
    }

    /**
     * 問合せへのキャンセルができること
     * (status == open)
     * @test
     */
    public function user_can_cancel_request_status_open()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //キャンセル
        $data = [
            'cancel_reason' => 'キャンセル理由'
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/cancel', $data);

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.CANCELLED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
        $rcr = \App\Models\RequestCancelReason::find($createRequest->request_id);
        $this->assertTrue($rcr->content === $data['cancel_reason']);
    }

    /**
     * 問合せへのキャンセルができること
     * (status ==  bid)
     * @test
     */
    public function user_can_cancel_request_status_bid()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'bid',
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //キャンセル
        $data = [
            'cancel_reason' => 'キャンセル理由'
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/cancel', $data);

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.CANCELLED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
        $rcr = \App\Models\RequestCancelReason::find($createRequest->request_id);
        $this->assertTrue($rcr->content === $data['cancel_reason']);
    }

    /**
     * 問合せへのキャンセルができること
     * (status == scheduling)
     * @test
     */
    public function user_can_cancel_request_status_scheduling()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $createUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //キャンセル
        $data = [
            'cancel_reason' => 'キャンセル理由'
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/cancel', $data);

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.WAITING_FOR_CANCEL'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
        $rcr = \App\Models\RequestCancelReason::find($createRequest->request_id);
        $this->assertTrue($rcr->content === $data['cancel_reason']);
    }

    /**
     * 問合せへのキャンセルができないこと
     * (status != open, scheduling)
     * @test
     */
    public function user_cannot_cancel_request_status_is_not_open_and_scheduling()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'completed',
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $createUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //キャンセル
        $data = [
            'cancel_reason' => 'キャンセル理由'
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/cancel', $data);

        $response->assertStatus(400);
    }

    /**
     * 問合せへのキャンセルができないこと
     * （バリデーション必須エラー）
     * @test
     */
    public function user_cannot_cancel_request_validation_error()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVn = factory(\App\Models\Vendor::class)->create();
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'completed',
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $createUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //キャンセル
        $data = [
            'cancel_reason' => ''
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/cancel', $data);

        $response->assertStatus(400);
        $this->assertTrue(in_array('キャンセル理由は必ず指定してください。' ,$response['errors']));
    }

    /**
     * 問合せへ入札承認ができること
     * @test
     */
    public function user_can_approve_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => config('const.Request.Status.BID'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //入札承認
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/approve');

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.SCHEDULING'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
    }

    /**
     * 問合せへ入札承認ができないこと
     * status !== bid
     * @test
     */
    public function user_can_not_approve_request_status_is_not_bid()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => config('const.Request.Status.OPEN'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //入札承認
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/approve');

        $response->assertStatus(400);
    }

    /**
     * 問合せへ入札承認ができないこと
     * 他入居者の問合せ
     * @test
     */
    public function user_can_not_approve_request_other_resident_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $dummyRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $dummyResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $dummyRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $dummyResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => config('const.Request.Status.BID'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //入札承認
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/approve');

        $response->assertStatus(400);
    }

    /**
     * 管理会社情報が取得できること
     * (サポートセンター電話番号なし)
     * @test
     */
    public function user_can_get_management_company_info_1()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //管理会社情報取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/managementCompany');

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($request->name === $createMc->name);
        $this->assertTrue($request->support_tel === $createMc->tel);
    }

    /**
     * 管理会社情報が取得できること
     * (サポートセンター電話番号あり)
     * @test
     */
    public function user_can_get_management_company_info_2()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //管理会社情報取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/managementCompany');

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($request->name === $createMc->name);
        $this->assertTrue($request->support_tel === $createMc->support_tel);
    }

    /**
     * 問合せ解決処理が行えること
     * （業者対応(DIY)・解決）
     * @test
     */
    public function user_can_solved_request_diy_solved()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = true;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.AUTO_COMPLETED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === 10);
    }

    /**
     * 問合せ解決処理が行えること
     * （業者対応(DIY)・未解決）
     * @test
     */
    public function user_can_solved_request_diy_not_solved()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = false;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === $createRequest->status);
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === 2);
    }

    /**
     * 問合せ解決処理が行えること
     * （業者対応(FAQ)・解決）
     * @test
     */
    public function user_can_solved_request_faq_solved()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.AUTO_RESPONSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = true;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.AUTO_COMPLETED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === 10);
    }

    /**
     * 問合せ解決処理が行えること
     * （業者対応(FAQ)・未解決）
     * @test
     */
    public function user_can_solved_request_faq_not_solved()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.AUTO_RESPONSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = false;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.TRANSFERRED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === 2);
    }

    /**
     * 問合せ解決処理が行えないこと
     * （問合せが他人のもの）
     * @test
     */
    public function user_can_not_solved_request_another_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $dummyRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
            'room_code' => '1234567890',
        ]);
        $dummyResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $dummyRoom->room_id,
            'resident_code' => '99999999',
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $dummyResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.AUTO_RESPONSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = true;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 問合せ解決処理が行えないこと
     * （ステータスが'waiting_for_open'でない）
     * @test
     */
    public function user_can_not_solved_status_is_not_waiting_for_open()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = true;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 問合せ解決処理が行えないこと
     * （リクエストタイプが'vendor','auto_response'でない）
     * @test
     */
    public function user_can_not_solved_invalid_request_type()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.IN_HOUSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せ解決処理
        $solved = true;
        $data = [
            'solved' => $solved,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/solved', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 問合せフィードバックが行えること
     * @test
     */
    public function user_can_feedback_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.IN_HOUSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せフィードバック
        $feedbackContent = 'フィードバックテスト';
        $data = [
            'feedback_content' => $feedbackContent,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/feedback', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $requestFeedback = \App\Models\RequestFeedback::where('request_id', $createRequest->request_id)->first();
        $this->assertTrue($requestFeedback->request_id == $createRequest->request_id);
        $this->assertTrue($requestFeedback->resident_id === $createRequest->resident_id);
        $this->assertTrue($requestFeedback->feedback_content === $feedbackContent);
    }

    /**
     * 問合せフィードバックが行えないこと
     * （他者の問合せのフィードバック）
     * @test
     */
    public function user_can_not_feedback_request_other_resident_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $dummyRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
            'room_code' => '1234567890',
        ]);
        $dummyResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $dummyRoom->room_id,
            'resident_code' => '99999999',
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $dummyResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.IN_HOUSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せフィードバック
        $feedbackContent = 'フィードバックテスト';
        $data = [
            'feedback_content' => $feedbackContent,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/feedback', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 問合せフィードバックが行えないこと
     * （フィードバック内容なし）
     * @test
     */
    public function user_can_not_feedback_request_content_is_nothing()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create([
            'support_tel' => '0120-999-999',
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.IN_HOUSE'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //問合せフィードバック
        $feedbackContent = '';
        $data = [
            'feedback_content' => $feedbackContent,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/feedback', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);

        $feedbackContent = null;
        $data = [
            'feedback_content' => $feedbackContent,
        ];
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->post('/api/rd/v1/request/'.$createRequest->request_id.'/feedback', $data);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * バッジ表示用情報の取得が行えること
     * @test
     */
    public function user_can_get_badge_info()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $data = [
            [
                //1件目
                'status' => config('const.Request.Status.OPEN'),
                'updated_at' => Carbon::now(),
            ],
            [
                //2件目
                'status' => config('const.Request.Status.OPEN'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //3件目
                'status' => config('const.Request.Status.SCHEDULING'),
                'updated_at' => Carbon::now(),
            ],
            [
                //4件目
                'status' => config('const.Request.Status.SCHEDULING'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //5件目
                'status' => config('const.Request.Status.IN_PROGRESS'),
                'updated_at' => Carbon::now(),
            ],
            [
                //6件目
                'status' => config('const.Request.Status.IN_PROGRESS'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //7件目
                'status' => config('const.Request.Status.COMPLETED'),
                'updated_at' => Carbon::now(),
            ],
            [
                //8件目
                'status' => config('const.Request.Status.COMPLETED'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //9件目
                'status' => config('const.Request.Status.WAITING_FOR_CANCEL'),
                'updated_at' => Carbon::now(),
            ],
            [
                //10件目
                'status' => config('const.Request.Status.WAITING_FOR_CANCEL'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //11件目
                'status' => config('const.Request.Status.CANCELLED'),
                'updated_at' => Carbon::now(),
            ],
            [
                //12件目
                'status' => config('const.Request.Status.CANCELLED'),
                'updated_at' => Carbon::now()->subDay(),
            ],
            [
                //13件目
                'status' => config('const.Request.Status.TRANSFERRED'),
                'updated_at' => Carbon::now(),
            ],
            [
                //14件目
                'status' => config('const.Request.Status.AUTO_COMPLETED'),
                'updated_at' => Carbon::now(),
            ],
        ];
        foreach ($data as $d) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $createResident->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'status' => $d['status'],
                'updated_at' => $d['updated_at'],
            ]);
        }

        //******************************
        // ここから検証
        //******************************
        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //バッジ表示用情報取得
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer '.$token,
                    ])
                    ->get('/api/rd/v1/badge');

        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        //検証
        $this->assertTrue(1 === $responseParams->open);
        $this->assertTrue(1 === $responseParams->scheduling);
        $this->assertTrue(1 === $responseParams->in_progress);
        $this->assertTrue(1 === $responseParams->completed);
        $this->assertTrue(1 === $responseParams->waiting_for_cancel);
        $this->assertTrue(1 === $responseParams->cancelled);
    }

    /**
     * S3アップロード用URL取得が行えること
     * @test
     */
    public function user_can_get_S3_pre_signed_URL()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //S3アップロード用URL取得
        $data = [
            'files' => [
                'test.jpg',
                'test2.jpg',
                'test3.mp4'
            ],
        ];
        $response = $this->call('GET','/api/rd/v1/request/'.$createRequest->request_id.'/preSignedUrl', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue(count($responseParams) === count($data['files']));
    }

    /**
     * S3アップロード用URL取得が行えないこと
     * filesパラメータなし
     * @test
     */
    public function user_can_not_get_S3_pre_signed_URL_parameter_is_nothing()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $createResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //S3アップロード用URL取得
        $data = [];
        $response = $this->call('GET','/api/rd/v1/request/'.$createRequest->request_id.'/preSignedUrl', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * S3アップロード用URL取得が行えないこと
     * 別入居者の問合せ
     * @test
     */
    public function user_can_not_get_S3_pre_signed_URL_other_resident_request()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $dummyRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);
        $dummyResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $dummyRoom->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $dummyResident->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'status' => config('const.Request.Status.WAITING_FOR_OPEN'),
            'request_type' => config('const.Request.RequestType.VENDOR'),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //S3アップロード用URL取得
        $data = [
            'files' => [
                'test.jpg',
                'test2.jpg',
                'test3.mp4'
            ],
        ];
        $response = $this->call('GET','/api/rd/v1/request/'.$createRequest->request_id.'/preSignedUrl', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * ユーザーがログインIDを使ってログインできること
     * @test
     */
    public function user_can_login_at_login_id()
    {
        $loginId = $this->faker->regexify('[0-9]{15}');
        $password = $this->faker->password;
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createUser = factory(\App\Models\User::class)->create([
            'email' => $loginId,
            'password' => Hash::make($password),
        ]);
        $createUser->assignRole(config('const.Roles.RESIDENT'));
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'user_id' => $createUser->user_id,
        ]);

        //ログイン
        $data = [
            'login_id' => $loginId,
            'password' => $password,
        ];
        $response = $this->post('/api/rd/v1/auth/localLogin', $data);
        $response->assertStatus(200);

        $responseParams = (Object) $response->decodeResponseJson();
        $this->assertTrue($responseParams->access_token !== null);
        $this->assertTrue($responseParams->refresh_token !== null);
        $this->assertTrue($responseParams->resident != null);
    }

    /**
     * ユーザーがログインIDを使ってログインできないこと
     * ログインID間違い & パスワード間違い
     * @test
     */
    public function user_can_not_login_at_login_id_wrong_login_id_and_wrong_password()
    {
        $loginId = $this->faker->regexify('[0-9]{15}');
        $password = $this->faker->password;
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createUser = factory(\App\Models\User::class)->create([
            'email' => $loginId,
            'password' => Hash::make($password),
        ]);
        $createUser->assignRole(config('const.Roles.RESIDENT'));
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'user_id' => $createUser->user_id,
        ]);

        //**********************
        // ログインID間違い
        //**********************
        $data = [
            'login_id' => $this->faker->regexify('[0-9]{15}'),
            'password' => $password,
        ];
        $response = $this->post('/api/rd/v1/auth/localLogin', $data);
        $response->assertStatus(401);

        //**********************
        // パスワード間違い
        //**********************
        $data = [
            'login_id' => $loginId,
            'password' => $this->faker->password,
        ];
        $response = $this->post('/api/rd/v1/auth/localLogin', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがログインIDを使ってログインできないこと
     * 入居者パーミッションなし
     * @test
     */
    public function user_can_not_login_at_login_id_permission_denied()
    {
        $loginId = $this->faker->regexify('[0-9]{15}');
        $password = $this->faker->password;
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createUser = factory(\App\Models\User::class)->create([
            'email' => $loginId,
            'password' => Hash::make($password),
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'user_id' => $createUser->user_id,
        ]);

        //ログイン
        $data = [
            'login_id' => $loginId,
            'password' => $password,
        ];
        $response = $this->post('/api/rd/v1/auth/localLogin', $data);
        $response->assertStatus(401);
    }

}
