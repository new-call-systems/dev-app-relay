<?php

namespace Tests\Unit\api;

use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class VendorMemberTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->seed('TimeSectionSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ユーザーがログインできること
     * @test
     */
    public function user_can_login()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $response->assertStatus(200);

        $responseParams = (Object) $response->decodeResponseJson();
        $this->assertTrue($responseParams->access_token !== null);
        $this->assertTrue($responseParams->refresh_token !== null);
        $this->assertTrue($responseParams->vendor_member !== null);
    }

    /**
     * ユーザーがログインできないこと
     * （パスワード間違い）
     * @test
     */
    public function user_cannot_login_wrong_password()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'email' => $loginUser->email,
            'password' => 'unauthenticated',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがログインできないこと
     * （ユーザーが存在しない）
     * @test
     */
    public function user_cannot_login_wrong_user()
    {
        $data = [
            'email' => 'hogehoge@test.com',
            'password' => 'unauthenticated',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがログインできないこと
     * （パーミッションがない）
     * @test
     */
    public function user_cannot_login_wrong_permission()
    {
        $data = [
            'email' => 'so@test.com',
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できること
     * @test
     */
    public function user_can_reissue_token()
    {
        $pass = 'password';
        $loginUser = factory(\App\Models\User::class)->create([
            'password' => Hash::make($pass),
        ]);
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => $pass,
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken,
                ])->post('/api/vn/v1/auth/token', $data);
        $response->assertStatus(200);

        $responseParams = (Object) $response->decodeResponseJson();
        $this->assertTrue($responseParams->access_token !== $accessToken);
        $this->assertTrue($responseParams->refresh_token !== $refreshToken);
    }

    /**
     * アクセストークンが再発行できないこと
     * Authorizationヘッダが不正
     * @test
     */
    public function user_cannot_reissue_token_wrong_authorization_header()
    {
        $pass = 'password';
        $loginUser = factory(\App\Models\User::class)->create([
            'password' => Hash::make($pass),
        ]);
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => $pass,
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer Test '.$accessToken,
                ])->post('/api/vn/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * AuthorizationヘッダがBearerでない
     * @test
     */
    public function user_cannot_reissue_token_authorization_header_is_not_bearer()
    {
        $pass = 'password';
        $loginUser = factory(\App\Models\User::class)->create([
            'password' => Hash::make($pass),
        ]);
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => $pass,
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Basic '.$accessToken,
                ])->post('/api/vn/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * access_tokenが異なる
     * @test
     */
    public function user_cannot_reissue_token_wrong_access_token()
    {
        $pass = 'password';
        $loginUser = factory(\App\Models\User::class)->create([
            'password' => Hash::make($pass),
        ]);
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => $pass,
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken,
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken.'test',
                ])->post('/api/vn/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * アクセストークンが再発行できないこと
     * refresh_tokenが異なる
     * @test
     */
    public function user_cannot_reissue_token_wrong_refresh_token()
    {
        $pass = 'password';
        $loginUser = factory(\App\Models\User::class)->create([
            'password' => Hash::make($pass),
        ]);
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => $pass,
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);

        //アクセストークン再発行
        $responseParams = (Object) $response->decodeResponseJson();
        $accessToken = $responseParams->access_token;
        $refreshToken = $responseParams->refresh_token;
        $data = [
            'refresh_token' => $refreshToken.'test',
        ];
        $response = $this->withHeaders([
                    'Authorization' => 'Bearer '.$accessToken,
                ])->post('/api/vn/v1/auth/token', $data);
        $response->assertStatus(401);
    }

    /**
     * 問合せの一覧が取得できること（public && open）
     * @test
     */
    public function user_can_get_request_list_public_and_open()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $requestCnt = 3;
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        for ($i = 0; $i < $requestCnt; $i++) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'public',
                'status' => 'open',
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'context' => $i.'件目の問合せ',
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request');

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === $requestCnt);
        $this->assertTrue($requests->total === $requestCnt);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            $this->assertTrue($request['user_name'] === array());
        }
    }

    /**
     * 問合せの一覧が取得できること（パーミッション有）
     * また、所属業者以外のパーミッションの問合せは取得できないこと
     * @test
     */
    public function user_can_get_request_list_have_permission()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $createOtherVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $requestCnt = 3;
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        for ($i = 0; $i < $requestCnt; $i++) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'protected',
                'status' => 'open',
            ]);
            $createOtherRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'protected',
                'status' => 'open',
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
            //RequestVendorPermissions
            $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
            ]);
            $createOtherVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createOtherRequest->request_id,
                'vendor_id' => $createOtherVn->vendor_id,
            ]);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request');

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === $requestCnt);
        $this->assertTrue($requests->total === $requestCnt);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            $this->assertTrue($request['user_name'] === array());
        }
    }

    /**
     * 問合せの一覧が取得できること
     * (status filter)
     * @test
     */
    public function user_can_get_request_list_status_filter_on()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $statuses = [
            config('const.Request.Status.OPEN'),
            config('const.Request.Status.SCHEDULING'),
            config('const.Request.Status.IN_PROGRESS'),
            config('const.Request.Status.COMPLETED'),
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($statuses as $i => $status) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'public',
                'status' => $status,
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
            //RequestVendorPermissions
            $createRvp = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
            ]);
            array_push($createRequests, $createRequest);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $status = config('const.Request.Status.OPEN').','.config('const.Request.Status.SCHEDULING');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request?status='.$status);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 2);
        $this->assertTrue($requests->total === 2);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            $this->assertTrue($request['user_name'] === array());
        }
    }

    /**
     * 問合せの一覧が取得できること
     * (emergency flg filter)
     * @test
     */
    public function user_can_get_request_list_emergency_flg_filter_on()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $emergencies = [
            true,false,true,false,
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($emergencies as $i => $emergency) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'public',
                'status' => 'open',
                'emergency_flg' => $emergency,
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
            //RequestVendorPermissions
            $createRvp = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
            ]);
            array_push($createRequests, $createRequest);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $emergencyFlg = 'only';
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request?emergency_flg='.$emergencyFlg);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 2);
        $this->assertTrue($requests->total === 2);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            $this->assertTrue($request['user_name'] === array());
        }
    }

    /**
     * 問合せの一覧が取得できること
     * (date filter)
     * @test
     */
    public function user_can_get_request_list_date_filter_on()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $emergencies = [
            true,false,true,false,
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($emergencies as $i => $emergency) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'public',
                'status' => 'open',
                'emergency_flg' => $emergency,
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
            //RequestVendorPermissions
            $createRvp = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
            ]);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $date = Carbon::now()->addDay(2)->format('Y/m/d');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request?date='.$date);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 2);
        $this->assertTrue($requests->total === 2);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            $this->assertTrue($request['user_name'] === array());
        }
    }

    /**
     * 問合せの一覧が取得できること
     * (all filter)
     * @test
     */
    public function user_can_get_request_list_all_filter_on()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $parameters = [
            [
                'status' => config('const.Request.Status.OPEN'),
                'emergency_flg' => false,
            ],
            [
                'status' => config('const.Request.Status.SCHEDULING'),
                'emergency_flg' => false,
            ],
            [
                'status' => config('const.Request.Status.SCHEDULING'),
                'emergency_flg' => true,
            ],
            [
                'status' => config('const.Request.Status.SCHEDULING'),
                'emergency_flg' => false,
            ],
        ];
        $createRequests = array();
        $createRequestContents = array();
        $createPreferableTimes = array();
        foreach ($parameters as $i => $param) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => 'public',
                'status' => $param['status'],
                'emergency_flg' => $param['emergency_flg'],
            ]);
            array_push($createRequests, $createRequest);
            //RequestContents
            $createContent = factory(\App\Models\RequestContent::class)->create([
                'request_id' => $createRequest->request_id,
            ]);
            array_push($createRequestContents, $createContent);
            //RequestPreferableTimes
            $tmpPreferableTimes = array();
            for ($j = 1; $j <= 3; $j++) {
                //1:tomorrow 2days_later 3days_later
                //2:2days_later 3days_later 4days_later
                //3:3days_later 4days_later 5days_later
                //4:4days_later 5days_later 6days_later
                $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                    'request_id' => $createRequest->request_id,
                    'priority' => $j,
                    'date' => Carbon::now()->addDay($j + $i)->format('Y/m/d'),
                    'time_section_id' => $j,
                ]);
                array_push($tmpPreferableTimes, $createPreferableTime);
            }
            array_push($createPreferableTimes, $tmpPreferableTimes);
            //RequestVendorPermissions
            $createRvp = factory(\App\Models\RequestVendorPermission::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
            ]);
            //RequestHandleUsers
            if ($param['status'] === config('const.Request.Status.SCHEDULING')) {
                $createRhu = factory(\App\Models\RequestHandleUser::class)->create([
                    'request_id' => $createRequest->request_id,
                    'user_id' => $loginUserVnm->user_id,
                    'group_id' => $loginUserVnm->vendor_id,
                ]);
            }
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //一覧取得処理
        $status = config('const.Request.Status.SCHEDULING');
        $emergencyFlg = 'exclude';
        $date = Carbon::now()->addDay(5)->format('Y/m/d');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request?status='.$status.'&emergency_flg='.$emergencyFlg.'&date='.$date);

        $requests = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        $this->assertTrue($requests->current_page === 1);
        $this->assertTrue($requests->first_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->from === 1);
        $this->assertTrue($requests->last_page === 1);
        $this->assertTrue($requests->last_page_url === 'http://localhost/api/vn/v1/request?page=1');
        $this->assertTrue($requests->next_page_url === null);
        $this->assertTrue($requests->path === 'http://localhost/api/vn/v1/request');
        $this->assertTrue($requests->per_page === 10);
        $this->assertTrue($requests->prev_page_url === null);
        $this->assertTrue($requests->to === 1);
        $this->assertTrue($requests->total === 1);
        foreach ($requests->data as $request) {
            foreach ($createRequests as $createRequest) {
                if ($createRequest->request_id == $request['request_id']) {
                    $this->assertTrue($createRequest->status === $request['status']);
                    $this->assertTrue($createRequest->emergency_flg === $request['emergency_flg']);
                    $this->assertTrue($createRequest->ai_category === $request['ai_category']);
                    $this->assertTrue($createRequest->ai_sub_category === $request['ai_sub_category']);
                }
            }
            foreach ($createRequestContents as $createContent) {
                if ($createContent->request_id == $request['request_id']) {
                    $this->assertTrue($createContent->context === $request['context']);
                }
            }
            foreach ($createPreferableTimes as $createPreferableTime) {
                if ($createPreferableTime[0]->request_id == $request['request_id']) {
                    foreach ($request['preferable_info'] as $preferableInfo) {
                        foreach ($createPreferableTime as $createPt) {
                            if ($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']) {
                                $this->assertTrue($createPt->request_preferable_time_id == $preferableInfo['request_preferable_time_id']);
                                $this->assertTrue($createPt->date == str_replace('-', '/', $preferableInfo['date']));
                                $this->assertTrue($createPt->time_section_id == $preferableInfo['time_section_id']);
                                $this->assertTrue($createPt->selected == $preferableInfo['selected']);
                            }
                        }
                    }
                }
            }
            if ($request['status'] === config('const.Request.Status.SCHEDULING')) {
                $this->assertTrue($request['user_name'] === [$loginUserVnm->last_name.$loginUserVnm->first_name]);
            } else {
                $this->assertTrue($request['user_name'] === array());
            }

        }
    }

    /**
     * 問合せの詳細が取得できること
     * 権限がないのでマスキングされていること
     * @test
     */
    public function user_can_get_request_info_maskinged()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ詳細取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($request->request_id == $createRequest->request_id);
        $this->assertTrue($request->resident_name === '********');
        $this->assertTrue($request->resident_tel === '********');
        $this->assertTrue($request->property_name === '********');
        $this->assertTrue($request->property_address === '********');
        $this->assertTrue($request->room_no === '********');
        $this->assertTrue($request->status === $createRequest->status);
        $this->assertTrue($request->emergency_flg === $createRequest->emergency_flg);
        $this->assertTrue($request->ai_category === $createRequest->ai_category);
        $this->assertTrue($request->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($request->context === $createContent->context);
        $this->assertTrue($request->response === $createContent->response);
        $this->assertTrue($request->user_name === array());
        $this->assertTrue($request->is_admin === false);
        $this->assertTrue($request->images === array());
        foreach ($request->preferable_info as $i => $preferableInfo) {
            $this->assertTrue($preferableInfo['request_preferable_time_id'] === $createPreerableTimes[$i]->request_preferable_time_id);
            $this->assertTrue(str_replace('-', '/', $preferableInfo['date']) === $createPreerableTimes[$i]->date);
            $this->assertTrue($preferableInfo['time_section_id'] === $createPreerableTimes[$i]->time_section_id);
        }
    }

    /**
     * 問合せの詳細が取得できること
     * 権限があるのでマスキングされていないこと
     * @test
     */
    public function user_can_get_request_info_not_maskinged()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ詳細取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id);

        $request = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($request->request_id == $createRequest->request_id);
        $this->assertTrue($request->resident_name === $resident1->name);
        $this->assertTrue($request->resident_tel === $resident1->tel);
        $this->assertTrue($request->property_name === $property1->name);
        $this->assertTrue($request->property_address === $property1->address);
        $this->assertTrue($request->room_no === $room1->room_no);
        $this->assertTrue($request->status === $createRequest->status);
        $this->assertTrue($request->emergency_flg === $createRequest->emergency_flg);
        $this->assertTrue($request->ai_category === $createRequest->ai_category);
        $this->assertTrue($request->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($request->context === $createContent->context);
        $this->assertTrue($request->response === $createContent->response);
        $this->assertTrue($request->user_name === [$loginUserVnm->last_name.$loginUserVnm->first_name]);
        $this->assertTrue($request->is_admin === true);
        $this->assertTrue($request->images === array());
        $this->assertTrue($request->cancel_reason === null);
        foreach ($request->preferable_info as $i => $preferableInfo) {
            $this->assertTrue($preferableInfo['request_preferable_time_id'] === $createPreerableTimes[$i]->request_preferable_time_id);
            $this->assertTrue(str_replace('-', '/', $preferableInfo['date']) === $createPreerableTimes[$i]->date);
            $this->assertTrue($preferableInfo['time_section_id'] === $createPreerableTimes[$i]->time_section_id);
        }
    }

    /**
     * 問合せの詳細が取得できないこと
     * （権限がないので）
     * @test
     */
    public function user_cannot_get_request_info_dont_have_permission()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $createOtherVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'protected',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $createOtherVn->vendor_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ詳細取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id);

        $response->assertStatus(400);
    }

    /**
     * 問合せの詳細が取得できないこと
     * （権限がないので）
     * @test
     */
    public function user_cannot_get_request_info_()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ詳細取得処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id);

        $response->assertStatus(400);
    }

    /**
     * 問合せへの入札ができること
     * (visibility == public)
     * @test
     */
    public function user_can_bid_request_visibility_public()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $selectPreferableTimeId = $createPreerableTimes[0]->request_preferable_time_id;
        $data = [
            'request_preferable_time_id' => $selectPreferableTimeId,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid', $data);

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.BID'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg === $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->bid_date === Carbon::now()->format('Y-m-d'));
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);

        $rhu = \App\Models\RequestHandleUser::where(['request_id' => $createRequest->request_id, 'user_id' => $loginUser->user_id])->first();
        $this->assertTrue($rhu->request_id == $createRequest->request_id);
        $this->assertTrue($rhu->user_id === $loginUser->user_id);
        $this->assertTrue($rhu->group_id === $userVn->vendor_id);

        $rvp = \App\Models\RequestVendorPermission::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rvp->request_id == $createRequest->request_id);
        $this->assertTrue($rvp->vendor_id === $userVn->vendor_id);

        $rh = \App\Models\RequestHandle::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rh->request_id == $createRequest->request_id);
        $this->assertTrue($rh->vendor_id === $userVn->vendor_id);
        $this->assertTrue($rh->scheduled_time === null);
        $this->assertTrue($rh->worked_minute === null);
        $this->assertTrue($rh->description === null);

        $rpt = \App\Models\RequestPreferableTime::find($selectPreferableTimeId);
        $this->assertTrue($rpt->selected === true);
    }

    /**
     * 問合せへの入札ができること
     * (visibility == protected)
     * @test
     */
    public function user_can_bid_request_visibility_protected()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'protected',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $selectPreferableTimeId = $createPreerableTimes[0]->request_preferable_time_id;
        $data = [
            'request_preferable_time_id' => $selectPreferableTimeId,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid', $data);

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.BID'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg === $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->bid_date === Carbon::now()->format('Y-m-d'));
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);

        $rhu = \App\Models\RequestHandleUser::where(['request_id' => $createRequest->request_id, 'user_id' => $loginUser->user_id])->first();
        $this->assertTrue($rhu->request_id == $createRequest->request_id);
        $this->assertTrue($rhu->user_id === $loginUser->user_id);
        $this->assertTrue($rhu->group_id === $userVn->vendor_id);

        $rvp = \App\Models\RequestVendorPermission::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rvp->request_id == $createRequest->request_id);
        $this->assertTrue($rvp->vendor_id === $userVn->vendor_id);

        $rh = \App\Models\RequestHandle::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rh->request_id == $createRequest->request_id);
        $this->assertTrue($rh->vendor_id === $userVn->vendor_id);
        $this->assertTrue($rh->scheduled_time === null);
        $this->assertTrue($rh->worked_minute === null);
        $this->assertTrue($rh->description === null);

        $rpt = \App\Models\RequestPreferableTime::find($selectPreferableTimeId);
        $this->assertTrue($rpt->selected === true);
    }

    /**
     * 問合せへの入札ができること
     * (emergency_flg == true)
     * @test
     */
    public function user_can_bid_request_emergency_flg_true()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
            'emergency_flg' => true,
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid');

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.SCHEDULING'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->bid_date === Carbon::now()->format('Y-m-d'));
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);

        $rhu = \App\Models\RequestHandleUser::where(['request_id' => $createRequest->request_id, 'user_id' => $loginUser->user_id])->first();
        $this->assertTrue($rhu->request_id == $createRequest->request_id);
        $this->assertTrue($rhu->user_id === $loginUser->user_id);
        $this->assertTrue($rhu->group_id === $userVn->vendor_id);

        $rvp = \App\Models\RequestVendorPermission::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rvp->request_id == $createRequest->request_id);
        $this->assertTrue($rvp->vendor_id === $userVn->vendor_id);

        $rh = \App\Models\RequestHandle::where(['request_id' => $createRequest->request_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($rh->request_id == $createRequest->request_id);
        $this->assertTrue($rh->vendor_id === $userVn->vendor_id);
        $this->assertTrue($rh->scheduled_time === null);
        $this->assertTrue($rh->worked_minute === null);
        $this->assertTrue($rh->description === null);
    }

    /**
     * 問合せへの入札ができないこと
     * (status != open)
     * @test
     */
    public function user_cannot_bid_request_status_not_open()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid');

        $response->assertStatus(400);
    }

    /**
     * 問合せへの入札ができないこと
     * (visibility == protected && dont have permission)
     * @test
     */
    public function user_cannot_bid_request_protected_and_dont_have_permission()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'protected',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid');

        $response->assertStatus(400);
    }

    /**
     * 問合せへの入札ができないこと
     * (visibility == in_house)
     * @test
     */
    public function user_cannot_bid_request_in_house()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'in_house',
            'status' => 'open',
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid');

        $response->assertStatus(400);
    }

    /**
     * 問合せへの入札ができないこと
     * (emergency_flg == false && preferable_time_id nothing)
     * @test
     */
    public function user_cannot_bid_request_emergency_flg_is_false_and_preferable_time_nothing()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
            'line_user_id' => null,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
            'emergency_flg' => false,
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'request_id' => $createRequest->request_id,
        ]);
        // $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'vendor_id' => $userVn->vendor_id,
        // ]);
        $createPreerableTimes = array();
        for ($i = 1; $i <= 3; $i++) {
            $createPreferableTime = factory(\App\Models\RequestPreferableTime::class)->create([
                'request_id' => $createRequest->request_id,
                'priority' => $i,
                'date' => Carbon::now()->format('Y/m/d'),
                'time_section_id' => $i,
            ]);
            array_push($createPreerableTimes, $createPreferableTime);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //入札処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/bid');

        $response->assertStatus(400);
    }

    /**
     * 問合せへのキャンセル承認ができること
     * (status == waiting_for_cancel)
     * @test
     */
    public function user_can_cancel_request()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //キャンセル処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/cancel');

        $response->assertStatus(201);

        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === config('const.Request.Status.CANCELLED'));
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
    }

    /**
     * 問合せへのキャンセル承認ができないこと
     * (status != waiting_for_cancel)
     * @test
     */
    public function user_cannot_cancel_request_status_is_not_waiting_for_cancel()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
            'emergency_flg' => true,
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //キャンセル処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/cancel');

        $response->assertStatus(400);
    }

    /**
     * 問合せへのキャンセル承認ができないこと
     * (user is not handler)
     * @test
     */
    public function user_cannot_cancel_request_user_is_not_handler()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //キャンセル処理
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/cancel');

        $response->assertStatus(400);
    }

    /**
     * 問合せ業者対応一覧の取得ができること
     * (業者用問合せアクセス権限のあるもののみ取得されること)
     * @test
     */
    public function user_can_get_request_handle_list()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $dummyUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $dummyUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $dummyVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $dummyVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $dummyUser->user_id,
            'vendor_id' => $dummyVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $dummyRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $createVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        $dummyVendorPermission = factory(\App\Models\RequestVendorPermission::class)->create([
            'request_id' => $dummyRequest->request_id,
            'vendor_id' => $dummyVn->vendor_id,
        ]);

        $requestHandleCnt = 3;
        $createRequestHandles = array();
        for ($i = 0; $i < $requestHandleCnt; $i++) {
            $createRequestHandle = factory(\App\Models\RequestHandle::class)->create([
                'request_id' => $createRequest->request_id,
                'vendor_id' => $userVn->vendor_id,
                'description' => '説明'.$i
            ]);
            array_push($createRequestHandles, $createRequestHandle);
            $dummyRequestHandle = factory(\App\Models\RequestHandle::class)->create([
                'request_id' => $dummyRequest->request_id,
                'vendor_id' => $dummyVn->vendor_id,
                'description' => '説明'.$i
            ]);
        }

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ業者対応一覧取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/handlers');

        $response->assertStatus(200);
        $requestHandles = (Object) $response->decodeResponseJson();
        $this->assertTrue($requestHandles->current_page === 1);
        $this->assertTrue($requestHandles->first_page_url === 'http://localhost/api/vn/v1/request/'.$createRequest->request_id.'/handlers?page=1');
        $this->assertTrue($requestHandles->from === 1);
        $this->assertTrue($requestHandles->last_page === 1);
        $this->assertTrue($requestHandles->last_page_url === 'http://localhost/api/vn/v1/request/'.$createRequest->request_id.'/handlers?page=1');
        $this->assertTrue($requestHandles->next_page_url === null);
        $this->assertTrue($requestHandles->path === 'http://localhost/api/vn/v1/request/'.$createRequest->request_id.'/handlers');
        $this->assertTrue($requestHandles->per_page === 10);
        $this->assertTrue($requestHandles->prev_page_url === null);
        $this->assertTrue($requestHandles->to === $requestHandleCnt);
        $this->assertTrue($requestHandles->total === $requestHandleCnt);
        foreach ($requestHandles->data as $requestHandle) {
            foreach ($createRequestHandles as $createRequestHandle) {
                if ($requestHandle['request_handle_id'] == $createRequestHandle->request_handle_id) {
                    $this->assertTrue($requestHandle['request_handle_id'] == $createRequestHandle->request_handle_id);
                    $this->assertTrue($requestHandle['request_id'] == $createRequestHandle->request_id);
                    $this->assertTrue(str_replace('-', '/', $requestHandle['scheduled_time']) == $createRequestHandle->scheduled_time);
                    $this->assertTrue($requestHandle['worked_minute'] == $createRequestHandle->worked_minute);
                    $this->assertTrue($requestHandle['description'] == $createRequestHandle->description);
                }
            }
        }
    }

    /**
     * 問合せ業者対応の更新ができること
     * @test
     */
    public function user_can_update_request_handle()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $createHandle = factory(\App\Models\RequestHandle::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ業者対応更新処理
        $data = [
            'scheduled_time' => '2020-01-01 01:23:45',
            'description' => 'テスト説明\nテスト',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/handlers/'.$createHandle->request_handle_id, $data);

        $response->assertStatus(201);

        $updateRequestHandle = \App\Models\RequestHandle::find($createHandle->request_handle_id);
        $this->assertTrue($updateRequestHandle->request_handle_id == $createHandle->request_handle_id);
        $this->assertTrue($updateRequestHandle->request_id == $createHandle->request_id);
        $this->assertTrue($updateRequestHandle->vendor_id === $createHandle->vendor_id);
        $this->assertTrue($updateRequestHandle->scheduled_time === $data['scheduled_time']);
        $this->assertTrue($updateRequestHandle->worked_minute == $createHandle->worked_minute);
        $this->assertTrue($updateRequestHandle->description === $data['description']);
    }

    /**
     * 問合せ業者対応の更新ができないこと
     * (担当でない問合せ)
     * @test
     */
    public function user_cannot_update_request_handle_not_handler()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $createHandle = factory(\App\Models\RequestHandle::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        // $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'user_id' => $loginUser->user_id,
        //     'group_id' => $userVn->vendor_id,
        // ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ業者対応更新処理
        $data = [
            'scheduled_time' => '2020-01-01 01:23:45',
            'description' => 'テスト説明\nテスト',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/handlers/'.$createHandle->request_handle_id, $data);

        $response->assertStatus(400);
    }

    /**
     * 問合せ業者対応の更新ができないこと
     * (日時のフォーマット違い)
     * @test
     */
    public function user_cannot_update_request_handle_wrong_timestamp()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'waiting_for_cancel',
            'emergency_flg' => true,
        ]);
        $createHandle = factory(\App\Models\RequestHandle::class)->create([
            'request_id' => $createRequest->request_id,
            'vendor_id' => $userVn->vendor_id,
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //問合せ業者対応更新処理
        $data = [
            'scheduled_time' => '2020/01/01 01:23:45',
            'description' => 'テスト説明\nテスト',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/handlers/'.$createHandle->request_handle_id, $data);

        $response->assertStatus(400);
    }

    /**
     * 業者対応報告ができること
     * status in 'scheduling', 'in_progress', 'completed'
     * @test
     */
    public function user_can_report_request()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
            'emergency_flg' => true,
        ]);
        $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者対応報告処理
        //scheduling -> in_progress
        $data = [
            'status' => config('const.Request.Status.IN_PROGRESS'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id, $data);
        $response->assertStatus(201);
        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === $data['status']);
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);

        //in_progress -> completed
        $data = [
            'status' => config('const.Request.Status.COMPLETED'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id, $data);
        $response->assertStatus(201);
        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === $data['status']);
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);

        //completed -> scheduling
        $data = [
            'status' => config('const.Request.Status.SCHEDULING'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id, $data);
        $response->assertStatus(201);
        $updateRequest = \App\Models\Request::find($createRequest->request_id);
        $this->assertTrue($updateRequest->request_id == $createRequest->request_id);
        $this->assertTrue($updateRequest->resident_id === $createRequest->resident_id);
        $this->assertTrue($updateRequest->management_company_id === $createRequest->management_company_id);
        $this->assertTrue($updateRequest->visibility === $createRequest->visibility);
        $this->assertTrue($updateRequest->status === $data['status']);
        $this->assertTrue($updateRequest->request_type === $createRequest->request_type);
        $this->assertTrue($updateRequest->ai_category === $createRequest->ai_category);
        $this->assertTrue($updateRequest->ai_sub_category === $createRequest->ai_sub_category);
        $this->assertTrue($updateRequest->emergency_flg == $createRequest->emergency_flg);
        $this->assertTrue($updateRequest->summary === $createRequest->summary);
        $this->assertTrue($updateRequest->rating === $createRequest->rating);
    }

    /**
     * 業者対応報告ができないこと
     * status not in 'scheduling', 'in_progress', 'completed'
     * @test
     */
    public function user_cannot_report_request_wrong_status()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest1 = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'open',
            'emergency_flg' => true,
        ]);
        $createHandleUsers1 = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest1->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequest2 = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
            'emergency_flg' => true,
        ]);
        $createHandleUsers2 = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest2->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者対応報告処理
        //open -> scheduling
        $data = [
            'status' => config('const.Request.Status.SCHEDULING'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest1->request_id, $data);
        $response->assertStatus(400);

        //scheduling -> open
        $data = [
            'status' => config('const.Request.Status.OPEN'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest2->request_id, $data);
        $response->assertStatus(400);
    }

    /**
     * 業者対応報告ができないこと
     * user is not handler
     * @test
     */
    public function user_cannot_report_request_not_handler()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => 'public',
            'status' => 'scheduling',
            'emergency_flg' => true,
        ]);
        // $createHandleUsers = factory(\App\Models\RequestHandleUser::class)->create([
        //     'request_id' => $createRequest->request_id,
        //     'user_id' => $loginUser->user_id,
        //     'group_id' => $userVn->vendor_id,
        // ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者対応報告処理
        $data = [
            'status' => config('const.Request.Status.IN_PROGRESS'),
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id, $data);
        $response->assertStatus(400);
    }

    /**
     * 業者作業員情報の更新ができること
     * @test
     */
    public function user_can_update_vendor_member_info()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者作業員情報更新
        $data = [
            'last_name' => '姓テスト',
            'first_name' => '名テスト',
            'emergency_tel' => '090-1234-5678',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/vendorMember/'.$loginUser->user_id.'/'.$userVn->vendor_id, $data);
        $response->assertStatus(201);

        $updateVnm = \App\Models\VendorMember::where(['user_id' => $loginUser->user_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($updateVnm->user_id == $loginUserVnm->user_id);
        $this->assertTrue($updateVnm->vendor_id == $loginUserVnm->vendor_id);
        $this->assertTrue($updateVnm->last_name == $data['last_name']);
        $this->assertTrue($updateVnm->first_name == $data['first_name']);
        $this->assertTrue($updateVnm->emergency_tel == $data['emergency_tel']);
        $this->assertTrue($updateVnm->retire_flg == $loginUserVnm->retire_flg);
        $this->assertTrue($updateVnm->representative_flg == $loginUserVnm->representative_flg);
    }

    /**
     * 業者作業員情報の更新ができないこと
     * ( 別作業者を指定 )
     * @test
     */
    public function user_cannot_update_another_vendor_member_info()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $dummyUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $dummyVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $dummyUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者作業員情報更新
        $data = [
            'last_name' => '姓テスト',
            'first_name' => '名テスト',
            'emergency_tel' => '090-1234-5678',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/vendorMember/'.$dummyUser->user_id.'/'.$userVn->vendor_id, $data);
        $response->assertStatus(400);
    }

    /**
     * 業者作業員情報の更新ができないこと
     * ( バリデーションエラー )
     * @test
     */
    public function user_cannot_update_vendor_member_info_for_validation_error()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //業者作業員情報更新
        //必須
        $data = [
            'last_name' => '',
            'first_name' => '',
            'emergency_tel' => '',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/vendorMember/'.$loginUser->user_id.'/'.$userVn->vendor_id, $data);
        $response->assertStatus(400);
        $this->assertTrue(in_array('姓は必ず指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('名は必ず指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('緊急連絡先は必ず指定してください。' ,$response['errors']));
        //桁数、型
        $str256 = $this->faker->realText(256);
        $data = [
            'last_name' => $str256,
            'first_name' => $str256,
            'emergency_tel' => '12345678901234',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/vendorMember/'.$loginUser->user_id.'/'.$userVn->vendor_id, $data);
        $response->assertStatus(400);
        $this->assertTrue(in_array('姓は、255文字以下で指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('名は、255文字以下で指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('緊急連絡先は携帯電話番号を指定してください。' ,$response['errors']));
    }

    /**
     * 完了報告書の作成が行えること
     * （問合せステータス：completed）
     * @test
     */
    public function user_can_create_report_request_status_is_completed()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
            'bid_date' => Carbon::now()->format('Y-m-d'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書作成
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $dt = Carbon::now();
        $reportId = $responseParams->report_id;
        $report = \App\Models\RequestReport::find($reportId);

        $this->assertTrue($report->request_id == $createRequest->request_id);
        $this->assertTrue($report->author_id === $loginUser->user_id);
        $this->assertTrue($report->vendor_id === $userVn->vendor_id);
        $this->assertTrue($report->management_company_id === $createMc->management_company_id);
        $this->assertTrue($report->resident_id === $resident1->resident_id);
        $this->assertTrue($report->reception_date === $createRequest->bid_date);
        $this->assertTrue($report->report_status === config('const.Reports.Status.WRITING'));
        $this->assertTrue($report->action_status === config('const.Reports.ActionStatus.COMPLETED'));
        $this->assertTrue($report->description === '');
        $this->assertTrue($report->action_date === $dt->format('Y-m-d'));
        $this->assertTrue($report->starting_time === null);
        $this->assertTrue($report->ending_time === null);
        $this->assertTrue($report->comment === null);
        $this->assertTrue($report->action_section === '1');
        $this->assertTrue($report->construction_amount === null);
    }

    /**
     * 完了報告書の作成が行えること
     * （問合せステータス：in_progress）
     * @test
     */
    public function user_can_create_report_request_status_is_in_progress()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.IN_PROGRESS'),
            'bid_date' => Carbon::now()->format('Y-m-d'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書作成
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $dt = Carbon::now();
        $reportId = $responseParams->report_id;
        $report = \App\Models\RequestReport::find($reportId);

        $this->assertTrue($report->request_id == $createRequest->request_id);
        $this->assertTrue($report->author_id === $loginUser->user_id);
        $this->assertTrue($report->vendor_id === $userVn->vendor_id);
        $this->assertTrue($report->management_company_id === $createMc->management_company_id);
        $this->assertTrue($report->resident_id === $resident1->resident_id);
        $this->assertTrue($report->reception_date === $createRequest->bid_date);
        $this->assertTrue($report->report_status === config('const.Reports.Status.WRITING'));
        $this->assertTrue($report->action_status === config('const.Reports.ActionStatus.COMPLETED'));
        $this->assertTrue($report->description === '');
        $this->assertTrue($report->action_date === $dt->format('Y-m-d'));
        $this->assertTrue($report->starting_time === null);
        $this->assertTrue($report->ending_time === null);
        $this->assertTrue($report->comment === null);
        $this->assertTrue($report->action_section === '1');
        $this->assertTrue($report->construction_amount === null);
    }

    /**
     * 完了報告書の作成が行えないこと
     * （担当者じゃない問合せ）
     * @test
     */
    public function user_can_not_create_report_permission_denied()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $dummyUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $dummyUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書作成
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書の作成が行えないこと
     * （問合せステータスが完了じゃない）
     * @test
     */
    public function user_can_not_create_report_status_is_not_completed()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.OPEN'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書作成
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書の取得が行えること
     * @test
     */
    public function user_can_get_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'action_status' => config('const.Reports.ActionStatus.COMPLETED'),
            'action_section' => '1',
            'construction_amount' => 15000,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->report_id == $createRequestReport->request_report_id);
        $this->assertTrue($responseParams->management_company_name === $createMc->name);
        $this->assertTrue($responseParams->property_name === $property1->name);
        $this->assertTrue($responseParams->room_no === $room1->room_no);
        $this->assertTrue($responseParams->resident_name === $resident1->name);
        $this->assertTrue($responseParams->action_date === $createRequestReport->action_date);
        $this->assertTrue($responseParams->starting_time === $createRequestReport->starting_time.':00');
        $this->assertTrue($responseParams->ending_time === $createRequestReport->ending_time.':00');
        $this->assertTrue($responseParams->description === $createRequestReport->description);
        $this->assertTrue($responseParams->user_name === [$loginUserVnm->last_name.$loginUserVnm->first_name]);
        $this->assertTrue($responseParams->report_status === $createRequestReport->report_status);
        $this->assertTrue($responseParams->action_status === $createRequestReport->action_status);
        $this->assertTrue($responseParams->request_id == $createRequest->request_id);
        $this->assertTrue($responseParams->action_section == $createRequestReport->action_section);
        $this->assertTrue($responseParams->construction_amount == $createRequestReport->construction_amount);
        $this->assertTrue($responseParams->reception_date === $createRequestReport->reception_date);
    }

    /**
     * 完了報告書の取得が行えること
     * （所属業者の他作業員が担当者）
     * @test
     */
    public function user_can_get_report_2()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'action_status' => config('const.Reports.ActionStatus.COMPLETED'),
            'action_section' => '1',
            'construction_amount' => 15000,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->report_id == $createRequestReport->request_report_id);
        $this->assertTrue($responseParams->management_company_name === $createMc->name);
        $this->assertTrue($responseParams->property_name === $property1->name);
        $this->assertTrue($responseParams->room_no === $room1->room_no);
        $this->assertTrue($responseParams->resident_name === $resident1->name);
        $this->assertTrue($responseParams->action_date === $createRequestReport->action_date);
        $this->assertTrue($responseParams->starting_time === $createRequestReport->starting_time.':00');
        $this->assertTrue($responseParams->ending_time === $createRequestReport->ending_time.':00');
        $this->assertTrue($responseParams->description === $createRequestReport->description);
        $this->assertTrue($responseParams->user_name === [$loginUserVnm->last_name.$loginUserVnm->first_name]);
        $this->assertTrue($responseParams->report_status === $createRequestReport->report_status);
        $this->assertTrue($responseParams->action_status === $createRequestReport->action_status);
        $this->assertTrue($responseParams->request_id == $createRequest->request_id);
        $this->assertTrue($responseParams->action_section == $createRequestReport->action_section);
        $this->assertTrue($responseParams->construction_amount == $createRequestReport->construction_amount);
        $this->assertTrue($responseParams->reception_date === $createRequestReport->reception_date);
    }

    /**
     * 完了報告書の取得が行えないこと
     * （他業者の作業員が担当者）
     * @test
     */
    public function user_can_not_get_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $createVn2 = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $createVn2->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $createVn2->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $createVn2->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'action_status' => config('const.Reports.ActionStatus.COMPLETED'),
            'action_section' => '1',
            'construction_amount' => 15000,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書の更新が行えること
     * @test
     */
    public function user_can_update_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id, $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->description === $data['description']);
        $this->assertTrue($report->action_date === $data['action_date']);
        $this->assertTrue($report->starting_time === $data['starting_time'].':00');
        $this->assertTrue($report->ending_time === $data['ending_time'].':00');
        $this->assertTrue($report->action_status === $data['action_status']);
        $this->assertTrue($report->action_section === $data['action_section']);
        $this->assertTrue($report->construction_amount == $data['construction_amount']);
    }

    /**
     * 完了報告書の更新が行えないこと
     * （バリデーションエラー）
     * @test
     */
    public function user_can_not_update_report_validation_error()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書更新
        //必須チェック
        $data = [
            'description' => '',
            'action_date' => '',
            'starting_time' => '',
            'ending_time' => '',
            'action_status' => '',
            'action_section' => '',
            'construction_amount' => null,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id, $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
        $this->assertTrue(in_array('作業内容は必ず指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('工事日は必ず指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('ステータスは必ず指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('対応区分は必ず指定してください。' ,$response['errors']));

        //型、桁数チェック
        $str256 = $this->faker->realText(256);
        $data = [
            'description' => $str256,
            'action_date' => '2020/13/01',
            'starting_time' => '13:00:10',
            'ending_time' => '25:00',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '123',
            'construction_amount' => 'abc',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id, $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
        $this->assertTrue(in_array('工事日には有効な日付を指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('開始時間はH:i形式で指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('終了時間はH:i形式で指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('対応区分は、1文字以下で指定してください。' ,$response['errors']));
        $this->assertTrue(in_array('工事金額は整数で指定してください。' ,$response['errors']));
    }

    /**
     * 完了報告書の更新が行えないこと
     * （担当者でない完了報告書）
     * @test
     */
    public function user_can_not_update_report_permission_denied()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id, $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書の更新が行えないこと
     * （完了報告書のステータスが'writing'でない）
     * @test
     */
    public function user_can_not_update_report_status_is_not_writing()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->put('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id, $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書のステータス変更が行えること
     * @test
     */
    public function user_can_change_report_status()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書ステータス変更
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/draft');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(201);

        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === config('const.Reports.Status.DRAFT'));
    }

    /**
     * 完了報告書のステータス変更が行えないこと
     * （担当者でない完了報告書）
     * @test
     */
    public function user_can_not_change_report_status_permission_denied()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書ステータス変更
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/draft');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書のステータス変更が行えないこと
     * （完了報告書のステータスが'writing'でない）
     * @test
     */
    public function user_can_not_change_report_status_is_not_writing()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //完了報告書ステータス変更
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/draft');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * AWS Config取得が行えること
     * @test
     */
    public function user_can_get_aws_config()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //AWS Config取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/config');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->bucket_name === env('AWS_BUCKET'));
        $this->assertTrue($responseParams->region === env('AWS_DEFAULT_REGION'));
        $this->assertTrue($responseParams->identity_pool_id === env('AWS_IDENTITY_POOL_ID'));
        $this->assertTrue($responseParams->prefix === 'reports/'.$userVn->vendor_id.'/'.$createRequest->request_id.'/'.$createRequestReport->request_report_id);
    }

    /**
     * AWS Config取得が行えること
     * （完了報告書担当者が所属業者の作業員）
     * @test
     */
    public function user_can_get_aws_config_2()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //AWS Config取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/config');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        $this->assertTrue($responseParams->bucket_name === env('AWS_BUCKET'));
        $this->assertTrue($responseParams->region === env('AWS_DEFAULT_REGION'));
        $this->assertTrue($responseParams->identity_pool_id === env('AWS_IDENTITY_POOL_ID'));
        $this->assertTrue($responseParams->prefix === 'reports/'.$userVn->vendor_id.'/'.$createRequest->request_id.'/'.$createRequestReport->request_report_id);
    }

    /**
     * AWS Config取得が行えないこと
     * （完了報告書の担当者が他業者所属）
     * @test
     */
    public function user_can_not_get_aws_config()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $createVn2 = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $createVn2->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $createVn2->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $createVn2->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //AWS Config取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/request/'.$createRequest->request_id.'/reports/'.$createRequestReport->request_report_id.'/config');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(400);
    }

    /**
     * 完了報告書一覧取得が行えること
     * @test
     */
    public function user_can_get_report_list()
    {
        //******************************
        // 業者生成
        // 1:ログイン担当者
        // 2:同じ業者所属担当者
        // 3:別業者所属担当者
        //******************************
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $sameVnUser = factory(\App\Models\User::class)->create();
        $sameVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $otherVnUser = factory(\App\Models\User::class)->create();
        $otherVnUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $otherVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $sameVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $otherVnUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $otherVnUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        //******************************
        // 入居者生成
        // 1:物件1
        // 2:物件2
        // 3:物件1
        // 4:物件1
        // 5:物件2
        // 6:物件3
        // 7:物件1
        // 8:物件2
        //******************************
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property2 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $property3 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $room2 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property2->property_id,
        ]);
        $room3 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $room4 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $room5 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property2->property_id,
        ]);
        $room6 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property3->property_id,
        ]);
        $room7 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $room8 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property2->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $resident2 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room2->room_id,
        ]);
        $resident3 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room3->room_id,
        ]);
        $resident4 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room4->room_id,
        ]);
        $resident5 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room5->room_id,
        ]);
        $resident6 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room6->room_id,
        ]);
        $resident7 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room7->room_id,
        ]);
        $resident8 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room8->room_id,
        ]);

        //******************************
        // 問合せ、完了報告書生成
        // 1:writing   エアコン 担当者1
        // 2:writing   水道     担当者1
        // 3:draft     エアコン 担当者1
        // 4:completed 窓       担当者1
        // 5:writing   エアコン 担当者2
        // 6:draft     異音     担当者2
        // 7:writing   エアコン 担当者3
        // 8:completed 床       担当者3
        //******************************
        $createRequests = array();
        $createRequestContents = array();
        $createRequestReports = array();
        //1件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => 'エアコンが壊れました',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'description' => '再現しませんでした',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //2件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident2->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '水道から水が出ません',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident2->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'description' => '直しました',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //3件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident3->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => 'エアコンからする音がうるさい',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident3->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
            'description' => 'エアコンを取り換えました',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //4件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident4->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '窓が割れたので直してほしい',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident4->resident_id,
            'report_status' => config('const.Reports.Status.COMPLETED'),
            'description' => '窓を交換しました',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //5件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident5->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => 'エアコンがうるさいです',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident5->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'description' => '取り替えました',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //6件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident6->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => 'なんか部屋で異音がする',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $sameVnUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $sameVnUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident6->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
            'description' => 'エアコンからの異音でした、対応しました。',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //7件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident7->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => 'エアコンが壊れました',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $otherVnUser->user_id,
            'group_id' => $otherVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $otherVnUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident7->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
            'description' => '修理いたしました',
        ]);
        array_push($createRequestReports, $createRequestReport);
        //8件目***************************
        //Requests
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident8->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        array_push($createRequests, $createRequest);
        //RequestContents
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '床が抜けました',
            'request_id' => $createRequest->request_id,
        ]);
        array_push($createRequestContents, $createContent);
        //RequestHandleUsers
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $otherVnUser->user_id,
            'group_id' => $otherVn->vendor_id,
        ]);
        //RequestReports
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $otherVnUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident8->resident_id,
            'report_status' => config('const.Reports.Status.COMPLETED'),
            'description' => '補強しました',
        ]);
        array_push($createRequestReports, $createRequestReport);


        //******************************
        // ここから検証
        //******************************
        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //******************************
        // 1:所属業者含め取得
        //******************************
        $data = [
            'include_group_reports' => true,
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(6 === count($responseParams->data));

        //******************************
        // 2:所属業者含めず取得
        // （自分の担当分だけ）
        //******************************
        $data = [
            'include_group_reports' => false,
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(4 === count($responseParams->data));

        //**************************************
        // 3:所属業者含め、ステータス'writing'
        //**************************************
        $data = [
            'include_group_reports' => true,
            'status' => config('const.Reports.Status.WRITING'),
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(3 === count($responseParams->data));

        //**************************************
        // 4:所属業者含めず、ステータス'writing'
        //**************************************
        $data = [
            'include_group_reports' => false,
            'status' => config('const.Reports.Status.WRITING'),
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(2 === count($responseParams->data));

        //***************************************************
        // 5:所属業者含め、ステータス'draft'と'completed'
        //***************************************************
        $data = [
            'include_group_reports' => true,
            'status' => config('const.Reports.Status.DRAFT').','.config('const.Reports.Status.COMPLETED'),
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(3 === count($responseParams->data));

        //***************************************************
        // 6:所属業者含め、クエリに'エアコン'
        //***************************************************
        $data = [
            'include_group_reports' => true,
            'query' => 'エアコン',
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(4 === count($responseParams->data));

        //***************************************************
        // 7:所属業者含め、クエリに物件1の名称
        //***************************************************
        $data = [
            'include_group_reports' => true,
            'query' => $property1->name,
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(3 === count($responseParams->data));

        //**********************************************************
        // 8:所属業者含め、クエリに'エアコン'、ステータス'writing'
        //**********************************************************
        $data = [
            'include_group_reports' => true,
            'query' => 'エアコン',
            'status' => config('const.Reports.Status.WRITING'),
        ];
        $response = $this->call('GET','/api/vn/v1/reports', $data, [], [], $headers = [
            'HTTP_AUTHORIZATION' => 'Bearer '.$token,
        ]);
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);
        //件数で検証
        $this->assertTrue(2 === count($responseParams->data));
    }

    /**
     * バッジ表示用情報の取得が行えること
     * @test
     */
    public function user_can_get_badge_info()
    {
        //******************************
        // 業者生成
        //******************************
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        //******************************
        // 入居者生成
        //******************************
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);

        //******************************
        // 問合せ生成
        //******************************
        $data = [
            [
                //1件目
                'status' => config('const.Request.Status.OPEN'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //2件目
                'status' => config('const.Request.Status.OPEN'),
                'visibility' => config('const.Request.Visibility.PROTECTED'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => false,
            ],
            [
                //3件目
                'status' => config('const.Request.Status.OPEN'),
                'visibility' => config('const.Request.Visibility.PROTECTED'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //4件目
                'status' => config('const.Request.Status.SCHEDULING'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //5件目
                'status' => config('const.Request.Status.SCHEDULING'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //6件目
                'status' => config('const.Request.Status.IN_PROGRESS'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //7件目
                'status' => config('const.Request.Status.IN_PROGRESS'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //8件目
                'status' => config('const.Request.Status.COMPLETED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //9件目
                'status' => config('const.Request.Status.COMPLETED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //10件目
                'status' => config('const.Request.Status.WAITING_FOR_CANCEL'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //11件目
                'status' => config('const.Request.Status.WAITING_FOR_CANCEL'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //12件目
                'status' => config('const.Request.Status.CANCELLED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //13件目
                'status' => config('const.Request.Status.CANCELLED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //14件目
                'status' => config('const.Request.Status.TRANSFERRED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //15件目
                'status' => config('const.Request.Status.AUTO_COMPLETED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //16件目
                'status' => config('const.Request.Status.OPEN'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => false,
                'rhu' => false,
            ],
            [
                //17件目
                'status' => config('const.Request.Status.OPEN'),
                'visibility' => config('const.Request.Visibility.PROTECTED'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => false,
            ],
            [
                //18件目
                'status' => config('const.Request.Status.SCHEDULING'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //19件目
                'status' => config('const.Request.Status.IN_PROGRESS'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //20件目
                'status' => config('const.Request.Status.COMPLETED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //21件目
                'status' => config('const.Request.Status.WAITING_FOR_CANCEL'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => true,
            ],
            [
                //22件目
                'status' => config('const.Request.Status.CANCELLED'),
                'visibility' => config('const.Request.Visibility.PUBLIC'),
                'updated_at' => Carbon::now()->subDay(),
                'rvp' => true,
                'rhu' => true,
            ],
        ];
        foreach ($data as $d) {
            //Requests
            $createRequest = factory(\App\Models\Request::class)->create([
                'resident_id' => $resident1->resident_id,
                'management_company_id' => $createMc->management_company_id,
                'visibility' => $d['visibility'],
                'status' => $d['status'],
                'updated_at' => $d['updated_at'],
            ]);
            if ($d['rvp']) {
                //RequestVendorPermissions
                $createRvp = factory(\App\Models\RequestVendorPermission::class)->create([
                    'request_id' => $createRequest->request_id,
                    'vendor_id' => $userVn->vendor_id,
                ]);
            }
            if ($d['rhu']) {
                //RequestHandleUsers
                $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
                    'request_id' => $createRequest->request_id,
                    'user_id' => $loginUser->user_id,
                    'group_id' => $userVn->vendor_id,
                ]);
            }
        }


        //******************************
        // ここから検証
        //******************************
        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //バッジ表示用情報取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/vn/v1/badge');
        $responseParams = (Object) $response->decodeResponseJson();
        $response->assertStatus(200);

        //検証
        $this->assertTrue(2 === $responseParams->open);
        $this->assertTrue(1 === $responseParams->scheduling);
        $this->assertTrue(1 === $responseParams->in_progress);
        $this->assertTrue(1 === $responseParams->completed);
        $this->assertTrue(1 === $responseParams->waiting_for_cancel);
        $this->assertTrue(1 === $responseParams->cancelled);
    }

    /**
     * FirebaseデバイスID登録ができること
     * @test
     */
    public function user_can_update_device_id()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //FirebaseデバイスID登録
        $data = [
            'device_id' => 'device_id',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/vendorMember/'.$loginUser->user_id.'/'.$userVn->vendor_id.'/deviceId', $data);
        $response->assertStatus(201);

        $updateVnm = \App\Models\VendorMember::where(['user_id' => $loginUser->user_id, 'vendor_id' => $userVn->vendor_id])->first();
        $this->assertTrue($updateVnm->firebase_device_id == $data['device_id']);
    }

    /**
     * FirebaseデバイスID登録ができないこと
     * ( 別作業者を指定 )
     * @test
     */
    public function user_cannot_update_another_vendor_member_device_id()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $dummyUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $dummyVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $dummyUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        //ログイン
        $data = [
            'email' => $loginUser->email,
            'password' => 'password',
        ];
        $response = $this->post('/api/vn/v1/auth/login', $data);
        $responseParams = (Object) $response->decodeResponseJson();
        $token = $responseParams->access_token;

        //FirebaseデバイスID登録
        $data = [
            'device_id' => 'device_id',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->post('/api/vn/v1/vendorMember/'.$dummyUser->user_id.'/'.$userVn->vendor_id.'/deviceId', $data);
        $response->assertStatus(400);
    }
}
