<?php

namespace Tests\Unit\api;

use Tests\TestCase;
use App\Library\Cmn;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class LineTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('TimeSectionSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    private function login($data, $accessKeyId, $secretKey) {
        $token = '';
        $loginFlg = false;
        while (!$loginFlg) {
            $response = $this->withHeaders([
                'Authorization' => 'Basic '.base64_encode($accessKeyId.':'.$secretKey),
            ])
            ->post('/api/rd/v1/auth/login', $data);
            $response = $this->get('/api/rd/v1/auth/login?code='.$response->content());
            $responseParams = (Object) $response->decodeResponseJson();
            if (isset($responseParams->access_token)) {
                $loginFlg = true;
                $token = $responseParams->access_token;
            }
        }

        return $token;
    }

    /**
     * LIFF IDの取得が行えること
     * @test
     */
    public function user_can_get_liff_id()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        //LIFF ID取得
        $response = $this->get('/api/rd/v1/auth/liff/'.$createMcli->channel_id);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(200);
        $this->assertTrue($returnObj->liff_id === $createMcli->liff_id);
    }


    /**
     * LIFF IDの取得が行えないこと
     * @test
     */
    public function user_can_not_get_liff_id()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);

        //LIFF ID取得
        $response = $this->get('/api/rd/v1/auth/liff/'.$this->faker->regexify('[0-9]{5}'));
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }

    /**
     * ログイン処理（LINE）が行えること
     * @test
     */
    public function user_can_line_login()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'scope' => 'profile',
                'client_id' => $createMcli->channel_id,
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([
                'displayName' => $this->faker->name,
                'userId' => $createResident->line_user_id,
                'pictureUrl' => '',
                'statusMessage' => '',
            ], 200, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $createMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(200);
        $this->assertTrue($returnObj->access_token !== null);
        $this->assertTrue($returnObj->refresh_token !== null);
        $this->assertTrue($returnObj->resident != null);
    }

    /**
     * ログイン処理（LINE）が行えないこと
     * アクセストークン検証失敗
     * @test
     */
    public function user_can_not_line_login_invalid_access_token()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'error' => 'invalid_request',
                'error_description' => 'access token expired',
            ], 400, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $createMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(401);
    }

    /**
     * ログイン処理（LINE）が行えないこと
     * チャンネルID検証失敗
     * @test
     */
    public function user_can_not_line_login_invalid_channel_id()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'scope' => 'profile',
                'client_id' => $this->faker->regexify('[0-9]{15}'),
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
            ], 200, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $createMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(401);
    }

    /**
     * ログイン処理（LINE）が行えないこと
     * プロフィール取得失敗
     * @test
     */
    public function user_can_not_line_login_get_profile_failed()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'scope' => 'profile',
                'client_id' => $createMcli->channel_id,
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([], 400, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $createMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(401);
    }

    /**
     * ログイン処理（LINE）が行えないこと
     * 入居者特定失敗
     * @test
     */
    public function user_can_not_line_login_resident_identification_failure()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'scope' => 'profile',
                'client_id' => $createMcli->channel_id,
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([
                'displayName' => $this->faker->name,
                'userId' => $this->faker->regexify('[0-9]{15}'),
                'pictureUrl' => '',
                'statusMessage' => '',
            ], 200, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $createMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(401);
    }

    /**
     * ログイン処理（LINE）が行えないこと
     * 入居物件の管理会社検証失敗
     * @test
     */
    public function user_can_not_line_login_management_company_identification_failure()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $dummyMc = factory(\App\Models\ManagementCompany::class)->create();
        $dummyMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $dummyMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        Http::fake([
            //アクセストークン検証スタブ
            'https://api.line.me/oauth2/v2.1/verify*' =>
            Http::response([
                'scope' => 'profile',
                'client_id' => $dummyMcli->channel_id,
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([
                'displayName' => $this->faker->name,
                'userId' => $createResident->line_user_id,
                'pictureUrl' => '',
                'statusMessage' => '',
            ], 200, ['Headers']),
        ]);

        //LINE LOGIN
        $data = [
            'line_access_token' => $this->faker->regexify('[0-9]{15}'),
            'line_client_id' => $dummyMcli->channel_id,
        ];
        $response = $this->post('/api/rd/v1/auth/line-login', $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(401);
    }

    /**
     * LINEアカウント連携用URL取得が行えること
     * @test
     */
    public function user_can_get_url_line_account_linkage()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携用URL取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/rd/v1/line/auth');
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(200);
        $this->assertTrue(strpos($returnObj->url, 'https://access.line.me/oauth2/v2.1/authorize?response_type=code&scope=profile') !== false);
        $this->assertTrue(strpos($returnObj->url, 'client_id='.$createMcli->channel_id) !== false);
        $this->assertTrue(strpos($returnObj->url, 'redirect_uri='.urlencode(env('APP_URL').'/resident/line-cb')) !== false);
    }

    /**
     * LINEアカウント連携用URL取得が行えないこと
     * (管理会社がLINE連携していない)
     * @test
     */
    public function user_can_not_get_url_line_account_linkage()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携用URL取得
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get('/api/rd/v1/line/auth');
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }

    /**
     * LINE連携解除が行えること
     * @test
     */
    public function user_can_canceling_line_account_linkage()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINE連携解除
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->delete('/api/rd/v1/line/auth');
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(200);
        $resident = \App\Models\Resident::find($createResident->resident_id);
        $this->assertTrue($resident->line_user_id === null);
    }

    /**
     * LINEアカウント連携認証が行えること
     * @test
     */
    public function user_can_line_account_linkage_authentication()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        $lineUserId = $this->faker->regexify('[0-9]{15}');
        Http::fake([
            //アクセストークン取得スタブ
            'https://api.line.me/oauth2/v2.1/token' =>
            Http::response([
                'access_token' => $this->faker->regexify('[0-9]{15}'),
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
                'refresh_token	' => $this->faker->regexify('[0-9]{15}'),
                'scope' => 'profile',
                'token_type' => 'Bearer',
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([
                'displayName' => $this->faker->name,
                'userId' => $lineUserId,
                'pictureUrl' => '',
                'statusMessage' => '',
            ], 200, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携認証
        $code = $this->faker->regexify('[A-Za-z0-9]{10}');
        $state = $this->faker->regexify('[A-Za-z0-9]{10}');
        $url = '/api/rd/v1/line/verify?code='.$code.'&state='.$state;
        Cache::put($createResident->resident_id, $state, 60);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get($url, $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(200);
        $resident = \App\Models\Resident::find($createResident->resident_id);
        $this->assertTrue($resident->line_user_id === $lineUserId);
    }

    /**
     * LINEアカウント連携認証が行えないこと
     * （管理会社がLINEを利用していない）
     * @test
     */
    public function user_can_not_line_account_linkage_authentication_mc_dont_use_line()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携認証
        $code = $this->faker->regexify('[A-Za-z0-9]{10}');
        $state = $this->faker->regexify('[A-Za-z0-9]{10}');
        $url = '/api/rd/v1/line/verify?code='.$code.'&state='.$state;
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get($url, $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }

    /**
     * LINEアカウント連携認証が行えないこと
     * （state未一致）
     * @test
     */
    public function user_can_not_line_account_linkage_authentication_invalid_state()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携認証
        $code = $this->faker->regexify('[A-Za-z0-9]{10}');
        $state = $this->faker->regexify('[A-Za-z0-9]{10}');
        $url = '/api/rd/v1/line/verify?code='.$code.'&state='.$state;
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get($url, $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }

    /**
     * LINEアカウント連携認証が行えないこと
     * （LINEアクセストークン取得失敗）
     * @test
     */
    public function user_can_not_line_account_linkage_authentication_access_token_acquisition_failure()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        $lineUserId = $this->faker->regexify('[0-9]{15}');
        Http::fake([
            //アクセストークン取得スタブ
            'https://api.line.me/oauth2/v2.1/token' =>
            Http::response([], 400, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携認証
        $code = $this->faker->regexify('[A-Za-z0-9]{10}');
        $state = $this->faker->regexify('[A-Za-z0-9]{10}');
        $url = '/api/rd/v1/line/verify?code='.$code.'&state='.$state;
        Cache::put($createResident->resident_id, $state, 60);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get($url, $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }

    /**
     * LINEアカウント連携認証が行えないこと
     * （LINEプロフィール取得失敗）
     * @test
     */
    public function user_can_not_line_account_linkage_authentication_profile_acquisition_failure()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createMcli = factory(\App\Models\ManagementCompanyLineInformation::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        //スタブ生成
        $lineUserId = $this->faker->regexify('[0-9]{15}');
        Http::fake([
            //アクセストークン取得スタブ
            'https://api.line.me/oauth2/v2.1/token' =>
            Http::response([
                'access_token' => $this->faker->regexify('[0-9]{15}'),
                'expires_in' => $this->faker->regexify('[0-9]{8}'),
                'refresh_token	' => $this->faker->regexify('[0-9]{15}'),
                'scope' => 'profile',
                'token_type' => 'Bearer',
            ], 200, ['Headers']),

            //プロフィール取得スタブ
            'https://api.line.me/v2/profile' =>
            Http::response([], 400, ['Headers']),
        ]);

        //ログイン
        $data = [
            'property_code' => $createProperty->property_code,
            'room_code' => $createRoom->room_code,
            'resident_code' => $createResident->resident_code,
        ];
        $token = $this->login($data, $createApi->access_key_id, $createApi->secret_key);

        //LINEアカウント連携認証
        $code = $this->faker->regexify('[A-Za-z0-9]{10}');
        $state = $this->faker->regexify('[A-Za-z0-9]{10}');
        $url = '/api/rd/v1/line/verify?code='.$code.'&state='.$state;
        Cache::put($createResident->resident_id, $state, 60);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token,
        ])
        ->get($url, $data);
        $returnObj = (Object) $response->decodeResponseJson();

        //検証
        $response->assertStatus(400);
    }
}