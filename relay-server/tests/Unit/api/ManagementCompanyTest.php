<?php

namespace Tests\Unit\api;

use Tests\TestCase;
use App\Library\Cmn;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ManagementCompanyTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('TimeSectionSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ユーザーがAPIを実行できないこと
     * （Authorizationヘッダがない）
     * @test
     */
    public function user_can_not_execute_api_authorization_header_is_nothing()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $response = $this->postJson('/api/mc/v1/account/tempRegistration', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがAPIを実行できないこと
     * （AuthorizationヘッダがBasicじゃない）
     * @test
     */
    public function user_can_not_execute_api_authorization_header_is_not_basic()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがAPIを実行できないこと
     * （AuthorizationヘッダのBasicがbase64(access_key:secret_key)のフォーマットでない）
     * @test
     */
    public function user_can_not_execute_api_basic_header_is_wrong()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.'/'.$createApi->secret_key),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);
        $response->assertStatus(401);
    }

    /**
     * ユーザーがAPIを実行できないこと
     * （access_key, secret_keyで管理会社が特定できない）
     * @test
     */
    public function user_can_not_execute_api_can_not_specific_mc()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $randomStr = $this->faker->regexify('[A-Za-z0-9]{20}');
        $response = $this->withHeaders([
            'Authorization' => 'Basic '.base64_encode($randomStr.':'.$randomStr),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);
        $response->assertStatus(401);

    }

    /**
     * 入居者利用登録（仮登録）が行えること
     * @test
     */
    public function user_can_resident_temporary_registration()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);

        //検証
        $response->assertStatus(200);
    }

    /**
     * 入居者利用登録（仮登録）が行えないこと
     * （入居者特定できず）
     * @test
     */
    public function user_can_resident_temporary_registration_can_not_specific_resident()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => '12345', //適当な入居者コード
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);

        //検証
        $response->assertStatus(401);
    }

    /**
     * 入居者利用登録（仮登録）が行えないこと
     * （既にアカウント登録済み）
     * @test
     */
    public function user_can_resident_temporary_registration_already_regist_account()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createProperty = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $createRoom = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $createProperty->property_id,
        ]);
        $residentUser = factory(\App\Models\User::class)->create();
        $createResident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $createRoom->room_id,
            'user_id' => $residentUser->user_id,
        ]);

        $data = [
            'tel' => $createResident->tel,
            'email' => $createResident->email,
            'resident_code' => $createResident->resident_code,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Basic '.base64_encode($createApi->access_key_id.':'.$createApi->secret_key),
        ])
        ->json('POST', '/api/mc/v1/account/tempRegistration', $data);

        //検証
        $response->assertStatus(400);
    }
}
