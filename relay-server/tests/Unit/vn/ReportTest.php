<?php

namespace Tests\Unit\vn;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ReportTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 完了報告書の更新ができずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $this->put('/vn/report/'.$createRequestReport->request_report_id, $data)
            ->assertRedirect('/vn/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で完了報告書が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->put('/vn/report/'.$createRequestReport->request_report_id, $data);

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->description === $data['description']);
        $this->assertTrue($report->action_date === $data['action_date']);
        $this->assertTrue($report->starting_time === $data['starting_time'].':00');
        $this->assertTrue($report->ending_time === $data['ending_time'].':00');
        $this->assertTrue($report->action_status === $data['action_status']);
        $this->assertTrue($report->action_section === $data['action_section']);
        $this->assertTrue($report->construction_amount === $data['construction_amount']);
    }

    /**
     * 報告書更新時バリデーション
     * @test
     */
    public function validator_check_for_update_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //****************************
        // 必須チェック
        //****************************
        $data = [
            'description' => '',
            'action_date' => '',
            'starting_time' => '',
            'ending_time' => '',
            'action_status' => '',
            'action_section' => '',
            'construction_amount' => '',
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $userVn->vendor_id])
                    ->put('/vn/report/'.$createRequestReport->request_report_id, $data);

        //検証
        $response->assertSessionHasErrors([
            'action_date' => '工事日は必ず指定してください。',
            'description' => '作業内容は必ず指定してください。',
            'action_section' => '対応区分は必ず指定してください。',
            'action_status' => 'ステータスは必ず指定してください。',
        ]);

        //****************************
        // 型、桁数チェック
        //****************************
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => '2020-13-01',
            'starting_time' => '50:00',
            'ending_time' => '10:10:10',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '123',
            'construction_amount' => 'abcd',
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $userVn->vendor_id])
                    ->put('/vn/report/'.$createRequestReport->request_report_id, $data);

        //検証
        $response->assertSessionHasErrors([
            'action_date' => '工事日には有効な日付を指定してください。',
            'starting_time' => '開始時間はH:i形式で指定してください。',
            'ending_time' => '終了時間はH:i形式で指定してください。',
            'action_section' => '対応区分は、1文字以下で指定してください。',
            'construction_amount' => '工事金額は整数で指定してください。',
        ]);
    }

    /**
     * backパラメータ指定時、編集画面に遷移すること
     * @test
     */
    public function parameter_have_back_when_redirect_edit_page()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書更新
        $data = [
            'back' => true,
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->put('/vn/report/'.$createRequestReport->request_report_id, $data)
            ->assertRedirect(route('vn.reportEdit', ['report_id' => $createRequestReport->request_report_id]));
    }

    /**
     * 報告書ステータスがDRAFT,DECLINED以外の場合更新できないこと
     * @test
     */
    public function user_can_not_update_report_when_status_is_not_draft_or_declined()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->put('/vn/report/'.$createRequestReport->request_report_id, $data);
        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->description == $createRequestReport->description);
        $this->assertTrue($report->action_date == $createRequestReport->action_date);
        $this->assertTrue($report->starting_time == $createRequestReport->starting_time.':00');
        $this->assertTrue($report->ending_time == $createRequestReport->ending_time.':00');
        $this->assertTrue($report->action_status == $createRequestReport->action_status);
        $this->assertTrue($report->action_section == $createRequestReport->action_section);
        $this->assertTrue($report->construction_amount == $createRequestReport->construction_amount);
    }

    /**
     * 他業者の報告書の場合更新できないこと
     * @test
     */
    public function user_can_not_update_report_when_other_vendors_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $otherUser = factory(\App\Models\User::class)->create();
        $userVn = factory(\App\Models\Vendor::class)->create();
        $otherVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $otherUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $otherUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $otherUser->user_id,
            'group_id' => $otherVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $otherUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書更新
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->put('/vn/report/'.$createRequestReport->request_report_id, $data);
        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->description == $createRequestReport->description);
        $this->assertTrue($report->action_date == $createRequestReport->action_date);
        $this->assertTrue($report->starting_time == $createRequestReport->starting_time.':00');
        $this->assertTrue($report->ending_time == $createRequestReport->ending_time.':00');
        $this->assertTrue($report->action_status == $createRequestReport->action_status);
        $this->assertTrue($report->action_section == $createRequestReport->action_section);
        $this->assertTrue($report->construction_amount == $createRequestReport->construction_amount);
    }

    /**
     * ログインしていない場合、
     * 完了報告書の提出ができずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_submit_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書提出
        $data = [
            'description' => '完了報告書、報告内容',
            'action_date' => Carbon::now()->format('Y-m-d'),
            'starting_time' => '00:00',
            'ending_time' => '23:45',
            'action_status' => config('const.Reports.ActionStatus.INCOMPLETE'),
            'action_section' => '2',
            'construction_amount' => 5000,
        ];
        $this->post('/vn/report/'.$createRequestReport->request_report_id, $data)
            ->assertRedirect('/vn/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で完了報告書が提出されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_submit_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書提出
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->post('/vn/report/'.$createRequestReport->request_report_id);

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === config('const.Reports.Status.SUBMITTED'));
    }

    /**
     * 報告書ステータスがDRAFT,DECLINED以外の場合提出できないこと
     * @test
     */
    public function user_can_not_submit_report_when_status_is_not_draft_or_declined()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $userVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $loginUser->user_id,
            'group_id' => $userVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.WRITING'),
        ]);

        //完了報告書提出
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->post('/vn/report/'.$createRequestReport->request_report_id);
        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status == $createRequestReport->report_status);
    }

    /**
     * 他業者の報告書の場合提出できないこと
     * @test
     */
    public function user_can_not_submit_report_when_other_vendors_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $otherUser = factory(\App\Models\User::class)->create();
        $userVn = factory(\App\Models\Vendor::class)->create();
        $otherVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $loginUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $loginUser->user_id,
            'vendor_id' => $userVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $otherUserVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $otherUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $createMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $otherUser->user_id,
            'group_id' => $otherVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $otherUser->user_id,
            'vendor_id' => $otherVn->vendor_id,
            'management_company_id' => $createMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書提出
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $userVn->vendor_id])
            ->post('/vn/report/'.$createRequestReport->request_report_id);
        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status == $createRequestReport->report_status);
    }
}
