<?php

namespace Tests\Unit\vn;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class MyPageTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 業者情報が更新されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_mypage()
    {
        $data = [
            'work_areas' => ['福岡市博多区', '福岡市中央区'],
            'work_time_from' => '09:00',
            'work_time_to' => '18:00',
            'work_weeks' => ['月', '火', '水', '木', '金'],
            'work_items' => ['水回り', '電気関係', '空調'],
        ];
        $this->post('/vn/mypage/update', $data)
            ->assertRedirect('/vn/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で業者情報が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_mypage()
    {
        $data = [
            'work_areas' => ['福岡市博多区', '福岡市中央区'],
            'work_time_from' => '09:00',
            'work_time_to' => '18:00',
            'work_weeks' => ['月', '火', '水', '木', '金'],
            'work_items' => ['水回り', '電気関係', '空調'],
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $createVendor->vendor_id])
            ->post('/vn/mypage/update', $data)
            ->assertRedirect(url()->previous());
        //検証
        $vendor = \App\Models\Vendor::find($createVendor->vendor_id);
        $this->assertTrue($data['work_areas'] === $vendor->work_areas);
        $this->assertTrue($data['work_time_from'].':00' === $vendor->work_time_from);
        $this->assertTrue($data['work_time_to'].':00' === $vendor->work_time_to);
        $this->assertTrue($data['work_weeks'] === $vendor->work_weeks);
        $this->assertTrue($data['work_items'] === $vendor->work_items);
    }

    /**
     * マイページ更新時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_update_mypage_type()
    {
        $data = [
            'work_areas' => ['福岡市博多区', '福岡市中央区'],
            'work_time_from' => '09:00:00',
            'work_time_to' => '18:00:00',
            'work_weeks' => ['月', '火', '水', '木', '金'],
            'work_items' => ['水回り', '電気関係', '空調'],
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->post('/vn/mypage/update', $data);
        //検証
        $response->assertSessionHasErrors([
            'work_time_from' => '作業可能開始時間はH:i形式で指定してください。',
            'work_time_to' => '作業可能終了時間はH:i形式で指定してください。',
        ]);
    }

}
