<?php

namespace Tests\Unit\vn;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class VendorMemberTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 作業者登録されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_store_vendor_member()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'emergency_tel' => '090-1111-2222',
        ];
        $this->post('/vn/vendorMember', $data)
            ->assertRedirect('/vn/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で作業者情報が登録されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_store_vendor_member()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'emergency_tel' => '090-1111-2222',
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $this->actingAs($user, 'guard_vendor_manager')
            ->withSession(['vendor_id' => $createVendor->vendor_id])
            ->post('/vn/vendorMember', $data)
            ->assertRedirect(url()->previous());
        //検証
        $user = \App\Models\User::where('email', $data['email'])->first();
        $this->assertTrue($data['email'] === $user->email);
        $vnm = \App\Models\VendorMember::where('user_id', $user->user_id)->where('vendor_id', $createVendor->vendor_id)->first();
        $this->assertTrue($user->user_id === $vnm->user_id);
        $this->assertTrue($createVendor->vendor_id === $vnm->vendor_id);
        $this->assertTrue($data['last_name'] === $vnm->last_name);
        $this->assertTrue($data['first_name'] === $vnm->first_name);
        $this->assertTrue($data['emergency_tel'] === $vnm->emergency_tel);
        $this->assertTrue(null === $vnm->representative_flg);
        $this->assertTrue(null === $vnm->access_token);
        $this->assertTrue(null === $vnm->token_expire);
        $this->assertTrue(null === $vnm->refresh_token);
        $this->assertTrue(null === $vnm->license_expire);
    }

    /**
     * 作業者登録時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_store_vendor_member_required()
    {
        $data = [
            'last_name' => '',
            'first_name' => '',
            'email' => '',
            'emergency_tel' => '',
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->post('/vn/vendorMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'email' => 'メールアドレスは必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 作業者登録時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_store_vendor_member_max_length()
    {
        $str256 = $this->faker->realText(256);
        $data = [
            'last_name' => $str256,
            'first_name' => $str256,
            'email' => $str256,
            'emergency_tel' => '012-345-6789000',
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->post('/vn/vendorMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'email' => 'メールアドレスは、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
        ]);
    }

    /**
     * 作業者登録時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_store_vendor_member_type()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'emailaddress',
            'emergency_tel' => '011-3415-6789',
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->post('/vn/vendorMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスには、有効なメールアドレスを指定してください。',
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
        ]);
    }

    /**
     * 作業者登録時バリデーション
     * （ユニークチェック）
     * @test
     */
    public function validator_check_for_store_vendor_member_unique()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'so@test.com',
            'emergency_tel' => '090-3415-6789',
        ];
        $user = \App\Models\User::find(2);
        $createVendor = factory(\App\Models\Vendor::class)->create();
        //登録処理
        $response = $this->actingAs($user, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->post('/vn/vendorMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスの値は既に存在しています。',
        ]);
    }

    /**
     * ログインしていない場合、
     * 作業者更新されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_vendor_member()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = null;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel' => '090-1111-2222',
            'updated_at' => $createVnm->updated_at,
        ];
        $this->withSession(['vendor_id' => $createVendor->vendor_id])
            ->put('/vn/vendorMember/'.$createVnm->user_id, $data)
            ->assertRedirect('/vn/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値で作業者情報が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_vendor_member()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = null;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => $createUser->user_id,
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel' => '090-1111-2222',
            'updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(2);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->put('/vn/vendorMember/'.$createVnm->user_id, $data);
        //検証
        $vnm = \App\Models\VendorMember::where('user_id', $createVnm->user_id)->where('vendor_id', $createVnm->vendor_id)->first();
        $this->assertTrue($createVnm->user_id === $vnm->user_id);
        $this->assertTrue($createVnm->vendor_id === $vnm->vendor_id);
        $this->assertTrue($data['last_name'] === $vnm->last_name);
        $this->assertTrue($data['first_name'] === $vnm->first_name);
        $this->assertTrue($data['emergency_tel'] === $vnm->emergency_tel);
        $this->assertTrue($representativeFlg === $vnm->representative_flg);
        $this->assertTrue($createVnm->access_token === $vnm->access_token);
        $this->assertTrue($createVnm->token_expire === $vnm->token_expire);
        $this->assertTrue($createVnm->refresh_token === $vnm->refresh_token);
        $this->assertTrue($createVnm->license_expire === $vnm->license_expire);
    }

    /**
     * 作業者更新時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_update_vendor_member_required()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = null;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $data = [
            'user_id' => '',
            'last_name' => '',
            'first_name' => '',
            'emergency_tel' => '',
            'updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(2);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->put('/vn/vendorMember/'.$createVnm->user_id, $data);
        //検証
        $response->assertSessionHasErrors([
            'user_id' => 'user idは必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * 作業者更新時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_update_vendor_member_max_length()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = null;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $str256 = $this->faker->realText(256);
        $data = [
            'user_id' => $createUser->user_id,
            'last_name' => $str256,
            'first_name' => $str256,
            'emergency_tel' => '090-11111-11111',
            'updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(2);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->put('/vn/vendorMember/'.$createVnm->user_id, $data);
        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
        ]);
    }

    /**
     * 作業者更新時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_update_vendor_member_type()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createVendor = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = null;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $createUser->user_id,
            'vendor_id' => $createVendor->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);

        $str256 = $this->faker->realText(256);
        $data = [
            'user_id' => $createUser->user_id,
            'last_name' => '姓',
            'first_name' => '名',
            'emergency_tel' => '092-111-1111',
            'updated_at' => $createVnm->updated_at,
        ];
        $loginUser = \App\Models\User::find(2);
        //更新処理
        $response = $this->actingAs($loginUser, 'guard_vendor_manager')
                    ->withSession(['vendor_id' => $createVendor->vendor_id])
                    ->put('/vn/vendorMember/'.$createVnm->user_id, $data);
        //検証
        $response->assertSessionHasErrors([
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
        ]);
    }
}
