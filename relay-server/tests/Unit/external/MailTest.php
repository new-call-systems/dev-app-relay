<?php

namespace Tests\Unit\external;

use App\Mail\TestNotification;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;

class MailTest extends TestCase
{
    /**
     * メール送信テスト
     * @test
     */
    public function i_can_send_mail()
    {
        //$to = 's-ishida@diagram.co.jp';
        $to = 'takawa22@gmail.com';

        Mail::to($to)
        ->send(new TestNotification());
        $this->assertTrue(true);
    }
}
