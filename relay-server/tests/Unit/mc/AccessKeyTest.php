<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccessKeyTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * アクセスキーの取得ができないこと
     * @test
     */
    public function unauthenticated_user_cannot_get_access_key()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        //登録処理
        $response = $this->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/getAccessKey')
                    ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、
     * アクセスキーの取得ができること
     * @test
     */
    public function authenticated_user_can_get_access_key()
    {
        $user = \App\Models\User::find(3);
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $createApi = factory(\App\Models\ManagementCompanyApiAccessKey::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        //登録処理
        $response = $this->actingAs($user, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/getAccessKey');
        $response->assertSeeText('accessKeyId='.$createApi->access_key_id);
        $response->assertSeeText('secretKey='.$createApi->secret_key);

    }
}
