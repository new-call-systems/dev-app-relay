<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequestTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしている場合、
     * 問合せ一覧画面に遷移できること
     * @test
     */
    public function authenticated_user_can_transition_request_list()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->actingAs($createUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->get('/mc/request');
        $response->assertStatus(200);
    }

    /**
     * ログインしていない場合、
     * 問合せ一覧画面に遷移できない
     * @test
     */
    public function unauthenticated_user_cannot_transition_request_list()
    {
        $createUser = factory(\App\Models\User::class)->create();
        $createUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->withSession(['management_company_id' => $createMc->management_company_id])
                    ->get('/mc/request')
                    ->assertRedirect('/mc/login');
    }
}
