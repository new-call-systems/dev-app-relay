<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ManagementCompanyMemberTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしている場合、
     * ユーザー管理画面に遷移できること
     * @test
     */
    public function authenticated_user_can_transition_user_list()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //ユーザー管理画面遷移
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->get('/mc/managementCompanyMember');
        $response->assertStatus(200);
    }

    /**
     * ログインしていない場合、
     * ユーザー管理画面に遷移できない
     * @test
     */
    public function unauthenticated_user_cannot_transition_user_list()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->withSession(['management_company_id' => $createMc->management_company_id])
                    ->get('/mc/managementCompanyMember')
                    ->assertRedirect('/mc/login');
    }

    /**
     * ログインしていない場合、
     * ユーザー登録されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_store_member()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'emergency_tel' => '090-1111-2222',
            'department' => '部署名',
            'manager_flg' => '1',
        ];
        $this->post('/mc/managementCompanyMember', $data)
            ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値でユーザー情報が登録されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_store_member()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_test@test.com',
            'emergency_tel' => '090-1111-2222',
            'department' => '部署名',
            'manager_flg' => '1',
        ];
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/managementCompanyMember', $data)->assertRedirect(url()->previous());
        //検証
        $user = \App\Models\User::where('email', $data['email'])->first();
        $this->assertTrue($data['email'] === $user->email);
        if ($data['manager_flg']) {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER')));
        } else {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MEMBER')));
        }

        $mcm = \App\Models\ManagementCompanyMember::where('user_id', $user->user_id)->where('management_company_id', $createMc->management_company_id)->first();
        $this->assertTrue($user->user_id === $mcm->user_id);
        $this->assertTrue($createMc->management_company_id === $mcm->management_company_id);
        $this->assertTrue($data['last_name'] === $mcm->last_name);
        $this->assertTrue($data['first_name'] === $mcm->first_name);
        $this->assertTrue($data['emergency_tel'] === $mcm->emergency_tel);
        $this->assertTrue(null === $mcm->representative_flg);
        $this->assertTrue($data['department'] === $mcm->department);
    }

    /**
     * ユーザー登録時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_store_member_required()
    {
        $data = [
            'last_name' => '',
            'first_name' => '',
            'email' => '',
            'emergency_tel' => '',
            'department' => '',
            'manager_flg' => '',
        ];
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/managementCompanyMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'email' => 'メールアドレスは必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * ユーザー登録時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_store_member_max_length()
    {
        $str256 = $this->faker->realText(256);
        $data = [
            'last_name' => $str256,
            'first_name' => $str256,
            'email' => $str256,
            'emergency_tel' => '12345678901234',
            'department' => $str256,
            'manager_flg' => '12',
        ];
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/managementCompanyMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'email' => 'メールアドレスは、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
            'department' => '部署名は、255文字以下で指定してください。',
            'manager_flg' => '全アクセス権限は、1文字以下で指定してください。',
        ]);
    }

    /**
     * ユーザー登録時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_store_member_type()
    {
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => 'unit_testtest.com',
            'emergency_tel' => '092-111-2222',
            'department' => '部署名',
            'manager_flg' => '1',
        ];
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        //登録処理
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/managementCompanyMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスには、有効なメールアドレスを指定してください。',
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
        ]);
    }

    /**
     * ユーザー登録時バリデーション
     * （ユニークチェック）
     * @test
     */
    public function validator_check_for_store_member_unique()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $data = [
            'last_name' => '姓',
            'first_name' => '名',
            'email' => $loginUser->email,
            'emergency_tel' => '092-111-2222',
            'department' => '部署名',
            'manager_flg' => '1',
        ];
        //登録処理
        $response = $this->actingAs($loginUser, 'guard_management_company_member')
                    ->withSession(['management_company_id' => $createMc->management_company_id])
                    ->post('/mc/managementCompanyMember', $data);
        //検証
        $response->assertSessionHasErrors([
            'email' => 'メールアドレスの値は既に存在しています。',
        ]);
    }

    /**
     * ログインしていない場合、
     * ユーザー更新されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_update_member()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $craeteUser = factory(\App\Models\User::class)->create();
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $craeteUser->user_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $data = [
            'user_id' => $craeteUser->user_id,
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel' => '090-1234-4321',
            'department' => '更新部署名',
            'manager_flg' => '1',
            'updated_at' => $createMcm->updated_at,
        ];
        //更新処理
        $this->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
            ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、
     * パラメータ値でユーザー情報が更新されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_update_member()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $craeteUser = factory(\App\Models\User::class)->create();
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $craeteUser->user_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $data = [
            'user_id' => $craeteUser->user_id,
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel' => '090-1234-4321',
            'department' => '更新部署名',
            'manager_flg' => null,
            'updated_at' => $createMcm->updated_at,
        ];
        //更新処理
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $createMc->management_company_id])
        ->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
        ->assertRedirect(url()->previous());

        //検証
        $user = \App\Models\User::find($craeteUser->user_id);
        if ($data['manager_flg']) {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER')));
        } else {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MEMBER')));
        }

        $mcm = \App\Models\ManagementCompanyMember::where('user_id', $createMcm->user_id)->where('management_company_id', $createMcm->management_company_id)->first();
        $this->assertTrue($data['last_name'] === $mcm->last_name);
        $this->assertTrue($data['first_name'] === $mcm->first_name);
        $this->assertTrue($data['emergency_tel'] === $mcm->emergency_tel);
        $this->assertTrue($data['department'] === $mcm->department);


        $data = [
            'user_id' => $craeteUser->user_id,
            'last_name' => '更新姓2',
            'first_name' => '更新名2',
            'emergency_tel' => '090-1234-2222',
            'department' => '更新部署名2',
            'manager_flg' => '1',
            'updated_at' => $mcm->updated_at,
        ];
        //更新処理
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $createMc->management_company_id])
        ->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
        ->assertRedirect(url()->previous());

        //検証
        $user = \App\Models\User::find($craeteUser->user_id);
        if ($data['manager_flg']) {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER')));
        } else {
            $this->assertTrue($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MEMBER')));
        }

        $mcm = \App\Models\ManagementCompanyMember::where('user_id', $createMcm->user_id)->where('management_company_id', $createMcm->management_company_id)->first();
        $this->assertTrue($data['last_name'] === $mcm->last_name);
        $this->assertTrue($data['first_name'] === $mcm->first_name);
        $this->assertTrue($data['emergency_tel'] === $mcm->emergency_tel);
        $this->assertTrue($data['department'] === $mcm->department);
    }

    /**
     * ユーザー更新時バリデーション
     * （必須チェック）
     * @test
     */
    public function validator_check_for_update_member_required()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $craeteUser = factory(\App\Models\User::class)->create();
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $craeteUser->user_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $data = [
            'user_id' => '',
            'last_name' => '',
            'first_name' => '',
            'emergency_tel' => '',
            'department' => '',
            'manager_flg' => '',
            'updated_at' => '',
        ];
        //更新処理
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $createMc->management_company_id])
        ->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
        ->assertRedirect(url()->previous());

        //検証
        $response->assertSessionHasErrors([
            'user_id' => 'user idは必ず指定してください。',
            'last_name' => '姓は必ず指定してください。',
            'first_name' => '名は必ず指定してください。',
            'emergency_tel' => '緊急連絡先は必ず指定してください。',
        ]);
    }

    /**
     * ユーザー更新時バリデーション
     * （最大桁）
     * @test
     */
    public function validator_check_for_update_member_max_length()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $craeteUser = factory(\App\Models\User::class)->create();
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $craeteUser->user_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $str256 = $this->faker->realText(256);
        $data = [
            'user_id' => $craeteUser->user_id,
            'last_name' => $str256,
            'first_name' => $str256,
            'emergency_tel' => '12345678901234',
            'department' => $str256,
            'manager_flg' => null,
            'updated_at' => $createMcm->updated_at,
        ];

        //更新処理
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $createMc->management_company_id])
        ->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
        ->assertRedirect(url()->previous());

        //検証
        $response->assertSessionHasErrors([
            'last_name' => '姓は、255文字以下で指定してください。',
            'first_name' => '名は、255文字以下で指定してください。',
            'emergency_tel' => '緊急連絡先は、13文字以下で指定してください。',
            'department' => '部署名は、255文字以下で指定してください。',
        ]);
    }

    /**
     * ユーザー更新時バリデーション
     * （型チェック）
     * @test
     */
    public function validator_check_for_update_member_type()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $craeteUser = factory(\App\Models\User::class)->create();
        $createMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $craeteUser->user_id,
            'management_company_id' => $createMc->management_company_id,
        ]);
        $data = [
            'user_id' => $craeteUser->user_id,
            'last_name' => '更新姓',
            'first_name' => '更新名',
            'emergency_tel' => '092-123-4444',
            'department' => '更新部署名',
            'manager_flg' => null,
            'updated_at' => $createMcm->updated_at,
        ];

        //更新処理
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $createMc->management_company_id])
        ->put('/mc/managementCompanyMember/'.$craeteUser->user_id, $data)
        ->assertRedirect(url()->previous());

        //検証
        $response->assertSessionHasErrors([
            'emergency_tel' => '緊急連絡先は携帯電話番号を指定してください。',
        ]);
    }
}
