<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ユーザーがログインできること
     * @test
     */
    public function user_can_login()
    {
        $data = [
            'email' => 'mc_manager@test.com',
            'password' => 'password',
        ];
        $this->post('/mc/login', $data)->assertRedirect('/mc');
    }

    /**
     * ユーザーがログインできないこと
     * @test
     */
    public function user_cannot_login()
    {
        $data = [
            'email' => 'mc_manager@test.com',
            'password' => 'unauthenticated',
        ];
        $response = $this->post('/mc/login', $data);
        $this->post('/mc/login', $data)->assertRedirect(url()->previous());
    }

    /**
     * ログインしていない場合、
     * ログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_can_view_login()
    {
        $response = $this->get('/mc/login');

        $response->assertStatus(200);
    }

    /**
     * 既にログインしている場合、
     * ログイン画面は表示されずにダッシュボード画面が表示されること
     * @test
     */
    public function authenticated_user_cannot_view_login()
    {
        $user = \App\Models\User::find(3);
        $response = $this->actingAs($user, 'guard_management_company_member')->get('/mc/login')->assertRedirect('/mc');;
    }

    /**
     * ログインしていない場合、
     * ダッシュボード画面が表示されずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_view_home()
    {
        $this->get('/mc')
            ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、
     * ダッシュボード画面が正常に表示されること
     * @test
     */
    public function authenticated_user_can_view_home()
    {
        $user = \App\Models\User::find(3);
        $response = $this->actingAs($user, 'guard_management_company_member')->get('/mc');
        $response->assertStatus(200);
    }
}
