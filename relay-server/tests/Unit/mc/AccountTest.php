<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class AccountTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * 入居者本登録処理が行えること
     * @test
     */
    public function user_can_account_main_registration()
    {
        $createMc = factory(\App\Models\ManagementCompany::class)->create();
        $property = factory(\App\Models\Property::class)->create([
            'management_company_id' => $createMc->management_company_id,
        ]);
        $room = factory(\App\Models\Room::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'property_id' => $property->property_id,
        ]);
        $resident = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $createMc->management_company_id,
            'room_id' => $room->room_id,
        ]);

        //認証コード
        $code = $this->faker->regexify('[A-Za-z0-9]{50}');
        Cache::put($code, $resident, 600);
        //パスワード
        $pass = $this->faker->password;

        //本登録処理
        $data = [
            'code' => $code,
            'password' => $pass,
            'password_confirmation' => $pass,
        ];
        $response = $this->post('/account/register', $data);

        //検証
        $response->assertStatus(200);
        $updateResident = \App\Models\Resident::find($resident->resident_id);
        $this->assertTrue($updateResident->user_id !== null);
        $user = \App\Models\User::find($updateResident->user_id);
        $this->assertTrue(Hash::check($pass, $user->password));
    }
}
