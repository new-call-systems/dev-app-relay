<?php

namespace Tests\Unit\mc;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ReportTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed('LaravelPermissionSeeder');
        $this->seed('LaravelPermissionAddRolesSeeder');
        $this->seed('LaravelPermissionAddRoles2Seeder');
        $this->seed('UserSeeder');
        $this->get('');
    }

    public function tearDown(): void
    {
        Artisan::call('migrate:refresh');
        parent::tearDown();
    }

    /**
     * ログインしていない場合、
     * 完了報告書の承認ができずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_approve_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書承認
        $this->post('/mc/report/'.$createRequestReport->request_report_id.'/approve')
            ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、完了報告書が承認されること
     * （正常）
     * @test
     */
    public function authenticated_user_can_approve_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書承認
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/approve');

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === config('const.Reports.Status.COMPLETED'));
    }

    /**
     * 他管理会社の完了報告書は承認できないこと
     * （正常）
     * @test
     */
    public function user_cannot_approve_other_management_companies_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $otherMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $otherMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $otherMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $otherMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $otherMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $otherMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書承認
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/approve');

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === $createRequestReport->report_status);
    }

    /**
     * ステータス = 'SUBMITTED'でない完了報告書は承認できないこと
     * （正常）
     * @test
     */
    public function user_cannot_approve_report_status_is_not_submitted()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書承認
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/approve');

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === $createRequestReport->report_status);
    }

    /**
     * ログインしていない場合、
     * 完了報告書の差し戻しができずログイン画面が表示されること
     * @test
     */
    public function unauthenticated_user_cannot_decline_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書差し戻し
        $data = [
            'comment' => '差し戻しコメント',
        ];
        $this->post('/mc/report/'.$createRequestReport->request_report_id.'/decline', $data)
            ->assertRedirect('/mc/login');
    }

    /**
     * ログインしている場合、完了報告書が差し戻しできること
     * （正常）
     * @test
     */
    public function authenticated_user_can_decline_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書差し戻し
        $data = [
            'comment' => '差し戻しコメント',
        ];
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/decline', $data);

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === config('const.Reports.Status.DECLINED'));
        $this->assertTrue($report->comment === $data['comment']);
    }

    /**
     * 報告書差し戻し時バリデーション
     * @test
     */
    public function validator_check_for_decline_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //****************************
        // 必須チェック
        //****************************
        $data = [
            'comment' => '',
        ];
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/decline', $data);

        //検証
        $response->assertSessionHasErrors([
            'comment' => 'commentは必ず指定してください。',
        ]);
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === $createRequestReport->report_status);
        $this->assertTrue($report->comment === $createRequestReport->comment);
    }

    /**
     * 他管理会社の完了報告書は差し戻しできないこと
     * （正常）
     * @test
     */
    public function user_cannot_decline_other_management_companies_report()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $otherMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $otherMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $otherMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $otherMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $otherMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $otherMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.SUBMITTED'),
        ]);

        //完了報告書差し戻し
        $data = [
            'comment' => '差し戻しコメント',
        ];
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/decline', $data);

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === $createRequestReport->report_status);
        $this->assertTrue($report->comment === $createRequestReport->comment);
    }

    /**
     * ステータス = 'SUBMITTED'でない完了報告書は差し戻しできないこと
     * （正常）
     * @test
     */
    public function user_cannot_decline_report_status_is_not_submitted()
    {
        $loginUser = factory(\App\Models\User::class)->create();
        $loginUser->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER'), config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
        $userMc = factory(\App\Models\ManagementCompany::class)->create();
        $loginUserMcm = factory(\App\Models\ManagementCompanyMember::class)->create([
            'user_id' => $loginUser->user_id,
            'management_company_id' => $userMc->management_company_id,
            'representative_flg' => true,
        ]);
        $vendorUser = factory(\App\Models\User::class)->create();
        $vendorUser->assignRole([config('const.Roles.VENDOR_MANAGER'), config('const.Roles.VENDOR_MEMBER')]);
        $createVn = factory(\App\Models\Vendor::class)->create();
        $representativeFlg = 1;
        $createVnm = factory(\App\Models\VendorMember::class)->create([
            'user_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'representative_flg' => $representativeFlg,
        ]);
        $property1 = factory(\App\Models\Property::class)->create([
            'management_company_id' => $userMc->management_company_id,
        ]);
        $room1 = factory(\App\Models\Room::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'property_id' => $property1->property_id,
        ]);
        $resident1 = factory(\App\Models\Resident::class)->create([
            'management_company_id' => $userMc->management_company_id,
            'room_id' => $room1->room_id,
        ]);
        $createRequest = factory(\App\Models\Request::class)->create([
            'resident_id' => $resident1->resident_id,
            'management_company_id' => $userMc->management_company_id,
            'visibility' => config('const.Request.Visibility.PUBLIC'),
            'status' => config('const.Request.Status.COMPLETED'),
        ]);
        $createContent = factory(\App\Models\RequestContent::class)->create([
            'context' => '問合せ内容',
            'request_id' => $createRequest->request_id,
        ]);
        $createHandleUser = factory(\App\Models\RequestHandleUser::class)->create([
            'request_id' => $createRequest->request_id,
            'user_id' => $vendorUser->user_id,
            'group_id' => $createVn->vendor_id,
        ]);
        $createRequestReport = factory(\App\Models\RequestReport::class)->create([
            'request_id' => $createRequest->request_id,
            'author_id' => $vendorUser->user_id,
            'vendor_id' => $createVn->vendor_id,
            'management_company_id' => $userMc->management_company_id,
            'resident_id' => $resident1->resident_id,
            'report_status' => config('const.Reports.Status.DRAFT'),
        ]);

        //完了報告書差し戻し
        $data = [
            'comment' => '差し戻しコメント',
        ];
        $response =
        $this->actingAs($loginUser, 'guard_management_company_member')
        ->withSession(['management_company_id' => $userMc->management_company_id])
        ->post('/mc/report/'.$createRequestReport->request_report_id.'/decline', $data);

        //検証
        $report = \App\Models\RequestReport::find($createRequestReport->request_report_id);
        $this->assertTrue($report->report_status === $createRequestReport->report_status);
        $this->assertTrue($report->comment === $createRequestReport->comment);
    }
}
