<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'guard_default'
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'guard_default' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'guard_system_owner' => [
            'driver' => 'custom_session',
            'provider' => 'users',
        ],

        'guard_vendor_manager' => [
            'driver' => 'custom_session',
            'provider' => 'users',
        ],

        'guard_management_company_member' => [
            'driver' => 'custom_session',
            'provider' => 'users',
        ],

        'guard_management_company' => [
            'driver' => 'custom_mc',
            'provider' => 'management_companies',
        ],

        'guard_vendor_member' => [
            'driver' => 'custom_token',
            'provider' => 'vendor_members'
        ],

        'guard_resident' => [
            'driver' => 'custom_token',
            'provider' => 'residents',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
        'vendor_members' => [
            'driver' => 'custom_token_provider',
            'model' => App\Models\VendorMember::class,
        ],
        'residents' => [
            'driver' => 'custom_token_provider',
            'model' => App\Models\Resident::class,
        ],
        'management_companies' => [
            'driver' => 'custom_mc_provider',
            'model' => App\Models\ManagementCompany::class,
        ],
    ],

];
