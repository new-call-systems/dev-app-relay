<?php

return [
    'Roles' => [
        'SYSTEM_OWNER' => 'system_owner',
        'VENDOR_MANAGER' => 'vendor_manager',
        'VENDOR_MEMBER' => 'vendor_member',
        'MANAGEMENT_COMPANY_MANAGER' => 'management_company_manager',
        'MANAGEMENT_COMPANY_MEMBER' => 'management_company_member',
        'RESIDENT' => 'resident',
    ],
    'Permissions' => [
        'SYSTEM_OWNER' => 'system_owner_permission',
        'VENDOR_MANAGER' => 'vendor_manager_permission',
        'VENDOR_MEMBER' => 'vendor_member_permission',
        'MANAGEMENT_COMPANY_MANAGER' => 'management_company_manager_permission',
        'MANAGEMENT_COMPANY_MEMBER' => 'management_company_member_permission',
        'RESIDENT' => 'resident_permission',
    ],
    'AiCategory' => [
        'COMMUNAL_AREA' => 'communal_area',
        'MANAGEMENT_COMPANY' => 'management_company',
        'SUPPORT_CENTER' => 'support_center',
        'VENDOR' => 'vendor',
        'UNKNOWN' => 'unknown',
    ],

    //問合せ関連
    'Request' => [
        'RequestType' => [
            //業者対応。DIYの提示や、業者出動依頼をする。
            'VENDOR' => 'vendor',
            //契約問題など、自動で回答できるもの。回答内容に満足出来なかった場合、サポートセンターの連絡先を提示する。
            'AUTO_RESPONSE' => 'auto_response',
            //管理会社対応（＝自社対応）。サポートセンターの連絡先を提示する。
            'IN_HOUSE' => 'in_house',
        ],
        'Visibility' => [
            //request_type が “vendor” かつ 指定の業者が無い場合。
            'PUBLIC' => 'public',
            //request_type が “vendor” かつ 指定の業者がある場合。
            'PROTECTED' => 'protected',
            //request_typeが “auto_response” or “in_house”の時場合。
            'IN_HOUSE' => 'in_house',
        ],
        'Status' => [
            //問合せ内容が理解できない場合
            'WAITING_GRASPED' => 'waiting_grasped',
            //業者に出動依頼していない or visibility === “in_house”
            'WAITING_FOR_OPEN' => 'waiting_for_open',
            //キャンセル待ち
            'WAITING_FOR_CANCEL' => 'waiting_for_cancel',
            //業者による入札可能
            'OPEN' => 'open',
            //業者による入札済みで未承認
            'BID' => 'bid',
            //業者による入札済みで承認済み
            'SCHEDULING' => 'scheduling',
            //対応中（一応用意。作業開始報告が必要かどうか）
            'IN_PROGRESS' => 'in_progress',
            //完了
            'COMPLETED' => 'completed',
            //open時に業者が入札する前に取り下げされた場合。
            'DELETED' => 'deleted',
            //キャンセル状態。スプリント５で策定。
            'CANCELLED' => 'cancelled',
            //入居者が管理会社に電話した際のステータス
            'TRANSFERRED' => 'transferred',
            //auto_response時に入居者が満足したらこの値となる
            'AUTO_COMPLETED' => 'auto_completed',
        ],
    ],

    //完了報告書関連
    'Reports' => [
        'DispSection' => [
            //未提出
            'NOT_SUBMITTED' => 'not_submitted',
            //提出済・受信未処理
            'SUBMITTED' => 'submitted',
            //差戻し
            'REMAND' => 'remand',
            //承認済み
            'COMPLETED' => 'completed',
        ],
        'Status' => [
            //業者作業員がレポートを作成中
            'WRITING' => 'writing',
            //業者管理者が確認中、編集可能
            'DRAFT' => 'draft',
            //業者管理者が管理会社に提出
            'SUBMITTED' => 'submitted',
            //管理会社が閲覧し、完了状態
            'COMPLETED' => 'completed',
            //Version管理するならこれを差し戻し時に使う
            //'ABORTED' => 'aborted',
            //Version管理が必要ないならこれを差し戻し時に使う
            'DECLINED' => 'declined',
        ],
        'ActionStatus' => [
            //未完了引継ぎ
            'INCOMPLETE' => 'incomplete',
            //工事完了
            'COMPLETED' => 'completed',
        ],
    ],
];