<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use Auth;
use Session;


class CheckSessionTimeout {
    protected $session;

    public function __construct(Store $session){
        $this->session = $session;
        //configファイルからセッションタイムアウトの時間を取得　分 * 60秒
        $this->timeout = config('session.lifetime') * 60;
    }

    /**
     * リクエストのハンドリングを行います。
     * セッションがタイムアウトした場合には対応するログインページにリダイレクトします。
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //ログイン時lastActivityTimeを格納
        if (!session('lastActivityTime')) {
            $this->session->put('lastActivityTime', time());
        //現在時刻 - 最終操作時間　> タイムアウト時間
        } elseif (time() - $this->session->get('lastActivityTime') > $this->timeout) {
            //lastActivityTimeを削除
            $this->session->forget('lastActivityTime');

            //ログイン中のガードを取得してログイアウトする
            $guard = $this->getGuard();
            Auth::guard($guard)->logout();

            switch ($guard) {
                case 'guard_system_owner':
                    $loginRoute = 'so.login';
                    break;
                case 'guard_vendor_manager':
                    $loginRoute = 'vn.login';
                    break;
                case 'guard_management_company_member':
                    $loginRoute = 'mc.login';
                    break;
            }

            return redirect()->route($loginRoute)->with('info', 'セッションの有効期限が切れました。再度やり直してください。');
        } else {
            //通常操作時はlastActivityTimeを更新する
            $this->session->put('lastActivityTime', time());
        }

        return $next($request);
    }

    /**
     * 現在のガードを取得します。
     *
     * @return string|null
     */
    private function getGuard()
    {
        foreach (array_keys(config('auth.guards')) as $guard) {
            if (Auth::guard($guard)->check()) {
                return $guard;
            }
        }
        return null;
    }
}
