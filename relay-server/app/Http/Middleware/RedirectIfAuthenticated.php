<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && $guard === 'guard_system_owner') {
            return redirect(RouteServiceProvider::SO_HOME);
        } elseif (Auth::guard($guard)->check() && $guard === 'guard_vendor_manager') {
            return redirect(RouteServiceProvider::VN_HOME);
        } elseif (Auth::guard($guard)->check() && $guard === 'guard_management_company_member') {
            return redirect(RouteServiceProvider::MC_HOME);
        }

        return $next($request);
    }
}
