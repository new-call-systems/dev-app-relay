<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    protected $so_route  = 'so.login';
    protected $mc_route = 'mc.login';
    protected $vn_route = 'vn.login';

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // ルーティングに応じて未ログイン時のリダイレクト先を振り分ける
        if (!$request->expectsJson()) {
            if (Route::is('so.*')) {
                return route($this->so_route);
            } elseif (Route::is('mc.*')) {
                return route($this->mc_route);
            } elseif (Route::is('vn.*')) {
                return route($this->vn_route);
            } elseif ($request->is('api/*')) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        }
    }
}
