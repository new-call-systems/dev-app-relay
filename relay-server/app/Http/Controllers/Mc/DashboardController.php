<?php

namespace App\Http\Controllers\Mc;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $mcId = $request->session()->get('management_company_id');
        //過去1ヶ月の時間帯別問合せ数
        $selectChartByTimeZone =
        \App\Models\Request::
        select(
            \DB::raw('HOUR(created_at) as hour'),
            \DB::raw('COUNT(*) as cnt'),
        )
        ->where('created_at', '>=', Carbon::now()->subMonth())
        ->where('management_company_id', $mcId)
        ->groupBy('hour')
        ->get();

        $chartByTimeZone = array();
        for ($i = 0; $i < 24; $i++) {
            foreach ($selectChartByTimeZone as $cbtz) {
                if ($i == $cbtz->hour) {
                    array_push($chartByTimeZone, $cbtz->cnt);
                }
            }
            if (count($chartByTimeZone) === $i) {
                array_push($chartByTimeZone, 0);
            }
        }

        //過去1ヶ月の曜日別問合せ数
        $selectChartByWeek =
        \App\Models\Request::
        select(
            \DB::raw('DAYOFWEEK(created_at) as week'),
            \DB::raw('COUNT(*) as cnt'),
        )
        ->where('created_at', '>=', Carbon::now()->subMonth())
        ->where('management_company_id', $mcId)
        ->groupBy('week')
        ->get();

        $chartByWeek = array();
        for ($i = 1; $i <= 7; $i++) {
            foreach ($selectChartByWeek as $cbw) {
                if ($i == $cbw->week) {
                    array_push($chartByWeek, $cbw->cnt);
                }
            }
            if (count($chartByWeek) === $i - 1) {
                array_push($chartByWeek, 0);
            }
        }

        //業者別 対応中件数
        $chartByVendor =
        \App\Models\Request::
        select(
            'ven.vendor_id',
            'ven.name',
            \DB::raw('COUNT(*) as cnt'),
        )
        ->from('requests as r')
        ->join('request_handle_users as rhu', function ($join) {
            $join->on('rhu.request_id', 'r.request_id');
        })
        ->join('vendors as ven', function ($join) {
            $join->on('ven.vendor_id', 'rhu.group_id');
        })
        ->where('r.management_company_id', $mcId)
        ->whereIn('r.status', [
            config('const.Request.Status.SCHEDULING'),
            config('const.Request.Status.IN_PROGRESS'),
        ])
        ->groupBy([
            'ven.vendor_id',
            'ven.name',
        ])
        ->get();

        //問合せ件数
        $count =
        \App\Models\Request::
        select(
            \DB::raw('COUNT(status = "'.config('const.Request.Status.OPEN').'" or null) as not_bid_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.BID').'" or status = "'.config('const.Request.Status.SCHEDULING').'" or status = "'.config('const.Request.Status.IN_PROGRESS').'" or status = "'.config('const.Request.Status.WAITING_FOR_CANCEL').'" or null) as in_progress_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.COMPLETED').'" or null) as completed_count'),
        )
        ->where('management_company_id', $mcId)
        ->where('request_type', config('const.Request.RequestType.VENDOR'))
        ->first();

        //12時間以上未入札件数
        $past12HourCount =
        \App\Models\Request::
        select(
            \DB::raw('COUNT(*) as cnt'),
        )
        ->where('management_company_id', $mcId)
        ->where('status', config('const.Request.Status.OPEN'))
        ->where('updated_at', '<=', Carbon::now()->subHour(12))
        ->where('request_type', config('const.Request.RequestType.VENDOR'))
        ->first();

        return view('mc.dashboard', [
            'chartByTimeZone' => $chartByTimeZone,
            'chartByWeek' => $chartByWeek,
            'chartByVendor' => $chartByVendor,
            'count' => $count,
            'past12HourCount' => $past12HourCount->cnt,
        ]);
    }
}
