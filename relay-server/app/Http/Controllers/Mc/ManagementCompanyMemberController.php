<?php

namespace App\Http\Controllers\Mc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ManagementCompanyMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $managementCompanyId = $request->session()->get('management_company_id');

        $managementCompanyMembers =
        \App\Models\ManagementCompanyMember::
        select(
        'user.user_id',
        'mcm.last_name',
        'mcm.first_name',
        'user.email',
        'mcm.emergency_tel',
        'mcm.representative_flg',
        'mcm.department',
        'mcm.created_at',
        'mcm.updated_at',)
        ->from('management_company_members as mcm')
        ->join('users as user', function ($join) {
            $join->on('user.user_id', 'mcm.user_id');
        })
        ->where('mcm.management_company_id', $managementCompanyId)
        ->orderByRaw('mcm.updated_at DESC')
        ->paginate(11);

        foreach ($managementCompanyMembers as $mcm) {
            $user = \App\Models\User::find($mcm->user_id);
            if ($user->hasRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'))) {
                $mcm->manager_flg = true;
            } else {
                $mcm->manager_flg = false;
            }
        }

        return view('mc/managementCompanyMember/index', ['managementCompanyMembers' => $managementCompanyMembers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //バリデーション
        $validatedData = $request->validate([
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
            'department' => 'nullable|string|max:255',
            'manager_flg' => 'nullable|string|max:1',
        ]);


        try {
            DB::transaction(function () use ($request) {

                \Log::debug('START');
                //管理会社ID
                $managementCompanyId = $request->session()->get('management_company_id');

                //パスワード発行
                $password = $this->makePassword();

                //ユーザー登録
                $user = new \App\Models\User;
                $user->email = $request->email;
                $user->password = Hash::make($password);
                $user->save();
                //ユーザーロール登録
                if ($request->manager_flg === '1') {
                    //全アクセス権限
                    $user->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MANAGER')]);
                } else {
                    //全アクセス権限でない
                    $user->assignRole([config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
                }

                //管理会社担当者登録
                $mcm = new \App\Models\ManagementCompanyMember();
                $mcm->user_id = $user->user_id;
                $mcm->management_company_id = $managementCompanyId;
                $mcm->last_name = $request->last_name;
                $mcm->first_name = $request->first_name;
                $mcm->emergency_tel = $request->emergency_tel;
                //$mcm->representative_flg = $request->representative_flg;
                $mcm->department = $request->department;
                $mcm->save();

                //登録メール送信
                Mail::to($request->email)
                    ->send(
                        new \App\Mail\CreateAccountManagementCompanyMemberNotification(
                            $request->last_name.' '.$request->first_name,
                            $request->email,
                            $password)
                    );
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '登録が完了しました。');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //バリデーション
        $validatedData = $request->validate([
            'user_id' => 'required',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
            'department' => 'nullable|string|max:255',
            'manager_flg' => 'nullable|string|max:1',
        ]);

        try {
            DB::transaction(function () use ($request) {

                \Log::debug('START');

                //変数
                $userId = $request->user_id;
                $managementCompanyId = $request->session()->get('management_company_id');

                //ユーザーロール登録
                $user = \App\Models\User::find($userId);
                if ($request->manager_flg === '1') {
                    //全アクセス権限
                    $user->syncRoles([config('const.Roles.MANAGEMENT_COMPANY_MANAGER')]);
                } else {
                    //全アクセス権限でない
                    $user->syncRoles([config('const.Roles.MANAGEMENT_COMPANY_MEMBER')]);
                }

                //管理会社担当者
                $mcm = \App\Models\ManagementCompanyMember::where(['user_id' => $userId, 'management_company_id' => $managementCompanyId])->first();
                $mcm->last_name = $request->last_name;
                $mcm->first_name = $request->first_name;
                $mcm->emergency_tel = $request->emergency_tel;
                $mcm->department = $request->department;

                $mcm->checkUpdatedAt($request->updated_at);
                $mcm->save();
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '更新が完了しました。');
    }

    /**
     * パスワード生成処理
     *
     * @param int $length
     * @return string
     */
    private function makePassword($length = 8) {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
