<?php

namespace App\Http\Controllers\Mc;

use App\Library\Cmn;
use App\Library\S3Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * 完了報告一覧画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportList(Request $webRequest)
    {
        $reportId = $webRequest->report_id;
        $mcId = $webRequest->session()->get('management_company_id');
        $s = $webRequest->s;
        $section = ($webRequest->section) ? $webRequest->section : config('const.Reports.DispSection.SUBMITTED');

        //検索条件
        $propertyName = $webRequest->property_name;
        $actionStatus = $webRequest->action_status;
        $keyword = $webRequest->keyword;
        $receptionDateFrom = $webRequest->reception_date_from;
        $receptionDateTo = $webRequest->reception_date_to;
        $constructionDateFrom = $webRequest->construction_date_from;
        $constructionDateTo = $webRequest->construction_date_to;
        $conditions = [
            'property_name' => $propertyName,
            'action_status' => $actionStatus,
            'keyword' => $keyword,
            'reception_date_from' => $receptionDateFrom,
            'reception_date_to' => $receptionDateTo,
            'construction_date_from' => $constructionDateFrom,
            'construction_date_to' => $constructionDateTo,
            'section' => $section,
        ];

        //完了報告一覧取得
        $query =
        \App\Models\RequestReport::
        select(
            'request_reports.request_report_id',
            'request_reports.request_id',
            'request_reports.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'request_reports.reception_date',
            'request_reports.report_status',
            'request_reports.action_status',
            'request_reports.action_date',
            'request_reports.action_section',
            'request_reports.construction_amount',
            'resident.name as resident_name',
            'vn.name as vendor_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'request_reports.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'request_reports.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) {
            $join->on('rhu.request_id', 'request_reports.request_id');
        })
        ->join('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->join('vendors as vn', function ($join) {
            $join->on('vn.vendor_id', 'vnm.vendor_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'request_reports.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'request_reports.request_id');
        })
        ->where('request_reports.management_company_id', $mcId)
        ->groupBy(
            'request_reports.request_report_id',
            'request_reports.request_id',
            'request_reports.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'request_reports.reception_date',
            'request_reports.report_status',
            'request_reports.action_status',
            'request_reports.action_date',
            'request_reports.action_section',
            'request_reports.construction_amount',
            'resident.name',
            'vn.name',
        );

        //1件のみ表示かどうか
        if ($reportId) {
            $query->where('request_reports.request_report_id', $reportId);
            $query->whereIn('request_reports.report_status', [config('const.Reports.Status.SUBMITTED'), config('const.Reports.Status.COMPLETED'), config('const.Reports.Status.DECLINED')]);

        } else {
            //区分
            switch ($section) {
                case config('const.Reports.DispSection.SUBMITTED') :
                    $query->whereIn('request_reports.report_status', [config('const.Reports.Status.SUBMITTED')]);
                    break;
                case config('const.Reports.DispSection.COMPLETED') :
                    $query->whereIn('request_reports.report_status', [config('const.Reports.Status.COMPLETED')]);
                    break;
                case config('const.Reports.DispSection.REMAND') :
                    $query->whereIn('request_reports.report_status', [config('const.Reports.Status.DECLINED')]);
                    break;
            }

            //検索条件
            if ($propertyName) {
                $query->where('property.name', 'LIKE', '%'.$propertyName.'%');
            }
            if ($actionStatus) {
                $query->where('request_reports.action_status', $actionStatus);
            }
            if ($receptionDateFrom) {
                $query->where('request_reports.reception_date', '>=', $receptionDateFrom.' 00:00:00');
            }
            if ($receptionDateTo) {
                $query->where('request_reports.reception_date', '<=', $receptionDateTo.' 23:59:59');
            }
            if ($constructionDateFrom) {
                $query->where('request_reports.action_date', '>=', $constructionDateFrom.' 00:00:00');
            }
            if ($constructionDateTo) {
                $query->where('request_reports.action_date', '<=', $constructionDateTo.' 23:59:59');
            }
            if ($keyword) {
                $query->where(function ($query) use ($keyword) {
                    $query->orWhere('property.name', 'LIKE', '%'.$keyword.'%')
                        ->orWhere('request_reports.description', 'LIKE', '%'.$keyword.'%')
                        ->orWhere('rc.context', 'LIKE', '%'.$keyword.'%');
                });
            }
        }
        $reports = $query->sortable()->paginate(10);

        //1件のみ表示の場合、sectionを再設定する
        if ($reportId) {
            foreach ($reports as $report) {
                switch ($report->report_status) {
                    case config('const.Reports.Status.SUBMITTED') :
                        $section = config('const.Reports.DispSection.SUBMITTED');
                        break;
                    case config('const.Reports.Status.COMPLETED') :
                        $section = config('const.Reports.DispSection.COMPLETED');
                        break;
                    case config('const.Reports.Status.DECLINED') :
                        $section = config('const.Reports.DispSection.REMAND');
                        break;
                }
            }
        }

        //件数取得
        //受信未処理
        $submittedCnt =
        \App\Models\RequestReport::
        select(DB::raw('count(*) AS cnt'))
        ->where('management_company_id', $mcId)
        ->whereIn('report_status', [config('const.Reports.Status.SUBMITTED')])
        ->first();
        //差戻し
        $declinedCnt =
        \App\Models\RequestReport::
        select(DB::raw('count(*) AS cnt'))
        ->where('management_company_id', $mcId)
        ->whereIn('report_status', [config('const.Reports.Status.DECLINED')])
        ->first();

        //S3
        $s3 = new S3Client();
        foreach ($reports as $r) {
            $prefix = 'reports/'.$r->vendor_id.'/'.$r->request_id.'/'.$r->request_report_id;
            $r->image_path = $s3->getPresignedUrlForReport($prefix, false);
        }

        return view('mc/report/reportList', [
            'reports' => $reports,
            'conditions' => $conditions,
            'section' => $section,
            's' => $s,
            'submitted_cnt' => $submittedCnt->cnt,
            'declined_cnt' => $declinedCnt->cnt,
            'report_id' => $reportId,
        ]);
    }

    /**
     * 完了報告詳細画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportDetail(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) {
            $join->on('rhu.request_id', 'rr.request_id');
        })
        ->join('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.management_company_id', $mcId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //画像一覧取得
        $s3 = new S3Client();
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id;
        $report->image_paths = $s3->getPresignedUrlForReport($prefix);

        return view('mc/report/reportDetail', ['report' => $report]);
    }

    /**
     * 完了報告承認処理
     *
     * @return \Illuminate\Http\Response
     */
    public function approveReport(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        where('request_report_id', $reportId)
        ->where('management_company_id', $mcId)
        ->first();

        //チェック
        if (!$report) {
            return back()->withInput()->withErrors(array('error' => '承認できない完了報告書です。'));
        }
        if ($report->report_status !== config('const.Reports.Status.SUBMITTED')) {
            return back()->withInput()->withErrors(array('error' => '承認できない完了報告書です。'));
        }

        //提出（ステータス更新）
        $report->report_status = config('const.Reports.Status.COMPLETED');
        $report->save();

        return redirect(route('mc.reportDetail', ['report_id' => $report->request_report_id]))->with('is_approve', '承認しました');
    }

    /**
     * 完了報告差戻し処理
     *
     * @return \Illuminate\Http\Response
     */
    public function declineReport(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //バリデーション
        $validatedData = $webRequest->validate([
            'comment' => 'required',
        ]);

        //更新対象
        $report =
        \App\Models\RequestReport::
        from('request_reports as rr')
        ->where('rr.request_report_id', $reportId)
        ->where('rr.management_company_id', $mcId)
        ->first();

        //チェック
        if (!$report) {
            return back()->withInput()->withErrors(array('error' => '差し戻しできない完了報告書です。'));
        }
        if ($report->report_status !== config('const.Reports.Status.SUBMITTED')) {
            return back()->withInput()->withErrors(array('error' => '差し戻しできない完了報告書です。'));
        }

        //更新
        $report->comment = $webRequest->comment;
        $report->report_status = config('const.Reports.Status.DECLINED');
        $report->save();

        return redirect(route('mc.reportDetail', ['report_id' => $report->request_report_id]))->with('is_declined', '差し戻しが完了しました。');
    }

    /**
     * 完了報告PDFダウンロード
     */
    public function downloadReportPdf(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) {
            $join->on('rhu.request_id', 'rr.request_id');
        })
        ->join('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.management_company_id', $mcId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //PDF生成
        $pdf = \PDF::loadView('mc/report/reportPdf', ['report' => $report])->setPaper('a4');
        $pdf->getDomPDF()->set_option('enable_font_subsetting', true);

        //envでstreamかdownloadかを管理
        if (Cmn::toBool(env('PDF_DOWNLOAD'))) {
            return $pdf->download('test.pdf');
        } else {
            return $pdf->stream('test.pdf');
        }
    }

    /**
     * Zipファイルダウンロード処理
     */
    public function downloadReportZip(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        where('request_report_id', $reportId)
        ->where('management_company_id', $mcId)
        ->first();

        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id;

        //zipダウンロード
        $s3->zipDownload($prefix);
    }
}
