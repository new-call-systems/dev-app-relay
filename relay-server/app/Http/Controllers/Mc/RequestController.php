<?php

namespace App\Http\Controllers\Mc;

use App\Library\Cmn;
use App\Library\S3Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $webRequest)
    {
        //検索条件
        $requestId = $webRequest->request_id;
        $requestType = $webRequest->request_type;
        $dateFrom = $webRequest->date_from;
        $dateTo = $webRequest->date_to;
        $requestStatuses = ($webRequest->request_statuses !== null) ? $webRequest->request_statuses : array();
        $conditions = [
            'request_type' => $requestType,
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'request_statuses' => $requestStatuses,
        ];

        //問合せ一覧取得
        $query =
        \App\Models\Request::
        select(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.request_type',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            \DB::raw('GROUP_CONCAT(reqcon.context separator " ") as context'),
            \DB::raw('GROUP_CONCAT(ven.name separator " , ") as vendor_name'),
            'rpt.date',
            'ts.label'
        )
        ->from('requests as req')
        ->join('request_contents as reqcon', function ($join) {
            $join->on('reqcon.request_id', 'req.request_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'req.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->leftJoin('request_handle_users as rhu', function ($join) {
            $join->on('rhu.request_id', 'req.request_id');
        })
        ->leftJoin('vendors as ven', function ($join) {
            $join->on('ven.vendor_id', 'rhu.group_id');
        })
        ->leftJoin('request_preferable_times as rpt', function ($join) {
            $join->on('rpt.request_id', 'req.request_id')
                 ->where('selected', true);
        })
        ->leftJoin('time_sections as ts', function ($join) {
            $join->on('ts.time_section_id', 'rpt.time_section_id');
        })
        ->where('req.management_company_id', $webRequest->session()->get('management_company_id'))
        ->where('req.status', '<>', config('const.Request.Status.WAITING_GRASPED'))
        ->groupBy(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.request_type',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name',
            'resident.tel',
            'property.name',
            'property.address',
            'room.room_no',
            'rpt.date',
            'ts.label'
        )
        ->orderByRaw('req.created_at DESC');

        //検索条件
        if ($requestId) {
            $query->where('req.request_id', $requestId);
        } else {
            if ($requestType) {
                $query->where('req.request_type', $requestType);
            }
            if ($dateFrom) {
                $query->where('req.created_at', '>=', $dateFrom.' 00:00:00');
            }
            if ($dateTo) {
                $query->where('req.created_at', '<=', $dateTo.' 23:59:59');
            }
            if (count($requestStatuses) !== 0) {
                $query->whereIn('req.status', $requestStatuses);
            }
        }
        $requests = $query->paginate(10);

        //S3インスタンス生成
        $s3 = new S3Client();

        foreach ($requests as $request) {
            //*************
            // その他情報
            //*************
            //希望時間
            if (!$request->emergency_flg) {
                $rpt =
                \App\Models\RequestPreferableTime::
                select(
                    'rpt.date',
                    'ts.label',
                    'rpt.selected',
                )
                ->from('request_preferable_times as rpt')
                ->join('time_sections as ts', function ($join) {
                    $join->on('ts.time_section_id', 'rpt.time_section_id');
                })
                ->where('rpt.request_id', $request->request_id)
                ->orderByRaw('rpt.priority ASC')
                ->get();

                $request->preferable_times = $rpt;
            }
            //業者作業者情報
            if ($request->vendor_name) {
                $rhu =
                \App\Models\RequestHandleUser::
                select(
                    \DB::raw('ven.name as vendor_name'),
                    \DB::raw('ven.tel as vendor_tel'),
                    \DB::raw('vm.last_name as vm_last_name'),
                    \DB::raw('vm.first_name as vm_first_name'),
                    \DB::raw('vm.emergency_tel as vm_emergency_tel'),
                )
                ->from('request_handle_users as rhu')
                ->join('vendors as ven', function ($join) {
                    $join->on('ven.vendor_id', 'rhu.group_id');
                })
                ->join('vendor_members as vm', function ($join) {
                    $join->on('vm.user_id', 'rhu.user_id')
                         ->on('vm.vendor_id', 'rhu.group_id');
                })
                ->where('rhu.request_id', $request->request_id)
                ->get();

                $request->handle_users = $rhu;
            }

            //*************
            // ラベル化
            //*************
            //request_type
            $request->request_type = Cmn::requestType2Label($request->request_type);
            //status
            $request->status = Cmn::status2Label($request->status);
            //date
            $request->date = ($request->date) ? Cmn::dateFormat($request->date).' '.$request->label : '-----';
            if ($request->emergency_flg) $request->date = '緊急';
            //vendor_name
            if (!$request->vendor_name) $request->vendor_name = '-----';
            //vendor_tel
            if (!$request->vendor_tel) $request->vendor_tel = '-----';

            //**************
            // 画像・動画
            //**************
            //一覧取得
            $prefix = 'reports/'.$request->management_company_id.'/'.$request->resident_id.'/'.$request->request_id;
            $request->image_paths = $s3->getPresignedUrlForRequest($prefix);
        }

        return view('mc/request/index', ['requests' => $requests, 'conditions' => $conditions, 'request_id' => $requestId]);
    }

    public function exportRequestCsv(Request $webRequest)
    {
        // リクエストデータ取得
        $requests = json_decode($webRequest->input('requests'), true);
        $requestsData = $requests['data'];


        // CSVの内容を格納する変数を初期化
        $csvOutput = '';

        // ヘッダーの作成
        $headers = ['問合せ日時', '対応状況', '問合せ種別', '建物名', '部屋番号', '入居者名','問合せ内容',];
        $csvOutput .= implode(',', array_map(function($header) {
                return mb_convert_encoding($header, 'SJIS-win', 'UTF-8');
            }, $headers)) . "\n";

        $timezone = new \DateTimeZone('Asia/Tokyo'); // JSTのタイムゾーンを指定
        //データの書き込み
        foreach ($requestsData as $request) {
            $date = new \DateTime($request['created_at']);
            $date->setTimezone($timezone); // タイムゾーンをJSTに変更
            $formattedDate = $date->format('Y/m/d H:i:s');
            $csvOutput .= implode(',', [
                    mb_convert_encoding($formattedDate, 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['status'], 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['request_type'], 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['property_name'], 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['room_no'], 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['resident_name'], 'SJIS-win', 'UTF-8'),
                    mb_convert_encoding($request['context'], 'SJIS-win', 'UTF-8'),
                ]) . "\n";
        }

        $response = Response::make($csvOutput);
        $response->header('Content-Type', 'text/csv');
        $response->header('Content-Disposition', 'attachment; filename="residents.csv"');
        return $response;
    }
}
