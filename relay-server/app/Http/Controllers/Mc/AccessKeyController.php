<?php

namespace App\Http\Controllers\Mc;

use App\Traits\CsvTrait as Csv;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccessKeyController extends Controller
{
    use Csv;

    /**
     * Get AccessKey
     */
    public function getAccessKey(Request $request)
    {
        try {
            //キー情報取得
            $managementCompanyId = $request->session()->get('management_company_id');
            $mcaak = \App\Models\ManagementCompanyApiAccessKey::find($managementCompanyId);
            if (!$mcaak) {
                throw new \Exception('管理会社情報が取得できなかったため、AccessKeyのダウンロードに失敗しました。');
            }
            $accessKeyId = $mcaak->access_key_id;
            $secretKey = $mcaak->secret_key;

            //csvファイル情報
            $fileName = 'accessKey.csv';
            $file = Csv::createCsv($fileName);

            //書き込み
            Csv::write($file, ['accessKeyId='.$accessKeyId]);
            Csv::write($file, ['secretKey='.$secretKey]);
            //ファイル情報取得
            $response = file_get_contents($file);
            //ストリームに入れたらファイル削除
            Csv::purge($fileName);

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return response($response, 200)
        ->header('Content-Type', 'text/csv')
        ->header('Content-Disposition', 'attachment; filename='.$fileName);

    }
}
