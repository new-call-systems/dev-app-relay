<?php

namespace App\Http\Controllers\Mc;

use App\Library\Cmn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResidentController extends Controller
{
    /**
     * 入居者一覧画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getResidentList(Request $webRequest)
    {
        $mcId = $webRequest->session()->get('management_company_id');

        //検索条件
        $propertyName = $webRequest->property_name;
        $keyword = $webRequest->keyword;
        $conditions = [
            'property_name' => $propertyName,
            'keyword' => $keyword,
        ];

        //入居者一覧取得
        $query =
        \App\Models\Resident::
        select(
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            // 'resident.email as resident_email',
            \DB::raw('"test@example.com" as resident_email'),
        )
        ->from('residents as resident')
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->where('resident.management_company_id', $mcId)
        ->orderByRaw('property.name ASC, LENGTH(room.room_no) ASC, room.room_no ASC');

        //検索条件
        if ($propertyName) {
            $query->where('property.name', 'LIKE', '%'.$propertyName.'%');
        }
        if ($keyword) {
            $query->where(function ($query) use ($keyword) {
                $query->orWhere('property.name', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('property.address', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('resident.name', 'LIKE', '%'.$keyword.'%');
            });
        }
        $residents = $query->paginate(10);

        return view('mc/resident/residentList', [
            'residents' => $residents,
            'conditions' => $conditions,
        ]);
    }

}
