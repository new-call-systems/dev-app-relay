<?php

namespace App\Http\Controllers\Mc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Hidehalo\Nanoid\Client;

class AccountController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $webRequest)
    {
        //GETパラメータより認証コード取得
        $code = $webRequest->c;
        if (!$code) {
            abort(403);
        }

        //認証コードから入居者特定
        $resident = Cache::get($code);
        if (!$resident) {
            abort(403);
        }

        return view('mc.other.accountRegister', ['code' => $code, 'email' => $resident->email]);
    }

    /**
     * 入居者本登録処理
     */
    public function register(Request $webRequest)
    {
        //パラメータより認証コード取得
        $code = $webRequest->code;
        if (!$code) {
            abort(403);
        }

        //認証コードから入居者特定
        $resident = Cache::get($code);
        if (!$resident) {
            abort(403);
        }

        //バリデーション
        $validatedData = $webRequest->validate([
            'password' => 'required|confirmed|min:8|max:128',
            'password_confirmation' => 'required',
        ]);

        //パラメータ
        $password = $webRequest->password;

        //ログインID生成
        $client = new Client();
        $loginId = $client->formattedId('1234567890', 15);

        \DB::transaction(function () use ($resident, $loginId, $password) {
            //登録処理
            $user = new \App\Models\User;
            $user->email = $loginId;
            $user->password = Hash::make($password);
            $user->save();
            //ユーザーロール登録
            $user->assignRole(config('const.Roles.RESIDENT'));
            //更新処理
            $resident->user_id = $user->user_id;
            $resident->save();
        });

        //メール送信
        Mail::to($resident->email)
        ->send(
            new \App\Mail\ResidentMainRegistrationNotification(
                $resident->name,
                $loginId
            )
        );

        //キャッシュクリア
        Cache::forget($code);

        return view('mc.other.accountRegister', ['email' => $resident->email, 's' => 'success']);
    }
}
