<?php

namespace App\Http\Controllers\Vn;

use App\Library\Cmn;
use App\Mail\CreateAccountVendorMemberNotification;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VendorMember;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVendor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class VendorMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->session()->get('vendor_id');
        $vendorMembers =
        \App\Models\VendorMember::select(
            'user.user_id',
            'vnm.first_name',
            'vnm.last_name',
            'user.email',
            'vnm.emergency_tel',
            'vnm.created_at',
            'vnm.updated_at',
            'vnm.representative_flg',
            'vnm.retire_flg',
        )
        ->from('vendor_members as vnm')
        ->leftJoin('users as user', function ($join) {
            $join->on('user.user_id', 'vnm.user_id');
        })
        ->where('vnm.vendor_id', $vendorId)
        ->whereNull('vnm.retire_flg')
        ->orderByRaw('vnm.updated_at DESC')->paginate(11);
        return view('vn/vendorMember/index', ['vendorMembers' => $vendorMembers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //バリデーション
        $validatedData = $request->validate([
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
        ]);

        try {
            DB::transaction(function () use ($request) {
                //業者ID
                $vendorId = $request->session()->get('vendor_id');
                //パスワード発行
                $password = $this->makePassword();

                //ユーザー登録
                $user = new User;
                $user->email = $request->email;
                $user->password = Hash::make($password);
                $user->save();
                //ユーザーロール登録
                $user->assignRole(['vendor_member']);

                //業者作業者登録
                $vendorMember = new VendorMember;
                $vendorMember->user_id = $user->user_id;
                $vendorMember->vendor_id = $vendorId;
                $vendorMember->last_name = $request->last_name;
                $vendorMember->first_name = $request->first_name;
                $vendorMember->emergency_tel = $request->emergency_tel;
                $vendorMember->save();

                //登録メール送信
                Mail::to($request->email)
                    ->send(
                        new CreateAccountVendorMemberNotification(
                            $request->last_name.' '.$request->first_name,
                            $request->email,
                            $password)
                    );

            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '登録が完了しました。');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //バリデーション
        $validatedData = $request->validate([
            'user_id' => 'required',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
        ]);

        //変数
        $userId = $request->user_id;
        $vendorId = $request->session()->get('vendor_id');
        $retireFlg = Cmn::toBool($request->retire_flg);

        //業者作業者取得
        $vendorMember = VendorMember::where('vendor_id', $vendorId)->where('user_id', $userId)->first();

        if ($vendorMember->representative_flg &&  $retireFlg) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                'retire_flg' => '代表者の退職フラグは編集できません。',
            ]);
        }

        try {
            DB::transaction(function () use ($request, $vendorMember, $retireFlg) {
                $vendorMember->last_name = $request->last_name;
                $vendorMember->first_name = $request->first_name;
                $vendorMember->emergency_tel = $request->emergency_tel;
                $vendorMember->retire_flg = $retireFlg;
                $vendorMember->checkUpdatedAt($request->updated_at);
                $vendorMember->save();
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '登録が完了しました。');
    }

    /**
     * パスワード生成処理
     *
     * @param int $length
     * @return string
     */
    private function makePassword($length = 8) {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
