<?php

namespace App\Http\Controllers\Vn;

use App\Library\Cmn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //管理会社別対応件数
        $chartByMc =
        \App\Models\Request::
        select(
            'mc.name',
            'r.management_company_id',
            \DB::raw('COUNT(*) as cnt'),
        )
        ->from('requests as r')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'r.management_company_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
            ->where('rhu.group_id', $vendorId);
        })
        ->groupBy([
            'mc.name',
            'r.management_company_id',
        ])
        ->get();

        //時間別問合せ数
        $selectChartByTimeZone =
        \App\Models\Request::
        select(
            \DB::raw('HOUR(r.created_at) as hour'),
            \DB::raw('COUNT(*) as cnt'),
        )
        ->from('requests as r')
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
            ->where('rhu.group_id', $vendorId);
        })
        ->groupBy('hour')
        ->get();
        $chartByTimeZone = array();
        for ($i = 0; $i < 24; $i++) {
            foreach ($selectChartByTimeZone as $cbtz) {
                if ($i == $cbtz->hour) {
                    array_push($chartByTimeZone, $cbtz->cnt);
                }
            }
            if (count($chartByTimeZone) === $i) {
                array_push($chartByTimeZone, 0);
            }
        }

        return view('vn.dashboard', [
            'chartByTimeZone' => $chartByTimeZone,
            'chartByMc' => $chartByMc,
        ]);
    }
}
