<?php

namespace App\Http\Controllers\Vn;

use App\Library\Cmn;
use App\Mail\CreateAccountVendorMemberNotification;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VendorMember;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVendor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MyPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendorId = $request->session()->get('vendor_id');
        $vendor = \App\Models\Vendor::find($vendorId);
        return view('vn/mypage', ['vendor' => $vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //バリデーション
        $validatedData = $request->validate([
            'work_areas' => 'nullable',
            'work_time_from' => 'nullable|date_format:H:i',
            'work_time_to' => 'nullable|date_format:H:i',
            'work_weeks' => 'nullable',
            'work_items' => 'nullable',
        ]);

        try {
            DB::transaction(function () use ($request) {
                //変数
                $vendorId = $request->session()->get('vendor_id');
                $workAreas = $request->work_areas;
                $workTimeFrom = $request->work_time_from;
                $workTimeTo = $request->work_time_to;
                $workWeeks = $request->work_weeks;
                $workItems = $request->work_items;

                //更新
                $vendor = \App\Models\Vendor::find($vendorId);
                $vendor->work_areas = $workAreas;
                $vendor->work_time_from = $workTimeFrom;
                $vendor->work_time_to = $workTimeTo;
                $vendor->work_weeks = $workWeeks;
                $vendor->work_items = $workItems;
                $vendor->save();
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '更新が完了しました。');
    }
}
