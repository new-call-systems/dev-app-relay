<?php

namespace App\Http\Controllers\Vn;

use App\Library\Cmn;
use App\Library\S3Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * 完了報告一覧画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportList(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');
        $s = $webRequest->s;
        $section = ($webRequest->section) ? $webRequest->section : config('const.Reports.DispSection.NOT_SUBMITTED');

        //検索条件
        $propertyName = $webRequest->property_name;
        $actionStatus = $webRequest->action_status;
        $keyword = $webRequest->keyword;
        $receptionDateFrom = $webRequest->reception_date_from;
        $receptionDateTo = $webRequest->reception_date_to;
        $constructionDateFrom = $webRequest->construction_date_from;
        $constructionDateTo = $webRequest->construction_date_to;
        $conditions = [
            'property_name' => $propertyName,
            'action_status' => $actionStatus,
            'keyword' => $keyword,
            'reception_date_from' => $receptionDateFrom,
            'reception_date_to' => $receptionDateTo,
            'construction_date_from' => $constructionDateFrom,
            'construction_date_to' => $constructionDateTo,
            'section' => $section,
        ];

        //完了報告一覧取得
        $query =
        \App\Models\RequestReport::
        select(
            'request_reports.request_report_id',
            'request_reports.request_id',
            'request_reports.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'request_reports.reception_date',
            'request_reports.report_status',
            'request_reports.action_status',
            'request_reports.action_date',
            'request_reports.action_section',
            'request_reports.construction_amount',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'request_reports.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'request_reports.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'request_reports.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->join('vendor_members as vnm', function ($join) use ($vendorId) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'request_reports.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'request_reports.request_id');
        })
        ->where('request_reports.vendor_id', $vendorId)
        ->groupBy(
            'request_reports.request_report_id',
            'request_reports.request_id',
            'request_reports.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'request_reports.reception_date',
            'request_reports.report_status',
            'request_reports.action_status',
            'request_reports.action_date',
            'request_reports.action_section',
            'request_reports.construction_amount',
            'resident.name',
            'resident.name',
        );

        //区分
        switch ($section) {
            case config('const.Reports.DispSection.NOT_SUBMITTED') :
                $query->whereIn('request_reports.report_status', [config('const.Reports.Status.DRAFT')]);
                break;
            case config('const.Reports.DispSection.SUBMITTED') :
                $query->whereIn('request_reports.report_status', [config('const.Reports.Status.SUBMITTED'), config('const.Reports.Status.COMPLETED')]);
                break;
            case config('const.Reports.DispSection.REMAND') :
                $query->whereIn('request_reports.report_status', [config('const.Reports.Status.DECLINED')]);
                break;
        }

        //検索条件
        if ($propertyName) {
            $query->where('property.name', 'LIKE', '%'.$propertyName.'%');
        }
        if ($actionStatus) {
            $query->where('request_reports.action_status', $actionStatus);
        }
        if ($receptionDateFrom) {
            $query->where('request_reports.reception_date', '>=', $receptionDateFrom.' 00:00:00');
        }
        if ($receptionDateTo) {
            $query->where('request_reports.reception_date', '<=', $receptionDateTo.' 23:59:59');
        }
        if ($constructionDateFrom) {
            $query->where('request_reports.action_date', '>=', $constructionDateFrom.' 00:00:00');
        }
        if ($constructionDateTo) {
            $query->where('request_reports.action_date', '<=', $constructionDateTo.' 23:59:59');
        }
        if ($keyword) {
            $query->where(function ($query) use ($keyword) {
                $query->orWhere('property.name', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('request_reports.description', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('rc.context', 'LIKE', '%'.$keyword.'%');
            });
        }
        $reports = $query->sortable()->paginate(10);

        //S3インスタンス生成
        $s3 = new S3Client();

        foreach ($reports as $r) {
            //画像一覧取得
            $prefix = 'reports/'.$r->vendor_id.'/'.$r->request_id.'/'.$r->request_report_id;
            $r->image_path = $s3->getPresignedUrlForReport($prefix, false);
        }

        //件数取得
        //未提出
        $notSubmittedCnt =
        \App\Models\RequestReport::
        select(DB::raw('count(*) AS cnt'))
        ->where('vendor_id', $vendorId)
        ->whereIn('report_status', [config('const.Reports.Status.DRAFT')])
        ->first();
        //差戻し
        $declinedCnt =
        \App\Models\RequestReport::
        select(DB::raw('count(*) AS cnt'))
        ->where('vendor_id', $vendorId)
        ->whereIn('report_status', [config('const.Reports.Status.DECLINED')])
        ->first();
        return view('vn/report/reportList', [
            'reports' => $reports,
            'conditions' => $conditions,
            'section' => $section,
            's' => $s,
            'not_submitted_cnt' => $notSubmittedCnt->cnt,
            'declined_cnt' => $declinedCnt->cnt,
        ]);
    }

    /**
     * 完了報告詳細画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportDetail(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->join('vendor_members as vnm', function ($join) use ($vendorId) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.vendor_id', $vendorId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id;
        $report->image_paths = $s3->getPresignedUrlForReport($prefix);

        return view('vn/report/reportDetail', ['report' => $report]);
    }

    /**
     * 完了報告編集画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportEdit(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;
        $selectImages = (old('select_images')) ? old('select_images') : array();
        $deleteImages = old('delete_images');

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->join('vendor_members as vnm', function ($join) use ($vendorId) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.vendor_id', $vendorId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id;
        $delImages = explode(',', $deleteImages);
        $report->image_paths = $s3->getPresignedUrlForReport($prefix, true, $delImages);

        return view('vn/report/reportEdit', ['report' => $report, 'select_images' => $selectImages, 'delete_images' => $deleteImages, 'postMaxSize' => Cmn::getPostMaxSize()]);
    }

    /**
     * 完了報告プレビュー画面
     *
     * @return \Illuminate\Http\Response
     */
    public function getReportPreview(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;
        $selectImages = ($webRequest->select_images) ? $webRequest->select_images : array();
        $deleteImages = $webRequest->delete_images;

        //バリデーション
        $validatedData = $webRequest->validate([
            'action_date' => 'required|date',
            'starting_time' => 'nullable|date_format:H:i',
            'ending_time' => 'nullable|date_format:H:i',
            'construction_amount' => 'nullable|integer',
            'description' => 'required',
            'action_section' => 'required|max:1',
            'action_status' => 'required',
        ]);
        $this->checkNewLineCount($webRequest->description, 'description');

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->join('vendor_members as vnm', function ($join) use ($vendorId) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.vendor_id', $vendorId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'rr.vendor_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //チェック
        if (!$report) {
            return back()->withInput()->withErrors(array('error' => 'プレビューできない完了報告書です。'));
        }

        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id;
        $delImages = explode(',', $deleteImages);
        $imagePaths = $s3->getPresignedUrlForReport($prefix, true, $delImages);

        //アップロードイメージ
        foreach ($selectImages as $img) {
            array_push($imagePaths, $img);
        }

        //画像
        $report->image_paths = $imagePaths;

        //表示内容をパラメータで上書き
        $report->action_date = $webRequest->action_date;
        $report->starting_time = $webRequest->starting_time;
        $report->ending_time = $webRequest->ending_time;
        $report->construction_amount = $webRequest->construction_amount;
        $report->description = $webRequest->description;
        $report->action_section = $webRequest->action_section;
        $report->action_status = $webRequest->action_status;

        return view('vn/report/reportDetail', ['report' => $report, 'preview' => 'true', 'select_images' => $selectImages, 'delete_images' => $deleteImages]);
    }


    /**
     * 完了報告更新
     *
     * @return \Illuminate\Http\Response
     */
    public function updateReport(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //戻る判定
        if ($webRequest->back) {
            return redirect()->route('vn.reportEdit', ['report_id' => $reportId])->withInput();
        }

        //バリデーション
        $validatedData = $webRequest->validate([
            'action_date' => 'required|date',
            'starting_time' => 'nullable|date_format:H:i',
            'ending_time' => 'nullable|date_format:H:i',
            'construction_amount' => 'nullable|integer',
            'description' => 'required',
            'action_section' => 'required|max:1',
            'action_status' => 'required',
        ]);
        $this->checkNewLineCount($webRequest->description, 'description');

        //更新対象
        $report =
        \App\Models\RequestReport::
        from('request_reports as rr')
        ->where('rr.request_report_id', $reportId)
        ->where('rr.vendor_id', $vendorId)
        ->first();

        //チェック
        if (!$report) {
            return back()->withInput()->withErrors(array('error' => '更新できない完了報告書です。'));
        }
        if ($report->report_status !== config('const.Reports.Status.DRAFT') && $report->report_status !== config('const.Reports.Status.DECLINED')) {
            return back()->withInput()->withErrors(array('error' => '更新できない完了報告書です。'));
        }

        //更新
        $report->action_date = $webRequest->action_date;
        $report->starting_time = $webRequest->starting_time;
        $report->ending_time = $webRequest->ending_time;
        $report->construction_amount = $webRequest->construction_amount;
        $report->description = $webRequest->description;
        $report->action_section = $webRequest->action_section;
        $report->action_status = $webRequest->action_status;
        $report->save();

        //S3インスタンス生成
        $s3 = new S3Client();

        //S3アップロード
        if ($webRequest->select_images) {
            foreach ($webRequest->select_images as $img) {
                //[data:image/***;base64,]を取り除く
                $body = preg_replace('/data\:image\/.*\;base64\,/', '', $img);
                //拡張子をMIME-typeから取得
                //$extension = preg_replace('/data\:image\/(.*)\;base64\,.*/', '.\1', $img);
                $extension = Cmn::getExtension($img);
                $fileName = 'image_'.Carbon::now()->format('YmdHisu').$extension;
                $key = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id.'/'.$fileName;
                $s3->putObject($key, base64_decode($body));
            }
        }

        //S3削除
        $delImages = explode(',', $webRequest->delete_images);
        $prefix = 'reports/'.$report->vendor_id.'/'.$report->request_id.'/'.$report->request_report_id.'/';
        foreach ($delImages as $di) {
            $s3->deleteObject($prefix.$di);
        }

        //そのまま提出判定
        if ($webRequest->submit) {
            return $this->submitReport($webRequest);
        }

        $act = '';
        if ($report->report_status === config('const.Reports.Status.DRAFT')) {
            $act = 'is_update';
        } else {
            $act = 'is_update_decline';
        }

        return redirect(route('vn.reportDetail', ['report_id' => $report->request_report_id]))->with($act, '更新が完了しました。');
    }

    /**
     * 完了報告提出処理
     *
     * @return \Illuminate\Http\Response
     */
    public function submitReport(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        where('request_report_id', $reportId)
        ->where('vendor_id', $vendorId)
        ->first();

        //チェック
        if (!$report) {
            return back()->withInput()->withErrors(array('error' => '提出できない完了報告書です。'));
        }
        if ($report->report_status !== config('const.Reports.Status.DRAFT') && $report->report_status !== config('const.Reports.Status.DECLINED')) {
            return back()->withInput()->withErrors(array('error' => '提出できない完了報告書です。'));
        }

        //提出（ステータス更新）
        $report->report_status = config('const.Reports.Status.SUBMITTED');
        $report->save();

        //return redirect(url()->previous(route('vn.reportDetail', ['report_id' => $report->request_report_id])))->with('is_submit', '提出しました');
        return redirect(route('vn.reportDetail', ['report_id' => $report->request_report_id]))->with('is_submit', '提出しました');
    }

    /**
     * 完了報告PDFダウンロード
     */
    public function downloadReportPdf(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //パラメータ
        $reportId = $webRequest->report_id;

        //完了報告取得
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id',
            'rr.request_id',
            'room.room_no',
            'mc.name as management_company_name',
            'property.name as property_name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name as resident_name',
            \DB::raw('GROUP_CONCAT(vnm.last_name, vnm.first_name order by vnm.user_id separator ",") as user_name'),
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->join('vendor_members as vnm', function ($join) use ($vendorId) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->leftJoin('requests as r', function ($join) {
            $join->on('r.request_id', 'rr.request_id');
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.vendor_id', $vendorId)
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'room.room_no',
            'mc.name',
            'property.name',
            'rr.reception_date',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.action_section',
            'rr.construction_amount',
            'rr.description',
            'rr.comment',
            'resident.name',
            'resident.name',
        )
        ->first();

        //更新前の場合を考慮しパラメータで印字内容を上書き
        $report->action_date = $webRequest->action_date;
        $report->starting_time = $webRequest->starting_time;
        $report->ending_time = $webRequest->ending_time;
        $report->construction_amount = $webRequest->construction_amount;
        $report->description = $webRequest->description;
        $report->action_section = $webRequest->action_section;
        $report->action_status = $webRequest->action_status;

        //PDF生成
        $pdf = \PDF::loadView('vn/report/reportPdf', ['report' => $report])->setPaper('a4');
        $pdf->getDomPDF()->set_option('enable_font_subsetting', true);

        //envでstreamかdownloadかを管理
        if (Cmn::toBool(env('PDF_DOWNLOAD'))) {
            return $pdf->download('test.pdf');
        } else {
            return $pdf->stream('test.pdf');
        }
    }

    /**
     * 改行数チェック
     */
    private function checkNewLineCount($val, $itemName, $threshold = 15) {
        $cnt = substr_count($val,"\n");
        if ($cnt > $threshold) {
            throw \Illuminate\Validation\ValidationException::withMessages([
                $itemName => '改行数は'.$threshold.'個以下にしてください。'
            ]);
        }
    }
}
