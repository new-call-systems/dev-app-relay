<?php

namespace App\Http\Controllers\Vn;

use App\Library\Cmn;
use App\Library\S3Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    /**
     * 問合せ入札履歴 取得
     *
     * @return \Illuminate\Http\Response
     */
    public function getBidHistory(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //検索条件
        $dateFrom = $webRequest->date_from;
        $dateTo = $webRequest->date_to;
        $keyword = $webRequest->keyword;
        $conditions = [
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'keyword' => $keyword,
        ];

        //問合せ一覧取得
        $requests = $this->_getBidHistory($dateFrom, $dateTo, $keyword, $vendorId);

        return view('vn/request/bidHistory', ['requests' => $requests, 'conditions' => $conditions]);
    }

    /**
     * 入札可能問合せ一覧 取得
     *
     * @return \Illuminate\Http\Response
     */
    public function getBidPossible(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //検索条件
        $dateFrom = $webRequest->date_from;
        $dateTo = $webRequest->date_to;
        $keyword = $webRequest->keyword;
        $conditions = [
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'keyword' => $keyword,
        ];

        //問合せ一覧取得
        $query =
        \App\Models\Request::
        select(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            \DB::raw('GROUP_CONCAT(reqcon.context separator " ") as context'),
            'rpt.date',
            'ts.label'
        )
        ->from('requests as req')
        ->join('request_contents as reqcon', function ($join) {
            $join->on('reqcon.request_id', 'req.request_id');
        })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'req.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->leftJoin('request_preferable_times as rpt', function ($join) {
            $join->on('rpt.request_id', 'req.request_id')
                 ->where('selected', true);
        })
        ->leftJoin('time_sections as ts', function ($join) {
            $join->on('ts.time_section_id', 'rpt.time_section_id');
        })
        ->leftJoin('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
            $join->on('rvp.request_id', 'req.request_id')
                ->where('rvp.vendor_id', $vendorId);
        })
        ->where(function($query) {
            $query->where(function($query) {
               $query->where('req.visibility' ,config('const.Request.Visibility.PUBLIC'));
               $query->where('req.status', config('const.Request.Status.OPEN'));
            });
            $query->orWhere(function($query) {
                $query->whereNotNull('rvp.request_id');
                $query->where('req.visibility' ,config('const.Request.Visibility.PROTECTED'));
                $query->where('req.status', config('const.Request.Status.OPEN'));
            });
        })
        ->groupBy(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name',
            'resident.tel',
            'property.name',
            'property.address',
            'room.room_no',
            'rpt.date',
            'ts.label'
        )
        ->orderByRaw('req.created_at DESC');

        //検索条件
        if ($dateFrom) {
            $query->where('req.created_at', '>=', $dateFrom.' 00:00:00');
        }
        if ($dateTo) {
            $query->where('req.created_at', '<=', $dateTo.' 23:59:59');
        }
        if ($keyword) {
            $query->where(function ($query) use ($keyword) {
                $query->orWhere('property.name', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('reqcon.context', 'LIKE', '%'.$keyword.'%');
            });
        }
        $requests = $query->paginate(10);

        //S3インスタンス生成
        $s3 = new S3Client();

        foreach ($requests as $request) {
            //*************
            // その他情報
            //*************
            //希望時間
            if (!$request->emergency_flg) {
                $rpt =
                \App\Models\RequestPreferableTime::
                select(
                    'rpt.date',
                    'ts.label',
                    'rpt.selected',
                )
                ->from('request_preferable_times as rpt')
                ->join('time_sections as ts', function ($join) {
                    $join->on('ts.time_section_id', 'rpt.time_section_id');
                })
                ->where('rpt.request_id', $request->request_id)
                ->orderByRaw('rpt.priority ASC')
                ->get();

                $request->preferable_times = $rpt;

                foreach ($rpt as $i => $r) {
                    if ($i === 0) {
                        $request->preferable_time = $r->date.' '.$r->label;
                    }
                }
            } else {
                $request->preferable_time = '緊急';
            }

            //*************
            // ラベル化
            //*************
            //status
            $request->status = Cmn::status2Label($request->status);
            //date
            $request->date = ($request->date) ? Cmn::dateFormat($request->date).' '.$request->label : '-----';
            if ($request->emergency_flg) $request->date = '緊急';
            //マスキング
            $request->resident_name = '********';
            $request->resident_tel = '********';
            $request->property_name = '********';
            $request->property_address = '********';
            $request->room_no = '********';

            //**************
            // 画像・動画
            //**************
            //一覧取得
            $prefix = 'reports/'.$request->management_company_id.'/'.$request->resident_id.'/'.$request->request_id;
            $request->image_paths = $s3->getPresignedUrlForRequest($prefix);
        }

        return view('vn/request/bidPossible', ['requests' => $requests, 'conditions' => $conditions]);
    }

    /**
     * 問合せ作業担当者変更処理
     *
     * @return \Illuminate\Http\Response
     */
    public function changeRequestHandler(Request $webRequest)
    {
        $vendorId = $webRequest->session()->get('vendor_id');

        //検索条件
        $dateFrom = null;
        $dateTo = null;
        $keyword = null;
        $conditions = [
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'keyword' => $keyword,
        ];

        //パラメータ
        $requestId = $webRequest->request_id;
        $pVms = $webRequest->vendor_members;

        $vms = null;
        if ($pVms) {
            //存在チェック
            $vms =
            \App\Models\VendorMember::
            where('vendor_id', $vendorId)
            ->whereIn('user_id', $pVms)
            ->get();
        }
        $vms = ($vms) ? $vms : array();

        //担当者削除＆登録
        DB::transaction(function () use ($vms, $requestId, $vendorId) {
            //削除
            \App\Models\RequestHandleUser::
            where('request_id', $requestId)
            ->delete();

            //登録
            foreach ($vms as $vm) {
                $rhu = new \App\Models\RequestHandleUser();
                $rhu->request_id = $requestId;
                $rhu->user_id = $vm->user_id;
                $rhu->group_id = $vendorId;
                $rhu->save();
            }
        });

        return redirect(route('vn.bidHistory'))->with('s', '担当者を変更しました。');
    }

    /**
     * 問合せ入札履歴取得
     */
    private function _getBidHistory($dateFrom = null, $dateTo = null, $keyword = null, $vendorId = null)
    {
        $query =
        \App\Models\Request::
        select(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            //\DB::raw('GROUP_CONCAT(reqcon.context separator " ") as context'),
            'rpt.date',
            'ts.label'
        )
        ->from('requests as req')
        // ->join('request_contents as reqcon', function ($join) {
        //     $join->on('reqcon.request_id', 'req.request_id');
        // })
        ->join('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'req.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
            $join->on('rvp.request_id', 'req.request_id')
                  ->where('rvp.vendor_id', $vendorId);
        })
        ->leftJoin('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'req.request_id');
                // ->where('rhu.group_id', $vendorId);
        })
        ->leftJoin('vendors as ven', function ($join) {
            $join->on('ven.vendor_id', 'rhu.group_id');
        })
        ->leftJoin('request_preferable_times as rpt', function ($join) {
            $join->on('rpt.request_id', 'req.request_id')
                 ->where('selected', true);
        })
        ->leftJoin('time_sections as ts', function ($join) {
            $join->on('ts.time_section_id', 'rpt.time_section_id');
        })
        // ->where('rhu.group_id', $vendorId)
        ->whereIn('req.status', [
            config('const.Request.Status.BID'),
            config('const.Request.Status.SCHEDULING'),
            config('const.Request.Status.IN_PROGRESS'),
            config('const.Request.Status.COMPLETED'),
        ])
        ->orWhere(function ($q) {
            $q->where('req.status', config('const.Request.Status.CANCELLED'));
            $q->whereNotNull('rhu.request_id');
        })
        ->groupBy(
            'req.request_id',
            'req.management_company_id',
            'req.resident_id',
            'req.created_at',
            'req.status',
            'req.visibility',
            'req.ai_category',
            'req.ai_sub_category',
            'req.emergency_flg',
            'resident.name',
            'resident.tel',
            'property.name',
            'property.address',
            'room.room_no',
            'rpt.date',
            'ts.label'
        )
        ->orderByRaw('req.created_at DESC');

        //検索条件
        if ($dateFrom) {
            $query->where('req.created_at', '>=', $dateFrom.' 00:00:00');
        }
        if ($dateTo) {
            $query->where('req.created_at', '<=', $dateTo.' 23:59:59');
        }
        if ($keyword) {
            $query->where(function ($query) use ($keyword) {
                $query->orWhere('property.name', 'LIKE', '%'.$keyword.'%')
                      ->orWhere('reqcon.context', 'LIKE', '%'.$keyword.'%');
            });
        }
        $requests = $query->paginate(10);

        //S3インスタンス生成
        $s3 = new S3Client();

        foreach ($requests as $request) {
            //*************
            // その他情報
            //*************
            //問合せ内容
            $rc =
            \App\Models\RequestContent::
            select(
                \DB::raw('GROUP_CONCAT(context separator " ") as context'),
            )
            ->where('request_id', $request->request_id)
            ->first();
            $request->context = $rc ? $rc->context : '';
            //希望時間
            if (!$request->emergency_flg) {
                $rpt =
                \App\Models\RequestPreferableTime::
                select(
                    'rpt.date',
                    'ts.label',
                    'rpt.selected',
                )
                ->from('request_preferable_times as rpt')
                ->join('time_sections as ts', function ($join) {
                    $join->on('ts.time_section_id', 'rpt.time_section_id');
                })
                ->where('rpt.request_id', $request->request_id)
                ->orderByRaw('rpt.priority ASC')
                ->get();

                $request->preferable_times = $rpt;

                foreach ($rpt as $r) {
                    if ($r->selected) {
                        $request->preferable_time = $r->date.' '.$r->label;
                    }
                }
            } else {
                $request->preferable_time = '緊急';
            }
            //業者作業者一覧
            $vms =
            \App\Models\VendorMember::
            select(
                'vm.last_name',
                'vm.first_name',
                'vm.user_id',
                'vm.emergency_tel',
                \DB::raw('(rhu.request_id is not null) as admin_flg'),
            )
            ->from('vendor_members as vm')
            ->leftJoin('request_handle_users as rhu', function ($join) use ($request) {
                $join->on('rhu.user_id', 'vm.user_id')
                     ->on('rhu.group_id', 'vm.vendor_id')
                     ->where('rhu.request_id', $request->request_id);
            })
            ->whereNull('vm.retire_flg')
            ->where('vm.vendor_id', $vendorId)
            ->get();
            $request->vendor_members = $vms;

            //*************
            // ラベル化
            //*************
            //status
            $request->status = Cmn::status2Label($request->status);
            //date
            $request->date = ($request->date) ? Cmn::dateFormat($request->date).' '.$request->label : '-----';
            if ($request->emergency_flg) $request->date = '緊急';

            //**************
            // 画像・動画
            //**************
            //一覧取得
            $prefix = 'reports/'.$request->management_company_id.'/'.$request->resident_id.'/'.$request->request_id;
            $request->image_paths = $s3->getPresignedUrlForRequest($prefix);
        }

        return $requests;
    }
}
