<?php

namespace App\Http\Controllers\So;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Hidehalo\Nanoid\Client;

class ManagementCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managementCompanies =
        \App\Models\ManagementCompany::
        select('mc.management_company_id',
        'user.user_id',
        'mc.name',
        'mcm.last_name',
        'mcm.first_name',
        'user.email',
        'mcm.emergency_tel',
        'mcm.department',
        'mc.address',
        'mc.tel',
        'mc.fax',
        'mc.support_tel',
        'mc.created_at',
        'mc.updated_at as mc_updated_at',
        'mcm.updated_at as mcm_updated_at',
        'mcli.channel_id',
        'mcli.channel_secret',
        'mcli.channel_access_token',
        'mcli.liff_id',
        )
        ->from('management_companies as mc')
        ->leftJoin('management_company_members as mcm', function ($join) {
            $join->on('mcm.management_company_id', 'mc.management_company_id')
                ->where('mcm.representative_flg', true);
        })
        ->leftJoin('users as user', function ($join) {
            $join->on('user.user_id', 'mcm.user_id');
        })
        ->leftJoin('management_company_line_informations as mcli', function ($join) {
            $join->on('mcli.management_company_id', 'mc.management_company_id');
        })
        ->orderByRaw('mc.updated_at DESC')->paginate(11);

        return view('so/managementCompany/index', ['managementCompanies' => $managementCompanies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //バリデーション
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'department' => 'nullable|string|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'tel' => 'required|tel_no|max:13',
            'fax' => 'nullable|tel_no|max:13',
            'address' => 'required|string|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
            'support_tel' => 'nullable|tel_no|max:13',
            'channel_id' => 'max:255|required_with:channel_secret,channel_access_token,liff_id',
            'channel_secret' => 'max:255|required_with:channel_id,channel_access_token,liff_id',
            'channel_access_token' => 'max:255|required_with:channel_id,channel_secret,liff_id',
            'liff_id' => 'max:255|required_with:channel_id,channel_secret,channel_access_token',
        ]);

        try {
            DB::transaction(function () use ($request) {

                //パスワード発行
                $password = $this->makePassword();

                //管理会社マスタ登録
                $mc = new \App\Models\ManagementCompany;
                $mc->name = $request->name;
                $mc->address = $request->address;
                $mc->tel = $request->tel;
                $mc->fax = $request->fax;
                $mc->support_tel = $request->support_tel;
                $mc->save();

                //ユーザー登録
                $user = new \App\Models\User;
                $user->email = $request->email;
                $user->password = Hash::make($password);
                $user->save();
                //ユーザーロール登録
                $user->assignRole(config('const.Roles.MANAGEMENT_COMPANY_MANAGER'));

                //管理会社担当者登録
                $mcm = new \App\Models\ManagementCompanyMember;
                $mcm->user_id = $user->user_id;
                $mcm->management_company_id = $mc->management_company_id;
                $mcm->last_name = $request->last_name;
                $mcm->first_name = $request->first_name;
                $mcm->emergency_tel = $request->emergency_tel;
                $mcm->representative_flg = true;
                $mcm->department = $request->department;
                $mcm->save();

                //管理会社APIアクセスキーテーブル登録
                $client = new Client();
                $accessKeyId = $client->formattedId('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', 20);
                $secretKey = $client->formattedId('abcdefghijklmnopqrstuvwxyz1234567890', 40);
                $mcaak = new \App\Models\ManagementCompanyApiAccessKey;
                $mcaak->management_company_id = $mc->management_company_id;
                $mcaak->access_key_id = $accessKeyId;
                $mcaak->secret_key = $secretKey;
                $mcaak->save();

                //管理会社LINE情報
                if ($request->channel_id) {
                    $mcli = new \App\Models\ManagementCompanyLineInformation;
                    $mcli->management_company_id = $mc->management_company_id;
                    $mcli->channel_id = $request->channel_id;
                    $mcli->channel_secret = $request->channel_secret;
                    $mcli->channel_access_token = $request->channel_access_token;
                    $mcli->liff_id = $request->liff_id;
                    $mcli->save();
                }

                //登録メール送信
                Mail::to($request->email)
                    ->send(
                        new \App\Mail\CreateAccountManagementCompanyNotification(
                            $request->last_name.' '.$request->first_name,
                            $request->email,
                            $password)
                    );
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '登録が完了しました。');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //バリデーション
        $validatedData = $request->validate([
            'user_id' => 'required',
            'management_company_id' => 'required',
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'department' => 'nullable|string|max:255',
            'tel' => 'required|tel_no|max:13',
            'fax' => 'nullable|tel_no|max:13',
            'address' => 'required|string|max:255',
            'emergency_tel' => 'required|mobile_no|max:13',
            'support_tel' => 'nullable|tel_no|max:13',
            'channel_id' => 'max:255|required_with:channel_secret,channel_access_token,liff_id',
            'channel_secret' => 'max:255|required_with:channel_id,channel_access_token,liff_id',
            'channel_access_token' => 'max:255|required_with:channel_id,channel_secret,liff_id',
            'liff_id' => 'max:255|required_with:channel_id,channel_secret,channel_access_token',
        ]);


        try {
            DB::transaction(function () use ($request) {
                //変数
                $userId = $request->user_id;
                $companyId = $request->management_company_id;

                //管理会社マスタ更新
                $mc = \App\Models\ManagementCompany::find($companyId);
                $mc->name = $request->name;
                $mc->address = $request->address;
                $mc->tel = $request->tel;
                $mc->fax = $request->fax;
                $mc->support_tel = $request->support_tel;
                $mc->checkUpdatedAt($request->mc_updated_at);
                $mc->save();

                //管理会社担当者更新
                $mcm = \App\Models\ManagementCompanyMember::where('user_id', $userId)->where('management_company_id', $companyId)->first();
                $mcm->last_name = $request->last_name;
                $mcm->first_name = $request->first_name;
                $mcm->emergency_tel = $request->emergency_tel;
                $mcm->checkUpdatedAt($request->mcm_updated_at);
                $mcm->department = $request->department;
                $mcm->save();

                //管理会社LINE情報
                $mcli = \App\Models\ManagementCompanyLineInformation::find($companyId);
                if ($mcli) {
                    //登録有
                    if ($request->channel_id) {
                        $mcli->channel_id = $request->channel_id;
                        $mcli->channel_secret = $request->channel_secret;
                        $mcli->channel_access_token = $request->channel_access_token;
                        $mcli->liff_id = $request->liff_id;
                        $mcli->save();
                    } else {
                        $mcli->delete();
                    }
                } else {
                    //登録無
                    if ($request->channel_id) {
                        $mcli = new \App\Models\ManagementCompanyLineInformation;
                        $mcli->management_company_id = $companyId;
                        $mcli->channel_id = $request->channel_id;
                        $mcli->channel_secret = $request->channel_secret;
                        $mcli->channel_access_token = $request->channel_access_token;
                        $mcli->liff_id = $request->liff_id;
                        $mcli->save();
                    }
                }
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '更新が完了しました。');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * パスワード生成処理
     *
     * @param int $length
     * @return string
     */
    private function makePassword($length = 8) {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
