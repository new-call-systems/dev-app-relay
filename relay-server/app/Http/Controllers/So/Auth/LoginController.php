<?php

namespace App\Http\Controllers\So\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        return RouteServiceProvider::SO_HOME;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:guard_system_owner')->except('logout');
    }

    // Guardの認証方法を指定
    protected function guard()
    {
        return Auth::guard('guard_system_owner');
    }

    // ログイン画面
    public function showLoginForm()
    {
        return view('so.auth.login');
    }

    // ログアウト処理
    public function logout(Request $request)
    {
        Auth::guard('guard_system_owner')->logout();
        //セッション情報削除
        $request->session()->forget('user_id');
        return $this->loggedOut($request);
    }

    // ログアウトした時のリダイレクト先
    public function loggedOut(Request $request)
    {
        return redirect(route('so.login'));
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //セッション格納
        $request->session()->put('user_id', $user->user_id);

        //最終ログイン日時更新
        \App\Models\User::where('user_id', $user->user_id)
        ->update(['logined_at' => Carbon::now()]);

        $response = redirect()->route('so.dashboard');

        return $response;
    }
}
