<?php

namespace App\Http\Controllers\So;

use App\Mail\CreateAccountVendorNotification;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VendorMember;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendors =
        \App\Models\Vendor::
        select('vn.vendor_id',
        'user.user_id',
        'vn.name',
        'vnm.last_name',
        'vnm.first_name',
        'user.email',
        'vnm.emergency_tel as emergency_tel_1',
        'vn.address',
        'vn.tel',
        'vn.fax',
        'vn.emergency_tel as emergency_tel_2',
        'vn.branch_name',
        'vn.parent_address',
        'vn.parent_tel',
        'vn.parent_fax',
        'vn.created_at',
        'vn.updated_at as vn_updated_at',
        'vnm.updated_at as vnm_updated_at',)
        ->from('vendors as vn')
        ->leftJoin('vendor_members as vnm', function ($join) {
            $join->on('vnm.vendor_id', 'vn.vendor_id')
                ->where('vnm.representative_flg', true);
        })
        ->leftJoin('users as user', function ($join) {
            $join->on('user.user_id', 'vnm.user_id');
        })
        ->orderByRaw('vn.updated_at DESC')->paginate(11);

        return view('so/vendor/index', ['vendors' => $vendors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //バリデーション
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'emergency_tel_1' => 'required|mobile_no|max:13',
            'address' => 'required|string|max:255',
            'tel' => 'required|tel_no|max:13',
            'fax' => 'nullable|tel_no|max:13',
            'emergency_tel_2' => 'required|tel_no|max:13',
            'branch_name' => 'nullable|string|max:255',
            'parent_address' => 'nullable|string|max:255',
            'parent_tel' => 'nullable|tel_no|max:13',
            'parent_fax' => 'nullable|tel_no|max:13',
        ]);


        try {
            DB::transaction(function () use ($request) {

                \Log::debug('START');
                //パスワード発行
                $password = $this->makePassword();

                //業者マスタ登録
                $vendor = new Vendor;
                $vendor->name = $request->name;
                $vendor->address = $request->address;
                $vendor->tel = $request->tel;
                $vendor->fax = $request->fax;
                $vendor->emergency_tel = $request->emergency_tel_2;
                $vendor->branch_name = $request->branch_name;

                // 20200624 add parent infos;
                $vendor->parent_address = $request->parent_address;
                $vendor->parent_tel = $request->parent_tel;
                $vendor->parent_fax = $request->parent_fax;

                $vendor->save();

                //ユーザー登録
                $user = new User;
                $user->email = $request->email;
                $user->password = Hash::make($password);
                $user->save();
                //ユーザーロール登録
                $user->assignRole(['vendor_manager', 'vendor_member']);

                //業者作業者登録
                $vendorMember = new VendorMember;
                $vendorMember->user_id = $user->user_id;
                $vendorMember->vendor_id = $vendor->vendor_id;
                $vendorMember->last_name = $request->last_name;
                $vendorMember->first_name = $request->first_name;
                $vendorMember->emergency_tel = $request->emergency_tel_1;
                $vendorMember->representative_flg = true;
                $vendorMember->save();

                //登録メール送信
                Mail::to($request->email)
                    ->send(
                        new CreateAccountVendorNotification(
                            $request->last_name.' '.$request->first_name,
                            $request->email,
                            $password)
                    );
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '登録が完了しました。');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vendorId)
    {
        //バリデーション
        $validatedData = $request->validate([
            'user_id' => 'required',
            'vendor_id' => 'required|string',
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'emergency_tel_1' => 'required|mobile_no|max:13',
            'address' => 'required|string|max:255',
            'tel' => 'required|tel_no|max:13',
            'fax' => 'nullable|tel_no|max:13',
            'emergency_tel_2' => 'required|tel_no|max:13',
            'branch_name' => 'nullable|string|max:255',
            'parent_address' => 'nullable|string|max:255',
            'parent_tel' => 'nullable|tel_no|max:13',
            'parent_fax' => 'nullable|tel_no|max:13',
        ]);

        try {
            DB::transaction(function () use ($request) {
                //変数
                $userId = $request->user_id;
                $vendorId = $request->vendor_id;

                //vendors
                $vendor = Vendor::find($vendorId);
                $vendor->name = $request->name;
                $vendor->address = $request->address;
                $vendor->tel = $request->tel;
                $vendor->fax = $request->fax;
                $vendor->emergency_tel = $request->emergency_tel_2;
                $vendor->branch_name = $request->branch_name;

                // 20200624 add parent infos;
                $vendor->parent_address = $request->parent_address;
                $vendor->parent_tel = $request->parent_tel;
                $vendor->parent_fax = $request->parent_fax;

                $vendor->checkUpdatedAt($request->vn_updated_at);
                $vendor->save();

                //vendor_members
                $vendorMember = VendorMember::where('user_id', $userId)->where('vendor_id', $vendorId)->first();
                $vendorMember->last_name = $request->last_name;
                $vendorMember->first_name = $request->first_name;
                $vendorMember->emergency_tel  = $request->emergency_tel_1;
                $vendorMember->checkUpdatedAt($request->vnm_updated_at);
                $vendorMember->save();
            });

        } catch (\Exception $e) {
            \Log::error("システム例外が発生しました。".$e->getMessage());
            return redirect()->back()->withInput()->withErrors("システム例外が発生しました。".$e->getMessage());
        }

        return redirect(url()->previous())->with('my_status', '更新が完了しました。');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * パスワード生成処理
     *
     * @param int $length
     * @return string
     */
    private function makePassword($length = 8) {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
