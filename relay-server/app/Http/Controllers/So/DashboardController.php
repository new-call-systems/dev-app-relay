<?php

namespace App\Http\Controllers\So;

use App\Library\Cmn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //時間帯別問合せ数
        $selectChartByTimeZone =
        \App\Models\Request::
        select(
            \DB::raw('HOUR(created_at) as hour'),
            \DB::raw('COUNT(*) as cnt'),
        )
        ->groupBy('hour')
        ->get();

        $chartByTimeZone = array();
        for ($i = 0; $i < 24; $i++) {
            foreach ($selectChartByTimeZone as $cbtz) {
                if ($i == $cbtz->hour) {
                    array_push($chartByTimeZone, $cbtz->cnt);
                }
            }
            if (count($chartByTimeZone) === $i) {
                array_push($chartByTimeZone, 0);
            }
        }

        //曜日別問合せ数
        $selectChartByWeek =
        \App\Models\Request::
        select(
            \DB::raw('DAYOFWEEK(created_at) as week'),
            \DB::raw('COUNT(*) as cnt'),
        )
        ->groupBy('week')
        ->get();

        $chartByWeek = array();
        for ($i = 1; $i <= 7; $i++) {
            foreach ($selectChartByWeek as $cbw) {
                if ($i == $cbw->week) {
                    array_push($chartByWeek, $cbw->cnt);
                }
            }
            if (count($chartByWeek) === $i - 1) {
                array_push($chartByWeek, 0);
            }
        }

        //管理会社別問合せ数
        $chartByMc =
        \App\Models\Request::
        select(
            'mc.name',
            \DB::raw('COUNT(*) as cnt'),
        )
        ->from('requests as req')
        ->leftJoin('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'req.management_company_id');
        })
        ->groupBy('mc.name')
        ->get();

        //問合せタイプ
        $chartByRequestType =
        \App\Models\Request::
        select(
            'request_type',
            \DB::raw('COUNT(*) as cnt'),
            \DB::raw('COUNT(*) / (SELECT COUNT(*) FROM requests) * 100 as per'),
        )
        ->groupBy('request_type')
        ->orderByRaw('cnt DESC')
        ->get();

        $cbrtTotalCnt = count($chartByRequestType);
        foreach ($chartByRequestType as $cbrt) {
            $cbrt->request_type = Cmn::requestType2Label($cbrt->request_type);
        }

        //業者別出動回数
        $chartByVendorOrder =
        \App\Models\RequestHandleUser::
        select(
            \DB::raw('rhu.group_id as vendor_id'),
            'ven.name',
            \DB::raw('COUNT(*) as cnt'),
        )
        ->from('request_handle_users as rhu')
        ->join('vendors as ven', function ($join) {
            $join->on('ven.vendor_id', 'rhu.group_id');
        })
        ->groupBy([
            'rhu.group_id',
            'ven.name',
        ])
        ->get();

        return view('so.dashboard', [
            'chartByTimeZone' => $chartByTimeZone,
            'chartByWeek' => $chartByWeek,
            'chartByMc' => $chartByMc,
            'chartByRequestType' => $chartByRequestType,
            'chartByVendorOrder' => $chartByVendorOrder,
        ]);
    }
}
