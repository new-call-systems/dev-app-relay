<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    /***
     * 希望時間区分取得
     */
    public function getTimeSections()
    {
        $timeSections =
        \App\Models\TimeSection::
        select(
            'time_section_id',
            'label',
        )
        ->where('valid_flg', true)
        ->orderbyRaw('time_section_id ASC')
        ->get();

        return $timeSections;
    }
}
