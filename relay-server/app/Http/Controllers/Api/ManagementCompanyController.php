<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Library\Cmn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Hidehalo\Nanoid\Client;

class ManagementCompanyController extends Controller
{
    /**
     * 入居者利用登録（仮登録）
     */
    public function residentTempRegistration(Request $webRequest)
    {
        $mc = auth('guard_management_company')->user();
        $mcId = $mc->management_company_id;

        //パラメータ
        $email = $webRequest->email;
        $tel = $webRequest->tel;
        $residentCode = $webRequest->resident_code;

        //入居者特定
        $resident = \App\Models\Resident::
        where('management_company_id', $mcId)
        ->where('resident_code', $residentCode)
        ->where('tel', $tel)
        ->where('email', $email)
        ->first();

        if (!$resident) {
            //入居者特定できず
            \Log::info('[401 Unauthorized] - Cannot identify Resident');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        if ($resident->user_id) {
            //既に会員登録済みの場合
            \Log::info('[400 Bad Request] - Already registered as a member');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //OKの場合、認証コード発行
        $client = new Client();
        $code = $client->generateId(50, Client::MODE_DYNAMIC);

        //cacheに保存(24h)
        Cache::put($code, $resident, 86400);

        //メール送信
        Mail::to($resident->email)
        ->send(
            new \App\Mail\ResidentTemporaryRegistrationNotification(
                $resident->name,
                $code
            )
        );

        \Log::info('SUCCESS');

        return response()->json([
        ], 200);
    }
}