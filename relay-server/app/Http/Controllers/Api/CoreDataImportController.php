<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\CoreDataEnVendor;
use App\Models\CoreDataEnFacility;
use App\Models\CoreDataEnParking;
use App\Models\CoreDataEnProperty;
use App\Models\CoreDataEnProprietaryFacility;
use App\Models\CoreDataEnResident;
use App\Models\CoreDataEnRoom;
use App\Models\CoreDataEnSharedFacility;

class CoreDataImportController extends Controller
{

    /**
     * CSVファイルを処理し、データを配列に変換して返します。
     *
     * @param $file UploadedFile CSVファイルのインスタンス
     * @return array CSVファイルの内容を配列に変換した結果
     */
    private function processCsv($file)
    {
        $filePath = $file->getRealPath();
        $fileContent = file_get_contents($filePath);
        $fileContent = mb_convert_encoding($fileContent, 'UTF-8', 'Shift-JIS');
        $tempFile = tmpfile();
        fwrite($tempFile, $fileContent);
        $tempFilePath = stream_get_meta_data($tempFile)['uri'];

        return array_map('str_getcsv', file($tempFilePath));
    }

    /**
     * Out_業者.csvをcore_data_en_vendorsにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importVendors(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnVendor::create([
                'vendor_name' => $row[0],
                'tel_number' => $row[1],
                'post_number' => $row[2],
                'address' => $row[3],
                'business_hours' => $row[4],
                'emergency_tel_number1' => $row[5],
                'emergency_tel_number2' => $row[6],
                'vendor_code' => $row[7],
                'emergency_tel_number3' => $row[8],
                'vendor_kbn' => $row[9],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_駐車場マスタ.csvをcore_data_en_pakingsにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importParking(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnParking::create([
                'parking_code' => $row[0],
                'property_code' => $row[1],
                'key_number' => $row[2],
                'roof' => $row[3],
                'floor' => $row[4],
                'parking_remark' => $row[5],
                'total_length' => $row[6],
                'total_width' => $row[7],
                'total_height' => $row[8],
                'max_weight' => $row[9],
                'shape' => $row[10],
                'parking_cd' => $row[11],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_物件.csvをcore_data_en_propertiesにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importProperties(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnProperty::create([
                'property_name' => $row[0],
                'property_code' => $row[1],
                'completion_date' => Carbon::parse($row[2]),
                'management_company_code' => $row[3],
                'company_name' => $row[4],
                'sticker_management_company_code' => $row[5],
                'sticker_management_company_tel' => $row[6],
                'parking_management_company_code' => $row[7],
                'parking_management_company_tel' => $row[8],
                'landmark' => $row[9],
                'management_room_tel' => $row[10],
                'work_day' => $row[11],
                'work_time' => $row[12],
                'manager_name' => $row[13],
                'manager_tel' => $row[14],
                'note' => $row[15],
                'address' => $row[16],
                'large_bike_contract_management_company_code' => $row[17],
                'large_bike_contract_company_tel' => $row[18],
                'company_code' => $row[19],
                'property_kana' => $row[20],
                'internet_general_guidance_code' => $row[21],
                'construction_company_code' => $row[22],
                'electric_equipment_code' => $row[23],
                'water_supply_and_drainage_facility_code' => $row[24],
                'internet_general_guidance' => $row[25],
                'construction_company' => $row[26],
                'electric_equipment' => $row[27],
                'water_supply_and_drainage_facility' => $row[28],
                'nearest_police_management_company_code' => $row[29],
                'nearest_police_management_contact' => $row[30],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_専有設備.csvをcore_data_en_proprietary_facilitiesにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importProprietaryFacilities(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnProprietaryFacility::create([
                'key_code' => $row[0],
                'key_type' => $row[1],
                'warm_water_toilet_model' => $row[2],
                'ac_model' => $row[3],
                'gas_water_heater_model' => $row[4],
                'room_code' => $row[5],
                'property_code' => $row[6],
                'warm_water_toilet_model_1' => $row[7],
                'ac_model_2' => $row[8],
                'gas_water_heater_model_3' => $row[9],
                'cooking_equipment_model' => $row[10],
                'intercom_entrance_set_model' => $row[11],
                'bath_drying_unit' => $row[12],
                'bath_faucet_model' => $row[13],
                'vanity_model' => $row[14],
                'toilet_tank_model' => $row[15],
                'tv_reception_equipment' => $row[16],
                'satellite_broadcast' => $row[17],
                'free_internet' => $row[18],
                'kitchen_faucet_model' => $row[19],
                'home_delivery_manual' => $row[20],
                'pet_breeding' => $row[21],
                'cooking_equipment_model_4' => $row[22],
                'intercom_entrance_set_model_5' => $row[23],
                'bath_drying_unit_6' => $row[24],
                'bath_faucet_model_7' => $row[25],
                'vanity_model_8' => $row[26],
                'toilet_tank_model_9' => $row[27],
                'tv_reception_equipment_10' => $row[28],
                'satellite_broadcast_11' => $row[29],
                'free_internet_12' => $row[30],
                'kitchen_faucet_model_13' => $row[31],
                'home_delivery_manual_14' => $row[32],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);

        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_入居者.csvをcore_data_en_residentsにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importResidents(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnResident::create([
                'property_code' => $row[0],
                'room_code' => $row[1],
                'key_unlock_service' => $row[2],
                'contract_date' => Carbon::parse($row[3]),
                'move_in_date' => Carbon::parse($row[4]),
                'move_out_date' => Carbon::parse($row[5]),
                'vendor_code' => $row[6],
                'move_out_declaration_date' => Carbon::parse($row[7]),
                'contractor_code' => $row[8],
                'resident_name' => $row[9],
                'cohabitant' => $row[10],
                'guarantor' => $row[11],
                'contractor' => $row[12],
                'guarantor_tel' => $row[13],
                'guarantor_address' => $row[14],
                'contractor_tel' => $row[15],
                'contractor_address' => $row[16],
                'en_club_id' => $row[17],
                'resident_tel' => $row[18],
                'resident_mobile' => $row[19],
                'resident_birthday' => Carbon::parse($row[20]),
                'resident_code' => $row[21],
                'resident_gender' => $row[22],
                'vehicle_info' => $row[23],
                'parking_code' => $row[24],
                'parking_contract_date' => Carbon::parse($row[25]),
                'parking_cancellation_date' => Carbon::parse($row[26]),
                'move_out_appointment_date' => Carbon::parse($row[27]),
                'parking_contractor_name' => $row[28],
                'parking_user_name' => $row[29],
                'parking_contractor_phone' => $row[30],
                'parking_contractor_mobile' => $row[31],
                'parking_user_phone' => $row[32],
                'parking_user_mobile' => $row[33],
                'resident_name_kana' => $row[34],
                'contractor_name_kana' => $row[35],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_部屋.csvをcore_data_en_roomにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importRooms(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnRoom::create([
                'room_code' => $row[0],
                'property_code' => $row[1],
                'support_code' => $row[2],
                'special_note' => $row[3],
                'parcel_locker_password' => $row[4],
                'layout' => $row[5],
                'owner_code' => $row[6],
                'management_type' => $row[7],
                'room_management_agency_code' => $row[8],
                'ssid' => $row[9],
                'ssid2' => $row[10],
                'encryption_key' => $row[11],
                'electricity_company_code' => $row[12],
                'water_company_code' => $row[13],
                'electricity_company_name' => $row[14],
                'water_company_name' => $row[15],
                'gas_company_name' => $row[16],
                'gas_company_contact' => $row[17],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_共有設備.csvをcore_data_en_shared_facilitiesにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importSharedFacilities(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnSharedFacility::create([
                'property_code' => $row[0],
                'cleaning_code' => $row[1],
                'guard_code' => $row[2],
                'fire_check_code' => $row[3],
                'parking_code' => $row[4],
                'delivery_box_code' => $row[5],
                'auto_lock_code' => $row[6],
                'pump_code' => $row[7],
                'elevator_code' => $row[8],
                'security_camera_watching_code' => $row[9],
                'construction_code' => $row[10],
                'construction_field_code' => $row[11],
                'plumbing_system_code' => $row[12],
                'electrical_facilities_code' => $row[13],
                'electric_code' => $row[14],
                'water_line_code' => $row[15],
                'wrecker_code' => $row[16],
                'cleaning_emergency_name' => $row[17],
                'guard_emergency_name' => $row[18],
                'fire_check_emergency_name' => $row[19],
                'parking_emergency_name' => $row[20],
                'delivery_box_emergency_name' => $row[21],
                'auto_lock_emergency_name' => $row[22],
                'pump_emergency_name' => $row[23],
                'elevator_emergency_name' => $row[24],
                'security_camera_emergency_name' => $row[25],
                'construction_emergency_name' => $row[26],
                'construction_field_emergency_name' => $row[27],
                'plumbing_system_emergency_name' => $row[28],
                'electrical_facilities_emergency_name' => $row[29],
                'electric_emergency_name' => $row[30],
                'water_line_emergency_name' => $row[31],
                'wrecker_emergency_name' => $row[32],
                'cleaning_name' => $row[33],
                'guard_name' => $row[34],
                'fire_check_name' => $row[35],
                'parking_name' => $row[36],
                'delivery_box_name' => $row[37],
                'auto_lock_name' => $row[38],
                'pump_name' => $row[39],
                'elevator_name' => $row[40],
                'security_camera_name' => $row[41],
                'construction_name' => $row[42],
                'construction_field_name' => $row[43],
                'plumbing_system_name' => $row[44],
                'electrical_facilities_name' => $row[45],
                'electric_name' => $row[46],
                'water_line_name' => $row[47],
                'wrecker_name' => $row[48],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }

    /**
     * Out_設備マスタ.csvをcore_data_en_facilitiesにimport
     *
     * @param Request $request HTTPリクエストオブジェクト
     * @return \Illuminate\Http\JsonResponse インポート結果を含むJSONレスポンス
     */
    public function importFacilities(Request $request)
    {
        $file = $request->file('csv_file');
        if (!$file) {
            return response()->json(['message' => 'ファイルがアップロードされていません。'], 400);
        }

        $data = $this->processCsv($file);

        foreach ($data as $index => $row) {
            if ($index === 0) {
                continue; // Skip header row
            }

            $currentTimestamp = now();

            CoreDataEnFacility::create([
                'housing_cd' => $row[0],
                'pamphlet_name' => $row[1],
                'housing_name' => $row[2],
                'housing_name1' => $row[3],
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ]);
        }

        return response()->json(['message' => 'CSVファイルが正常にインポートされました。']);
    }
}




