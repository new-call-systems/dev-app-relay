<?php

namespace App\Http\Controllers\Api;

use App\Library\Cmn;
use App\Library\S3Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use ParagonIE\ConstantTime\Base32;
use Hidehalo\Nanoid\Client;

class VendorMemberController extends Controller
{
    // Guardの認証方法を指定
    protected function guard()
    {
        return Auth::guard('guard_vendor_member');
    }

    /**
     * ログイン
     */
    public function login(Request $request)
    {
        //パラメータ
        $email = $request->email;
        $password = $request->password;

        \Log::info('PARAM [email:'.$email.' password:'.str_repeat('*', mb_strlen($password)).']');

        //user取得
        $user = \App\Models\User::from('users as user')
        ->leftJoin('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'user.user_id');
        })
        ->where('user.email', $email)
        ->whereNull('vnm.retire_flg')
        ->first();

        //userがないか、パスワードが異なる場合401返却
        if (!$user || !Hash::check($password, $user->password)) {
            \Log::info('[401 Unauthorized] - Invalid email or password');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //パーミッションがない場合401返却
        if (!$user->hasPermissionTo('vendor_member_permission')) {
            \Log::info('[401 Unauthorized] - Logined User don\'t have Permission');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $obj = DB::transaction(function () use ($user) {
            //最終ログイン日時更新
            \App\Models\User::where('user_id', $user->user_id)
            ->update(['logined_at' => Carbon::now()]);

            //リフレッシュトークン発行・登録
            $client = new Client();
            $accessToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $refreshToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $dt = Carbon::now();
            $vnm = \App\Models\VendorMember::where(['user_id' => $user->user_id, 'vendor_id' => $user->vendor_id])->first();
            $vnm->access_token = hash('sha256', $accessToken);
            $vnm->refresh_token = hash('sha256', $refreshToken);
            $vnm->token_expire = $dt->addHour();
            $vnm->save();

            $returnObj = (Object) [
                'access_token' => $accessToken,
                'refresh_token' => $refreshToken,
                'vendor_member' => $vnm,
            ];
            return $returnObj;
        });

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $obj->access_token,
            'refresh_token' => $obj->refresh_token,
            'vendor_member' => $obj->vendor_member
        ]);
    }

    /**
     * アクセストークン再発行
     */
    public function accessTokenReissue(Request $request)
    {
        //パラメータ
        $refreshToken = hash('sha256', $request->refresh_token);

        \Log::info('PARAM [refresh_token:'.$refreshToken.']');

        //Authorizationヘッダ確認
        $authHeader = $request->header('Authorization');
        $aryHeader = preg_split('/ /', $authHeader);
        if (count($aryHeader) !== 2) {
            //Authorizationヘッダ不備
            \Log::info('[401 Unauthorized] - Invalid Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if ($aryHeader[0] !== 'Bearer') {
            //Authorizationヘッダ Bearerでない
            \Log::info('[401 Unauthorized] - Not Bearer at Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $accessToken = hash('sha256', $aryHeader[1]);

        //vendor_member取得
        $vnm = \App\Models\VendorMember::
        where('access_token', $accessToken)
        ->where('refresh_token', $refreshToken)
        ->whereNull('retire_flg')
        ->first();

        //vendor_memberがない
        if (!$vnm) {
            \Log::info('[401 Unauthorized] - Invalid token or refresh token');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $obj = DB::transaction(function () use ($vnm) {
            //アクセストークン・リフレッシュトークン発行・登録
            $client = new Client();
            $accessToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $refreshToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $dt = Carbon::now();
            $vnm->access_token = hash('sha256', $accessToken);
            $vnm->refresh_token = hash('sha256', $refreshToken);
            $vnm->token_expire = $dt->addHour();
            $vnm->save();

            $returnObj = (Object) [
                'access_token' => $accessToken,
                'refresh_token' => $refreshToken,
            ];
            return $returnObj;
        });

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $obj->access_token,
            'refresh_token' => $obj->refresh_token,
        ]);
    }

    /**
     * 問合せ一覧取得
     */
    public function getRequestList(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $status = ($webRequest->status === null) ? array() : preg_split('/\,/', $webRequest->status);
        $emergencyFlg = $webRequest->emergency_flg;
        $date = $webRequest->date;

        $query = \App\Models\Request::
                    select(
                        'r.request_id',
                        'r.status',
                        'r.emergency_flg',
                        'r.ai_category',
                        'r.ai_sub_category',
                        'r.created_at',
                    )
                    ->from('requests as r')
                    ->leftJoin('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
                        $join->on('rvp.request_id', 'r.request_id')
                            ->where('rvp.vendor_id', $vendorId);
                    })
                    ->leftJoin('request_preferable_times as rpt', function ($join) {
                        $join->on('rpt.request_id', 'r.request_id');
                    })
                    ->leftJoin('request_handle_users as rhu', function ($join) use ($vendorId) {
                        $join->on('rhu.request_id', 'r.request_id')
                            ->where('rhu.group_id', $vendorId);
                    })
                    ->where(function($query) {
                        $query->where(function($query) {
                           $query->where('r.visibility' ,config('const.Request.Visibility.PUBLIC'));
                           $query->where('r.status', config('const.Request.Status.OPEN'));
                        });
                        $query->orWhereNotNull('rvp.request_id');
                    })
                    ->groupBy(
                        'r.request_id',
                        'r.status',
                        'r.emergency_flg',
                        'r.ai_category',
                        'r.ai_sub_category',
                        'r.created_at',
                    )
                    ->orderByRaw('r.created_at DESC');

        //ステータスフィルタ
        if (count($status) > 0) {
            if (in_array(config('const.Request.Status.CANCELLED'), $status, true)) {
                $cancelKeys = array_keys($status, config('const.Request.Status.CANCELLED'), true);
                foreach ($cancelKeys as $key) {
                    unset($status[$key]);
                }
                $query->where(function($query) use ($status, $userId) {
                    $query
                    ->where('r.status', config('const.Request.Status.CANCELLED'))
                    ->where('rhu.user_id', $userId);
                    if (count($status) > 0) {
                        $query->orWhereIn('r.status', $status);
                    }
                });
            } else {
                $query->whereIn('r.status', $status);
            }
        }

        //緊急フラグフィルタ
        if ($emergencyFlg === 'only') {
            $query->where('r.emergency_flg', true);
        } else if ($emergencyFlg === 'exclude') {
            $query->where('r.emergency_flg', false);
        }

        //希望日フィルタ
        if ($date) {
            $query->where('rpt.date', $date);
        }

        //実行
        $requests = $query->paginate(10);

        foreach ($requests as $req) {
            //問合せ内容
            $requestContents =
            \App\Models\RequestContent::
            select(
                'request_id',
                'context',
            )
            ->where('request_id', $req->request_id)
            ->orderByRaw('created_at ASC')
            ->get();
            foreach ($requestContents as $requestContent) {
                $req->context .= $requestContent->context;
            }
            //希望時間
            $preferableTimes =
            \App\Models\RequestPreferableTime::
            select(
                'request_preferable_time_id',
                'date',
                'time_section_id',
                'selected',
            )
            ->where('request_id', $req->request_id)
            ->orderByRaw('priority ASC')
            ->get();
            $req->preferable_info = $preferableTimes;
            //担当者
            $requestHandleUsers =
            \App\Models\RequestHandleUser::
            select(
                \DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
            )
            ->from('request_handle_users as rhu')
            ->leftJoin('vendor_members as vnm', function ($join) {
                $join->on('vnm.user_id', 'rhu.user_id')
                    ->on('vnm.vendor_id', 'rhu.group_id');
            })
            ->where('rhu.request_id', $req->request_id)
            ->where('rhu.group_id', $vendorId)
            ->get();
            $rhuAry = array();
            foreach ($requestHandleUsers as $rhu) {
                array_push($rhuAry, $rhu->user_name);
            }
            $req->user_name = $rhuAry;
        }

        return $requests;
    }

    /**
     * 問合せ情報取得
     */
    public function getRequestInfo(Request $webRequest)
    {
        $requestId = $webRequest->request_id;
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //問合せ情報＋入居者情報
        $request =
        \App\Models\Request::
        select(
            'r.request_id',
            'r.management_company_id',
            'r.resident_id',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            'r.status',
            'r.emergency_flg',
            'r.ai_category',
            'r.ai_sub_category',
            'rc.context',
            'rc.response',
            'r.created_at',
            'r.updated_at',
        )
        ->from('requests as r')
        ->leftJoin('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
            $join->on('rvp.request_id', 'r.request_id')
                ->where('rvp.vendor_id', $vendorId);
        })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'r.request_id');
        })
        ->leftJoin('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'r.resident_id');
        })
        ->leftJoin('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->leftJoin('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->where('r.request_id', $requestId)
        ->where(function($query) {
            $query->where(function($query) {
                $query->where('r.visibility' ,config('const.Request.Visibility.PUBLIC'));
                $query->where('r.status', config('const.Request.Status.OPEN'));
            });
            $query->orWhereNotNull('rvp.request_id');
        })
        ->first();

        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //希望時間
        $preferableTimes =
        \App\Models\RequestPreferableTime::
        select(
            'request_preferable_time_id',
            'date',
            'time_section_id',
            'selected',
        )
        ->where('request_id', $requestId)
        ->orderByRaw('priority ASC')
        ->get();

        $request->preferable_info = $preferableTimes;

        //キャンセル理由
        $cancelReason = null;
        $requestCancelReason = \App\Models\RequestCancelReason::find($requestId);
        if ($requestCancelReason) {
            $cancelReason = $requestCancelReason->content;
        }
        $request->cancel_reason = $cancelReason;

        //マスキング確認＆担当者
        $requestHandleUsers =
        \App\Models\RequestHandleUser::
        select(
            'rhu.user_id',
            \DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
        )
        ->from('request_handle_users as rhu')
        ->leftJoin('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'rhu.user_id')
                ->on('vnm.vendor_id', 'rhu.group_id');
        })
        ->where('rhu.request_id', $requestId)
        ->where('rhu.group_id', $vendorId)
        ->get();

        $rhuAry = array();
        $ids = array();
        foreach ($requestHandleUsers as $rhu) {
            array_push($rhuAry, $rhu->user_name);
            array_push($ids, $rhu->user_id);
        }
        $request->user_name = $rhuAry;

        if (in_array($userId, $ids, true)) {
            //担当者
            $request->is_admin = true;
        } else {
            //担当者じゃない
            $request->is_admin = false;
            $request->resident_name = '********';
            $request->resident_tel = '********';
            $request->property_name = '********';
            $request->property_address = '********';
            $request->room_no = '********';
        }

        //**************
        // 画像・動画
        //**************
        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$request->management_company_id.'/'.$request->resident_id.'/'.$request->request_id;
        $request->images = $s3->getPresignedUrlForRequest($prefix);

        return $request;
    }

    /**
     * 問合せ入札処理
     */
    public function bidRequest(Request $webRequest)
    {
        $requestId = $webRequest->request_id;
        $requestPreferableTimeId = $webRequest->request_preferable_time_id;
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        \Log::info('PARAM [request_id:'.$requestId.' request_preferable_time_id:'.$requestPreferableTimeId.']');

        //権限チェック
        $request = \App\Models\Request::
                    selectRaw('r.*')
                    ->from('requests as r')
                    ->leftJoin('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
                        $join->on('rvp.request_id', 'r.request_id')
                            ->where('rvp.vendor_id', $vendorId);
                    })
                    ->where('r.request_id', $requestId)
                    ->where('r.status', config('const.Request.Status.OPEN'))
                    ->where(function($query) {
                        $query->where('r.visibility' ,config('const.Request.Visibility.PUBLIC'));
                        $query->orWhere(function($query) {
                            $query->where('r.visibility', config('const.Request.Visibility.PROTECTED'));
                            $query->whereNotNull('rvp.request_id');
                        });
                    })
                    ->first();

        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //緊急チェック
        if (!$request->emergency_flg) {
            $rpt = \App\Models\RequestPreferableTime::find($requestPreferableTimeId);
            if (!$rpt || $rpt->request_id != $requestId) {
                \Log::info('[400 Bad Request] - request_preferable_time_id does not exist.');
                return response()->json(['error' => 'Bad Request'], 400);
            }
        }

        DB::transaction(function () use ($request, $userId, $vendorId, $requestPreferableTimeId) {
            //問合せテーブル
            if ($request->emergency_flg) {
                $request->status = config('const.Request.Status.SCHEDULING');
            } else {
                $request->status = config('const.Request.Status.BID');
            }
            $request->bid_date = Carbon::now()->format('Y-m-d');
            $request->save();

            //問合せ担当者テーブル
            $requestHandleUser = new \App\Models\RequestHandleUser;
            $requestHandleUser->request_id = $request->request_id;
            $requestHandleUser->user_id = $userId;
            $requestHandleUser->group_id = $vendorId;
            $requestHandleUser->save();

            //業者用問合せアクセス権限テーブル
            $delRequestVendorPermissions = \App\Models\RequestVendorPermission::where('request_id', $request->request_id)->get();
            foreach ($delRequestVendorPermissions as $delRequestVendorPermission) {
                $delRequestVendorPermission->delete();
            }
            $requestVendorPermission = new \App\Models\RequestVendorPermission;
            $requestVendorPermission->request_id = $request->request_id;
            $requestVendorPermission->vendor_id = $vendorId;
            $requestVendorPermission->save();

            //問合せ業者対応テーブル
            $requestHandle = new \App\Models\RequestHandle;
            $requestHandle->request_id = $request->request_id;
            $requestHandle->vendor_id = $vendorId;
            $requestHandle->save();

            //問合せ希望時間テーブル
            if (!$request->emergency_flg) {
                $rpt = \App\Models\RequestPreferableTime::find($requestPreferableTimeId);
                $rpt->selected = true;
                $rpt->save();
            }
        });

        //入居者へ入札通知
        $residentId = $request->resident_id;
        $resident = \App\Models\Resident::find($residentId);
        $msg = "お客様の問合せに対応する業者が決定いたしました。\n詳細はアプリの「履歴」タブよりご参照ください。";
        $resident->sendMessage($msg);

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 問合せキャンセル
     */
    public function requestCancel(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        $requestId = $webRequest->request_id;
        \Log::info('PARAM [request_id:'.$requestId.']');

        //問合せ取得
        $request =
        \App\Models\Request::from('requests as r')
        ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->where('r.request_id', $requestId)
        ->first();

        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        if ($request->status !== config('const.Request.Status.WAITING_FOR_CANCEL')) {
            //キャンセルできないステータス
            \Log::info('[400 Bad Request] - Request status don\'t cancel');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($request) {
            $request->status = config('const.Request.Status.CANCELLED');
            $request->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 業者対応報告
     */
    public function changeRequestStatus(Request $webRequest)
    {
        $enableStatuses = [config('const.Request.Status.SCHEDULING'), config('const.Request.Status.IN_PROGRESS'), config('const.Request.Status.COMPLETED')];
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $status = $webRequest->status;
        \Log::info('PARAM [request_id:'.$requestId.' status:'.$status.']');

        //変更後ステータス確認
        if (!in_array($status, $enableStatuses)) {
            \Log::info('[400 Bad Request] - An unchangeable status was specified');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //問合せ取得
        $request =
        \App\Models\Request::from('requests as r')
        ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->where('r.request_id', $requestId)
        ->whereIn('r.status', $enableStatuses)
        ->first();

        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($request, $status) {
            $request->status = $status;
            $request->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 問合せ業者対応一覧取得
     */
    public function getRequestHandlers(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        $requestId = $webRequest->request_id;
        \Log::info('PARAM [request_id:'.$requestId.']');

        //取得
        $requestHandles =
        \App\Models\RequestHandle::
        select(
            'rh.request_handle_id',
            'rh.request_id',
            'rh.scheduled_time',
            'rh.worked_minute',
            'rh.description',
        )
        ->from('request_handles as rh')
        ->join('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
            $join->on('rvp.request_id', 'rh.request_id')
                ->where('rvp.vendor_id', $vendorId);
        })
        ->where('rh.request_id', $requestId)
        ->where('rh.vendor_id', $vendorId)
        ->paginate(10);

        return $requestHandles;
    }

    /**
     * 問合せ業者対応更新
     */
    public function updateRequestHandler(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $requestHandleId = $webRequest->request_handle_id;
        $scheduledTime = $webRequest->scheduled_time;
        $description = $webRequest->description;
        \Log::info('PARAM [request_id:'.$requestId.' request_handle_id:'.$requestHandleId.' scheduled_time:'.$scheduledTime.' description:'.$description.']');

        //sheculed_timeフォーマット確認
        if ($scheduledTime) {
            if (!Cmn::validateDate($scheduledTime)) {
                \Log::info('[400 Bad Request] - Wrong scheduled_time');
                return response()->json(['error' => 'Bad Request'], 400);
            }
        }

        //取得
        $requestHandle =
        \App\Models\RequestHandle::
        from('request_handles as rh')
        ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'rh.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->where('rh.request_handle_id', $requestHandleId)
        ->where('rh.request_id', $requestId)
        ->where('rh.vendor_id', $vendorId)
        ->first();

        if (!$requestHandle) {
            //問合せ業者対応情報なし
            \Log::info('[400 Bad Request] - RequestHandle not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($requestHandle, $scheduledTime, $description) {
            $requestHandle->scheduled_time = $scheduledTime;
            $requestHandle->description = $description;
            $requestHandle->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 業者作業員情報更新
     */
    public function updateVendorMember(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        try {
            //バリデーション
            $validatedData = $webRequest->validate([
                'last_name' => 'required|string|max:255',
                'first_name' => 'required|string|max:255',
                'emergency_tel' => 'required|mobile_no|max:13',
            ]);

            $lastName = $webRequest->last_name;
            $firstName = $webRequest->first_name;
            $emergencyTel = $webRequest->emergency_tel;

            //更新対象チェック
            if ($userId != $webRequest->user_id || $vendorId != $webRequest->vendor_id) {
                \Log::info('[400 Bad Request] - Another vendor member was specified.');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //更新
            $vendorMember->last_name = $lastName;
            $vendorMember->first_name = $firstName;
            $vendorMember->emergency_tel = $emergencyTel;
            $vendorMember->save();

        } catch (\Illuminate\Validation\ValidationException $e) {
            //バリデーションエラー
            $errors = array();
            foreach ($e->errors() as $error) {
                array_push($errors, $error[0]);
            }
            return response()->json(['errors' => $errors], 400);
        }

        return response()->json([
        ], 201);
    }

    /**
     * 完了報告書作成
     */
    public function createReport(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;

        //問合せ情報
        $request =
        \App\Models\Request::
        select(
            'r.resident_id',
            'r.status',
            'r.management_company_id',
            'r.updated_at',
            'r.bid_date',
        )
        ->from('requests as r')
        ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->where('r.request_id', $requestId)
        ->first();

        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //ステータスチェック
        if ($request->status !== config('const.Request.Status.COMPLETED') && $request->status !== config('const.Request.Status.IN_PROGRESS')) {
            \Log::info('[400 Bad Request] - Request status is not \'completed\'. [status:'.$request->status.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        $residentId = $request->resident_id;
        $mcId = $request->management_company_id;
        $actionDate = $request->updated_at;
        $receptionDate = $request->bid_date;

        //完了報告書作成処理
        $reportId =
        DB::transaction(function () use ($requestId, $userId, $vendorId, $residentId, $mcId, $actionDate, $receptionDate) {
            $requestReport = new \App\Models\RequestReport;
            $requestReport->request_id = $requestId;
            $requestReport->author_id = $userId;
            $requestReport->vendor_id = $vendorId;
            $requestReport->management_company_id = $mcId;
            $requestReport->resident_id = $residentId;
            $requestReport->reception_date = $receptionDate;
            $requestReport->report_status = config('const.Reports.Status.WRITING');
            $requestReport->action_status = config('const.Reports.ActionStatus.COMPLETED');
            $requestReport->description = '';
            $requestReport->action_date = $actionDate;
            $requestReport->action_section = '1';
            $requestReport->save();

            return $requestReport->request_report_id;
        });

        \Log::info('SUCCESS');

        return response()->json([
            'report_id' => $reportId,
        ]);
    }

    /**
     * 完了報告書取得
     */
    public function getReport(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $reportId = $webRequest->report_id;

        //完了報告書情報
        $report =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id as report_id',
            'rr.request_id',
            'mc.name as management_company_name',
            'property.name as property_name',
            'room.room_no',
            'rd.name as resident_name',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.description',
            //\DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
            'rr.reception_date',
            'rr.action_section',
            'rr.construction_amount',
        )
        ->from('request_reports as rr')
        ->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as rd', function ($join) {
            $join->on('rd.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'rd.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        // ->join('vendor_members as vnm', function ($join) {
        //     $join->on('vnm.user_id', 'rhu.user_id');
        // })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.request_id', $requestId)
        ->first();

        if (!$report) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //担当者
        $rhus =
        \App\Models\RequestHandleUser::
        select(
            \DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
        )
        ->from('request_handle_users as rhu')
        ->join('vendor_members as vnm', function ($join) {
            $join->on('vnm.user_id', 'rhu.user_id');
        })
        ->where('rhu.request_id', $requestId)
        ->where('rhu.group_id', $vendorId)
        ->get();
        $rhuAry = array();
        foreach ($rhus as $rhu) {
            array_push($rhuAry, $rhu->user_name);
        }
        $report->user_name = $rhuAry;

        \Log::info('SUCCESS');

        return response()->json($report);
    }

    /**
     * 完了報告書更新
     */
    public function updateReport(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $reportId = $webRequest->report_id;

        try {
            //バリデーション
            $validatedData = $webRequest->validate([
                'description' => 'required|string',
                'action_date' => 'required|date',
                'starting_time' => 'nullable|date_format:H:i',
                'ending_time' => 'nullable|date_format:H:i',
                'action_status' => 'required',
                'action_section' => 'required|max:1',
                'construction_amount' => 'nullable|integer',
            ]);

            //パラメータ
            $description = $webRequest->description;
            $actionDate = $webRequest->action_date;
            $startingTime = $webRequest->starting_time;
            $endingTime = $webRequest->ending_time;
            $actionStatus = $webRequest->action_status;
            $actionSection = $webRequest->action_section;
            $constructionAmount = $webRequest->construction_amount;

            //更新対象
            $report =
            \App\Models\RequestReport::
            from('request_reports as rr')
            ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
                $join->on('rhu.request_id', 'rr.request_id')
                    ->where('rhu.user_id', $userId)
                    ->where('rhu.group_id', $vendorId);
            })
            ->where('rr.request_report_id', $reportId)
            ->where('rr.request_id', $requestId)
            ->first();

            if (!$report) {
                \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //ステータスチェック
            if ($report->report_status !== config('const.Reports.Status.WRITING')) {
                \Log::info('[400 Bad Request] - Report status is not \'writing\'. [status:'.$report->report_status.']');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //更新
            $report->description = $description;
            $report->action_date = $actionDate;
            $report->starting_time = $startingTime;
            $report->ending_time = $endingTime;
            $report->action_status = $actionStatus;
            $report->action_section = $actionSection;
            $report->construction_amount = $constructionAmount;
            $report->save();

        } catch (\Illuminate\Validation\ValidationException $e) {
            //バリデーションエラー
            $errors = array();
            foreach ($e->errors() as $error) {
                array_push($errors, $error[0]);
            }
            return response()->json(['errors' => $errors], 400);
        }

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 完了報告書ステータス変更
     */
    public function changeReportStatus(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $reportId = $webRequest->report_id;

        //更新対象
        $report =
        \App\Models\RequestReport::
        from('request_reports as rr')
        ->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.request_id', $requestId)
        ->first();

        if (!$report) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //ステータスチェック
        if ($report->report_status !== config('const.Reports.Status.WRITING')) {
            \Log::info('[400 Bad Request] - Report status is not \'writing\'. [status:'.$report->report_status.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新
        $report->report_status = config('const.Reports.Status.DRAFT');
        $report->save();

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 完了報告書一覧取得
     */
    public function getReportList(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $includeGroupReports = Cmn::toBool($webRequest->include_group_reports);
        $query = $webRequest->get('query');
        $status = preg_split('/\,/', $webRequest->status);

        //完了報告書情報
        $q =
        \App\Models\RequestReport::
        select(
            'rr.request_report_id as report_id',
            'rr.request_id',
            'mc.name as management_company_name',
            'property.name as property_name',
            'room.room_no',
            'rd.name as resident_name',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.description',
            //\DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
            'rr.reception_date',
            'rr.action_section',
            'rr.construction_amount',
        )
        ->from('request_reports as rr')
        ->groupBy(
            'rr.request_report_id',
            'rr.request_id',
            'mc.name',
            'property.name',
            'room.room_no',
            'rd.name',
            'rr.report_status',
            'rr.action_status',
            'rr.action_date',
            'rr.starting_time',
            'rr.ending_time',
            'rr.description',
            'rr.reception_date',
            'rr.action_section',
            'rr.construction_amount',
        );

        //所属業者の全完了報告書を表示するか
        $q->join('request_handle_users as rhu', function ($join) use ($userId, $vendorId, $includeGroupReports) {
            $join->on('rhu.request_id', 'rr.request_id')
                    ->where('rhu.group_id', $vendorId);
            if (!$includeGroupReports) {
                $join->where('rhu.user_id', $userId);
            }
        });

        //結合
        $q->join('management_companies as mc', function ($join) {
            $join->on('mc.management_company_id', 'rr.management_company_id');
        })
        ->join('residents as rd', function ($join) {
            $join->on('rd.resident_id', 'rr.resident_id');
        })
        ->join('rooms as room', function ($join) {
            $join->on('room.room_id', 'rd.room_id');
        })
        ->join('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        // ->join('vendor_members as vnm', function ($join) {
        //     $join->on('vnm.user_id', 'rhu.user_id');
        // })
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'rr.request_id');
        })
        ->orderByRaw('rr.created_at DESC');

        //ステータスフィルタ
        if (count($status) > 0 && $status[0] !== '') {
            $q->whereIn('rr.report_status', $status);
        }

        //クエリフィルタ
        if ($query) {
            $q->where(function ($q) use ($query) {
                $q->orWhere('property.name', 'LIKE', '%'.$query.'%')
                    ->orWhere('rr.description', 'LIKE', '%'.$query.'%')
                    ->orWhere('rc.context', 'LIKE', '%'.$query.'%');
            });
        }

        //取得
        $reports = $q->paginate(10);

        foreach ($reports as $report) {
            //S3情報
            $s3Config = new \stdClass();
            $s3Config->bucket_name = env('AWS_BUCKET');
            $s3Config->region = env('AWS_DEFAULT_REGION');
            $s3Config->identity_pool_id = env('AWS_IDENTITY_POOL_ID');
            $s3Config->prefix = 'reports/'.$vendorId.'/'.$report->request_id.'/'.$report->report_id;
            $report->s3_config = $s3Config;

            //担当者
            $rhus =
            \App\Models\RequestHandleUser::
            select(
                \DB::raw('CONCAT(vnm.last_name, vnm.first_name) as user_name'),
            )
            ->from('request_handle_users as rhu')
            ->join('vendor_members as vnm', function ($join) {
                $join->on('vnm.user_id', 'rhu.user_id');
            })
            ->where('rhu.request_id', $report->request_id)
            ->where('rhu.group_id', $vendorId)
            ->get();
            $rhuAry = array();
            foreach ($rhus as $rhu) {
                array_push($rhuAry, $rhu->user_name);
            }
            $report->user_name = $rhuAry;
        }

        \Log::info('SUCCESS');

        return response()->json($reports);
    }

    /**
     * AWS Config取得
     */
    public function getAwsConfig(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $reportId = $webRequest->report_id;

        //完了報告書情報
        $report =
        \App\Models\RequestReport::
        from('request_reports as rr')
        ->join('request_handle_users as rhu', function ($join) use ($vendorId) {
            $join->on('rhu.request_id', 'rr.request_id')
                 ->where('rhu.group_id', $vendorId);
        })
        ->where('rr.request_report_id', $reportId)
        ->where('rr.request_id', $requestId)
        ->first();

        if (!$report) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //S3情報
        $bucketName = env('AWS_BUCKET');
        $region = env('AWS_DEFAULT_REGION');
        $identityPoolId = env('AWS_IDENTITY_POOL_ID');
        $prefix = 'reports/'.$vendorId.'/'.$requestId.'/'.$reportId;

        \Log::info('SUCCESS');

        return response()->json([
            'bucket_name' => $bucketName,
            'region' => $region,
            'identity_pool_id' => $identityPoolId,
            'prefix' => $prefix,
        ]);
    }

    /**
     * バッジ表示用情報取得
     */
    public function getBadgeInfo(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;
        $badgeGotAt = $vendorMember->badge_got_at;

        //問合せ状況取得
        $count =
        \App\Models\Request::
        select(
            \DB::raw('COUNT(status = "'.config('const.Request.Status.OPEN').'" or null) as open_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.SCHEDULING').'" or null) as scheduling_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.IN_PROGRESS').'" or null) as in_progress_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.COMPLETED').'" or null) as completed_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.WAITING_FOR_CANCEL').'" or null) as waiting_for_cancel_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.CANCELLED').'" or null) as cancelled_count'),
        )
        ->from('requests as r')
        ->leftJoin('request_handle_users as rhu', function ($join) use ($userId, $vendorId) {
            $join->on('rhu.request_id', 'r.request_id')
                ->where('rhu.user_id', $userId)
                ->where('rhu.group_id', $vendorId);
        })
        ->leftJoin('request_vendor_permissions as rvp', function ($join) use ($vendorId) {
            $join->on('rvp.request_id', 'r.request_id')
                ->where('rvp.vendor_id', $vendorId);
        })
        ->where(function($query) {
            $query->where(function($query) {
                $query->where('r.visibility' ,config('const.Request.Visibility.PUBLIC'));
                $query->where('r.status', config('const.Request.Status.OPEN'));
            });
            $query->orWhere(function($query) {
                $query->where('r.visibility' ,config('const.Request.Visibility.PROTECTED'));
                $query->where('r.status', config('const.Request.Status.OPEN'));
                $query->whereNotNull('rvp.request_id');
            });
            $query->orWhereNotNull('rhu.request_id');
        })
        ->where('r.request_type', config('const.Request.RequestType.VENDOR'))
        ->where('r.updated_at', '>=', $badgeGotAt)
        ->first();

        //更新処理
        DB::transaction(function () use ($vendorMember) {
            $vendorMember->badge_got_at = Carbon::now();
            $vendorMember->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
            'open' => $count->open_count,
            'scheduling' => $count->scheduling_count,
            'in_progress' => $count->in_progress_count,
            'completed' => $count->completed_count,
            'waiting_for_cancel' => $count->waiting_for_cancel_count,
            'cancelled' => $count->cancelled_count,
        ]);
    }

    /**
     * FirebaseデバイスID登録
     */
    public function updateFirebaseDeviceId(Request $webRequest)
    {
        $vendorMember = auth('guard_vendor_member')->user();
        $userId = $vendorMember->user_id;
        $vendorId = $vendorMember->vendor_id;

        //パラメータ
        $deviceId = $webRequest->device_id;

        //更新対象チェック
        if ($userId != $webRequest->user_id || $vendorId != $webRequest->vendor_id) {
            \Log::info('[400 Bad Request] - Another vendor member was specified.');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新
        $vendorMember->firebase_device_id = $deviceId;
        $vendorMember->save();

        return response()->json([
        ], 201);
    }
}
