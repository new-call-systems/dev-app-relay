<?php

namespace App\Http\Controllers\Api;

use App\Library\Cmn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Hidehalo\Nanoid\Client;

class LineController extends Controller
{
    /**
     * LIFF ID取得処理
     * ※認証なし
     */
    public function getLiffId(Request $webRequest) {
        //パラメータ
        $clientId = $webRequest->line_client_id;

        //LINE情報取得
        $mcli = \App\Models\ManagementCompanyLineInformation::where('channel_id', $clientId)->first();
        if (!$mcli) {
            \Log::info('[400 Bad Request] - Channel Id not found.');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        \Log::info('SUCCESS');

        return response()->json([
            'liff_id' => $mcli->liff_id,
        ]);
    }

    /**
     * ログイン処理（LINE）
     * ※認証なし
     */
    public function login(Request $webRequest) {
        //パラメータ
        $lineAccessToken = $webRequest->line_access_token;
        $lineClientId = $webRequest->line_client_id;

        \Log::info('PARAM [line_access_token:'.$lineAccessToken.' line_client_id:'.$lineClientId.']');

        //アクセストークン検証
        $response = Http::get('https://api.line.me/oauth2/v2.1/verify', [
            'access_token' => $lineAccessToken,
        ]);
        if (!$response->ok()) {
            \Log::info('[401 Unauthorized] - Access Token verification failure. LINE SERVER Returned Error Status ['.$response->status().'] responsebody:['.$response->body().']');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $resVal = Cmn::array2Object($response->json());

        //チャンネルID検証
        if ($resVal->client_id !== $lineClientId) {
            \Log::info('[401 Unauthorized] - LINE Client ID verification failure. client_id:['.$resVal->client_id.'] param_client_id:['.$lineClientId.']');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //プロフィール取得
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.$lineAccessToken,
        ])->get('https://api.line.me/v2/profile');
        if (!$response->ok()) {
            \Log::info('[401 Unauthorized] - Failed to get profile. LINE SERVER Returned Error Status ['.$response->status().'] responsebody:['.$response->body().']');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $resVal = Cmn::array2Object($response->json());
        $lineUserId = $resVal->userId;

        //入居者特定
        $resident = \App\Models\Resident::where('line_user_id', $lineUserId)->first();
        if (!$resident) {
            \Log::info('[401 Unauthorized] - Resident not found. Line User ID ['.$lineUserId.']');
            return response()->json(['errors' => ['Not linked with LINE']], 401);
        }

        //入居物件の管理会社検証
        $mcli = \App\Models\ManagementCompanyLineInformation::find($resident->management_company_id);
        if ($mcli->channel_id !== $lineClientId) {
            \Log::info('[401 Unauthorized] - LINE Client ID is the ID of another management company.');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //アクセストークン・リフレッシュトークン、発行
        $client = new Client();
        $accessToken = $client->generateId(20, Client::MODE_DYNAMIC);
        $refreshToken = $client->generateId(20, Client::MODE_DYNAMIC);
        $dt = Carbon::now();
        $resident->access_token = hash('sha256', $accessToken);
        $resident->refresh_token = hash('sha256', $refreshToken);
        $resident->token_expire = $dt->addHour();
        $resident->save();

        //入居者Model整形
        $resident->line_connected = !empty($resident->line_user_id);
        unset($resident->extra, $resident->line_user_id);

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $accessToken,
            'refresh_token' => $refreshToken,
            'resident' => $resident
        ]);
    }

    /**
     * LINEアカウント連携用URL取得
     */
    public function getUrlLineAccountLinkage(Request $webRequest) {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $mcId = $resident->management_company_id;

        //管理会社LINE情報
        $mcli = \App\Models\ManagementCompanyLineInformation::find($mcId);
        if (!$mcli) {
            \Log::info('[400 Bad Request] - management company cannot use LINE. [management_company_id:'.$mcId.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //連携用URL生成
        $url = 'https://access.line.me/oauth2/v2.1/authorize?response_type=code&scope=profile&prompt=consent&bot_prompt=aggressive';
        //チャンネルID
        $url .= '&client_id=';
        $url .= $mcli->channel_id;
        //リダイレクトURI
        $url .= '&redirect_uri=';
        $url .= urlencode(env('APP_URL').'/resident/line-cb');
        //ステート
        $state = $this->makeState();
        $url .= '&state=';
        $url .= $state;

        //stateをcacheに保存
        Cache::put($residentId, $state, 60);

        \Log::info('SUCCESS');

        return response()->json([
            'url' => $url,
        ]);
    }

    /**
     * LINEアカウント連携 認証処理
     */
    public function lineAccountLinkageAuthentication(Request $webRequest) {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $mcId = $resident->management_company_id;

        //管理会社LINE情報
        $mcli = \App\Models\ManagementCompanyLineInformation::find($mcId);
        if (!$mcli) {
            \Log::info('[400 Bad Request] - management company cannot use LINE. [management_company_id:'.$mcId.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //パラメータ
        $code = $webRequest->code;
        $state = $webRequest->state;
        \Log::info('PARAM [code:'.$code.' state:'.$state.']');

        //state検証
        $cacheState = Cache::pull($residentId);
        if ($cacheState !== $state) {
            \Log::info('[400 Bad Request] - state does not match.[cacheState:'.$cacheState.' paramState:'.$state.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //LINEアクセストークン取得
        $response = Http::asForm()->post('https://api.line.me/oauth2/v2.1/token', [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => env('APP_URL').'/resident/line-cb',
            'client_id' => $mcli->channel_id,
            'client_secret' => $mcli->channel_secret,
        ]);
        if (!$response->ok()) {
            \Log::info('[400 Bad Request] - Failed to get access token. LINE SERVER Returned Error Status ['.$response->status().'] responsebody:['.$response->body().'] resident_id:['.$residentId.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }
        $resVal = Cmn::array2Object($response->json());
        $lineAccessToken = $resVal->access_token;

        //プロフィール取得
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.$lineAccessToken,
        ])->get('https://api.line.me/v2/profile');
        if (!$response->ok()) {
            \Log::info('[400 Bad Request] - Failed to get profile. LINE SERVER Returned Error Status ['.$response->status().'] responsebody:['.$response->body().'] access_token:['.$lineAccessToken.']');
            return response()->json(['error' => 'Bad Request'], 400);
        }
        $resVal = Cmn::array2Object($response->json());
        $lineUserId = $resVal->userId;

        //LINEユーザーID登録
        $resident = \App\Models\Resident::find($residentId);
        $resident->line_user_id = $lineUserId;
        $resident->save();

        //入居者Model整形
        $resident->line_connected = !empty($resident->line_user_id);
        unset($resident->extra, $resident->line_user_id);

        \Log::info('SUCCESS');

        return response()->json($resident);
    }

    /**
     * LINE連携解除
     */
    public function cancelingLineAccountLinkage() {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;

        //LINEユーザーID削除
        $resident = \App\Models\Resident::find($residentId);
        $resident->line_user_id = null;
        $resident->save();

        //入居者Model整形
        $resident->line_connected = !empty($resident->line_user_id);
        unset($resident->extra, $resident->line_user_id);

        \Log::info('SUCCESS');

        return response()->json($resident);
    }

    /**
     * ステート生成処理
     *
     * @param int $length
     * @return string
     */
    private function makeState($length = 20) {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, $length);
    }
}
