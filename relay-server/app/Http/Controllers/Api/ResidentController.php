<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\ValueObjects\ApiVersion;
use App\Library\Cmn;
use App\Library\S3Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use ParagonIE\ConstantTime\Base32;
use OTPHP\TOTP;
use Hidehalo\Nanoid\Client;

class ResidentController extends Controller
{
    // Guardの認証方法を指定
    protected function guard()
    {
        return Auth::guard('guard_resident');
    }

    /**
     * 1段階目認証
     * 認証に成功したら、OTPを返却する
     */
    public function getOtp(Request $request)
    {
        //パラメータ
        $propertyCode = $request->property_code;
        $roomCode = $request->room_code;
        $residentCode = $request->resident_code;
        \Log::info('PARAM [property_code:'.$propertyCode.' room_code:'.$roomCode.' resident_code:'.$residentCode.']');

        //Authorizationヘッダ確認
        $authHeader = $request->header('Authorization');
        $aryHeader = preg_split('/ /', $authHeader);
        if (count($aryHeader) !== 2) {
            //Authorizationヘッダ不備
            \Log::info('[401 Unauthorized] - Invalid Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if ($aryHeader[0] !== 'Basic') {
            //Authorizationヘッダ Basicでない
            \Log::info('[401 Unauthorized] - Not Basic at Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $credentials = base64_decode($aryHeader[1]);
        $aryCredentials = preg_split('/:/', $credentials);
        if (count($aryCredentials) !== 2) {
            //credentials不備
            \Log::info('[401 Unauthorized] - Invalid Authorization header credentials');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $accessKeyId = $aryCredentials[0];
        $secretKey = $aryCredentials[1];
        $mc = \App\Models\ManagementCompanyApiAccessKey::where(['access_key_id' => $accessKeyId, 'secret_key' => $secretKey])->first();
        if (!$mc) {
            //管理会社特定できず
            \Log::info('[401 Unauthorized] - Cannot identify Management Company');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $resident = \App\Models\Resident::
                    select('rd.*')
                    ->from('residents as rd')
                    ->join('rooms as room', function ($join) use ($mc, $roomCode) {
                        $join->on('room.room_id', 'rd.room_id')
                            ->where('room.management_company_id', $mc->management_company_id)
                            ->where('room.room_code', $roomCode);
                    })
                    ->join('properties as property', function ($join) use ($mc, $propertyCode) {
                        $join->on('property.property_id', 'room.property_id')
                            ->where('property.management_company_id', $mc->management_company_id)
                            ->where('property.property_code', $propertyCode);
                    })
                    ->where('rd.management_company_id', $mc->management_company_id)
                    ->where('rd.resident_code', $residentCode)
                    ->first();
        if (!$resident) {
            //入居者特定できず
            \Log::info('[401 Unauthorized] - Cannot identify Resident');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //OKの場合、otp発行
        $totp = TOTP::create(Base32::encodeUpper($resident->resident_id.env('OTP_SECRET')), 20);
        $otp = $totp->now();

        //cacheに保存
        Cache::put($otp, $resident, 20);

        \Log::info('SUCCESS');

        return $otp;
    }

    /**
     * 2段階目認証
     * 認証に成功したら、トークンおよびユーザー情報を返却する
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $otp = $request->code;
        \Log::info('PARAM [code:'.$otp.']');

        //情報取得
        $resident = Cache::pull($otp);
        if (!$resident) {
            \Log::info('[401 Unauthorized] - Invalid otp');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //検証
        $totp = TOTP::create(Base32::encodeUpper($resident->resident_id.env('OTP_SECRET')), 20);
        if (!$totp->verify($otp)) {
            //失敗
            \Log::info('[401 Unauthorized] - otp timeout');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //アクセストークン・リフレッシュトークン、発行
        $token = $resident->tokenIssue();

        //入居者Model整形
        $resident->line_connected = !empty($resident->line_user_id);
        unset($resident->extra, $resident->line_user_id);

        //キャッシュ削除
        Cache::forget($otp);

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $token->access_token,
            'refresh_token' => $token->refresh_token,
            'resident' => $resident
        ]);
    }

    /**
     * ログイン
     * ログインID＋パスワードで認証
     */
    public function localLogin(Request $request)
    {
        //パラメータ
        $loginId = $request->login_id;
        $password = $request->password;
        \Log::info('PARAM [login_id:'.$loginId.' password:'.str_repeat('*', mb_strlen($password)).']');

        //user取得
        $user = \App\Models\User::from('users as user')
        ->where('user.email', $loginId)
        ->first();

        //userがないか、パスワードが異なる場合401返却
        if (!$user || !Hash::check($password, $user->password)) {
            \Log::info('[401 Unauthorized] - Invalid email or password');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //パーミッションがない場合401返却
        if (!$user->hasPermissionTo(config('const.Permissions.RESIDENT'))) {
            \Log::info('[401 Unauthorized] - Logined User don\'t have Permission');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //入居者取得
        $resident = \App\Models\Resident::where('user_id', $user->user_id)->first();
        if (!$resident) {
            //入居者特定できず
            \Log::info('[401 Unauthorized] - Cannot identify Resident');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //アクセストークン・リフレッシュトークン、発行
        $token = $resident->tokenIssue();

        //入居者Model整形
        $resident->line_connected = !empty($resident->line_user_id);
        unset($resident->extra, $resident->line_user_id);

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $token->access_token,
            'refresh_token' => $token->refresh_token,
            'resident' => $resident
        ]);
    }

    /**
     * アクセストークン再発行
     */
    public function accessTokenReissue(Request $request)
    {
        //パラメータ
        $refreshToken = hash('sha256', $request->refresh_token);
        \Log::info('PARAM [refresh_token:'.$refreshToken.']');

        //Authorizationヘッダ確認
        $authHeader = $request->header('Authorization');
        $aryHeader = preg_split('/ /', $authHeader);
        if (count($aryHeader) !== 2) {
            //Authorizationヘッダ不備
            \Log::info('[401 Unauthorized] - Invalid Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if ($aryHeader[0] !== 'Bearer') {
            //Authorizationヘッダ Bearerでない
            \Log::info('[401 Unauthorized] - Not Bearer at Authorization header');
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $accessToken = hash('sha256', $aryHeader[1]);

        //resident取得
        $resident = \App\Models\Resident::
        where('access_token', $accessToken)
        ->where('refresh_token', $refreshToken)
        ->first();

        //residentがない
        if (!$resident) {
            \Log::info('[401 Unauthorized] - Invalid token or refresh token');
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //アクセストークン・リフレッシュトークン発行・登録
        $token = $resident->tokenIssue();

        \Log::info('SUCCESS');

        return response()->json([
            'access_token' => $token->access_token,
            'refresh_token' => $token->refresh_token,
        ]);
    }

    /**
     * 問合せ一覧取得
     */
    public function getRequestList(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $filter = $webRequest->filter;

        $query = \App\Models\Request::
                    select(
                        'r.request_id',
                        'r.status',
                        'r.emergency_flg',
                        'r.ai_category',
                        'r.ai_sub_category',
                        'r.created_at',
                        \DB::raw('GROUP_CONCAT(rc.context separator " ") as context'),
                    )
                    ->from('requests as r')
                    ->leftJoin('request_contents as rc', function ($join) {
                        $join->on('rc.request_id', 'r.request_id');
                    })
                    ->where('r.resident_id', $residentId)
                    ->groupBy(
                        'r.request_id',
                        'r.status',
                        'r.emergency_flg',
                        'r.ai_category',
                        'r.ai_sub_category',
                        'r.created_at',
                    )
                    ->orderByRaw('r.created_at DESC');

        //filter
        if ($filter === 'future') {
            $query->whereIn('r.status', [config('const.Request.Status.WAITING_FOR_OPEN'), config('const.Request.Status.OPEN'), config('const.Request.Status.BID'), config('const.Request.Status.SCHEDULING'), config('const.Request.Status.IN_PROGRESS')]);
        } else if ($filter === 'past') {
            $query->whereNotIn('r.status', [config('const.Request.Status.WAITING_FOR_OPEN'), config('const.Request.Status.OPEN'), config('const.Request.Status.BID'), config('const.Request.Status.SCHEDULING'), config('const.Request.Status.IN_PROGRESS')]);
        }

        $requests = $query->paginate(10);

        foreach ($requests as $req) {
            //希望時間
            $preferableTimes =
            \App\Models\RequestPreferableTime::
            select(
                'request_preferable_time_id',
                'date',
                'time_section_id',
                'selected',
            )
            ->where('request_id', $req->request_id)
            ->orderByRaw('priority ASC')
            ->get();

            $req->preferable_info = $preferableTimes;
        }

        \Log::info('SUCCESS');

        return $requests;
    }

    /**
     * 問合せ情報取得
     */
    public function getRequestInfo(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $requestId = $webRequest->request_id;

        \Log::info('PARAM [request_id:'.$requestId.']');

        //問合せ情報＋入居者情報
        $request =
        \App\Models\Request::
        select(
            'r.request_id',
            'r.management_company_id',
            'r.resident_id',
            'resident.name as resident_name',
            'resident.tel as resident_tel',
            'property.name as property_name',
            'property.address as property_address',
            'room.room_no',
            'r.status',
            'r.emergency_flg',
            'r.ai_category',
            'r.ai_sub_category',
            'rc.context',
            'rc.response',
            'r.created_at',
            'r.updated_at',
        )
        ->from('requests as r')
        ->leftJoin('request_contents as rc', function ($join) {
            $join->on('rc.request_id', 'r.request_id');
        })
        ->leftJoin('residents as resident', function ($join) {
            $join->on('resident.resident_id', 'r.resident_id');
        })
        ->leftJoin('rooms as room', function ($join) {
            $join->on('room.room_id', 'resident.room_id');
        })
        ->leftJoin('properties as property', function ($join) {
            $join->on('property.property_id', 'room.property_id');
        })
        ->where('r.request_id', $requestId)
        ->where('r.resident_id', $residentId)
        ->first();

        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //希望時間
        $preferableTimes =
        \App\Models\RequestPreferableTime::
        select(
            'request_preferable_time_id',
            'date',
            'time_section_id',
            'selected',
        )
        ->where('request_id', $requestId)
        ->orderByRaw('priority ASC')
        ->get();

        $request->preferable_info = $preferableTimes;

        //キャンセル理由
        $cancelReason = null;
        $requestCancelReason = \App\Models\RequestCancelReason::find($requestId);
        if ($requestCancelReason) {
            $cancelReason = $requestCancelReason->content;
        }
        $request->cancel_reason = $cancelReason;

        //**************
        // 画像・動画
        //**************
        //S3インスタンス生成
        $s3 = new S3Client();

        //一覧取得
        $prefix = 'reports/'.$request->management_company_id.'/'.$request->resident_id.'/'.$request->request_id;
        $request->images = $s3->getPresignedUrlForRequest($prefix);

        \Log::info('SUCCESS');

        return $request;
    }

    /**
     * 問合せ登録
     */
    public function createRequest(ApiVersion $ver, Request $webRequest)
    {
        if ($ver->is(2)) {
            //*********************************
            //* Version 2
            //*********************************
            $resident = auth('guard_resident')->user();
            $residentId = $resident->resident_id;
            $mcId = $resident->management_company_id;
            $roomId = $resident->room_id;
            $room = \App\Models\Room::find($roomId);
            $property = \App\Models\Property::find($room->property_id);

            //パラメータ
            $originalText = $webRequest->original_text;
            $process = $webRequest->process;
            $step = $webRequest->step;
            $abort = Cmn::toBool($webRequest->abort);
            \Log::info('PARAM [original_text:'.$originalText.' process:'.preg_replace('/\n|\r|\r\n/', '', print_r($process, true)).' step:'.$step.' abort:'.Cmn::bool2str($abort).']');

            //チェック
            if ($step === 'start') {
                //初回問合せ時
                if (!$originalText) {
                    \Log::info('[400 Bad Request] - Prameter original_text is nothing');
                    return response()->json(['error' => 'Bad Request'], 400);
                }
            }

            //AIサーバリクエスト
            $aiOrgResponse = Http::asForm()->post(env('AI_SERVER_HOST').'/api/v2/answer', [
                'original_text' => $originalText,
                'process' => $process,
                'step' => $step,
                'abort' => $abort,
            ]);
            if (!$aiOrgResponse->ok()) {
                \Log::info('[400 Bad Request] - AI Server Returned Error Status ['.$aiOrgResponse->status().']');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //AIレスポンス
            $aiResponse = Cmn::array2Object($aiOrgResponse->json());

            //"complete"チェック
            if (!$aiResponse->complete) {
                //"complete" === false
                $response = $aiResponse;
                $response->specific = null;
                return response()->json($response);
            }

            //DIY,FAQをIDから検索
            $responseText = '';
            $processId = null;
            $faqDiyId = Cmn::nothing2Empty($aiResponse->specific, 'answer_id');
            $faqDiyData =
            \App\Models\FaqDiyData::
            where('faq_diy_id', $faqDiyId)
            ->where('management_company_id', $mcId)
            ->where('resident_type', $resident->resident_type)
            ->first();

            //検索結果があった場合
            if ($faqDiyData) {
                $responseText = $faqDiyData->text;
                $processId = $faqDiyData->process_id;
                //テンプレート系があった場合
                if ($faqDiyData->template && $faqDiyData->extra_table && $faqDiyData->extra_column) {
                    $rec = null;
                    switch ($faqDiyData->extra_table) {
                        case 'properties':
                            $rec = $property;
                            break;
                        case 'rooms':
                            $rec = $room;
                            break;
                        case 'residents':
                            $rec = $resident;
                            break;
                    }
                    //extraに該当カラムがあるかどうか
                    $plusLabel = '';
                    if (isset($rec->extra[$faqDiyData->extra_column])) {
                        $plusLabel = Cmn::templeteRender($faqDiyData->template, $rec->extra[$faqDiyData->extra_column]);
                    }
                    if ($responseText === '') {
                        $responseText .= $plusLabel;
                    } else {
                        $responseText .= "\n".$plusLabel;
                    }
                }
            }

            //ステータスチェックロジック
            //TODO 管理会社毎のチェックロジックをコールする
            $requestInfo = $this->checkLogicForEnV2($resident, $property, $room, $aiResponse, $responseText, $processId);

            $requestId = '';
            if (!$requestInfo->may_unknown) {
                $requestId = DB::transaction(function () use ($resident, $mcId, $aiResponse, $requestInfo) {
                    //登録処理
                    $request = new \App\Models\Request;
                    $request->resident_id = $resident->resident_id;
                    $request->management_company_id = $mcId;
                    $request->visibility = $requestInfo->visibility;
                    $request->status = $requestInfo->status;
                    $request->request_type = $requestInfo->request_type;
                    $request->ai_category = $requestInfo->ai_category;
                    $request->ai_sub_category = $requestInfo->ai_sub_category;
                    $request->emergency_flg = false;
                    $request->summary = null;
                    $request->rating = null;
                    $request->save();

                    $requestContent = new \App\Models\RequestContent;
                    $requestContent->request_id = $request->request_id;
                    $requestContent->context = $aiResponse->specific->formatted_text;
                    $requestContent->response = $requestInfo->response_text;
                    $requestContent->save();

                    return $request->request_id;
                });
            }

            \Log::info('SUCCESS');

            $response = $aiResponse;
            $response->specific = [
                'request_id' => $requestId,
                'request_type' => $requestInfo->request_type,
                'response_text' => $requestInfo->response_text,
                'may_unknown' => $requestInfo->may_unknown,
                'can_order' => $requestInfo->can_order,
                'support_center' => $requestInfo->support_center,
            ];
            return response()->json($response);



        } else {
            //*********************************
            //* Version 1
            //*********************************
            $resident = auth('guard_resident')->user();
            $residentId = $resident->resident_id;
            $mcId = $resident->management_company_id;
            $roomId = $resident->room_id;
            $context = $webRequest->context;

            \Log::info('PARAM [context:'.$context.']');

            if (!$context) {
                \Log::info('[400 Bad Request] - Prameter context is nothing');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //AIサーバリクエスト
            $response = Http::asForm()->post(env('AI_SERVER_HOST').'/api/v1/answer', [
                'text' => $context,
            ]);
            if (!$response->ok()) {
                \Log::info('[400 Bad Request] - AI Server Returned Error Status ['.$response->status().']');
                return response()->json(['error' => 'Bad Request'], 400);
            }

            //AIレスポンス
            $aiResponse = Cmn::array2Object($response->json());

            //ステータスチェックロジック
            $requestInfo = $this->checkLogicForEn($aiResponse);

            //FAQでテンプレートがあった場合
            if ($requestInfo->template) {
                $rec = null;
                switch ($requestInfo->table) {
                    case 'properties':
                        $room = \App\Models\Room::find($roomId);
                        $rec = \App\Models\Property::find($room->property_id);
                        break;
                    case 'rooms':
                        $rec = \App\Models\Room::find($roomId);
                        break;
                    case 'residents':
                        $rec = \App\Models\Resident::find($residentId);
                        break;
                }
                //extraに該当カラムがあるかどうか
                $plusLabel = '';
                if (isset($rec->extra[$requestInfo->label])) {
                    $plusLabel = Cmn::templeteRender($requestInfo->template, $rec->extra[$requestInfo->label]);
                } else {
                    $plusLabel = $requestInfo->empty_label;
                }
                if ($requestInfo->response_text === '') {
                    $requestInfo->response_text .= $plusLabel;
                } else {
                    $requestInfo->response_text .= "\n".$plusLabel;
                }
            }

            //AUTO_RESPONSE & response_textが空の場合、status='transferred'に変更
            if ($requestInfo->request_type === config('const.Request.RequestType.AUTO_RESPONSE') && $requestInfo->response_text === '') {
                $requestInfo->status = config('const.Request.Status.TRANSFERRED');
            }

            $requestId = DB::transaction(function () use ($resident, $mcId, $context, $requestInfo) {
                //登録処理
                $request = new \App\Models\Request;
                $request->resident_id = $resident->resident_id;
                $request->management_company_id = $mcId;
                $request->visibility = $requestInfo->visibility;
                $request->status = $requestInfo->status;
                $request->request_type = $requestInfo->request_type;
                $request->ai_category = $requestInfo->ai_category;
                $request->ai_sub_category = '';
                $request->emergency_flg = false;
                $request->summary = null;
                $request->rating = null;
                $request->save();

                $requestContent = new \App\Models\RequestContent;
                $requestContent->request_id = $request->request_id;
                $requestContent->context = $context;
                $requestContent->response = $requestInfo->response_text;
                $requestContent->save();

                return $request->request_id;
            });

            \Log::info('SUCCESS');

            return response()->json([
                'request_id' => $requestId,
                'request_type' => $requestInfo->request_type,
                'response_text' => $requestInfo->response_text,
                'may_unknown' => $requestInfo->may_unknown,
                'can_order' => $requestInfo->can_order,
                'support_center' => $requestInfo->support_center,
            ]);
        }
    }

    /**
     * 問合せ出動依頼
     */
    public function requestOrder(Request $webRequest) {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $requestId = $webRequest->request_id;
        $emergencyFlg = Cmn::toBool($webRequest->emergency_flg);
        $preferableTimes = $webRequest->preferable_times;

        \Log::info('PARAM [request_id:'.$requestId.' emergency_flg:'.Cmn::bool2str($emergencyFlg).' preferable_times:'.json_encode($preferableTimes).']');

        //パラメータチェック
        if (!$emergencyFlg) {
            if (!$preferableTimes || count($preferableTimes) === 0) {
                \Log::info('[400 Bad Request] - Not enough parameters');
                return response()->json(['error' => 'Bad Request'], 400);
            }
        }

        //問合せ情報
        $request =
        \App\Models\Request::
        from('requests as r')
        ->where('r.request_id', $requestId)
        ->where('r.resident_id', $residentId)
        ->where('r.request_type', config('const.Request.RequestType.VENDOR'))
        ->where('r.status', config('const.Request.Status.WAITING_FOR_OPEN'))
        ->first();
        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing. Or Request Status is not waitng_for_open');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        DB::transaction(function () use ($resident, $request, $emergencyFlg, $preferableTimes) {
            //指定業者
            $existsAssignedVendor = false;
            if ($resident->assigned_vendors) {
                $assignedVendors = explode(',', $resident->assigned_vendors);
                foreach ($assignedVendors as $av) {
                    $vendor = \App\Models\Vendor::find($av);
                    if ($vendor) {
                        $rvp = new \App\Models\RequestVendorPermission;
                        $rvp->request_id = $request->request_id;
                        $rvp->vendor_id = $av;
                        $rvp->save();
                        $existsAssignedVendor = true;
                    }
                }
            }

            //問合せ更新
            if ($existsAssignedVendor) {
                $request->visibility = config('const.Request.Visibility.PROTECTED');
            }
            $request->status = config('const.Request.Status.OPEN');
            $request->emergency_flg = $emergencyFlg;
            $request->save();

            if (!$emergencyFlg) {
                foreach ($preferableTimes as $idx => $preferableTime) {
                    $requestPreferableTime = new \App\Models\RequestPreferableTime;
                    $requestPreferableTime->request_id = $request->request_id;
                    $requestPreferableTime->priority = ($idx + 1);
                    $requestPreferableTime->date = $preferableTime['date'];
                    $requestPreferableTime->time_section_id = $preferableTime['time_section_id'];
                    $requestPreferableTime->selected = false;
                    $requestPreferableTime->save();
                }
            }
        });

        //業者通知処理
        $request->notifyVendor();

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 問合せキャンセル
     */
    public function requestCancel(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $cancelReason = $webRequest->cancel_reason;
        \Log::info('PARAM [request_id:'.$requestId.' cancel_reason:'.$cancelReason.']');

        try {
            //バリデーション
            $validatedData = $webRequest->validate([
                'cancel_reason' => 'required',
            ]);

        } catch (\Illuminate\Validation\ValidationException $e) {
            //バリデーションエラー
            $errors = array();
            foreach ($e->errors() as $error) {
                array_push($errors, $error[0]);
            }
            return response()->json(['errors' => $errors], 400);
        }

        //問合せ取得
        $request =
        \App\Models\Request::
        where('request_id', $requestId)
        ->where('resident_id', $residentId)
        ->first();
        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }
        $status = '';
        switch ($request->status) {
            case config('const.Request.Status.OPEN'):
            case config('const.Request.Status.BID'):
                $status = config('const.Request.Status.CANCELLED');
                break;
            case config('const.Request.Status.SCHEDULING'):
                $status = config('const.Request.Status.WAITING_FOR_CANCEL');
                break;
            default:
                //キャンセルできないステータス
                \Log::info('[400 Bad Request] - Request status don\'t cancel');
                return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($request, $status, $cancelReason) {
            //問合せテーブル
            $request->status = $status;
            $request->save();

            //問合せキャンセル理由テーブル
            $rcr = new \App\Models\RequestCancelReason;
            $rcr->request_id = $request->request_id;
            $rcr->content = $cancelReason;
            $rcr->save();
        });

        //入札業者に通知
        $request->notifyVendorCancel();

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 問合せ解決
     */
    public function requestSolved(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;

        $requestId = $webRequest->request_id;
        $solved = Cmn::toBool($webRequest->solved);
        \Log::info('PARAM [request_id:'.$requestId.' solved:'.$solved.']');

        //問合せ取得
        $request =
        \App\Models\Request::
        where('request_id', $requestId)
        ->where('resident_id', $residentId)
        ->first();
        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //チェック
        if ($request->status !== config('const.Request.Status.WAITING_FOR_OPEN')) {
            //変更不可なステータス
            \Log::info('[400 Bad Request] - Request status cannot be changed (status='.$request->status.')');
            return response()->json(['error' => 'Bad Request'], 400);
        }
        if ($request->request_type !== config('const.Request.RequestType.VENDOR') && $request->request_type !== config('const.Request.RequestType.AUTO_RESPONSE')) {
            //変更不可なリクエストタイプ
            \Log::info('[400 Bad Request] - Request type cannot be changed (request_type='.$request->request_type.')');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($request, $solved) {
            if ($solved) {
                //解決
                $request->rating = 10;
                $request->status = config('const.Request.Status.AUTO_COMPLETED');
            } else {
                //未解決
                $request->rating = 2;
                if ($request->request_type === config('const.Request.RequestType.AUTO_RESPONSE')) {
                    $request->status = config('const.Request.Status.TRANSFERRED');
                }
            }
            $request->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 問合せ入札承認
     */
    public function requestBidApprove(Request $webRequest) {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;

        //パラメータ
        $requestId = $webRequest->request_id;

        //問合せ情報
        $request =
        \App\Models\Request::
        from('requests as r')
        ->where('r.request_id', $requestId)
        ->where('r.resident_id', $residentId)
        ->where('r.status', config('const.Request.Status.BID'))
        ->first();
        if (!$request) {
            \Log::info('[400 Bad Request] - Logined User don\'t have Permission for Request. Or Request Nothing. Or Request Status is not bid');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        DB::transaction(function () use ($request) {
            //問合せ更新
            $request->status = config('const.Request.Status.SCHEDULING');
            $request->save();
        });

        //業者通知処理
        $request->notifyVendorApprove();

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * 管理会社情報取得
     */
    public function getManagementCompanyInfo(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $mcId = $resident->management_company_id;

        //管理会社情報
        $mc = \App\Models\ManagementCompany::find($mcId);
        if (!$mc) {
            \Log::info('[400 Bad Request] - Management Company Id not found.');
            return response()->json(['error' => 'Bad Request'], 400);
        }
        $tel = $mc->support_tel;
        if (!$tel) {
            $tel = $mc->tel;
        }

        //LINE情報
        $mcli = \App\Models\ManagementCompanyLineInformation::find($mcId);
        $use_line = !empty($mcli);

        \Log::info('SUCCESS');

        return response()->json([
            'name' => $mc->name,
            'support_tel' => $tel,
            'use_line' => $use_line,
        ]);
    }

    /**
     * 問合せフィードバック登録
     */
    public function createFeedback(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;

        $requestId = $webRequest->request_id;
        $feedbackContent = $webRequest->feedback_content;
        \Log::info('PARAM [request_id:'.$requestId.' feedback_content:'.$feedbackContent.']');

        //チェック
        if ($feedbackContent === null || $feedbackContent === '') {
            //フィードバック内容なし
            \Log::info('[400 Bad Request] - feedback content is nothing.');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        $request =
        \App\Models\Request::
        where('request_id', $requestId)
        ->where('resident_id', $residentId)
        ->first();
        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //更新処理
        DB::transaction(function () use ($requestId, $residentId, $feedbackContent) {
            $requestFeedback = new \App\Models\RequestFeedback;
            $requestFeedback->request_id = $requestId;
            $requestFeedback->resident_id = $residentId;
            $requestFeedback->feedback_content = $feedbackContent;
            $requestFeedback->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
        ], 201);
    }

    /**
     * チェックロジック
     * （en用）
     */
    private function checkLogicForEn($aiResponse)
    {
        //返却用オブジェクト
        $response = new \stdClass;
        $response->ai_category = $aiResponse->ai_category;
        $response->may_unknown = $aiResponse->may_unknown;
        $response->response_text = '';
        $response->template = '';
        $response->table = '';
        $response->label = '';
        $response->empty_label = '';
        $response->can_order = false;
        $response->support_center = false;
        $response->vendor_permissions = [];

        //質問不明性
        // if ($aiResponse->may_unknown) {
        //     return $response;
        // }

        //ai_category
        switch ($aiResponse->ai_category)
        {
            //共用部・管理会社対応の場合
            case config('const.AiCategory.COMMUNAL_AREA') :
            case config('const.AiCategory.MANAGEMENT_COMPANY') :
                //specific->answerがあれば、内部的に自動回答として扱う
                if (!isset($aiResponse->specific->answer)) {
                    $response->request_type = config('const.Request.RequestType.IN_HOUSE');
                    $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                    $response->status = config('const.Request.Status.TRANSFERRED');
                    break;
                }

            //サポートセンター（FAQ）の場合
            case config('const.AiCategory.SUPPORT_CENTER') :
                $response->request_type = config('const.Request.RequestType.AUTO_RESPONSE');
                $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                if (isset($aiResponse->specific->answer)) {
                    $response->response_text = Cmn::nothing2Empty($aiResponse->specific->answer, 'answer');
                    $response->template = Cmn::nothing2Empty($aiResponse->specific->answer, 'template');
                    $response->table = Cmn::nothing2Empty($aiResponse->specific->answer, 'table');
                    $response->label = Cmn::nothing2Empty($aiResponse->specific->answer, 'label');
                    $response->empty_label = Cmn::nothing2Empty($aiResponse->specific->answer, 'empty_label');
                    $response->support_center = Cmn::nothing2Empty($aiResponse->specific->answer, 'support_center');
                }
                break;

            //業者対応の場合
            case config('const.AiCategory.VENDOR') :
                $response->vendor_permissions = [];
                $response->visibility = config('const.Request.Visibility.PUBLIC');
                $response->can_order = true;
                $response->request_type = config('const.Request.RequestType.VENDOR');
                $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                if (isset($aiResponse->specific->answer)) {
                    $response->response_text = Cmn::nothing2Empty($aiResponse->specific->answer, 'answer');
                }
                break;
        }

        //返却
        return $response;
    }

    /**
     * チェックロジックV2
     * （en用）
     */
    private function checkLogicForEnV2($resident, $property, $room, $aiResponse, $responseText, $processId)
    {
        //返却用オブジェクト
        $response = new \stdClass;
        $response->ai_category = $aiResponse->ai_category;
        $response->ai_sub_category = $aiResponse->ai_sub_category;
        $response->may_unknown = false;
        $response->can_order = false;
        $response->support_center = false;
        $response->response_text = $responseText;

        //**********************
        //* 基本判断
        //**********************
        //ai_category
        switch ($aiResponse->ai_category)
        {
            //共用部・管理会社対応・サポートセンターの場合
            case config('const.AiCategory.COMMUNAL_AREA') :
            case config('const.AiCategory.MANAGEMENT_COMPANY') :
            case config('const.AiCategory.SUPPORT_CENTER') :
                //responseTextがあれば、内部的に自動回答として扱う
                if (!$responseText) {
                    $response->request_type = config('const.Request.RequestType.IN_HOUSE');
                    $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                    $response->status = config('const.Request.Status.TRANSFERRED');
                } else {
                    $response->request_type = config('const.Request.RequestType.AUTO_RESPONSE');
                    $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                    $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                }
                break;

            //業者対応の場合
            case config('const.AiCategory.VENDOR') :
                $response->can_order = $resident->can_order;
                if ($response->can_order) {
                    $response->request_type = config('const.Request.RequestType.VENDOR');
                    $response->visibility = config('const.Request.Visibility.PUBLIC');
                    $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                } else {
                    $response->request_type = config('const.Request.RequestType.IN_HOUSE');
                    $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                    $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                }
                break;

            //unknownの場合
            case config('const.AiCategory.UNKNOWN') :
                $response->may_unknown = true;
                $response->request_type = '';
                $response->visibility = '';
                $response->status = '';
                break;
        }

        //**********************
        //* process_id 判断
        //**********************
        switch ($processId)
        {
            //玄関ドアが開錠できない
            case 'house_key_can_not_opened':
                if (!$room->security_company_tel) {
                    //警備会社電話番号がなかったらスルー
                    break;
                }
                switch ($room->house_key_type)
                {
                    case 'HB':
                    case 'SL':
                        $response->request_type = config('const.Request.RequestType.AUTO_RESPONSE');
                        $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                        $response->status = config('const.Request.Status.TRANSFERRED');
                        $response->response_text = "以下にご連絡ください。\n".$room->security_company_tel;
                        break;
                    default:
                        $response->request_type = config('const.Request.RequestType.AUTO_RESPONSE');
                        $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                        $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                        $response->response_text = "・換気扇が回っていると扉が開きにくいかもしれないので、扉を少し手前に引きながら開錠して下さい。\n・電池切れの可能性があります。最近電池交換はされましたでしょうか。交換されてない場合、えんくらぶ会員の方はえんくらぶアプリ内えんサポート24の鍵を選択していただき、交換方法をご確認下さい。\n・それでも開錠できない場合、以下にご連絡ください。\n緊急対応、時間外の場合：".$room->security_company_tel."\n上記以外の場合：092-260-5343";
                }
                break;

            //玄関ドアから警報が鳴っている
            case 'house_key_sounds_alarm':
                $response->request_type = config('const.Request.RequestType.AUTO_RESPONSE');
                $response->visibility = config('const.Request.Visibility.IN_HOUSE');
                $response->status = config('const.Request.Status.WAITING_FOR_OPEN');
                $response->response_text = "・電池切れの可能性があります。最近電池交換はされましたでしょうか。交換されてない場合、えんくらぶ会員の方はえんくらぶアプリ内えんサポート24の鍵を選択していただき、交換方法をご確認下さい。\n・それでも開錠できない場合、以下にご連絡ください。\n緊急対応、時間外の場合：".$room->security_company_tel."\n上記以外の場合：092-260-5343";
                break;
        }

        //返却
        return $response;
    }

    /**
     * バッジ表示用情報取得
     */
    public function getBadgeInfo(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $badgeGotAt = $resident->badge_got_at;

        //問合せ状況取得
        $count =
        \App\Models\Request::
        select(
            \DB::raw('COUNT(status = "'.config('const.Request.Status.OPEN').'" or null) as open_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.SCHEDULING').'" or null) as scheduling_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.IN_PROGRESS').'" or null) as in_progress_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.COMPLETED').'" or null) as completed_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.WAITING_FOR_CANCEL').'" or null) as waiting_for_cancel_count'),
            \DB::raw('COUNT(status = "'.config('const.Request.Status.CANCELLED').'" or null) as cancelled_count'),
        )
        ->where('resident_id', $residentId)
        ->where('request_type', config('const.Request.RequestType.VENDOR'))
        ->where('updated_at', '>=', $badgeGotAt)
        ->first();

        //更新処理
        DB::transaction(function () use ($resident) {
            $resident->badge_got_at = Carbon::now();
            $resident->save();
        });

        \Log::info('SUCCESS');

        return response()->json([
            'open' => $count->open_count,
            'scheduling' => $count->scheduling_count,
            'in_progress' => $count->in_progress_count,
            'completed' => $count->completed_count,
            'waiting_for_cancel' => $count->waiting_for_cancel_count,
            'cancelled' => $count->cancelled_count,
        ]);
    }

    /**
     * S3アップロード用URL取得
     */
    public function getS3PreSignedURL(Request $webRequest)
    {
        $resident = auth('guard_resident')->user();
        $residentId = $resident->resident_id;
        $mcId = $resident->management_company_id;

        //パラメータ
        $requestId = $webRequest->request_id;
        $files = $webRequest->get('files');
        if (!is_array($files) || count($files) <= 0) {
            \Log::info('[400 Bad Request] - files parameter is wrong');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //問合せ取得
        $request =
        \App\Models\Request::
        where('request_id', $requestId)
        ->where('resident_id', $residentId)
        ->first();
        if (!$request) {
            //問合せ情報なし
            \Log::info('[400 Bad Request] - Request not found');
            return response()->json(['error' => 'Bad Request'], 400);
        }

        //S3インスタンス生成
        $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
        $s3 = new \Aws\S3\S3Client([
            'version'     => 'latest',
            'signature_version' => 'v4',
            'region'      => env('AWS_DEFAULT_REGION'),
            'credentials' => $credentials
        ]);
        $prefix = 'reports/'.$mcId.'/'.$residentId.'/'.$requestId.'/';

        //署名付きURL取得
        $urls = array();
        foreach ($files as $file) {
            $cmd = $s3->getCommand('PutObject', [
                'Bucket' => env('AWS_BUCKET'),
                'Key' => $prefix.$file,
            ]);
            $s3Request = $s3->createPresignedRequest($cmd, '+20 minutes');
            $url = (string)$s3Request->getUri();
            array_push($urls, $url);
        }

        // $postObject = new \Aws\S3\PostObjectV4(
        //     $s3,
        //     env('AWS_BUCKET'),
        //     ['key' => 'test.jpg',],
        //     [],
        //     '+20 minutes'
        // );
        // $formAttributes = $postObject->getFormAttributes();
        // $formInputs = $postObject->getFormInputs();

        \Log::info('SUCCESS');

        return response()->json($urls);
    }
}