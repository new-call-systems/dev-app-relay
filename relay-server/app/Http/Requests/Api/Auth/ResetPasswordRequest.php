<?php

namespace App\Http\Requests\Api\Auth;

use App\Rules\TokenExpirationTimeRule;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['required','confirmed','min:8', 'max:128'],
            'password_confirmation' => ['required'],
            'reset_token' => ['required', new TokenExpirationTimeRule],
        ];
    }

    /**
     * バリデーションメッセージのカスタマイズ
     * @return array
     */
    public function messages()
    {
        return [
            'password.required' => ':attributeを入力してください',
            'password.min' => ':attributeは最低8文字である必要があります',
            'password.max' => ':attributeは最大128文字までです',
            'password_confirmation.confirmed' => '入力されたパスワードが一致しません',
        ];
    }

    /**
     * attribute名をカスタマイズ
     * @return array
     */
    public function attributes()
    {
        return [
            'password' => 'パスワード',
        ];
    }
}
