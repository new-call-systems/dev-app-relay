<?php

namespace App\Http\ValueObjects;

class ApiVersion
{
    /**
     * @var int
     */
    protected $value;

    /**
     * @param string $versionString
     */
    public function __construct(string $versionString)
    {
        $this->value = (int)substr($versionString, 1);
    }

    /**
     * @return int
     */
    public function value(): int
    {
        return $this->value;
    }

    /**
     * @param int $number
     * @return bool
     */
    public function is(int $number): bool
    {
        return $this->value === $number;
    }
}