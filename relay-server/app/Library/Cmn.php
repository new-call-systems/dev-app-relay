<?php

namespace App\Library;
use App;

class Cmn
{
    /**
     * boolean型へ変換
     */
    public static function toBool($obj) {
        if (is_string($obj)) {
            return Cmn::str2bool($obj);
        } else if (is_bool($obj)) {
            return $obj;
        } else if (is_int($obj)) {
            return boolval($obj);
        } else if ($obj === null) {
            return false;
        }
    }
    /**
     * 文字列をbooleanへ変換
     */
    public static function str2bool(string $str) {
        return ($str === 'true');
    }
    /**
     * booleanを文字列へ変換
     */
    public static function bool2str(bool $bool) {
        return ($bool) ? 'true' : 'false';
    }

    /**
     * requestTypeを日本語ラベルへ変換
     */
    public static function requestType2Label(string $requestType) {
        $return = '';
        switch ($requestType) {
            case config('const.Request.RequestType.VENDOR'):
                $return = '業者対応';
                break;
            case config('const.Request.RequestType.AUTO_RESPONSE'):
                $return = '自動回答';
                break;
            case config('const.Request.RequestType.IN_HOUSE'):
                $return = '管理会社対応';
                break;
        }
        return $return;
    }

    /**
     * statusを日本語ラベルへ変換
     */
    public static function status2Label(string $status) {
        $return = '';
        switch ($status) {
            case config('const.Request.Status.WAITING_GRASPED'):
                $return = '内容不明';
                break;
            case config('const.Request.Status.WAITING_FOR_OPEN'):
                $return = '入居者問合せ済み';
                break;
            case config('const.Request.Status.WAITING_FOR_CANCEL'):
                $return = '入居者キャンセル待ち';
                break;
            case config('const.Request.Status.OPEN'):
                $return = '業者入札待ち';
                break;
            case config('const.Request.Status.BID'):
                $return = '業者入札済み';
                break;
            case config('const.Request.Status.SCHEDULING'):
                $return = '業者対応予定';
                break;
            case config('const.Request.Status.IN_PROGRESS'):
                $return = '業者対応中';
                break;
            case config('const.Request.Status.COMPLETED'):
                $return = '業者対応完了';
                break;
            case config('const.Request.Status.DELETED'):
                $return = '削除';
                break;
            case config('const.Request.Status.CANCELLED'):
                $return = 'キャンセル';
                break;
            case config('const.Request.Status.TRANSFERRED'):
                $return = '電話番号案内済み';
                break;
            case config('const.Request.Status.AUTO_COMPLETED'):
                $return = '自動回答済み';
                break;
        }
        return $return;
    }

    /**
     * 日付チェック
     */
    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * テンプレートレンダー
     */
    public static function templeteRender($template, array $vars) {
        return preg_replace_callback('/{{(\w+)}}/', function($m) use ($vars) {
            return $vars[$m[1]];
        }, $template);
    }

    /**
     * 連想配列⇒オブジェクト型
     */
    public static function array2Object($array, $exclusion = array()) {
        $obj = new \stdClass;
        foreach($array as $k => $v) {
            if(strlen($k)) {
                if (in_array($k, $exclusion, true)) {
                    $obj->{$k} = $v;
                } else if(is_array($v)) {
                    $obj->{$k} = Cmn::array2Object($v);
                } else {
                    $obj->{$k} = $v;
                }
            }
        }
        return $obj;
    }

    /**
     * 該当のオブジェクト（プロパティ）をチェック
     * 存在すればそのまま返す
     * 無かったら空を返す
     */
    public static function nothing2Empty($obj, $property) {
        if (isset($obj->{$property})) {
            return $obj->{$property};
        } else {
            return '';
        }
    }

    /**
     * 日付フォーマットの変換
     * '-'区切り ⇒ '/'区切り オンリー
     */
    public static function dateFormat($val) {
        return preg_replace('/-/', '/', $val);
    }

    /**
     * basenameの拡張
     * GETパラメータを削除する
     */
    public static function basename($val) {
        $val = basename($val);
        $val = preg_replace('/(.*)\?.*/', '\1', $val);
        return $val;
    }

    /**
     * S3のファイルが画像か動画かを判断してlabelを返す
     */
    public static function getFileType($path)
    {
        if (!$path) {
            return '';
        }
        $videos = ['mp4', 'mpg', 'mpeg', 'webm', 'ogv', 'mov', 'avi'];
        $fi = pathinfo($path);
        if (in_array(strtolower($fi['extension']), $videos, false)) {
            return 'video';
        } else {
            return 'image';
        }
    }

    /**
     * 空の場合Nullを返す
     */
    public static function empty2Null($val)
    {
        if ($val) {
            return $val;
        } else {
            return null;
        }
    }

    /**
     * 囲み文字（1文字目と最終文字）を消す
     */
    public static function removeEnclosure($val)
    {
        if (strlen($val) >= 3) {
            return substr($val, 1, strlen($val) - 2);
        } else {
            return '';
        }
    }

    /**
     * base64からファイルの拡張子を取得
     */
    public static function getExtension($base64Text)
    {
        $encodedImgString = explode(',', $base64Text, 2)[1];
        $decodedImgString = base64_decode($encodedImgString);
        $info = getimagesizefromstring($decodedImgString);
        $ext = '';
        if ($info['mime']) {
            switch ($info['mime'])
            {
                case 'image/bmp':
                    $ext .= '.bmp';
                    break;
                case 'image/gif':
                    $ext .= '.gif';
                    break;
                case 'image/png':
                    $ext .= '.png';
                    break;
                case 'image/jpeg':
                    $ext .= '.jpg';
                    break;
            }
        }
        return $ext;
    }

    /**
     * post_max_size取得
     */
    public static function getPostMaxSize()
    {
        if (is_numeric($postMaxSize = ini_get('post_max_size'))) {
            return (int) $postMaxSize;
        }

        $metric = strtoupper(substr($postMaxSize, -1));
        $postMaxSize = (int) $postMaxSize;

        switch ($metric) {
            case 'K':
                return $postMaxSize * 1024;
            case 'M':
                return $postMaxSize * 1048576;
            case 'G':
                return $postMaxSize * 1073741824;
            default:
                return $postMaxSize;
        }
    }
}