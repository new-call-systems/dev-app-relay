<?php

namespace App\Library;
use App;

class S3Client
{
    protected $s3;

    public function __construct()
    {
        try {
            $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
            $this->s3 = new \Aws\S3\S3Client([
                'version'     => 'latest',
                'region'      => env('AWS_DEFAULT_REGION'),
                'credentials' => $credentials
            ]);

        } catch (\Exception $e) {
            //

        }
    }

    /**
     * S3アップロード
     */
    public function putObject($key, $body)
    {
        try {
            if (!$this->s3) {
                //S3クライアントが無かったら一応生成
                $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
                $this->s3 = new \Aws\S3\S3Client([
                    'version'     => 'latest',
                    'region'      => env('AWS_DEFAULT_REGION'),
                    'credentials' => $credentials
                ]);
            }
            $this->s3->putObject([
                'Bucket' => env('AWS_BUCKET'),
                'Key'    => $key,
                'Body'   => $body,
            ]);

        } catch (\Exception $e) {
            //

        }
    }

    /**
     * S3削除
     */
    public function deleteObject($key)
    {
        try {
            if (!$this->s3) {
                //S3クライアントが無かったら一応生成
                $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
                $this->s3 = new \Aws\S3\S3Client([
                    'version'     => 'latest',
                    'region'      => env('AWS_DEFAULT_REGION'),
                    'credentials' => $credentials
                ]);
            }
            $this->s3->deleteObjects([
                'Bucket'  => env('AWS_BUCKET'),
                'Delete' => [
                    'Objects' => [
                        [
                            'Key' => $key
                        ]
                    ]
                ]
            ]);

        } catch (\Exception $e) {
            //

        }
    }

    /**
     * 署名リンク一覧取得（完了報告書向け）
     */
    public function getPresignedUrlForReport($prefix, $multiple = true, $exclusions = array())
    {
        try {
            if (!$this->s3) {
                //S3クライアントが無かったら一応生成
                $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
                $this->s3 = new \Aws\S3\S3Client([
                    'version'     => 'latest',
                    'region'      => env('AWS_DEFAULT_REGION'),
                    'credentials' => $credentials
                ]);
            }
            //一覧取得
            $list = $this->s3->listObjects([
                'Bucket' => env('AWS_BUCKET'),
                'Prefix' => $prefix,
            ]);

            //署名リンク取得処理
            if ($multiple) {
                $imagePath = array();
            } else {
                $imagePath = null;
            }
            if ($list['Contents']) {
                foreach ($list['Contents'] as $img) {
                    if ($img['Size'] === '0') {
                        continue;
                    }
                    if(strpos($img['Key'],$prefix.'/') === false){
                        //prefix + / が含まれていない場合
                        continue;
                    }
                    //exclusionsはスキップ
                    if (in_array(basename($img['Key']), $exclusions, true)) {
                        continue;
                    }
                    $cmd = $this->s3->getCommand('GetObject', [
                        'Bucket' => env('AWS_BUCKET'),
                        'Key' => $img['Key'],
                    ]);
                    $request = $this->s3->createPresignedRequest($cmd, '+20 minutes');
                    $url = (string)$request->getUri();
                    if ($multiple) {
                        array_push($imagePath, $url);
                    } else {
                        $imagePath = $url;
                        break;
                    }
                }
            }

            return $imagePath;

        } catch (\Exception $e) {
            //例外の場合は空返却
            if ($multiple) {
                return array();
            } else {
                return null;
            }

        }
    }

    /**
     * 署名リンク一覧取得（問合せ向け）
     */
    public function getPresignedUrlForRequest($prefix)
    {
        try {
            if (!$this->s3) {
                //S3クライアントが無かったら一応生成
                $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
                $this->s3 = new \Aws\S3\S3Client([
                    'version'     => 'latest',
                    'region'      => env('AWS_DEFAULT_REGION'),
                    'credentials' => $credentials
                ]);
            }
            //一覧取得
            $list = $this->s3->listObjects([
                'Bucket' => env('AWS_BUCKET'),
                'Prefix' => $prefix,
            ]);

            //署名リンク取得処理
            $imagePaths = array();
            if ($list['Contents']) {
                foreach ($list['Contents'] as $file) {
                    if ($file['Size'] === '0') {
                        continue;
                    }
                    if(strpos($file['Key'],$prefix.'/') === false){
                        //prefix + / が含まれていない場合
                        continue;
                    }
                    $cmd = $this->s3->getCommand('GetObject', [
                        'Bucket' => env('AWS_BUCKET'),
                        'Key' => $file['Key'],
                    ]);
                    $s3req = $this->s3->createPresignedRequest($cmd, '+20 minutes');
                    $url = (string)$s3req->getUri();
                    $type = Cmn::getFileType($file['Key']);
                    $obj = new \stdClass;
                    $obj->type = $type;
                    $obj->url = $url;
                    array_push($imagePaths, $obj);
                }
            }

            return $imagePaths;

        } catch (\Exception $e) {
            //例外の場合は空返却
            return array();

        }
    }

    public function zipDownload($prefix, $zipFileName = 'image.zip')
    {
        try {
            if (!$this->s3) {
                //S3クライアントが無かったら一応生成
                $credentials = new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
                $this->s3 = new \Aws\S3\S3Client([
                    'version'     => 'latest',
                    'region'      => env('AWS_DEFAULT_REGION'),
                    'credentials' => $credentials
                ]);
            }

            //一覧取得
            $list = $this->s3->listObjects([
                'Bucket' => env('AWS_BUCKET'),
                'Prefix' => $prefix,
            ]);

            //ストリームラッパーを登録
            $this->s3->registerStreamWrapper();

            //オプション設定
            $options = new \ZipStream\Option\Archive();
            $options->setSendHttpHeaders(true);
            $options->setZeroHeader(true);

            //処理
            $zip = new \ZipStream\ZipStream($zipFileName, $options);
            if ($list['Contents']) {
                foreach ($list['Contents'] as $img) {
                    if ($img['Size'] === '0') {
                        //ディレクトリは無視
                        continue;
                    }
                    if(strpos($img['Key'],$prefix.'/') === false){
                        //prefix + / が含まれていない場合
                        continue;
                    }
                    $file = 's3://'.env('AWS_BUCKET').'/'.$img['Key'];
                    if ($streamRead = fopen($file, 'r')) {
                        $zip->addFileFromStream(basename($img['Key']), $streamRead);
                        fclose($streamRead);
                    }
                }
            }
            $zip->finish();

        } catch (\Exception $e) {
            //

        }
    }
}