<?php

namespace App\Logging;

use Illuminate\Contracts\Foundation\Application;

/**
 * アプリケーションログ設定クラス
 *
 * @package app.Bootstrap
 */
class ApplicationLog
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Bootstrap the given application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        $this->app = $app;

        // テスト時はアプリケーションログなし
        if (!$app->environment('testing')) {
            // シャットダウンハンドラ追加：シャットダウンログ
            register_shutdown_function([$this, 'shutdownLog']);

            // 起動ログ
            $this->startupLog();
        }
    }

    /**
     * 起動ログ
     */
    public function startupLog()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, '/api/ping') === false) {
            \Log::info(' <<<START>>> - ['.$method.'] ['.$uri.']');
        }
    }

    /**
     * シャットダウンログ
     *
     * @return void
     */
    public function shutdownLog()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, '/api/ping') === false) {
            \Log::info('<<< END >>> - ['.$method.'] ['.$uri.']');
        }
    }

}