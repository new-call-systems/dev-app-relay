<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use Illuminate\Database\Eloquent\Model;

class ManagementCompanyVendor extends Model
{
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'management_company_vendors';
    protected $primaryKey = ['management_company_id', 'core_system_vendor_code'];
    public $incrementing = false;
}
