<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Hidehalo\Nanoid\Client;

class Request extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'requests';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_id';

    public $incrementing = false;

    protected $casts = [
        'emergency_flg' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(Request $request){
            $client = new Client();
            $requestId = $client->formattedId('1234567890', 11);
            //頭0削除のため
            $request->request_id = abs($requestId);
        });
    }

    /**
     * 該当問合せに入札ができる業者全員にプッシュ通知を行う
     */
    public function notifyVendor()
    {
        //request_type判定
        if ($this->request_type !== config('const.Request.RequestType.VENDOR')) {
            //業者判定以外はスルー
            return;
        }

        //対象業者判定
        $vendors = array();
        if ($this->visibility === config('const.Request.Visibility.PROTECTED')) {
            $rvp = \App\Models\RequestVendorPermission::where('request_id', $this->request_id)->get();
            foreach ($rvp as $r) {
                array_push($vendors, $r->vendor_id);
            }
        }

        //対象作業員
        $query =
        \App\Models\VendorMember::
        whereNotNull('firebase_device_id');
        if (count($vendors) > 0) {
            $query->whereIn('vendor_id', $vendors);
        }
        $vendorMembers = $query->get();

        //****************
        // プッシュ通知
        //****************
        if ($vendorMembers) {
            try {
                //チャンネル名(固定?)
                $channelName = 'notify-vendor-'.Carbon::now()->format('YmdHisu');

                //インスタンス生成
                $expo = \ExponentPhpSDK\Expo::normalSetup();

                //通知内容
                $notification = [
                    'title' => '入札可能な問合せがありました',
                    'body' => '入居者から入札可能な問合せがありました。',
                    'channelId' => env('FIREBASE_CHANNEL_ID'),
                ];

                //subscribe
                foreach ($vendorMembers as $vnm) {
                    $recipient= $vnm->firebase_device_id;
                    $expo->subscribe($channelName, $recipient);
                }

                //通知
                $expo->notify([$channelName], $notification);

            } catch (\ExponentPhpSDK\Exceptions\ExpoException $e) {
                //送れなかった場合、ログだけ吐いとく
                \Log::info('[ExpoException]'.$e->getMessage());
            } catch (\ExponentPhpSDK\Exceptions\UnexpectedResponseException $e) {
                //送れなかった場合、ログだけ吐いとく
                \Log::info('[UnexpectedResponseException]'.$e->getMessage());
            } finally {
                //最終的にチャンネルID情報削除
                $expo->unsubscribe($channelName);
            }
        }
    }

    /**
     * 該当問合せの担当作業者に問合せがキャンセルされた旨のプッシュ通知を行う
     */
    public function notifyVendorCancel()
    {
        //ステータスが'waiting_for_cancel'の場合のみ通知
        if ($this->status === config('const.Request.Status.WAITING_FOR_CANCEL')) {
            //作業担当者取得
            $rhu = \App\Models\RequestHandleUser::where('request_id', $this->request_id)->get();
            $vnms = array();
            foreach ($rhu as $r) {
                $vnm =
                \App\Models\VendorMember::
                where('user_id', $r->user_id)
                ->where('vendor_id', $r->group_id)
                ->whereNotNull('firebase_device_id')
                ->first();
                if ($vnm) {
                    array_push($vnms, $vnm);
                }
            }

            //****************
            // プッシュ通知
            //****************
            if (count($vnms) > 0) {
                try {
                    //チャンネル名
                    $channelName = 'notify-vendor-cancel-'.Carbon::now()->format('YmdHisu');

                    //インスタンス生成
                    $expo = \ExponentPhpSDK\Expo::normalSetup();

                    //通知内容
                    $notification = [
                        'title' => '入札した問合せがキャンセルされました',
                        'body' => "入居者が問合せをキャンセルしました。\n確認の上、キャンセル承認をしてください。",
                        'channelId' => env('FIREBASE_CHANNEL_ID'),
                    ];

                    //subscribe
                    foreach ($vnms as $vnm) {
                        $recipient= $vnm->firebase_device_id;
                        $expo->subscribe($channelName, $recipient);
                    }

                    //通知
                    $expo->notify([$channelName], $notification);

                } catch (\ExponentPhpSDK\Exceptions\ExpoException $e) {
                    //送れなかった場合、ログだけ吐いとく
                    \Log::info('[ExpoException]'.$e->getMessage());
                } catch (\ExponentPhpSDK\Exceptions\UnexpectedResponseException $e) {
                    //送れなかった場合、ログだけ吐いとく
                    \Log::info('[UnexpectedResponseException]'.$e->getMessage());
                } finally {
                    //最終的にチャンネルID情報削除
                    $expo->unsubscribe($channelName);
                }
            }
        }
    }

    /**
     * 該当問合せの担当作業者に問合せの入札が承認された旨のプッシュ通知を行う
     */
    public function notifyVendorApprove()
    {
        //作業担当者取得
        $rhu = \App\Models\RequestHandleUser::where('request_id', $this->request_id)->get();
        $vnms = array();
        foreach ($rhu as $r) {
            $vnm =
            \App\Models\VendorMember::
            where('user_id', $r->user_id)
            ->where('vendor_id', $r->group_id)
            ->whereNotNull('firebase_device_id')
            ->first();
            if ($vnm) {
                array_push($vnms, $vnm);
            }
        }

        //****************
        // プッシュ通知
        //****************
        if (count($vnms) > 0) {
            try {
                //チャンネル名
                $channelName = 'notify-vendor-approve-'.Carbon::now()->format('YmdHisu');

                //インスタンス生成
                $expo = \ExponentPhpSDK\Expo::normalSetup();

                //通知内容
                $notification = [
                    'title' => '入札した問合せが承認されました',
                    'body' => "入居者が入札内容で承認しました。\n指定日に問合せ対応をしてください。",
                    'channelId' => env('FIREBASE_CHANNEL_ID'),
                ];

                //subscribe
                foreach ($vnms as $vnm) {
                    $recipient= $vnm->firebase_device_id;
                    $expo->subscribe($channelName, $recipient);
                }

                //通知
                $expo->notify([$channelName], $notification);

            } catch (\ExponentPhpSDK\Exceptions\ExpoException $e) {
                //送れなかった場合、ログだけ吐いとく
                \Log::info('[ExpoException]'.$e->getMessage());
            } catch (\ExponentPhpSDK\Exceptions\UnexpectedResponseException $e) {
                //送れなかった場合、ログだけ吐いとく
                \Log::info('[UnexpectedResponseException]'.$e->getMessage());
            } finally {
                //最終的にチャンネルID情報削除
                $expo->unsubscribe($channelName);
            }
        }
    }
}
