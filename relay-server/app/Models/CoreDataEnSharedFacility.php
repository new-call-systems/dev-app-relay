<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnSharedFacility extends Model
{
    protected $fillable = [
        "property_code",
        "cleaning_code",
        "guard_code",
        "fire_check_code",
        "parking_code",
        "delivery_box_code",
        "auto_lock_code",
        "pump_code",
        "elevator_code",
        "security_camera_watching_code",
        "construction_code",
        "construction_field_code",
        "plumbing_system_code",
        "electrical_facilities_code",
        "electric_code",
        "water_line_code",
        "wrecker_code",
        "cleaning_emergency_name",
        "guard_emergency_name",
        "fire_check_emergency_name",
        "parking_emergency_name",
        "delivery_box_emergency_name",
        "auto_lock_emergency_name",
        "pump_emergency_name",
        "elevator_emergency_name",
        "security_camera_emergency_name",
        "construction_emergency_name",
        "construction_field_emergency_name",
        "plumbing_system_emergency_name",
        "electrical_facilities_emergency_name",
        "electric_emergency_name",
        "water_line_emergency_name",
        "wrecker_emergency_name",
        "cleaning_name",
        "guard_name",
        "fire_check_name",
        "parking_name",
        "delivery_box_name",
        "auto_lock_name",
        "pump_name",
        "elevator_name",
        "security_camera_name",
        "construction_name",
        "construction_field_name",
        "plumbing_system_name",
        "electrical_facilities_name",
        "electric_name",
        "water_line_name",
        "wrecker_name",
    ];
}
