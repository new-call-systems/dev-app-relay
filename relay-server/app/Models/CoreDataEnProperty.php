<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnProperty extends Model
{
    protected $fillable = [
        "property_name",
        "property_code",
        "completion_date",
        "management_company_code",
        "company_name",
        "sticker_management_company_code",
        "sticker_management_company_tel",
        "parking_management_company_code",
        "parking_management_company_tel",
        "landmark",
        "management_room_tel",
        "work_day",
        "work_time",
        "manager_name",
        "manager_tel",
        "note",
        "address",
        "large_bike_contract_management_company_code",
        "large_bike_contract_company_tel",
        "company_code",
        "property_kana",
        "internet_general_guidance_code",
        "construction_company_code",
        "electric_equipment_code",
        "water_supply_and_drainage_facility_code",
        "internet_general_guidance",
        "construction_company",
        "electric_equipment",
        "water_supply_and_drainage_facility",
        "nearest_police_management_company_code",
        "nearest_police_management_contact",
    ];
}
