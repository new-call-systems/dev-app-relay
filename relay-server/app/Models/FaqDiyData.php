<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use Illuminate\Database\Eloquent\Model;

class FaqDiyData extends Model
{
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'faq_diy_datas';
    protected $primaryKey = ['faq_diy_id', 'management_company_id', 'resident_type'];
    public $incrementing = false;
}
