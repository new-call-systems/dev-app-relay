<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestFeedback extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'request_feedbacks';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_feedback_id';

    public $incrementing = true;
}
