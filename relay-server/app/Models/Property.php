<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'properties';

    // 主キーのカラム名（デフォルトは id なので）
    public $primaryKey = 'property_id';

    public $incrementing = false;

    protected $casts = [
        'extra'  => 'json',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(Property $property){
            $property->property_id = 'PR_'.$property->management_company_id.'_'.$property->property_code;
        });
    }
}
