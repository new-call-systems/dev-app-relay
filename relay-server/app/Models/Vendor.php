<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;
use Hidehalo\Nanoid\Client;

class Vendor extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'vendors';

    // 主キーのカラム名（デフォルトは id なので）
    public $primaryKey = 'vendor_id';

    public $incrementing = false;

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'work_areas' => \App\Casts\CustomArray::class,
        'work_weeks' => \App\Casts\CustomArray::class,
        'work_items' => \App\Casts\CustomArray::class,
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(Vendor $vendor){
            $client = new Client();
            $vendor->vendor_id = 'VN_'.$client->generateId(12, Client::MODE_DYNAMIC);
        });
    }
}
