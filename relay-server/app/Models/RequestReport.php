<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class RequestReport extends Model
{
    use OptimisticLockObserverTrait;
    use Sortable;

    // テーブル名
    protected $table = 'request_reports';
    protected $primaryKey = 'request_report_id';
    public $incrementing = true;

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * 一覧画面のソート対象カラム
     */
    public $sortable = [
        'reception_date',
        'action_date',
        'construction_amount',
    ];

    public function receptionDateSortable($query, $direction)
    {
        return
        $query
        ->orderBy('reception_date', $direction)
        ->orderBy('management_company_name')
        ->orderBy('property_name')
        ->orderBy('room_no');
    }

    public function actionDateSortable($query, $direction)
    {
        return
        $query
        ->orderBy('action_date', $direction)
        ->orderBy('management_company_name')
        ->orderBy('property_name')
        ->orderBy('room_no');
    }

    public function constructionAmountSortable($query, $direction)
    {
        return
        $query
        ->orderBy('construction_amount', $direction)
        ->orderBy('management_company_name')
        ->orderBy('property_name')
        ->orderBy('room_no');
    }
}
