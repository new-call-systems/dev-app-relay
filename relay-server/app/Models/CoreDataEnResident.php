<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnResident extends Model
{
    protected $fillable = [
        "property_code",
        "room_code",
        "key_unlock_service",
        "contract_date",
        "move_in_date",
        "move_out_date",
        "vendor_code",
        "move_out_declaration_date",
        "contractor_code",
        "resident_name",
        "cohabitant",
        "guarantor",
        "contractor",
        "guarantor_tel",
        "guarantor_address",
        "contractor_tel",
        "contractor_address",
        "en_club_id",
        "resident_tel",
        "resident_mobile",
        "resident_birthday",
        "resident_code",
        "resident_gender",
        "vehicle_info",
        "parking_code",
        "parking_contract_date",
        "parking_cancellation_date",
        "move_out_appointment_date",
        "parking_contractor_name",
        "parking_user_name",
        "parking_contractor_phone",
        "parking_contractor_mobile",
        "parking_user_phone",
        "parking_user_mobile",
        "resident_name_kana",
        "contractor_name_kana",
    ];
}
