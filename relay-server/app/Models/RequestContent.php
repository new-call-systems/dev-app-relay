<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestContent extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'request_contents';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_content_id';

    public $incrementing = true;
}
