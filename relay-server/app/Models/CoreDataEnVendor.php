<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnVendor extends Model
{
    protected $fillable = [
        'vendor_name',
        'tel_number',
        'post_number',
        'address',
        'business_hours',
        'emergency_tel_number1',
        'emergency_tel_number2',
        'vendor_code',
        'emergency_tel_number3',
        'vendor_kbn',
    ];
}
