<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnProprietaryFacility extends Model
{
    protected $fillable = [
        "key_code",
        "key_type",
        "warm_water_toilet_model",
        "ac_model",
        "gas_water_heater_model",
        "room_code",
        "property_code",
        "warm_water_toilet_model_1",
        "ac_model_2",
        "gas_water_heater_model_3",
        "cooking_equipment_model",
        "intercom_entrance_set_model",
        "bath_drying_unit",
        "bath_faucet_model",
        "vanity_model",
        "toilet_tank_model",
        "tv_reception_equipment",
        "satellite_broadcast",
        "free_internet",
        "kitchen_faucet_model",
        "home_delivery_manual",
        "pet_breeding",
        "cooking_equipment_model_4",
        "intercom_entrance_set_model_5",
        "bath_drying_unit_6",
        "bath_faucet_model_7",
        "vanity_model_8",
        "toilet_tank_model_9",
        "tv_reception_equipment_10",
        "satellite_broadcast_11",
        "free_internet_12",
        "kitchen_faucet_model_13",
        "home_delivery_manual_14"
    ];
}
