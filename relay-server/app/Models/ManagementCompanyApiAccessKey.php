<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class ManagementCompanyApiAccessKey extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'management_company_api_access_keys';
    protected $primaryKey = 'management_company_id';
    public $incrementing = false;

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
