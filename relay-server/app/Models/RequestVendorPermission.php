<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestVendorPermission extends Model
{
    use OptimisticLockObserverTrait;
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'request_vendor_permissions';
    protected $primaryKey = ['request_id', 'vendor_id'];
    public $incrementing = false;
}
