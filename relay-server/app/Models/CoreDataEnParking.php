<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoreDataEnParking extends Model
{
    protected $fillable = [
      "parking_code" ,
      "property_code",
      "key_number" ,
      "roof" ,
      "floor" ,
      "parking_remark" ,
      "total_length" ,
      "total_width" ,
      "total_height",
      "max_weight" ,
      "shape" ,
      "parking_cd"
    ];
}
