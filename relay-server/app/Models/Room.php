<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'rooms';

    // 主キーのカラム名（デフォルトは id なので）
    public $primaryKey = 'room_id';

    public $incrementing = false;

    protected $casts = [
        'extra'  => 'json',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(Room $room){
            $property = \App\Models\Property::find($room->property_id);
            if ($property) {
                $room->room_id = 'RM_'.$room->management_company_id.'_'.$property->property_code.'_'.$room->room_code;
            }
        });
    }
}
