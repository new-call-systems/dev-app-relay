<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestHandleUser extends Model
{
    use OptimisticLockObserverTrait;
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'request_handle_users';
    protected $primaryKey = ['request_id', 'user_id'];
    public $incrementing = false;
}
