<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestCancelReason extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'request_cancel_reasons';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_id';

    public $incrementing = false;
}
