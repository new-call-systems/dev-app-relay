<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class TimeSection extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'time_sections';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'time_section_id';

    protected $casts = [
        'valid_flg' => 'boolean'
    ];
}
