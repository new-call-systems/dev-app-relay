<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnRoom extends Model
{
    protected $fillable = [
        "room_code",
        "property_code",
        "support_code",
        "special_note",
        "parcel_locker_password",
        "layout",
        "owner_code",
        "management_type",
        "room_management_agency_code",
        "ssid",
        "ssid2",
        "encryption_key",
        "electricity_company_code",
        "water_company_code",
        "electricity_company_name",
        "water_company_name",
        "gas_company_name",
        "gas_company_contact",
    ];
}
