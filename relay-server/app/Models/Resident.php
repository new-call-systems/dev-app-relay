<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Spatie\Permission\Traits\HasRoles;
use Hidehalo\Nanoid\Client;

class Resident extends Authenticatable
{
    use HasRoles;
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'residents';

    // 主キーのカラム名（デフォルトは id なので）
    public $primaryKey = 'resident_id';

    public $incrementing = false;

    protected $casts = [
        'system_unavailable_flg' => 'boolean',
        'leave_flg' => 'boolean',
        'extra'  => 'json',
        'can_order' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'access_token',
        'token_expire',
        'refresh_token',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(Resident $resident){
            $resident->resident_id = 'RD_'.$resident->management_company_id.'_'.$resident->resident_code;
        });
    }

    /**
     * アクセストークン・リフレッシュトークン発行・登録
     */
    public function tokenIssue()
    {
        return \DB::transaction(function () {
            $client = new Client();
            $accessToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $refreshToken = $client->generateId(20, Client::MODE_DYNAMIC);
            $dt = Carbon::now();
            $this->access_token = hash('sha256', $accessToken);
            $this->refresh_token = hash('sha256', $refreshToken);
            $this->token_expire = $dt->addHour();
            $this->save();

            $obj = new \stdClass();
            $obj->access_token = $accessToken;
            $obj->refresh_token = $refreshToken;

            return $obj;
        });
    }

    /**
     * 通知処理
     * LINE通知できたらLINE通知のみ
     * できなかったらメール送信
     */
    public function sendMessage($message)
    {
        //LINE送信
        if ($this->sendLineMessage($message)) {
            return;
        }

        //メール送信
    }

    /**
     * LINEメッセージ送信処理
     */
    public function sendLineMessage($message)
    {
        if (!$this->line_user_id) {
            //LINE IDの登録がない場合は何もしない
            return false;
        }

        //LINE ID
        $lineUserId = $this->line_user_id;

        //管理会社LINE情報
        $mcli = \App\Models\ManagementCompanyLineInformation::find($this->management_company_id);
        $accessToken = $mcli->channel_access_token;

        //メッセージ送信処理
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '']);
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($message);
        $response = $bot->pushMessage($lineUserId, $textMessageBuilder);

        if (!$response->isSucceeded()) {
            $msg = '';
            foreach ($response->getJSONDecodedBody() as $eMsg) {
                $msg .= $eMsg;
            }
            \Log::info('[LINE Message Send Error] '.$msg.' [resident_id:'.$this->resident_id.']');
            return false;
        }

        return true;
    }
}
