<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoreDataEnFacility extends Model
{
    protected $fillable = [
        "housing_cd",
        "pamphlet_name",
        "housing_name",
        "housing_name1",
    ];
}
