<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestHandle extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'request_handles';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_handle_id';
}
