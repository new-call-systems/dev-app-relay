<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class RequestPreferableTime extends Model
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'request_preferable_times';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'request_preferable_time_id';

    protected $casts = [
        'selected' => 'boolean',
    ];
}
