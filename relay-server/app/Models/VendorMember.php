<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class VendorMember extends Authenticatable
{
    use HasRoles;
    use OptimisticLockObserverTrait;
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'vendor_members';
    protected $primaryKey = ['user_id', 'vendor_id'];
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'access_token',
        'token_expire',
        'refresh_token',
        'license_expire',
    ];

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'retire_flg' => 'boolean',
        'representative_flg' => 'boolean',
    ];

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        //return $this->{$this->getAuthIdentifierName()};
        return $this->user_id;
    }

    /**
     * 該当作業者にプッシュ通知を行う
     */
    public function pushNotification($title, $message)
    {
        if ($this->firebase_device_id) {
            //チャンネル名(固定?)
            $channelName = 'cocoen';

            //インスタンス生成
            $expo = \ExponentPhpSDK\Expo::normalSetup();

            //通知内容
            $notification = [
                'title' => $title,
                'body' => $message,
                'channelId' => env('FIREBASE_CHANNEL_ID'),
            ];

            //subscribe
            $expo->subscribe($channelName, $this->firebase_device_id);

            try {
                //通知
                $expo->notify([$channelName], $notification);
            } catch (\ExponentPhpSDK\Exceptions\ExpoException $e) {
                //送れなかった場合、ログだけ吐いとく
                \Log::info('[ExpoException]'.$e->getMessage());
            }
        }
    }

}
