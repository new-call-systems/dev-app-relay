<?php

namespace App\Models;

use App\Traits\CompositePrimaryKeyTrait;
use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Database\Eloquent\Model;

class ManagementCompanyMember extends Model
{
    use OptimisticLockObserverTrait;
    use CompositePrimaryKeyTrait;

    // テーブル名
    protected $table = 'management_company_members';
    protected $primaryKey = ['user_id', 'management_company_id'];
    public $incrementing = false;

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'representative_flg' => 'boolean',
    ];
}
