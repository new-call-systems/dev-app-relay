<?php

namespace App\Models;

use App\Traits\OptimisticLockObserverTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Hidehalo\Nanoid\Client;

class ManagementCompany extends Authenticatable
{
    use OptimisticLockObserverTrait;

    // テーブル名
    protected $table = 'management_companies';

    // プライマリーキー（指定なしだと「id」）
    protected $primaryKey = 'management_company_id';

    public $incrementing = false;

    /**
     * 日付へキャストする属性
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function(ManagementCompany $mc){
            $client = new Client();
            $mc->management_company_id = 'MC_'.$client->generateId(12, Client::MODE_DYNAMIC);
        });
    }

}
