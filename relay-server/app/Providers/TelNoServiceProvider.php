<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class TelNoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('tel_no', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^0\d{1,3}-\d{2,4}-\d{3,4}$/', $value) === 1;
        });
    }
}
