<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Database\Events\TransactionCommitted;
use Illuminate\Database\Events\TransactionRolledBack;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        //SSL化
        if (in_array(config('app.env'), ['production', 'staging'], true)) {
            $url->forceScheme('https');
        }

        // 実行クエリ
        \DB::listen(function ($query) {
            $sql = $query->sql;
            foreach ($query->bindings as $binding) {
                if (is_string($binding)) {
                    $binding = "'{$binding}'";
                } elseif ($binding === null) {
                    $binding = 'NULL';
                } elseif ($binding instanceof Carbon) {
                    $binding = "'{$binding->toDateTimeString()}'";
                }

                $sql = preg_replace("/\?/", $binding, $sql, 1);
            }
            \Log::debug($sql, ['time' => "{$query->time}ms"]);
        });

        // トランザクション開始・終了
        \Event::listen(TransactionBeginning::class, function ($event) {
            \Log::debug('START TRANSACTION');
        });

        \Event::listen(TransactionCommitted::class, function ($event) {
            \Log::debug('COMMIT');
        });

        \Event::listen(TransactionRolledBack::class, function ($event) {
            \Log::debug('ROLLBACK');
        });
    }
}
