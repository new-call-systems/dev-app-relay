<?php

namespace App\Providers;

use Illuminate\Support\Carbon;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Support\Arrayable;

class CustomMcProvider extends EloquentUserProvider  {


    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        //$query = $this->newModelQuery();
        $query = \App\Models\ManagementCompanyApiAccessKey::query();

        foreach ($credentials as $key => $value) {
            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }
        $mcak = $query->first();

        $mc = null;
        if ($mcak) {
            $mc = \App\Models\ManagementCompany::find($mcak->management_company_id);
        }

        return $mc;
    }
}