<?php

namespace App\Providers;

use App\Auth\CustomTokenGuard;
use App\Auth\CustomSessionGuard;
use App\Auth\CustomMcGuard;
use App\Providers\CustomTokenProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->app['auth']->extend('custom_session', function($app, $name, array $config) {
            $provider = Auth::createUserProvider($config['provider']);
            $guard = new CustomSessionGuard($name, $provider, $app['session.store']);
            if (method_exists($guard, 'setCookieJar')) {
                $guard->setCookieJar($app['cookie']);
            }

            if (method_exists($guard, 'setDispatcher')) {
                $guard->setDispatcher($app['events']);
            }

            if (method_exists($guard, 'setRequest')) {
                $guard->setRequest($app->refresh('request', $guard, 'setRequest'));
            }

            return $guard;
        });

        $this->app['auth']->extend('custom_token', function($app, $name, array $config) {
            return new CustomTokenGuard(Auth::createUserProvider($config['provider']), $app['request']);
        });

        $this->app['auth']->provider('custom_token_provider', function($app, array $config) {
            return new CustomTokenProvider($app['hash'], $config['model']);
        });

        $this->app['auth']->extend('custom_mc', function($app, $name, array $config) {
            return new CustomMcGuard(Auth::createUserProvider($config['provider']), $app['request']);
        });

        $this->app['auth']->provider('custom_mc_provider', function($app, array $config) {
            return new CustomMcProvider($app['hash'], $config['model']);
        });
    }
}
