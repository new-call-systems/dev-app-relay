<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class MobileNoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('mobile_no', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^0\d0-\d{4}-\d{4}$/', $value) === 1;
        });
    }
}
