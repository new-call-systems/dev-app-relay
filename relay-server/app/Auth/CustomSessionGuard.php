<?php
namespace App\Auth;

use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\Auth\UserProvider;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class CustomSessionGuard extends SessionGuard {

    /**
     * Determine if the user matches the credentials.
     *
     * @param  mixed  $user
     * @param  array  $credentials
     * @return bool
     */
    protected function hasValidCredentials($user, $credentials)
    {
        $validated = ! is_null($user) && $this->provider->validateCredentials($user, $credentials);

        if ($user === null) {
            //ユーザーがいない
            $validated = false;

        } else {
            //ここでパーミッション・ロールを聞いてログインをはじく
            switch ($this->name) {

                case 'guard_system_owner':
                    if (!$user->hasPermissionTo(config('const.Permissions.SYSTEM_OWNER'))) {
                        $validated = false;
                    }
                    break;

                case 'guard_vendor_manager':
                    if (!$user->hasPermissionTo(config('const.Permissions.VENDOR_MANAGER'))) {
                        $validated = false;
                    } else {
                        $vnm = \App\Models\VendorMember::where('user_id', $user->user_id)->first();
                        if (!$vnm || $vnm->retire_flg) {
                            $validated = false;
                        }
                    }
                    break;

                case 'guard_management_company_member':
                    if (!$user->hasPermissionTo(config('const.Permissions.MANAGEMENT_COMPANY_MEMBER'))) {
                        $validated = false;
                    }
                    break;
            }
        }

        if ($validated) {
            $this->fireValidatedEvent($user);
        }

        return $validated;
    }
}