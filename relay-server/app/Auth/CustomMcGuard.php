<?php
namespace App\Auth;

use Illuminate\Auth\TokenGuard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class CustomMcGuard extends TokenGuard {

    /**
     * Get the currently authenticated user.
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (! is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $token = $this->getTokenForRequest();

        if (! empty($token)) {
            $aryCredentials = preg_split('/:/', $token);
            if (count($aryCredentials) === 2) {
                $user = $this->provider->retrieveByCredentials([
                    'access_key_id' => $aryCredentials[0],
                    'secret_key' => $aryCredentials[1],
                ]);
            }
        }

        return $this->user = $user;
    }

    /**
     * Get the token for the current request.
     *
     * @return string
     */
    public function getTokenForRequest()
    {
        $token = $this->request->query($this->inputKey);

        if (empty($token)) {
            $token = $this->request->input($this->inputKey);
        }

        if (empty($token)) {
            $token = $this->request->bearerToken();
        }

        if (empty($token)) {
            $authHeader = $this->request->header('Authorization');
            $aryHeader = preg_split('/ /', $authHeader);
            if (count($aryHeader) === 2 && $aryHeader[0] === 'Basic') {
                $credentials = base64_decode($aryHeader[1]);
                $aryCredentials = preg_split('/:/', $credentials);
                if (count($aryCredentials) === 2) {
                    $token = $credentials;
                }
            }
        }

        if (empty($token)) {
            $token = $this->request->getPassword();
        }

        return $token;
    }

}