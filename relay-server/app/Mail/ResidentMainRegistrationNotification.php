<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResidentMainRegistrationNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $loginId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $loginId)
    {
        $this->name = $name;
        $this->loginId = $loginId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('mail.resident_main_registration')
                    ->subject('【Coco-en】登録完了についてのお知らせ')
                    ->with([
                        'name' => $this->name,
                        'login_id' => $this->loginId,
                    ]);
    }
}
