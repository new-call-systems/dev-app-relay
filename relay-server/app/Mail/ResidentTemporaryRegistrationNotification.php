<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResidentTemporaryRegistrationNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $code)
    {
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('mail.resident_temporary_registration')
                    ->subject('【Coco-en】仮登録完了および本登録についてのお知らせ')
                    ->with([
                        'name' => $this->name,
                        'code' => $this->code,
                    ]);
    }
}
