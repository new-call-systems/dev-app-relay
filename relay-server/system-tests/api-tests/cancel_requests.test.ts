import * as chai from 'chai';
import * as chaiAsPromised from "chai-as-promised";
import * as dayjs from "dayjs";

import loadServices from "../src/services"
import * as utils from "../src/libs/utils";
//import loadConfig from "../libs/load-config";
import * as rdInterfaces from "../src/interfaces/resident";
import * as comInterfaces from "../src/interfaces/common";
import * as vnInterfaces from "../src/interfaces/vendor";

chai.use(chaiAsPromised);
const { expect } = chai
const logger = require("mocha-logger");

const { apiClient, residentApiClient } = loadServices();

const dummyVendorMember = {
    email: process.env.VENDOR_MEM_EMAIL,
    password: process.env.VENDOR_MEM_PASSWORD,
}

let residentToken = "";
let vendorMemToken = "";
let currentRequestId = "";
let timeSections = [] as comInterfaces.ITimeSection[];
let emptyRequestNotEmergency = undefined as rdInterfaces.IFormRequestRes | undefined;
let emptyRequestIsEmergency = undefined as rdInterfaces.IFormRequestRes | undefined;


/***/
const maskedText = "********";

describe('Resident Login', () => {
    let otp = "";
    it('Get one time token with dummy data', async () => {
        const res = await residentApiClient.login();
        logger.log(`Get otp: ${res}`)
        otp = res;
        expect(otp).to.be.a("number");
    });
    it('Auth from one time token', async () => {
        const res = await residentApiClient.loginWithCode(otp);
        expect(res.access_token).to.be.a("string");
        expect(res.refresh_token).to.be.a("string");
        residentToken = utils.createAccessToken(res.access_token);
    });
});

describe( "Request from context and cancel", () => {
    it( "Get time sections" , async() => {
        const res = await apiClient.getJson<comInterfaces.ITimeSection[]>( "/rd/v1/timeSection", {}, residentToken );
        expect(res).be.a("array");
        timeSections = res;
    })
    it( "Get question answer", async () => {
        const reqText = "エアコンから水が漏れる"
        const res = await apiClient.postJson<rdInterfaces.IFormRequestRes>( "/rd/v1/request", { context : reqText }, residentToken );
        expect(res.request_id).to.be.a("string");
        expect(res.request_type).to.be.a("string");
        emptyRequestNotEmergency = res
    } )
    it( "Order request ", async() => {
       if( emptyRequestNotEmergency?.request_type === "vendor" ){
           const today = dayjs( new Date() );
           const date = today.add(1, 'day').format( 'YYYY-MM-DD' );
           const datetimes = [{
                date: date,
                time_section_id: timeSections[0].time_section_id
           },]
           await apiClient.postJson<any>( `/rd/v1/request/${emptyRequestNotEmergency.request_id}/order`, { emergency_flg: false, preferable_times: datetimes }, residentToken );
       };
    } );
    it( "Cancel request", async() => {
        const requestId = emptyRequestNotEmergency?.request_id || ""
        const request = await residentApiClient.getRequest(requestId,residentToken);
        expect(request.status).to.be.equal("open")
        await residentApiClient.cancelRequest(requestId,residentToken);
        const updatedRequest = await residentApiClient.getRequest(requestId,residentToken);
        expect(updatedRequest.status).to.be.equal("cancelled")
    })
} );

describe( "Request from context", () => {
    it( "Get time sections" , async() => {
        const res = await apiClient.getJson<comInterfaces.ITimeSection[]>( "/rd/v1/timeSection", {}, residentToken );
        expect(res).be.a("array");
        timeSections = res;
    })
    it( "Get question answer", async () => {
        const reqText = "エアコンから水が漏れる"
        const res = await apiClient.postJson<rdInterfaces.IFormRequestRes>( "/rd/v1/request", { context : reqText }, residentToken );
        expect(res.request_id).to.be.a("string");
        expect(res.request_type).to.be.a("string");
        emptyRequestNotEmergency = res
    } )
    it( "Order request ", async() => {
       if( emptyRequestNotEmergency?.request_type === "vendor" ){
           const today = dayjs( new Date() );
           const date = today.add(1, 'day').format( 'YYYY-MM-DD' );
           const datetimes = [{
                date: date,
                time_section_id: timeSections[0].time_section_id
           },]
           await apiClient.postJson<any>( `/rd/v1/request/${emptyRequestNotEmergency.request_id}/order`, { emergency_flg: false, preferable_times: datetimes }, residentToken );
       };
    } );
} );

describe( "Order from vendor", () => {
    it( "Login", async () => {
        const res = await apiClient.postJson<vnInterfaces.IVendorAuthInfos>( `/vn/v1/auth/login`, dummyVendorMember, "" );
        expect(res.access_token).to.be.a("string");
        expect(res.refresh_token).to.be.a("string");
        expect(res.vendor_member.user_id).to.be.a("number");
        expect(res.vendor_member.first_name).to.be.a("string");
        vendorMemToken = utils.createAccessToken(res.access_token);
    } );
    it( "Bit request.", async () => {
        if( emptyRequestNotEmergency?.request_type === "vendor" ){
            const requestFormResident = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
            await apiClient.putJson<comInterfaces.IRequestsFromResident>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}/bid`, { request_preferable_time_id: requestFormResident.preferable_info[0].request_preferable_time_id }, vendorMemToken );
            const changedInfos = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
            expect(changedInfos.status).to.be.equal(comInterfaces.RequestStatusType.scheduling);
            expect(changedInfos.resident_name).to.not.equal(maskedText);
            expect(changedInfos.resident_tel).to.not.equal(maskedText);
            expect(changedInfos.property_address).to.not.equal(maskedText);
            expect(changedInfos.property_name).to.not.equal(maskedText);
            expect(changedInfos.room_no).to.not.equal(maskedText);
        };
    } );
} )

describe("Cancel request after bid", () => {
    it( "Error calcel request that status is not waiting for cancel from vendor ", async() => {
        const requestId = emptyRequestNotEmergency?.request_id || "";
        const err = async () => await apiClient.postJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}/cancel`, {}, vendorMemToken );
        expect(err()).to.be.rejected;
    } )
    it( "waiting for cancel", async() => {
        const requestId = emptyRequestNotEmergency?.request_id || ""
        const request = await residentApiClient.getRequest(requestId,residentToken);
        expect(request.status).to.be.equal(comInterfaces.RequestStatusType.scheduling)
        await residentApiClient.cancelRequest(requestId,residentToken);
        const updatedRequest = await residentApiClient.getRequest(requestId,residentToken);
        expect(updatedRequest.status).to.be.equal(comInterfaces.RequestStatusType.waiting_for_cancel);
    })
    it( "Ensure get requests that status is wating for cancel from vendor.", async () => {
        const res = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { status: comInterfaces.RequestStatusType.waiting_for_cancel }, vendorMemToken );
        expect(res.data).be.a("array");
        res.data.forEach( request => {
            expect(request.status).to.be.equal(comInterfaces.RequestStatusType.waiting_for_cancel)
        })
        const unit = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
        expect(unit.status).to.be.equal(comInterfaces.RequestStatusType.waiting_for_cancel)
    } );
    it( "apply cancel from vendor", async() => {
        await apiClient.postJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}/cancel`, {}, vendorMemToken );
        const unit = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
        expect(unit.status).to.be.equal(comInterfaces.RequestStatusType.cancelled)
        expect(unit.resident_name).to.be.equal(maskedText);
        expect(unit.resident_tel).to.be.equal(maskedText);
        expect(unit.property_address).to.be.equal(maskedText);
        expect(unit.property_name).to.be.equal(maskedText);
        expect(unit.room_no).to.be.equal(maskedText);
    } )
})
