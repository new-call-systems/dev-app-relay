import { expect } from 'chai';
import {} from "chai-as-promised";
import * as dayjs from "dayjs";

import loadServices from "../src/services"
import * as utils from "../src/libs/utils";

import * as rdInterfaces from "../src/interfaces/resident";
import { ITestOutput } from "../src/services/interfaces/iload-test-cases";


const logger = require("mocha-logger");

const { apiClient, residentApiClient, loadTestCases } = loadServices();

let residentToken = "";

/***/
const maskedText = "********";

describe('Resident Login', () => {
    let otp = "";
    it('Get one time token with dummy data', async () => {

        const res = await residentApiClient.login();
        logger.log(`Get otp: ${res}`)
        otp = res;
        expect(otp).to.be.a("number");
    });
    it('Auth from one time token', async () => {
        const res = await residentApiClient.loginWithCode(otp);
        expect(res.access_token).to.be.a("string");
        expect(res.refresh_token).to.be.a("string");
        residentToken = utils.createAccessToken(res.access_token);
    });

    after( function() {
        return loadTestCases.load( { useCase1: true } ).then( testData => {
            const cachedResult: ITestOutput[] = []
            describe('DIY test case 1', function() {
                for( const data of testData ){
                    if( !data.AI判定 ) continue;
                    const context = data["TL1テストケース（問合せ内容）"] //: data["TL2テストケース（問合せパターン）"]
                    it( context, async function(){
                        const res = await apiClient.postJson<rdInterfaces.IFormRequestRes,any>( "/rd/v1/request", { context: context }, residentToken  )
                        expect(res.request_id).to.be.a("string");
                        cachedResult.push({ case: data, api: res })
                    });
                }
                it( "Write result to excel", async() => {
                    await loadTestCases.writeResult( "TL1", ...cachedResult )
                } )
            });
        })
    });
});

it('This is a required placeholder to allow before() to work', function () {
    console.log('Mocha should not require this hack IMHO');
});