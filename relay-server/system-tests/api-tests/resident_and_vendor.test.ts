import { expect } from 'chai';
import {} from "chai-as-promised";
import * as dayjs from "dayjs";

import loadServices from "../src/services"
import * as utils from "../src/libs/utils";
//import loadConfig from "../libs/load-config";
import * as rdInterfaces from "../src/interfaces/resident";
import * as comInterfaces from "../src/interfaces/common";
import * as vnInterfaces from "../src/interfaces/vendor";

const logger = require("mocha-logger");

const { apiClient, residentApiClient } = loadServices();

const dummyVendorMember = {
    email: process.env.VENDOR_MEM_EMAIL,
    password: process.env.VENDOR_MEM_PASSWORD,
}


let residentToken = "";
let vendorMemToken = "";
let currentRequestId = "";
let timeSections = [] as comInterfaces.ITimeSection[];
let emptyRequestNotEmergency = undefined as rdInterfaces.IFormRequestRes | undefined;
let emptyRequestIsEmergency = undefined as rdInterfaces.IFormRequestRes | undefined;


/***/
const maskedText = "********";

describe('Resident Login', () => {
    let otp = "";
    it('Get one time token with dummy data', async () => {

        const res = await residentApiClient.login();
        logger.log(`Get otp: ${res}`)
        otp = res;
        expect(otp).to.be.a("number");
    });
    it('Auth from one time token', async () => {
        const res = await residentApiClient.loginWithCode(otp);
        expect(res.access_token).to.be.a("string");
        expect(res.refresh_token).to.be.a("string");
        residentToken = utils.createAccessToken(res.access_token);
    });
});

describe( "Request from context", () => {
    it( "Get time sections" , async() => {
        const res = await apiClient.getJson<comInterfaces.ITimeSection[]>( "/rd/v1/timeSection", {}, residentToken );
        expect(res).be.a("array");
        timeSections = res;
    })
    it( "Get question answer", async () => {
        const reqText = "エアコンから水が漏れる"
        const res = await apiClient.postJson<rdInterfaces.IFormRequestRes>( "/rd/v1/request", { context : reqText }, residentToken );
        expect(res.request_id).to.be.a("string");
        expect(res.request_type).to.be.a("string");
        emptyRequestNotEmergency = res
    } )
    it( "Get request list", async () => {
        const res = await apiClient.getJson<comInterfaces.IRequestsFromResident[]>( "/rd/v1/request", {}, residentToken );
        const res2 = await apiClient.getJson<comInterfaces.IRequestsFromResident[]>( "/rd/v1/request", {}, residentToken );
        //expect(res).be.a("array");
        //console.log( "aaaaaaaaaaaaaa", res.length )
        /*
        res.forEach( request => {
            expect(request.request_id).to.be.a("number");
            expect(request.ai_category).to.be.a("string");
            expect(request.ai_sub_category).to.be.a("string");
        })
        */
    } )
    it( "Get unit request. Status is waiting for open", async () => {
        const res = await apiClient.getJson<comInterfaces.IRequestsFromResident>( `/rd/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, residentToken );
        expect(res.status).to.be.equal("waiting_for_open")
    } )
    it( "Order request ", async() => {
       if( emptyRequestNotEmergency?.request_type === "vendor" ){
           const today = dayjs( new Date() );
           const date = today.add(1, 'day').format( 'YYYY-MM-DD' );
           const datetimes = [{
                date: date,
                time_section_id: timeSections[0].time_section_id
           },]
           const res = await apiClient.postJson<any>( `/rd/v1/request/${emptyRequestNotEmergency.request_id}/order`, { emergency_flg: false, preferable_times: datetimes }, residentToken );
       };
    } );
    it( "Get unit request. Status is open", async () => {
        if( emptyRequestNotEmergency?.request_type === "vendor" ){
            const res = await apiClient.getJson<comInterfaces.IRequestsFromResident>( `/rd/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, residentToken );
            expect(res.status).to.be.equal("open")
        };
    } );
} );

describe( "Order from vendor", () => {
    it( "Login", async () => {
        const res = await apiClient.postJson<vnInterfaces.IVendorAuthInfos>( `/vn/v1/auth/login`, dummyVendorMember, "" );
        expect(res.access_token).to.be.a("string");
        expect(res.refresh_token).to.be.a("string");
        expect(res.vendor_member.user_id).to.be.a("number");
        expect(res.vendor_member.first_name).to.be.a("string");
        vendorMemToken = utils.createAccessToken(res.access_token);
    } );
    it( "Get requests.", async () => {
        const res = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, {}, vendorMemToken );
        expect(res.data).be.a("array");
        res.data.forEach( request => {
            expect(request.status).to.be.oneOf([ comInterfaces.RequestStatusType.open, comInterfaces.RequestStatusType.scheduling, comInterfaces.RequestStatusType.waiting_for_cancel, comInterfaces.RequestStatusType.cancelled ])
        })
    } );
    it( "Get unit request.", async () => {
        if( emptyRequestNotEmergency?.request_type === "vendor" ){
            const res = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
            expect(res.status).to.be.equal("open");
            expect(res.resident_name).to.be.equal(maskedText);
            expect(res.resident_tel).to.be.equal(maskedText);
            expect(res.property_address).to.be.equal(maskedText);
            expect(res.property_name).to.be.equal(maskedText);
            expect(res.room_no).to.be.equal(maskedText);
        };
    } );
    it( "Bit request.", async () => {
        if( emptyRequestNotEmergency?.request_type === "vendor" ){
            const requestFormResident = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
            await apiClient.putJson<comInterfaces.IRequestsFromResident>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}/bid`, { request_preferable_time_id: requestFormResident.preferable_info[0].request_preferable_time_id }, vendorMemToken );
            const changedInfos = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestNotEmergency?.request_id}`, {}, vendorMemToken );
            expect(changedInfos.status).to.be.equal("scheduling");
            expect(changedInfos.resident_name).to.not.equal(maskedText);
            expect(changedInfos.resident_tel).to.not.equal(maskedText);
            expect(changedInfos.property_address).to.not.equal(maskedText);
            expect(changedInfos.property_name).to.not.equal(maskedText);
            expect(changedInfos.room_no).to.not.equal(maskedText);
        };
    } );
} )


describe( "Request from context is emergency", () => {
    it( "Get question answer", async () => {
        const reqText = "エアコンから水が漏れる"
        const res = await apiClient.postJson<rdInterfaces.IFormRequestRes>( "/rd/v1/request", { context : reqText }, residentToken );
        expect(res.request_id).to.be.a("string");
        expect(res.request_type).to.be.a("string");
        emptyRequestIsEmergency = res
    } )
    it( "Order request ", async() => {
       if( emptyRequestIsEmergency?.request_type === "vendor" ){
           await apiClient.postJson<any>( `/rd/v1/request/${emptyRequestIsEmergency.request_id}/order`, { emergency_flg: true }, residentToken );
       };
    } );
} );

describe( "Order request that emergency is true ", () => {
    it( "Bit request.", async () => {
        if( emptyRequestNotEmergency?.request_type === "vendor" ){
            const requestFormResident = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestIsEmergency?.request_id}`, {}, vendorMemToken );
            await apiClient.putJson<comInterfaces.IRequestsFromResident>( `/vn/v1/request/${emptyRequestIsEmergency?.request_id}/bid`, {}, vendorMemToken );
            const changedInfos = await apiClient.getJson<comInterfaces.IRequestsForVendorMember>( `/vn/v1/request/${emptyRequestIsEmergency?.request_id}`, {}, vendorMemToken );
            expect(changedInfos.status).to.be.equal("scheduling");
        };
    } );
} )

describe("Get requests from vendor with filters", () => {
    it( "filter by status.", async () => {
        const open = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { status: "open" }, vendorMemToken );
        open.data.forEach( request => {
            expect(request.status).to.be.equal("open")
        })
        const scheduling = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { status: "scheduling" }, vendorMemToken );
        scheduling.data.forEach( request => {
            expect(request.status).to.be.equal("scheduling")
        })
        const completed = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { status: "completed" }, vendorMemToken );
        completed.data.forEach( request => {
            expect(request.status).to.be.equal("completed")
        })
    } );
    it( "filter by multi status.", async () => {
        const openAndScheduling = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { status: ["open", "scheduling"].join(",") }, vendorMemToken );
        openAndScheduling.data.forEach( request => {
            expect(request.status).to.be.oneOf(["open", "scheduling"])
        })
    } );
    it( "filter by date.", async () => {
        const today = dayjs( new Date() );
        const date = today.add(1, 'day').format( 'YYYY-MM-DD' );
        const dateFilterRes = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { date: date }, vendorMemToken );
        dateFilterRes.data.forEach( request => {
           // expect(request.status).to.be.oneOf(["open", "scheduling"])
        })
    } );
    it( "filter by only emergency flg", async () => {
        const emergencyFilterRes = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { emergency_flg: "only" }, vendorMemToken );
        console.log( emergencyFilterRes )
        emergencyFilterRes.data.forEach( request => {
           // expect(request.status).to.be.oneOf(["open", "scheduling"])
        })
    } );
    it( "filter by exclude emergency flg", async () => {
        const emergencyFilterRes = await apiClient.getJson<comInterfaces.IRequestsForVendorMemberApiRes>( `/vn/v1/request`, { emergency_flg: "exclude" }, vendorMemToken );
        console.log( emergencyFilterRes )
        emergencyFilterRes.data.forEach( request => {
           // expect(request.status).to.be.oneOf(["open", "scheduling"])
        })
    } );

} )
