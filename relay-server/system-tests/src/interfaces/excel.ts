
import * as dayjs from "dayjs";

export const TestCaseMaster = {
    'サブカテゴリ#': 'sub_cat_id',
    'サブカテゴリ': 'sub_cat',
    'TL1テストケース#': 'TL1',
    'TL1テストケース（問合せ内容）': 'TL1_context',
    'TL2テストケース#': 'TL2',
    'TL2テストケース（問合せパターン）': 'TL2_context',
    '案件区分': 'category',
    'AI判定': 'ai_answer',
    'AI機能（参考）': 'ai_responce',
    'FAQ番号': 'FAQ_no',
    'DIY番号': 'DIY_no',
    '業者(えん様Ver)': 'support_en',
    '業者(他管理会社)': 'anyway',
    'データの流れ（ストーリー）': 'data_story',
    '備考': 'note'
} as const;

export const AIAnswerExcel = {
    '自動回答or管理会社対応': ['support_center', 'management_company' ],
    '業者出動': ['vendor'],
    '管理会社対応': ['management_company'],
    '管理会社対応(業者出動)': ['management_company', 'communal_area'],
    '自動回答FAQ': ['support_center'],
    '自動回答': ['support_center'],
} as const;
export type AIAnswerExcelType = keyof typeof AIAnswerExcel;

export const ExcelCategory = {
    '専有部': '',
    '共用部or専有部': '',
    '専OR共': '',
    '契約関係': '',
    '共用部': '',
};
export type ExcelCategoryType = keyof typeof ExcelCategory;
export const common_area_excel_category = ["共用部"] as const;


let startRow = 46

export const TEST_CASE_EXCEL_OUTPUT_ROW = {
    Classify_TL1: { pass: startRow, sum: startRow += 1 },
    FAQ_TL1: { pass: startRow += 2, sum: startRow += 1 },
    DIY_TL1: { pass: startRow += 2, sum: startRow += 1 },
    Classify_TL2: { pass: startRow += 2, sum: startRow += 1 },
    FAQ_TL2: { pass: startRow += 2, sum: startRow += 1 },
    DIY_TL2: { pass: startRow += 2, sum: startRow += 1 },
} as const;

/*
export const TEST_CASE_EXCEL_OUTPUT_ROW = {
    Classify_TL1: { pass: 2, sum: 3 },
    FAQ_TL1: { pass: 5, sum: 6 },
    DIY_TL1: { pass: 8, sum: 9 },
    Classify_TL2: { pass: 11, sum: 12 },
    FAQ_TL2: { pass: 14, sum: 15 },
    DIY_TL2: { pass: 17, sum: 18 },
} as const;
*/

export const TEST_CASE_OUTPUT_START_COLUMN = 3;
export const TEST_START_DAY = dayjs("2020-09-16");

export const TL1_RESULT_SHEET_NAME = "";
export type TEST_RESULT_CASE = "TL1" | "TL2";

export type TEST_CASES = keyof typeof TEST_CASE_EXCEL_OUTPUT_ROW;


export const COMMON_AREA = "共用部";
