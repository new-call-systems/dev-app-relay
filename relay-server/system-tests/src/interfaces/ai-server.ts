

export type AiCategoryType = "vendor" | "support_center" | "communal_area" | "management_company";

export interface IAiServerResponce {
    ai_category: AiCategoryType;
    may_unknown: boolean;
    confidence: number;
    all_classes_probabilities: string;
    specific: ISpecific<ISpecificFAQAnswer> | null
};

export interface ISpecific<T> {
    answer: T;
    case_ids:string[];
};

export interface ISpecificFAQAnswer {
    answer: string;
    solution: string;
    case_id: string;
    empty_label: string;
    label: string;
    support_center: string;
    table: string;
    template: string;
    type: string;    
}

export const AUTO_RESPONCE_TYPES: Exclude<AiCategoryType, "vendor">[] = [ "support_center", "communal_area", "management_company"];
export const ALL_AI_CATEGORIES: AiCategoryType[] = [ "vendor", "support_center", "communal_area", "management_company"];