
export enum RequestStatusType {
    "waiting_for_open" = "waiting_for_open",
    "open" = "open",
    "scheduling" = "scheduling",
    "waiting_for_cancel" = "waiting_for_cancel",
    "cancelled" = "cancelled"
}


export interface ITimeSection {
    time_section_id: string;
    label: string;
};


export interface IRequestsFromResident {
    request_id: string;
    status: string;
    ai_category: string;
    ai_sub_category: string;
};

export interface IRequestsForVendorMember extends IRequestsFromResident {
    resident_name: string;
    resident_tel: string;
    property_name: string;
    property_address: string;
    room_no: string;
    context: string;
    response: string;
    preferable_info: IPreferableInfos[];
};

export interface IPreferableInfos {
    request_preferable_time_id: string;
    date: string;
    time_section_id: string;
    selected: string;
};

export interface IRequestsForVendorMemberApiRes {
    data: IRequestsForVendorMember[];
}