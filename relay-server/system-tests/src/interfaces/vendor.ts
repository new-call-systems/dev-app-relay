

export interface IVendorAuthInfos {
    access_token: string;
    refresh_token: string;
    vendor_member: IVendorMemberInfos;
};

export interface IVendorMemberInfos {
    user_id: number;
    vendor_id: string;
    last_name: string;
    first_name: string;
    emergency_tel: string;
    retire_flg: null | number;
    representative_flg: number;
};