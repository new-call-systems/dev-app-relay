

export interface IAuthInfos {
    access_token: string,
    refresh_token: string,
    resident: { 
        resident_id: string,
        management_company_id: string,
        room_id: string,
        resident_code: string
        name: string
        tel: string
        created_at: string
        updated_at: string
    };
};

export interface IFormRequestRes {
    request_id: string;
    request_type: string;
};

