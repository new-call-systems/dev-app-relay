


export interface IApiClientConfig {
    baseUrl: string;
    apiPath: string;
    residentInfos: IResidentInfos;
};

export interface IResidentInfos {
    resident_code: string;
    property_code: string;
    room_code: string;
};

const baseUrl = process.env.BASE_URL || "";
const apiPath = process.env.API_PATH || "";

const dummyResident: IResidentInfos = {
    resident_code: process.env.ResidentCode ||"",
    property_code: process.env.PropertyCode ||"",
    room_code: process.env.RoomCode ||""
};

export function loadConfig(){
    return {
        baseUrl, apiPath, residentInfos: dummyResident
    } as IApiClientConfig;
};

export const emptyConfig: IApiClientConfig = {
    baseUrl: "",
    apiPath: "",
    residentInfos: {
        resident_code: "",
        property_code: "",
        room_code: ""
    }
};



export default loadConfig;