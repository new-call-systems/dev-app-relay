
import { Base64 } from "js-base64";

export function createMCBasicToken(){
    const secretKey = [process.env.AccessKeyId, process.env.SecretKey].join(":")
    const token = Base64.btoa(secretKey);
    return `Basic ${token}`
};
export function createAccessToken( accessToken: string ){
    return `Bearer ${accessToken}`;
};