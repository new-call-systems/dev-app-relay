
import * as exceljs from "exceljs";
import * as fs from "fs-extra";
import * as xlsx from "xlsx-populate";
import * as dayjs from "dayjs"

import { IDependencies, ILoadTestCases, ILoadTestCasesConfig, ITestCaseResource, ITestCase, ILoadOptions, ITestOutput } from "./interfaces/iload-test-cases";
import { TEST_CASES, TEST_CASE_EXCEL_OUTPUT_ROW, TEST_CASE_OUTPUT_START_COLUMN, TEST_START_DAY, TEST_RESULT_CASE } from "../interfaces/excel";

class LoadTestCases implements ILoadTestCases {
    _resource: ITestCaseResource;
    _startRow = 3;
    _startColumn = 2;

    _excel: exceljs.Workbook;
    _workbook: exceljs.Workbook | null = null;

    constructor( config: ILoadTestCasesConfig ){
        this._resource = config.test_case
        this._excel = new exceljs.Workbook();
    };

    inject( s: IDependencies ) { /** nothing */ }

    async load(opt?: Partial<ILoadOptions> ): Promise<ITestCase[]> {
        const workbookFromExceljs = await this._excel.xlsx.readFile( this._resource.path );
        this._workbook = workbookFromExceljs;
        const header: string[] = [];
        const arr: ITestCase[] = [];
        let maxColumnNum = 0;

        for( const worksheet of workbookFromExceljs.worksheets) {
            if( worksheet.name !== this._resource.sheetName ) continue;
            worksheet.eachRow( (row, number) => {
                if( this._startRow === number ){
                    row.eachCell( (cell, cellNumber ) => {
                        header.push( cell.text.replace("\n", "") )
                        if( maxColumnNum < cellNumber ) maxColumnNum = cellNumber;
                    })
                }
                if( this._startRow < number ){
                    arr.push( this.__convertObjectFromRow(row, header, maxColumnNum ) )
                };
            } )
        };

        if( opt && opt.useCase1 ){
            const case1Arr = this.__filterUniqueCase1(arr)
            return case1Arr
        };
        return arr
    };

    async writeStats( type: TEST_CASES, sumCount: number, passedCount: number ){
        const resSheetName = this._resource.resultSheet;
        const excel = await xlsx.fromFileAsync(this._resource.path);
        let sheet = excel.sheet(resSheetName);

        if( !sheet ) sheet = excel.addSheet(resSheetName);
        const targetCokumn = this.__calcUseCol();
        const { pass, sum } = TEST_CASE_EXCEL_OUTPUT_ROW[type];
        const resSheet = excel.sheet(this._resource.resultSheet);
        
        resSheet.column(targetCokumn).cell(pass).value(passedCount);
        resSheet.column(targetCokumn).cell(sum).value(sumCount);

        await excel.toFileAsync(this._resource.path)
    };

    async writeResult(type: TEST_RESULT_CASE, ...results: ITestOutput[] ){
        const excel = await xlsx.fromFileAsync(this._resource.path);
        let startRow = 6;
        const sheet = type === "TL1" ? excel.sheet("テスト表 (TL1 C1 業者対応)") :
        excel.sheet("テスト表 (TL2)") ;

        for( const res of results ){
            let startCol = 2;
            const rowValue = this.__formatExcelTestResult(type, res);
            for( const val of rowValue ){
                sheet.row(startRow).cell(startCol).value(val);
                ++startCol
            };
            ++startRow
        };
        await excel.toFileAsync(this._resource.path)
    };

    __formatExcelTestResult( type: TEST_RESULT_CASE, res: ITestOutput ){
        const today = dayjs( new Date() ).format("YYYY-MM-DD")
        // TODO create new sheet by date
        const context = type === "TL1" ? res.case["TL1テストケース（問合せ内容）"] : res.case["TL2テストケース（問合せパターン）"];
        const testCase = type === "TL1" ? res.case["TL1テストケース#"] : res.case["TL2テストケース#"];
        const expect = this.__expect(res)
        return [ 
            res.case["サブカテゴリ#"], res.case.サブカテゴリ, testCase, 
            context, res.case.AI判定, "", "", today, "AUTO", expect.res, expect.memo
        ]
    };

    __expect( res: ITestOutput ){

        if ( res.case.AI判定 === "業者出動" ){
            const pass = res.api.request_type === 'vendor'
            return {
                res: pass ? 'OK' : 'NG',
                memo: pass ? '' : '業者出動をそれ以外と判定'
            }
        };
        if( res.api.request_type === 'vendor' ){
            return {
                res: 'NG',
                memo: '業者出動以外を業者出動と判定'
            }
        }

        return {
            res: 'OK',
            memo: ''
        }
    };

   /*
   Format broken.....
    async writeResult( type: TEST_CASES, sumCount: number, passedCount: number ){

        if( !this._resource.resultPath ) return;
        const isExist = fs.existsSync( this._resource.resultPath );

        if( !isExist ) return;

        const excel = new exceljs.Workbook();
        const workbook = await excel.xlsx.readFile( this._resource.resultPath );

        const sheet = workbook.getWorksheet(1);
        
        const { pass, sum } = TEST_CASE_EXCEL_OUTPUT_ROW[type];
        
        sheet.getRow(pass).getCell(TEST_CASE_OUTPUT_START_COLUMN).value = passedCount;
        sheet.getRow(sum).getCell(TEST_CASE_OUTPUT_START_COLUMN).value = sumCount;

        await excel.xlsx.writeFile( this._resource.resultPath, { useStyles: true, useSharedStrings: true })
    };
    */
    __convertObjectFromRow( row: exceljs.Row, header: string[], max: number ){
        const empty: any = {};
        let number = 0;
        row.eachCell( { includeEmpty: true }, ( cell, cellNum ) => {
            if( this._startColumn <= cellNum && cellNum <= max) {
                empty[ header[number] ] = cell.text.replace("\n", "").replace(/\s/g, "" );
                ++number
            };
        })
        return empty;
    };

    __calcUseCol(){
        const today = dayjs(new Date());
        const diff = today.diff(TEST_START_DAY, "d" );
        return TEST_CASE_OUTPUT_START_COLUMN + diff
    };

    __filterUniqueCase1( cases: ITestCase[] ){
        const uniqueCase1: string[] = [];
        const newArr = cases.filter( tc => {
            const context = tc["TL1テストケース（問合せ内容）"]
            if( uniqueCase1.includes( context) ) return false
            uniqueCase1.push(context)
            return true
        })
        return newArr
    };

};

export default LoadTestCases;