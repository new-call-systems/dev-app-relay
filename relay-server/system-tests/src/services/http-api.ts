
import fetch, { Response } from "node-fetch";

import { IApiClientConfig, emptyConfig } from "../libs/load-config";

interface IAuthorizeHeadersFunction {
    ( headers: object ): { [ header: string ]: string  }
}

const jsonMimeType = "application/json";

const handleErrors = (response: Response) => {
  if (!response.ok) { 
    throw Error(response.statusText);
  };
  return response;
};


class ApiClient {

    config: IApiClientConfig = emptyConfig;

    constructor( config: IApiClientConfig ){
        this.config = config;
    };

    async postJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
        const res = await fetch( this._makeUrl(uri), {
            method: "post",
            headers: this.authorizeHeaders({
              Accept: jsonMimeType,
              "Content-Type": jsonMimeType,
              "Authorization": token
            }),
            body: JSON.stringify(req)
        }).then(handleErrors);
        
        return await this._ensureParseJson<ResT>(res)
    };
    async putJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
      const res = await fetch( this._makeUrl(uri), {
          method: "put",
          headers: this.authorizeHeaders({
            Accept: jsonMimeType,
            "Content-Type": jsonMimeType,
            "Authorization": token
          }),
          body: JSON.stringify(req)
      }).then(handleErrors);
      
      return await this._ensureParseJson<ResT>(res)
  };

    async getJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
      const q = this.encodeQueryData(req);
      let url = this._makeUrl(uri);
      url = q ? [url,q].join("?") : url;
      const res = await fetch( url, {
          method: "get",
          headers: this.authorizeHeaders({
            "Accept": jsonMimeType,
            "Content-Type": jsonMimeType,
            "Authorization": token
          })
      }).then(handleErrors);
      
      const jsonRes = await this._ensureParseJson<ResT>(res);
      return jsonRes
    }; 
    
    async _ensureParseJson<ResT>( res: Response ){
        try{
          return await res.json() as ResT
        } catch(e){
          return { } as ResT
        }
    };
    private authorizeHeaders: IAuthorizeHeadersFunction = headers => {
        return {
          ...headers,
          //Authorization: this._token
        }
    };
    private _makeUrl ( apiUrl: string ) {
        return this.config.baseUrl + this.config.apiPath + apiUrl;
    };
    private encodeQueryData(data: any) {
      const ret = [];    
      for (let d in data) {
        if(data[d] !== undefined) {
          ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        }      
      }      
      return ret.join('&');
    }
};

export default ApiClient;