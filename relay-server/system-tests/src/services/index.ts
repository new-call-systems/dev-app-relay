

import ApiClient from "./http-api";
import ResidentApiClient from "./resident";

import { IServices } from "./interfaces/iservices";

import { loadConfig } from "../libs/load-config";

function createServices(){
    const config = loadConfig();

    const services: IServices = {
        apiClient: new ApiClient(config),
        residentApiClient: new ResidentApiClient(config)
    };

    Object.values(services).forEach( s => {
        if( s.inject ) s.inject( services )
    })
    return services 
};

export default createServices;