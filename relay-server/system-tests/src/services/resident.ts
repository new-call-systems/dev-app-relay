
import * as utils from "../libs/utils";

import { IApiClientConfig } from "../libs/load-config";
import { IResidentApiClient } from "./interfaces/iresident";
import { IServices } from "./interfaces/iservices";
import { IHttpApi, EmptyHttpApi } from "./interfaces/ihttp-api";

import * as rdInterfaces from "../interfaces/resident";
import * as comInterfaces from "../interfaces/common";
import * as vnInterfaces from "../interfaces/vendor";


class ResidentApiClient implements IResidentApiClient {

    _client: IHttpApi = new EmptyHttpApi();
    _config: IApiClientConfig
    constructor( config: IApiClientConfig){
        this._config = config;
    };

    inject(services: IServices){
        this._client = services.apiClient
    };

    async login(){
        const tokenVal = utils.createMCBasicToken()
        const res = await this._client.postJson<string>("/rd/v1/auth/login", this._config.residentInfos, tokenVal);
        return res;
    };
    async loginWithCode( code: string ){
        const res = await this._client.getJson<rdInterfaces.IAuthInfos>("/rd/v1/auth/login", { code: code }, "" );
        return res;
    };
    async cancelRequest( requestId: string, token: string ){
        await this._client.postJson<void>(`/rd/v1/request/${requestId}/cancel`, {}, token );
    };
    async getRequest( requestId: string, token: string ){
        return await this._client.getJson<comInterfaces.IRequestsForVendorMember>(`/rd/v1/request/${requestId}`, {}, token );
    };
};

export default ResidentApiClient;
