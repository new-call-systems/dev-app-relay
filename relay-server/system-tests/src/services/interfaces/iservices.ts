
import { IHttpApi } from "./ihttp-api"
import { IResidentApiClient } from "./iresident";

export interface IServices {
    apiClient: IHttpApi;
    residentApiClient: IResidentApiClient;
};