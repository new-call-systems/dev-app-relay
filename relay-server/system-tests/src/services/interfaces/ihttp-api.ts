

export interface IHttpApi {
    getJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string): Promise<ResT>;
    postJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string): Promise<ResT>;
    putJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string): Promise<ResT>;
};

export class EmptyHttpApi implements IHttpApi {
    async postJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
        return { } as ResT
    };
    async putJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
        return { } as ResT
    };

    async getJson<ResT, ReqT=any>( uri: string, req: ReqT, token: string ){
        return { } as ResT
    }; 
};