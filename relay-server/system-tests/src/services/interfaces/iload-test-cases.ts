

import { AIAnswerExcelType, ExcelCategoryType, TEST_CASES, TEST_RESULT_CASE } from "../../interfaces/excel";
import { IFormRequestRes } from "../../interfaces/resident";

export interface ILoadTestCasesConfig {
    test_case: ITestCaseResource
};

export interface ITestCaseResource {
    type: 'local';
    path: string;
    sheetName: string;
    resultPath: string;
    resultSheet: string;
};

export interface ITestCase {
    'サブカテゴリ#': string;
    'サブカテゴリ': string;
    'TL1テストケース#': string;
    'TL1テストケース（問合せ内容）': string;
    'TL2テストケース#': 'TL2',
    'TL2テストケース（問合せパターン）': string;
    '案件区分': ExcelCategoryType;
    'AI判定': AIAnswerExcelType;
    'AI機能（参考）': string;
    'FAQ番号': string;
    'DIY番号': string;
    '業者(えん様Ver)': string;
    '業者(他管理会社)': string;
    'データの流れ（ストーリー）': string;
    '備考': string;
};

export interface ITestOutput {
    case: ITestCase;
    api: IFormRequestRes
};

export interface ILoadOptions {
    useCase1: boolean;
};

export interface IDependencies { }

export interface ILoadTestCases /*extends IService<IDependencies> */ {
    load(opt?: Partial<ILoadOptions>): Promise<ITestCase[]>;
    writeResult( type: TEST_RESULT_CASE, ...result: ITestOutput[] ): Promise<void>;
    writeStats( type: TEST_CASES, sumCount: number, passedCount: number ): Promise<void>;
}