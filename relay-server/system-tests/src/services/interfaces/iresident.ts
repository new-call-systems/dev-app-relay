

import * as rdInterfaces from "../../interfaces/resident";
import * as comInterfaces from "../../interfaces/common";

export interface IResidentApiClient {
    login(): Promise<string>;
    loginWithCode( code: string ): Promise<rdInterfaces.IAuthInfos>;
    cancelRequest( requestId: string, token: string ): Promise<void>;
    getRequest( requestId: string, token: string ): Promise<comInterfaces.IRequestsForVendorMember>
};