# DB Design
### チームAIBOD 村上輔


## Description
このドキュメントはdb設計の際のER図を管理する「db_design」フォルダの運用方法を定義するものです。<br>
ER図作成に関して「PlantUML」を使用します。

- PlantUML: https://plantuml.com/
- PlantUML in VSCode: https://qiita.com/koara-local/items/e7a7a7d68a4f99a91ab1
- PlantUML Web Viewer: http://www.plantuml.com/plantuml/uml/


## File Manage
スプリント開発を採用しているので、以下のようにスプリントの連番となります。

 - sprint_01.uml

## Version Manage
各ファイルにVersion情報を連番で持たせます。
- sprint_01_01.uml


