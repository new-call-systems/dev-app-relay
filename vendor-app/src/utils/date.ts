import { format, addDays, parse, parseISO } from 'date-fns'
import { ja } from 'date-fns/locale'

export const FormatType = {
  YY_MM_DD: 'yyyy-MM-dd',
  VIEW_YMD: 'yyyy/MM/dd',
  VIEW_YMDHM: 'yyyy/MM/dd HH:mm',
  VIEW_YMDHMS: 'yyyy/MM/dd HH:mm:ss',
  VIEW_HM: 'HH:mm',
};

const f = (d, t) => format(d, t, { locale: ja });

export const getToday = (formatType: string) => {
  return f(new Date(), formatType);
};

export const addDay = (date: string, amount: number) => {
  const parseDate = parseDay(date, FormatType.YY_MM_DD);
  const calc = addDays(parseDate, amount);
  return f(calc, FormatType.YY_MM_DD);
};

export const parseDay = (date: string, formatType: string) => {
  if (date == null) {
    return new Date();
  }
  return parse(date, formatType, new Date());
};

export const formatString = (date: Date) => {
  return f(date, FormatType.YY_MM_DD);
};

export const formatDate = (date: Date, formatType: string) => {
  return f(date, formatType);
};

export const formatTZ = (date: string, formatType: string) => {
  try {
    const utcDate: Date = parseISO(date);
    return f(utcDate, formatType);
  } catch (ex) {
    return date;
  }
};

export const convertStrDay2Date = (str: string) => {
  if (str == null || str === '') {
    return new Date();
  }
  const ymd = str.replace(/-/g, '/');
  return new Date(ymd);
};

export const convertStrTime2Date = (str: string) => {
  if (str == null || str === '') {
    return new Date();
  }
  const time = str.replace(/:/g, '');

  const dt = new Date();
  const y = dt.getFullYear();
  const m = ('00' + (dt.getMonth() + 1)).slice(-2);
  const d = ('00' + dt.getDate()).slice(-2);
  const hh = time.substr(0, 2);
  const mi = time.substr(2, 2);
  const ss = '00';

  const date = `${y}/${m}/${d} ${hh}:${mi}:${ss}`;
  return new Date(date);
};

export const getDateTimeMillisecond = () => {
  const dt = new Date();
  const y = dt.getFullYear();
  const m = ('00' + (dt.getMonth() + 1)).slice(-2);
  const d = ('00' + dt.getDate()).slice(-2);
  const hh = ('00' + dt.getHours()).slice(-2);
  const mi = ('00' + dt.getMinutes()).slice(-2);
  const ss = ('00' + dt.getSeconds()).slice(-2);
  const dd = dt.getMilliseconds();
  return (`${y}${m}${d}${hh}${mi}${ss}${dd}`);
};
