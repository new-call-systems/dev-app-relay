import * as FileSystem from "expo-file-system";
import SHA1 from "js-sha1";
import { getDateTimeMillisecond } from "./date";

const BASE_DIR = `${FileSystem.cacheDirectory}image-cache/`;

export default class CacheManager {
  static entries: { [uri: string]: string | null } = {};

  static async get(key: string): Promise<string | null> {
    // memory cache
    if (CacheManager.entries[key]) {
      return CacheManager.entries[key];
    }
    // file cache
    const path = generateFileName(key);
    const exists = await existFile(path);
    if (exists) {
      CacheManager.entries[key] = path;
      return path;
    }
    return null;
  }

  static async add(url: string, key: string): Promise<string | null> {
    try {
      await FileSystem.makeDirectoryAsync(BASE_DIR);
    } catch (e) {
      // do nothing
    }
    const path = generateFileName(key);
    const uniq = getDateTimeMillisecond();
    const tmpPath = path + `.${uniq}.tmp`;

    try {
      const result = await FileSystem
        .createDownloadResumable(url, tmpPath).downloadAsync();
      if (result && result.status !== 200) {
        console.log(`Download Error: {$key}`);
        return null
      }
      await FileSystem.moveAsync({ from: tmpPath, to: path });
      return path;
    } catch (e) {
      console.log(e);
      return null
    }
  }

  static async clearCache(): Promise<void> {
    await FileSystem.deleteAsync(BASE_DIR, { idempotent: true });
    await FileSystem.makeDirectoryAsync(BASE_DIR);
  }

  static async getCacheSize(): Promise<number> {
    const result = await FileSystem.getInfoAsync(BASE_DIR);
    if (!result.exists) {
      return 0;
    }
    return result.size;
  }
}

const existFile = async (path: string): Promise<boolean> => {
  const info = await FileSystem.getInfoAsync(path);
  const { exists } = info;
  return exists;
};

const generateFileName = (key: string): string => {
  const splitKey = key.split('/');
  const filename = splitKey[splitKey.length - 1];
  const ext = filename.indexOf(".") === -1 ? ".jpg" : filename.substring(filename.lastIndexOf("."));
  const hash = SHA1(key);
  return `${BASE_DIR}${hash}${ext}`;
};
