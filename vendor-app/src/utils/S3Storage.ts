import AWS, { S3 } from "aws-sdk";
import { IReportAwsConfig } from "../api/schema/report";
import CacheManager from "../utils/ImageCache";

interface S3Parameters {
  Bucket: string;
  Prefix: string;
}

export default class S3Storage {
  s3 = null;
  s3Params: S3Parameters;

  /**
   * Constructor
   * @param config
   */
  constructor(config: IReportAwsConfig | null) {
    if (config == null) {
      return;
    }
    const {
      bucket_name,
      region,
      identity_pool_id,
      prefix,
    } = config;

    if (!bucket_name || !region || !identity_pool_id) {
      return;
    }
    this.s3Params = {
      Bucket: bucket_name,
      Prefix: prefix,
    };
    AWS.config.update({
      region: region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: identity_pool_id
      })
    });
    this.s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      maxRetries: 5,
    });
  }

  /**
   * getList
   * アルバム内のファイルを全て取得
   */
  public async getList() {
    if (!this.s3) {
      return [];
    }
    const filterLikeFolder = (obj: S3.Object) => {
      return (obj?.Size || 0) > 0;
    };
    try {
      const data = await this.s3.listObjectsV2(this.s3Params).promise()
      return ((data?.Contents || [])
          .filter(filterLikeFolder)
          .map(v => {
            return {
              key: v.Key,
              url: '',
              add: false,
              remove: false,
            }
          })
      );
    } catch {
      return [];
    }
  }

  /**
   * getFileObject
   * キャッシュが無ければS3からDL
   * @param key
   */
  public async getFileObject(key: string) {
    if (!this.s3) {
      return null;
    }
    // キャッシュから取得
    const cacheFilePath = await CacheManager.get(key);
    if (cacheFilePath) {
      return cacheFilePath;
    }
    // 署名リンクでファイルをDLしてキャッシュに入れる
    const s3Params = { Key: key, Bucket: this.s3Params.Bucket };
    const signedUrl = await this.s3.getSignedUrlPromise('getObject', s3Params);
    const addFilePath = await CacheManager.add(signedUrl, key);
    if (addFilePath) {
      return addFilePath;
    }
    // ファイルが取得できない場合はgetObjectで取得
    return await this.generateBase64Image(key);
  }

  /**
   * generateObjectUrl
   * S3のファイルをBase64形式に変換
   */
  public async generateBase64Image(key: string) {
    if (!this.s3) {
      return null;
    }
    const data = await this.s3.getObject({
      Key: key,
      Bucket: this.s3Params.Bucket,
    }).promise();

    const baseUri = `data:${data.ContentType};base64,`;
    return baseUri + data.Body.toString('base64');
  }

  /**
   * uploadFile
   * ファイルをアップロード
   */
  public async uploadFile(url: string) {
    if (!this.s3) {
      return;
    }

    const localUri = await fetch(url);
    const blob = await localUri.blob();

    const path = url.split('/');
    const fileName = path[path.length - 1];
    const key = `${this.s3Params.Prefix}/${fileName}`;

    await this.s3.upload({
      Bucket: this.s3Params.Bucket,
      Key: key,
      Body: blob,
    }).promise();
  }

  /**
   * deleteFile
   * ファイルを削除
   */
  public async deleteFile(key: string) {
    if (!this.s3) {
      return;
    }
    await this.s3.deleteObject({
      Bucket: this.s3Params.Bucket,
      Key: key,
    }).promise();
  }

  /**
   * deleteFiles
   * 複数ファイルを削除
   */
  public async deleteFiles(keys: string[]) {
    if (!this.s3) {
      return;
    }
    const params = keys.map(v => {
      return { Key: v };
    });
    await this.s3.deleteObjects({
      Bucket: this.s3Params.Bucket,
      Delete: {
        Objects: params,
        Quiet: false
      }
    }).promise();
  }
}
