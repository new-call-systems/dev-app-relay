// 設計書より抜粋
// ● waiting_grasped : 問合せ内容が理解できない場合
// ● waiting_for_cancel : 業者が入札済みの問合せを入居者がキャンセルした場合
// ● waiting_for_open : 業者に出動依頼していない or visibility === “in-house”
// ● open : 業者による入札可能
// ● scheduling : 業者による入札済みで対応予定
// ● in_progress : 業者対応中(一応用意。作業開始報告が必要かどうか)
// ● completed : 業者による完了
// ● deleted : open時に業者が入札する前に取り下げされた場合。スプリント5で策定。
// ● cancelled : 入札後のキャンセル。スプリント5で策定。
// ● transferred : 入居者が管理会社(=サポートセンター)に電話した際のステータス
// ● auto_completed : auto_response時に入居者が満足したらこの値となる
export const REQUEST_STATUS = {
  waiting_grasped: '問合せ内容不明',
  waiting_for_cancel: '入居者キャンセル',
  waiting_for_open: '業者対応不要',
  open: '入札可',
  bid: '承認待ち',
  scheduling: '入札済み',
  in_progress: '対応中',
  completed: '対応済み',
  deleted: '入居者キャンセル',
  cancelled: 'キャンセル承認済み',
  transferred: '管理会社対応',
  auto_completed: '対応完了',
};
