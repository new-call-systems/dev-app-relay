import { createSlice } from '@reduxjs/toolkit';
import { AppThunk } from "../store";
import RequestRepository from "../api/repository/request";
import VendorRepository from "../api/repository/vendor";
import { REQUEST_TYPE } from "./request";
import { IVendorDevicePost } from "../api/schema/vendor";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface NotificationState {
  token: string;
  badgeOpen: boolean;
  badgeSchedule: boolean;
  badgeComplete: boolean;
}

const initialState: NotificationState = {
  token: '',
  badgeOpen: false,
  badgeSchedule: false,
  badgeComplete: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setToken: (state, action: PayloadAction<String>) => {
      return {
        ...state,
        token: action.payload,
      };
    },
    setBadge: (state, action) => {
      const {
        open = 0,
        scheduling = 0,
        in_progress = 0,
        completed = 0,
        waiting_for_cancel = 0,
        cancelled = 0,
      } = action.payload;

      // ※検索条件と合わせる
      // [OPEN]    : 'open',
      // [SCHEDULE]: 'bid,scheduling,in_progress,waiting_for_cancel',
      // [COMPLETE]: 'cancelled,completed,auto_completed',
      const isOpen = (open) > 0;
      const isSchedule = (scheduling + in_progress + waiting_for_cancel) > 0;
      const isComplete = (completed + cancelled) > 0;
      return {
        ...state,
        badgeOpen: state.badgeOpen || isOpen,
        badgeSchedule: state.badgeSchedule || isSchedule,
        badgeComplete: state.badgeComplete || isComplete,
      };
    },
    clearBadge: (state, action) => {
      switch (action.payload) {
        case REQUEST_TYPE.OPEN:
          return {...state, badgeOpen: false};
        case REQUEST_TYPE.SCHEDULE:
          return {...state, badgeSchedule: false};
        case REQUEST_TYPE.COMPLETE:
          return {...state, badgeComplete: false};
        default:
          return state;
      }
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { clear, setToken, setBadge, clearBadge } = slice.actions;

// ---------------------------------------------------------------------------
// Async Action
// ---------------------------------------------------------------------------
export const sendDeviceId: AppThunk = () => async (dispatch, getState) => {
  const { auth, vendor, notification } = getState();
  const isLogin = auth?.isLogin || false;
  if (isLogin) {
    const { userId, vendorId } = vendor;
    const { token } = notification;
    const data: IVendorDevicePost = {device_id: token};
    (async() => {
      await VendorRepository.postDeviceToken(userId, vendorId, data);
    })();
  }
};

export const getBadge: AppThunk = (type?: REQUEST_TYPE) => async (dispatch) => {
  const { data, err } = await RequestRepository.getBadge();
  if (!err) {
    dispatch(setBadge(data));
    dispatch(clearBadge(type));
  }
};

export default slice.reducer;
