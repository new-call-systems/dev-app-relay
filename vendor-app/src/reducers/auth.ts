import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk, RootState } from '../store';
import AuthRepository from '../api/repository/auth';
import RequestRepository from "../api/repository/request";
import { ILoginRequest } from '../api/schema/auth';
import { setProfile } from './vendor';
import { setAlertCode, setAlertMessage } from './message';
import { setBadge, sendDeviceId } from './notification';
import { IForm } from '../components/pages/SignIn';
import { setToken } from '../store/storage';

import { clear as messageClear } from './message';
import { clear as notificationClear } from './notification';
import { clear as reportClear } from './report';
import { clear as requestClear } from './request';
import { clear as vendorClear } from './vendor';

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IAuthState {
  loading: boolean;
  isLogin: boolean;
}

const initialState: IAuthState = {
  loading: false,
  isLogin: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, action: PayloadAction<boolean>) => {
      return {
        ...state,
        loading: action.payload,
      };
    },
    setLogin: (state) => {
      return {
        ...state,
        loading: false,
        isLogin: true,
      };
    },
    setLogout: (state) => {
      return {
        ...state,
        isLogin: false,
      };
    },
  },
});

// ===========================================================================
// Async Action
// ===========================================================================
export const login: AppThunk<IForm> = (params) => async (dispatch, getState) => {
  dispatch(setLoading(true));

  const requestParams: ILoginRequest = {
    email: params.email,
    password: params.password,
    // Debug
    // 開発環境
    // email: 'vm2@cocoen.com', password: 'password',
    // Dev環境
    // email: 'v1@cocoen.com', password: 'password',
  };

  const result = await AuthRepository.login(requestParams);
  if (result.err) {
    if (result.err.code === 401) {
      dispatch(setAlertMessage('ID、またはパスワードが異なります'));
    } else {
      dispatch(setAlertCode('予期せぬエラーが発生しました。'));
    }

    dispatch(setLoading(false));
    return;
  }

  // LocalStorageにTokenを保持
  await setToken({
    access_token: result.data.access_token,
    refresh_token: result.data.refresh_token,
  });

  // 全てのStoreをクリア
  dispatch(messageClear());
  dispatch(reportClear());
  dispatch(requestClear());
  dispatch(vendorClear());
  //dispatch(notificationClear()); pushTokenが消えるとダメ

  // 作業員情報をセット
  dispatch(setProfile(result.data.vendor_member));

  // ログイン状態にセット
  dispatch(setLogin());

  // 新着情報を取得
  const resultBadge = await RequestRepository.getBadge();
  if (!resultBadge.err) {
    dispatch(setBadge(resultBadge.data));
  }

  // デバイスIDを送信
  dispatch(sendDeviceId());
};

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { setLoading, setLogin, setLogout } = slice.actions;

// ---------------------------------------------------------------------------
// Selector
// ---------------------------------------------------------------------------
export const selectIsLogin = (state: RootState): boolean => {
  const { auth } = state;
  return auth.isLogin;
};

export default slice.reducer;
