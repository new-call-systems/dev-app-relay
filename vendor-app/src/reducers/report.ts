import { createSlice } from '@reduxjs/toolkit';
import ReportRepository from "../api/repository/report";
import { IReportListRequest, IReportAwsConfig } from "../api/schema/report";
import {
  convertStrDay2Date,
  convertStrTime2Date,
  formatDate,
  FormatType
} from "../utils/date";
import S3Storage from "../utils/S3Storage";

// ---------------------------------------------------------------------------
// Constants
// ---------------------------------------------------------------------------
export const REPORT_TYPE = {
  IN_PROCESS: 'InProcess',
  COMPLETED: 'Completed',
  REJECT: 'Reject',
};

const REPORT_TYPE_STATUS = {
  [REPORT_TYPE.IN_PROCESS]: 'writing',
  [REPORT_TYPE.COMPLETED]: 'draft,submitted,completed',
  [REPORT_TYPE.REJECT]: 'aborted,declined',
};

export const ACTION_SECTION = {
  '1': '一次対応',
  '2': '二次対応',
};

export const ACTION_STATUS = {
  'incomplete': '未完了引継ぎ',
  'completed': '工事完了',
};

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IReportState {
  forceRequestInProcess: boolean;
  textInProcess: string;
  dataInProcess: IReportDetailState[];
  dataInProcessCurrentPage: number;
  dataInProcessLastPage: number;
  forceRequestCompleted: boolean;
  textCompleted: string;
  dataCompleted: IReportDetailState[];
  dataCompletedCurrentPage: number;
  dataCompletedLastPage: number;
  forceRequestReject: boolean;
  textReject: string;
  dataReject: IReportDetailState[];
  dataRejectCurrentPage: number;
  dataRejectLastPage: number;
  dataDetail: IReportDetailState | null;
  dataDetailTemp: IReportDetailState | null;
  isPreviewMode: boolean;
}

export interface ImageObject {
  key: string;
  url: string;
  add: boolean;
  remove: boolean;
}

export interface IReportDetailState {
  id: string;
  reportId: string;
  managementCompanyName: string;
  propertyName: string;
  roomNo: string;
  residentName: string;
  actionDate: string;
  startingTime: string;
  endingTime: string;
  description: string;
  userName: string;
  receptionDate: string;
  reportStatus: string;
  requestId: string;
  actionStatus: string;
  actionSection: string;
  constructionAmount: number;
  imageObjectList: ImageObject[];
  s3Config: IReportAwsConfig;
}

const initialState: IReportState = {
  forceRequestInProcess: false,
  textInProcess: '',
  dataInProcess: [],
  dataInProcessCurrentPage: 0,
  dataInProcessLastPage: 0,
  forceRequestCompleted: false,
  textCompleted: '',
  dataCompleted: [],
  dataCompletedCurrentPage: 0,
  dataCompletedLastPage: 0,
  forceRequestReject: false,
  textReject: '',
  dataReject: [],
  dataRejectCurrentPage: 0,
  dataRejectLastPage: 0,
  dataDetail: null,
  dataDetailTemp: null,
  isPreviewMode: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'report',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setForceRequest: (state) => {
      return {
        ...state,
        forceRequestInProcess: true,
        forceRequestCompleted: true,
      };
    },
    setList: (state, action) => {
      const type = action.payload.type;
      return {
        ...state,
        [`forceRequest${type}`]: false,
        [`data${type}`]: action.payload.list,
        [`data${type}CurrentPage`]: action.payload.currentPage,
        [`data${type}LastPage`]: action.payload.lastPage,
      };
    },
    appendList: (state, action) => {
      const type = action.payload.type;
      return {
        ...state,
        [`data${type}`]: state[`data${type}`].concat(action.payload.list),
        [`data${type}CurrentPage`]: action.payload.currentPage,
        [`data${type}LastPage`]: action.payload.lastPage,
      };
    },
    setDetail: (state, action) => {
      return {
        ...state,
        isPreviewMode: false,
        dataDetail: action.payload,
        dataDetailTemp: action.payload,
      };
    },
    setPreview: (state, action) => {
      // 日付、時間の書式を整形
      let {actionDate, startingTime, endingTime} = action.payload;
      actionDate = (actionDate == null || actionDate === '') ?
        '' : formatDate(convertStrDay2Date(actionDate), FormatType.VIEW_YMD);
      startingTime = (startingTime == null || startingTime === '') ?
        '' : formatDate(convertStrTime2Date(startingTime), FormatType.VIEW_HM);
      endingTime = (endingTime == null || endingTime === '') ?
        '' : formatDate(convertStrTime2Date(endingTime), FormatType.VIEW_HM);

      return {
        ...state,
        isPreviewMode: true,
        dataDetail: {
          ...state.dataDetail,
          actionDate: actionDate,
          startingTime: startingTime,
          endingTime: endingTime,
          constructionAmount: action.payload.constructionAmount,
          description: action.payload.description,
          userName: action.payload.userName,
          actionSection: action.payload.actionSection,
          actionStatus: action.payload.actionStatus,
          imageObjectList: action.payload.imageObjectList,
        },
      };
    },
    setUnPreview: (state) => {
      return {
        ...state,
        isPreviewMode: false,
        dataDetail: state.dataDetailTemp,
      };
    },
    changeSearchText: (state, action) => {
      const type = action.payload.type;
      return {
        ...state,
        [`forceRequest${type}`]: true,
        [`text${type}`]: action.payload.text,
      };
    },
  },
});


// ---------------------------------------------------------------------------
// Export
// ---------------------------------------------------------------------------
export default slice.reducer;
export const {
  clear,
  setForceRequest,
  setList,
  appendList,
  setDetail,
  setPreview,
  setUnPreview,
  changeSearchText,
} = slice.actions;


// ---------------------------------------------------------------------------
// AsyncAction
// ---------------------------------------------------------------------------
export const getList = (type, query, page) => {
  return async () => {
    // 一覧取得
    const params: IReportListRequest = {
      include_group_reports: false,
      query: query,
      status: REPORT_TYPE_STATUS[type],
      page: page,
    };
    const resultList = await ReportRepository.getList(params);
    if (resultList.err) {
      return resultList;
    }

    // 詳細のセット
    const data = resultList.data.data;
    let details: IReportDetailState[] = [];
    for (let v of data) {
      const detail: IReportDetailState = {
        id: v.report_id,
        reportId: v.report_id,
        managementCompanyName: v.management_company_name,
        propertyName: v.property_name,
        roomNo: v.room_no,
        residentName: v.resident_name,
        actionDate: v.action_date,
        startingTime: v.starting_time,
        endingTime: v.ending_time,
        description: v.description,
        userName: v.user_name,
        receptionDate: v.reception_date,
        reportStatus: v.report_status,
        requestId: v.request_id,
        actionStatus: v.action_status,
        actionSection: v.action_section,
        constructionAmount: v.construction_amount,
        imageObjectList: [],
        s3Config: v.s3_config,
      };
      details.push(detail);
    }

    // AWS S3の情報をセット
    const list = await Promise.all(
      details.map(async (v) => {
        const imageObjectList = await new S3Storage(v.s3Config).getList();
        return {
          ...v,
          imageObjectList: imageObjectList,
        };
      })
    );

    return {
      data: {
        type: type,
        list: list,
        currentPage: resultList.data.current_page,
        lastPage: resultList.data.last_page,
      },
      err: null
    };
  }
};

export const reportSave = (detail: IReportDetailState) => async () => {
  const result = await save(detail);
  return result;
};

export const reportSubmit = (detail: IReportDetailState) => async () => {
  const resultSave = await save(detail);
  if (resultSave.err) {
    return resultSave;
  }

  const resultUpdate = await ReportRepository.statusUpdate(
    detail.requestId,
    detail.reportId,
  );
  return resultUpdate;
};

const save = async (detail: IReportDetailState) => {
  const result = await ReportRepository.update(
    detail.requestId,
    detail.reportId,
    detail,
  );
  if (result.err) {
    return result;
  }

  const s3 = new S3Storage(detail.s3Config);

  // S3 削除
  const del = detail.imageObjectList.filter(
    v => !v.add && v.remove
  ).map(v => {
    return v.key;
  });
  if ((del || []).length > 0) {
    await s3.deleteFiles(del);
  }

  // S3 追加
  await Promise.all(
    detail.imageObjectList.filter(
      v => v.add && !v.remove
    ).map(async v => {
      await s3.uploadFile(v.key);
    })
  );

  return result;
};
