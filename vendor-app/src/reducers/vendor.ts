import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';
import { IVendor, IVendorUpdate } from '../api/schema/vendor';
import VendorRepository from "../api/repository/vendor";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IVendorState {
  userId: number;
  vendorId: string;
  lastName: string;
  firstName: string;
  emergencyTel: string;
  retireFlg: boolean;
  representativeFlg: boolean;
  createdAt: string;
  updatedAt: string;
  isInput: boolean;
}

const initialState: IVendorState = {
  userId: 0,
  vendorId: '',
  lastName: '',
  firstName: '',
  emergencyTel: '',
  retireFlg: false,
  representativeFlg: false,
  createdAt: '',
  updatedAt: '',
  isInput: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'vendor',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setProfile: (state, action: PayloadAction<IVendor>) => {
      return {
        ...state,
        userId: action.payload.user_id,
        vendorId: action.payload.vendor_id,
        lastName: action.payload.last_name,
        firstName: action.payload.first_name,
        emergencyTel: action.payload.emergency_tel,
        retireFlg: action.payload.retire_flg,
        representativeFlg: action.payload.representative_flg,
        createdAt: action.payload.created_at,
        updatedAt: action.payload.updated_at,
        isInput: false,
      };
    },
    completeProfile: (state) => {
      return {
        ...state,
        isInput: true,
      };
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const {
  clear,
  setProfile,
  completeProfile,
} = slice.actions;

// ---------------------------------------------------------------------------
// AsyncAction
// ---------------------------------------------------------------------------
export const registerProfile = (userId, vendorId, data) => {
  return async () => {
    const params: IVendorUpdate = {
      last_name: data.lastName,
      first_name: data.firstName,
      emergency_tel: data.emergencyTel,
    };
    const result = await VendorRepository.update(
      userId,
      vendorId,
      params,
    );
    return result;
  }
};

// ---------------------------------------------------------------------------
// Selector
// ---------------------------------------------------------------------------
export const selectIsProfileRegistered = (state: RootState): boolean => {
  return state.vendor.isInput;
};

export default slice.reducer;
