import { combineReducers } from '@reduxjs/toolkit'

import authReducer from "./auth";
import vendorReducer from "./vendor";
import requestReducer from "./request";
import requestReport from "./report";
import requestMessage from "./message";
import requestNotification from "./notification";

const reducers = combineReducers({
  auth: authReducer,
  vendor: vendorReducer,
  request: requestReducer,
  report: requestReport,
  message: requestMessage,
  notification: requestNotification,
});

export default reducers
