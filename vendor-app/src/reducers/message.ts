import { createSlice } from '@reduxjs/toolkit';
import { API_MANY_REQUEST, FAILED_REFRESH_TOKEN } from "../constants/errorCode";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IMessageState {
  message: string;
}

const initialState: IMessageState = {
  message: '',
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'message',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setAlertMessage: (state, action: PayloadAction<String>) => {
      return {
        ...state,
        message: action.payload,
      };
    },
    setAlertCode: (state, action: PayloadAction<number>) => {
      let msg: string = '';
      switch (action.payload) {
        case API_MANY_REQUEST:
          msg = 'システムエラーが発生しました。時間を置いてもう一度お試しください。';
          break;
        case FAILED_REFRESH_TOKEN:
          // メッセージは出力しない
          break;
        default:
          msg = '予期せぬエラーが発生しました。';
      }
      return {
        ...state,
        message: msg,
      };
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { clear, setAlertMessage, setAlertCode } = slice.actions;


export default slice.reducer;
