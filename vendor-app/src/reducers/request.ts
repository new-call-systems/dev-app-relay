import { createSlice } from '@reduxjs/toolkit';
import RequestRepository from '../api/repository/request';
import ReportRepository from '../api/repository/report';
import {
  IImageObject,
  IRequestListRequest,
  IRequestPreferableInfo,
  ITimeSection,
  RequestStatus,
} from '../api/schema/request';
import { FormatType, formatTZ } from "../utils/date";
import { setAlertMessage } from "./message";
import { IApiError } from "../types/apiResult";
import i18n from "../localization";
import { requestStatus } from "../localization/label.i18n";

// ---------------------------------------------------------------------------
// Constants
// ---------------------------------------------------------------------------
export enum REQUEST_TYPE {
  OPEN,
  SCHEDULE,
  COMPLETE,
}

const REQUEST_TYPE_NAME = {
  [REQUEST_TYPE.OPEN]: 'Open',
  [REQUEST_TYPE.SCHEDULE]: 'Schedule',
  [REQUEST_TYPE.COMPLETE]: 'Complete',
};

const REQUEST_TYPE_STATUS = {
  [REQUEST_TYPE.OPEN]: 'open',
  [REQUEST_TYPE.SCHEDULE]: 'bid,scheduling,in_progress,waiting_for_cancel',
  [REQUEST_TYPE.COMPLETE]: 'cancelled,completed,auto_completed',
};

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IRequestState {
  timeSection: ITimeSection[];
  dataOpen: IRequestDetailState[];
  dataOpenCurrentPage: number;
  dataOpenLastPage: number;
  dataOpenDetail: IRequestDetailState | null;
  forceRequestOpen: boolean;
  textOpen: string;
  dataSchedule: IRequestDetailState[];
  dataScheduleCurrentPage: number;
  dataScheduleLastPage: number;
  dataScheduleDetail: IRequestDetailState | null;
  forceRequestSchedule: boolean;
  textSchedule: string;
  dataComplete: IRequestDetailState[];
  dataCompleteCurrentPage: number;
  dataCompleteLastPage: number;
  dataCompleteDetail: IRequestDetailState | null;
  forceRequestComplete: boolean;
  textComplete: string;
}

export interface IRequestDetailState {
  id: number;
  aiSubCategory: string;
  status: string;
  emergencyFlg: boolean;
  context: string;
  residentName: string;
  residentTel: string;
  propertyName: string;
  propertyAddress: string;
  roomNo: string;
  preferableInfo: IRequestPreferableInfo[];
  images: IImageObject[];
  cancelReason: string;
  userName: string;
  isAdmin: boolean;
  createdAt: string;
  updatedAt: string;
}

const initialState: IRequestState = {
  timeSection: [],
  dataOpen: [],
  dataOpenCurrentPage: 0,
  dataOpenLastPage: 0,
  dataOpenDetail: null,
  forceRequestOpen: false,
  textOpen: '',
  dataSchedule: [],
  dataScheduleCurrentPage: 0,
  dataScheduleLastPage: 0,
  dataScheduleDetail: null,
  forceRequestSchedule: false,
  textSchedule: '',
  dataComplete: [],
  dataCompleteCurrentPage: 0,
  dataCompleteLastPage: 0,
  dataCompleteDetail: null,
  forceRequestComplete: false,
  textComplete: '',
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'request',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setForceRequest: (state, action) => {
      const key = `forceRequest${action.payload}`;
      return {
        ...state,
        [key]: true,
      };
    },
    setOpenList: (state, action) => {
      return {
        ...state,
        dataOpen: action.payload.list,
        timeSection: action.payload.timeSection,
        dataOpenCurrentPage: action.payload.currentPage,
        dataOpenLastPage: action.payload.lastPage,
        forceRequestOpen: false,
      };
    },
    appendOpenList: (state, action) => {
      return {
        ...state,
        dataOpen: state.dataOpen.concat(action.payload.list),
        dataOpenCurrentPage: action.payload.currentPage,
        dataOpenLastPage: action.payload.lastPage,
      };
    },
    setScheduleList: (state, action) => {
      return {
        ...state,
        dataSchedule: action.payload.list,
        timeSection: action.payload.timeSection,
        dataScheduleCurrentPage: action.payload.currentPage,
        dataScheduleLastPage: action.payload.lastPage,
        forceRequestSchedule: false,
      };
    },
    appendScheduleList: (state, action) => {
      return {
        ...state,
        dataSchedule: state.dataSchedule.concat(action.payload.list),
        dataScheduleCurrentPage: action.payload.currentPage,
        dataScheduleLastPage: action.payload.lastPage,
      };
    },
    setCompleteList: (state, action) => {
      return {
        ...state,
        dataComplete: action.payload.list,
        timeSection: action.payload.timeSection,
        dataCompleteCurrentPage: action.payload.currentPage,
        dataCompleteLastPage: action.payload.lastPage,
        forceRequestComplete: false,
      };
    },
    appendCompleteList: (state, action) => {
      return {
        ...state,
        dataComplete: state.dataComplete.concat(action.payload.list),
        dataCompleteCurrentPage: action.payload.currentPage,
        dataCompleteLastPage: action.payload.lastPage,
      };
    },
    setOpenDetail: (state, action) => {
      return {
        ...state,
        dataOpenDetail: action.payload.detail,
        timeSection: action.payload.timeSection,
      };
    },
    setScheduleDetail: (state, action) => {
      return {
        ...state,
        dataScheduleDetail: action.payload.detail,
        timeSection: action.payload.timeSection,
      };
    },
    setCompleteDetail: (state, action) => {
      return {
        ...state,
        dataCompleteDetail: action.payload.detail,
        timeSection: action.payload.timeSection,
      };
    },
    changeSearchText: (state, action) => {
      const type = REQUEST_TYPE_NAME[action.payload.type];
      return {
        ...state,
        [`forceRequest${type}`]: true,
        [`text${type}`]: action.payload.text,
      };
    },
  },
});


// ---------------------------------------------------------------------------
// Export
// ---------------------------------------------------------------------------
export default slice.reducer;
export const {
  clear,
  setForceRequest,
  setOpenList,
  appendOpenList,
  setOpenDetail,
  setScheduleList,
  appendScheduleList,
  setScheduleDetail,
  setCompleteList,
  appendCompleteList,
  setCompleteDetail,
  changeSearchText,
} = slice.actions;


// ---------------------------------------------------------------------------
// AsyncAction
// ---------------------------------------------------------------------------
export const getList = (type: REQUEST_TYPE, query: string, page: number) => {
  return async () => {
    // 一覧取得
    const params: IRequestListRequest = {
      status: REQUEST_TYPE_STATUS[type],
      page: page,
    };
    const resultList = await RequestRepository.getRequest(params);
    if (resultList.err) {
      return resultList;
    }

    // 時間リスト
    const resultTimeSection = await RequestRepository.getTimeSection();
    if (resultTimeSection.err) {
      return resultTimeSection;
    }

    // フィルタ
    const filterList = resultList.data.data.filter((v) => {
      if (query === '') return true;
      const target = ''
        + v.context
        + i18n.t([requestStatus,RequestStatus[v.status]].join("."))
        + (v.emergency_flg ? '緊急' : '');
      return target.includes(query);
    });

    // 詳細のセット
    let details = [];
    for (let v of filterList) {
      const detail = {
        id: v.request_id,
        aiSubCategory: v.ai_sub_category,
        status: v.status,
        emergencyFlg: v.emergency_flg,
        context: v.context,
        residentName: '',
        residentTel: '',
        propertyName: '',
        propertyAddress: '',
        roomNo: '',
        preferableInfo: v.preferable_info,
        images: [],
        cancelReason: '',
        userName: '',
        isAdmin: false,
        createdAt: v.created_at,
        updatedAt: '',
      };
      details.push(detail);
    }

    return {
      data: {
        list: details,
        timeSection: resultTimeSection.data,
        currentPage: resultList.data.current_page,
        lastPage: resultList.data.last_page,
      },
      err: null
    };
  }
};

export const getDetail = (requestId: number) => {
  return async () => {
    // 時間リスト
    const resultTimeSection = await RequestRepository.getTimeSection();
    if (resultTimeSection.err) {
      return resultTimeSection;
    }

    // 詳細取得
    const resultDetail = await RequestRepository.getRequestDetail(requestId);
    if (resultDetail.err) {
      return resultDetail;
    }

    // 詳細のセット
    const detail = {
      id: resultDetail.data.request_id,
      aiSubCategory: resultDetail.data.ai_sub_category,
      status: resultDetail.data.status,
      emergencyFlg: resultDetail.data.emergency_flg,
      context: resultDetail.data.context,
      residentName: resultDetail.data.resident_name,
      residentTel: resultDetail.data.resident_tel,
      propertyName: resultDetail.data.property_name,
      propertyAddress: resultDetail.data.property_address,
      roomNo: resultDetail.data.room_no,
      preferableInfo: resultDetail.data.preferable_info,
      images: resultDetail.data.images,
      cancelReason: resultDetail.data.cancel_reason,
      userName: resultDetail.data.user_name,
      isAdmin: resultDetail.data.is_admin,
      createdAt: resultDetail.data.created_at,
      updatedAt: resultDetail.data.updated_at,
    };

    return {
      data: {
        detail: detail,
        timeSection: resultTimeSection.data,
      },
      err: null
    };
  }
};

export const registerBid = (requestId, preferableId) => {
  return async (dispatch) => {
    const result = await RequestRepository.putRequestBid(
      requestId,
      preferableId,
    );
    if (result.err) {
      if (isConflict(result.err)) {
        dispatch(setAlertMessage("問合せ情報が更新されてます。再読み込みをしてお試し下さい"));
        return { data: {}, err: null };
      }
    }
    return result;
  }
};

export const acceptCancel = (requestId) => {
  return async (dispatch) => {
    const result = await RequestRepository.cancel(
      requestId,
    );
    if (result.err) {
      if (isConflict(result.err)) {
        dispatch(setAlertMessage("問合せ情報が更新されてます。再読み込みをしてお試し下さい"));
        return { data: {}, err: null };
      }
    }
    return result;
  }
};

export const requestComplete = (requestId) => {
  return async (dispatch) => {
    // 問合せステータスの変更
    const resultRequest = await RequestRepository.putRequestStatus(requestId, 'completed');
    if (resultRequest.err) {
      if (isConflict(resultRequest.err)) {
        dispatch(setAlertMessage("問合せ情報が更新されてます。再読み込みをしてお試し下さい"));
        return { data: {}, err: null };
      }
      return resultRequest;
    }
    // 完了報告書の作成
    const resultReport = await ReportRepository.create(requestId);
    if (resultReport) {
      return resultReport;
    }
    return;
  }
};


// ---------------------------------------------------------------------------
// Selector
// ---------------------------------------------------------------------------
export const selectPreferable = (
  preferable: IRequestPreferableInfo[]
): IRequestPreferableInfo | null => {
  for (let i = 0; i < preferable.length; i++) {
    if (preferable[i].selected) {
      return preferable[i];
    }
  }
  return null
};

export const preferable2Text = (
  timeSection: ITimeSection[] | null,
  preferable: IRequestPreferableInfo | null
): string => {
  if (timeSection == null || preferable == null) {
    return '';
  }
  const { date, time_section_id } = preferable;
  return formatTZ(date, FormatType.VIEW_YMD)
    + ' '
    + time2Text(time_section_id, timeSection);
};

export const time2Text = (
  sectionId: number,
  timeSection: ITimeSection[],
) => {
  const row = timeSection.find((v) => v.time_section_id === sectionId);
  if (row) {
    return row.label;
  }
  return '';
};

const isConflict = (error: IApiError) => {
  return (error?.code === 400 && error?.data?.error === "Bad Request");
};
