export interface IApiResult<T> {
  data: T | null;
  err: IApiError | null;
}

export interface IApiError {
  code: number;
  data: object;
}
