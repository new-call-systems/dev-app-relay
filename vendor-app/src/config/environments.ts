import Constants from 'expo-constants';

interface IEnvironments {
  mode: string;
  endpointRelayServer: string;
}

const ENVs = {
  development: {
    mode: 'development',
    endpointRelayServer: 'http://localhost:8080',
  },
  staging: {
    mode: 'staging',
    endpointRelayServer: 'https://www2.dev.coco-en.com',
  },
  production: {
    mode: 'production',
    endpointRelayServer: 'https://www.dev.coco-en.com',
  },
};

function getEnvVars(): IEnvironments {
  const options = Constants.manifest.packagerOpts;
  const channel = Constants.manifest.releaseChannel;
  const isDev = options && options.dev;
  if (isDev) {
    // 開発環境
    // 環境変数に中継サーバが指定してあれば、そちらのエンドポイントを優先する
    const endpointRelayServer = process.env.REACT_NATIVE_RELAY_SERVER;
    if (endpointRelayServer) {
      return {
        ...ENVs.development,
        endpointRelayServer: endpointRelayServer,
      };
    }

    return ENVs.development;
  }

  return ENVs[channel];
}

const selectENV = getEnvVars();

export const isDev = (): boolean => selectENV.mode === 'development';

export default selectENV;
