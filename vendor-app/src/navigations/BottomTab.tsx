import React from 'react';
import { useSelector } from 'react-redux';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { TabBarOptions } from '../components/assets/constants/navigator';
import { PAGE_TYPE, PAGE_TYPE_NAME } from '../constants/page';
import { NavigatorBottomTabIcon } from '../components/atoms';
import Request from '../navigations/RequestStack';
import Schedule from '../navigations/ScheduleStack';
import Complete from '../navigations/CompleteStack';
import Report from '../navigations/ReportStack';

const Tab = createBottomTabNavigator();

const hideOnScreens = [
  `${PAGE_TYPE.RequestDetail}`,
  `${PAGE_TYPE.ScheduleDetail}`,
  `${PAGE_TYPE.CompleteDetail}`,
  `${PAGE_TYPE.ReportDetail}`,
  `${PAGE_TYPE.ReportForm}`,
  `${PAGE_TYPE.ReportPreview}`,
];

export default function BottomTab(): React.FC {
  const badgeOpen: boolean = useSelector((state) => state.notification.badgeOpen);
  const badgeSchedule: boolean = useSelector((state) => state.notification.badgeSchedule);
  const badgeComplete: boolean = useSelector((state) => state.notification.badgeComplete);
  const badges = {
    [PAGE_TYPE.Request]: badgeOpen,
    [PAGE_TYPE.Schedule]: badgeSchedule,
    [PAGE_TYPE.Complete]: badgeComplete,
    [PAGE_TYPE.Report]: false,
  };

  return (
    <Tab.Navigator
      initialRouteName={PAGE_TYPE.Request}
      tabBarOptions={TabBarOptions}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          return <NavigatorBottomTabIcon
            focused={focused}
            name={route.name}
            badge={badges[route.name]}
          />;
        },
        tabBarVisible: (() => {
          // 子画面はフッターを非表示
          const routeName = getFocusedRouteNameFromRoute(route);
          return (hideOnScreens.indexOf(routeName) === -1);
        })(),
      })}
    >
      <Tab.Screen
        name={`${PAGE_TYPE.Request}`}
        component={Request}
        options={() => ({ title: PAGE_TYPE_NAME.Request })}
      />
      <Tab.Screen
        name={`${PAGE_TYPE.Schedule}`}
        component={Schedule}
        options={() => ({ title: PAGE_TYPE_NAME.Schedule })}
      />
      <Tab.Screen
        name={`${PAGE_TYPE.Complete}`}
        component={Complete}
        options={() => ({ title: PAGE_TYPE_NAME.Complete })}
      />
      <Tab.Screen
        name={`${PAGE_TYPE.Report}`}
        component={Report}
        options={() => ({ title: PAGE_TYPE_NAME.Report })}
      />
    </Tab.Navigator>
  );
}
