import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { PAGE_TYPE, PAGE_TYPE_NAME } from '../constants/page';
import Complete from '../components/pages/Complete';
import CompleteDetail from '../components/pages/CompleteDetail';
import ReportDetail from "../components/pages/ReportDetail";
import { StackOptions } from '../components/assets/constants/navigator';

const Stack = createStackNavigator();

export default function CompleteStack(): React.FC {
  return (
    <Stack.Navigator {...StackOptions}>
      <Stack.Screen
        name={`${PAGE_TYPE.Complete}`}
        component={Complete}
        options={{ title: PAGE_TYPE_NAME.Complete }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.CompleteDetail}`}
        component={CompleteDetail}
        options={{ title: PAGE_TYPE_NAME.CompleteDetail }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.CompleteDetailReport}`}
        component={ReportDetail}
        options={{ title: PAGE_TYPE_NAME.CompleteDetailReport }}
      />
    </Stack.Navigator>
  );
}
