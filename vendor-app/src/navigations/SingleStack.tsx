import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { StackOptions } from '../components/assets/constants/navigator';

const Stack = createStackNavigator();

interface IProps {
  component: React.ReactNode;
  title: string;
}

const SingleStack: React.FC<IProps> = (props) => {
  return (
    <Stack.Navigator {...StackOptions}>
      <Stack.Screen
        name={props.title}
        component={props.component}
        options={{ title: props.title }}
      />
    </Stack.Navigator>
  );
};

export default SingleStack;
