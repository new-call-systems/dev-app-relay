import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { PAGE_TYPE, PAGE_TYPE_NAME } from '../constants/page';
import Report from '../components/pages/Report';
import ReportDetail from '../components/pages/ReportDetail';
import ReportForm from '../components/pages/ReportForm';
import { StackOptions } from '../components/assets/constants/navigator';

const Stack = createStackNavigator();

export default function ReportStack(): React.FC {
  return (
    <Stack.Navigator {...StackOptions}>
      <Stack.Screen
        name={`${PAGE_TYPE.Report}`}
        component={Report}
        options={{ title: PAGE_TYPE_NAME.Report }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.ReportDetail}`}
        component={ReportDetail}
        options={{ title: PAGE_TYPE_NAME.ReportDetail }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.ReportForm}`}
        component={ReportForm}
        options={{ title: PAGE_TYPE_NAME.ReportForm }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.ReportPreview}`}
        component={ReportDetail}
        options={{ title: PAGE_TYPE_NAME.ReportPreview }}
      />
    </Stack.Navigator>
  );
}
