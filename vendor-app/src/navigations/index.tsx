import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { selectIsLogin } from '../reducers/auth';
import { PAGE_TYPE_NAME } from '../constants/page';
import SignIn from '../components/pages/SignIn';
import SingleStack from './SingleStack';
import BottomTab from './BottomTab';

export default function Navigation(): React.FC {
  const isLogin = useSelector(selectIsLogin);

  return (
    <NavigationContainer>
      {!isLogin ? (
        // サインイン
        <SingleStack title={PAGE_TYPE_NAME.SignIn} component={SignIn} />
      ) : (
        // メイン画面
        <BottomTab />
      )}
    </NavigationContainer>
  );
}
