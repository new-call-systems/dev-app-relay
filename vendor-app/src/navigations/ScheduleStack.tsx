import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { PAGE_TYPE, PAGE_TYPE_NAME } from '../constants/page';
import Schedule from '../components/pages/Schedule';
import ScheduleDetail from '../components/pages/ScheduleDetail';
import ReportForm from '../components/pages/ReportForm';
import { StackOptions } from '../components/assets/constants/navigator';

const Stack = createStackNavigator();

export default function ScheduleStack(): React.FC {
  return (
    <Stack.Navigator {...StackOptions}>
      <Stack.Screen
        name={`${PAGE_TYPE.Schedule}`}
        component={Schedule}
        options={{ title: PAGE_TYPE_NAME.Schedule }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.ScheduleDetail}`}
        component={ScheduleDetail}
        options={{ title: PAGE_TYPE_NAME.ScheduleDetail }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.ReportForm}`}
        component={ReportForm}
        options={{ title: PAGE_TYPE_NAME.ReportForm }}
      />
    </Stack.Navigator>
  );
}
