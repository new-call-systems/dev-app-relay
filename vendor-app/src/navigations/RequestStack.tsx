import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { PAGE_TYPE, PAGE_TYPE_NAME } from '../constants/page';
import Request from '../components/pages/Request';
import RequestDetail from '../components/pages/RequestDetail';
import { StackOptions } from '../components/assets/constants/navigator';

const Stack = createStackNavigator();

export default function RequestStack(): React.FC {
  return (
    <Stack.Navigator {...StackOptions}>
      <Stack.Screen
        name={`${PAGE_TYPE.Request}`}
        component={Request}
        options={{ title: PAGE_TYPE_NAME.Request }}
      />
      <Stack.Screen
        name={`${PAGE_TYPE.RequestDetail}`}
        component={RequestDetail}
        options={{ title: PAGE_TYPE_NAME.RequestDetail }}
      />
    </Stack.Navigator>
  );
}
