import AsyncStorage from '@react-native-async-storage/async-storage';

const TOKEN_KEY: string = 'cocoenvendor:tokens';

interface IToken {
  access_token: string;
  refresh_token: string;
}

/**
 * Tokenを取得
 */
const getToken = async (): Promise<IToken | null> => {
  try {
    const token = await AsyncStorage.getItem(TOKEN_KEY);
    return JSON.parse(token);
  } catch (error) {
    return null;
  }
};

/**
 * アクセストークンを取得
 */
const getAccessToken = async (): Promise<string | null> => {
  const token = await getToken();
  return token?.access_token;
};

/**
 * リフレッシュトークンを取得
 */
const getRefreshToken = async (): Promise<string | null> => {
  const token = await getToken();
  return token?.refresh_token;
};

/**
 * Tokenをセット
 * @param token
 */
const setToken = async (token: IToken): Promise<void> => {
  await AsyncStorage.setItem(TOKEN_KEY, JSON.stringify(token));
};

/**
 * Storageをクリア
 */
const clearStorage = async (): Promise<void> => {
  // await AsyncStorage.clear();
  // await AsyncStorage.getAllKeys().then(AsyncStorage.mulitRemove)
  const asyncStorageKeys = await AsyncStorage.getAllKeys();
  if (asyncStorageKeys.length > 0) {
    await AsyncStorage.clear();
  }
};

export { AsyncStorage, setToken, clearStorage, getAccessToken, getRefreshToken };
