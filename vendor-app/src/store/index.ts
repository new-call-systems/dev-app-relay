import {
  configureStore,
  getDefaultMiddleware,
  ThunkAction,
  Action,
} from '@reduxjs/toolkit';
import { combinePersist, getPersistStore, ignoredActionList } from './persist';
import reducers from '../reducers';
import { isDev } from '../config/environments';

// redux-middleware
const middleware = [
  ...getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: ignoredActionList,
    },
  }),
];

// devtools
if (isDev()) {
  // 開発モードのみloggerをimportするのでrequireを使用（ESLintを除外)
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const logger = require('redux-logger');
  const loggerMiddleware = logger.createLogger({
    diff: false,
    collapsed: true,
  });

  //middleware.push(loggerMiddleware);
}

// store (persist連携)
const persistReducer = combinePersist(reducers);
const store = configureStore({
  reducer: persistReducer,
  middleware: middleware,
  devTools: isDev(),
});

// persist用
const persistor = getPersistStore(store);

// 型定義
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export { store, persistor };
