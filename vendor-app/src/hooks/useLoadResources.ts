import { useState, useEffect } from 'react';
import { Asset } from 'expo-asset';
import icons from '../components/assets/icon';
import {
  useFonts,
  NotoSansJP_500Medium,
  NotoSansJP_700Bold,
} from '@expo-google-fonts/noto-sans-jp';

export const useLoadResources = () => {
  const [isCompleted, setCompleted] = useState(false);
  const [assetLoaded, setAssetLoaded] = useState(false);
  let [fontsLoaded] = useFonts({
    NotoSansJP_500Medium,
    NotoSansJP_700Bold,
  });

  useEffect(() => {
    if (fontsLoaded && assetLoaded) {
      setCompleted(true);
    }
  }, [fontsLoaded, assetLoaded]);

  const loadResourcesAsync = async () => {
    await Asset.loadAsync(
      Object.keys(icons).map(key => icons[key])
    );
    setAssetLoaded(true);
  };

  return [loadResourcesAsync, isCompleted];
};
