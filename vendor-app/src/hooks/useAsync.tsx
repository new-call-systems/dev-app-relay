import { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setLogout } from '../reducers/auth';
import { setAlertCode } from '../reducers/message';
import { FAILED_REFRESH_TOKEN } from "../constants/errorCode";
import { AppDispatch } from '../store';
import { clearStorage } from "../store/storage";

export const useAsync = () => {
  const dispatch: AppDispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const asyncAction = useCallback(
    async (action, callback) => {

    setLoading(true);

    const errorAction = (error) => {
      console.log('Exception useAsync', error);

      setLoading(false);
      setError(error);

      // AlertMessage
      dispatch(setAlertCode(error.code));

      // Logout
      if (error.code === FAILED_REFRESH_TOKEN) {
        clearStorage();
        dispatch(setLogout());
      }
    };

    try {
      const result = await action(dispatch);

      setLoading(false);
      if (result.err) {
        errorAction(result.err);
      } else {
        callback(result.data);
      }

    } catch (error) {
      errorAction(error.err);
    }
  }, [loading, error]);

  return [asyncAction, loading, error];
};
