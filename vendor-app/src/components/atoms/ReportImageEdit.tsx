import React, { useState, useEffect } from "react";
import { View, Image, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import { ImageObject } from "../../reducers/report";
import { IReportAwsConfig } from "../../api/schema/report";
import S3Storage from "../../utils/S3Storage";
import LoadingImage from "./LoadingImage";

interface IProps {
  s3Config: IReportAwsConfig;
  data: ImageObject[];
  onPress: () => void;
  onRemove: () => void;
}

interface ImageObjectExt extends ImageObject {
  originIndex: number;
}

// ローディング用
const DUMMY_LOADING_IMAGE_PATH = '@';

const ReportImageEdit: React.FC<IProps> = (props) => {
  const {
    s3Config,
    data = [],
    onPress,
    onRemove,
  } = props;

  const [images, setImages] = useState<ImageObjectExt[]>([]);

  useEffect(() => {
    let isMounted = true;

    const setLoadingImage = () => {
      let result: ImageObjectExt[] = [];
      for (let i = 0; i < data.length; i++) {
        const row = data[i];
        if (row.remove) {
          continue;
        }
        if (row.add) {
          const obj = {...row, originIndex: i};
          result.push(obj);
          continue;
        }
        const obj = {...row, url: DUMMY_LOADING_IMAGE_PATH, originIndex: i};
        result.push(obj);
      }
      if (isMounted) {
        setImages(result);
      }
    };

    const getImageList = async () => {
      let stateIndex = 0;
      data.filter(v => !v.remove).map(async (v: ImageObject, index: number) => {
        if (v.add) {
          stateIndex++;
          return;
        }
        const base64 = await new S3Storage(s3Config).getFileObject(v.key);
        if (isMounted) {
          setImages(prevState => {
            const nextState = [...prevState];
            nextState[index]['url'] = base64;
            return nextState;
          });
        }
        stateIndex++;
      });
    };

    (async () => {
      setLoadingImage();
      getImageList();
    })();

    return () => {
      isMounted = false;
    };
  }, [data]);

  return (
    <View style={styles.container}>
      <ScrollView horizontal={true}>
        <TouchableOpacity
          onPress={onPress}
          style={styles.block}
        >
          <Image
            style={styles.imageLibrary}
            source={require('../assets/icon/icon-library.png')}
          />
          <Image
            style={[styles.iconPosition, styles.iconSize]}
            source={require('../assets/icon/btn-photo-add.png')}
          />
        </TouchableOpacity>
        {images.map((v: ImageObjectExt) => {
          return (
            <React.Fragment key={v.originIndex}>
              <View style={styles.dummyImageSpace} />
              <View style={styles.block}>
                <ViewImage
                  url={v.url}
                  originIndex={v.originIndex}
                  onRemove={onRemove}
                />
              </View>
            </React.Fragment>
          );
        })}
      </ScrollView>
    </View>
  );
};

const ViewImage = ({ url, originIndex, onRemove }) => {
  if (url === DUMMY_LOADING_IMAGE_PATH) {
    return <LoadingImage style={styles.imageUploaded}/>
  }
  return (
    <>
      <Image
        style={styles.imageUploaded}
        source={{uri: url}}
      />
      <TouchableOpacity
        style={styles.iconPosition}
        onPress={(e) => onRemove(e, originIndex)}>
        <Image
          style={styles.iconSize}
          source={require('../assets/icon/btn-photo-delete.png')}
        />
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 7,
    flexDirection: 'row'
  },
  block: {
    width: 100,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: "#c7c7c7",
    backgroundColor: '#ffffff',
    position: 'relative',
  },
  iconPosition: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  iconSize: {
    width: 20,
    height: 20,
  },
  imageLibrary: {
    width: 34,
    height: 28,
  },
  imageUploaded: {
    width: 100,
    height: 75,
  },
  dummyImageSpace: {
    width: 11,
  },
});

export default ReportImageEdit;
