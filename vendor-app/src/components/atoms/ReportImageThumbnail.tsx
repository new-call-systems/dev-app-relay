import React, { useState, useEffect } from "react";
import { Image, StyleSheet } from 'react-native';
import { ImageObject } from "../../reducers/report";
import { IReportAwsConfig } from "../../api/schema/report";
import LoadingImage from "../atoms/LoadingImage";
import S3Storage from "../../utils/S3Storage";

interface IProps {
  s3Config: IReportAwsConfig;
  data: ImageObject[];
}

const ReportImageThumbnail: React.FC<IProps> = (props) => {
  const {
    s3Config,
    data,
  } = props;

  const [image, setImage] = useState<string>(null);
  const isFetch = (data || []).length > 0;

  useEffect(() => {
    let isMounted = true;

    const getThumbnail = async () => {
      const key = data[0]?.key;
      if (key) {
        const image = await new S3Storage(s3Config).getFileObject(key);
        if (isMounted) {
          setImage(image);
        }
      }
    };

    if (isFetch) {
      getThumbnail();
    }

    return () => {
      isMounted = false;
    };
  }, [data]);

  // データ無し
  if (!isFetch) {
    return (
      <Image
        style={styles.imageSize}
        source={require('../assets/icon/no-image.png')}
      />
    );
  }

  // データ取得中
  if (isFetch && !image) {
    return <LoadingImage style={styles.imageSize}/>
  }

  return (
    <Image
      style={styles.imageSize}
      source={{uri: `${image}`}}
    />
  );
};

const styles = StyleSheet.create({
  imageSize: {
    height: 90,
    width: 120,
  },
});

export default ReportImageThumbnail;
