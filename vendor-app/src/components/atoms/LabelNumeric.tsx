import React from "react";
import { Text, ViewStyle } from 'react-native';

interface IProps {
  children: string;
  style?: ViewStyle;
}

const formatText = (val) => {
  if (val == null || val === '') {
    return ''
  }
  try {
    return Number(val).toLocaleString();
  } catch {
    return String(val).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
  }
};

const LabelNumeric: React.FC<IProps> = (props) => {
  const {
    children,
    style,
  } = props;
  const value = formatText(children);

  return (
    <Text style={style}>
      {value}
      {value && ' 円'}
    </Text>
  );
};

export default LabelNumeric;
