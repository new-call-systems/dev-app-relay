import React from 'react';
import { ViewStyle } from 'react-native';
import { Video } from 'expo-av';

interface IProps {
  uri: string;
  style?: ViewStyle;
}

const CustomVideo: React.FC<IProps> = (props) => {
  const { uri, style } = props;

  return (
    <Video
      source={{uri: uri}}
      resizeMode={Video.RESIZE_MODE_COVER}
      shouldPlay={false}
      useNativeControls={true}
      style={style}
    />
  );
};

export default CustomVideo;
