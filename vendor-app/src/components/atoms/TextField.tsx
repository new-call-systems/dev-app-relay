import React from 'react';
import { TextInput, ViewStyle } from 'react-native';

interface IProps {
  value: string;
  onChangeText?: (str: string) => void;
  style?: ViewStyle;
  autoCompleteType?:
    | 'off'
    | 'username'
    | 'password'
    | 'email'
    | 'name'
    | 'tel'
    | 'street-address'
    | 'postal-code'
    | 'cc-number'
    | 'cc-csc'
    | 'cc-exp'
    | 'cc-exp-month'
    | 'cc-exp-year';
  secureTextEntry?: boolean;
  disabled?: boolean;
  keyboardType?:
    | 'default'
    | 'number-pad'
    | 'decimal-pad'
    | 'numeric'
    | 'email-address'
    | 'phone-pad';
}

const TextField: React.FC<IProps> = (props) => {
  const {
    value,
    onChangeText,
    style,
    autoCompleteType,
    secureTextEntry,
    disabled,
    keyboardType,
  } = props;

  return (
    <TextInput
      value={value}
      disabled={disabled}
      onChangeText={onChangeText}
      mode="outlined"
      style={style}
      autoCompleteType={autoCompleteType}
      autoCapitalize="none"
      secureTextEntry={secureTextEntry}
      keyboardType={keyboardType}
    />
  );
};

export default TextField;
