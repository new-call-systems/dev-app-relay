import React from 'react';
import {
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView as NativeScrollView,
} from 'react-native';

interface IProps {
  children?: React.ReactNode;
}

const dismiss = (): void => Keyboard.dismiss();

const ScrollView: React.FC<IProps> = (props) => {
  const { children } = props;

  return (
    <TouchableWithoutFeedback onPress={dismiss}>
      <NativeScrollView keyboardShouldPersistTaps="handled">
        {children}
      </NativeScrollView>
    </TouchableWithoutFeedback>
  );
};

export default ScrollView;
