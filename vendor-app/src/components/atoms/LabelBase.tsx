import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

interface IProps {
  children: React.ReactNode;
  frameStyles: string;
  textStyles: string;
}

const LabelBase: React.FC<IProps> = (props) => {
  const {
    children,
    frameStyles,
    textStyles,
  } = props;

  return (
    <View style={[styles.frame, frameStyles]}>
      <Text style={[styles.text, textStyles]}>
        {children}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  frame: {
    height: 22,
    borderRadius: 6,
  },
  text: {
    marginTop: 'auto',
    marginRight: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
  },
});

export default LabelBase;
