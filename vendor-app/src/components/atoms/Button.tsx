import React from 'react';
import { StyleSheet, Text, TextStyle, ViewStyle } from 'react-native';
import { Button as PaperButton } from 'react-native-paper';
import COLOR from '../assets/constants/theme';

interface IProps {
  onPress: () => void;
  style?: ViewStyle | ViewStyle[];
  textStyle?: TextStyle;
  label?: string;
  color?: string;
  disabled?: boolean;
  disabledColor?: string;
  icon?: string;
  loading?: boolean;
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    fontWeight: '500',
    color: COLOR.WHITE,
  },
});

const Button: React.FC<IProps> = (props) => {
  const {
    onPress,
    style,
    textStyle,
    label,
    color = COLOR.SLATE,
    disabled,
    disabledColor = COLOR.SLATE,
    icon,
    loading,
  } = props;

  return (
    <PaperButton
      mode="contained"
      onPress={onPress}
      style={style}
      disabled={disabled}
      contentStyle={{
        backgroundColor: disabled ? disabledColor : color,
        height: 48,
      }}
      icon={icon}
      loading={loading}
    >
      {label && !loading && (
        <Text style={[styles.text, textStyle]}>{label}</Text>
      )}
    </PaperButton>
  );
};

export default Button;
