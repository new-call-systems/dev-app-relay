import React from "react";
import { ViewStyle, StyleSheet } from 'react-native';
import NumericInput from "@wwdrew/react-native-numeric-textinput";

interface IProps {
  value: string;
  placeholder?: string;
  onChange: (str: string) => void;
  style?: ViewStyle;
}

const TextInputNumeric: React.FC<IProps> = (props) => {
  const {
    value,
    placeholder = '',
    onChange,
    style,
  } = props;

  return (
    <NumericInput
      style={[style, styles.text]}
      locale='ja-JP'
      decimalPlaces={0}
      maxLength={10}
      placeholder={placeholder}
      placeholderTextColor="#C6C6C6"
      value={value}
      onUpdate={onChange}
    />
  );
};

const styles = StyleSheet.create({
  text: {
    paddingTop: 0,
  },
});

export default TextInputNumeric;
