import React from "react";
import { View, StyleSheet } from 'react-native';
import { IImageObject } from "../../api/schema/request";
import Video from "./Video";
import Image from "./Image";

interface IProps {
  data: IImageObject[];
}

const S3Images: React.FC<IProps> = (props) => {
  const { data } = props;

  if ((data || []).length <= 0) {
    return null;
  }

  return (
    <View>
      {data.map((v: IImageObject, i) => {
        return (
          <View key={i} style={styles.media}>
            {v.type === 'image' &&
              <Image uri={v.url} style={styles.size}/>
            }
            {v.type === 'video' &&
              <Video uri={v.url} style={styles.size}/>
            }
          </View>
        )
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  media: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    marginLeft: 0,
    height: 160,
    width: '100%',
    backgroundColor: '#000000',
  },
  size: {
    height: 150,
    width: 200,
  },
});

export default S3Images;
