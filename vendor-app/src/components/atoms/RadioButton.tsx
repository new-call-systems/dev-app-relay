import React, { useState } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

interface IProps {
  list: {
    key: number,
    text: string
  }[];
  value: string;
  onChange: (str: string) => void;
}

const RadioButton: React.FC<IProps> = (props) => {
  const {
    list,
    value,
    onChange,
  } = props;

  return (
    <View style={styles.container}>
      {list.map(res => {
        const isOn = value === res.key;
        return (
          <View key={res.key} style={styles.radioGroup}>
            <TouchableOpacity
              style={[styles.radioCircle, isOn ? styles.radioCircleOn : {}]}
              onPress={() => onChange(res.key)}
            >
              <View style={styles.radioSelected}/>
            </TouchableOpacity>
            <Text style={[styles.radioText, isOn ? styles.radioTextOn : {}]}>
              {res.value}
            </Text>
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  radioGroup: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingRight: 15,
  },
  radioCircle: {
    height: 16,
    width: 16,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#adb5bd',
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioCircleOn: {
    borderColor: '#3687fc',
    backgroundColor: '#3687fc',
  },
  radioSelected: {
    width: 6,
    height: 6,
    borderRadius: 50,
    backgroundColor: '#ffffff',
  },
  radioText: {
    paddingLeft: 5,
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: '#464f69',
  },
  radioTextOn: {
    fontFamily: 'NotoSansJP_700Bold',
  },
});

export default RadioButton;
