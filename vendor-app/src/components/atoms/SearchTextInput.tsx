import React from 'react';
import { View, TextInput, Image, StyleSheet } from 'react-native';
import { PARTS_COLOR } from "../assets/constants/theme";

interface IProps {
  value?: string;
  onChangeText?: (text: string) => void;
  onSubmitEditing?: () => void;
}

const SearchTextInput: React.FC<IProps> = (props) => {
  const {
    value = '',
    onChangeText,
    onSubmitEditing,
  } = props;

  return (
    <View style={styles.container}>
      <View style={styles.sectionStyle}>
        <Image
          source={require('../assets/icon/btn-search.png')}
          style={styles.imageStyle}
        />
        <TextInput
          value={value}
          style={styles.inputStyle}
          underlineColorAndroid="transparent"
          clearButtonMode="always"
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
        />
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    height: 52,
    backgroundColor: PARTS_COLOR.SEARCH_CONTENTS_BACKGROUND_COLOR
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 33,
    borderRadius: 20,
    backgroundColor: PARTS_COLOR.SEARCH_CONTENTS_TEXT_BACKGROUND_COLOR,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: PARTS_COLOR.SEARCH_CONTENTS_TEXT_BORDER_COLOR,
    margin: 13,
  },
  imageStyle: {
    padding: 10,
    margin: 15,
    height: 26,
    width: 26,
  },
  inputStyle: {
    flex: 1,
  },
});

export default SearchTextInput;
