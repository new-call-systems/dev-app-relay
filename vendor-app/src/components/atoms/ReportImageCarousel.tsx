import React, { useState, useEffect } from "react";
import { StyleSheet, Dimensions } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { ImageObject } from "../../reducers/report";
import { IReportAwsConfig } from "../../api/schema/report";
import S3Storage from "../../utils/S3Storage";
import ModalImage from "./Image";
import LoadingImage from "./LoadingImage";

interface IProps {
  s3Config: IReportAwsConfig;
  data: ImageObject[];
}

// 親要素でpaddingLeft[16]を取っているので写真をセンターにするため調整
const CarouselWidth = Dimensions.get("window").width - 16;

// ローディング用
const DUMMY_LOADING_IMAGE_PATH = '@';

const ReportImageCarousel: React.FC<IProps> = (props) => {
  const {
    s3Config,
    data,
  } = props;

  const [images, setImages] = useState<string[]>([]);
  const showList = (data || []).filter(v => !v.remove);
  const isFetch = showList.length > 0;

  useEffect(() => {
    let isMounted = true;

    const setLoadingImage = () => {
      const list = showList.map(() => DUMMY_LOADING_IMAGE_PATH);
      if (isMounted) {
        setImages(list);
      }
    };

    const getImageList = async () => {
      showList.map(async (v: ImageObject, index: number) => {
        const img = v.add
          ? v.url
          : await new S3Storage(s3Config).getFileObject(v.key);

        if (isMounted) {
          setImages(prevState => {
            const nextState = [...prevState];
            nextState[index] = img;
            return nextState;
          });
        }
      });
    };

    if (isFetch) {
      setLoadingImage();
      getImageList();
    }

    return () => {
      isMounted = false;
    };
  }, [data]);

  // データ無し
  if (!isFetch) {
    return null;
  }

  return (
    <Carousel
      layout={'default'}
      itemWidth={290}
      sliderWidth={CarouselWidth}
      inactiveSlideScale={1}
      useScrollView={true}
      data={images}
      renderItem={(data) => {
        const { item } = data;
        return (
          <>
            {item === DUMMY_LOADING_IMAGE_PATH
              ? <LoadingImage style={styles.imageSize}/>
              : <ModalImage style={styles.imageSize} uri={item}/>
            }
          </>
        );
      }}
    />
  );
};

const styles = StyleSheet.create({
  imageSize: {
    height: 210,
    width: 280,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

export default ReportImageCarousel;
