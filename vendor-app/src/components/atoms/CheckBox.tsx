import React from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  View,
  ViewStyle,
  Text,
  TextStyle,
  Image,
} from 'react-native';
import COLOR from '../assets/constants/theme';

interface IProps {
  checked: boolean;
  style?: ViewStyle | ViewStyle[];
  textStyle?: TextStyle;
  label?: string;
  onPress: () => void;
}

const styles = StyleSheet.create({
  box: {
    width: 20,
    height: 20,
    backgroundColor: COLOR.CADET_BLUE,
    borderRadius: 2,
    padding: 1,
  },
  checkedView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  boxImage: {
    width: 20,
    height: 20,
  },
  uncheckedView: {
    flex: 1,
    backgroundColor: COLOR.WHITE,
  },
  labelText: {
    fontSize: 16,
    paddingLeft: 5,
    color: COLOR.CADET_BLUE,
  },
});

const checkedElement = (checked) => {
  if (checked) {
    return (
      <View style={styles.checkedView}>
        <Image
          style={styles.boxImage}
          source={require('../assets/icon/checkbox.png')}
        />
      </View>
    );
  }

  return <View style={styles.uncheckedView} />;
};

const Checkbox: React.FC<IProps> = (props) => {
  const { checked, style, textStyle, label, onPress } = props;
  const callbackOnPress = () => onPress(!checked);

  return (
    <TouchableHighlight
      onPress={callbackOnPress}
      underlayColor="transparent"
      style={[style, { marginVertical: 2 }]}
    >
      <View style={styles.row}>
        <View style={styles.box}>{checkedElement(checked)}</View>
        {label && <Text style={[styles.labelText, textStyle]}>{label}</Text>}
      </View>
    </TouchableHighlight>
  );
};

export default Checkbox;
