import React from 'react';
import { View, Image } from 'react-native';
import { PAGE_TYPE } from '../../constants/page';

interface IProps {
  name: string;
  focused: boolean;
  badge: boolean;
}

const convertNameToIcon = {
  [PAGE_TYPE.Request]: {
    on : require('../assets/icon/tab-new-on.png'),
    off : require('../assets/icon/tab-new-off.png'),
  },
  [PAGE_TYPE.Schedule]: {
    on : require('../assets/icon/tab-bidding-on.png'),
    off : require('../assets/icon/tab-bidding-off.png'),
  },
  [PAGE_TYPE.Complete]: {
    on : require('../assets/icon/tab-done-on.png'),
    off : require('../assets/icon/tab-done-off.png'),
  },
  [PAGE_TYPE.Report]: {
    on : require('../assets/icon/tab-report-on.png'),
    off : require('../assets/icon/tab-report-off.png'),
  },
};

const Badge = () => {
  return (
    <View
      style={{
        position: 'absolute',
        top: 6,
        right: 0,
      }}
    >
      <View
        style={{
          width: 18,
          height: 18,
          backgroundColor: "#f02849",
          borderRadius: 20,
        }}
      />
    </View>
  )
};

const NavigatorBottomTabIcon: React.FC<IProps> = (props) => {
  const {
    focused,
    name,
    badge,
  } = props;
  const active: string = focused ? 'on' : 'off';
  const file: string = convertNameToIcon[name][active];

  return (
    <View
      style={{
        width: 44,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
      }}
    >

      {/* アイコン */}
      <Image
        style={{width: 44, height: 44}}
        source={file}
        alt={props.name}
      />

      {/* バッジ */}
      {badge && <Badge/>}

    </View>
  );
};

export default NavigatorBottomTabIcon;
