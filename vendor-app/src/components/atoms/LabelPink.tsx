import React from 'react';
import { PARTS_COLOR } from '../assets/constants/theme';
import LabelBase from "./LabelBase";

interface IProps {
  children: React.ReactNode;
}

const LabelPink: React.FC<IProps> = (props) => {
  const {
    children,
  } = props;

  return (
    <LabelBase
      frameStyles={{
        backgroundColor: PARTS_COLOR.LABEL_REPORT_STATUS_BACKGROUND_COLOR,
      }}
      textStyles={{
        color: PARTS_COLOR.LABEL_REPORT_STATUS_FONT_COLOR,
      }}
    >
      {children}
    </LabelBase>
  );
};

export default LabelPink;
