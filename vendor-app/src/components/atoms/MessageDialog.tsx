import React from 'react';
import { Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { clear } from "../../reducers/message";

const MessageDialog: React.FC = () => {
  const dispatch = useDispatch();
  const message = useSelector((state) => state.message.message);
  if (message) {
    Alert.alert(
      'エラー',
      message,
      [{text: 'OK', onPress: () => dispatch(clear())}],
      {cancelable: false}
    );
  }
  return null;
};

export default MessageDialog;
