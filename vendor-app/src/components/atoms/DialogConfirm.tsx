import React from 'react';
import { ConfirmDialog } from 'react-native-simple-dialogs';

interface IProps {
  visible: boolean;
  onTouchOutside: () => void;
  onPressYes: () => void;
  onPressNo: () => void;
}

const DialogConfirm: React.FC<IProps> = (props) => {
  const {
    visible,
    onPressYes,
    onPressNo,
  } = props;

  return (
    <ConfirmDialog
      title="報告書提出"
      message="登録内容を保存して報告書を管理会社へ提出しますか？"
      visible={visible}
      positiveButton={{
        title: "提出する",
        onPress: onPressYes,
        titleStyle: {
          fontFamily: 'NotoSansJP_500Medium',
          fontSize: 16,
          color: "#00cf67",
        },
      }}
      negativeButton={{
        title: "キャンセル",
        onPress: onPressNo,
        titleStyle: {
          fontFamily: 'NotoSansJP_500Medium',
          fontSize: 16,
          color: "#464f69",
        }
      }}
      dialogStyle={{
      }}
      titleStyle={{
        fontFamily: 'NotoSansJP_700Bold',
        fontSize: 16,
        textAlign: "left",
        color: "#8692b5",
        marginTop: 13,
        marginRight: 24,
        marginBottom: 10,
        marginLeft: 24,
      }}
      contentStyle={{
        borderStyle: "solid",
        borderTopWidth: 1,
        borderTopColor: "#00000011",
        paddingTop: 20,
        paddingRight: 24,
        paddingBottom: 20,
        paddingLeft: 24,
      }}
      messageStyle={{
        fontFamily: 'NotoSansJP_500Medium',
        fontSize: 14,
        textAlign: "left",
        color: "#8692b5",
      }}
      buttonsStyle={{
      }}
    />
  );
};

export default DialogConfirm;
