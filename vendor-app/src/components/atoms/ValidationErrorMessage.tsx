import React from "react";
import { View, Image, Text, StyleSheet } from 'react-native';
import { PARTS_COLOR } from "../assets/constants/theme";

interface IProps {
  children: React.ReactNode;
}

const ValidationErrorMessage: React.FC<IProps> = (props) => {
  const { children } = props;

  return (
    <View style={styles.area}>
      <Image
        style={styles.image}
        source={require('../assets/icon/icon-error.png')}
      />
      <Text style={styles.text}>
        {children}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  area: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    height: 30,
  },
  image: {
    width: 21,
    height: 21,
  },
  text: {
    marginLeft: 2,
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 11,
    color: PARTS_COLOR.TEXT_VALIDATION_ERROR_FONT_COLOR,
  },
});

export default ValidationErrorMessage;
