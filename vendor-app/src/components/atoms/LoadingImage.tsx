import React from "react";
import { ActivityIndicator, StyleSheet, View, ViewStyle } from "react-native";

interface IProps {
  style?: ViewStyle;
}

const LoadingImage: React.FC<IProps> = (props) => {
  const {style = {}} = props;

  return (
    <View style={[styles.container, style]}>
      <ActivityIndicator
        size="small"
        color="#999999"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    backgroundColor: '#e6e6e6',
  },
});

export default LoadingImage;
