import React from 'react';
import { PARTS_COLOR } from '../assets/constants/theme';
import LabelBase from "./LabelBase";

const LabelRequired: React.FC = () => {
  return (
    <LabelBase
      frameStyles={{
        backgroundColor: PARTS_COLOR.LABEL_REQUIRED_BACKGROUND_COLOR,
        borderRadius: 0,
        width: 24,
        height: 12,
      }}
      textStyles={{
        color: PARTS_COLOR.LABEL_REQUIRED_STATUS_FONT_COLOR,
        fontSize: 8,
      }}
    >
      必須
    </LabelBase>
  );
};

export default LabelRequired;
