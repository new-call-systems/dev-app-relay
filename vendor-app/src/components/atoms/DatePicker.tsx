import React, { useState } from "react";
import { View, ViewStyle, Text, TouchableOpacity, StyleSheet } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { FormatType, formatDate, convertStrDay2Date, convertStrTime2Date } from "../../utils/date";

interface IProps {
  mode: 'date' | 'time';
  value: string;
  onChange: (str: string) => void;
  style?: ViewStyle;
}

const DatePicker: React.FC<IProps> = (props) => {
  const {
    mode,
    value,
    onChange,
    style,
  } = props;

  const [isVisible, setVisibility] = useState(false);
  const showDatePicker = () => setVisibility(true);
  const hideDatePicker = () => setVisibility(false);
  const placeholder = mode === 'date' ? '年/月/日' : 'hh:mm';
  const formatType = mode === 'date' ? FormatType.VIEW_YMD : FormatType.VIEW_HM;
  const date = mode === 'date' ? convertStrDay2Date(value) : convertStrTime2Date(value);
  const viewDate = (value == null || value === '') ? '' : formatDate(date, formatType);

  const onConfirm = (date: Date) => {
    hideDatePicker();
    onChange(formatDate(date, formatType));
  };

  return (
    <View>
      <TouchableOpacity
        onPress={showDatePicker}
        style={[styles.area, style]}
      >
        {(() => {
          if (viewDate === '') {
            return <Text style={styles.placeholder}>{placeholder}</Text>;
          } else {
            return <Text style={styles.text}>{viewDate}</Text>;
          }
        })()}
      </TouchableOpacity>
      <DateTimePickerModal
        mode={mode}
        date={date}
        locale="ja_JP"
        cancelTextIOS="キャンセル"
        confirmTextIOS="決定"
        headerTextIOS="選択してください"
        isDarkModeEnabled={false}
        isVisible={isVisible}
        onConfirm={onConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  placeholder: {
    fontSize: 12,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 12,
    color: "#c7c7c7",
  },
  text: {
    fontSize: 12,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 12,
    color: "#464f69",
  },
});

export default DatePicker;
