import React from "react";
import { Text, StyleSheet } from 'react-native';
import { openURL } from 'expo-linking';
import COLOR from "../assets/constants/theme";

interface IProps {
  children: string;
}

const styles = StyleSheet.create({
  text: {
    color: COLOR.DODGER_BLUE,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: COLOR.DODGER_BLUE,
  },
});

const TelLink: React.FC<IProps> = (props) => {
  const tel = props.children;

  const _handlePress = () => {
    openURL(`tel:${tel}`);
  };

  return (
    <Text
      style={styles.text}
      onPress={_handlePress}
    >
      {tel}
    </Text>
  );
};

export default TelLink;
