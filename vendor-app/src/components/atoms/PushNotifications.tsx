import React, { useEffect, useRef } from 'react';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import { Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import { setToken, sendDeviceId } from "../../reducers/notification";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const PushNotifications: React.FC = () => {
  const dispatch = useDispatch();
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    // デバイストークンを取得してStoreに保管
    registerForPushNotificationsAsync().then(token => {
      dispatch(setToken(token));
      dispatch(sendDeviceId());
    });

    // アプリがフォアグラウンド時にプッシュ通知を受信した場合
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      // バックグラウンドで通知
      if (notification.origin === 'selected') {
        // const url = notification.data.url || '';
      }
      // フォアグラウンドで通知
      if (notification.origin === 'received') {
      }
    });

    // ユーザがプッシュ通知をタップした場合
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  return null;
};

export default PushNotifications;

async function registerForPushNotificationsAsync() {
  let token = null;
  try {
    // 実機でない場合はなにもしない
    if (!Constants.isDevice) {
      return;
    }

    // プッシュ通知のパーミッションを取得
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );

    // プッシュ通知の許可を表示
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(
        Permissions.NOTIFICATIONS
      );
      finalStatus = status;
    }

    // プッシュ通知が許可されなかった場合なにもしない
    if (finalStatus !== 'granted') {
      return;
    }

    // Expo 用のデバイストークンを取得
    token = (await Notifications.getExpoPushTokenAsync()).data;

    // Androidの場合はChannelを指定
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('cocoen', {
        name: 'cocoen',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

  } catch (e) {
    // なにもしない
  }

  return token;
}
