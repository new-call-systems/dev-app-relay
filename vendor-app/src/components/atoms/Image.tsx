import React, { useState } from 'react';
import { Image, TouchableOpacity, ViewStyle } from 'react-native';
import ImageView from "react-native-image-viewing";

interface IProps {
  uri: string;
  style?: ViewStyle;
}

const CustomImage: React.FC<IProps> = (props) => {
  const { uri, style } = props;
  const [visible, setIsVisible] = useState(false);

  const onPressImage = () => {
    setIsVisible(true)
  };

  const onPressClose = () => {
    setIsVisible(false)
  };

  if (visible) {
    return (
      <ImageView
        images={[{ uri: uri }]}
        imageIndex={0}
        visible={visible}
        onRequestClose={onPressClose}
      />
    );
  }

  return (
    <TouchableOpacity onPress={onPressImage}>
      <Image
        source={{ uri: uri }}
        style={style}
        resizeMode={'cover'}
      />
    </TouchableOpacity>
  );
};

export default CustomImage;
