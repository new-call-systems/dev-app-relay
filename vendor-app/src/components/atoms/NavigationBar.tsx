import React from 'react';
import { StyleSheet, Platform, SafeAreaView } from 'react-native';
import Constants from 'expo-constants';
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import COLOR from '../assets/constants/theme';

type IProps = {
  children?: React.ReactNode;
};

const styles = StyleSheet.create({
  safe_area: {
    flex: 1,
    backgroundColor: COLOR.SLATE,
    paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight,
  },
});

const NavigationVar: React.FC<IProps> = (props) => {
  return (
    <ActionSheetProvider>
      <SafeAreaView style={styles.safe_area}>
        {props.children}
      </SafeAreaView>
    </ActionSheetProvider>
  );
};

export default NavigationVar;
