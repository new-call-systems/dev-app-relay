import React from 'react';
import { StyleSheet, KeyboardAvoidingView, Platform, StatusBar } from 'react-native';
import { useHeaderHeight } from '@react-navigation/stack';
import { ScrollView } from '../atoms';
import COLOR from '../assets/constants/theme';

interface IProps {
  children: React.ReactNode;
}

const styles = StyleSheet.create({
  keyboard: {
    flex: 1,
  }
});

const Form: React.FC<IProps> = (props) => {
  const headerHeight = useHeaderHeight();
  const verticalOffset = headerHeight + StatusBar.currentHeight;

  return (
      <ScrollView backgroundColor={COLOR.IVORY}>
        <KeyboardAvoidingView
          style={styles.keyboard}
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          keyboardVerticalOffset={verticalOffset}
          enabled
        >
        {props.children}
        </KeyboardAvoidingView>
      </ScrollView>
  );
};

export default Form;
