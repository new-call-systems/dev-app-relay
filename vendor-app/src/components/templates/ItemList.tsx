import React, {useState} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  Text,
  FlatList,
  TouchableHighlight,
  ScrollView,
  RefreshControl,
} from 'react-native';
import COLOR, { PARTS_COLOR } from '../assets/constants/theme';
import { Loading, SearchTextInput } from "../atoms";
import { IRequestDetailState } from '../../reducers/request'

interface IProps {
  data: IRequestDetailState[];
  opPress?: void;
  refreshing: boolean;
  onRefresh: any;
  renderItem: void;
  nextPageLoading: boolean;
  onEndReached: void;
  noDataText: string;
  textSearch: string;
  onSearchText: void;
}

const dismiss = (): void => Keyboard.dismiss();

const ItemList: React.FC<IProps> = (props) => {
  const {
    data,
    onPress = null,
    refreshing,
    onRefresh,
    renderItem,
    nextPageLoading,
    onEndReached,
    noDataText = 'データがありません。',
    textSearch,
    onSearchText,
  } = props;
  const [textFilterValue, setTextFilterValue] = useState(textSearch);

  const _onRefresh = () => {
    //setTextFilterValue('');
    onRefresh();
  };

  // フィルタ用テキストChange
  const _onChangeText = (value: string) => {
    setTextFilterValue(value);
    if (value === '' && textSearch !== '') {
      onSearchText(value);
    }
  };

  const _onSubmitEditing = () => {
    if (textFilterValue !== textSearch) {
      onSearchText(textFilterValue);
    }
  };

  // 行タッチ
  const _onPress = (key) => {
    if (onPress) {
      onPress(key);
    }
  };

  // 最下部までスクロール
  const _onEndReached = () => {
    if (textFilterValue !== '') {
      return;
    }
    onEndReached();
  };

  // データ無し
  if (data == null || data.length <= 0) {
    return (
      <ScrollView
        style={styles.noData}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      >
        <SearchTextInput
          value={textFilterValue}
          onChangeText={_onChangeText}
          onSubmitEditing={_onSubmitEditing}
        />
        <View style={styles.sectionBorder}/>
        <Text style={styles.noDataText}>
          {noDataText}
        </Text>
      </ScrollView>
    );
  }

  return (
    <TouchableWithoutFeedback onPress={dismiss}>
      <View style={styles.container}>
        <SearchTextInput
          value={textFilterValue}
          onChangeText={_onChangeText}
          onSubmitEditing={_onSubmitEditing}
        />
        <View style={styles.sectionBorder}/>
        <FlatList
          style={styles.list}
          data={data}
          refreshing={refreshing}
          onRefresh={_onRefresh}
          keyExtractor={(item) => `${item.id}`}
          renderItem={({ item }) => (
            <TouchableHighlight
              style={styles.item}
              onPress={() => _onPress(item.id)}
              underlayColor={PARTS_COLOR.CONTENTS_BACKGROUND_COLOR}
            >
              {renderItem(item)}
            </TouchableHighlight>
          )}
          onEndReachedThreshold={0.3}
          onEndReached={_onEndReached}
        />
        {nextPageLoading && <Loading/>}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: PARTS_COLOR.CONTENTS_BACKGROUND_COLOR,
  },
  sectionBorder: {
    marginTop: 3,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: COLOR.LIGHT_GRAY,
  },
  list: {
    marginTop: 5,
    marginBottom: 5,
  },
  item: {
    marginTop: 6,
    marginLeft: 17,
    marginRight: 17,
    marginBottom: 6,
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    shadowColor: "rgba(0, 0, 0, 0.16)",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 6,
    shadowOpacity: 1,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: COLOR.CHINESE_SILVER,
  },
  noData: {
    flex: 1,
    backgroundColor: PARTS_COLOR.CONTENTS_BACKGROUND_COLOR,
  },
  noDataText: {
    marginTop: 61,
    marginLeft: 'auto',
    marginRight: 'auto',
    fontFamily: 'NotoSansJP_700Bold',
    fontSize: 16,
    color: COLOR.SLATE,
  },
});

export default ItemList;
