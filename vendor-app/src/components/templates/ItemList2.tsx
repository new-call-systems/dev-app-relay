import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  RefreshControl,
} from 'react-native';
import { PARTS_COLOR } from '../assets/constants/theme';
import { Loading } from "../atoms";
import { IRequestDetailState } from '../../reducers/request'

interface IProps {
  data: IRequestDetailState[];
  opPress: void;
  refreshing: boolean;
  onRefresh: any;
  renderItem: void;
  nextPageLoading: boolean;
  onEndReached: void;
}

const ItemList: React.FC<IProps> = (props) => {
  const {
    data,
    onPress,
    refreshing,
    onRefresh,
    renderItem,
    nextPageLoading,
    onEndReached,
  } = props;

  const NoData = () => {
    return (
      <ScrollView
        style={styles.noData}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
    );
  };

  const _onRefresh = () => {
    onRefresh();
  };

  // データ無し
  if (data == null || data.length <= 0) {
    return <NoData/>
  }

  // 最下部までスクロール
  const _onEndReached = () => {
    onEndReached();
  };

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.list}
        refreshing={refreshing}
        onRefresh={_onRefresh}
        onEndReachedThreshold={0.3}
        onEndReached={_onEndReached}
        data={data}
        keyExtractor={(item) => `${item.id}`}
        renderItem={({item}) => renderItem(item)}
      />
      {nextPageLoading && <Loading/>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: PARTS_COLOR.CONTENTS_BACKGROUND_COLOR,
  },
  list: {
    marginBottom: 30,
  },
  noData: {
    flex: 1,
    backgroundColor: PARTS_COLOR.CONTENTS_BACKGROUND_COLOR,
  },
});

export default ItemList;
