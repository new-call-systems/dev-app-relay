import React from 'react';
import { StyleSheet, View } from 'react-native';
import { ScrollView } from '../atoms';
import COLOR from '../assets/constants/theme';

interface IProps {
  children: React.ReactNode;
}

const ItemDetail: React.FC<IProps> = (props) => {
  return (
    <View style={styles.wrapper}>
      <ScrollView>
        <View style={styles.area}>
          <View style={styles.contents}>
            {props.children}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: COLOR.IVORY,
  },
  area: {
    marginTop: 21,
    marginRight: 17,
    marginLeft: 17,
    marginBottom: 10,
  },
  contents: {
    paddingTop: 16,
    paddingRight: 18,
    paddingBottom: 28,
    paddingLeft: 24,
    backgroundColor: "#ffffff",
    shadowColor: "rgba(0, 0, 0, 0.16)",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 6,
    shadowOpacity: 1,
    borderRadius: 10,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#c7c7c7",
  }
});

export default ItemDetail;
