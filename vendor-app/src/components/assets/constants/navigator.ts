import { Platform } from 'react-native';
import { PARTS_COLOR } from './theme';

const getHeight = () => {
  let styles: object = {
    backgroundColor: PARTS_COLOR.HEADER_BACKGROUND_COLOR,
  };
  if (Platform.OS === 'ios') {
    styles = {...styles, height: 66};
  }
  return styles;
};

const getContainerStyle = () => {
  return {
    paddingTop: Platform.OS === 'ios' ? 20 : 10,
  };
};

// ヘッダー
export const StackOptions = {
  headerMode: 'screen',
  screenOptions: {

    // ヘッダーの高さ、背景色を設定
    headerStyle: getHeight(),

    // ヘッダーの文字色
    headerTintColor: PARTS_COLOR.HEADER_FONT_COLOR,

    // ヘッダータイトルの縦位置
    headerTitleContainerStyle: getContainerStyle(),

    // ヘッダータイトルの横位置
    headerTitleAlign: 'center',

    // ヘッダータイトルのテキスト
    headerTitleStyle: {
      fontFamily: 'NotoSansJP_500Medium',
      fontSize: 16,
      includeFontPadding: false,
    },

    // headerBackTitleStyle: getContainerStyle(),
    headerLeftContainerStyle: getContainerStyle(),
    headerRightContainerStyle: getContainerStyle(),

    // 必要な画面は個別で設定する
    headerBackTitleVisible: false,
  },
};

// フッター
export const TabBarOptions = {
  activeTintColor: PARTS_COLOR.FOOTER_FONT_COLOR_ACTIVE,
  inactiveTintColor: PARTS_COLOR.FOOTER_FONT_COLOR_INACTIVE,
  style: {
    backgroundColor: PARTS_COLOR.FOOTER_BACKGROUND_COLOR,
    height: 68,
  },
  labelStyle: {
    paddingBottom: 10,
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 10,
    includeFontPadding: false,
  },
};
