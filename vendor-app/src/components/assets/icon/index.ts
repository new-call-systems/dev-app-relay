const btnCancel = require('./btn-cancel.png');
const btnPhotoAdd = require('./btn-photo-add.png');
const btnPhotoDelete = require('./btn-photo-delete.png');
const btnSearch = require('./btn-search.png');
const checkbox = require('./checkbox.png');
const iconBill = require('./icon-bill.png');
const iconDate = require('./icon-date.png');
const iconError = require('./icon-error.png');
const iconLibrary = require('./icon-library.png');
const iconMan = require('./icon-man.png');
const iconTime = require('./icon-time.png');
const noImage = require('./no-image.png');
const tabBiddingOff = require('./tab-bidding-off.png');
const tabBiddingOn = require('./tab-bidding-on.png');
const tabDoneOff = require('./tab-done-off.png');
const tabDoneOn = require('./tab-done-on.png');
const tabNewOff = require('./tab-new-off.png');
const tabNewOn = require('./tab-new-on.png');
const tabReportOff = require('./tab-report-off.png');
const tabReportOn = require('./tab-report-on.png');
const iconUrgent = require('./icon-urgent.png');

const icons = {
  btnCancel,
  btnPhotoAdd,
  btnPhotoDelete,
  btnSearch,
  checkbox,
  iconBill,
  iconDate,
  iconError,
  iconLibrary,
  iconMan,
  iconTime,
  noImage,
  tabBiddingOff,
  tabBiddingOn,
  tabDoneOff,
  tabDoneOn,
  tabNewOff,
  tabNewOn,
  tabReportOff,
  tabReportOn,
  iconUrgent,
};

export default icons;
