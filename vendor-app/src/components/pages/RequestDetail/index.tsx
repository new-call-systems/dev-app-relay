import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { CheckBox, Loading, S3Images } from '../../atoms';
import { useAsync } from "../../../hooks/useAsync";
import { FormatType, formatTZ } from "../../../utils/date";
import TemplateItemDetail from '../../templates/ItemDetail';
import styles from '../RequestDetail/styles';
import {
  getDetail,
  setOpenDetail,
  registerBid,
  time2Text,
  setForceRequest,
} from '../../../reducers/request';

type IForm = {
  preferableSelected1: boolean;
  preferableSelected2: boolean;
  preferableSelected3: boolean;
};

const RequestDetail: React.FC = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const [enableSubmit, setEnableSubmit] = useState(false);
  const [asyncAction, loading] = useAsync();
  const { requestId } = route.params;
  const { control, handleSubmit, setValue } = useForm<IForm>();
  const item = useSelector((state) => state.request.dataOpenDetail);
  const timeSection = useSelector((state) => state.request.timeSection);

  useEffect(() => {
    (async () => {
      asyncAction(
        getDetail(requestId),
        (data) => dispatch(setOpenDetail(data))
      )
    })();
  }, [requestId]);

  const emergencyFlg = item?.emergencyFlg || false;
  useEffect(() => {
    setEnableSubmit(emergencyFlg);
  }, [emergencyFlg]);

  const onSubmit = (data) => {
    if (!enableSubmit) {
      return;
    }
    const preferableId = findPreferableId(data);
    asyncAction(
      registerBid(requestId, preferableId),
      successGoBack(navigation, dispatch)
    );
  };

  const successGoBack = (navigation, dispatch) => {
    return () => {
      navigation.goBack();
      dispatch(setForceRequest('Open'));
    }
  };

  const findPreferableId = (data): number | null => {
    return data.preferableSelected1
      ? item.preferableInfo[0].request_preferable_time_id : data.preferableSelected2
      ? item.preferableInfo[1].request_preferable_time_id : data.preferableSelected3
      ? item.preferableInfo[2].request_preferable_time_id : null;
  };

  if (!item) {
    return null;
  }

  if (loading) {
    return <Loading />
  }

  return (
    <TemplateItemDetail>

      {/* 問合せ内容、入居者情報 */}
      <View style={styles.sectionRow}>
        <Text style={[styles.sectionContext, styles.label]}>詳細</Text>
        {(item.emergencyFlg) &&
          <View style={[styles.sectionRow, styles.sectionEmergency]}>
            <Image
              style={styles.iconEmergency}
              source={require('../../assets/icon/icon-urgent.png')}
            />
            <Text style={styles.textEmergency}>緊急</Text>
          </View>
        }
      </View>
      <Text style={styles.text}>
        {item.context}
      </Text>
      <View style={styles.sectionResident}>
        <Text style={styles.label}>入居者情報</Text>
        <Text style={[styles.sectionProperty, styles.text]}>
          {item.propertyAddress} {item.propertyName}
        </Text>
      </View>

      <View style={styles.sectionBorder}/>

      {/* 予約日程 */}
      {(() => {
        if (!item.emergencyFlg) {
          return (
            <>
              <Text style={styles.label}>予約日程</Text>
              {item.preferableInfo.map((v, i) => {
                const num = i + 1;
                return (
                  <View key={v.request_preferable_time_id}>
                    <View style={styles.sectionRow}>
                      <View style={styles.listPreferable}/>
                      <Text style={styles.text}>第{num}候補</Text>
                      <Controller
                        control={control}
                        name={`preferableSelected${num}`}
                        defaultValue={v.selected}
                        render={({onChange, value}) => (
                          <CheckBox
                            checked={value}
                            onPress={(value) => {
                              setValue("preferableSelected1", false);
                              setValue("preferableSelected2", false);
                              setValue("preferableSelected3", false);
                              onChange(value);
                              setEnableSubmit(value);
                            }}
                            style={styles.checkbox}
                          />
                        )}
                      />
                    </View>
                    <View style={styles.sectionRow}>
                      <Image
                        style={styles.iconPreferable}
                        source={require('../../assets/icon/icon-date.png')}
                      />
                      <Text style={styles.textS}>
                        {formatTZ(v.date, FormatType.VIEW_YMD)}
                      </Text>
                    </View>
                    <View style={styles.sectionPreferableIcon}/>
                    <View style={styles.sectionRow}>
                      <Image
                        style={styles.iconPreferable}
                        source={require('../../assets/icon/icon-time.png')}
                      />
                      <Text style={styles.textS}>
                        {time2Text(v.time_section_id, timeSection)}
                      </Text>
                    </View>
                  </View>
                );
              })}
              <View style={styles.sectionBorder}/>
            </>
          )
        }
      })()}

      {/* 画像-動画 */}
      <S3Images data={item.images}/>

      {/* 説明 */}
      <Guide
        emergencyFlg={item.emergencyFlg}
        enableSubmit={enableSubmit}
      />

      {/* 入札ボタン */}
      <View style={styles.sectionButton}>
        <TouchableOpacity
          style={[styles.button, enableSubmit && styles.buttonEnabled]}
          onPress={handleSubmit(onSubmit)}
        >
          <Text style={styles.buttonText}>入札</Text>
        </TouchableOpacity>
      </View>

    </TemplateItemDetail>
  );
};

const Guide = ({emergencyFlg, enableSubmit}) => {
  if (emergencyFlg) {
    return (
      <Text style={styles.text}>
        お客様からのお問合せにご対応いただける場合、[入札ボタン]を押して、入札を完了してください。
      </Text>
    )
  }
  if (enableSubmit) {
    return (
      <Text style={styles.text}>
        お客様からのお問合せにご対応いただける場合、予約日程を選択し、[入札ボタン]を押して、入札を完了してください。
      </Text>
    )
  }
  return (
    <>
      <Text style={styles.text}>
        お客様からのお問合せにご対応いただける場合、
        <Text style={styles.textGuide}>
          予約日程を選択し、
        </Text>
        [入札ボタン]を押して、入札を完了してください。
      </Text>
    </>
  )
};

export default RequestDetail;
