import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  sectionRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sectionContext: {
    marginTop: 5,
  },
  sectionEmergency: {
    marginLeft: 'auto',
  },
  iconEmergency: {
    width: 28,
    height: 28
  },
  textEmergency: {
    marginTop: 4,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 12,
    color: COLOR.CHILEAN_FIRE,
    includeFontPadding: false,
  },
  sectionResident: {
    marginTop: 17,
  },
  sectionProperty: {
    marginTop: 5,
  },
  sectionPreferableIcon: {
    marginTop: 2,
  },
  sectionBorder: {
    marginTop: 21,
    marginBottom: 16,
    borderStyle: "solid",
    borderTopWidth: 1,
    borderColor: COLOR.CHINESE_SILVER,
  },
  label: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  text: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textS: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  listPreferable: {
    marginLeft: 14,
    marginRight: 7,
    width: 13,
    height: 13,
    backgroundColor: COLOR.SLATE,
    borderRadius: 2,
  },
  iconPreferable: {
    marginLeft: 34,
    marginRight: 7,
    width: 20,
    height: 20,
  },
  checkbox: {
    marginTop: 5,
    marginLeft: 'auto',
    marginRight: 40,
  },
  sectionButton: {
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 230,
    height: 40,
    backgroundColor: COLOR.LIGHT_GRAY,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonEnabled: {
    backgroundColor: COLOR.MALACHITE,
  },
  buttonText: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 16,
    color: COLOR.WHITE,
  },
  textGuide: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.CHILEAN_FIRE,
    includeFontPadding: false,
  },
});

export default styles;
