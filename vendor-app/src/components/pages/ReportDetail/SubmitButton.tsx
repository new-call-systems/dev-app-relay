import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { PARTS_COLOR } from "../../assets/constants/theme";

interface IProps {
  onSubmit: () => void;
  userName: string;
}

const SubmitButton: React.FC<IProps> = (props) => {
  const {
    onSubmit,
    userName,
  } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        上記の内容で作業を実施しました
      </Text>
      <Text style={styles.userName}>
        作業担当者名{' 　'}{userName}
      </Text>
      <View style={styles.block}>
        <TouchableOpacity
          style={styles.button}
          onPress={onSubmit}
        >
          <Text style={styles.text}>
            提出する
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 14,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  userName: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  block: {
    paddingTop: 20,
  },
  button: {
    width: 280,
    height: 48,
    marginLeft: 'auto',
    marginRight: 16,
    borderRadius: 24,
    backgroundColor: '#00cf67',
  },
  text: {
    marginTop: 'auto',
    marginRight: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 19,
    color: '#ffffff',
    includeFontPadding: false,
  },
});

export default SubmitButton;
