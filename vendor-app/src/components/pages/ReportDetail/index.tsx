import React, { useState, useLayoutEffect } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { HeaderBackButton } from '@react-navigation/stack';

import { useAsync } from "../../../hooks/useAsync";
import { PAGE_TYPE } from "../../../constants/page";
import SubmitButton from "./SubmitButton";
import styles from "./styles";
import {
  LabelPink,
  LabelYellow,
  DialogConfirm,
  ReportImageCarousel,
  LabelNumeric,
  Loading,
} from "../../atoms";
import {
  convertStrTime2Date,
  formatDate,
  FormatType,
  formatTZ
} from "../../../utils/date";
import {
  ACTION_SECTION,
  ACTION_STATUS,
  reportSave,
  reportSubmit,
  setUnPreview,
  setForceRequest,
} from "../../../reducers/report";

const ReportDetail: React.FC = ({ navigation }) => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const [asyncAction, loading] = useAsync();
  const [dialogVisible, setDialogVisible] = useState(false);
  const item = useSelector((state) => state.report.dataDetail);
  const isPreviewMode = useSelector((state) => state.report.isPreviewMode);

  if (!item) {
    return null;
  }

  // ヘッダのボタンを個別に定義
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: (props) => (
        <HeaderBackButton
          {...props}
          onPress={() => headerLeftButton()}
        />
      ),
      headerRight: () => headerRightButton(),
    });
  }, [navigation]);

  const headerLeftButton = () => {
    if (isPreviewMode) {
      dispatch(setUnPreview());
    }
    navigation.goBack();
  };

  const headerRightButton = () => {
    // 編集可能かチェック
    if (item.reportStatus !== 'writing') {
      return null;
    }
    // プレビューかチェック
    if (isPreviewMode) {
      return (
        <TouchableOpacity
          onPress={onPressHeaderRightSave}
          style={styles.headerButton}
        >
          <Text style={styles.headerButtonText}>保存する</Text>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        onPress={onPressHeaderRightEdit}
        style={styles.headerButton}
      >
        <Text style={styles.headerButtonText}>編集する</Text>
      </TouchableOpacity>
    )
  };

  const onPressHeaderRightEdit = () => {
    navigate(`${PAGE_TYPE.ReportForm}`);
  };

  const onPressHeaderRightSave = () => {
    asyncAction(
      reportSave(item),
      successGoBack(navigation, dispatch)
    );
  };

  const onSubmit = () => {
    setDialogVisible(true);
  };

  const onSubmitCancel = () => {
    setDialogVisible(false);
  };

  const onSubmitAccept = () => {
    setDialogVisible(false);
    asyncAction(
      reportSubmit(item),
      successGoBack(navigation, dispatch)
    );
  };

  const successGoBack = (navigation, dispatch) => {
    return () => {
      navigate(`${PAGE_TYPE.Report}`);
      dispatch(setForceRequest());
    }
  };

  if (loading) {
    return <Loading />
  }

  return (
    <View style={styles.container}>
      {/* 提出ダイアログ */}
      <DialogConfirm
        visible={dialogVisible}
        onPressYes={onSubmitAccept}
        onPressNo={onSubmitCancel}
      />
      <ScrollView style={styles.main}>
        <View style={styles.dateAndState}>
          <View style={styles.labelDate}>
            <Text style={styles.labelTextMedium}>
              受付日
            </Text>
            <Text style={styles.labelTextMedium}>
              {formatTZ(item.receptionDate, FormatType.VIEW_YMD)}
            </Text>
          </View>
          <View style={styles.labelStep}>
            {(() => {
              if (item.actionSection) {
                return (
                  <LabelYellow>
                    {ACTION_SECTION[item.actionSection]}
                  </LabelYellow>
                );
              }
            })()}
          </View>
          <View style={styles.labelStatus}>
            {(() => {
              if (item.actionStatus) {
                return (
                  <LabelPink>
                    {ACTION_STATUS[item.actionStatus]}
                  </LabelPink>
                );
              }
            })()}
          </View>
        </View>
        <View style={styles.managementCompany}>
          <Text style={styles.labelTextBold1}>
            {item.managementCompanyName}
          </Text>
        </View>
        <View style={styles.residentAddress}>
          <View>
            <Text style={styles.labelTextBold2}>
              {item.propertyName}
            </Text>
          </View>
          <View>
            <Text style={styles.labelTextBold1}>
              {item.roomNo}号室
            </Text>
          </View>
        </View>
        <View style={styles.report}>
          <View style={styles.reportDetail}>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-man.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                お客様名
              </Text>
              <View style={styles.flexWordWrap}>
                <Text style={[styles.reportDetailText, styles.labelTextMedium]}>
                  {item.residentName} 様
                </Text>
              </View>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-date.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                工事日
              </Text>
              <Text style={[styles.reportDetailText, styles.labelTextMedium]}>
                {formatTZ(item.actionDate, FormatType.VIEW_YMD)}
              </Text>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-time.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                対応時間
              </Text>
              <Text style={[styles.reportDetailText, styles.labelTextMedium]}>
                {(() => {
                  if (item.startingTime) {
                    const startingTime = formatDate(convertStrTime2Date(item.startingTime), FormatType.VIEW_HM);
                    const endingTime = formatDate(convertStrTime2Date(item.endingTime), FormatType.VIEW_HM);
                    return `${startingTime} - ${endingTime}`;
                  }
                })()}
              </Text>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-bill.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                工事金額
              </Text>
              <LabelNumeric style={[styles.reportDetailText, styles.labelTextMedium]}>
                {item.constructionAmount}
              </LabelNumeric>
            </View>
          </View>
          <View style={styles.reportDetailPhoto}>
            <ReportImageCarousel
              s3Config={item.s3Config}
              data={item.imageObjectList}
            />
          </View>
          <View style={styles.reportDetailContext}>
            <Text style={styles.labelTextDescription}>
              {item.description}
            </Text>
          </View>
          {isPreviewMode &&
            <SubmitButton
              onSubmit={onSubmit}
              userName={item.userName}
            />
          }
          <View style={{marginBottom: 90}}/>
        </View>
      </ScrollView>
      {(() => {
        if (!isPreviewMode) {
          return (
            <View style={styles.footer}>
              <Text style={styles.footerTextTitle}>
                上記の内容で作業を実施しました
              </Text>
              <Text style={styles.footerTextUserName}>
                作業担当者名{' 　'}{item.userName}
              </Text>
            </View>
          );
        }
      })()}
    </View>
  );
};

export default ReportDetail;
