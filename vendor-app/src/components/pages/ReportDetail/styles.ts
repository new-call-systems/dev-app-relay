import { StyleSheet } from 'react-native';
import { PARTS_COLOR } from '../../assets/constants/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: PARTS_COLOR.REPORT_LIST_BACKGROUND_COLOR,
  },
  main: {
    paddingLeft: 16,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    height: 68,
    width: '100%',
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#e6e6e6",
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelTextMedium: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  labelTextBold1: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_TITLE_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  labelTextBold2: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 14,
    color: PARTS_COLOR.LABEL_TITLE_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  labelTextDescription: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_DESCRIPTION_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  dateAndState: {
    height: 49,
    flexDirection: 'row',
  },
  labelDate: {
    paddingTop: 7,
    width: 170,
  },
  labelStep: {
    paddingTop: 15,
    width: 70,
  },
  labelStatus: {
    paddingTop: 15,
    marginLeft: 5,
    width: 100,
  },
  managementCompany: {
    marginBottom: 4,
  },
  residentAddress: {
    marginBottom: 10,
  },
  report: {
  },
  reportDetail: {
  },
  reportDetailRow: {
    flexDirection: 'row',
    marginTop: 2,
    marginBottom: 2,
  },
  reportDetailIcon: {
    height: 20,
    width: 20,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportDetailTitle: {
    width: 70,
    marginLeft: 4,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportDetailText: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportDetailPhoto: {
    marginTop: 10,
    marginBottom: 17,
    // 親要素でpaddingLeft[16]を取っているので写真をセンターにするため調整
    marginRight: 16,
  },
  reportPhoto: {
    height: 210,
    width: 280,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  reportDetailContext: {
    paddingRight: 15,
  },
  footerTextTitle: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 14,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  footerTextUserName: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  headerButton: {
    marginRight: 8,
    backgroundColor: "#464f69",
  },
  headerButtonText: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 16,
    color: "#ffffff",
    includeFontPadding: false,
  },
  flexWordWrap: {
    flexShrink: 1,
  },
});

export default styles;
