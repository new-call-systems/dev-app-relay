import React from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { login } from '../../../reducers/auth';
import { TextField, Button, ValidationErrorMessage } from '../../atoms';
import TemplateForm from '../../templates/Form';
import styles from './styles';

export type IForm = {
  email: string;
  password: string;
};

const SignIn: React.FC = () => {
  const dispatch = useDispatch();
  const authLoading = useSelector((state) => state.auth.loading);
  const { control, handleSubmit, errors } = useForm<IForm>();
  const onSubmit = (data) => dispatch(login(data));

  return (
    <TemplateForm>
      <View style={styles.container}>
        {/* Email */}
        <View style={styles.email}>
          <Text style={styles.label}>
            作業員ID(メールアドレス)
          </Text>
          <Controller
            control={control}
            name="email"
            rules={{ required: true }}
            defaultValue=""
            render={({ onChange, value }) => (
              <TextField
                autoCompleteType="email"
                value={value}
                onChangeText={(value) => onChange(value)}
                keyboardType="email-address"
                style={[
                  styles.text,
                  errors.email && styles.textErrorStyle
                ]}
              />
            )}
          />
          {errors.email && (
            <ValidationErrorMessage>
              IDを入力してください。
            </ValidationErrorMessage>
          )}
        </View>

        {/* Password */}
        <View style={styles.password}>
          <Text style={styles.label}>
            パスワード
          </Text>
          <Controller
            control={control}
            name="password"
            rules={{ required: true }}
            defaultValue=""
            render={({ onChange, value }) => (
              <TextField
                autoCompleteType="password"
                secureTextEntry={true}
                value={value}
                onChangeText={(value) => onChange(value)}
                style={[
                  styles.text,
                  errors.password && styles.textErrorStyle
                ]}
              />
            )}
          />
          {errors.password && (
            <ValidationErrorMessage>
              パスワードを入力してください。
            </ValidationErrorMessage>
          )}
        </View>

        {/* Submit */}
        <Button
          label="ログイン"
          style={styles.button}
          onPress={handleSubmit(onSubmit)}
          loading={authLoading}
        />
      </View>
    </TemplateForm>
  );
};

export default SignIn;
