import { StyleSheet } from 'react-native';
import COLOR, { PARTS_COLOR } from '../../assets/constants/theme';

const styles = StyleSheet.create({
  container: {
    marginTop: 59,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    paddingLeft: 5,
    marginBottom: 1,
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 14,
    color: COLOR.BLUE_GREY,
  },
  text: {
    width: 320,
    height: 48,
    paddingLeft: 8,
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 18,
    color: COLOR.SLATE,
    borderRadius: 3,
    backgroundColor: COLOR.WHITE,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: COLOR.PATTENS_BLUE,
  },
  email: {
  },
  password: {
    marginTop: 35,
  },
  button: {
    marginTop: 121,
    width: 280,
    borderRadius: 30,
  },
  textErrorStyle: {
    borderColor: PARTS_COLOR.TEXT_VALIDATION_ERROR_BORDER_COLOR,
  },
});

export default styles;
