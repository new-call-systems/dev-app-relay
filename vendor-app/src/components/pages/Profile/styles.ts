import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  titleText: {
    fontSize: 20,
    fontWeight: '500',
    color: '#259afb',
  },
  description: {
    marginBottom: 10,
  },
  label: {
    color: '#259afb',
    fontSize: 16,
  },
  button: {
    marginTop: 70,
  },
  lastName: {
    marginTop: 30,
  },
  firstName: {
    marginTop: 30,
  },
  emergencyTel: {
    marginTop: 30,
  },
  textError: {
    marginTop: 5,
    color: '#ff0000',
  },
});

export default styles;
