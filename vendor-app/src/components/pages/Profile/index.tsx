import React from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { TextField, Button } from '../../atoms';
import TemplateForm from '../../templates/Form';
import { registerProfile, completeProfile } from '../../../reducers/vendor';
import { setForceRequest } from '../../../reducers/request';
import { useAsync } from "../../../hooks/useAsync";
import styles from './styles';

export type IForm = {
  lastName: string;
  firstName: string;
  emergencyTel: string;
};

const Profile: React.FC = () => {
  const dispatch = useDispatch();
  const [asyncAction, loading] = useAsync();
  const vendor = useSelector((state) => state.vendor);

  const onSubmit = (data) => {
    const {userId, vendorId} = vendor;
    asyncAction(
      registerProfile(userId, vendorId, data),
      successGoMain(dispatch)
    );
  };

  const successGoMain = (dispatch) => {
    return () => {
      dispatch(setForceRequest());
      dispatch(completeProfile());
    }
  };

  const { control, handleSubmit, errors } = useForm<IForm>({
    defaultValues: {
      firstName: vendor.firstName,
      lastName: vendor.lastName,
      emergencyTel: vendor.emergencyTel,
    },
  });

  return (
    <TemplateForm>
      <View style={styles.container}>

        {/* 説明 */}
        <View style={styles.description}>
          <Text style={styles.label}>
            アプリケーションを使用する前に、以下の情報を入力してください。
          </Text>
        </View>

        {/* 氏名（姓） */}
        <View style={styles.lastName}>
          <Text style={styles.label}>氏名（姓）</Text>
          <Controller
            control={control}
            name="lastName"
            rules={{ required: true }}
            defaultValue=""
            render={({ onChange, value }) => (
              <TextField
                label=""
                value={value}
                onChangeText={(value) => onChange(value)}
                style={styles.text}
                autoCompleteType="name"
              />
            )}
          />
          {errors.lastName && (
            <Text style={styles.textError}>氏名（姓）を指定してください。</Text>
          )}
        </View>

        {/* 氏名（名） */}
        <View style={styles.firstName}>
          <Text style={styles.label}>氏名（名）</Text>
          <Controller
            control={control}
            name="firstName"
            rules={{ required: true }}
            defaultValue=""
            render={({ onChange, value }) => (
              <TextField
                label=""
                value={value}
                onChangeText={(value) => onChange(value)}
                style={styles.text}
                autoCompleteType="name"
              />
            )}
          />
          {errors.firstName && (
            <Text style={styles.textError}>氏名（名）を指定してください。</Text>
          )}
        </View>

        {/* 緊急時連絡先 */}
        <View style={styles.emergencyTel}>
          <Text style={styles.label}>緊急時連絡先</Text>
          <Controller
            control={control}
            name="emergencyTel"
            rules={{ required: true }}
            defaultValue=""
            render={({ onChange, value }) => (
              <TextField
                label=""
                value={value}
                onChangeText={(value) => onChange(value)}
                style={styles.text}
                autoCompleteType="tel"
                keyboardType="email-address"
              />
            )}
          />
          {errors.emergencyTel && (
            <Text style={styles.textError}>
              緊急時連絡先を指定してください。
            </Text>
          )}
        </View>

        {/* 登録ボタン */}
        <Button
          label="→"
          style={styles.button}
          onPress={handleSubmit(onSubmit)}
          loading={loading}
        />
      </View>
    </TemplateForm>
  );
};

export default Profile;
