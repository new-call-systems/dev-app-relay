import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  sectionRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStatus: {
    marginTop: 2,
    marginRight: 10,
    marginLeft: 'auto',
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.MALACHITE,
    includeFontPadding: false,
  },
  text: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textB: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textS: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  textM: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 14,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  textLabel: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    width: 40,
    includeFontPadding: false,
  },
  sectionLabel: {
    marginTop: 1,
    marginBottom: 1,
  },
  sectionBorder: {
    marginTop: 17,
    marginBottom: 17,
    borderStyle: "solid",
    borderTopWidth: 1,
    borderColor: COLOR.CHINESE_SILVER,
  },
  sectionButton: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 230,
    height: 40,
    backgroundColor: COLOR.MALACHITE,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 16,
    color: COLOR.WHITE,
    includeFontPadding: false,
  },
  sectionResident: {
    marginTop: 17,
    marginBottom: 3,
  },
  sectionPreferable: {
    marginTop: 11,
    marginBottom: 3,
  },
  sectionUserName: {
    marginTop: 11,
    marginBottom: 3,
  },
  textMessage: {
    marginBottom: 20,
  },
  textMessageStrong: {
    fontFamily: "NotoSansJP_700Bold",
    color: COLOR.RUSTY_RED,
    includeFontPadding: false,
    marginBottom: 20,
  },
  sectionImage: {
    marginTop: 20,
  },
  flexWordWrap: {
    flexShrink: 1,
  },
});

export default styles;
