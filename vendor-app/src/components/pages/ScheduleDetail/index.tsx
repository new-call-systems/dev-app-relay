import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from "react-i18next";
import TemplateItemDetail from '../../templates/ItemDetail';
import { Loading, S3Images, TelLink } from "../../atoms";
import { useAsync } from "../../../hooks/useAsync";
import { RequestStatus, requestStatus } from "../../../localization/label.i18n";
import styles from './styles';
import {
  acceptCancel,
  getDetail,
  setScheduleDetail,
  setForceRequest,
  selectPreferable,
  preferable2Text,
  requestComplete,
} from '../../../reducers/request';

const ScheduleDetail: React.FC = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [asyncAction, loading] = useAsync();
  const { requestId } = route.params;
  const item = useSelector((state) => state.request.dataScheduleDetail);
  const timeSection = useSelector((state) => state.request.timeSection);

  useEffect(() => {
    (async () => {
      asyncAction(
        getDetail(requestId),
        (data) => dispatch(setScheduleDetail(data))
      )
    })();
  }, [requestId]);

  const onSubmit = () => {
    if (isCanceled()) {
      asyncAction(
        acceptCancel(requestId),
        successGoBack(navigation, dispatch)
      );
    } else {
      asyncAction(
        requestComplete(requestId),
        successGoBack(navigation, dispatch)
      );
    }
  };

  const successGoBack = (navigation, dispatch) => {
    return () => {
      navigation.goBack();
      dispatch(setForceRequest('Schedule'));
    }
  };

  const isCanceled = (): boolean => {
    return item.status === RequestStatus.waiting_for_cancel;
  };

  if (!item) {
    return null;
  }

  if (loading) {
    return <Loading />
  }

  return (
    <TemplateItemDetail>

      {/* 問合せ内容、入居者情報 */}
      <View style={styles.sectionRow}>
        <Text style={styles.textB}>詳細</Text>
        <Text style={styles.textStatus}>
          {t([requestStatus,RequestStatus[item.status]].join("."))}
        </Text>
      </View>
      <Text style={styles.text}>
        {item.context}
      </Text>

      {/* 入居者情報 */}
      <Text style={[styles.sectionResident, styles.textB]}>入居者情報</Text>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>氏名</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.residentName}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>住所</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.propertyAddress}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>物件</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.propertyName}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>部屋</Text>
        <Text style={styles.textS}>
          {'：  '}
          {item.roomNo}
        </Text>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>連絡先</Text>
        <Text style={styles.textS}>
          {'：  '}
          {(() => {
            if (item.isAdmin) {
              return <TelLink>{item.residentTel}</TelLink>;
            } else {
              return item.residentTel
            }
          })()}
        </Text>
      </View>

      {/* 予約日程 */}
      <Text style={[styles.sectionPreferable, styles.textB]}>予約日程</Text>
      <Text style={styles.textM}>
        {(() => {
          if (item.emergencyFlg) {
            return '緊急対応';
          } else {
            const preferable = selectPreferable(item.preferableInfo);
            return preferable2Text(timeSection, preferable);
          }
        })()}
      </Text>

      {/* 作業担当者名 */}
      <Text style={[styles.sectionUserName, styles.textB]}>作業担当者名</Text>
      <Text style={styles.textM}>
        {item.userName}
      </Text>

      {/* 画像-動画 */}
      {((item.images || []).length > 0) &&
        <View style={styles.sectionImage}>
          <S3Images data={item.images}/>
        </View>
      }

      <View style={styles.sectionBorder}/>

      {/* メッセージ１ */}
      <Message1
        status={item.status}
        emergency={item.emergencyFlg}
        isAdmin={item.isAdmin}
      />

      {/* メッセージ２ */}
      <Message2
        status={item.status}
        isAdmin={item.isAdmin}
      />

      {/* 対応済みボタン */}
      <SubmitButton
        status={item.status}
        isAdmin={item.isAdmin}
        onSubmit={onSubmit}
      />

    </TemplateItemDetail>
  );
};

const Message1 = ({ status, emergency, isAdmin }) => {
  if (!isAdmin) {
    return null;
  }
  if (status !== RequestStatus.bid) {
    if (emergency) {
      return (
        <Text style={styles.textMessageStrong}>
          入居者へ電話連絡してください。
        </Text>
      )
    }
    return (
      <Text style={[styles.textMessage, styles.text]}>
        さらに詳細確認が必要な場合、連絡先から入居者へ電話連絡してください。
      </Text>
    )
  }
  return null;
};

const Message2 = ({ status, isAdmin }) => {
  if (status === RequestStatus.bid) {
    return (
      <Text style={[styles.textMessage, styles.text]}>
        入居者様の入札承認待ちです。
      </Text>
    )
  }
  if (!isAdmin) {
    if (status === RequestStatus.waiting_for_cancel) {
      return (
        <Text style={[styles.textMessage, styles.text]}>
          入居者様からキャンセルがありました。
        </Text>
      )
    } else {
      return null;
    }
  }
  if (status === RequestStatus.waiting_for_cancel) {
    return (
      <Text style={[styles.textMessage, styles.text]}>
        入居者様からキャンセルがありました。「キャンセルの承認」ボタンを押して承認してください。
      </Text>
    )
  }
  if (status === RequestStatus.scheduling) {
    return (
      <Text style={[styles.textMessage, styles.text]}>
        <Text style={styles.textB}>
          対応完了後は[対応済み]ボタン
        </Text>
        を押して通知してください。また、画面下部の
        <Text style={styles.textB}>
          完了報告
        </Text>
        メニューから完了報告を行ってください。
      </Text>
    )
  }
  return null;
};

const SubmitButton = ({ status, isAdmin, onSubmit }) => {
  if (!isAdmin) {
    return (
      <Text style={styles.textMessageStrong}>
        担当外のお問合せです。
      </Text>
    );
  }
  const target = [RequestStatus.waiting_for_cancel, RequestStatus.scheduling];
  if (!target.includes(status)) {
    return null;
  }
  const msg = status === RequestStatus.waiting_for_cancel
    ? 'キャンセルの承認'
    : '対応済み';
  return (
    <View style={styles.sectionButton}>
      <TouchableOpacity
        style={styles.button}
        onPress={onSubmit}
      >
        <Text style={styles.buttonText}>{msg}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ScheduleDetail;
