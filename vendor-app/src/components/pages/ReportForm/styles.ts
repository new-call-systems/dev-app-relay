import { StyleSheet } from 'react-native';
import { PARTS_COLOR } from "../../assets/constants/theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: PARTS_COLOR.REPORT_FORM_BACKGROUND_COLOR,
  },
  main: {
    paddingLeft: 16,
    paddingRight: 16,
  },
  labelAndInputField: {
    paddingTop: 13,
  },
  textFieldActionDate: {
    paddingTop: 13,
  },
  textFieldActionDateBlock: {
    position: 'relative',
  },
  textFieldActionDateSize: {
    width: 280,
    height: 31,
  },
  textFieldActionDateIconPosition: {
    position: 'absolute',
    left: 250,
    top: 6,
  },
  textFieldActionDateIconSize: {
    width: 20,
    height: 20,
  },
  textFieldActionTime: {
    paddingTop: 19,
  },
  textFieldActionTimeDelimiter: {
    width: 22,
    textAlign: 'center',
    paddingTop: 10,
  },
  textFieldActionTimeRow: {
    flexDirection: 'row',
  },
  textFieldActionTimeSize: {
    width: 128,
    height: 31,
  },
  textFieldConstructionAmount: {
    paddingTop: 5,
  },
  textFieldConstructionAmountRow: {
    flexDirection: 'row',
  },
  textFieldConstructionAmountSize: {
    width: 280,
    height: 31,
  },
  textFieldConstructionAmountUnit: {
    textAlign: 'center',
    paddingLeft: 4,
    paddingTop: 10,
    fontSize: 14,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 12,
    color: "#464f69",
  },
  textFieldDescription: {
    paddingTop: 13,
  },
  textFieldDescriptionSize: {
    width: 343,
    height: 100,
    textAlignVertical: 'top',
  },
  textFieldPhoto: {
    paddingTop: 20,
  },
  textFieldUserName: {
    paddingTop: 11,
  },
  textFieldUserNameSize: {
    width: 280,
    height: 31,
    paddingTop: 0,
  },
  textFieldActionSection: {
    paddingTop: 13,
  },
  textFieldStatus: {
    paddingTop: 13,
  },
  textStyle: {
    paddingTop: 9,
    paddingLeft: 6,
    borderRadius: 3,
    backgroundColor: "#ffffff",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#dee2e6",
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: "#464f69",
    includeFontPadding: false,
  },
  textErrorStyle: {
    borderColor: PARTS_COLOR.TEXT_VALIDATION_ERROR_BORDER_COLOR,
  },
  headerButton: {
    marginRight: 8,
    backgroundColor: "#464f69",
  },
  headerButtonText: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 16,
    color: "#ffffff",
    includeFontPadding: false,
  },
  textRemarks: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 11,
    color: "#98a4c6",
    paddingRight: 8,
    includeFontPadding: false,
  },
});

export default styles;
