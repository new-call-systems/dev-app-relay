import React, { useState, useLayoutEffect, useRef } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, ScrollView, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from 'react-hook-form';
import * as ImagePicker from "expo-image-picker";
import * as ImageManipulator from 'expo-image-manipulator';
import { useActionSheet } from '@expo/react-native-action-sheet'
import { FormatType, formatTZ } from "../../../utils/date";
import { PAGE_TYPE } from "../../../constants/page";
import { setAlertMessage } from "../../../reducers/message";
import styles from "./styles";
import {
  LabelRequired,
  RadioButton,
  DatePicker,
  TextInputNumeric,
  ReportImageEdit,
  ValidationErrorMessage
} from "../../atoms";
import {
  ACTION_SECTION,
  ACTION_STATUS,
  setPreview,
  ImageObject
} from "../../../reducers/report";

type IForm = {
  actionDate: string;
  startingTime: string;
  endingTime: string;
  constructionAmount: number;
  description: string;
  userName: string;
  actionSection: string;
  actionStatus: string;
};

const ImagePickerOption = {
  mediaTypes: ImagePicker.MediaTypeOptions.Images,
  allowsEditing: false,
  aspect: [4, 3],
  quality: 1,
  exif: true,
};

const ACTION_SHEET_INDEX_CAMERA = 0;
const ACTION_SHEET_INDEX_LIBRARY = 1;
const ACTION_SHEET_INDEX_CANCEL = 2;
const ActionSheetOption = {
  title: '写真をアップロードする方法を選択してください。',
  options: ['写真を撮る', 'フォトライブラリ', 'キャンセル'],
  cancelButtonIndex: ACTION_SHEET_INDEX_CANCEL,
};

const ReportForm: React.FC = ({ navigation }) => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const item = useSelector((state) => state.report.dataDetail);
  const vendorUserName = useSelector((state) => `${state.vendor.lastName}　${state.vendor.firstName}`);
  const RadioSection = Object.entries(ACTION_SECTION).map(([key, value]) => ({key, value}));
  const RadioStatus = Object.entries(ACTION_STATUS).map(([key, value]) => ({key, value}));
  const [images, setImages] = useState(item.imageObjectList);
  const processing = useRef(false);
  const { showActionSheetWithOptions } = useActionSheet();

  const { control, handleSubmit, errors, setValue } = useForm<IForm>({
    defaultValues: {
      actionDate: item.actionDate,
      startingTime: item.startingTime,
      endingTime: item.endingTime,
      constructionAmount: item.constructionAmount,
      description: item.description,
      userName: vendorUserName,
      actionSection: item.actionSection,
      actionStatus: item.actionStatus,
      imageObjectList: item.imageObjectList,
    },
  });

  // ヘッダのボタンを個別に定義
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={handleSubmit(onSubmit)}
          style={styles.headerButton}
        >
          <Text style={styles.headerButtonText}>プレビュー</Text>
        </TouchableOpacity>
    ),
    });
  }, [navigation]);

  // 写真アップロード 2重起動対策
  const onClickCamera = (e) => {
    e.preventDefault();
    if (processing.current) {
      return;
    }
    if (images.filter(i => !i.remove).length >= 10) {
      dispatch(setAlertMessage('10枚以上の写真はアップロードできません'));
      return;
    }
    processing.current = true;
    setTimeout(() => {
      showActionSheet();
    }, 200);
  };

  // カメラ or 写真選択のダイアログを表示
  const showActionSheet = () => {
    showActionSheetWithOptions(ActionSheetOption, async (index: number) => {
      switch (index) {
        case ACTION_SHEET_INDEX_CAMERA:
          await takePhoto();
          break;
        case ACTION_SHEET_INDEX_LIBRARY:
          await pickImage();
          break;
      }
      processing.current = false;
    });
  };

  // カメラ起動
  const takePhoto = async () => {
    const { status } = await ImagePicker.requestCameraPermissionsAsync();
    if (status !== 'granted') {
      dispatch(setAlertMessage('カメラの使用を許可してください'));
      return;
    }
    let result = await ImagePicker.launchCameraAsync(ImagePickerOption);
    if (result.cancelled) {
      return;
    }
    await addImage(result);
  };

  // ライブラリから写真を選択
  const pickImage = async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      dispatch(setAlertMessage('写真へのアクセスを許可してください'));
      return;
    }
    let result = await ImagePicker.launchImageLibraryAsync(ImagePickerOption);
    if (result.cancelled) {
      return;
    }
    // exifチェック
    // 緯度、軽度、高度のどれかあればOKとする
    const { GPSLatitude, GPSLongitude, GPSAltitude } = result.exif;
    if (!GPSLatitude && !GPSLongitude && !GPSAltitude) {
      dispatch(setAlertMessage('位置情報がないため使用できません'));
      return;
    }
    await addImage(result);
  };

  // 画像を追加
  const addImage = async (file) => {
    const limitSize = 600;
    const { width, height } = file;
    let resize = {};
    if (width < height) {
      resize = limitSize < height ? {height: limitSize} : {};
    } else {
      resize = limitSize < width ? {width: limitSize} : {};
    }

    const photo = await ImageManipulator.manipulateAsync(
      file.uri,
      [{ resize: resize }],
      { compress: 0.5 }
    );

    const image: ImageObject = {
      key: photo.uri,
      url: photo.uri,
      add: true,
      remove: false,
    };
    const newImages = images.concat(image);
    setImages(newImages);
    setValue("imageObjectList", newImages);
  };

  // 削除ボタンクリック 2重起動対策
  const onClickRemove = (e, index) => {
    e.preventDefault();
    if (processing.current) {
      return;
    }
    processing.current = true;
    setTimeout(() => {
      removeImage(index);
    }, 200);
  };

  // 画像を削除
  const removeImage = (index) => {
    let newImages = [...images];
    const img = {
      ...newImages[index],
      remove: true
    };
    newImages[index] = img;
    setImages(newImages);
    setValue("imageObjectList", newImages);
    processing.current = false;
  };

  // 提出
  const onSubmit = (data) => {
    dispatch(setPreview(data));
    navigate(`${PAGE_TYPE.ReportPreview}`);
  };

  return (
    <View style={styles.container}>
      <ScrollView style={styles.main}>
        <LabelField label="受付日">
          {formatTZ(item.receptionDate, FormatType.VIEW_YMD)}
        </LabelField>
        <LabelField label="管理会社名">
          {item.managementCompanyName}
        </LabelField>
        <LabelField label="物件名">
          {item.propertyName}
        </LabelField>
        <LabelField label="部屋番号">
          {item.roomNo}号室
        </LabelField>
        <LabelField label="お客様名">
          {item.residentName} 様
        </LabelField>

        {/* 工事日 */}
        <View style={styles.textFieldActionDate}>
          <Label required={true}>工事日</Label>
          <View style={styles.textFieldActionDateBlock}>
            <Controller
              control={control}
              name="actionDate"
              render={({ onChange, value }) => (
                <DatePicker
                  mode="date"
                  value={value}
                  rules={{
                    required: true,
                  }}
                  onChange={(value) => onChange(value)}
                  style={[
                    styles.textFieldActionDateSize,
                    styles.textStyle,
                    errors.actionDate && styles.textErrorStyle
                  ]}
                />
              )}
            />
            <View style={styles.textFieldActionDateIconPosition}>
              <Image
                style={styles.textFieldActionDateIconSize}
                source={require('../../assets/icon/icon-date.png')}
              />
            </View>
          </View>
          {errors.actionDate && (
            <ValidationErrorMessage>
              工事日を入力してください。
            </ValidationErrorMessage>
          )}
        </View>

        {/* 対応時間 */}
        <View style={styles.textFieldActionTime}>
          <Label>対応時間</Label>
          <View style={styles.textFieldActionTimeRow}>
            <Controller
              control={control}
              name="startingTime"
              render={({ onChange, value }) => (
                <DatePicker
                  mode="time"
                  value={value}
                  onChange={(value) => onChange(value)}
                  style={[
                    styles.textFieldActionTimeSize,
                    styles.textStyle,
                    errors.startingTime && styles.textErrorStyle
                  ]}
                />
              )}
            />
            <Text style={styles.textFieldActionTimeDelimiter}>－</Text>
            <Controller
              control={control}
              name="endingTime"
              render={({ onChange, value }) => (
                <DatePicker
                  mode="time"
                  value={value}
                  onChange={(value) => onChange(value)}
                  style={[
                    styles.textFieldActionTimeSize,
                    styles.textStyle,
                    errors.endingTime && styles.textErrorStyle
                  ]}
                />
              )}
            />
          </View>
        </View>

        {/* 工事金額 */}
        <View style={styles.textFieldConstructionAmount}>
          <Label>工事金額</Label>
          <View style={styles.textFieldConstructionAmountRow}>
            <Controller
              control={control}
              name="constructionAmount"
              rules={{
                min: 0,
                max: 99999999,
              }}
              render={({ onChange, value }) => (
                <TextInputNumeric
                  placeholder={'10,000'}
                  value={value}
                  onChange={(value) => onChange(value)}
                  style={[
                    styles.textFieldConstructionAmountSize,
                    styles.textStyle,
                    errors.constructionAmount && styles.textErrorStyle
                  ]}
                />
              )}
            />
            <Text style={styles.textFieldConstructionAmountUnit}>円</Text>
          </View>
          {errors.constructionAmount && (
            <ValidationErrorMessage>
              工事金額を正しく入力してください。
            </ValidationErrorMessage>
          )}
        </View>

        {/* 作業内容 */}
        <View style={styles.textFieldDescription}>
          <Label required={true}>作業内容</Label>
          <Controller
            control={control}
            name="description"
            rules={{
              required: true,
              validate: {
                line15: v => v.split("\n").length <= 16
              }
            }}
            render={({ onChange, value }) => (
              <TextInput
                label=""
                multiline
                placeholder={'作業した内容を記載してください。'}
                value={value}
                onChangeText={(value) => onChange(value)}
                style={[
                  styles.textFieldDescriptionSize,
                  styles.textStyle,
                  errors.description && styles.textErrorStyle
                ]}
              />
            )}
          />
          {errors.description && (
            <ValidationErrorMessage>
              {errors.description.type === 'required' && '作業内容を入力してください。'}
              {errors.description.type === 'line15' && '改行数は15個以下にしてください。'}
            </ValidationErrorMessage>
          )}
        </View>

        {/* 現場画像 */}
        <View style={styles.textFieldPhoto}>
          <Label>現場画像</Label>
          <Text style={styles.textRemarks}>10枚まで写真を載せることが可能です。</Text>
          <Text style={styles.textRemarks}>それ以上の画像を載せる場合は管理サイトをご利用下さい。</Text>
          <Controller
            control={control}
            name="imageObjectList"
          />
          <ReportImageEdit
            s3Config={item.s3Config}
            data={images}
            onPress={onClickCamera}
            onRemove={onClickRemove}
          />
        </View>

        {/* 作業担当者 */}
        <View style={styles.textFieldUserName}>
          <Controller
            control={control}
            name="userName"
            render={({ value }) => (
              <LabelField label="作業担当者名">
                {value}
              </LabelField>
            )}
          />
        </View>

        {/* 対応区分 */}
        <View style={styles.textFieldActionSection}>
          <Label>対応区分</Label>
          <Controller
            control={control}
            name="actionSection"
            render={({ onChange, value }) => (
              <RadioButton
                list={RadioSection}
                value={value}
                onChange={(value) => onChange(value)}
              />
            )}
          />
        </View>

        {/* ステータス */}
        <View style={styles.textFieldStatus}>
          <Label>ステータス</Label>
          <Controller
            control={control}
            name="actionStatus"
            render={({ onChange, value }) => (
              <RadioButton
                list={RadioStatus}
                value={value}
                onChange={(value) => onChange(value)}
              />
            )}
          />
        </View>
        <View style={{marginBottom: 30}}/>
      </ScrollView>
    </View>
  )
};

const Label = (props) => {
  const {
    required = false,
    children,
  } = props;

  const styles = StyleSheet.create({
    area: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    label: {
      fontFamily: "NotoSansJP_500Medium",
      fontSize: 12,
      color: "#8692b5",
      paddingRight: 8,
      includeFontPadding: false,
    },
  });

  return (
    <View style={styles.area}>
      <Text style={styles.label}>{children}</Text>
      {required && <LabelRequired/>}
    </View>
  );
};

const LabelField = (props) => {
  const {
    label,
    children,
  } = props;

  const styles = StyleSheet.create({
    area: {
      paddingTop: 13,
    },
    text: {
      fontFamily: "NotoSansJP_500Medium",
      fontSize: 12,
      color: "#464f69",
      includeFontPadding: false,
    },
    breakLine: {
      width: '100%',
      borderStyle: "solid",
      borderWidth: 1,
      borderColor: "#e6e6e6",
    },
  });

  return (
    <View style={styles.area}>
      <Label>{label}</Label>
      <Text style={styles.text}>{children}</Text>
      <View style={styles.breakLine} />
    </View>
  );
};

export default ReportForm;
