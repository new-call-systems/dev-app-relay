import React, { useEffect, useCallback } from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useTranslation } from "react-i18next";
import { PAGE_TYPE } from '../../../constants/page';
import TemplateItemList from '../../templates/ItemList';
import { FormatType, formatTZ } from "../../../utils/date";
import { useAsync } from "../../../hooks/useAsync";
import { RequestStatus, requestStatus } from "../../../localization/label.i18n";
import { getBadge } from "../../../reducers/notification";
import styles from './styles';
import {
  getList,
  setScheduleList,
  appendScheduleList,
  selectPreferable,
  preferable2Text,
  changeSearchText,
  REQUEST_TYPE,
} from '../../../reducers/request';

const Schedule: React.FC = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const [asyncActionRefresh, loadingRefresh] = useAsync();
  const [asyncActionNextPage, loadingNextPage] = useAsync();
  const timeSection = useSelector((state) => state.request.timeSection);
  const list = useSelector((state) => state.request.dataSchedule);
  const currentPage = useSelector((state) => state.request.dataScheduleCurrentPage);
  const lastPage = useSelector((state) => state.request.dataScheduleLastPage);
  const forceRequest = useSelector((state) => state.request.forceRequestSchedule);
  const textQuery = useSelector((state) => state.request.textSchedule);

  useFocusEffect(
    useCallback(() => {
      dispatch(getBadge());
    }, [])
  );

  useEffect(() => {
    if (forceRequest) {
      onRefresh();
    }
  }, [forceRequest]);

  const onRefresh = async () => {
    asyncActionRefresh(
      getList(REQUEST_TYPE.SCHEDULE, textQuery, 0),
      refreshSuccess(dispatch)
    );
  };

  const refreshSuccess = (dispatch) => {
    return (data) => {
      dispatch(setScheduleList(data));
      dispatch(getBadge(REQUEST_TYPE.SCHEDULE));
    }
  };

  const onEndReached = () => {
    const nextPage = currentPage + 1;
    if (lastPage < nextPage) {
      return;
    }
    if (loadingNextPage) {
      return;
    }
    asyncActionNextPage(
      getList(REQUEST_TYPE.SCHEDULE, textQuery, nextPage),
      (data) => dispatch(appendScheduleList(data))
    );
  };

  const onSearchText = (value) => {
    dispatch(changeSearchText({type: REQUEST_TYPE.SCHEDULE, text: value}));
  };

  const onPress = (requestId) => {
    navigate(`${PAGE_TYPE.ScheduleDetail}`, { requestId: requestId });
  };

  const renderItem = (item) => {
    return (
      <>
        <View style={styles.rowUpper}>
          <Text style={[styles.text, styles.textDate]}>
            {'受付日時: '}
            {formatTZ(item.createdAt, FormatType.VIEW_YMDHM)}
          </Text>
          <Text style={[styles.text, styles.textStatus]}>
            {t([requestStatus,RequestStatus[item.status]].join("."))}
          </Text>
        </View>
        <View style={styles.rowMiddle}>
          <Text
            style={styles.textContext}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {item.context}
          </Text>
        </View>
        <View style={styles.sectionBorder}/>
        <View style={styles.rowLower}>
          <Message
            status={item.status}
            emergencyFlg={item.emergencyFlg}
            preferableInfo={item.preferableInfo}
            timeSection={timeSection}
          />
        </View>
      </>
    );
  };

  return (
    <TemplateItemList
      data={list}
      refreshing={loadingRefresh}
      onRefresh={onRefresh}
      onPress={onPress}
      renderItem={renderItem}
      nextPageLoading={loadingNextPage}
      onEndReached={onEndReached}
      noDataText={'現在の予約はありません。'}
      textSearch={textQuery}
      onSearchText={onSearchText}
    />
  );
};

const Message = ({status, emergencyFlg, preferableInfo, timeSection}) => {
  if (status === 'waiting_for_cancel') {
    return (
      <Text style={[styles.text, styles.textMessage]}>
        お問合せがキャンセルされました。
      </Text>
    )
  }
  if (emergencyFlg) {
    return (
      <Text style={[styles.text, styles.textMessage]}>
        <Text style={styles.textMessageStrong}>
          緊急対応
        </Text>
        してください。
      </Text>
    )
  }
  const preferable = selectPreferable(preferableInfo);
  return (
    <Text style={[styles.text, styles.textMessage]}>
      <Text style={styles.textMessageStrong}>
        {preferable2Text(timeSection, preferable)}
      </Text>
      に対応してください。
    </Text>
  )
};

export default Schedule;
