import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  rowUpper: {
    flexDirection: 'row',
  },
  text: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    includeFontPadding: false,
  },
  textDate: {
    marginTop: 10,
    marginLeft: 15,
    color: COLOR.BLUE_GREY,
  },
  textStatus: {
    marginTop: 10,
    marginLeft: 'auto',
    marginRight: 18,
    color: COLOR.MALACHITE,
  },
  rowMiddle: {
  },
  rowLower: {
    paddingRight: 15,
    paddingBottom: 18,
    paddingLeft: 15,
  },
  sectionBorder: {
    marginTop: 11,
    marginBottom: 11,
    marginRight: 12,
    marginLeft: 12,
    borderStyle: "solid",
    borderTopWidth: 1,
    borderColor: COLOR.CHINESE_SILVER,
  },
  textContext: {
    marginTop: 3,
    marginLeft: 15,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textMessage: {
    color: COLOR.SLATE,
  },
  textMessageStrong: {
    fontFamily: "NotoSansJP_700Bold",
    includeFontPadding: false,
  },
});

export default styles;
