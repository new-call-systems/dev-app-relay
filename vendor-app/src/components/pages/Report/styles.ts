import { StyleSheet } from 'react-native';
import { PARTS_COLOR } from '../../assets/constants/theme';

const styles = StyleSheet.create({
  container: {
    height: 250,
    paddingLeft: 16,
    marginBottom: 1,
    backgroundColor: PARTS_COLOR.REPORT_LIST_BACKGROUND_COLOR,
  },
  labelTextMedium: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  labelTextBold1: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.LABEL_TITLE_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  labelTextBold2: {
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 14,
    color: PARTS_COLOR.LABEL_TITLE_REPORT_FONT_COLOR,
    includeFontPadding: false,
  },
  dateAndState: {
    height: 44,
    flexDirection: 'row',
  },
  labelDate: {
    paddingTop: 6,
  },
  labelStepAndStatus: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 16,
  },
  labelStep: {
    paddingTop: 14,
    width: 70,
  },
  labelStatus: {
    paddingTop: 14,
    marginLeft: 5,
    width: 100,
  },
  managementCompany: {
    height: 21,
  },
  managementCompanyText: {
    height: 21,
  },
  residentAddress: {
    height: 45,
  },
  report: {
    height: 102,
    flexDirection: 'row',
  },
  reportLeft: {
  },
  reportDetailRow: {
    height: 25,
    flexDirection: 'row',
  },
  reportDetailIcon: {
    height: 20,
    width: 20,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportDetailTitle: {
    width: 70,
    marginLeft: 4,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportDetailText: {
    width: 120,
    marginLeft: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  reportRight: {
    marginLeft: 'auto',
    marginRight: 16,
  },
  reportButtonBlock: {
    height: 38,
  },
  reportButton: {
    width: 120,
    height: 28,
    marginLeft: 'auto',
    marginRight: 16,
    borderRadius: 14,
    backgroundColor: PARTS_COLOR.BUTTON_REPORT_DETAIL_BACKGROUND_COLOR,
  },
  reportButtonText: {
    marginTop: 'auto',
    marginRight: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    fontFamily: 'NotoSansJP_500Medium',
    fontSize: 12,
    color: PARTS_COLOR.BUTTON_REPORT_DETAIL_FONT_COLOR,
    includeFontPadding: false,
  },
});

export default styles;
