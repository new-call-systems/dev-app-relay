import React from 'react';
import { TouchableWithoutFeedback, Keyboard } from 'react-native';
import SearchText from "./SearchText";
import NavTab from "./NavTab";

const dismiss = (): void => Keyboard.dismiss();

const Report = () => {
  return (
    <>
      <SearchText/>
      <TouchableWithoutFeedback onPress={dismiss}>
        <NavTab/>
      </TouchableWithoutFeedback>
    </>
  );
};

export default Report;
