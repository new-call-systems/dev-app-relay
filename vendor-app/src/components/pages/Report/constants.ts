import { REPORT_TYPE } from "../../../reducers/report";

export const TAB_NAME = {
  IN_PROCESS: '未提出',
  COMPLETED: '提出済み',
  REJECT: '差し戻し',
};

export const TAB_2_KEY = {
  [TAB_NAME.IN_PROCESS]: REPORT_TYPE.IN_PROCESS,
  [TAB_NAME.COMPLETED]: REPORT_TYPE.COMPLETED,
  [TAB_NAME.REJECT]: REPORT_TYPE.REJECT,
};

export const TAB_INDEX_2_KEY = {
  0: REPORT_TYPE.IN_PROCESS,
  1: REPORT_TYPE.COMPLETED,
  2: REPORT_TYPE.REJECT,
};
