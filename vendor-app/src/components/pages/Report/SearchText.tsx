import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigationState } from '@react-navigation/native';
import { SearchTextInput } from "../../atoms";
import { TAB_INDEX_2_KEY } from "./constants";
import { changeSearchText } from "../../../reducers/report";

const SearchText = () => {
  const dispatch = useDispatch();
  const stateNav = useNavigationState(state => state);
  const tabIndex = stateNav?.routes[0]?.state?.index || 0;
  const dataKey = TAB_INDEX_2_KEY[tabIndex];
  const text = useSelector((state) => state.report[`text${dataKey}`]);
  const [textFilterValue, setTextFilterValue] = useState(text);

  const _onChangeText = (value: string) => {
    setTextFilterValue(value);
    if (value === '' && text !== '') {
      onSearch(value);
    }
  };

  const _onSubmitEditing = () => {
    if (textFilterValue !== text) {
      onSearch(textFilterValue);
    }
  };

  const onSearch = (value) => {
    dispatch(changeSearchText({type: dataKey, text: value}));
  };

  return (
    <SearchTextInput
      value={textFilterValue}
      onChangeText={_onChangeText}
      onSubmitEditing={_onSubmitEditing}
    />
  );
};

export default SearchText;
