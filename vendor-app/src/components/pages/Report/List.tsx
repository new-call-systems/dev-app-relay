import React, { useEffect, useCallback } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useAsync } from "../../../hooks/useAsync";
import { PAGE_TYPE } from '../../../constants/page';
import ItemList from '../../templates/ItemList2';
import { LabelPink, LabelYellow, LabelNumeric, ReportImageThumbnail } from "../../atoms";
import { FormatType, formatTZ, formatDate, convertStrTime2Date } from "../../../utils/date";
import { ACTION_STATUS, ACTION_SECTION, getList, setList, appendList, setDetail } from "../../../reducers/report";
import { getBadge } from "../../../reducers/notification";
import { TAB_2_KEY } from "./constants";
import styles from './styles';

const List: React.FC = ({ route }) => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const [asyncActionRefresh, loadingRefresh] = useAsync();
  const [asyncActionNextPage, loadingNextPage] = useAsync();

  const dataKey = TAB_2_KEY[route.name];
  const forceRequest = useSelector((state) => state.report[`forceRequest${dataKey}`]);
  const list = useSelector((state) => state.report[`data${dataKey}`]);
  const currentPage = useSelector((state) => state.report[`data${dataKey}CurrentPage`]);
  const lastPage = useSelector((state) => state.report[`data${dataKey}LastPage`]);
  const textQuery = useSelector((state) => state.report[`text${dataKey}`]);

  useFocusEffect(
    useCallback(() => {
      dispatch(getBadge());
    }, [])
  );

  useEffect(() => {
    if (forceRequest) {
      onRefresh();
    }
  }, [forceRequest]);

  const onRefresh = async () => {
    asyncActionRefresh(
      getList(dataKey, textQuery, 0),
      (data) => dispatch(setList(data))
    );
  };

  const onEndReached = () => {
    const nextPage = currentPage + 1;
    if (lastPage < nextPage) {
      return;
    }
    if (loadingNextPage) {
      return;
    }
    asyncActionNextPage(
      getList(dataKey, textQuery, nextPage),
      (data) => dispatch(appendList(data))
    );
  };

  const onPress = async (item) => {
    dispatch(setDetail(item));
    navigate(`${PAGE_TYPE.ReportDetail}`);
  };

  const renderItem = (item) => {
    return (
      <View style={styles.container}>
        <View style={styles.dateAndState}>
          <View style={styles.labelDate}>
            <Text style={styles.labelTextMedium}>
              受付日
            </Text>
            <Text style={styles.labelTextMedium}>
              {formatTZ(item.receptionDate, FormatType.VIEW_YMD)}
            </Text>
          </View>
          <View style={styles.labelStepAndStatus}>
            <View style={styles.labelStep}>
              <LabelYellow>
                {ACTION_SECTION[item.actionSection]}
              </LabelYellow>
            </View>
            <View style={styles.labelStatus}>
              <LabelPink>
                {ACTION_STATUS[item.actionStatus]}
              </LabelPink>
            </View>
          </View>
        </View>
        <View style={styles.managementCompany}>
          <Text
            style={styles.labelTextBold1}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {item.managementCompanyName}
          </Text>
        </View>
        <View style={styles.residentAddress}>
          <View>
            <Text
              style={styles.labelTextBold2}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {item.propertyName}
            </Text>
          </View>
          <View>
            <Text style={styles.labelTextBold1}>
              {item.roomNo}号室
            </Text>
          </View>
        </View>
        <View style={styles.report}>
          <View style={styles.reportLeft}>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-man.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                お客様名
              </Text>
              <Text
                style={[styles.reportDetailText, styles.labelTextMedium]}
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                {item.residentName} 様
              </Text>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-date.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                工事日
              </Text>
              <Text style={[styles.reportDetailText, styles.labelTextMedium]}>
                {formatTZ(item.actionDate, FormatType.VIEW_YMD)}
              </Text>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-time.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                対応時間
              </Text>
              <Text style={[styles.reportDetailText, styles.labelTextMedium]}>
                {(() => {
                  if (item.startingTime) {
                    const startingTime = formatDate(convertStrTime2Date(item.startingTime), FormatType.VIEW_HM);
                    const endingTime = formatDate(convertStrTime2Date(item.endingTime), FormatType.VIEW_HM);
                    return `${startingTime} - ${endingTime}`;
                  }
                })()}
              </Text>
            </View>
            <View style={styles.reportDetailRow}>
              <Image
                style={styles.reportDetailIcon}
                source={require('../../assets/icon/icon-bill.png')}
              />
              <Text style={[styles.reportDetailTitle, styles.labelTextMedium]}>
                工事金額
              </Text>
              <LabelNumeric style={[styles.reportDetailText, styles.labelTextMedium]}>
                {item.constructionAmount}
              </LabelNumeric>
            </View>
          </View>
          <View style={styles.reportRight}>
            <ReportImageThumbnail
              s3Config={item.s3Config}
              data={item.imageObjectList}
            />
          </View>
        </View>
        <View style={styles.reportButtonBlock}>
          <TouchableOpacity
            style={styles.reportButton}
            onPress={() => onPress(item)}
          >
            <Text style={styles.reportButtonText}>
              詳細を見る
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <ItemList
      data={list}
      refreshing={loadingRefresh}
      onRefresh={onRefresh}
      onPress={onPress}
      renderItem={renderItem}
      nextPageLoading={loadingNextPage}
      onEndReached={onEndReached}
    />
  );
};

export default List;
