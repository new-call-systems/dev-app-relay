import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { PARTS_COLOR } from "../../assets/constants/theme";
import { TAB_NAME } from "./constants";
import List from "./List";

const TopTabNavigator = createMaterialTopTabNavigator();

const NavTab = () => {
  return (
    <TopTabNavigator.Navigator
      initialRouteName={TAB_NAME.IN_PROCESS}
      swipeEnabled={false}
      tabBarOptions={{
        activeTintColor: PARTS_COLOR.REPORT_TAB_FONT_COLOR_ACTIVE,
        inactiveTintColor: PARTS_COLOR.REPORT_TAB_FONT_COLOR_INACTIVE,
        indicatorStyle: {
          backgroundColor: PARTS_COLOR.REPORT_TAB_INDICATOR_COLOR,
        },
        style: {
          backgroundColor: PARTS_COLOR.REPORT_TAB_BACKGROUND_COLOR,
        },
        labelStyle: {
          fontFamily: 'NotoSansJP_500Medium',
          fontSize: 14,
        },
      }}
    >
      <TopTabNavigator.Screen name={`${TAB_NAME.IN_PROCESS}`} component={List} />
      <TopTabNavigator.Screen name={`${TAB_NAME.COMPLETED}`} component={List} />
      <TopTabNavigator.Screen name={`${TAB_NAME.REJECT}`} component={List} />
    </TopTabNavigator.Navigator>
  );
};

export default NavTab;
