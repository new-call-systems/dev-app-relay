import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from "react-i18next";
import TemplateItemDetail from '../../templates/ItemDetail';
import { Loading, S3Images, TelLink } from "../../atoms";
import { useAsync } from "../../../hooks/useAsync";
import { RequestStatus, requestStatus } from "../../../localization/label.i18n";
import { FormatType, formatTZ } from "../../../utils/date";
import { getDetail, setCompleteDetail } from '../../../reducers/request';
import styles from './styles';

const CompleteDetail: React.FC = ({ route }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { requestId } = route.params;
  const [asyncAction, loading] = useAsync();
  const item = useSelector((state) => state.request.dataCompleteDetail);

  useEffect(() => {
    (async () => {
      asyncAction(
        getDetail(requestId),
        (data) => dispatch(setCompleteDetail(data))
      )
    })();
  }, [requestId]);

  if (!item) {
    return null;
  }

  if (loading) {
    return <Loading />
  }

  return (
    <TemplateItemDetail>
      {/* 問合せ内容、入居者情報 */}
      <View style={styles.sectionRow}>
        <Text style={styles.textB}>詳細</Text>
        <Text style={styles.textStatus}>
          {t([requestStatus,RequestStatus[item.status]].join("."))}
        </Text>
      </View>
      <Text style={styles.text}>
        {item.context}
      </Text>

      {/* 入居者情報 */}
      <Text style={[styles.sectionResident, styles.textB]}>入居者情報</Text>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>氏名</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.residentName}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>住所</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.propertyAddress}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>物件</Text>
        <Text style={styles.textS}>
          {'：  '}
        </Text>
        <View style={styles.flexWordWrap}>
          <Text style={styles.textS}>
            {item.propertyName}
          </Text>
        </View>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>部屋</Text>
        <Text style={styles.textS}>
          {'：  '}
          {item.roomNo}
        </Text>
      </View>
      <View style={[styles.sectionRow, styles.sectionLabel]}>
        <Text style={styles.textLabel}>連絡先</Text>
        <Text style={styles.textS}>
          {'：  '}
          {(() => {
            if (item.isAdmin) {
              return <TelLink>{item.residentTel}</TelLink>;
            } else {
              return item.residentTel
            }
          })()}
        </Text>
      </View>

      {/* 対応完了日 or キャンセル理由 */}
      {(() => {
        if (item.status === 'cancelled') {
          return (
            <>
              <Text style={[styles.sectionPreferable, styles.textB]}>キャンセル理由</Text>
              <Text style={styles.textM}>
                {item.cancelReason}
              </Text>
            </>
          )
        } else {
          return (
            <>
              <Text style={[styles.sectionPreferable, styles.textB]}>対応完了日</Text>
              <Text style={styles.textM}>
                {formatTZ(item.updatedAt, FormatType.VIEW_YMD)}
              </Text>
            </>
          )
        }
      })()}

      {/* 作業担当者名 */}
      <Text style={[styles.sectionUserName, styles.textB]}>作業担当者名</Text>
      <Text style={styles.textM}>
        {item.userName}
      </Text>

      {/* 画像-動画 */}
      {((item.images || []).length > 0) &&
        <View style={styles.sectionImage}>
          <S3Images data={item.images}/>
        </View>
      }

    </TemplateItemDetail>
  );
};

export default CompleteDetail;
