import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  sectionRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStatus: {
    marginTop: 2,
    marginRight: 10,
    marginLeft: 'auto',
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.MALACHITE,
    includeFontPadding: false,
  },
  text: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textB: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
  textS: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  textM: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 14,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  textLabel: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    width: 40,
    includeFontPadding: false,
  },
  sectionLabel: {
    marginTop: 1,
    marginBottom: 1,
  },
  sectionResident: {
    marginTop: 17,
    marginBottom: 3,
  },
  sectionPreferable: {
    marginTop: 11,
    marginBottom: 3,
  },
  sectionUserName: {
    marginTop: 11,
    marginBottom: 3,
  },
  sectionImage: {
    marginTop: 20,
  },
  flexWordWrap: {
    flexShrink: 1,
  }
});

export default styles;
