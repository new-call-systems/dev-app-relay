import React, { useEffect, useCallback } from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useTranslation } from "react-i18next";
import { PAGE_TYPE } from '../../../constants/page';
import TemplateItemList from '../../templates/ItemList';
import { FormatType, formatTZ } from "../../../utils/date";
import { useAsync } from "../../../hooks/useAsync";
import { getBadge } from "../../../reducers/notification";
import { RequestStatus, requestStatus } from "../../../localization/label.i18n";
import styles from './styles';
import {
  getList,
  setCompleteList,
  appendCompleteList,
  REQUEST_TYPE,
  changeSearchText,
} from '../../../reducers/request';

const Complete: React.FC = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const [asyncActionRefresh, loadingRefresh] = useAsync();
  const [asyncActionNextPage, loadingNextPage] = useAsync();
  const list = useSelector((state) => state.request.dataComplete);
  const currentPage = useSelector((state) => state.request.dataCompleteCurrentPage);
  const lastPage = useSelector((state) => state.request.dataCompleteLastPage);
  const forceRequest = useSelector((state) => state.request.forceRequestComplete);
  const textQuery = useSelector((state) => state.request.textComplete);

  useFocusEffect(
    useCallback(() => {
      dispatch(getBadge());
    }, [])
  );

  useEffect(() => {
    if (forceRequest) {
      onRefresh();
    }
  }, [forceRequest]);

  const onRefresh = async () => {
    asyncActionRefresh(
      getList(REQUEST_TYPE.COMPLETE, textQuery, 0),
      refreshSuccess(dispatch)
    );
  };

  const refreshSuccess = (dispatch) => {
    return (data) => {
      dispatch(setCompleteList(data));
      dispatch(getBadge(REQUEST_TYPE.COMPLETE));
    }
  };

  const onEndReached = () => {
    const nextPage = currentPage + 1;
    if (lastPage < nextPage) {
      return;
    }
    if (loadingNextPage) {
      return;
    }
    asyncActionNextPage(
      getList(REQUEST_TYPE.COMPLETE, textQuery, nextPage),
      (data) => dispatch(appendCompleteList(data))
    );
  };

  const onSearchText = (value) => {
    dispatch(changeSearchText({type: REQUEST_TYPE.COMPLETE, text: value}));
  };

  const onPress = (requestId) => {
    navigate(`${PAGE_TYPE.CompleteDetail}`, { requestId: requestId });
  };

  const renderItem = (item) => {
    return (
      <>
        <View style={styles.rowUpper}>
          <Text style={[styles.text, styles.textDate]}>
            {'受付日時: '}
            {formatTZ(item.createdAt, FormatType.VIEW_YMDHM)}
          </Text>
          <Text style={[styles.text, styles.textStatus]}>
            {t([requestStatus,RequestStatus[item.status]].join("."))}
          </Text>
        </View>
        <View style={styles.rowLower}>
          <Text
            style={styles.textContext}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {item.context}
          </Text>
        </View>
      </>
    );
  };

  return (
    <TemplateItemList
      data={list}
      refreshing={loadingRefresh}
      onRefresh={onRefresh}
      onPress={onPress}
      renderItem={renderItem}
      nextPageLoading={loadingNextPage}
      onEndReached={onEndReached}
      noDataText={'対応済みの問合せはありません。'}
      textSearch={textQuery}
      onSearchText={onSearchText}
    />
  );
};

export default Complete;
