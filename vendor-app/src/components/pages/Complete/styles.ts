import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  rowUpper: {
    flexDirection: 'row',
  },
  text: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    includeFontPadding: false,
  },
  textDate: {
    marginTop: 10,
    marginLeft: 15,
    color: COLOR.BLUE_GREY,
  },
  textStatus: {
    marginTop: 10,
    marginLeft: 'auto',
    marginRight: 18,
    color: COLOR.MALACHITE,
  },
  rowLower: {
    marginBottom: 25,
  },
  textContext: {
    marginTop: 3,
    marginLeft: 15,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 16,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
});

export default styles;
