import { StyleSheet } from 'react-native';
import COLOR from '../../assets/constants/theme';

const styles = StyleSheet.create({
  rowUpper: {
    flexDirection: 'row',
  },
  sectionDate: {
    marginTop: 10,
    marginLeft: 15,
    height: 28
  },
  textDate: {
    fontFamily: "NotoSansJP_500Medium",
    fontSize: 12,
    color: COLOR.BLUE_GREY,
    includeFontPadding: false,
  },
  sectionEmergency: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 'auto',
    marginRight: 18,
  },
  iconEmergency: {
    width: 28,
    height: 28
  },
  textEmergency: {
    marginTop: 4,
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 12,
    color: COLOR.CHILEAN_FIRE,
    includeFontPadding: false,
  },
  rowLower: {
    marginTop: 3,
    paddingRight: 15,
    paddingBottom: 18,
    paddingLeft: 15,
  },
  textContext: {
    fontFamily: "NotoSansJP_700Bold",
    fontSize: 14,
    color: COLOR.SLATE,
    includeFontPadding: false,
  },
});

export default styles;
