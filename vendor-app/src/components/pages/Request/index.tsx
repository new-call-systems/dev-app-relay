import React, { useEffect, useCallback } from 'react';
import { View, Text, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { PAGE_TYPE } from '../../../constants/page';
import TemplateItemList from '../../templates/ItemList';
import { getBadge } from "../../../reducers/notification";
import { useAsync } from "../../../hooks/useAsync";
import { FormatType, formatTZ } from '../../../utils/date';
import styles from './styles';
import {
  getList,
  setOpenList,
  appendOpenList,
  changeSearchText,
  REQUEST_TYPE,
} from '../../../reducers/request';

const Request: React.FC = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const [asyncActionRefresh, loadingRefresh] = useAsync();
  const [asyncActionNextPage, loadingNextPage] = useAsync();
  const list = useSelector((state) => state.request.dataOpen);
  const currentPage = useSelector((state) => state.request.dataOpenCurrentPage);
  const lastPage = useSelector((state) => state.request.dataOpenLastPage);
  const forceRequest = useSelector((state) => state.request.forceRequestOpen);
  const textQuery = useSelector((state) => state.request.textOpen);

  useFocusEffect(
    useCallback(() => {
      dispatch(getBadge());
    }, [])
  );

  useEffect(() => {
    if (forceRequest) {
      onRefresh();
    }
  }, [forceRequest]);

  const onRefresh = async () => {
    asyncActionRefresh(
      getList(REQUEST_TYPE.OPEN, textQuery, 0),
      refreshSuccess(dispatch)
    );
  };

  const refreshSuccess = (dispatch) => {
    return (data) => {
      dispatch(setOpenList(data));
      dispatch(getBadge(REQUEST_TYPE.OPEN));
    }
  };

  const onEndReached = () => {
    const nextPage = currentPage + 1;
    if (lastPage < nextPage) {
      return;
    }
    if (loadingNextPage) {
      return;
    }
    asyncActionNextPage(
      getList(REQUEST_TYPE.OPEN, textQuery, nextPage),
      (data) => dispatch(appendOpenList(data))
    );
  };

  const onSearchText = (value) => {
    dispatch(changeSearchText({type: REQUEST_TYPE.OPEN, text: value}));
  };

  const onPress = (requestId) => {
    navigate(`${PAGE_TYPE.RequestDetail}`, { requestId: requestId });
  };

  const renderItem = (item) => {
    return (
      <>
        <View style={styles.rowUpper}>
          <View style={styles.sectionDate}>
            <Text style={styles.textDate}>
              {'受付日時: '}
              {formatTZ(item.createdAt, FormatType.VIEW_YMDHM)}
            </Text>
          </View>
          {(() => {
            if (item.emergencyFlg) {
              return (
                <View style={styles.sectionEmergency}>
                  <Image
                    style={styles.iconEmergency}
                    source={require('../../assets/icon/icon-urgent.png')}
                  />
                  <Text style={styles.textEmergency}>
                    緊急
                  </Text>
                </View>
              );
            }
          })()}
        </View>
        <View style={styles.rowLower}>
          <Text
            style={styles.textContext}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {item.context}
          </Text>
        </View>
      </>
    );
  };

  return (
    <TemplateItemList
      data={list}
      refreshing={loadingRefresh}
      onRefresh={onRefresh}
      onPress={onPress}
      renderItem={renderItem}
      nextPageLoading={loadingNextPage}
      onEndReached={onEndReached}
      noDataText={'新着の問合せはありません。'}
      textSearch={textQuery}
      onSearchText={onSearchText}
    />
  );
};

export default Request;
