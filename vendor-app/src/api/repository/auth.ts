import HttpClient from '../client/axios';
import { ILoginRequest, ILoginResponse } from '../schema/auth';
import { IApiResult } from '../../types/apiResult';

/**
 * 認証のリポジトリクラス
 */
export default class AuthRepository {
  /**
   * ログイン
   * @param params ログイン情報
   */
  static login(
    params: ILoginRequest,
  ): Promise<IApiResult<ILoginResponse>> {
    return HttpClient.post<ILoginResponse>('/api/vn/v1/auth/login', params);
  }

}
