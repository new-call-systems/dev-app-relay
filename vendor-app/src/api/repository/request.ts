import HttpClient from '../client/axios';
import {
  IRequestListRequest,
  IRequestListResponse,
  IRequestDetail,
  ITimeSection,
  IBadge,
} from '../schema/request';
import { IApiResult } from '../../types/apiResult';

/**
 * 問合せのリポジトリクラス
 */
export default class RequestRepository {
  /**
   * 問合せ一覧取得
   * @param params リクエストパラメータ
   */
  static getRequest(
    params: IRequestListRequest,
  ): Promise<IApiResult<IRequestListResponse>> {
    return HttpClient.get<IRequestListResponse>('/api/vn/v1/request', {params: params});
  }

  /**
   * 問合せ詳細取得
   * @param request_id 問合せ番号
   */
  static getRequestDetail(
    request_id: number,
  ): Promise<IApiResult<IRequestDetail>> {
    return HttpClient.get<IRequestDetail>(`/api/vn/v1/request/${request_id}`);
  }

  /**
   * 希望時間区分取得
   */
  static getTimeSection(): Promise<IApiResult<ITimeSection>> {
    return HttpClient.get<ITimeSection>('/api/vn/v1/timeSection');
  }

  /**
   * 入札処理
   * @param request_id 問合せ番号
   * @param request_preferable_time_id 希望時間ID
   */
  static putRequestBid(
    request_id: number,
    request_preferable_time_id: number | null,
  ): Promise<IApiResult<void>> {
    const params = { request_preferable_time_id: request_preferable_time_id };

    return HttpClient.put<void>(`/api/vn/v1/request/${request_id}/bid`, params);
  }

  /**
   * 業者対応報告
   * @param request_id 問合せ番号
   * @param status [scheduling, in_progress, completed]
   */
  static putRequestStatus(
    request_id: number,
    status: string,
  ): Promise<IApiResult<void>> {
    const params = { status: status };
    return HttpClient.put<void>(`/api/vn/v1/request/${request_id}`, params);
  }

  /**
   * 入居者のキャンセルを承認
   * @param request_id 問合せ番号
   */
  static cancel(
    request_id: number,
  ): Promise<IApiResult<void>> {
    return HttpClient.post<void>(`/api/vn/v1/request/${request_id}/cancel`);
  }

  /**
   * バッジ表示用情報取得
   */
  static getBadge(): Promise<IApiResult<IBadge>> {
    return HttpClient.get<IBadge>('/api/vn/v1/badge');
  }

}
