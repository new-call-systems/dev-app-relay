import HttpClient from '../client/axios';
import { IApiResult } from '../../types/apiResult';
import { IReportList, IReportDetail, IReportAwsConfig, IReportListRequest } from '../schema/report';

/**
 * 完了報告書のリポジトリクラス
 */
export default class ReportRepository {
  /**
   * 完了報告書作成
   * @param request_id 問合せ番号
   */
  static create(
    request_id: string,
  ): Promise<IApiResult<string>> {
    return HttpClient.post<string>(`/api/vn/v1/request/${request_id}/reports`);
  }

  /**
   * 完了報告書取得
   * @param request_id 問合せ番号
   * @param report_id 完了報告書番号
   */
  static getDetail(
    request_id: string,
    report_id: string,
  ): Promise<IApiResult<IReportDetail>> {
    return HttpClient.get<IReportDetail>(`/api/vn/v1/request/${request_id}/reports/${report_id}`);
  }

  /**
   * 完了報告書更新
   * @param request_id 問合せ番号
   * @param report_id 完了報告書番号
   * @param form 画面入力値
   */
  static update(
    request_id: string,
    report_id: string,
    form: any,
  ): Promise<IApiResult<void>> {
    const params = {
      description: form.description,
      action_date: form.actionDate,
      starting_time: form.startingTime,
      ending_time: form.endingTime,
      action_status: form.actionStatus,
      action_section: form.actionSection,
      construction_amount: form.constructionAmount,
    };
    return HttpClient.put<void>(`/api/vn/v1/request/${request_id}/reports/${report_id}`, params);
  }

  /**
   * 完了報告書ステータス変更
   * @param request_id 問合せ番号
   * @param report_id 完了報告書番号
   */
  static statusUpdate(
    request_id: string,
    report_id: string,
  ): Promise<IApiResult<void>> {
    return HttpClient.post<void>(`/api/vn/v1/request/${request_id}/reports/${report_id}/draft`);
  }

  /**
   * 完了報告書一覧取得
   * @param params
   */
  static getList(
    params: IReportListRequest
  ): Promise<IApiResult<IReportList>> {
    return HttpClient.get<IReportList>('/api/vn/v1/reports', {params: params});
  }

  /**
   * AWS Config取得
   * @param request_id 問合せ番号
   * @param report_id 完了報告書番号
   */
  static getAwsConfig(
    request_id: string,
    report_id: string,
  ): Promise<IApiResult<IReportAwsConfig>> {
    return HttpClient.get<IReportAwsConfig>(`/api/vn/v1/request/${request_id}/reports/${report_id}/config`);
  }

}
