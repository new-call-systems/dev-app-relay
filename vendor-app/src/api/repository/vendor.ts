import HttpClient from '../client/axios';
import { IVendorUpdate, IVendorDevicePost } from '../schema/vendor';
import { IApiResult } from '../../types/apiResult';

/**
 * 業者のリポジトリクラス
 */
export default class VendorRepository {

  /**
   * 業者の担当者情報を更新
   * @param user_id
   * @param vendor_id
   * @param data
   */
  static update (
    user_id: number,
    vendor_id: string,
    data: IVendorUpdate,
  ): Promise<IApiResult<void>> {
    return HttpClient.put<void>(`/api/vn/v1/vendorMember/${user_id}/${vendor_id}`, data);
  }

  /**
   * デバイスIDを送信
   * @param user_id
   * @param vendor_id
   * @param data
   */
  static postDeviceToken (
    user_id: number,
    vendor_id: string,
    data: IVendorDevicePost,
  ): Promise<IApiResult<void>> {
    return HttpClient.post<void>(`/api/vn/v1/vendorMember/${user_id}/${vendor_id}/deviceId`, data);
  }

}
