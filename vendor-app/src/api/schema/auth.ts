import IVendor from './vendor';

export interface ILoginRequest {
  email: string;
  password: string;
}

export interface ILoginResponse {
  access_token: string;
  refresh_token: string;
  vendor_member?: IVendor;
}

export interface ITokenResponse {
  access_token: string;
  refresh_token: string;
}
