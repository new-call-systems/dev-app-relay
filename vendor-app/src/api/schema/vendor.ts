export interface IVendor {
  user_id: number;
  vendor_id: string;
  last_name: string;
  first_name: string;
  emergency_tel: string;
  retire_flg?: boolean;
  representative_flg: number;
  created_at: string;
  updated_at: string;
}

export interface IVendorUpdate {
  last_name: string;
  first_name: string;
  emergency_tel: string;
}

export interface IVendorDevicePost {
  device_id: string;
}
