export interface IReportList {
  current_page: string;
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: string;
  path: string;
  per_page: string;
  prev_page_url?: string;
  to: number;
  total: number;
  data: IReportDetail[];
}

export interface IReportDetail {
  report_id: string;
  management_company_name: string;
  property_name: string;
  room_no: string;
  resident_name: string;
  action_date: string;
  starting_time: string;
  ending_time: string;
  description: string;
  user_name: string;
  reception_date: string;
  report_status: string;
  request_id: string;
  action_status: string;
  action_section: string;
  construction_amount: number;
  s3_config: IReportAwsConfig;
}

export interface IReportAwsConfig {
  bucket_name: string;
  region: string;
  identity_pool_id: string;
  prefix: string;
}

export interface IReportListRequest {
  include_group_reports: boolean,
  query: string,
  status: string,
  page: number,
}
