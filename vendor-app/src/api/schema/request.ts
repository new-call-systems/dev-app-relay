export const RequestStatus = {
  'waiting_grasped': 'waiting_grasped',
  'waiting_for_cancel': 'waiting_for_cancel',
  'waiting_for_open': 'waiting_for_open',
  'open': 'open',
  'bid': 'bid',
  'scheduling': 'scheduling',
  'in_progress': 'in_progress',
  'completed': 'completed',
  'deleted': 'deleted',
  'cancelled': 'cancelled',
  'transferred': 'transferred',
  'auto_completed': 'auto_completed',
} as const;

export type RequestStatusType = typeof RequestStatus[keyof typeof RequestStatus];

export interface IRequestListRequest {
  status?: string;
  emergency_flg?: string;
  date?: string;
  page: number;
}

export interface IRequestListResponse {
  current_page: string;
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: string;
  path: string;
  per_page: string;
  prev_page_url?: string;
  to: number;
  total: number;
  data: IRequestData[];
}

export interface IRequestData {
  request_id: number;
  status: RequestStatusType;
  ai_category: string;
  ai_sub_category: string;
  created_at: string;
  emergency_flg: boolean;
  context: string;
  user_name: string;
  preferable_info: IRequestPreferableInfo[];
}

export interface IRequestDetail {
  request_id: number;
  resident_name: string;
  resident_tel: string;
  property_name: string;
  property_address: string;
  room_no: string;
  status: RequestStatusType;
  emergency_flg: boolean;
  ai_category: string;
  ai_sub_category: string;
  context: string;
  response: string;
  created_at: string;
  updated_at: string;
  preferable_info: IRequestPreferableInfo[];
  cancel_reason: string;
  user_name: string;
  is_admin: boolean;
  images: IImageObject[];
}

export interface IRequestPreferableInfo {
  request_preferable_time_id: number;
  date: string;
  time_section_id: number;
  selected: boolean;
}

export interface ITimeSection {
  time_section_id: number;
  label: string;
}

export interface IBadge {
  open: number;
  scheduling: number;
  in_progress: number;
  completed: number;
  waiting_for_cancel: number;
  cancelled: number;
}

export interface IImageObject {
  type: 'image' | 'video';
  url: string;
}
