import React from 'react';
import { Provider } from 'react-redux';
import AppLoading from 'expo-app-loading';
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationBar, MessageDialog, PushNotifications } from './components/atoms';
import Navigation from './navigations';
import { store, persistor } from './store';
import { useLoadResources } from './hooks/useLoadResources';

export default function Main(): React.FC {
  const [loadResourcesAsync, isCompleted] = useLoadResources();

  if (!isCompleted) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={error => console.warn('Error AppLoading', error)}
        onFinish={() => console.log('Complete AppLoading')}
      />
    );
  }
  return <AppMain/>;
}

const AppMain = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PushNotifications />
        <MessageDialog />
        <NavigationBar>
          <Navigation />
        </NavigationBar>
      </PersistGate>
    </Provider>
  );
};
