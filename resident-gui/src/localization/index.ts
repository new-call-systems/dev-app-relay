import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import { formatString } from "../utils/date";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    defaultNS: 'common',
    resources: {
      /* Load everything dynamically for two readons:
       * - better sorting code
       * - prevent Rect HMR to reload the whole page everytime
       */
    },
    fallbackLng: "jp",
    lng: "jp",

    //keySeparator: true, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
      format: function(value, format, lng) {
        if (format === 'uppercase') {
          // Test format, works with
          // { "key2": "{{text, uppercase}} just uppercased" }
          return value.toUpperCase();
        }

        if (value instanceof Date) {
          // Example:
          // { "key": "The current date is {{date, MM/DD/YYYY}}" }
          return formatString(value);
        }
        return value;
      }
    }
  });

  i18n.on('languageChanged', function(lng) {
    
  });

  export default i18n;
