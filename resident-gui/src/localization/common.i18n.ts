import i18n from '.';

export const I18N_NS = "common";

export const TabPanes = {
  "home": "home",
  "request": "request",
  "schedule": "schedule"
} as const;

i18n.addResourceBundle('jp', I18N_NS, {
  [TabPanes.home]: "ホーム",
  [TabPanes.request]: "お問合せ",
  [TabPanes.schedule]: "履歴"
});
