import i18n from '.';

import { I18N_NS } from "./common.i18n";

import { RequestStatus } from "../api/schema/request";
export { RequestStatus } from "../api/schema/request";

export const requestStatus = "requestStatus";

i18n.addResourceBundle('jp', I18N_NS, {
  [requestStatus]: {
    [RequestStatus.waiting_grasped]: "問合せ判定不可",
    [RequestStatus.waiting_for_cancel]: "キャンセル確認中",
    [RequestStatus.waiting_for_open]: "未解決",
    [RequestStatus.open]: "問合せ中",
    [RequestStatus.bid]: "承認待ち",
    [RequestStatus.scheduling]: "予約済み",
    [RequestStatus.in_progress]: "対応中",
    [RequestStatus.completed]: "対応済み",
    [RequestStatus.deleted]: "キャンセル済み",
    [RequestStatus.cancelled]: "キャンセル済み",
    [RequestStatus.transferred]: "サポートセンター対応",
    [RequestStatus.auto_completed]: "解決済み",
    [RequestStatus.discard]: "問合せ破棄",
  },
} );

