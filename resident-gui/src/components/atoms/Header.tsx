import React from 'react'
import styled from 'styled-components';
import { HEADER_HEIGHT } from "../../constants/ThreeRowsDesign";
import { cocoEnLogo } from "../assets/icon";

const Contents = styled.header`
  width: 100%;
  height: ${HEADER_HEIGHT};
  background-color: #464f69;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow-y: hidden;
  z-index: 100;

  .logo {
    height: 22px;
  }
`;

const Header: React.FC = () => {
  return (
    <Contents>
      <div className="logo">
        <img
          src={cocoEnLogo}
          width="160px"
          height="22px"
          alt="logo"
        />
      </div>
    </Contents>
  );
};

export default Header;
