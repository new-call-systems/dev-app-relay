import React from 'react'
import { spinner } from "../assets/icon";

interface IProps {
  width?: string;
  height?: string;
}

const Spinner: React.FC<IProps> = (props) => {
  const {
    width = '25px',
    height = '25px',
  } = props;

  return (
    <img
      src={spinner}
      width={width}
      height={height}
      alt="ローディング"
    />
  );
};

export default Spinner;
