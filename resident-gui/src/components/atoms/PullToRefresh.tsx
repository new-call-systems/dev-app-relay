import React from 'react'
import styled from 'styled-components';
import {
  PullToRefresh,
  PullDownContent,
  ReleaseContent,
  RefreshContent
} from "react-js-pull-to-refresh";

interface IProps {
  // children: React.ReactNode;
  onRefresh: () => Promise<any>;
}

const Contents = styled.div`
  #basic-container {
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    background: darkslategray;
  }
  #basic-label {
    user-select: none;
    margin-top: 20px;
    color: aliceblue;
    border: 1px solid aliceblue;
    border-radius: 6px;
    padding: 5px 2px;
  }
  #basic-label:hover {
    cursor: pointer;
  }
`;

const PullToRefreshCustom: React.FC<IProps> = (props) => {
  const {
    // children,
    onRefresh,
  } = props;

  return (
    <Contents>
      <PullToRefresh
        pullDownContent={<PullDownContent/>}
        releaseContent={<ReleaseContent/>}
        refreshContent={<RefreshContent/>}
        onRefresh={onRefresh}
        pullDownThreshold={100}
        triggerHeight={0}
        backgroundColor='white'
        startInvisible={true}
      >

        {/*{children}*/}
        <div id="basic-container">
          <div id="basic-label">PullToRefresh</div>
        </div>

      </PullToRefresh>
    </Contents>
  );
};

export default PullToRefreshCustom;
