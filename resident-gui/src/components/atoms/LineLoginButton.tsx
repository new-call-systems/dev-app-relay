import React from 'react'
import { lineBtnLoginBase } from '../assets/icon';

interface IProps {
  url: string;
}

const LineLoginButton: React.FC<IProps> = (props) => {
  const { url } = props;

  if (!url) {
    return null;
  }

  return (
    <a href={url}>
      <img src={lineBtnLoginBase} alt="LINEでログインする"/>
    </a>
  );
};

export default LineLoginButton;
