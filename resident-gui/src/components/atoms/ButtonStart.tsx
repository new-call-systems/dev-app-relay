import React from 'react'
import styled from 'styled-components';
import ButtonBase from "./ButtonBase";

interface Props {
  onClick: () => void;
}

const ButtonStart: React.FC<Props> = (props) => {
  return <Button onClick={props.onClick}>はじめる</Button>
};

const Button = styled(ButtonBase)`
  width: 280px;
  height: 48px;
  border-radius: 25px;
  background-color: #464f69;
  font-size: 19px;
  font-weight: bold;
  color: #ffffff;
`;

export default ButtonStart;
