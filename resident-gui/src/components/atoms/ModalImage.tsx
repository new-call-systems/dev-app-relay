import React, { useState } from 'react'
import LightBoxImage from "./LightBoxImage";

interface IProps {
  src: string;
  style?: React.CSSProperties;
}

const ModalImage: React.FC<IProps> = (props) => {
  const { src, style } = props;
  const [visible, setIsVisible] = useState(false);

  const openLightbox = () => {
    setIsVisible(true);
  };

  const closeLightbox = () => {
    setIsVisible(false);
  };

  if (visible) {
    return (
      <LightBoxImage
        src={src}
        onClose={closeLightbox}
      />
    );
  }

  return (
    <img
      src={src}
      alt=""
      style={style}
      onClick={openLightbox}
    />
  );
};

export default ModalImage;
