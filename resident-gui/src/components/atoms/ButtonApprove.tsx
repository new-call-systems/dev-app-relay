import React from 'react'
import styled from 'styled-components';
import ButtonBase from "./ButtonBase";

interface Props {
  onClick: () => void;
}

const ButtonApprove: React.FC<Props> = (props) => {
  return <Button onClick={props.onClick}>OK</Button>
};

const Button = styled(ButtonBase)`
  width: 230px;
  height: 40px;
  border-radius: 25px;
  background-color: #464f69;
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
`;

export default ButtonApprove;
