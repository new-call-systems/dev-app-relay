import React, { useEffect, useCallback, useRef } from 'react'

interface IProps {
  src: string;
  onClose: () => void;
}

const LightBoxImage: React.FC<IProps> = (props) => {
  const { src, onClose } = props;
  const imgRef = useRef<HTMLImageElement>(null);

  const handleKeyDown = useCallback(event => {
    const { keyCode } = event;
    if (keyCode === 27 || keyCode === 13) {
      onClose();
    }
  }, []);

  const handleOnClick = useCallback(event => {
    event.preventDefault();
    if (
      imgRef &&
      imgRef.current &&
      !imgRef.current.contains(event.target)
    ) {
      onClose();
    }
  }, []);

  useEffect(() => {
    window.addEventListener("keydown", handleKeyDown);
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, [handleKeyDown]);

  useEffect(() => {
    window.addEventListener("touchstart", handleOnClick);
    window.addEventListener("click", handleOnClick);
    return () => {
      window.removeEventListener("touchstart", handleOnClick);
      window.removeEventListener("click", handleOnClick);
    };
  }, [handleOnClick]);

  return (
    <>
      <div
        style={{
          position: 'fixed',
          top: '0px',
          left: '0px',
          right: '0px',
          bottom: '0px',
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
          zIndex: 999,
          overflowY: 'auto',
          transform: 'translate3d(0, 0, 0)',
        }}
      >

        {/* 画像 */}
        <div
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            border: 'none',
            overflow: 'visible',
            borderRadius: '10px',
            outline: 'none',
            padding: '20px',
            transform: 'translate(-50%, -50%)',
          }}
        >
          <img
            ref={imgRef}
            src={src}
            alt=""
            style={{
              maxHeight: 'initial',
              maxWidth: 'initial',
            }}
          />
        </div>

        {/* 閉じる */}
        <div
          style={{
            position: 'absolute',
            top: 0,
            width: '100%',
            overflow: 'hidden',
          }}
        >
        <span
          style={{
            float: 'right',
            height: '40px',
            width: '40px',
            backgroundColor: 'rgba(0, 0, 0, 1)',
            borderRadius: '20px',
          }}
        >
          <a
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              width: '100%',
              cursor: 'pointer',
              border: 'none',
            }}
            onClick={onClose}
          >
            <svg fill="#ffffff" height="30" viewBox="0 0 24 24" width="30" xmlns="http://www.w3.org/2000/svg"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path><path d="M0 0h24v24H0z" fill="none"></path></svg>
          </a>
        </span>
        </div>

      </div>
    </>
  )

};

export default LightBoxImage;
