import React from 'react'

interface IProps {
  children?: React.ReactNode;
  tel: string;
}

const TelLink: React.FC<IProps> = (props) => {
  const {
    children = null,
    tel = '',
  } = props;

  const val = tel.replace(/-/g, '');
  return (
    <a href={`tel:${val}`}>{children}</a>
  );
};

export default TelLink;
