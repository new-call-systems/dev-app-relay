import styled from 'styled-components';

const ButtonBase = styled.button`
  background: none;
  border: none;
  outline: none;
  appearance: none;
`;

export default ButtonBase;
