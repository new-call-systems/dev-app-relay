import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { clear } from "../../reducers/message";
import { RootState } from "../../store";

const MessageDialog: React.FC = () => {
  const dispatch = useDispatch();
  const message = useSelector((state: RootState) => state.message.message);
  if (message) {
    setTimeout(() => alert(message), 0);
    dispatch(clear());
  }
  return null;
};

export default MessageDialog;
