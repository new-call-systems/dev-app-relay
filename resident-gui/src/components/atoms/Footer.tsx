import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router'
import styled from 'styled-components';
import { useTranslation } from "react-i18next"
import { TabPanes } from "../../localization/common.i18n";
import { clear } from "../../reducers/request";
import { getBadge } from "../../reducers/notification";
import { FOOTER_HEIGHT } from "../../constants/ThreeRowsDesign";
import {
  tabHome,
  tabHomeOn,
  tabRequest,
  tabRequestOn,
  tabSchedule,
  tabScheduleOn,
  iconBadge,
} from "../assets/icon"
import {RootState} from "../../store";

interface IProps {
  active?: string;
}

const menus = ['home', 'request', 'schedule'];
const icons = {
  'home' : tabHome,
  'homeOn' : tabHomeOn,
  'request' : tabRequest,
  'requestOn' : tabRequestOn,
  'schedule' : tabSchedule,
  'scheduleOn' : tabScheduleOn,
};

const Footer: React.FC<IProps> = (props) => {
  const { active } = props;
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const badgeSchedule = useSelector((state: RootState) => state.notification.badgeSchedule);

  const onClick = ((url) => {
    if (url === 'request') {
      dispatch(clear());
    }
    if (url === 'schedule') {
      dispatch(getBadge());
    }
    dispatch(push(`/${url}`));
  });

  return (
    <Contents>
      <ul className="tab-group">
        {menus.map((v) => {
          const isActive = active === v;
          const icon = isActive ? `${v}On` : v;
          const badge = (v === 'schedule') && (badgeSchedule);
          return (
            <li
              key={v}
              className={isActive ? 'tab active' : 'tab'}
              onClick={() => onClick(v)}
            >
              <IconArea>
                <IconFile
                  src={icons[icon]}
                  alt={v}
                />
                {badge &&
                  <IconBadge
                    src={iconBadge}
                    alt=""
                  />
                }
              </IconArea>
              <p>
                {t(TabPanes[v])}
              </p>
            </li>
          )
        })}
      </ul>
    </Contents>
  );
};

const Contents = styled.footer`
  width: 100%;
  height: ${FOOTER_HEIGHT};
  background-color: #ffffff;
  text-align: center;
  margin-top: auto;
  overflow-y: hidden;

  .tab-group {
    display: flex;
    justify-content: center;
  }

  .tab {
    flex-grow: 1;
    list-style :none;
    text-align :center;
    cursor :pointer;
    margin-top: 1px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    p {
      font-size: 10px;
      color: #8c8c8c;
    }

    &.active p {
      font-weight: bold;
      color: #464f69;
    }
  }
`;

const IconArea = styled.div`
  position: relative;
`;

const IconFile = styled.img`
  width: 44px;
  height: 44px;
`;

const IconBadge = styled.img`
  position: absolute;
  right: 1px;
  width: 18px;
  height: 18px;
`;


export default Footer;
