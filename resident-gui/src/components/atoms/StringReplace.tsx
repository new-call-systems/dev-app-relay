import React from 'react'
import reactStringReplace from "react-string-replace";
import { TelLink } from "./index";

interface IProps {
  children: string;
}

const StringReplace: React.FC<IProps> = (props) => {
  const { children } = props;

  //-----------------------------------------------------------------
  // 1.改行コードを<br>タグに変換
  // 2.URLの書式が文書にあれば<a>タグで囲む
  // 3.電話番号の書式がが文書にあれば<a href='tel:xxx'>タグで囲む
  //-----------------------------------------------------------------
  const renderAutoResponseText = (val) => {
    const text = val.replace(/\\n/g, '\n');
    return text.split('\n').map((line, index) => {
      let replacedText;

      // URL
      replacedText = reactStringReplace(
        line,
        /(https?:\/\/\S+)/g,
        (match, i) => (
          <a key={match + i} href={match}>{match}</a>
        )
      );

      // 電話番号
      replacedText = reactStringReplace(
        replacedText,
        /([0-9]+[\-]?[0-9]+[\-]?[0-9]+[\-]?[0-9]+)/g,
        (match, i) => (
          <TelLink key={match + i} tel={match}>{match}</TelLink>
        )
      );

      return (
        <React.Fragment key={index}>
          {replacedText}
          <br/>
        </React.Fragment>
      )
    });
  };

  return renderAutoResponseText(children);
};

export default StringReplace;
