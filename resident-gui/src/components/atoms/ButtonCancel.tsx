import React from 'react'
import styled from 'styled-components';
import ButtonBase from "./ButtonBase";
import { iconCrossMark } from "../assets/icon";

interface Props {
  onClick: () => void;
}

const ButtonCancel: React.FC<Props> = (props) => {
  return (
    <Button onClick={props.onClick}>
      <Icon/>
      予約をキャンセル
    </Button>
  );
};

const Button = styled(ButtonBase)`
  position: relative;
  width: 230px;
  height: 40px;
  border-radius: 25px;
  background-color: #d94e24;
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
`;

const Icon = styled.div`
  position: absolute;
  top: 14px;
  left: 25px;
  width: 14px;
  height: 14px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${iconCrossMark});
`;

export default ButtonCancel;
