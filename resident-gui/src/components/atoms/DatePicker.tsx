import React, { useEffect } from 'react'
import DatePicker, { registerLocale } from "react-datepicker";
import ja from 'date-fns/locale/ja';
import "react-datepicker/dist/react-datepicker.css";
import { parseDay, formatString } from '../../utils/date'

ja.options.weekStartsOn = 0;
registerLocale('ja', ja);

interface IProps {
  selected?: string;
  minDate?: Date;
  maxDate?: Date;
  onChange?: any;
}

const CustomClassName = 'preferable-date-custom-input';

const CustomDatePicker: React.FC<IProps> = (props) => {
  const {
    selected,
    minDate,
    maxDate,
    onChange,
  } = props;

  // コントロール選択時にキーボードが開くのでread_only属性を付与
  // ライブラリの「readOnly」を使うとカレンダーが開かないので独自で設定
  useEffect(() => {
    const inputs: HTMLCollectionOf<Element> =
      document.getElementsByClassName(CustomClassName);
    if (inputs && 0 < inputs.length) {
      for (let i = 0; i < inputs.length; i++) {
        const input = inputs[i] as HTMLInputElement;
        if (input) {
          input.readOnly = true;
        }
      }
    }
  });

  const handleChange = (date) => {
    const sdate = formatString(date);
    onChange(sdate);
  };

  const viewDate = parseDay(selected);

  return (
    <DatePicker
      className={CustomClassName}
      dateFormat="yyyy/MM/dd"
      locale='ja'
      selected={viewDate}
      minDate={minDate}
      maxDate={maxDate}
      onChange={handleChange}
      withPortal
    />
  );
};

export default CustomDatePicker;
