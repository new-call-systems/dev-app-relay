import React from 'react'
import styled from 'styled-components';
import ButtonBase from "./ButtonBase";

interface Props {
  onClick: () => void;
}

const ButtonScheduleDetail: React.FC<Props> = (props) => {
  return <Button onClick={props.onClick}>詳細を見る</Button>
};

const Button = styled(ButtonBase)`
  width: 103px;
  height: 28px;
  object-fit: contain;
  border-radius: 25px;
  background-color: #464f69;
  font-size: 12px;
  font-weight: 500;
  color: #ffffff;
  line-height: 12px;
`;

export default ButtonScheduleDetail;
