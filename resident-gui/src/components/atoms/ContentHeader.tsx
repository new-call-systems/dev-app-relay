import React from 'react'
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router'
import { btnBack } from "../assets/icon";
import { CONTENT_HEADER_HEIGHT } from "../../constants/ThreeRowsDesign";

interface IProps {
  title: string;
  backUrl?: string;
}

const ContentHeader: React.FC<IProps> = (props) => {
  const {
    title,
    backUrl,
  } = props;

  const dispatch = useDispatch();

  const onBack = (() => {
    dispatch(push(backUrl))
  });

  return (
    <Contents>
      {(() => {
        if (backUrl) {
          return (
            <BackButton
              src={btnBack}
              alt="back"
              onClick={onBack}
            />
          );
        } else {
          return <Dummy/>;
        }
      })()}
      <Title>
        {title}
      </Title>
    </Contents>
  );
};

const Contents = styled.header`
  width: 100%;
  height: ${CONTENT_HEADER_HEIGHT};
  display: flex;
  background-color: #ffff;
  overflow-y: hidden;
  z-index: 100;
`;

const BackButton = styled.img`
  width: 44px;
  height: 44px;
  margin: auto 1px auto 0;
  object-fit: contain;
`;

const Dummy = styled.div`
  margin-right: 10px;
`;

const Title = styled.p`
  margin-left: 1px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  text-align: left;
  color: #464f69;
`;

export default ContentHeader;
