import btnBack from './btn-back.svg';
import btnCancel from './btn-cancel.svg';
import btnInquiring from './btn-inquiring.svg';
import btnOk from './btn-ok.svg';
import btnPhotoDelete from './btn-photo-delete.svg';
import btnResolved from './btn-resolved.svg';
import btnSupport from './btn-support.svg';
import btnUnresolved from './btn-unresolved.svg';
import calendar from './calendar.png';
import cocoEnLogo from './Coco-en-logo.svg';
import feedback from './feedback.svg';
import home from './home.png';
import iconDate from './icon-date.svg';
import iconTime from './icon-time.svg';
import lineBtnLoginBase from './line_btn_login_base.png';
import lineBtnLoginHover from './line_btn_login_hover.png';
import robot from './robot.png';
import spinner from './spinner.svg';
import tabHome from './tab-home.svg';
import tabHomeOn from './tab-home-on.svg';
import tabRequest from './tab-request.svg';
import tabRequestOn from './tab-request-on.svg';
import tabSchedule from './tab-schedule.svg';
import tabScheduleOn from './tab-schedule-on.svg';
import user from './user.png';
import welcomeImg from './WelcomeImg.png';
import iconRobot from './icon-robot.svg';
import btnSend from './btn-send.svg';
import iconTel from './icon-tel.svg';
import btnPick from './btn-pic.svg';
import iconCrossMark from './icon-cross-mark.svg';
import iconBadge from './icon-badge.svg';

export {
  btnBack,
  btnCancel,
  btnInquiring,
  btnOk,
  btnPhotoDelete,
  btnResolved,
  btnSupport,
  btnUnresolved,
  calendar,
  cocoEnLogo,
  feedback,
  home,
  iconDate,
  iconTime,
  lineBtnLoginBase,
  lineBtnLoginHover,
  robot,
  spinner,
  tabHome,
  tabHomeOn,
  tabRequest,
  tabRequestOn,
  tabSchedule,
  tabScheduleOn,
  user,
  welcomeImg,
  iconRobot,
  btnSend,
  iconTel,
  btnPick,
  iconCrossMark,
  iconBadge,
};
