const color = {
  MAIN_LIGHT: '#bbdffd',
  MAIN_DARK: '#259afb',
  ACTIVE: '#ffffff',
  INACTIVE: '#67b7fc',
  WHITE: '#ffffff',
  CAUTION: '#ff0000',
  LINE: '#f0f0f0',
};
export default color;
