import React from 'react'
import styled from 'styled-components';

interface Props {
  text: string;
}

const ChatUserText: React.FC<Props> = (props) => {
  const { text } = props;
  return (
    <Text>{text}</Text>
  );
};

const Text = styled.p`
  font-size: 12px;
  font-weight: bold;
  color: #ffffff;
  word-break: break-all;
`;

export default ChatUserText;
