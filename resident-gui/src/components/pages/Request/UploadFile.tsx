import React, { useState, useRef, useEffect } from 'react'
import styled from 'styled-components';
import { btnPhotoDelete, btnPick } from "../../assets/icon";
import { getDateTimeMillisecond } from "../../../utils/date";
import { useFileResource, Resource } from "../../../hooks/useFileResource";
import { compressImage } from "../../../utils/image";

interface IProps {
  reverseElementId: (id: string) => void;
}

const UploadFile: React.FC<IProps> = (props) => {
  const { reverseElementId } = props;
  const { resources, addResources, removeResources } = useFileResource();

  // FileUpload用
  // Element[input file]を2つ用意する
  // １．Clickイベント発火用をrefで作成
  // ２．Fileオブジェクトの保管用(ReduxStoreにFileを保持できない)をidで管理
  const ref = useRef<HTMLInputElement>(null);
  const [fileElementId] = useState<string>('file-upload-' + getDateTimeMillisecond());

  // InputFileのDomIdを返却
  useEffect(() => {
    reverseElementId(fileElementId);
  }, []);

  // ファイル追加
  // InputFileのClickイベントを発火
  const onClick = (e) => {
    e.preventDefault();
    ref.current?.click()
  };

  // ファイル削除
  const remove = (index: number) => {
    removeResources(index);
    removeInputDomFile(index);
  };

  // プレビューに必要な値を保持
  const handleChangeFile = files => {
    addResources(files);
    addInputDomFile(files);
  };

  // 追加 InputFile(Id)
  const addInputDomFile = (newFiles) => {
    (async () => {
      let fileList = getDomFiles();
      await Promise.all(
        Object.keys(newFiles).map(async item => {
          const file: File = newFiles[Number(item)];
          let convertFile = null;
          if (0 <= file.type.indexOf('video')) {
            convertFile = file;
          } else {
            convertFile = await compressImage(file);
          }
          fileList.push(convertFile);
        })
      );
      setDomFiles(fileList);
    })();
  };

  // 削除 InputFile(Id)
  const removeInputDomFile = (index: number) => {
    const fileList = getDomFiles();
    const newFileList = fileList.filter((_, i) => i !== index);
    setDomFiles(newFileList);
  };

  // DomオブジェクトからFileリストを取得
  const getDomFiles = (): File[] => {
    const input = getFileElement();
    return input?._value || [];
  };

  // DomオブジェクトにFileリストをセット
  const setDomFiles = (fileList: File[]) => {
    const input = getFileElement();
    input._value = fileList;
  };

  // iOS safariではFileListを利用するためのDataTransferが利用できない
  // そのため_valueという存在しなプロパティにFile[]を格納する
  // _valueは存在しないプロパティのためTSの型をanyとする
  const getFileElement = (): any => {
    return document.getElementById(fileElementId);
  };

  return (
    <>
      <input
        type="file"
        accept="image/*,video/*"
        multiple
        ref={ref}
        style={{display: 'none'}}
        value=''
        onChange={e => handleChangeFile(e.target.files)}
      />
      <input
        type="file"
        accept="image/*,video/*"
        multiple
        style={{display: 'none'}}
        id={fileElementId}
      />
      <Image
        src={btnPick}
        alt=""
        onClick={onClick}
      />
      {resources.map((resource: Resource, i: number) => {
        const { fileType, previewUrl } = resource;
        const isVideo = 0 <= fileType.indexOf('video');
        return (
          <ImageArea key={i}>
            {isVideo
              ? <PreviewVideo source={previewUrl}/>
              : <PreviewImage source={previewUrl}/>
            }
            <IconImageDelete
              onClick={() => remove(i)}
            />
          </ImageArea>
        )
      })}
    </>
  );
};

const PreviewVideo = ({ source }) => {
  return (
    <Video controls>
      <source src={source}/>
    </Video>
  );
};

const PreviewImage = ({ source }) => {
  return <Image src={source} alt=""/>
};

const ImageArea = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 100px;
  height: 75px;
  margin-top: 7px;
  margin-left: 5px;
`;

const Video = styled.video`
  width: 100px;
  height: 75px;
  margin-top: 7px;
  margin-left: 5px;
`;

const IconImageDelete = styled.div`
  position: absolute;
  bottom: 5px;
  right: 1px;
  width: 19px;
  height: 19px;
  background-position: center center;
  background-repeat: no-repeat;
  background-image: url(${btnPhotoDelete});
  // pointer-events: none;
`;

export default UploadFile;
