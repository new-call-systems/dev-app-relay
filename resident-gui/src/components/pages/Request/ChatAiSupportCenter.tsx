import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import styled from 'styled-components';
import { iconTel } from '../../assets/icon';
import { TelLink } from "../../atoms";
import { RootState } from "../../../store";

import ChatAiText from "./ChatAiText";
import ButtonBase from "../../atoms/ButtonBase";
import { setDisabledForm, setSupportCenterFileUpload } from "../../../reducers/request";
import UploadFile from "./UploadFile";

const text = '申し訳ありません。問題解決できませんでした。サポートセンターまでお問合せください。';
const upload = '問題の早期改善のために問題箇所の写真・動画をお送りください。';

const ChatAiSupportCenter: React.FC = () => {
  const dispatch = useDispatch();
  const { handleSubmit, control } = useForm();
  const disabledForm: boolean = useSelector((state: RootState) => state.request.disabledForm);
  const tel = useSelector((state: RootState) => state.request.supportTel);

  const onSubmit = (data: any) => {
    if (!disabledForm) {
      dispatch(setDisabledForm(true));
      dispatch(setSupportCenterFileUpload(data));
    }
  };

  return (
    <>
      <ChatAiText text={text}/>
      <TelLink tel={tel}>
        <Button>
          <Icon/>
          {tel}
        </Button>
      </TelLink>
      <SectionText>
        <ChatAiText text={upload}/>
      </SectionText>
      <SectionImages>
        <Controller
          control={control}
          name={`fileElementId`}
          defaultValue={null}
          render={({ onChange }) => (
            <UploadFile
              reverseElementId={onChange}
            />
          )}
        />
      </SectionImages>
      <SectionBorder/>
      <ButtonArea>
        <ButtonSubmit onClick={handleSubmit(onSubmit)}>
          ファイルの送信
        </ButtonSubmit>
      </ButtonArea>
    </>
  );
};

const Button = styled(ButtonBase)`
  position: relative;
  width: 100%;
  height: 40px;
  padding: 0 7px;
  margin-top: 10px;
  border-radius: 20px;
  background-color: #f25a89;
  font-size: 12px;
  font-weight: bold;
  color: #ffffff;
  line-height: 12px;
`;

const Icon = styled.div`
  position: absolute;
  top: 5px;
  left: 12px;
  width: 30px;
  height: 30px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${iconTel});
`;

const SectionText = styled.div`
  flex-wrap: wrap;
  margin-top: 30px;
`;

const SectionImages = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
`;

const SectionBorder = styled.div`
  margin: 30px 0 15px 0;
  border-top: solid 1px #dee2e6;
`;

const ButtonArea = styled.div`
  display: flex;
  justify-content: space-around;
`;

const ButtonSubmit = styled(ButtonBase)`
  width: 80%;
  height: 40px;
  border-radius: 20px;
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
  background-color: #464f69;
`;

export default ChatAiSupportCenter;
