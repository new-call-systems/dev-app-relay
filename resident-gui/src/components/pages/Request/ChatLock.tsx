import React from 'react'
import styled from 'styled-components';

interface Props {
  lock: boolean | null | undefined;
}

const ChatLock: React.FC<Props> = (props) => {
  const { lock } = props;
  if (lock) {
    return <Lock/>
  }
  return null;
};

const Lock = styled.div`
  position: absolute;
  top:0;
  left: 0;
  bottom: 0;
  right: 0;
  background: rgba(0,0,0,0);
`;

export default ChatLock;
