import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { iconDate, iconTime } from "../../assets/icon";
import { formatTZ } from "../../../utils/date";
import ButtonBase from "../../atoms/ButtonBase";
import { ITimeResponse } from "../../../api/schema/request";
import { RootState } from "../../../store";
import { useAsync } from "../../../hooks/useAsync";
import { Spinner } from "../../atoms";
import PreviewMedia from "./PreviewMedia";
import {
  cancelVendorReserve,
  submitVendorReserve,
  setDisabledForm,
  setLockMessage, appendMessages,
} from "../../../reducers/request";

interface Props {
  index: number;
  data: any;
}

const ChatUserVendorConfirm: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [asyncAction, loading] = useAsync();
  const requestId: number = useSelector((state: RootState) => state.request.aiResult.specific.request_id);
  const preferableTimes: ITimeResponse[] = useSelector((state: RootState) => state.request.preferableTimes);
  const { index, data } = props;

  const onCancel = () => {
    if (loading) {
      return;
    }
    dispatch(setDisabledForm(false));
    dispatch(cancelVendorReserve());
  };

  const onSubmit = () => {
    if (loading) {
      return;
    }
    asyncAction(
      submitVendorReserve(requestId, data),
      successSubmit(dispatch)
    );
  };

  const successSubmit = (dispatch) => {
    return (data) => {
      dispatch(appendMessages(data));
      dispatch(setLockMessage({index: index, lock: true}));
    }
  };

  return (
    <>
      <Title>予定日程調整</Title>
      <Confirm>
        {(() => {
          if (data.emergencyFlg) {
            return (
              <SectionCandidate>
                <TextCandidate>緊急対応</TextCandidate>
              </SectionCandidate>
            );
          } else {
            return (
              <List>
                {[1, 2, 3].map((v) => {
                  return (
                    <li key={v}>
                      <SectionCandidate>
                        <IconCandidate/>
                        <TextCandidate>第{v}候補</TextCandidate>
                      </SectionCandidate>
                      <SectionDateTime>
                        <IconDate/>
                        <TextDateTime>
                          {formatTZ(data[`preferableDay${v}`])}
                        </TextDateTime>
                      </SectionDateTime>
                      <SectionDateTime>
                        <IconTime/>
                        <TextDateTime>
                          {fetchTime(preferableTimes, data[`preferableTime${v}`])}
                        </TextDateTime>
                      </SectionDateTime>
                    </li>
                  )
                })}
              </List>
            );
          }
        })()}
        <PreviewMedia elementId={data.fileElementId}/>
        <SectionButton>
          <ButtonSubmit onClick={onSubmit}>
            {loading ? <Spinner/> : 'ご予約内容を確定'}
          </ButtonSubmit>
          <ButtonEdit onClick={onCancel}>
            ご予約内容を変更
          </ButtonEdit>
        </SectionButton>
      </Confirm>
    </>
  );
};

const fetchTime = (timeSection, sectionId: string) => {
  const iSectionId = parseInt(sectionId);
  const row = timeSection
    .find((v) => v.time_section_id === iSectionId);
  if (row) {
    return row.label;
  }
  return '';
};

const Title = styled.p`
  font-size: 12px;
  font-weight: bold;
  color: #ffffff;
`;

const Confirm = styled.div`
  width: 100%;
  margin-top: 6px;
  padding: 5px 0;
  border-radius: 10px;
  background-color: #ffff;
`;

const List = styled.ul`
  margin-top: 7px;
  margin-bottom: 11px;
  list-style: none;
`;

const SectionCandidate = styled.div`
  margin-left: 8px;
  display: flex;
  align-items: center;
`;

const IconCandidate = styled.div`
  width: 13px;
  height: 13px;
  border-radius: 2px;
  background-color: #464f69;
`;

const TextCandidate = styled.p`
  margin-left: 7px;
  font-size: 14px;
  color: #464f69;
`;

const SectionDateTime = styled.div`
  margin-left: 26px;
  display: flex;
  align-items: center;
`;

const IconDate = styled.div`
  width: 20px;
  height: 20px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${iconDate});
  pointer-events: none;
`;

const IconTime = styled.div`
  width: 20px;
  height: 20px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${iconTime});
  pointer-events: none;
`;

const TextDateTime = styled.p`
  margin: 3px 0 3px 7px;
  font-size: 12px;
  color: #8692b5;
`;

const SectionButton = styled.div`
  margin: 15px 0;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Button = styled(ButtonBase)`
  margin: 5px 10px;
  height: 40px;
  border-radius: 20px;
  font-size: 12px;
  font-weight: bold;
  line-height: 12px;
`;

const ButtonSubmit = styled(Button)`
  background-color: #ffda00;
  color: #464f69;
`;

const ButtonEdit = styled(Button)`
  background-color: #464f69;
  color: #ffffff;
`;

export default ChatUserVendorConfirm;
