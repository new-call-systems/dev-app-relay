import React from 'react'
import styled, { keyframes  } from 'styled-components';
import { Message } from '../../../reducers/request';
import { iconRobot } from '../../assets/icon';

import ChatBaseLayout, { BalloonBase } from "./ChatBaseLayout";
import ChatAiText from "./ChatAiText";
import ChatAiInputFinished from "./ChatAiInputFinished";
import ChatAiSupportCenter from "./ChatAiSupportCenter";
import ChatAiChooseOption from "./ChatAiChooseOption";
import ChatAiFaqDiy from "./ChatAiFaqDiy";
import ChatAiConfirmSolved from "./ChatAiConfirmSolved";
import ChatAiVendorArrange from "./ChatAiVendorArrange";
import ChatAiUnknown from "./ChatAiUnknown";
import ChatLock from "./ChatLock";

interface Props {
  data: Message;
}

const ChatAi: React.FC<Props> = (props) => {
  const { data } = props;
  return (
    <ChatLayout
      className={`balloon_${data.type}`}
      leftComponent={<Icon/>}
      centerComponent={
        <Balloon>
          <ChatLock lock={data?.data?.lock}/>
          {renderContent(data)}
        </Balloon>
      }
    />
  );
};

const renderContent = (data) => {
  switch (data.type) {
    case 'text':
      //-----------------------------------------------
      // テキスト
      //-----------------------------------------------
      return <ChatAiText text={data.data.text}/>;
    case 'input_finished':
      //-----------------------------------------------
      // 入力確定ボタン
      //-----------------------------------------------
      return <ChatAiInputFinished index={data.index}/>;
    case 'support_center':
      //-----------------------------------------------
      // サポートセンター
      //-----------------------------------------------
      return <ChatAiSupportCenter/>;
    case 'choose_option':
      //-----------------------------------------------
      // QtoQ ラベル選択
      //-----------------------------------------------
      return <ChatAiChooseOption index={data.index} labels={data.data.labels}/>;
    case 'faq_diy':
      //-----------------------------------------------
      // DIY or FAQ
      //-----------------------------------------------
      return <ChatAiFaqDiy text={data.data.text}/>;
    case 'confirm_solved':
      //-----------------------------------------------
      // DIY or FAQ [解決][未解決]ボタン
      //-----------------------------------------------
      return <ChatAiConfirmSolved index={data.index}/>;
    case 'vendor':
      //-----------------------------------------------
      // 業者予約入力フォーム
      //-----------------------------------------------
      return <ChatAiVendorArrange/>;
    case 'unknown':
      //-----------------------------------------------
      // AI判定不可
      //-----------------------------------------------
      return <ChatAiUnknown/>;
    default:
      return null;
  }
};

const effect = keyframes`
  0% { opacity: 0; }
  90% { opacity: 1; }
  100% { opacity: 1; }
`;

const ChatLayout = styled(ChatBaseLayout)`
  &.balloon_input_finished {
    animation-name: ${effect};
    animation-duration: 2s;
    animation-fill-mode: forwards;
  }
`;

const Icon = styled.div`
  width: 41px;
  height: 41px;
  margin: 11px 0 0 2px; 
  background-position: center center;
  background-repeat: no-repeat;
  background-image: url(${iconRobot});
`;

const Balloon = styled(BalloonBase)`
  background-color: #ffffff;

  // 吹き出し
  position: relative;
  
  // 吹き出し 左側に配置
  &:after, &:before {
    right: 100%;
    top: 50%;
    border: solid transparent;
    content: "";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  // 吹吹き出し ベース
  &:after {
    border-color: rgba(255, 255, 255, 0);
    border-right-color: #ffffff;
    border-width: 7px 14px 14px 0;
    margin-top: -13px;
  }

  // 吹き出し　ボーダー
  &:before {
    border-color: rgba(230, 230, 230, 0);
    border-right-color: #e6e6e6;
    border-width: 10px 18px 17px 0;
    margin-top: -15px;
  }
`;

export default ChatAi;
