import React from 'react'
import styled from 'styled-components';
import { useFileResource, Resource } from "../../../hooks/useFileResource";
import { ModalImage } from "../../atoms";

interface IProps {
  elementId: string;
}

const PreviewMedia: React.FC<IProps> = (props) => {
  const { elementId } = props;
  const { resources } = useFileResource(elementId);

  return (
    <Section>
      {resources.map((resource: Resource, i: number) => {
        const { fileType, previewUrl } = resource;
        const isVideo = 0 <= fileType.indexOf('video');
        return (
          <Media key={i}>
            {isVideo
              ? <PreviewVideo src={previewUrl}/>
              : <PreviewImage src={previewUrl}/>
            }
          </Media>
        )
      })}
    </Section>
  );
};

const PreviewVideo = ({ src }) => {
  return (
    <Video controls>
      <source src={src}/>
    </Video>
  );
};

const PreviewImage = ({ src }) => {
  return (
    <ModalImage
      src={src}
      style={ImageStyle}
    />
  );
};

const Section = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Media = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px 5px;
  height: 160px;
  background-color: #000000;
`;

const ImageStyle: React.CSSProperties = {
  maxHeight: '150px',
  maxWidth: '200px',
};

const Video = styled.video`
  max-height: 150px;
  max-width: 200px;
`;

export default PreviewMedia;
