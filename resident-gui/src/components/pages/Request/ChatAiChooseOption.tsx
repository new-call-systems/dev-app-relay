import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useAsync } from "../../../hooks/useAsync";
import { RootState } from "../../../store";
import ButtonBase from "../../atoms/ButtonBase";
import {
  selectOption,
  postMessage,
  successPostMessage,
  AiLabel,
  setLockMessage,
} from "../../../reducers/request";

interface Props {
  index: number;
  labels: AiLabel[];
}

const ChatAiChooseOption: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [asyncAction] = useAsync();
  const [lock, setLock] = useState(false);
  const aiResult = useSelector((state: RootState) => state.request.aiResult);
  const messages = useSelector((state: RootState) => state.request.messages);
  const { index, labels } = props;

  const onClick = (label) => {
    if (lock) {
      return;
    }
    setLock(true);
    dispatch(setLockMessage({index: index, lock: true}));
    dispatch(selectOption(label.text));
    asyncAction(
      postMessage(messages, label.aiText, aiResult),
      (payload) => dispatch(successPostMessage(payload))
    );
  };

  return (
    <List>
      {labels.map((v: AiLabel, i) => {
        return (
          <Button
            key={i}
            onClick={() => onClick(v)}
          >
            {v.text}
          </Button>
        );
      })}
    </List>
  );
};

const List = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: space-around;
`;

const Button = styled(ButtonBase)`
  width: 100%;
  height: 100%;
  min-height: 40px;
  margin: 6px 0 6px 0;
  padding: 7px;
  border-radius: 20px;
  background-color: #ffda00;
  font-size: 12px;
  font-weight: bold;
  color: #464f69;
  line-height: 12px;
`;

export default ChatAiChooseOption;
