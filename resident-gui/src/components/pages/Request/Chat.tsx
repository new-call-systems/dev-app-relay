import React, { useEffect, useCallback, useRef } from 'react'
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { RootState } from "../../../store";

import ChatAi from "./ChatAi";
import ChatUser from "./ChatUser";

const Chat: React.FC = () => {
  const messages = useSelector((state: RootState) => state.request.messages);

  // 最下部まで自動スクロール
  const ref = useRef<HTMLInputElement>(null);
  const scrollToBottomOfList = useCallback(() => {
    ref?.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
    })
  }, [ref]);

  useEffect(() => {
    scrollToBottomOfList()
  }, [messages]);

  return (
    <Main>
      <TimeLine ref={ref}>
        {messages.map((v, i) => {
          return (
            <React.Fragment key={i}>
              {v.owner === 'ai'
                ? <ChatAi data={v}/>
                : <ChatUser data={v}/>
              }
            </React.Fragment>
          );
        })}
        <Dummy/>
      </TimeLine>
    </Main>
  );
};

const Main = styled.main`
  flex: 1;
  overflow-y: auto;
  
  // モーダルが効かなくなるのでコメントアウト
  // スクロールがカクカクする
  // -webkit-overflow-scrolling: touch;
`;

const TimeLine = styled.div`
  background: #f9f9f8;
`;

const Dummy = styled.div`
  height: 10px;
`;

export default Chat;
