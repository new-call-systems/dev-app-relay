import React from 'react'
import styled from 'styled-components';
import { Message } from "../../../reducers/request";

import ChatBaseLayout, { BalloonBase } from "./ChatBaseLayout";
import ChatUserText from "./ChatUserText";
import ChatUserVendorConfirm from "./ChatUserVendorConfirm";
import ChatUserFileUploadConfirm from "./ChatUserFileUploadConfirm";
import ChatLock from "./ChatLock";

interface Props {
  data: Message;
}

const ChatUser: React.FC<Props> = (props) => {
  const { data } = props;
  return (
    <ChatBaseLayout
      centerComponent={
        <Balloon>
          <ChatLock lock={data?.data?.lock}/>
          {renderContent(data)}
        </Balloon>
      }
    />
  );
};

const renderContent = (data) => {
  switch (data.type) {
    case 'text':
      //-----------------------------------------------
      // テキスト
      //-----------------------------------------------
      return <ChatUserText text={data.data.text}/>;
    case 'original_text':
      //-----------------------------------------------
      // ユーザ入力値
      //-----------------------------------------------
      return <ChatUserText text={data.data.text}/>;
    case 'vendor_confirm':
      //-----------------------------------------------
      // 業者予約 確認フォーム
      //-----------------------------------------------
      return <ChatUserVendorConfirm index={data.index} data={data.data}/>;
    case 'file_upload_confirm':
      //-----------------------------------------------
      // 画像・動画アップロード 確認フォーム
      //-----------------------------------------------
      return <ChatUserFileUploadConfirm index={data.index} data={data.data}/>;
    default:
      return null;
  }
};

const Balloon = styled(BalloonBase)`
  background-color: #3d9648;

  // 吹き出し
  position: relative;

  // 吹き出し 左側に配置
  &:after, &:before {
    left: 100%;
    top: 50%;
    border: solid transparent;
    content: "";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  // 吹吹き出し ベース
  &:after {
    border-color: rgba(61, 150, 72, 0);
    border-left-color: #3d9648;
    border-width: 10px 0 4px 12px;
    margin-top: -10px;
  }

  // 吹き出し　ボーダー
  &:before {
    border-color: rgba(230, 230, 230, 0);
    border-left-color: #e6e6e6;
    border-width: 14px 0 6px 16px;
    margin-top: -12px;
  }
`;

export default ChatUser;
