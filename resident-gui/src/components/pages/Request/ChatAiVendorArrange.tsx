import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import styled from 'styled-components';
import { RootState } from "../../../store";
import { addDay, getToday, parseDay } from "../../../utils/date";
import { ITimeResponse } from "../../../api/schema/request";
import { DatePicker } from "../../atoms";
import { iconDate } from "../../assets/icon";
import { setVendorReserve, setDisabledForm } from "../../../reducers/request";
import ButtonBase from "../../atoms/ButtonBase";
import UploadFile from "./UploadFile";

const text = '以上の件は業者がお伺いの上、対応致します。日程はいつがよろしいですか？緊急の場合は、日程を指定せず、[緊急]ボタンを選択してください。';
const textEmergency = '問題箇所の写真・動画が撮れる場合は下記から写真・動画を投稿してください。';

const ChatAiVendorArrange: React.FC = () => {
  const dispatch = useDispatch();
  const { handleSubmit, control } = useForm();
  const disabledForm: boolean = useSelector((state: RootState) => state.request.disabledForm);
  const preferableTimes: ITimeResponse[] = useSelector((state: RootState) => state.request.preferableTimes);
  const toDay = getToday();
  const minDate = parseDay(addDay(toDay, 1));
  const defaultTime = 0 < preferableTimes.length ? preferableTimes[0].time_section_id : 0;

  const onSubmitReserve = (data: any) => {
    onSubmit(data, false);
  };
  const onSubmitEmergency = (data: any) => {
    onSubmit(data, true);
  };
  const onSubmit = (data: any, emergencyFlg: boolean) => {
    if (!disabledForm) {
      dispatch(setDisabledForm(true));
      dispatch(setVendorReserve(data, emergencyFlg));
    }
  };

  return (
    <>
      <Text>{text}</Text>
      <Title>予定日程調整</Title>
      <List>
        {[1, 2, 3].map((v) => {
          const preferableDay = addDay(toDay, v);
          return (
            <li key={v}>
              <TextCandidate>第{v}候補</TextCandidate>
              <SectionDateTime>
                <DateArea>
                  <Controller
                    control={control}
                    name={`preferableDay${v}`}
                    defaultValue={preferableDay}
                    render={({onChange, value}) => (
                      <DatePicker
                        selected={value}
                        minDate={minDate}
                        onChange={onChange}
                      />
                    )}
                  />
                  <IconDate/>
                </DateArea>
                <Controller
                  control={control}
                  name={`preferableTime${v}`}
                  defaultValue={defaultTime}
                  render={({onChange, value}) => (
                    <TimeList value={value} onChange={onChange}>
                      {preferableTimes.map((v) => {
                        return (
                          <option
                            key={v.time_section_id}
                            value={v.time_section_id}
                          >
                            {v.label}
                          </option>
                        )
                      })}
                    </TimeList>
                  )}
                />
              </SectionDateTime>
            </li>
          )
        })}
      </List>
      <TextEmergencyBold>スピード解決をご希望の方へ</TextEmergencyBold>
      <TextEmergency>{textEmergency}</TextEmergency>
      <SectionImages>
        <Controller
          control={control}
          name={`fileElementId`}
          defaultValue={null}
          render={({ onChange }) => (
            <UploadFile
              reverseElementId={onChange}
            />
          )}
        />
      </SectionImages>
      <SectionBorder/>
      <ButtonArea>
        <ButtonReserve onClick={handleSubmit(onSubmitReserve)}>
          予約
        </ButtonReserve>
        <ButtonEmergency onClick={handleSubmit(onSubmitEmergency)}>
          緊急
        </ButtonEmergency>
      </ButtonArea>
    </>
  );
};


const TextMedium = styled.p`
  font-size: 12px;
  font-weight: 500;
`;

const TextBold = styled.p`
  font-size: 12px;
  font-weight: bold;
`;

const Text = styled(TextMedium)`
  color: #464f69;
`;

const Title = styled(TextBold)`
  margin-top: 10px;
  color: #1f263b;
`;

const TextEmergency = styled(TextMedium)`
  color: #cc436d;
`;

const TextEmergencyBold = styled(TextBold)`
  color: #cc436d;
`;

const TextCandidate = styled.p`
  font-size: 14px;
  font-weight: 500;
  color: #464f69;
`;

const List = styled.ul`
  margin-top: 7px;
  margin-bottom: 11px;
  list-style: none;
`;

const SectionDateTime = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0 1px 7px 5px;
`;

const DateArea = styled.div`
  position: relative;
  margin-top: 2px;
  margin-right: 4px;

  input[type="text"] {
    width: 97px;
    height: 21px;
    padding: 6px 7px 5px 8px;
    border-radius: 3px;
    border: solid 1px #dee2e6;
    background-color: #ffffff;    
    font-size: 12px;
    color: #464f69;
  }
`;

const IconDate = styled.div`
  position: absolute;
  top: 8px;
  right: 5px;
  width: 20px;
  height: 20px;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url(${iconDate});
  pointer-events: none;
`;

const TimeList = styled.select`
  width: 113px;
  height: 33px;
  margin-top: 2px;
  padding: 6px 2px 7px 5px;
  border-radius: 3px;
  border: solid 1px #dee2e6;
  background-color: #ffffff;
  font-size: 12px;
  color: #464f69;
`;

const SectionImages = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
`;

const SectionBorder = styled.div`
  margin: 30px 0 15px 0;
  border-top: solid 1px #dee2e6;
`;

const ButtonArea = styled.div`
  display: flex;
  justify-content: space-around;
`;

const Button = styled(ButtonBase)`
  width: 40%;
  height: 40px;
  border-radius: 20px;
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
`;

const ButtonReserve = styled(Button)`
  background-color: #464f69;
`;

const ButtonEmergency = styled(Button)`
  background-color: #f25a89;
`;

export default ChatAiVendorArrange;
