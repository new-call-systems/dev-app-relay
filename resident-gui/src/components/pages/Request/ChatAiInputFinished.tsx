import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { useAsync } from "../../../hooks/useAsync";
import { RootState } from "../../../store";
import ButtonBase from "../../atoms/ButtonBase";
import { Spinner } from "../../atoms";
import {
  postMessage,
  successPostMessage,
  disabledUserInput,
  setLockMessage,
} from "../../../reducers/request";

import ChatAiText from "./ChatAiText";

const text = 'お問合せ内容を確定する場合は[お問合せ内容を確定]ボタンを押してください。';

interface Props {
  index: number;
}

const ChatAiInputFinished: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [asyncAction, loading] = useAsync();
  const messages = useSelector((state: RootState) => state.request.messages);
  const { index } = props;

  const onClick = () => {
    if (loading) {
      return;
    }
    asyncAction(
      postMessage(messages),
      successPost(dispatch)
    );
  };

  const successPost = (dispatch) => {
    return (data) => {
      dispatch(successPostMessage(data));
      dispatch(disabledUserInput());
      dispatch(setLockMessage({index: index, lock: true}));
    }
  };

  return (
    <>
      <ChatAiText text={text}/>
      <Button onClick={onClick}>
        {loading ? <Spinner/> : 'お問合せ内容を確定'}
      </Button>
    </>
  );
};

const Button = styled(ButtonBase)`
  width: 100%;
  height: 40px;
  padding: 0 7px;
  margin-top: 10px;
  border-radius: 20px;
  background-color: #ffda00;
  font-size: 12px;
  font-weight: bold;
  color: #464f69;
  line-height: 12px;
`;

export default ChatAiInputFinished;
