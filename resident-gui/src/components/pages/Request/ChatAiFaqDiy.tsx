import React from 'react'
import styled from 'styled-components';
import { StringReplace } from "../../atoms";

interface Props {
  text: string;
}

const ChatAiFaqDiy: React.FC<Props> = (props) => {
  const { text } = props;
  return (
    <Text>
      <StringReplace>
        {text}
      </StringReplace>
    </Text>
  );
};

const Text = styled.p`
  font-size: 12px;
  font-weight: 500;
  color: #464f69;
  
  a {
    word-break: break-all;
  }
`;

export default ChatAiFaqDiy;
