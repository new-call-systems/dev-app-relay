import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import TextareaAutosize from 'react-textarea-autosize';
import { useForm  } from 'react-hook-form';
import { RootState } from "../../../store";
import { btnSend }  from '../../assets/icon';
import {
  pushMessageConfirmFinished,
  pushMessageUserInput
} from "../../../reducers/request";

const UserInput: React.FC = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, reset } = useForm();
  const disabledUserInput: boolean = useSelector((state: RootState) => state.request.disabledUserInput);

  const onSubmit = handleSubmit((data: any) => {
    const message = data.txtMessage.replace(/\r?\n/g, '');
    if (message !== '') {
      dispatch(pushMessageUserInput(message));
      reset({txtMessage: ''});
      // 確認ボタンを出すタイミングを調整
      setTimeout(() => {
        dispatch(pushMessageConfirmFinished());
      }, 2500);
    }
  });

  if (disabledUserInput) {
    return null;
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Input>
        <TextareaAutosize
          name="txtMessage"
          placeholder={"メッセージを送信"}
          minRows={1}
          maxRows={3}
          ref={register({required: true})}
        />
        <button/>
      </Input>
    </form>
  );
};

const Input = styled.div`
  position: relative;
  display: flex;
  height: 100%;
  min-height: 85px;
  background-color: #ffffff;

  textarea {
    margin: 7px 5px 5px 33px;
    padding: 10px 11px;
    width: 100%;
    font-size: 16px;
    color: #464f69;
    border-radius: 20px;
    border: solid 1px #e1e1e1;
    background-color: #f5f5f5;
    resize: none;
    outline: none;
    appearance: none;
  }
  
  textarea:focus {
    outline: none;
  }
  
  button {
    margin: 5px 7px auto auto;
    top: 0;
    bottom: 0;
    right: 0;
    width: 44px;
    height: 44px;
    border: none;
    background-color: #ffffff;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url(${btnSend});
  }
`;

export default UserInput;
