import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import ButtonBase from "../../atoms/ButtonBase";
import { useAsync } from "../../../hooks/useAsync";
import { RootState } from "../../../store";
import { Spinner } from "../../atoms";
import PreviewMedia from "./PreviewMedia";
import {
  appendMessages,
  cancelFileUpload,
  setDisabledForm,
  setLockMessage,
  submitFileUpload,
} from "../../../reducers/request";

interface Props {
  index: number;
  data: any;
}

const ChatUserFileUploadConfirm: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [asyncAction, loading] = useAsync();
  const requestId: number = useSelector((state: RootState) => state.request.aiResult.specific.request_id);
  const { index, data } = props;

  const onCancel = () => {
    if (loading) {
      return;
    }
    dispatch(setDisabledForm(false));
    dispatch(cancelFileUpload());
  };

  const onSubmit = () => {
    if (loading) {
      return;
    }
    asyncAction(
      submitFileUpload(requestId, data),
      successSubmit(dispatch)
    );
  };

  const successSubmit = (dispatch) => {
    return (data) => {
      dispatch(appendMessages(data));
      dispatch(setLockMessage({index: index, lock: true}));
    }
  };

  return (
    <>
      <Title>ファイルアップロードの確認</Title>
      <Confirm>
        <PreviewMedia elementId={data.fileElementId}/>
        <SectionButton>
          <ButtonSubmit onClick={onSubmit}>
            {loading ? <Spinner/> : 'ファイルの送信を確定'}
          </ButtonSubmit>
          <ButtonEdit onClick={onCancel}>
            ファイルの送信を変更
          </ButtonEdit>
        </SectionButton>
      </Confirm>
    </>
  );
};

const Title = styled.p`
  font-size: 12px;
  font-weight: bold;
  color: #ffffff;
`;

const Confirm = styled.div`
  width: 100%;
  margin-top: 6px;
  padding: 5px 0;
  border-radius: 10px;
  background-color: #ffff;
`;

const SectionButton = styled.div`
  margin: 15px 0;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Button = styled(ButtonBase)`
  margin: 5px 10px;
  height: 40px;
  border-radius: 20px;
  font-size: 12px;
  font-weight: bold;
  line-height: 12px;
`;

const ButtonSubmit = styled(Button)`
  background-color: #ffda00;
  color: #464f69;
`;

const ButtonEdit = styled(Button)`
  background-color: #464f69;
  color: #ffffff;
`;

export default ChatUserFileUploadConfirm;
