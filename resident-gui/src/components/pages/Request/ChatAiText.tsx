import React from 'react'
import styled from 'styled-components';

interface Props {
  text: string;
}

const ChatAiText: React.FC<Props> = (props) => {
  const { text } = props;
  return (
    <Text>{text}</Text>
  );
};

const Text = styled.p`
  font-size: 12px;
  font-weight: 500;
  color: #464f69;
`;

export default ChatAiText;
