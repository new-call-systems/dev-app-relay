import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import ChatAiText from "./ChatAiText";
import ButtonBase from "../../atoms/ButtonBase";
import { RootState } from "../../../store";
import { useAsync } from "../../../hooks/useAsync";
import {
  requestSolved,
  setLockMessage,
  successPostMessage
} from "../../../reducers/request";

const text = '解決しましたか？';

interface Props {
  index: number;
}

const ChatAiConfirmSolved: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [asyncAction] = useAsync();
  const [lock, setLock] = useState(false);
  const aiResult = useSelector((state: RootState) => state.request.aiResult);
  const { index } = props;

  const onClickYes = () => {
    dispatchSolved(true);
  };

  const onClickNo = () => {
    dispatchSolved(false);
  };

  const dispatchSolved = (solved: boolean) => {
    if (lock) {
      return;
    }
    setLock(true);
    dispatch(setLockMessage({index: index, lock: true}));
    asyncAction(
      requestSolved(solved, aiResult),
      (payload) => dispatch(successPostMessage(payload))
    );
  };

  return (
    <>
      <ChatAiText text={text}/>
      <ButtonArea>
        <Button onClick={onClickYes}>はい</Button>
        <Button onClick={onClickNo}>いいえ</Button>
      </ButtonArea>
    </>
  );
};

const ButtonArea = styled.div`
  display: flex;
  justify-content: space-around;
`;

const Button = styled(ButtonBase)`
  width: 40%;
  height: 40px;
  margin: 14px 5px 0 5px;
  border-radius: 20px;
  background-color: #ffda00;
  font-size: 12px;
  font-weight: bold;
  color: #464f69;
  line-height: 12px;
`;

export default ChatAiConfirmSolved;
