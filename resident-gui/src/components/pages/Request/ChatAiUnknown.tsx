import React from 'react'
import ChatAiText from "./ChatAiText";

const text = 'お問合せ内容が理解できませんでした。お手数ですが再度お問合せ内容を入力してください。';

const ChatAiUnknown: React.FC = () => {
  return <ChatAiText text={text}/>;
};

export default ChatAiUnknown;
