import React from 'react'
import styled from 'styled-components';

interface IProps {
  className?: string;
  leftComponent?: React.ReactNode;
  centerComponent?: React.ReactNode;
  rightComponent?: React.ReactNode;
}

const ChatBaseLayout: React.FC<IProps> = (props) => {
  const {
    className,
    leftComponent,
    centerComponent,
    rightComponent,
  } = props;

  return (
    <Wrap className={className}>
      <SideLeft>{leftComponent}</SideLeft>
      <Center>{centerComponent}</Center>
      <SideRight>{rightComponent}</SideRight>
    </Wrap>
  );
};

const Wrap = styled.div`
  display: flex;
  flex-direction: row;
`;

const SideLeft = styled.div`
  flex: 0 0 55px;
`;

const Center = styled.div`
  flex: 1;
`;

const SideRight = styled.div`
  flex: 0 0 20px;
`;

export const BalloonBase = styled.div`
  margin-top: 30px;
  padding: 15px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  border: solid 1px #e6e6e6;
  border-radius: 15px;
`;

export default ChatBaseLayout;
