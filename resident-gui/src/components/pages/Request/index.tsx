import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { Header, ContentHeader } from "../../atoms";
import { useAsync } from "../../../hooks/useAsync";
import { dataInitialize, setInitialData } from "../../../reducers/request";

import Chat from "./Chat";
import UserInput from "./UserInput";

const Request: React.FC = () => {
  const dispatch = useDispatch();
  const [asyncAction] = useAsync();

  useEffect(() => {
    (async () => {
      asyncAction(
        dataInitialize(),
        (data) => dispatch(setInitialData(data))
      )
    })();
  }, [dispatch]);

  return (
    <div className="App">
      <Header/>
      <ContentHeader
        title={'お問合せ'}
        backUrl={'/home'}
      />
      <Chat/>
      <UserInput/>
    </div>
  );
};

export default Request;
