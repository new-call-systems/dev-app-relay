import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { push } from 'connected-react-router';
import { RootState } from "../../../store";
import { useAsync } from "../../../hooks/useAsync";
import { parseQuery } from "../../../utils/query-string";
import { getLiffId, login, setLiffId } from "../../../reducers/line";
import { setUser } from "../../../reducers/resident";
import { setToken } from "../../../store/storage";
import { setLogin } from "../../../reducers/auth";
import liff from '@line/liff';

const Liff: React.FC = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [asyncAction] = useAsync();
  const [isLineBrowser, setIsLineBrowser] = useState(false);
  const [lineAccessToken, setLineAccessToken] = useState('');
  const lineClientId = parseQuery(location.search, 'line_client_id', null);
  const liffId = useSelector((state: RootState) => state.line.liffId);

  // １．LINEブラウザかチェック-----------------------------------------
  useEffect(() => {
    if (!isLineBrowser) {
      // TODO: 常にfalseとなるので要調査
      //setIsLineBrowser(liff.isInClient());
      setIsLineBrowser(true);
    }
  }, [isLineBrowser]);

  // ２．中継サーバからLIFF IDを取得------------------------------------
  useEffect(() => {
    if (!isLineBrowser || !lineClientId) {
      return;
    }
    (async () => {
      asyncAction(
        getLiffId(lineClientId),
        (data) => dispatch(setLiffId(data))
      )
    })();
  }, [lineClientId, isLineBrowser]);

  // ３．LIFF IDで初期化-----------------------------------------------
  useEffect(() => {
    if (!liffId) {
      return;
    }
    (async () => {
      await liff.init({ liffId: liffId });
      if (!liff.isLoggedIn()) {
        liff.login();
      }
      liff.ready.then(() => {
        setLineAccessToken(liff.getAccessToken());
      })
    })();
  }, [liffId]);

  // ４．coco-enログイン-------------------------------------------------
  useEffect(() => {
    if (!lineAccessToken) {
      return;
    }
    (async () => {
      asyncAction(
        login(lineAccessToken, lineClientId),
        successLogin(push, dispatch)
      )
    })();
  }, [lineAccessToken]);

  const successLogin = (push, dispatch) => {
    return (data) => {
      // 入居者情報
      dispatch(setUser(data.resident));

      // ログイン情報
      dispatch(setLogin(data));
      setToken({
        access_token: data.access_token,
        refresh_token: data.refresh_token,
      });

      // ホーム画面に遷移
      dispatch(push('/home'));
    }
  };

  if (!isLineBrowser) {
    return (
      <div>
        <p>LIFFアプリではありません</p>
      </div>
    )
  }

  if (!lineAccessToken) {
    return (
      <div>
        <p>初期化中</p>
      </div>
    )
  }

  return (
    <div>
      <p>LIFF</p>
    </div>
  )
};

export default Liff;
