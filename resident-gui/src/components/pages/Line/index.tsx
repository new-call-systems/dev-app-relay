import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { parseQuery} from "../../../utils/query-string";
import { RootState } from "../../../store";
import { useAsync } from "../../../hooks/useAsync";
import { auth, verify, deactivate, setAuthUrl } from "../../../reducers/line";
import { setUser } from "../../../reducers/resident";
import { LineLoginButton } from "../../atoms";

const Line: React.FC = () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const [asyncAction] = useAsync();
  const code = parseQuery(location.search, 'code', null);
  const state = parseQuery(location.search, 'state', null);
  const authUrl = useSelector((state: RootState) => state.line.authUrl);
  const lineConnected = useSelector((state: RootState) => state.resident.lineConnected);

  // 初期ロード時にLINE連携用URLを取得
  useEffect(() => {
    if (code) {
      return;
    }
    (async () => {
      asyncAction(
        auth(),
        (data) => dispatch(setAuthUrl(data))
      )
    })();
  }, [dispatch, code]);

  // LINEログイン後のコールバック用
  useEffect(() => {
    if (!code) {
      return;
    }
    (async () => {
      asyncAction(
        verify(code, state),
        (data) => dispatch(setUser(data))
      )
    })();
  }, [code, state]);

  // LINE連携の解除
  const clickDeactivate = (() => {
    (async () => {
      asyncAction(
        deactivate(),
        (data) => dispatch(setUser(data))
      )
    })();
  });

  if (lineConnected) {
    return (
      <div>
        <p>LINE連携済み</p>
        <p>line_connected: true</p>
        <button onClick={clickDeactivate}>LINE連携解除</button>
      </div>
    )
  }

  return (
    <div>
      <p>LINE連携</p>
      <LineLoginButton url={authUrl} />
    </div>
  )
};

export default Line;
