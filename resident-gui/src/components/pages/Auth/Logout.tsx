import React from 'react'
import styled from 'styled-components';
import { Header } from "../../atoms";

const Logout: React.FC = () => {
  return (
    <div className="App">
      <Header/>
      <Main>
        <Title>ログアウト</Title>
        <Description>
          画面上部の✕ボタンから再ログインしてください。
        </Description>
      </Main>
    </div>
  );
};

const Main = styled.main`
  flex: 1;
  padding-top: 25px;
  padding-left: 26px;
  padding-right: 26px;
`;

const Title = styled.p`
  font-size: 28px;
  font-weight: bold;
  color: #464f69;
  text-align: left;
`;

const Description = styled.p`
  margin-top: 8px;
  font-size: 18px;
  color: #464f69;
  word-wrap: break-word;
`;

export default Logout;
