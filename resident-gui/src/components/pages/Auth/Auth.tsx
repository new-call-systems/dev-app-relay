import React, {useEffect} from 'react'
import { useDispatch } from 'react-redux';
import { getLogin } from '../../../reducers/auth';

const Auth = (code: string) => {
  const dispatch = useDispatch();
  useEffect(() => {
    if (code) {
      onAuthorization(code);
    }
  });

  const onAuthorization = async (code: string) => {
    await new Promise((resolve) => {
      dispatch(getLogin(code));
      resolve();
    });
  };

  return (
    <div>
      {code ? <p>第２認証中...</p> : <p>認証失敗</p>}
    </div>
  )
};

export default Auth;
