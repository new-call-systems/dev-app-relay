import React, {useEffect, useMemo, useState, useCallback} from 'react'
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from "react-router-dom";
import { getLogin, useLoginStatus, reset } from '../../../reducers/auth';

interface IProps extends RouteComponentProps<{authCode:string}> {

}

const Verify: React.FC<IProps> = props => {
  const code = props.match.params.authCode;
  const loginStatus = useLoginStatus();
  const [ atFirst, setAtFirst ] = useState(true);
  const { loading, onError } = loginStatus;
  const dispatch = useDispatch();

  const shouldVerify = useMemo( () => {
    return !loading && !onError && atFirst
  }, [loading,onError,atFirst] );

  const onAuthorization = useCallback( async ( code: string ) => {
    dispatch(getLogin(code));
  }, [dispatch] ) ;

  useEffect(() => {
    if (shouldVerify && code) {
      setAtFirst( false );
      onAuthorization(code);
    }
  }, [shouldVerify, onAuthorization,code, setAtFirst] );

  useEffect( () => {
    dispatch(reset({}))
  }, [ dispatch ] );

  return (
    <div>
      { !code && <p>不正なアクセスです。</p> }
      { loading && <p>第２認証中...</p> }
      { onError && <p>認証失敗</p> }
    </div>
  )
};

export default Verify;
