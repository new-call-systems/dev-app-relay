import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';
import { parseQuery } from "../../../utils/query-string";
import styled from "styled-components";
import { login } from '../../../reducers/enclub';
import { RootState } from "../../../store";
import { Input } from "../../templates/form"

export interface IForm {
  residentCode: string;
  propertyCode: string;
  roomCode: string;
}

const EnClub = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { register, handleSubmit } = useForm();
  const loading: boolean = useSelector((state: RootState) => state.enclub.loading);
  const accessKey = parseQuery(location.search, 'accesskey', null);
  const secretKey = parseQuery(location.search, 'secretkey', null);

  const onSubmit = handleSubmit((data: IForm) => {
    dispatch(login(data, accessKey, secretKey))
  });

  return (
    <Container onSubmit={onSubmit}>
      <h3>ログイン（1段階目認証）</h3>
      <p>※えんくらぶアプリに実装するダミーログイン</p>
      <br/>
      <StyledInput
        type="text"
        placeholder="入居者コード"
        name="residentCode"
        ref={register({ required: true })}
      />
      <StyledInput
        type="text"
        placeholder="物件コード"
        name="propertyCode"
        ref={register({ required: true })}
      />
      <StyledInput
        type="text"
        placeholder="部屋コード"
        name="roomCode"
        ref={register({ required: true })}
      />
      <br/>
      <br/>
      <br/>
      <input type="submit" value="ログイン"/>
      {loading && <p>処理中...</p>}
    </Container>
  )
};

export default EnClub;


/**
 * styled component
 */

const StyledInput = styled(Input)`
  margin: 1em 0;
`;

const Container = styled.form`
  display: flex;
  flex-direction: column;
  width: inherit;
  padding: 1em;
`;
