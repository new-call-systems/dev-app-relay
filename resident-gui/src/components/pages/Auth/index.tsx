import React from 'react'
import { useLocation } from 'react-router-dom';
import { parseQuery } from '../../../utils/query-string';
import EnClub from './EnClub'
import Auth from './Auth'

const AuthRoot: React.FC = () => {
  const location = useLocation();
  const code = parseQuery(location.search, 'code', null);
  return (
    <>
      {code ? Auth(code) : EnClub()}
    </>
  )
};

export default AuthRoot;
