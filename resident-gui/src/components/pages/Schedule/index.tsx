import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from "react-i18next";
import './Schedule.css';
import { RootState } from "../../../store";
import { formatTZ } from '../../../utils/date';
import { useAsync } from "../../../hooks/useAsync";
import { Spinner, ContentHeader, ButtonScheduleDetail } from "../../atoms";
import { useElementBottom } from "../../../hooks/useElementBottom";
import { refreshBadge } from "../../../reducers/notification";
import {
  getRequest,
  appendRequestList,
  setRequestList,
  moveDetail,
  requestStatusText,
  requestStatusIcon,
} from "../../../reducers/schedule";

const Schedule: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [asyncAction, loading] = useAsync();
  const [asyncActionNextPage, loadingNextPage] = useAsync();
  const isBottom = useElementBottom('scrollTarget');
  const list = useSelector((state: RootState) => state.schedule.list);
  const currentPage = useSelector((state: RootState) => state.schedule.listCurrentPage);
  const lastPage = useSelector((state: RootState) => state.schedule.listLastPage);

  useEffect(() => {
    onRefresh();
  }, [dispatch]);

  useEffect(() => {
    if (isBottom) {
      onEndReached();
    }
  }, [isBottom]);

  const onRefresh = async () => {
    asyncAction(
      getRequest(),
      successGetRequest(dispatch)
    );
  };

  const successGetRequest = (dispatch) => {
    return (data) => {
      dispatch(setRequestList(data));
      dispatch(refreshBadge());
    }
  };

  const onEndReached = () => {
    if (loading || loadingNextPage) {
      return false;
    }
    const nextPage = currentPage + 1;
    if (lastPage < nextPage) {
      return false;
    }
    asyncActionNextPage(
      getRequest(nextPage),
      (data) => dispatch(appendRequestList(data))
    );
  };

  const onClick = (id: number) => {
    dispatch(moveDetail(id));
  };

  return (
    <div id="scrollTarget" className="schedule-contents">
      <ContentHeader
        title={'問合せ履歴'}
      />
      <ul className="schedule-ul">
        {list.map((v) => {
          const iconFile = requestStatusIcon(v);
          return (
            <li key={v.id}>
              <div className="schedule-li-upper">
                <p className="schedule-li-date">
                  受付日：{formatTZ(v.createdAt)}
                </p>
                <p className="schedule-li-title">
                  {v.context}
                </p>
              </div>
              <div className="schedule-li-border"/>
              <div className="schedule-li-lower">
                <img
                  className="schedule-li-icon"
                  src={iconFile}
                  alt="status"
                />
                <p className="schedule-li-status">
                  {requestStatusText(t, v)}
                </p>
                <div className="schedule-li-detail-button">
                  <ButtonScheduleDetail
                    onClick={() => onClick(v.id)}
                  />
                </div>
              </div>
            </li>
          );
        })}
        {(loading || loadingNextPage) &&
          <li className="spinner"><Spinner/></li>
        }
      </ul>
    </div>
  )
};

export default Schedule;
