import React from 'react'
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import TextareaAutosize from 'react-textarea-autosize';
import {
  closeCancel,
  cancel,
} from "../../../reducers/schedule";

const ModalCancel: React.FC = () => {
  const dispatch = useDispatch();
  const { handleSubmit, control } = useForm();
  const { id } = useParams();

  const clickClose = (() => {
    dispatch(closeCancel())
  });

  const clickCancel = ((data) => {
    let reason = '';
    if (data.reason === 1) {
      reason = '問題が解決したから';
    }
    if (data.reason === 2) {
      reason = '都合が悪くなり日時の変更を希望するから';
    }
    if (data.reason === 3) {
      reason = 'その他の理由';
    }
    reason = reason + ' ' + data.reasonText;
    dispatch(cancel(id, reason));
  });

  return (
    <>
      <div
        style={{
          position: 'fixed',
          top: '0px',
          left: '0px',
          right: '0px',
          bottom: '0px',
          backgroundColor: 'rgba(0, 0, 0, 0.4)',
          zIndex: 999,
          height: '100%',
          width: '100%',
          overflowY: 'auto',
        }}
      >
        <div
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            border: 'none',
            overflow: 'visible',
            borderRadius: '10px',
            outline: 'none',
            padding: '20px',
            transform: 'translate(-50%, -50%)',
          }}
        >
          <div
            style={{
              margin: 'auto',
              width: '300px',
              height: '472px',
              backgroundColor: '#fff',
              borderRadius: '4.8px',
              fontSize: '14px',
            }}
          >

            {/* タイトル */}
            <div
              style={{
                width: '100%',
                height: '65px',
                textAlign: 'center',
              }}
            >
              <div
                style={{
                  margin: '20px 0 20px 0',
                  display: 'inline-block',
                  wordWrap: 'break-word',
                }}
              >
                <p
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    fontStyle: "normal",
                    color: '#8692b5',
                  }}
                >
                  予約をキャンセルしますか？
                </p>
              </div>
            </div>

            {/* 区切り線 */}
            <div
              style={{
                width: '298px',
                height: 0,
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#dee2e6"
              }}
            />

            {/* フォーム */}
            <div
              style={{
                padding: '20px 18px',
                wordWrap: 'break-word',
              }}
            >
              <p
                style={{
                  height: 25,
                  fontSize: 14,
                  fontWeight: "bold",
                  fontStyle: "normal",
                  textAlign: "left",
                  color: "#212529"
                }}
              >
                キャンセル理由を選択してください
              </p>
              <form>
                <div>
                  <Controller
                    control={control}
                    name={'reason'}
                    rules={{required: true}}
                    defaultValue={1}
                    render={({onChange, value}) => (
                      <>
                        <label style={{display: 'flex'}}>
                          <input
                            type="checkbox"
                            style={{marginTop: '4px'}}
                            onChange={() => onChange(1)}
                            checked={value === 1}
                          />
                          <span style={{marginLeft: '4px'}}>問題が解決したから</span>
                        </label>
                        <label style={{display: 'flex'}}>
                          <input
                            type="checkbox"
                            style={{marginTop: '4px'}}
                            onChange={() => onChange(2)}
                            checked={value === 2}
                          />
                          <span style={{marginLeft: '4px'}}>都合が悪くなり日時の変更を希望するから</span>
                        </label>
                        <label style={{display: 'flex'}}>
                          <input
                            type="checkbox"
                            style={{marginTop: '4px'}}
                            onChange={() => onChange(3)}
                            checked={value === 3}
                          />
                          <span style={{marginLeft: '4px'}}>その他の理由</span>
                        </label>
                      </>
                    )}
                  />
                </div>
                <div
                  style={{
                    marginTop: '15px',
                  }}
                >
                  <Controller
                    control={control}
                    name={'reasonText'}
                    // rules={{required: true}}
                    defaultValue={''}
                    render={({onChange, value}) => (
                      <TextareaAutosize
                        style={{
                          width: '238px',
                          borderStyle: 'solid',
                          borderRadius: 4,
                          backgroundColor: "#ffffff",
                          borderWidth: 1,
                          borderColor: "#dee2e6"
                        }}
                        minRows={10}
                        value={value}
                        onChange={onChange}
                      />
                    )}
                  />
                </div>
              </form>
            </div>

            {/* 区切り線 */}
            <div
              style={{
                width: '298px',
                height: 0,
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#dee2e6"
              }}
            />

            {/* ボタン */}
            <div
              style={{
                display: 'flex',
                height: '48px',
                marginTop: '10px',
              }}
            >
              <div>
                <span
                  style={{
                    marginLeft: '35px',
                    width: 48,
                    height: 24,
                    fontSize: 16,
                    fontWeight: "bold",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "left",
                    color: "#464f69",
                  }}
                  onClick={clickClose}
                >
                  閉じる
                </span>
              </div>
              <div>
                <span
                  style={{
                    marginLeft: '56px',
                    width: 128,
                    height: 24,
                    fontSize: 16,
                    fontWeight: "bold",
                    fontStyle: "normal",
                    letterSpacing: 0,
                    textAlign: "left",
                    color: "#d94e24"
                  }}
                  onClick={handleSubmit(clickCancel)}
                >
                  予約をキャンセル
                </span>
              </div>
            </div>

          </div>
        </div>
      </div>
    </>
  )
};

export default ModalCancel;
