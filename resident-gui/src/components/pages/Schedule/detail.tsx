import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useTranslation } from "react-i18next";
import { useAsync } from "../../../hooks/useAsync";
import { RootState } from "../../../store";
import { iconDate, iconTime } from "../../assets/icon";
import { formatTZ } from "../../../utils/date";
import { RequestStatus } from "../../../api/schema/request";
import { ModalImage } from "../../atoms";
import ModalCancel from "./ModalCancel";
import './Schedule.css';
import {
  ContentHeader,
  ButtonApprove,
  ButtonCancel,
  TelLink,
  StringReplace
} from "../../atoms";
import {
  getDetail,
  setRequestDetail,
  showCancel,
  approve,
  IRequestDetailState,
  requestStatusText,
  requestStatusIcon,
  getReservedDateText,
} from "../../../reducers/schedule";

const ScheduleDetail: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [asyncAction, loading] = useAsync();
  const { id } = useParams();
  const item: IRequestDetailState = useSelector((state: RootState) => state.schedule.selectedDetail);
  const timeSection = useSelector((state: RootState) => state.schedule.timeSection);
  const isValidStatus: string[] = [RequestStatus.open, RequestStatus.bid, RequestStatus.scheduling];
  const showCancelDialog: boolean = useSelector((state: RootState) => state.schedule.showCancelDialog);
  const iconFile = requestStatusIcon(item);

  useEffect(() => {
    (async () => {
      asyncAction(
        getDetail(id),
        (data) => dispatch(setRequestDetail(data))
      )
    })();
  }, [id]);

  const onCancel = (() => {
    dispatch(showCancel());
  });

  const onApprove = (() => {
    dispatch(approve(id));
  });

  const convTime = (id: number) => {
    const row = timeSection.find((v) => v.time_section_id === id);
    return row.label;
  };

  if (loading) {
    return null;
  }

  if (!item) {
    return null;
  }

  if (showCancelDialog) {
    return <ModalCancel/>;
  }

  return (
    <div className="schedule-detail-contents">
      <ContentHeader
        title={'問合せ履歴詳細'}
        backUrl={'/schedule'}
      />
      <div className="schedule-detail-contents-area">

        {/* 問合せ情報 */}
        <div className="schedule-detail-contents-area-1">
          <img
            className="schedule-detail-contents-area-1-icon"
            src={iconFile}
            alt="status"
          />
          <p className="schedule-detail-contents-area-1-status">
            {requestStatusText(t, item)}
          </p>
        </div>

        {/* 受付日 */}
        <div className="schedule-detail-contents-area-1">
          <p className="schedule-detail-contents-area-1-date">
            受付日：{formatTZ(item.createdAt)}
          </p>
        </div>

        {/* 問合せ内容 */}
        <div className="schedule-detail-contents-area-2">
          <p className="schedule-detail-contents-area-2-context">
            {item.context}
          </p>
        </div>

        <div className="schedule-detail-contents-area-border"/>

        {/* 自動回答 */}
        {(() => {
          if (item.response) {
            return (
              <>
                <div className="schedule-detail-contents-area-response">
                  <p className="schedule-detail-contents-area-response-title">自動回答の内容</p>
                  <p className="schedule-detail-contents-area-response-text">
                    <StringReplace>{item.response}</StringReplace>
                  </p>
                </div>
                <div className="schedule-detail-contents-area-border"/>
              </>
            );
          }
        })()}

        {/* 希望日 */}
        {(() => {
          if (item.emergencyFlg) {
            return (
              <>
                <div className="schedule-detail-contents-area-3">
                  <p className="schedule-detail-contents-area-3-title">希望日</p>
                  <p className="schedule-detail-contents-area-3-emergency">緊急対応</p>
                </div>
                <div className="schedule-detail-contents-area-border"/>
              </>
            );
          } else if (0 < item.preferableInfo?.length) {
            return (
              <>
                <div className="schedule-detail-contents-area-3">
                  <p className="schedule-detail-contents-area-3-title">希望日</p>
                  <ul className="schedule-detail-contents-area-3-list">
                    {item.preferableInfo.map((v, i) => {
                      return (
                        <li key={i}>
                          <div className="schedule-detail-contents-area-3-list-title">
                            <span/>
                            <p>第{i+1}候補</p>
                          </div>
                          <div className="schedule-detail-contents-area-3-list-date">
                            <img
                              src={iconDate}
                              alt="date"
                            />
                            <p>{formatTZ(v.date)}</p>
                          </div>
                          <div className="schedule-detail-contents-area-3-list-date">
                            <img
                              src={iconTime}
                              alt="time"
                            />
                            <p>{convTime(v.time_section_id)}</p>
                          </div>
                        </li>
                      )
                    })}
                  </ul>
                </div>
                <div className="schedule-detail-contents-area-border"/>
              </>
            );
          }
        })()}

        {/* 訪問日時 */}
        {(() => {
          const vendorReserve = getReservedDateText(timeSection, item.preferableInfo);
          if (vendorReserve) {
            return (
              <>
                <div className="schedule-detail-contents-area-reserve">
                  <p className="schedule-detail-contents-area-reserve-title">訪問日時</p>
                  <p className="schedule-detail-contents-area-reserve-text">
                    {vendorReserve}
                  </p>
                </div>
                <div className="schedule-detail-contents-area-border"/>
              </>
            );
          }
        })()}

        {/* 画像 or 動画 */}
        {(() => {
          if ((item.images || []).length > 0) {
            return (
              <ul className="schedule-detail-contents-area-image">
                {item.images.map((v, i) => {
                  return (
                    <li key={i} className="schedule-detail-contents-area-image-block">
                      {v.type === 'image' && <ModalImage src={v.url}/>}
                      {v.type === 'video' && <video controls><source src={v.url}/></video>}
                    </li>
                  )
                })}
              </ul>
            );
          }
        })()}

        {/* 問合せが有効なステータスであればメッセージを出す */}
        {(() => {
          if (isValidStatus.includes(item.status)) {
            return (
              <div className="schedule-detail-contents-area-4">
                <p>
                  ご予約内容を確認した上で、業者から日程確定の
                  ご連絡をメッセージまたはお電話いたします。
                  確定した日時に業者が伺います。
                </p>
                {(item.status === RequestStatus.bid) &&
                  <p style={{marginTop: '15px', fontWeight: 'bold'}}>
                    業者から入札がありました。許可する場合は[OK]ボタンを押してください。
                  </p>
                }
              </div>
            );
          }
        })()}

        {/* 入札承認 */}
        {(() => {
          if (item.status === RequestStatus.bid) {
            return (
              <div className="schedule-detail-contents-area-bid">
                <ButtonApprove onClick={onApprove}/>
              </div>
            );
          }
        })()}

        {/* キャンセル */}
        {(() => {
          if (isValidStatus.includes(item.status)) {
            return (
              <div className="schedule-detail-contents-area-cancel">
                <ButtonCancel onClick={onCancel}/>
              </div>
            );
          }
        })()}

        {/* サポートセンター */}
        {(() => {
          if (item.status === RequestStatus.transferred) {
            return (
              <div className="schedule-detail-contents-area-4">
                <p>申し訳ありません。</p>
                <p>以下のお問合せ先まで再度お問合せください。</p>
                <TelLink tel={item.supportTel}>{item.supportTel}</TelLink>
              </div>
            );
          }
        })()}

        {/* キャンセル理由 */}
        {(() => {
          if (item.cancelReason && item.cancelReason !== '') {
            return (
              <div className="schedule-detail-contents-area-4">
                <p className="schedule-detail-contents-area-4-title">
                  キャンセル理由
                </p>
                <p>{item.cancelReason}</p>
              </div>
            );
          }
        })()}

      </div>
    </div>
  )
};

export default ScheduleDetail;
