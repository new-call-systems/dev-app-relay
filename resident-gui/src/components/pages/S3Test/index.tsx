
import React, { useState, useEffect, useCallback, useMemo } from "react";
import { S3 } from "aws-sdk";

import useS3Storage from "../../../hooks/useS3Storage";

export interface Props {};

const Component: React.FC<Props> = props => {
    const { objects, generateObjectUrl, updateFile } = useS3Storage({});
    return <div>
        {
            objects.map( obj => {
                return <LoadImage key={obj.Key} object={obj} generateImageUrl={generateObjectUrl} />
            })
        }
        <FileUpload onUpload={updateFile} />
    </div>
};

interface LoadImageProps {
    generateImageUrl( obj: S3.Object ): Promise<string>;
    object: S3.Object
};

const LoadImage: React.FC<LoadImageProps> = props => {
    const { generateImageUrl, object } = props;
    const [ onLoaded, setOnLoaded ] = useState(false);
    const [ imageUrl, setImageUrl ] = useState("");

    const init = useCallback( async () => {
        const res = await generateImageUrl(object)
        setImageUrl(res);
        setOnLoaded(true)
    }, [ generateImageUrl, object ] )

    useEffect( () => {
        if(!onLoaded) init()
    }, [ init, onLoaded ] )

    if( !onLoaded || !imageUrl ) return null;

    return <img src={imageUrl} width="100px" height="100px" />
};

interface FileUploadProps {
    onUpload(file: File): Promise<void>;
};

const FileUpload: React.FC<FileUploadProps> = props => {
    const { onUpload } = props;
    const [ file, setFile ] = useState<File>();
    const [ onProgress, setOnProgress ] = useState(false);

    const handleSetFile = useCallback( ( files: File[] ) => {
        if( files.length > 0 ) setFile(files[0])
    }, [] )

    const commonButtonStyle = useMemo( () => {
        return {
            width: '100px',
            height: '30px',
            padding: '5px'
        }
    }, [] )

    const { WrapperdComponent } = useInputFile({
        onChange: handleSetFile,
        Component: (p) => <button {...p} style={commonButtonStyle}>画像選択</button>,
        accept: "image/*"
    })

    const handleUpload = useCallback( async() => {
        if( !file ) return;
        setOnProgress(true);
        await onUpload(file);
        setOnProgress(false);
    }, [ onUpload, file ] )

    return <div>
        <WrapperdComponent />
        <button
          onClick={() => handleUpload()}
          style={commonButtonStyle}
          disabled={onProgress}
        >
          S3へアップロード
        </button>
        Selectd: { file ? file.name : "" }
    </div>
}



interface Params {
    onChange( files: File[] ): void | Promise<void>;
    Component: React.FC<{ onClick: () => void }>;
    isDirectory?: boolean;
    accept?: string;
};

function useInputFile(params: Params){

    const { onChange, Component, isDirectory=false, accept } = params

    let nodeRef:any = {};
    function _addDirectory(node:any) {
      if (node) {
        node.directory = isDirectory;
        node.webkitdirectory = isDirectory;
        node.accept = accept
        nodeRef = node;
      }
    }

    function uploadFolderClick() {
      nodeRef.click();
    }

    function onSelectFileOrFolder( event: any){
      onChange(event.target.files);
      if( nodeRef ) {
        nodeRef.value = "";
      }
    };

    const WrapperdComponent: React.FC<{}> = props => {
        return <React.Fragment>
            <input id="folder-upload" accept={accept} style={{ display: "none" }} type="file" ref={node => _addDirectory(node)} onChange={ onSelectFileOrFolder } />
            <Component onClick={ () => uploadFolderClick() } />
        </React.Fragment>
    };

    return { WrapperdComponent }
};


export default Component;
