import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router'
import { clear } from "../../../reducers/request";
import { ButtonStart } from "../../atoms";
import { welcomeImg } from "../../assets/icon";
import { getBadge } from "../../../reducers/notification";
import './Home.css';

const Home: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBadge());
  }, [dispatch]);

  const move = () => {
    dispatch(clear());
    dispatch(push('request'))
  };

  return (
    <div className="home-contents">
      <div className="home-head">
        <p className="home-title">
          お困りですか？
        </p>
        <p className="home-description">
          お住まいに対する質問や疑問、お困りごとなどをAIが自動で回答します。<br/>
          質問する際は、一つの質問につき、一つのトークで送ってください。<br/>
        </p>
      </div>
      <div className="home-welcome">
        <img
          src={welcomeImg}
          alt=""
        />
      </div>
      <div className="home-button">
        <ButtonStart onClick={move}/>
      </div>
    </div>
  );
};

export default Home;
