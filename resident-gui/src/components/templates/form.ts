import styled from "styled-components";

export const Input = styled.input`
    width: 100%;
    height: 1.5rem;
    padding: 0.5em 0.25em 0.25em 0;
    line-height: 1;
    font-size: 0.9em;
`;

export const Button = styled.button`

`;
