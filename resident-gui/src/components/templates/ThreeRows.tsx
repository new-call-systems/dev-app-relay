import React, { useCallback, useMemo } from "react";
import styled from 'styled-components';
import { RouteComponentProps } from "react-router-dom";
import { TabPanes } from "../../localization/common.i18n";

import Header from "../atoms/Header";
import Footer from "../atoms/Footer";

import { HEADER_HEIGHT, FOOTER_HEIGHT } from "../../constants/ThreeRowsDesign";

export interface IProps extends RouteComponentProps<{}> {
  title: string;
  withBack?: boolean
}

const Component: React.FC<IProps> = props => {
  const { history } = props;
  const currentPathName = history.location.pathname;

  const getCurrentTab = useCallback((url: string) => {
    for (const tab of Object.values(TabPanes)) {
      if (tab && url.startsWith(`/${tab}`)) return tab;
    }
    return null
  }, []);

  const currentPath = useMemo(() => {
    return getCurrentTab(currentPathName)
  }, [getCurrentTab, currentPathName]);

  return (
    <div className="App">
      <RealHeader/>
      <Main>
        {props.children}
      </Main>
      <RealFooter active={currentPath} />
    </div>
  )
};

export default Component;

/**
 * Styles components
 */
const RealHeader = styled(Header)`
  height: ${HEADER_HEIGHT};
`;
const RealFooter = styled(Footer)`
  height: ${FOOTER_HEIGHT};
`;
const Main = styled.div`
  height: calc(100% - ${HEADER_HEIGHT} - ${FOOTER_HEIGHT});
`;
