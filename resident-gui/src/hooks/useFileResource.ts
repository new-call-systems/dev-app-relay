import { useEffect, useState } from 'react';

export type Resource = {
  fileType: string;
  previewUrl: string | ArrayBuffer | null;
}

export const useFileResource = (elementId?: string) => {
  const [resources, setResources] = useState<Resource[]>([]);

  useEffect(() => {
    if (elementId) {
      const input = document.getElementById(elementId) as any;
      const files = input?._value || [];
      addResources(files);
    }
  }, [elementId]);

  const addResources = (files) => {
    (async () => {
      const selectFiles: Resource[] = [];
      await Promise.all(
        Object.keys(files).map(async item => {
          const file: File = files[Number(item)];
          const reader: FileReader = await readerOnloadend(file);
          selectFiles.push({
            fileType: file.type,
            previewUrl: reader.result,
          });
        })
      );
      setResources([...resources, ...selectFiles]);
    })();
  };

  const removeResources = (index: number) => {
    resources.splice(index, 1);
    setResources([...resources]);
  };

  // FileオブジェクトからDataURLを取得
  const readerOnloadend = async (file: File): Promise<FileReader> => {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader);
      reader.readAsDataURL(file);
    });
  };

  return {resources, addResources, removeResources};
};
