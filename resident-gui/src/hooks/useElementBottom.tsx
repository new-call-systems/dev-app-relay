import { useEffect, useState } from 'react';

export const useElementBottom = (elementId: string) => {
  const [isBottom, setBottom] = useState(false);

  useEffect(() => {
    const element = document.getElementById(elementId);
    function handleScroll(event) {
      const {scrollHeight, scrollTop, clientHeight} = event.target;
      if (scrollHeight - scrollTop - clientHeight <= 0) {
        setBottom(true);
      } else {
        setBottom(false);
      }
    }
    element.addEventListener("scroll", handleScroll);
    return () => {
      element.removeEventListener("scroll", handleScroll);
    };
  }, [elementId]);

  return isBottom;
};
