import { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setLogout } from '../reducers/auth';
import { setAlertCode } from '../reducers/message';
import { FAILED_REFRESH_TOKEN } from "../constants/ErrorCode";
import { AppDispatch } from '../store';

export const useAsync = () => {
  const dispatch: AppDispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const asyncAction = useCallback(
    async (action, callback) => {

    setLoading(true);

    const errorAction = (error) => {
      console.log('★Debug Exception useAsync', error);

      setLoading(false);
      setError(error);

      // AlertMessage
      dispatch(setAlertCode(error.code));

      // Logout
      if (error.code === FAILED_REFRESH_TOKEN) {
        dispatch(setLogout());
      }
    };

    try {
      const result = await action();

      setLoading(false);
      if (result.err) {
        errorAction(result.err);
      } else {
        callback(result.data);
      }

    } catch (error) {
      errorAction(error.err);
    }
  }, [loading, error]);

  return [asyncAction, loading, error];
};
