
import { useEffect, useCallback, useMemo, useState } from "react";

import AWS, { S3 } from "aws-sdk";

export interface IOptions {

};

interface S3Paramters {
    Bucket: string;
    Prefix: string;
    //Delimiter: string;
}

function useS3Storage( opt: IOptions ){

    const [ s3Params, setS3Params ] = useState<S3Paramters>( { Bucket: '', Prefix: ''} )
    const [ objects, setObjects ] = useState<S3.ObjectList>([]);
    const cashedImageObjectUrl = new Map<string,string>();

    const s3 = new S3({
        apiVersion: '2006-03-01',
    });
    const init = useCallback( async () => {
        /** TODO get from api */
        const awsConfigRes = await getDymmyData();

        const awsConfig = {
            region: awsConfigRes.region,
            credentials: new AWS.CognitoIdentityCredentials({ 
                IdentityPoolId: awsConfigRes.identity_pool_id
            })
        };
        AWS.config.update(awsConfig)
        setS3Params({
            Bucket: awsConfigRes.bucket_name,
            Prefix: awsConfigRes.prefix,
        })
    }, [ ] )

    const getlistObjectsFromPrefix = useCallback( async () => {
        if( Object.values(s3Params).some( (val: string) => val.length === 0) ) return

        const filterLikeFolder = ( obj: S3.Object ) => {
            return (obj.Size || 0 ) > 0
        };
        const list = await s3.listObjectsV2(s3Params).promise();
        setObjects( (list.Contents || []).filter(filterLikeFolder) );

    }, [ s3Params ] );

    const generateObjectUrl = useCallback( async ( obj: S3.Object ) => {
        if( !s3Params.Bucket || !obj.Key ) return "";

        const cached = cashedImageObjectUrl.get( obj.Key );
        if( cached ) {
            console.log( "Return from cache" )
            return cached;
        };
        const res = await s3.getObject({Key: obj.Key, Bucket: s3Params.Bucket}).promise();
        const blob = new Blob([res.Body as Buffer], { type: res.ContentType })
        const objectUrl = window.URL.createObjectURL(blob);
        cashedImageObjectUrl.set(obj.Key,objectUrl);
        return objectUrl
    }, [ s3Params, cashedImageObjectUrl ] );

    const updateFile = useCallback( async (file: File) => {
        if( !s3Params.Bucket ) return;
        // TODO use unique name
        const key = s3Params.Prefix + file.name;
        const res = await s3.upload({ Bucket: s3Params.Bucket, Key: key, Body: file }).promise();

        console.log( "Upload res!!", res );
        await getlistObjectsFromPrefix();
    }, [ s3Params, getlistObjectsFromPrefix ] )

    useEffect( () => {
        init();
    }, [ init ] )

    useEffect( () => {
        getlistObjectsFromPrefix()
    }, [ getlistObjectsFromPrefix ] )

    return { objects, generateObjectUrl, updateFile }
};

export default useS3Storage;

async function getDymmyData() {
    return {
        bucket_name: 'image-mytest',
        region: 'ap-northeast-1',
        identity_pool_id: 'ap-northeast-1:4fb7d1d4-a160-4df4-95a2-1731855e8b50',
        prefix: 'hoge/hoge/'
    }
};