import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router';
import RequestRepository from "../api/repository/request";
import { DateFormat, formatDateString } from "../utils/date";
import { RequestStatus, requestStatus } from "../localization/label.i18n";
import { btnResolved, btnUnresolved, btnSupport, btnInquiring } from "../components/assets/icon"
import { IApiResult } from "../types/apiResult";
import { setAlertMessage } from "./message";
import {
  IRequestListRequest,
  ITimeResponse,
  preferableTime,
  MediaObject,
  IManagementCompanyResponse,
} from "../api/schema/request";

// ---------------------------------------------------------------------------
// Type
// ---------------------------------------------------------------------------
export interface IScheduleState {
  timeSection: ITimeResponse[];
  list: IRequestDetailState[];
  listCurrentPage: number;
  listLastPage: number;
  selectedDetail: IRequestDetailState | null;
  showCancelDialog: boolean;
}

export interface IRequestDetailState {
  id: number;
  aiCategory: string;
  aiSubCategory: string;
  status: string;
  emergencyFlg: boolean;
  context: string;
  response: string;
  residentName: string;
  residentTel: string;
  propertyName: string;
  propertyAddress: string;
  roomNo: string;
  preferableInfo: preferableTime[];
  images: MediaObject[];
  cancelReason: string;
  supportTel: string;
  createdAt: string;
  updatedAt: string;
}

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
const initialState: IScheduleState = {
  timeSection: [],
  list: [],
  listCurrentPage: 0,
  listLastPage: 0,
  selectedDetail: null,
  showCancelDialog: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'schedule',
  initialState,
  reducers: {
    setRequestList: (state, action) => {
      return {
        ...state,
        list: action.payload.list,
        timeSection: action.payload.timeSection,
        listCurrentPage: action.payload.currentPage,
        listLastPage: action.payload.lastPage,
      }
    },
    appendRequestList: (state, action) => {
      return {
        ...state,
        list: state.list.concat(action.payload.list),
        listCurrentPage: action.payload.currentPage,
        listLastPage: action.payload.lastPage,
      }
    },
    setRequestDetail: (state, action) => {
      return {
        ...state,
        selectedDetail: action.payload.detail,
        timeSection: action.payload.timeSection,
        showCancelDialog: false,
      };
    },
    showCancel: (state) => {
      return {
        ...state,
        showCancelDialog: true,
      };
    },
    closeCancel: (state) => {
      return {
        ...state,
        showCancelDialog: false,
      };
    },
  },
});

// ===========================================================================
// Async Action
// ===========================================================================
export const getRequest = (page: number = 1) => {
  return async () => {
    // 一覧取得
    const params: IRequestListRequest = {page: page};
    const resultList = await RequestRepository.getList(params);
    if (resultList.err) {
      return resultList;
    }

    // 時間リスト
    const resultTimeSection = await RequestRepository.getTimeSection();
    if (resultTimeSection.err) {
      return resultTimeSection;
    }

    // 詳細のセット
    let details = [];
    const list = resultList.data.data;
    for (let v of list) {
      const detail = {
        id: v.request_id,
        aiCategory: v.ai_category,
        aiSubCategory: v.ai_sub_category,
        status: v.status,
        emergencyFlg: v.emergency_flg,
        context: v.context,
        response: '',
        residentName: '',
        residentTel: '',
        propertyName: '',
        propertyAddress: '',
        roomNo: '',
        preferableInfo: v.preferable_info,
        createdAt: v.created_at,
        updatedAt: '',
      };
      details.push(detail);
    }

    return {
      data: {
        list: details,
        timeSection: resultTimeSection.data,
        currentPage: resultList.data.current_page,
        lastPage: resultList.data.last_page,
      },
      err: null
    };
  }
};

export const getDetail = (requestId) => {
  return async () => {
    // 時間リスト
    const resultTimeSection = await RequestRepository.getTimeSection();
    if (resultTimeSection.err) {
      return resultTimeSection;
    }

    // 詳細取得
    const resultDetail = await RequestRepository.getDetail(requestId);
    if (resultDetail.err) {
      return resultDetail;
    }

    // 詳細のセット
    const detail = {
      id: resultDetail.data.request_id,
      aiCategory: resultDetail.data.ai_category,
      aiSubCategory: resultDetail.data.ai_sub_category,
      status: resultDetail.data.status,
      emergencyFlg: resultDetail.data.emergency_flg,
      context: resultDetail.data.context,
      response: resultDetail.data.response,
      residentName: resultDetail.data.resident_name,
      residentTel: resultDetail.data.resident_tel,
      propertyName: resultDetail.data.property_name,
      propertyAddress: resultDetail.data.property_address,
      roomNo: resultDetail.data.room_no,
      preferableInfo: resultDetail.data.preferable_info,
      images: resultDetail.data.images,
      cancelReason: resultDetail.data.cancel_reason,
      supportTel: '',
      createdAt: resultDetail.data.created_at,
      updatedAt: resultDetail.data.updated_at,
    };

    // サポートセンターの場合は連絡先を取得
    if (detail.status === RequestStatus.transferred) {
      const resultSupport: IApiResult<IManagementCompanyResponse> =
        await RequestRepository.getManagementCompany();
      if (resultSupport.err) {
        return resultSupport;
      }
      detail.supportTel = resultSupport.data.support_tel;
    }

    return {
      data: {
        detail: detail,
        timeSection: resultTimeSection.data,
      },
      err: null
    };
  }
};

export const cancel = (requestId: number, reason: string) => async (dispatch: any) => {
  try {
    const { err } = await RequestRepository.cancel(requestId, reason);
    if (err) {
      if (isConflict(err)) {
        dispatch(setAlertMessage("問合せ情報が更新されてます。再読み込みをしてお試し下さい"));
      }
    }
    dispatch(push(`/schedule`))

  } catch (e) {
    alert('キャンセル処理に失敗しました。');
  }
};

export const approve = (requestId: number) => async (dispatch: any) => {
  try {
    const { err } = await RequestRepository.approve(requestId);
    if (err) {
      if (isConflict(err)) {
        dispatch(setAlertMessage("問合せ情報が更新されてます。再読み込みをしてお試し下さい"));
      }
    }
    dispatch(push(`/schedule`))

  } catch (e) {
    alert('入札承認に失敗しました。');
  }
};

export const moveDetail = (id: number) => async (dispatch: any) => {
  dispatch(push(`/schedule/${id}`))
};

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const {
  setRequestList,
  appendRequestList,
  setRequestDetail,
  showCancel,
  closeCancel,
} = slice.actions;
export default slice.reducer;


// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const selectPreferable = (
  preferable: preferableTime[]
): preferableTime | null => {
  for (let i = 0; i < preferable.length; i++) {
    if (preferable[i].selected) {
      return preferable[i];
    }
  }
  return null
};

export const getReservedDateText = (
  timeSection: ITimeResponse[],
  preferableInfo: preferableTime[]
) => {
  const preferable = selectPreferable(preferableInfo);
  if (!preferable) {
    return '';
  }
  const fetchTime = (sectionId: number) => {
    const row = timeSection.find((v) => v.time_section_id === sectionId);
    return row.label;
  };
  return formatDateString(preferable.date, DateFormat.YY__MM__DD)
    + '　'
    + fetchTime(preferable.time_section_id);
};

export const requestStatusText = (t, item: IRequestDetailState): string => {
  const status = convertRealState(item);
  const key = [requestStatus,RequestStatus[status]].join(".");
  const text = t(key);
  return text ? text : '';
};

export const requestStatusIcon = (item: IRequestDetailState | null) => {
  if (!item) {
    return null;
  }
  const status = convertRealState(item);
  switch (status) {
    case RequestStatus.waiting_for_cancel:
    case RequestStatus.cancelled:
    case RequestStatus.discard:
      // 未解決
      return btnUnresolved;
    case RequestStatus.completed:
    case RequestStatus.auto_completed:
      // 解決済み
      return btnResolved;
    case RequestStatus.transferred:
      // サポートセンター
      return btnSupport;
    default:
      return btnInquiring;
  }
};

const convertRealState = (item: IRequestDetailState): string => {

  // 業者対応　日程調整前に問合せ破棄
  const completeStatus: string[] = [RequestStatus.completed, RequestStatus.auto_completed];
  if ( item.aiCategory === 'vendor'
    && completeStatus.includes(item.status) === false
    && item.preferableInfo.length === 0
    && item.emergencyFlg === false
  ) {
    return RequestStatus.discard;
  }

  // FAQ/DIY　「はい/いいえ」回答前に問合せ破棄
  if ( item.aiCategory !== 'vendor'
    && item.status === RequestStatus.waiting_for_open
  ) {
    return RequestStatus.discard;
  }

  return item.status
};

const isConflict = (error) => {
  return (error?.code === 400 && error?.data?.error === "Bad Request");
};
