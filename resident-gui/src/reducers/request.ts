import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router'
import RequestRepository from "../api/repository/request";
import AWSRepository from "../api/repository/aws";
import { IApiResult } from "../types/apiResult";
import {
  ITimeResponse,
  IManagementCompanyResponse,
  AIRequest,
  AIResponse,
} from "../api/schema/request";

const ABORT_CODE = 'ABORT_CODE_999999999';
export const SELECT_LABEL_ABORT: AiLabel = {
  text: 'その他',
  aiText: ABORT_CODE,
};

// ---------------------------------------------------------------------------
// Type
// ---------------------------------------------------------------------------
export interface IRequestState {
  supportName: string;
  supportTel: string;
  preferableTimes: ITimeResponse[];
  aiResult: AIResponse | null;
  messages: Message[];
  disabledUserInput: boolean;
  disabledForm: boolean;
}

export interface Message {
  index?: number
  owner: 'ai' | 'user';
  type: string;
  data: any;
}

export type AiLabel = {
  aiText: string;
  text: string;
}

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
const initialState: IRequestState = {
  supportName: '',
  supportTel: '',
  preferableTimes: [],
  aiResult: null,
  messages: [],
  disabledUserInput: false,
  disabledForm: false,
};

const setMessageKey = (message: Message[]): Message[] => {
  return message.map((v, i) => ({...v, index: i}));
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'request',
  initialState,
  reducers: {
    clear: () => {
      const messages: Message[] = [{
        owner: 'ai',
        type: 'text',
        data: {
          text: 'ゲストさん、こんにちは。\n複数のメッセージを送信できますが、確定ボタンを押すことで複数のメッセージを1つの質問と判断しAIが回答します。'
        },
      }];

      return {
        ...initialState,
        messages: setMessageKey(messages),
      }
    },
    setInitialData: (state, action) => {
      return {
        ...state,
        supportName: action.payload.supportName,
        supportTel: action.payload.supportTel,
        preferableTimes: action.payload.preferableTimes,
      }
    },
    setMessages: (state, action) => {
      return {
        ...state,
        messages: setMessageKey(action.payload),
      }
    },
    appendMessages: (state, action) => {
      const newMessages = [...state.messages, action.payload];
      return {
        ...state,
        messages: setMessageKey(newMessages),
      }
    },
    successPostMessage: (state, action) => {
      const { aiResult, messages } = action.payload;
      const newAiResult = aiResult ? aiResult : state.aiResult;
      const newMessages = [...state.messages, ...messages];
      return {
        ...state,
        aiResult: newAiResult,
        messages: setMessageKey(newMessages),
      }
    },
    disabledUserInput: (state) => {
      return {
        ...state,
        disabledUserInput: true,
      }
    },
    setDisabledForm: (state, action) => {
      return {
        ...state,
        disabledForm: action.payload,
      }
    },
    setLockMessage: (state, action) => {
      const { index, lock } = action.payload;
      const messages = [...state.messages];
      const targetMessage = {
        ...messages[index],
        data: {
          ...messages[index].data,
          lock: lock,
        }
      };
      messages[index] = targetMessage;
      return {
        ...state,
        messages: messages
      }
    },
  },
});


// ===========================================================================
// Async Action
// ===========================================================================
/**
 * 画面初期処理
 */
export const dataInitialize = () =>
  async () => {

  // 時間リストを取得
  const resultTime: IApiResult<ITimeResponse[]> =
    await RequestRepository.getTimeSection();
  if (resultTime.err) {
    return resultTime;
  }
  const preferableTimes = resultTime.data;

  // サポートセンターの連絡先を取得
  const resultSupport: IApiResult<IManagementCompanyResponse> =
    await RequestRepository.getManagementCompany();
  if (resultSupport.err) {
    return resultSupport;
  }

  return {
    data: {
      preferableTimes: preferableTimes,
      supportName: resultSupport.data.name,
      supportTel: resultSupport.data.support_tel,
    },
    err: null
  };
};

/**
 * 戻るボタン押下時
 */
export const onBackClear = () => async (dispatch: any) => {
  dispatch(clear());
  dispatch(push('/home'))
};

/**
 * ユーザが入力した内容を送信
 *
 * @param text
 */
export const pushMessageUserInput = (text: string) =>
  async (dispatch, getState) => {

  const { request } = getState();
  const messages = request?.messages || [];
  const newMessages = messages.filter(v => v.type !== 'input_finished');
  newMessages.push({
    owner: 'user',
    type: 'original_text',
    data: {
      text: text
    },
  });
  dispatch(setMessages(newMessages));
};

/**
 * 確定のメッセージを表示
 */
export const pushMessageConfirmFinished = () =>
  async (dispatch, getState) => {

  const { request } = getState();
  const messages = request?.messages || [];
  const newMessages = messages.filter(v => v.type !== 'input_finished');
  newMessages.push({
    owner: 'ai',
    type: 'input_finished',
    data: {},
  });
  dispatch(setMessages(newMessages));
};

/**
 * 問合せV2
 * ・確定ボタン押下時
 * ・QtoQ 選択形式を押下時
 *
 * @param messages
 * @param label
 * @param aiResult
 */
export const postMessage = (messages, label?, aiResult?) => async () => {
  // QtoQ 2回目以降の問合せKey
  const key = aiResult?.next_step;

  // original_text
  const text = fetchOriginalText(messages);

  // その他
  const abort = label === ABORT_CODE;

  // Process
  //  - その他: 前回のtargetのみ引継ぎ
  //  - QtoQ: 前回のprocess＋next_stepを付与
  const newProcess =
    abort ? {target: aiResult?.process?.target} :
    key ? {...aiResult.process, [key]: label} :
    {};

  // step
  const step = key
    ? key
    : 'start';

  const params: AIRequest = {
    original_text: text,
    process: JSON.stringify(newProcess),
    step: step,
    abort: abort,
  };

  const result: IApiResult<AIResponse> =
    await RequestRepository.postV2(params);
  if (result.err) {
    return result;
  }

  // TODO: Debug用 強制的にxxx対応へ
  if (false) {
    result.data.specific.request_id = 99999;
    result.data.specific.may_unknown = false;

    // 業者対応
    // result.data.specific.can_order = true;
    // result.data.specific.response_text = '';

    // DIY or FAQ + サポート
    // result.data.specific.can_order = false;
    // result.data.specific.response_text = 'DIY or FAQ';

    // DIY or FAQ + 業者
    result.data.specific.can_order = true;
    result.data.specific.response_text = 'DIY or FAQ';
  }

  const newMessages = makeNewMessage(result.data);

  return {
    data: {
      aiResult: result.data,
      messages: newMessages,
    },
    err: null
  };
};

/**
 * QtoQ 選択形式を押下
 * 選択したテキストを表示する
 *
 * @param text
 */
export const selectOption = (text: string) =>
  async (dispatch) => {

  const newMessages = {
    owner: 'user',
    type: 'text',
    data: {
      text: text
    },
  };
  dispatch(appendMessages(newMessages));
};

/**
 * FAQ or DIY
 * [解決] or [未解決]
 *
 * @param solved
 * @param aiResult
 */
export const requestSolved = (solved: boolean, aiResult: AIResponse) => {
  return async () => {
    const requestId = aiResult.specific.request_id;
    await RequestRepository.solved(requestId, solved);

    let newMessages: Message[] = [];
    if (solved) {
      newMessages.push({
        owner: 'ai',
        type: 'text',
        data: {
          text: 'ご利用ありがとうございました。'
        },
      });
    } else {
      const data = {
        ...aiResult,
        specific : {
          ...aiResult.specific,
          response_text: ''
        }
      };
      newMessages = makeNewMessage(data);
    }

    return {
      data: {
        messages: newMessages,
      },
      err: null
    };
  }
};

/**
 * 業者予約
 * 確認を表示する
 *
 * @param data
 * @param emergencyFlg
 */
export const setVendorReserve = (
  data: any,
  emergencyFlg: boolean,
) => async (dispatch) => {

  const messages = {
    owner: 'user',
    type: 'vendor_confirm',
    data: {
      ...data,
      emergencyFlg: emergencyFlg,
    },
  };
  dispatch(appendMessages(messages));
};

/**
 * 業者予約
 * キャンセルボタン押下
 */
export const cancelVendorReserve = () =>
  async (dispatch, getState) => {

  const { request } = getState();
  const messages = request?.messages || [];
  const newMessages = messages.filter(v => v.type !== 'vendor_confirm');
  dispatch(setMessages(newMessages));
};

/**
 * 業者予約
 * 確定ボタン押下
 *
 * @param requestId
 * @param data
 */
export const submitVendorReserve = (requestId: number, data: any) => async () => {

  // 希望時間の登録
  const params = {
    emergency_flg: data.emergencyFlg,
    preferable_times: [1, 2, 3].map(v => {
      return {
        date: data[`preferableDay${v}`],
        time_section_id: data[`preferableTime${v}`],
      }
    }),
  };

  const result = await RequestRepository.order(requestId, params);
  if (result.err) {
    return result;
  }

  // 画像・動画のアップロード
  const uploadResult = await uploadFiles(requestId, data.fileElementId);
  if (uploadResult.err) {
    return uploadResult;
  }

  const messages = {
    owner: 'ai',
    type: 'text',
    data: {
      text: 'お問合せいただいた内容を業者が確認の上、詳しい内容を伺う必要がある際はお電話をさせていただきます。ご利用ありがとうございました。',
    },
  };

  return {
    data: messages,
    err: null
  };
};

/**
 * サポートセンター対応
 * 確認を表示する
 *
 * @param data
 */
export const setSupportCenterFileUpload = (data: any) => async (dispatch) => {
  let message;
  const files = getElementFiles(data.fileElementId);
  if (files.length === 0) {
    message = {
      owner: 'ai',
      type: 'text',
      data: {
        text: 'ご利用ありがとうございました。'
      },
    };
  } else {
    message = {
      owner: 'user',
      type: 'file_upload_confirm',
      data: data,
    };
  }
  dispatch(appendMessages(message));
};

/**
 * サポートセンター対応
 * キャンセルボタン押下
 */
export const cancelFileUpload = () =>
  async (dispatch, getState) => {

  const { request } = getState();
  const messages = request?.messages || [];
  const newMessages = messages.filter(v => v.type !== 'file_upload_confirm');
  dispatch(setMessages(newMessages));
};

/**
 * サポートセンター対応
 * 確定ボタン押下
 *
 * @param requestId
 * @param data
 */
export const submitFileUpload = (requestId: number, data: any) => async () => {

  // 画像・動画のアップロード
  const uploadResult = await uploadFiles(requestId, data.fileElementId);
  if (uploadResult.err) {
    return uploadResult;
  }

  const messages = {
    owner: 'ai',
    type: 'text',
    data: {
      text: 'ご利用ありがとうございました。'
    },
  };

  return {
    data: messages,
    err: null
  };
};

// ---------------------------------------------------------------------------
// Method
// ---------------------------------------------------------------------------
/**
 * AIの結果から出力内容を判定
 *
 * @param data
 */
const makeNewMessage = (data: AIResponse): Message[] => {

  //---------------------------------------------------
  // AIが判定できな場合は、再入力
  //---------------------------------------------------
  if (data == null) {
    return [{
      owner: 'ai',
      type: 'unknown',
      data: {},
    }];
  }

  //---------------------------------------------------
  // MayUnknownの場合は、再入力
  //---------------------------------------------------
  const mayUnknown = data?.specific?.may_unknown || false;
  if (mayUnknown) {
    return [{
      owner: 'ai',
      type: 'unknown',
      data: {},
    }];
  }

  //---------------------------------------------------
  // ラベル選択
  //---------------------------------------------------
  const isComplete = data.complete;
  const selectLabels = data.select_labels || {};
  if (!isComplete && 0 < Object.keys(selectLabels).length) {
    let labels: AiLabel[] = Object
      .entries(selectLabels)
      .map((v: any[]) => ({
        text: v[1].text,
        aiText: v[1].ai_text,
      }));

    // [その他]の選択肢を追加
    labels.push(SELECT_LABEL_ABORT);

    return [{
      owner: 'ai',
      type: 'choose_option',
      data: {
        labels: labels,
      },
    }];
  }

  //---------------------------------------------------
  // DIY or FAQ
  //---------------------------------------------------
  const responseText = data?.specific?.response_text || '';
  if (responseText !== '') {
    return [
      {
        owner: 'ai',
        type: 'faq_diy',
        data: {
          text: responseText,
        },
      },
      {
        owner: 'ai',
        type: 'confirm_solved',
        data: {},
      },
    ];
  }

  //---------------------------------------------------
  // 業者
  //---------------------------------------------------
  const canOrder = data?.specific?.can_order || false;
  if (canOrder) {
    return [{
      owner: 'ai',
      type: 'vendor',
      data: {},
    }];
  }

  //---------------------------------------------------
  // 判定不可
  // 問合せIDあり：サポートセンター
  //---------------------------------------------------
  const requestId = data?.specific?.request_id;
  if (requestId) {
    return [{
      owner: 'ai',
      type: 'support_center',
      data: {},
    }];
  }

  //---------------------------------------------------
  // 判定不可
  // 問合せIDなし：再入力
  //---------------------------------------------------
  return [{
    owner: 'ai',
    type: 'unknown',
    data: {},
  }];
};

/**
 * ユーザが入力したテキストのみを取得
 *
 * @param messages
 */
const fetchOriginalText = (messages: Message[]): string => {
  return messages
    .filter(v => (v.owner === 'user' && v.type === 'original_text'))
    .map(v => v.data.text)
    .join('');
};

/**
 * 画像・動画をS3にアップロード
 *
 * @param requestId
 * @param elementId
 */
const uploadFiles = async (requestId, elementId) => {
  if (!requestId) {
    return { data: {}, err: null };
  }

  const files = getElementFiles(elementId);
  if (files.length === 0) {
    return { data: {}, err: null };
  }

  const fileNames = files.map((file) => file.name);
  const result = await RequestRepository.getPreSignedUrl(requestId, fileNames);
  if (result.err) {
    return result;
  }

  const links = result.data;
  if (files.length !== links.length) {
    return { data: {}, err: {code: null} };
  }

  try {
    await Promise.all(
      links.map(async (v, i) => {
        await AWSRepository.s3FileUpload(links[i], files[i]);
      })
    );
  } catch (error) {
    return { data: {}, err: {code: null} };
  }

  return { data: {}, err: null };
};

/**
 * [input file]ElementからFileオブジェクトを取得
 *
 * @param elementId
 */
const getElementFiles = (elementId): File[] => {
  const input = document.getElementById(elementId) as any;
  return input?._value || [];
};

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const {
  clear,
  setInitialData,
  setMessages,
  appendMessages,
  successPostMessage,
  disabledUserInput,
  setDisabledForm,
  setLockMessage,
} = slice.actions;
export default slice.reducer;
