import { createSlice } from '@reduxjs/toolkit';
import { IResident } from "../api/schema/resident";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IVendorState {
  managementCompanyId: string;
  name: string;
  residentCode: string;
  residentId: string;
  roomId: string;
  tel: string;
  lineConnected: boolean;
  createdAt: boolean;
  updatedAt: string;
}

const initialState = {
  managementCompanyId:'',
  name: '',
  residentCode: '',
  residentId: '',
  roomId: '',
  tel: '',
  lineConnected: false,
  created_at: false,
  updated_at: '',
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'resident',
  initialState,
  reducers: {
    setUser: (state, action) => {
      const payload: IResident = action.payload;
      return {
        ...state,
        managementCompanyId: payload.management_company_id,
        name: payload.name,
        residentCode: payload.resident_code,
        residentId: payload.resident_id,
        roomId: payload.room_id,
        tel: payload.tel,
        lineConnected: payload.line_connected,
        createdAt: payload.created_at,
        updatedAt: payload.updated_at,
      };
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { setUser } = slice.actions;


export default slice.reducer;
