import { createSlice } from '@reduxjs/toolkit';
import { IApiResult } from "../types/apiResult";
import {ResponseAuth, ResponseLiffId} from "../api/schema/line";
import { IResident } from "../api/schema/resident";
import LineRepository from "../api/repository/line";
import {ILoginResponse} from "../api/schema/auth";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface LineState {
  authUrl: string;
  liffId: string;
}

const initialState: LineState = {
  authUrl: '',
  liffId: '',
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'line',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState }
    },
    setAuthUrl: (state, action) => {
      return {
        ...state,
        authUrl: action.payload.url,
      };
    },
    setLiffId: (state, action) => {
      return {
        ...state,
        liffId: action.payload.liff_id,
      };
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { clear, setAuthUrl, setLiffId } = slice.actions;

export const getLiffId = (lineClientId: string) => {
  return async () => {
    const result: IApiResult<ResponseLiffId>
      = await LineRepository.getLiffId(lineClientId);
    if (result.err) {
      return result;
    }
    return {data: result.data, err: null};
  }
};

export const login = (
  lineAccessToken: string,
  lineClientId: string,
) => {
  return async () => {
    const result: IApiResult<ILoginResponse>
      = await LineRepository.login(lineAccessToken, lineClientId);
    if (result.err) {
      return result;
    }
    return {data: result.data, err: null};
  }
};

export const auth = () => {
  return async () => {
    const result: IApiResult<ResponseAuth> = await LineRepository.auth();
    if (result.err) {
      return result;
    }
    return {data: result.data, err: null};
  }
};

export const verify = (
  code: string,
  state: string,
) => {
  return async () => {
    const result: IApiResult<IResident>
      = await LineRepository.verify(code, state);
    if (result.err) {
      return result;
    }
    return {data: result.data, err: null};
  }
};

export const deactivate = () => {
  return async () => {
    const result: IApiResult<IResident> = await LineRepository.deactivate();
    if (result.err) {
      return result;
    }
    return {data: result.data, err: null};
  }
};

export default slice.reducer;
