import { createSlice } from '@reduxjs/toolkit';
import RequestRepository from "../api/repository/request";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface NotificationState {
  badgeSchedule: boolean;
}

const initialState: NotificationState = {
  badgeSchedule: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    clear: () => {
      return { ...initialState };
    },
    setBadge: (state, action) => {
      const {
        open = 0,
        scheduling = 0,
        in_progress = 0,
        completed = 0,
        waiting_for_cancel = 0,
        cancelled = 0,
      } = action.payload;

      const isSchedule = 0 < (
        open +
        scheduling +
        in_progress +
        completed +
        waiting_for_cancel +
        cancelled
      );
      return {
        ...state,
        badgeSchedule: state.badgeSchedule || isSchedule,
      };
    },
    clearBadge: (state) => {
      return {
        ...state,
        badgeSchedule: false,
      };
    },
  },
});

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { clear, setBadge, clearBadge } = slice.actions;

// ---------------------------------------------------------------------------
// Async Action
// ---------------------------------------------------------------------------
export const getBadge = () => async (dispatch) => {
  const { data, err } = await RequestRepository.getBadge();
  if (!err) {
    dispatch(setBadge(data));
  }
};

export const refreshBadge = () => async (dispatch) => {
  dispatch(clearBadge());
  await RequestRepository.getBadge();
};

export default slice.reducer;
