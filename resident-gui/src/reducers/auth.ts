import { createSlice } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';
import { push } from 'connected-react-router'
import AuthRepository from '../api/repository/auth';
import { IApiResult } from '../types/apiResult';
import { setUser } from './resident'
import { RootState } from "../store";
import { ILoginResponse } from "../api/schema/auth";
import { setToken } from "../store/storage";

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
export interface IAuthState {
  loading: boolean;
  onError: boolean;
  accessToken: string;
  refreshToken: string;
}

const initialState: IAuthState = {
  loading: false,
  onError: false,
  accessToken: '',
  refreshToken: '',
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, action: {payload : Pick<IAuthState, "loading" | "onError" > } ) => {
      return {
        ...state,
        loading: action.payload.loading,
        onError: action.payload.onError,
      };
    },
    setLogin: (state, action) => {
      return {
        ...state,
        loading: false,
        accessToken: action.payload.access_token,
        refreshToken: action.payload.refresh_token,
      };
    },
    reset : (state, action) => {
      return { ...initialState }
    }
  },
});

// ===========================================================================
// Async Action
// ===========================================================================
export const getLogin = (code: string) => async (dispatch: any) => {
  try {
    dispatch(setLoading({ loading: true, onError: false }));

    const result: IApiResult<ILoginResponse> = await AuthRepository.login(code);
    if (result.err) {
      dispatch(setLoading({ loading: false, onError: true }));
      return;
    }

    // 入居者情報
    dispatch(setUser(result.data.resident));

    // ログイン情報
    dispatch(setLogin(result.data));
    setToken({
      access_token: result.data.access_token,
      refresh_token: result.data.refresh_token,
    });

    // ホーム画面に遷移
    dispatch(push('/home'));
    dispatch(setLoading({ loading: false, onError: false }));

  } catch (e) {
    dispatch(setLoading({ loading: false, onError: true }));
    alert('予期せぬエラーが発生しました。');
  }
};

export const setLogout = () => (dispatch: any) => {
  dispatch(reset({}));
  dispatch(push('/logout'));
};

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { setLoading, setLogin, reset } = slice.actions;
export default slice.reducer;


// ---------------------------------------------------------------------------
// Selector
// ---------------------------------------------------------------------------
export const selectIsLogin = (state: RootState): boolean => {
  const { auth } = state;
  return auth.accessToken !== '' && auth.refreshToken !== '';
};

export const useLoginStatus = () => {
  return useSelector( ( state: { auth: IAuthState } ) => state.auth )
};
