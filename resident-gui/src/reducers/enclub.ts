import { createSlice } from '@reduxjs/toolkit';
import { push } from 'connected-react-router'
import EnClubRepository from "../api/repository/enclub";
import { IForm } from '../components/pages/Auth/EnClub'

// ---------------------------------------------------------------------------
// State
// ---------------------------------------------------------------------------
interface IAuthState {
  loading: boolean;
}

const initialState: IAuthState = {
  loading: false,
};

// ---------------------------------------------------------------------------
// Reducer
// ---------------------------------------------------------------------------
const slice = createSlice({
  name: 'enclub',
  initialState,
  reducers: {
    setLoading: (state, action) => {
      return {
        ...state,
        loading: action.payload,
      };
    },
  },
});

// ===========================================================================
// Async Action
// ===========================================================================
export const login = (data: IForm, accessKey, secretKey) => async (dispatch: any) => {
  try {
    dispatch(setLoading(true));

    let key = process.env.REACT_APP_BASIC_KEY;
    if (accessKey && secretKey) {
      key = `${accessKey}:${secretKey}`;
    }

    const basicKey = await base64Encode(key);
    const authRes = await EnClubRepository.login(
      data.residentCode,
      data.propertyCode,
      data.roomCode,
      basicKey,
    );

    dispatch(push(`/auth/verify/${authRes.authCode}`))

  } catch (e) {
    dispatch(setLoading(false));
    alert('ログインに失敗しました。');
  }
};

const base64Encode = (...parts: string[]): Promise<string> => {
  return new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      let result = e.target.result;
      const offset = result.indexOf(",") + 1;
      resolve(result.slice(offset));
    };
    reader.readAsDataURL(new Blob(parts));
  });
};

// ---------------------------------------------------------------------------
// Action
// ---------------------------------------------------------------------------
export const { setLoading  } = slice.actions;
export default slice.reducer;
