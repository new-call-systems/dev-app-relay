import { combineReducers } from '@reduxjs/toolkit'
import { connectRouter } from 'connected-react-router'
import { History } from 'history'

import enclubReducer from "./enclub";
import authReducer from "./auth";
import residentReducer from "./resident";
import requestReducer from "./request";
import scheduleReducer from "./schedule";
import messageReducer from "./message";
import lineReducer from "./line";
import notificationReducer from "./notification";

const reducers = (history: History) => combineReducers({
  router: connectRouter(history),
  enclub: enclubReducer,
  auth: authReducer,
  resident: residentReducer,
  request: requestReducer,
  schedule: scheduleReducer,
  message: messageReducer,
  line: lineReducer,
  notification: notificationReducer,
});

export default reducers
