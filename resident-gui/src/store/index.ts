import {
  configureStore,
  getDefaultMiddleware,
  ThunkAction,
  Action,
} from '@reduxjs/toolkit';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import createRootReducer from '../reducers';
import { combinePersist, getPersistStore, ignoredActionList } from './persist';


// BrowserHistory
const history = createBrowserHistory({
  basename: process.env.REACT_APP_BASE_URL || ""
});

// redux-middleware
const middleware = [
  ...getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: ignoredActionList,
    },
  }),
];

// router
const router = routerMiddleware(history);
middleware.push(router);

// devtools
const isDev = process.env.NODE_ENV === 'development';
if (isDev) {
  const logger = require('redux-logger');
  const loggerMiddleware = logger.createLogger({
    diff: false,
    collapsed: true,
  });

  middleware.push(loggerMiddleware);
}

// store
const reducer = createRootReducer(history);
const persistReducer = combinePersist(reducer);
const store = configureStore({
  reducer: persistReducer,
  middleware: middleware,
  devTools: isDev,
});

// persist用
const persistor = getPersistStore(store);

// Debug
//persistor.purge();

// 型定義
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof reducer>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export { store, history, persistor };
