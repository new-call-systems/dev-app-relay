const TOKEN_KEY: string = 'cocoenresident:tokens';

interface IToken {
  access_token: string;
  refresh_token: string;
}

/**
 * Tokenを取得
 */
const getToken = (): IToken | null => {
  try {
    const token = localStorage.getItem(TOKEN_KEY);
    return JSON.parse(token);
  } catch (error) {
    return null;
  }
};

/**
 * アクセストークンを取得
 */
const getAccessToken = (): string | null => {
  const token = getToken();
  return token?.access_token;
};

/**
 * リフレッシュトークンを取得
 */
const getRefreshToken = (): string | null => {
  const token = getToken();
  return token?.refresh_token;
};

/**
 * Tokenをセット
 * @param token
 */
const setToken = (token: IToken): void => {
  localStorage.setItem(TOKEN_KEY, JSON.stringify(token));
};

/**
 * Storageをクリア
 */
const clearStorage = (): void => {
  localStorage.clear();
};

// Debug
// (() => {
//   clearStorage();
// })();

export { setToken, clearStorage, getAccessToken, getRefreshToken };
