import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const PERSIST_KEY = 'cocoen';

// 永続化するstate
const persistConfig = {
  key: PERSIST_KEY,
  storage,
};

const ignoredActionList = [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER];

// persistで使用するActionをReduxReducerと合体
const combinePersist = (reducer: any) => {
  return persistReducer(persistConfig, reducer);
};

// persist用のstoreを作成
const getPersistStore = (store: any) => {
  return persistStore(store);
};

export { ignoredActionList, combinePersist, getPersistStore };
