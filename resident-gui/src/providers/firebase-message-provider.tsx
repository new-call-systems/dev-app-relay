
import React, { useEffect, useCallback, createContext, useState, useMemo } from "react";
import * as firebase from "firebase/app";
import { initializeApp } from "firebase";
import * as path from "path";

//import AppContext, { RequiredAppContext } from "../app-context";
import firebaseConfig from "../config/firebase-config";

export interface IFirebaseMessageValue {
	messaging: firebase.messaging.Messaging | null;
	token: string;
};

type FirebaseMessageKeys = keyof IFirebaseMessageValue;

export type RequiredFirebaseMessageContext = {
	[K in FirebaseMessageKeys]: NonNullable<IFirebaseMessageValue[K]>;
};

const FirebaseMessageContext = createContext<IFirebaseMessageValue>({
	messaging: null, token: ""
})

export const FirebaseMessageContextProvider = FirebaseMessageContext.Provider
export const FirebaseMessageContextConsumer = FirebaseMessageContext.Consumer

export default FirebaseMessageContext

export interface Props { };

export const FirebaseMessageProvider: React.FC<Props> = props => {

	const [ value, setValue ] = useState<IFirebaseMessageValue>({messaging: null, token: "" });
	const [ loaded, setLoaded ] = useState<boolean>(false)

	const isSupported = useMemo( () => {
		return firebase.messaging.isSupported()
	}, [] )
	
	const init = useCallback( async (firebaseApp: firebase.app.App ) => {

		const message = firebaseApp.messaging();
		const registWorkerPath = path.resolve((process.env.REACT_APP_BASE_URL || ""), "firebase-messaging-sw.js")

		try{
			const registration = await navigator.serviceWorker.register(registWorkerPath)
			message.useServiceWorker(registration);
			// Request permission and get token.....
			await message.requestPermission()
			const token = await message.getToken();
			console.log( "Get token", token );
			setValue({ messaging: message, token});
		}catch(e){
			console.log(e)
		};
	}, [] )

	useEffect( () => {
		if( !loaded && isSupported ){
			const firebaseApp = initializeApp(firebaseConfig)
			init( firebaseApp );
			setLoaded(true)
		};
	}, [init, loaded, isSupported] )

	return <FirebaseMessageContextProvider value={value} >
		{ props.children }
	</FirebaseMessageContextProvider>
};


