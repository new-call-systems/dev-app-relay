export interface IApiResult<T> {
  data: T;
  err?: any;
}
