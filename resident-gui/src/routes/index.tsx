import React from 'react'
import { Route, Switch } from 'react-router-dom';
import Div100Vh from "react-div-100vh";

import PrivateRoute from "./private";
import Auth from '../components/pages/Auth/index';
import AuthVerify from "../components/pages/Auth/Verify";
import Logout from '../components/pages/Auth/Logout';
import Liff from "../components/pages/Liff";
import TreeRowsTemplate from "../components/templates/ThreeRows";
import Home from "../components/pages/Home";
import Request from "../components/pages/Request";
import Schedule from "../components/pages/Schedule";
import ScheduleDetail from "../components/pages/Schedule/detail";
import Line from "../components/pages/Line";

const NoMatch: React.FC = () => <div>404 NotFound</div>;

const Routes: React.FC = () => {
  return (
    <Div100Vh>
      <Switch>
        <Route exact path="/" component={Auth} />
        <Route exact path="/logout" component={Logout} />
        <Route exact path="/auth/verify/:authCode" component={AuthVerify} />
        <Route exact path="/liff-cb" component={Liff}/>
        <PrivateRoute path="/home" component={(p) => <TreeRowsTemplate {...p} ><Home /></TreeRowsTemplate>}/>
        <PrivateRoute path="/request" component={Request}/>
        <PrivateRoute exact path="/schedule" component={(p) => <TreeRowsTemplate {...p} ><Schedule /></TreeRowsTemplate>}/>
        <PrivateRoute exact path="/schedule/:id" component={(p) => <TreeRowsTemplate {...p} ><ScheduleDetail /></TreeRowsTemplate>}/>
        <PrivateRoute exact path="/line" component={Line}/>
        <PrivateRoute exact path="/line-cb" component={Line}/>
        <Route component={NoMatch} />
      </Switch>
    </Div100Vh>
  )
};

export default Routes;
