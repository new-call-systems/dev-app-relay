import React from 'react'
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom'
import { selectIsLogin } from "../reducers/auth";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isAuthenticated = useSelector(selectIsLogin);
  return (
    <Route {...rest} render={props => (isAuthenticated
        ? <Component {...props} />
        : <Redirect to="/"/>
    )}/>
  )
};

export default PrivateRoute;
