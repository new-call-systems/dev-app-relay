import { format, addDays, parse, parseISO } from 'date-fns'
import { utcToZonedTime } from 'date-fns-tz'
import { ja } from 'date-fns/locale'

export const DateFormat = {
  YY_MM_DD: 'yyyy-MM-dd',
  YY__MM__DD: 'yyyy/MM/dd',
};

const f = (d, t) => format(d, t, { locale: ja });

export const getToday = () => {
  return f(new Date(), DateFormat.YY_MM_DD);
};

export const addDay = (date: string, amount: number) => {
  const parseDate = parseDay(date);
  const calc = addDays(parseDate, amount);
  return f(calc, DateFormat.YY_MM_DD);
};

export const parseDay = (date: string) => {
  if (date == null) {
    return new Date();
  }
  return parse(date, DateFormat.YY_MM_DD, new Date());
};

export const formatString = (date: Date) => {
  return f(date, DateFormat.YY_MM_DD);
};

export const formatTZ = (date: string) => {
  const utcDate: Date = parseISO(date);
  const jstDate = utcToZonedTime(utcDate, 'Asia/Tokyo');
  return f(jstDate, DateFormat.YY__MM__DD);
};

export const formatDateString = (date: string, formatType: string) => {
  try {
    const parse: Date = parseISO(date);
    return f(parse, formatType);
  } catch (ex) {
    return date;
  }
};

export const getDateTimeMillisecond = () => {
  const dt = new Date();
  const y = dt.getFullYear();
  const m = ('00' + (dt.getMonth() + 1)).slice(-2);
  const d = ('00' + dt.getDate()).slice(-2);
  const hh = ('00' + dt.getHours()).slice(-2);
  const mi = ('00' + dt.getMinutes()).slice(-2);
  const ss = ('00' + dt.getSeconds()).slice(-2);
  const dd = dt.getMilliseconds();
  return (`${y}${m}${d}${hh}${mi}${ss}${dd}`);
};
