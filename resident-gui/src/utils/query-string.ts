import qs from 'query-string'

export const parseQuery = (path: string, key: string, defaultValue?: any) => {
  const values = qs.parse(path);
  const v = values[key];
  return v ? v : defaultValue;
};
