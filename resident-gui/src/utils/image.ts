import loadImage from 'blueimp-load-image';

export const compressImage = async (file): Promise<File> => {
  const resize = {maxHeight: 500, maxWidth: 500};
  const options = await parseMetaData(file, resize);
  return await makeFileObject(file, options);
};

const parseMetaData = async (file, options) => {
  return new Promise((resolve) => {
    loadImage.parseMetaData(file, (data) => {
      if (data.exif) {
        options.orientation = data.exif.get('Orientation');
      }
      resolve(options);
    });
  });
};

const makeFileObject = async (file, options): Promise<File> => {
  return new Promise((resolve) => {
    options.canvas = true;
    loadImage(file, (canvas: HTMLCanvasElement) => {
      const data = canvas.toDataURL(file.type);
      const fileObject = base64ToFile(data, file.type, file.name);
      resolve(fileObject);
    }, options);
  });
};

const base64ToFile = (base64, fileType, fileName) => {
  const bin = atob(base64.split(',')[1]);
  const buffer = new Uint8Array(bin.length);
  for (let i = 0; i < bin.length; i++) {
    buffer[i] = bin.charCodeAt(i);
  }
  return new File([buffer.buffer], fileName, {type: fileType});
};
