import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  AxiosError,
} from 'axios';

import { getAccessToken, getRefreshToken, setToken, clearStorage } from "../../store/storage";
import { IApiResult } from "../../types/apiResult";
import { UNEXPECTED_ERRORS, FAILED_REFRESH_TOKEN } from "../../constants/ErrorCode";

const isDev = process.env.NODE_ENV !== 'production';

/**
 * axios instance
 */
const instance: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_ENDPOINT_RELAY_SERVER,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
  responseType: 'json',
});

/**
 * request
 */
instance.interceptors.request.use((options: AxiosRequestConfig) => {
  const token = getAccessToken();
  if (token) {
    options.headers.Authorization = `Bearer ${token}`;
  }
  return options;
});

/**
 * response
 */
instance.interceptors.response.use(
  (response: AxiosResponse): any => {
    // eslint-disable-next-line no-console
    isDev && console.log('SUCCESS', response);

    return response;
  },
  (error: AxiosError): any => {
    const retError = error.response || error;
    // eslint-disable-next-line no-console
    isDev && console.log('ERROR', retError);

    return Promise.reject(error);
  },
);

export default class HttpClient {

  /**
   * get
   * @param path
   * @param params
   */
  public static async get<T>(
    path: string,
    params?: object
  ): Promise<IApiResult<T>> {
    try {
      const response: AxiosResponse = await instance.get<T>(path, params);
      return {data: response.data, err: null};

    } catch (error) {
      const response = await this.apiErrorResponse(error);
      return {data: response.data, err: response.error};
    }
  }

  /**
   * post
   * @param path
   * @param data
   */
  public static async post<T>(
    path: string,
    data?: any
  ): Promise<IApiResult<T>> {
    try {
      const response: AxiosResponse = await instance.post<T>(path, data);
      return {data: response.data, err: null};

    } catch (error) {
      const response = await this.apiErrorResponse(error);
      return {data: response.data, err: response.error};
    }
  }

  /**
   * put
   * @param path
   * @param data
   */
  public static async put<T>(
    path: string,
    data?: any
  ): Promise<IApiResult<T>> {
    try {
      const response: AxiosResponse = await instance.put<T>(path, data);
      return {data: response.data, err: null};

    } catch (error) {
      const response = await this.apiErrorResponse(error);
      return {data: response.data, err: response.error};
    }
  }

  /**
   * patch
   * @param path
   * @param data
   */
  public static async patch<T>(
    path: string,
    data?: any
  ): Promise<IApiResult<T>> {
    try {
      const response: AxiosResponse = await instance.patch<T>(path, data);
      return {data: response.data, err: null};

    } catch (error) {
      const response = await this.apiErrorResponse(error);
      return {data: response.data, err: response.error};
    }
  }

  /**
   * delete
   * @param path
   * @param params
   */
  public static async delete<T>(
    path: string,
    params?: object
  ): Promise<IApiResult<T>> {
    try {
      const response: AxiosResponse = await instance.delete<T>(path, params);
      return {data: response.data, err: null};

    } catch (error) {
      const response = await this.apiErrorResponse(error);
      return {data: response.data, err: response.error};
    }
  }

  /**
   * エラー時のレスポンス
   * @param error
   */
  private static async apiErrorResponse (
    error: AxiosError
  ): Promise<any> {

    // アクセストークンの再発行
    let isRetry = false;
    try {
      isRetry = await this.refreshToken();
    } catch (retryError) {
      await clearStorage();
      const retryResponse = {
        code: FAILED_REFRESH_TOKEN,
        data: null,
      };
      return {data: null, error: retryResponse};
    }

    // 元の処理を再度呼ぶ
    if (isRetry) {
      try {
        const retryResponse = await instance.request(error.config);
        return {data: retryResponse.data, error: null};
      } catch (retryError) {
        // 何もしない
      }
    }

    let response;
    if (error.response) {
      response = {
        code: error.response.status,
        data: error.response.data,
      };
    } else {
      response = {
        code: UNEXPECTED_ERRORS,
        data: error,
      };
    }

    return {data: null, error: response};
  }

  /**
   * アクセストークンの再発行
   */
  private static async refreshToken (): Promise<boolean> {
    let result: boolean = false;
    const refreshToken = getRefreshToken();
    if (refreshToken) {
      const params = { refresh_token: refreshToken };
      const response = await instance.post('/api/rd/v1/auth/token', params);
      setToken({...response.data});
      result = true;
    }
    return result;
  }

}
