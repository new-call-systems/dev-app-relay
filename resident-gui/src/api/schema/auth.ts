import { IResident } from './resident';

export interface ILoginResponse {
  access_token: string;
  refresh_token: string;
  resident?: IResident;
}

export interface IAuthRequest {
  resident_code: string;
  property_code: string;
  room_code: string;
}

