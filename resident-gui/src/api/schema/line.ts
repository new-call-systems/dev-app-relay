export interface ResponseLiffId {
  liff_id: string;
}

export interface ResponseAuth {
  url: string;
}
