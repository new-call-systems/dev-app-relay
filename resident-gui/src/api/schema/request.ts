export type IAiRequestType =
  | "vendor"
  | "auto_response"
  | "in_house";

export const RequestStatus = {
  'waiting_grasped': 'waiting_grasped',
  'waiting_for_cancel': 'waiting_for_cancel',
  'waiting_for_open': 'waiting_for_open',
  'open': 'open',
  'bid': 'bid',
  'scheduling': 'scheduling',
  'in_progress': 'in_progress',
  'completed': 'completed',
  'deleted': 'deleted',
  'cancelled': 'cancelled',
  'transferred': 'transferred',
  'auto_completed': 'auto_completed',
  'discard': 'discard',
} as const;

export type RequestStatusType = typeof RequestStatus[keyof typeof RequestStatus];


export interface IRequestListRequest {
  filter?: string;
  page: number;
}

export interface IRequestResponse {
  request_id: number;
  request_type: IAiRequestType;
  response_text: string;
  may_unknown: boolean;
  can_order: boolean;
  support_center: boolean;
}

export interface ITimeResponse {
  time_section_id: number;
  label: string;
}

export interface IManagementCompanyResponse {
  name: string;
  support_tel: string;
}

export interface IOrderRequest {
  emergency_flg: boolean;
  preferable_times: preferableTime[];
}

export interface preferableTime {
  request_preferable_time_id?: string,
  date: string,
  time_section_id: number,
  selected?: boolean,
}

export interface IListResponse {
  current_page: number;
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: string;
  path: string;
  per_page: number;
  prev_page_url?: string;
  to: number;
  total: number;
  data: IRequest[];
}

interface IRequest {
  request_id: number;
  status: RequestStatusType;
  ai_category: string;
  ai_sub_category: string;
  created_at: string;
  emergency_flg: string;
  context: string;
  preferable_info: preferableTime[];
}

export interface IDetail {
  request_id: number;
  resident_name: string;
  resident_tel: string;
  property_name: string;
  property_address: string;
  room_no: string;
  status: string;
  emergency_flg: boolean;
  ai_category: string;
  ai_sub_category: string;
  context: string;
  response: string;
  created_at: string;
  updated_at: string;
  preferable_info: preferableTime[];
  images: MediaObject[];
  cancel_reason: string;
}

export interface MediaObject {
  type: string;
  url: string;
}

export interface AIRequest {
  original_text: string;
  process: Process | {};
  step: 'start' | 'target' | 'keyword' | 'state';
  abort: boolean;
}

export interface AIResponse {
  ai_category: string;
  ai_sub_category: string;
  complete: boolean;
  maybe_mistake: boolean;
  next_step: string;
  process: Process;
  // TODO: レスポンス違い
  // select_labels: {
  //   ai_text: string;
  //   text: string;
  // }[];
  select_labels: any;
  type_ai: string;
  specific?: {
    request_id: number;
    request_type: string;
    response_text: string;
    may_unknown: boolean;
    can_order: boolean;
    support_center: boolean;
  }
}

export type Process = {
  keyword: string;
  state: string;
  target: string;
}

export interface IBadge {
  open: number;
  scheduling: number;
  in_progress: number;
  completed: number;
  waiting_for_cancel: number;
  cancelled: number;
}
