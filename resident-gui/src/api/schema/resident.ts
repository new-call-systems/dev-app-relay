export interface IResident {
  resident_id: string;
  management_company_id: string;
  room_id: string;
  resident_code: string;
  name: string;
  tel: string;
  line_connected: boolean;
  created_at: string;
  updated_at: string;
}
