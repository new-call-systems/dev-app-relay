import axios, { AxiosResponse } from 'axios';
import { IAuthRequest } from "../schema/auth"


/**
 * えんくらぶのダミーリポジトリクラス
 */
export default class EnClub {

  /**
   * ログイン（1段階目）
   * ※本来はアプリ側で認証するので不要だがwebで完結するための暫定
   * 　またパラメタが他と違うためaxiosも独自で利用する
   *
   * @param residentCode: 入居者コード
   * @param propertyCode: 物件コード
   * @param roomCode: 部屋コード
   * @param basicKey: Basic認証用のキー
   */
  static login(
    residentCode: string,
    propertyCode: string,
    roomCode: string,
    basicKey: string,
  ): Promise<{ authCode: string }> {
    return new Promise( async (resolve, reject) => {
      const server = process.env.REACT_APP_ENDPOINT_RELAY_SERVER;
      const header = { headers: {"Authorization" : `Basic ${basicKey}`} };
      const params: IAuthRequest = {
        resident_code: residentCode,
        property_code: propertyCode,
        room_code: roomCode,
      };
      try {
        const res = await axios.post<typeof params, AxiosResponse<string>>(
          `${server}/api/rd/v1/auth/login`,
          params,
          header,
        );
        resolve({ authCode: res.data });
      } catch (e) {
        reject(e);
      }
    })
  }
}
