import HttpClient from '../client/axios';
import { IApiResult } from '../../types/apiResult';
import {
  IRequestListRequest,
  IRequestResponse,
  ITimeResponse,
  IManagementCompanyResponse,
  IOrderRequest,
  IListResponse,
  IDetail,
  AIRequest,
  AIResponse,
  IBadge,
} from '../schema/request';

/**
 * 問合せのリポジトリクラス
 */
export default class RequestRepository {

  /**
   * 問合せ登録
   * @param context
   */
  static post(
    context: string,
  ): Promise<IApiResult<IRequestResponse>> {
    const params = {context: context};
    return HttpClient.post<IRequestResponse>('/api/rd/v1/request', params);
  }

  /**
   * 問合せ登録 （Phase2）
   * @param context
   */
  static postV2(
    params: AIRequest,
  ): Promise<IApiResult<AIResponse>> {
    return HttpClient.post<AIResponse>('/api/rd/v2/request', params);
  }

  /**
   * 問合せ一覧
   */
  static getList(
    params: IRequestListRequest,
  ): Promise<IApiResult<IListResponse>> {
    return HttpClient.get<IListResponse>('/api/rd/v1/request', {params: params});
  }

  /**
   * 問合せ詳細
   * @param requestId
   */
  static getDetail(
    requestId: number
  ): Promise<IApiResult<IDetail>> {
    return HttpClient.get<IDetail>(`/api/rd/v1/request/${requestId}`);
  }

  /**
   * 希望時間のリスト
   */
  static getTimeSection(): Promise<IApiResult<ITimeResponse[]>> {
    return HttpClient.get<ITimeResponse[]>('/api/rd/v1/timeSection');
  }

  /**
   * 予約
   * @param requestId
   * @param params
   */
  static order(
    requestId: number,
    params: IOrderRequest,
  ): Promise<IApiResult<void>> {
    return HttpClient.post<void>(`/api/rd/v1/request/${requestId}/order`, params);
  }

  /**
   * キャンセル
   * @param requestId
   * @param reason
   */
  static cancel(
    requestId: number,
    reason: string,
  ): Promise<IApiResult<void>> {
    const params = {cancel_reason: reason};
    return HttpClient.post<void>(`/api/rd/v1/request/${requestId}/cancel`, params);
  }

  /**
   * 入札承認
   * @param requestId
   */
  static approve(
    requestId: number,
  ): Promise<IApiResult<void>> {
    return HttpClient.post<void>(`/api/rd/v1/request/${requestId}/approve`);
  }

  /**
   * 解決
   * @param requestId
   * @param solved
   */
  static solved(
    requestId: number,
    solved: boolean,
  ): Promise<IApiResult<void>> {
    const params = {solved: solved};
    return HttpClient.post<void>(`/api/rd/v1/request/${requestId}/solved`, params);
  }

  /**
   * 管理会社情報取得
   */
  static getManagementCompany(): Promise<IApiResult<IManagementCompanyResponse>> {
    return HttpClient.get<IManagementCompanyResponse>('/api/rd/v1/managementCompany');
  }

  /**
   * フィードバック登録
   * @param requestId
   * @param content
   */
  static feedback(
    requestId: number,
    content: string,
  ): Promise<IApiResult<void>> {
    const params = {feedback_content: content};
    return HttpClient.post<void>(`/api/rd/v1/request/${requestId}/feedback`, params);
  }

  /**
   * S3アップロード用URL取得
   * @param requestId
   * @param files
   */
  static getPreSignedUrl(
    requestId: number,
    files: string[],
  ): Promise<IApiResult<string[]>> {
    const params = {files: files};
    return HttpClient.get<string[]>(`/api/rd/v1/request/${requestId}/preSignedUrl`, {params: params});
  }

  /**
   * バッジ表示用情報取得
   */
  static getBadge(): Promise<IApiResult<IBadge>> {
    return HttpClient.get<IBadge>('/api/rd/v1/badge');
  }

}
