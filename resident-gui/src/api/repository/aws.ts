import axios from 'axios';

/**
 * AWS リポジトリクラス
 */
export default class AWSRepository {

  /**
   * S3の署名リンクにファイルをアップロードする
   *
   * @param preSignedUrl
   * @param file
   */
  static s3FileUpload(
    preSignedUrl: string,
    file: File,
  ) {
    return new Promise( async (resolve, reject) => {
      try {
        await axios({
          method: 'PUT',
          url: preSignedUrl,
          headers: {'Content-Type': file.type},
          data: file,

        }).then(response => {
          resolve(response);

        }).catch(err => {
          console.log(JSON.stringify(err));
          reject(err);
        });

      } catch (ex) {
        console.log(ex);
        reject(ex);
      }
    })
  }
}
