import HttpClient from '../client/axios';
import { ILoginResponse } from '../schema/auth';
import { IApiResult } from '../../types/apiResult';

/**
 * 認証のリポジトリクラス
 */
export default class AuthRepository {

  /**
   * ログイン
   * @param code
   */
  static login(
    code: string,
  ): Promise<IApiResult<ILoginResponse>> {
    return HttpClient.get<ILoginResponse>(`/api/rd/v1/auth/login?code=${code}`);
  }

}
