import HttpClient from '../client/axios';
import { IApiResult } from '../../types/apiResult';
import { ILoginResponse } from "../schema/auth";
import { IResident } from "../schema/resident";
import { ResponseLiffId, ResponseAuth } from "../schema/line";

/**
 * LINE連携用のリポジトリクラス
 */
export default class LineRepository {

  /**
   * LIFF ID取得
   *
   * @param lineClientId
   */
  static getLiffId(
    lineClientId: string
  ): Promise<IApiResult<ResponseLiffId>> {
    return HttpClient.get<ResponseLiffId>(`/api/rd/v1/auth/liff/${lineClientId}`);
  }

  /**
   * LINEログイン
   *
   * @param lineAccessToken
   * @param lineClientId
   */
  static login(
    lineAccessToken: string,
    lineClientId: string,
  ): Promise<IApiResult<ILoginResponse>> {
    const params = {
      line_access_token: lineAccessToken,
      line_client_id: lineClientId,
    };
    return HttpClient.post<ILoginResponse>('/api/rd/v1/auth/line-login', params);
  }

  /**
   * LINEアカウント連携用URL取得
   */
  static auth(): Promise<IApiResult<ResponseAuth>> {
    return HttpClient.get<ResponseAuth>('/api/rd/v1/line/auth');
  }

  /**
   * LINEアカウント連携解除
   */
  static deactivate(): Promise<IApiResult<IResident>> {
    return HttpClient.delete<IResident>('/api/rd/v1/line/auth');
  }

  /**
   * LINEアカウント連携認証
   *
   * @param code
   * @param state
   */
  static verify(
    code: string,
    state: string,
  ): Promise<IApiResult<IResident>> {
    const params = {
      code: code,
      state: state,
    };
    return HttpClient.get<IResident>('/api/rd/v1/line/verify', {params: params});
  }

}
