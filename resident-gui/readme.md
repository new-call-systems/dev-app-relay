
## deploy

流れは以下の通り：

1. build
    - package.jsonのScriptを使う
    - buildのタイプは３つ用意。development(=.env.staging.dev), staging(=.env.staging), production(.env.production)
        - development: AWSの開発環境用
        - staging: AWSの検証環境（最終テスト）用
        - production: AWSの本番環境用
    - 各envファイルは用意しておく（下記に詳細）
2. scp
    - 対象のAWS環境にbuildで出力されたファイルを転送する
    - サンプル：``scp -i {YOUR_SSH_KEY} -r {OUTPUT_BUILD_FOLDER} ubuntu@ec2-3-112-241-56.ap-northeast-1.compute.amazonaws.com:/home/ubuntu/resident-gui/{YYYYMMDD}``
    - 今のAWS開発環境はubuntuユーザー上で展開している（本番ではそうしない）
    - YYYYMMDDはリリースした日時を使用
3. deploy
    - 対象のEC2にSSHログインして以下のコマンドを実行
    - ``cd ~/resident-gui``
    - ``ln -nfs /home/ubuntu/resident-gui/{YYYYMMDD} ./current``(20201224 破棄)
    ``ln -nfs /home/ubuntu/resident-gui/{YYYYMMDD} ./resident``
    - ~/resident-gui/current にシンボリンクリンクを貼っており、nginxはそこのindex.htmlを見に行っている


## ENV
 - REACT_APP_ENDPOINT_RELAY_SERVER: 中継サーバへのパス。AWS環境だと現状は``"/"``
 - REACT_APP_BASIC_KEY: 管理会社のアクセスキー情報。本番環境だと使用しない
 - REACT_APP_BASE_URL: 中継サーバとの兼ね合いもあり、http://domain.com/ で展開できないため、uriを指定。今のところ、``"/resident"``。ここを変えたらnginxのconfも変える必要がある。
 -　package.json の homepageも合わせる必要がある。