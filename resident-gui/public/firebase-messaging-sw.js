importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.17.1/firebase-messaging.js');

/*
firebase.initializeApp({
  'messagingSenderId': '718109771173' //718109771173
});
*/

firebase.initializeApp({
  apiKey: "AIzaSyBe9RekhtLpTw8kDlGnloGibkaZ7nyQGfs",
  authDomain: "en-system-specs.firebaseapp.com",
  databaseURL: "https://en-system-specs.firebaseio.com",
  projectId: "en-system-specs",
  storageBucket: "en-system-specs.appspot.com",
  messagingSenderId: "718109771173",
  appId: "1:718109771173:web:f60cb4cbe8f268d7eb144c"
})

// バックグラウンドメッセージを処理できるようにFirebase Messagingのインスタンスを取得します。
// この一文がないとブラウザがアクティブなときでもPush通知を受信できません。
const messaging = firebase.messaging();

// サーバー側にPOSTするデータに `notification` が含まれている場合は
// `setBackgroundMessageHandler` は呼ばれない。
// `notification` をサーバー側で設定しない場合はここで通知のtitle, bodyを組み立てることになる。
// => Firebase Cloud Messaging（FCM）でより簡単にWebブラウザにPush通知を送るサンプル - DRYな備忘録
//    http://otiai10.hatenablog.com/entry/2017/06/22/023025

messaging.setBackgroundMessageHandler((payload) => {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    title: 'push-test-web',
    body: 'push-test-web: Background Message body',
    icon: '/favicon.ico',
    // icon: '/firebase-logo.png',
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

