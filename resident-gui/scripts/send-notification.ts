
import axios from "axios";

const url = 'https://fcm.googleapis.com/fcm/send';

// => メッセージを送信する  |  Firebase
//    https://firebase.google.com/docs/cloud-messaging/admin/send-messages?hl=ja#defining_the_message

// TODO get from env
const toToken = "dNjYkCk9xrM89uSoOvBBok:APA91bFiZLvo9VvK2yUpgqh-BKU3XIemC2iMcQ14KSSpVXSEZFs7yXZVbIod6Ag67yIogNBGvQVfviub6iNg0GncaTgC8BIszvmY6L6cSOEWUTJ9X1N3c_tUfxqytZf5y_lb-gsUS2GE";
const serverKey = "AAAApzKthaU:APA91bFn0ggbKJfMr9uTTTzzUhLlea5tXFe7YcxQO6zIxEw2Bk1y4rw-TzWrvNSv33zifygPI0GhE8mE4sZbeUej23jxCO7YBjDf7RM5BjXMmMhPK0qrgkuV4yOyIS8Te7mOrEWltQ-p"
const payload = {
  to: toToken,
  data: {
    foo: 'bar'
  },
  notification: {
    title: 'notification title',
    body: 'notification body',
  }
};

// fetchする際のオプション
// headerにサーバーキーを指定する
// サーバーキーはFirebaseコンソール > 設定 > クラウド メッセージング で確認できる
const headers = {
  'Authorization': `key=${serverKey}`,
  'Content-Type': 'application/json',
};

async function run(){
  try{
    const data =  JSON.stringify(payload)
    const res = await axios.post(url, data, { headers: headers } )
    console.log( res.status )
  }catch(e){
    console.log(e)
  }
};

run()


